package com.idsdatanet.d2.common.util;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.util.NumberUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.DaoUserContext;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.model.BasinFormation;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.CostAfeDetail;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.CostCurrencyLookup;
import com.idsdatanet.d2.core.model.CustomCodeLink;
import com.idsdatanet.d2.core.model.CustomFtrLink;
import com.idsdatanet.d2.core.model.CustomParameterLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DvdSummary;
import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.Platform;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.model.RigSlowPumpParam;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.model.WitsmlChangeLog;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.SummaryUtils;
import com.idsdatanet.d2.core.web.mvc.SystemMessage;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;
import com.idsdatanet.d2.drillnet.dailyUidMaintenance.DailyUidMaintenanceSetting;
import com.idsdatanet.d2.drillnet.operationPlanMaster.DefaultDynamicLookaheadDVDPlanImpl;
import com.idsdatanet.d2.drillnet.operationPlanMaster.DynamicLookaheadDVDPlan;
import com.idsdatanet.d2.drillnet.rigpump.RigPumpUtils;
import com.idsdatanet.d2.drillnet.well.Constant;
import com.idsdatanet.d2.tournet.TourUtils;

public class CommonUtil extends DefaultBeanSupport {
	private static String SPUD_DAY_ACTUAL_DURATION = "ACTUALDURATION";
	private static String SPUD_DAY_INCLUDE_CURRENT_DAY = "CALCINCURRENTDAY";
	
	private static CommonUtil configuredInstance = null;
	
	public static CommonUtil getConfiguredInstance() throws Exception {
		if(configuredInstance == null) configuredInstance = getSingletonInstance(CommonUtil.class); 
		return configuredInstance;
	}
	
	public ReportDaily getReportDailyWithPriorityAwareByDailyUid(UserSession session, String dailyUid) throws Exception {
		Operation operation = session.getCurrentOperation();
		if (operation == null) return null;
		return this.getReportDailyWithPriorityAwareByDailyUid(session, operation.getOperationUid(), operation.getOperationCode(), dailyUid, true);
	}
	
	public ReportDaily getReportDailyWithPriorityAwareByDailyUid(UserSession session, _Operation operation, String dailyUid, Boolean checkAccess) throws Exception {
		if (operation == null) return null;
		return this.getReportDailyWithPriorityAwareByDailyUid(session, operation.getOperationUid(), operation.getOperationCode(), dailyUid, true);
	}
	
	public ReportDaily getReportDailyWithPriorityAwareByDailyUid(UserSession session, String operationUid, String operationCode, String dailyUid, Boolean checkAccess) throws Exception {
		if (StringUtils.isBlank(operationUid)) return null;
		
		HashMap<String, String> reportTypePriority = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();

		String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and dailyUid=:dailyUid";
		String[] paramNames = {"operationUid", "dailyUid"};
		Object[] paramValues = {operationUid, dailyUid};
		List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
		Map<String, ReportDaily> reportDailyByReportType = new HashMap<String, ReportDaily>();
		
		if (reportDailyList != null && reportDailyList.size() > 0) {
			for (ReportDaily reportDaily : reportDailyList) {
				reportDailyByReportType.put(reportDaily.getReportType(), reportDaily);
			}
		}

		if (reportDailyByReportType != null && reportDailyByReportType.size() > 0) {
			if (reportDailyByReportType.containsKey(SummaryUtils.SUMMARY_TYPE_DDR)) {
				ReportDaily reportDaily = reportDailyByReportType.get(SummaryUtils.SUMMARY_TYPE_DDR);
				if (!checkAccess) return reportDaily;
				if (session.withinAccessScope(reportDaily)) {
					return reportDaily;
				}
			// ddr not found, check for priority to get report number
			} else {
				if (reportTypePriority != null) {
					for (Map.Entry entry : reportTypePriority.entrySet()) {
						String operationType = (String) entry.getKey();
						
						if (operationType.equals(operationCode)) {
							String reportType = (String) entry.getValue();
							String[] reportPriorities = reportType.split("[,]");
							
							for (String type : reportPriorities) {
								type = type.trim();
								if (reportDailyByReportType.containsKey(type)) {
									ReportDaily reportDaily = reportDailyByReportType.get(type);
									if (!checkAccess) return reportDaily;
									if (session.withinAccessScope(reportDaily)) {
										return reportDaily;
									}
								}
							}
						}
					}
				}
			}
		} else {
			// has daily object but do not has a reportDaily object
		}
		return null;
	}

	public SupportVesselInformation createSupportVesselInformation(String vesselName) throws Exception {
		SupportVesselInformation supportVesselInformation = new SupportVesselInformation();
		supportVesselInformation.setVesselName(vesselName);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(supportVesselInformation);
		
		// TODO index it
		
		return supportVesselInformation;
	}
	
	public LookupCompany createLookupCompany(String companyName) throws Exception {
		LookupCompany lookupCompany = new LookupCompany();
		lookupCompany.setCompanyName(companyName);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookupCompany);
		return lookupCompany;
	}

	public Platform createPlatform(String platformName) throws Exception {
		Platform platform = new Platform();
		platform.setPlatformName(platformName);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(platform);
		
		// TODO index it
		
		return platform;
	}
	
	public Basin createBasin(String name) throws Exception {
		Basin basin = new Basin();
		basin.setName(name);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(basin);
		return basin;
	}
	
	public RigInformation createRigInformation(String rigName) throws Exception {
		RigInformation rigInformation = new RigInformation();
		rigInformation.setRigName(rigName);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigInformation);
		return rigInformation;
	}
	
	public Campaign createCampaignOperation(String campaignName) throws Exception {
		Campaign campaign = new Campaign();
		campaign.setCampaignName(campaignName);
		campaign.setStartDate(new Date());
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(campaign);
		
		Operation coops = new Operation();
		coops.setOperationCampaignUid(campaign.getCampaignUid());
		coops.setOperationName(campaignName);
		coops.setIsCampaignOverviewOperation(true);
		coops.setOperationCode(MenuManager.CAMPAIGN_UID);
		coops.setWellboreFinalPurpose(MenuManager.CAMPAIGN_UID);
		coops.setWellboreUid(MenuManager.CAMPAIGN_UID);
		coops.setWellUid(MenuManager.CAMPAIGN_UID);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(coops);
		return campaign;
	}
	
	public Well createWell(String wellName, String onOffShore) throws Exception {
		Well well = new Well();
		well.setWellName(wellName);
		well.setOnOffShore(onOffShore);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well);
		return well;
	}
	
	public void setWellOnOffShore (String wellUid, String onOffShore) throws Exception {
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
		if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
			well.setOnOffShore(onOffShore);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well);
		}
	}
	
	public BasinFormation createBasinFormation(String formationName, String basinUid) throws Exception {
		BasinFormation basinFormation = new BasinFormation();
		
		basinFormation.setBasinUid(basinUid);
		basinFormation.setName(formationName);
		
		String strSql = "SELECT sequence FROM BasinFormation WHERE (isDeleted = false or isDeleted is null) and basinUid =:basinUid ORDER BY sequence DESC";
		String[] paramsFields = {"basinUid"};
		Object[] paramsValues = {basinUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Integer lastSequence = 0;
		if (lstResult.size() > 0){
			Object b = (Object) lstResult.get(0);
			if (b != null) lastSequence = (Integer) b + 1;
		}
		
		basinFormation.setSequence(lastSequence);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(basinFormation);
		return basinFormation;
	}
	
	public Wellbore createWellbore(String wellboreName, String wellUid, String parentWellboreUid) throws Exception {
		Wellbore wellbore = new Wellbore();
		wellbore.setWellUid(wellUid);
		wellbore.setParentWellboreUid(parentWellboreUid);
		wellbore.setWellboreName(wellboreName);
		if (StringUtils.isBlank(parentWellboreUid)) {
			wellbore.setWellboreType("INITIAL");
		} else {
			wellbore.setWellboreType("SIDETRACK");
		}
		wellbore.setWellboreFinalPurpose("");
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wellbore);
		return wellbore;
	}
	
	public Daily createDay (Date DayDate, String OperationUid, UserSession session, HttpServletRequest request, SystemMessage systemMessage) throws Exception  
	{
		return this.createDay (DayDate, OperationUid, session, request, systemMessage, null, null, null, null,true);
	}
	
	public Daily createDay (Date DayDate, String OperationUid, UserSession session, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_forward, String selectedRigUid, String[] selected_carry_dailyUid, String[] selected_carry_rigInformationUid) throws Exception  
	{
		return this.createDay(DayDate, OperationUid, session, request, systemMessage, selected_carry_forward, selectedRigUid, selected_carry_dailyUid, selected_carry_rigInformationUid, false);
	}
	
	public Daily createDay (Date DayDate, String OperationUid, UserSession session, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_forward, String selectedRigUid, String[] selected_carry_dailyUid, String[] selected_carry_rigInformationUid, boolean stopAutoInjection) throws Exception  
	{
		String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate =:thisDayDate AND operationUid=:thisOperationUid";
		String[] paramsFields = {"thisDayDate", "thisOperationUid"};
		Object[] paramsValues = {DayDate, OperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			return (Daily) lstResult.get(0);
		}else{
			Daily thisDay = new Daily();
			thisDay.setDayNumber("0");
			thisDay.setDayDate(DayDate);
			thisDay.setDepthMdMsl(0.00);
			thisDay.setDepthTvdMsl(0.00);
			thisDay.setOperationUid(OperationUid);
			saveDaily(thisDay,session,stopAutoInjection);
			//carryOverNextDayActivity(thisDay,session);
			if (ModuleConstants.TOUR_MANAGEMENT.equalsIgnoreCase(session.getCurrentView()))
				TourUtils.carryOverTourDaily(thisDay);
			D2ApplicationEvent.refresh();
			
			Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(OperationUid, thisDay.getDailyUid());
			int thisDayNum ;
			
			if (yesterday != null){
				String yesterdayNum = yesterday.getDayNumber();
				try{
					thisDayNum = Integer.valueOf(yesterdayNum);
					thisDayNum = thisDayNum + 1;
				}catch(Exception e){
					e.printStackTrace();
					thisDayNum = 0;
				}
			}else{
				thisDayNum = 1;
			}
			
			thisDay.setDayNumber(String.valueOf(thisDayNum));
			saveDaily(thisDay,session,stopAutoInjection);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			session.setCurrentDailyUid(thisDay.getDailyUid());
			if (ModuleConstants.TOUR_MANAGEMENT.equalsIgnoreCase(session.getCurrentView()))
			{
				RigTour rt = TourUtils.getRigTour(session.getCurrentOperationUid(), session.getCurrentDailyUid(), false);
				if (rt!=null)session.setCurrentRigTourUid(rt.getRigTourUid());
			}
			//carry over yesterday data, check if user select to cop[y from other operation or not
			//this.carryFromYesterday(thisDay, request, systemMessage, selected_carry_dailyUid, selected_carry_rigInformationUid, selected_carry_forward, selectedRigUid);
		
			this.setStartAndLastOperationDate(OperationUid);
			return thisDay;
		}
	}
	
	public void carryFromYesterday (Daily thisDay, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_dailyUid, String[] selected_carry_rigInformationUid, String[] selected_carry_forward, String selectedRigUid) throws Exception {
		if (selected_carry_dailyUid != null) {
			if (selected_carry_rigInformationUid != null) selectedRigUid = selected_carry_rigInformationUid[0];
			Daily copyFromDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(selected_carry_dailyUid[0]);
			if (copyFromDaily != null) 
				this.copyFromYesterday(copyFromDaily, request, systemMessage, selected_carry_forward, selectedRigUid, true);
		}
		else this.copyFromYesterday(thisDay, request, systemMessage, selected_carry_forward, selectedRigUid, false);
		
//		thisDay.setUsed(true);
//		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDay);
//		String hql = "FROM ReportDaily WHERE dailyUid=:dailyUid AND (isDeleted IS NULL OR isDeleted= false)";
//		List<ReportDaily> rp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "dailyUid", thisDay.getDailyUid());
//		if(rp != null && !rp.isEmpty()) {
//			ReportDaily reportDaily = rp.get(0); 
//			reportDaily.setCopyFromYesterdayUsed(true);
//			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
//		}
	}
	
	/**
	 * Ticket #18161
	 * Clone of carryFromYesterday, called by reportDataNodeListener
	 * 
	 */ 
	public void carryFromLastDay (Daily thisDay, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_dailyUid, String[] selected_carry_rigInformationUid, String[] selected_carry_forward, String selectedRigUid) throws Exception {
		if (selected_carry_dailyUid != null) {
			if (selected_carry_rigInformationUid != null) selectedRigUid = selected_carry_rigInformationUid[0];
			Daily copyFromDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(selected_carry_dailyUid[0]);
			if (copyFromDaily != null) 
				this.copyFromLastDay(copyFromDaily, request, systemMessage, selected_carry_forward, selectedRigUid, true);
		}
		else this.copyFromLastDay(thisDay, request, systemMessage, selected_carry_forward, selectedRigUid, false);
	}
	
	/**
	 * Helper method to stop auto-injection of well/wellbore uids
	 * 
	 * @param reportDaily
	 * @param session
	 * @throws Exception
	 */
	private void saveDaily(Daily daily, UserSession session,boolean stopAutoInjection) throws Exception {
		if (stopAutoInjection){
			List<String> excludedFields = new ArrayList<String>();
			excludedFields.add("daily.wellboreuid"); // all lowercase
			excludedFields.add("daily.welluid");
			ApplicationUtils.getConfiguredInstance().setCommonFields(daily, session, excludedFields); // groupUid still need to be auto injected
			boolean oldValue = DaoUserContext.getThreadLocalInstance().isAutoApplyCommonFieldsValue(); // remember the previous state
			DaoUserContext.getThreadLocalInstance().setAutoApplyCommonFieldsValue(false); // turn off auto injection
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(daily);
			DaoUserContext.getThreadLocalInstance().setAutoApplyCommonFieldsValue(oldValue); // restore the state
		} else {
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(daily);
		}
	}
	
	public void carryOverNextDayActivity (Daily thisDay, UserSession session) throws Exception {
		String thisDailyUid = thisDay.getDailyUid();
		String thisOperationUid = thisDay.getOperationUid();
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(thisOperationUid, thisDailyUid);
		if (yesterday != null){
			Date yesterdayDate = yesterday.getDayDate();
			Date todayDate = thisDay.getDayDate();
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(todayDate);
			thisCalendar.add(Calendar.DATE, -1);
			thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
			
			Date previousDate = thisCalendar.getTime();
			
			thisCalendar.setTime(yesterdayDate);
			thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
			
			yesterdayDate = thisCalendar.getTime();
			
			String yesterdayDailyUid = yesterday.getDailyUid();
			
			if (yesterdayDate != null && yesterdayDate.compareTo(previousDate) == 0){
				List<Activity> nextDayActivities = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity a WHERE (a.isDeleted = false or a.isDeleted is null) AND dailyUid = :dailyUid AND dayPlus = '1'", "dailyUid", yesterdayDailyUid);
				List<String> planReferenceUids = new ArrayList<String>();
				for (Activity activity : nextDayActivities) {
					
					//Ticket: 50780
					//Check carriedForwardActivityUid before proceed to create new day activities, 
					//Carry activity with blank carriedForwardActivityUid, not blank had been carried over before
					if(StringUtils.isNotBlank(activity.getCarriedForwardActivityUid())) {
						continue;
					}
					
					Activity newActivity = new Activity();
					PropertyUtils.copyProperties(newActivity, activity);
					
					String oldActivityUid = activity.getActivityUid();
					newActivity.setActivityUid(null);
					newActivity.setDayPlus(0);
					newActivity.setDailyUid(thisDailyUid);
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
					String strDate = sdf.format(newActivity.getStartDatetime());
					if (!strDate.equalsIgnoreCase("1970/01/01"))
					{
						newActivity.setStartDatetime(DateUtils.addDays(newActivity.getStartDatetime(), 1));
					    newActivity.setEndDatetime(DateUtils.addDays(newActivity.getEndDatetime(), 1));
					}
					
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newActivity);
					
					String newActivityUid = newActivity.getActivityUid();

					//check if there is custom code link, custom parameter details, or module parameter details linked to activity. create for new activity
					carryForwardActivityDescriptionMatrixLinkIfFound(newActivityUid, activity.getActivityUid(), thisDailyUid);
					
					//check if there's LL linked to next day activity
					carryForwardLessonLearnedLink(newActivityUid, activity.getActivityUid(), thisDay, session);
					
					//Ticket: 39128
					//Indicator if CarriedForwardActivityUid is set to null
					if (StringUtils.isBlank(newActivityUid)){
						newActivityUid = "Ticket:39128";
					}
					
					
					activity.setCarriedForwardActivityUid(newActivityUid);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(activity);
					
					
					//AUTO CREATE UNWANTED EVENT FOR EACH NPT ACTIVITY
					if("1".equals(GroupWidePreference.getValue(thisDay.getGroupUid(), GroupWidePreference.GWP_AUTO_CREATE_UNWANTEDEVENTS))){
						List<UnwantedEvent> ueList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM UnwantedEvent WHERE (isDeleted=false or isDeleted is null) and activityUid=:activityUid AND type='UE' ", "activityUid", oldActivityUid);
						for (UnwantedEvent ue : ueList) {
							ue.setActivityUid(newActivityUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ue);
						}
						List<ActivityUnwantedEventLink> ueLinkList  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ActivityUnwantedEventLink WHERE (isDeleted=false or isDeleted is null) and activityUid=:activityUid", "activityUid", oldActivityUid);
						for (ActivityUnwantedEventLink ueLink : ueLinkList) {
							ueLink.setActivityUid(newActivityUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ueLink);
						}
						
					}
					
					//remove yesterday's unwanted event (nptEvent) after carry over nextdayactivity
					//revert current day's to yesterday if deleted
					//(only keep one up to date copy)
					activity.setNptEventUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(activity);
					
					String planReference = activity.getPlanReference();
					if(StringUtils.isNotBlank(planReference)) {
						planReferenceUids.add(planReference);
					}
				}
				
				if(planReferenceUids.size() > 0) {
					DynamicLookaheadDVDPlan dvdHandler = new DefaultDynamicLookaheadDVDPlanImpl();
					dvdHandler.processDVDPlanPhaseActualData(planReferenceUids);
				}
			}
		}
	}
	
	private void carryForwardActivityDescriptionMatrixLinkIfFound(String newActivityUid, String activityUid, String newDailyUid) throws Exception
	{
		if (StringUtils.isNotBlank(newActivityUid) && StringUtils.isNotBlank(activityUid) && StringUtils.isNotBlank(newDailyUid)) {
			String sql = "FROM CustomCodeLink WHERE activityUid = :activityUid AND (isDeleted = FALSE or isDeleted IS NULL)";
			List<CustomCodeLink> resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"activityUid"}, new String[]{activityUid});
			if (resultList != null && !resultList.isEmpty()) {
				for (CustomCodeLink ccl: resultList) {
					CustomCodeLink carryForwardCCL = new CustomCodeLink();
					PropertyUtils.copyProperties(carryForwardCCL, ccl);
					carryForwardCCL.setActivityUid(newActivityUid);
					carryForwardCCL.setDailyUid(newDailyUid);
					carryForwardCCL.setCustomCodeLinkUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(carryForwardCCL);
					ccl.setIsDeleted(true);
					ccl.setSysDeleted(3);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ccl);
				}
			}

			sql = "FROM CustomFtrLink WHERE activityUid = :activityUid AND (isDeleted = FALSE or isDeleted IS NULL)";
			List<CustomFtrLink> resultListCFL = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"activityUid"}, new String[]{activityUid});
			if (resultListCFL != null && !resultListCFL.isEmpty()) {
				for (CustomFtrLink cfl: resultListCFL) {
					CustomFtrLink carryForwardCFL = new CustomFtrLink();
					PropertyUtils.copyProperties(carryForwardCFL, cfl);
					carryForwardCFL.setActivityUid(newActivityUid);
					carryForwardCFL.setDailyUid(newDailyUid);
					carryForwardCFL.setCustomFtrLinkUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(carryForwardCFL);
					cfl.setIsDeleted(true);
					cfl.setSysDeleted(3);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cfl);
				}
			}
			
			sql = "FROM CustomParameterLink WHERE activityUid = :activityUid AND (isDeleted = FALSE or isDeleted IS NULL)";
			List<CustomParameterLink> resultList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"activityUid"}, new String[]{activityUid});
			if (resultList2 != null && !resultList2.isEmpty()) {
				for (CustomParameterLink cpl: resultList2) {
					CustomParameterLink carryForwardCPL = new CustomParameterLink();
					PropertyUtils.copyProperties(carryForwardCPL, cpl);
					carryForwardCPL.setActivityUid(newActivityUid);
					carryForwardCPL.setDailyUid(newDailyUid);
					carryForwardCPL.setCustomParameterLinkUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(carryForwardCPL);
					cpl.setIsDeleted(true);
					cpl.setSysDeleted(3);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cpl);
				}
			}
			
			String[] tableNameList = {"FtrTripping","FtrBopEquipment","FtrDrillLineEquipment","FtrRig","FtrDrilling","FtrWorkoverCompletionSystem"};
			List<Object> ftrList = null;
			for (String tableName : tableNameList){
				String queryString = "from " + tableName +" where (isDeleted is null or isDeleted = FALSE) and activityUid = :activityUid";
				ftrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
				if (ftrList != null && ftrList.size() > 0) {
					for (int i = 0; i < ftrList.size(); i++) {
						Object o = ftrList.get(i);
						String classPathName = ApplicationUtils.getConfiguredInstance().getTablePackage();
						Class ftr = Class.forName(classPathName + tableName);
						Constructor constructor = null;
						Object temp = null;
						if(ftr != null){
							try {
								constructor = ftr.getDeclaredConstructor();
								if(constructor != null){
									temp = constructor.newInstance();
								}
							}catch (InvocationTargetException x) {
					            x.printStackTrace();
					          } catch (NoSuchMethodException x) {
					            x.printStackTrace();
					          } catch (InstantiationException x) {
					            x.printStackTrace();
					          } catch (IllegalAccessException x) {
					            x.printStackTrace();
					          }
						}
						PropertyUtils.copyProperties(temp, o);
						Method method;
						
						try {
							  method = ftr.getMethod("set"+tableName+"Uid", String.class);
							  method.invoke(temp,new Object[]{null});
							  method = ftr.getMethod("setActivityUid", String.class);
							  method.invoke(temp,new Object[]{newActivityUid});
							  method = ftr.getMethod("setDailyUid", String.class);
							  method.invoke(temp,new Object[]{newDailyUid});
						} catch (SecurityException x) {
								x.printStackTrace();
						} catch (NoSuchMethodException x) {
								x.printStackTrace();
						} catch (IllegalArgumentException x) { 
								x.printStackTrace();
						} catch (IllegalAccessException x) { 
								x.printStackTrace();
						} catch (InvocationTargetException x) { 
								x.printStackTrace();
						}
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(temp);
						PropertyUtils.setProperty(o, "isDeleted",true);
						PropertyUtils.setProperty(o, "sysDeleted",3);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o);
					}
				}
			}
		}
	}
	
	private void carryForwardLessonLearnedLink(String newActivityUid, String activityUid, Daily thisDay, UserSession session) throws Exception
	{
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		Operation selectedOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(thisDay.getOperationUid());
		if (selectedOperation == null) {
			selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, thisDay.getOperationUid());
		}
		String prefixPattern = GroupWidePreference.getValue(thisDay.getGroupUid(), "lessonTicketNumberPrefixPattern");
		String getPrefixName = formatDisplayPrefix(prefixPattern, userSelection, selectedOperation, thisDay, null);
		
		if (StringUtils.isNotBlank(newActivityUid) && StringUtils.isNotBlank(activityUid)) {
			String sql = "FROM ActivityLessonTicketLink WHERE activityUid = :activityUid AND (isDeleted = FALSE or isDeleted IS NULL)";
			List<ActivityLessonTicketLink> resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"activityUid"}, new String[]{activityUid});
			if (resultList != null && !resultList.isEmpty()) {
				for (ActivityLessonTicketLink altl: resultList) {
					ActivityLessonTicketLink carryForwardALT = new ActivityLessonTicketLink();
					PropertyUtils.copyProperties(carryForwardALT, altl);
					carryForwardALT.setActivityUid(newActivityUid);
					carryForwardALT.setActivityLessonTicketLinkUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(carryForwardALT);
					altl.setIsDeleted(true);
					altl.setSysDeleted(3);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(altl);
					
					String sql2 = "FROM LessonTicket WHERE lessonTicketUid = :lessonTicketUid AND (isDeleted = FALSE or isDeleted IS NULL)";
					List<LessonTicket> resultList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, new String[]{"lessonTicketUid"}, new String[]{carryForwardALT.getLessonTicketUid()});
					if (resultList2 != null && !resultList2.isEmpty()) {
						LessonTicket lt = (LessonTicket) resultList2.get(0);
						lt.setLessonTicketNumberPrefix(getPrefixName);
						lt.setRp2Date(thisDay.getDayDate());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lt);
					}
				}
			}
		}
	}
	
	private void copyFromYesterday (Daily thisDay, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_forward, String selectedRigUid, boolean fromOtherOperation) throws Exception
	{
		//StartNewDaySetting thisSetting =  StartNewDaySetting.getConfiguredInstance();
		//Map<String, String> dailyCommandBean = thisSetting.getDailyCommandBeans();
		
		Date thisDate = thisDay.getDayDate();
		
		if(selected_carry_forward != null){
			for (String CommandBean : selected_carry_forward){
				Object bean = this.getApplicationContext().getBean(CommandBean);
				if(bean instanceof BaseCommandBean){
					try{
						if (fromOtherOperation) ((BaseCommandBean) bean).carryForwardDataFromYesterday(request, thisDate, selectedRigUid, thisDay.getDailyUid());
						else ((BaseCommandBean) bean).carryForwardDataFromYesterday(request, thisDate, selectedRigUid, null);
					}catch(Exception e){
						e.printStackTrace();
						systemMessage.addError("Failed to carry data forward for " + CommandBean);
					}
				}
			}
		}
	}
	
	private void copyFromLastDay (Daily thisDay, HttpServletRequest request, SystemMessage systemMessage, String[] selected_carry_forward, String selectedRigUid, boolean fromOtherOperation) throws Exception
	{
		//StartNewDaySetting thisSetting =  StartNewDaySetting.getConfiguredInstance();
		//Map<String, String> dailyCommandBean = thisSetting.getDailyCommandBeans();
		
		Date thisDate = thisDay.getDayDate();
		
		if(selected_carry_forward != null){
			for (String CommandBean : selected_carry_forward){
				Object bean = this.getApplicationContext().getBean(CommandBean);
				if(bean instanceof BaseCommandBean){
					try{
						if (fromOtherOperation) ((BaseCommandBean) bean).carryForwardDataFromLastDay(request, thisDate, selectedRigUid, thisDay.getDailyUid());
						else ((BaseCommandBean) bean).carryForwardDataFromLastDay(request, thisDate, selectedRigUid, null);
					}catch(Exception e){
						e.printStackTrace();
						systemMessage.addError("Failed to carry data forward for " + CommandBean);
					}
				}
			}
		}
	}
	
	/**
	 * To calculate TAF value only then pass to saveTFA to save the value
	 * @param String bitrunUid, SystemMessage systemMessage
	 * @return Double
	 * @throws Exception
	 */
	private Double calculateTFA (String bitrunUid, CommandBean commandBean) throws Exception  
	{
		//can verify calculation here: http://www.tdaweb.com/TNFA.calculator.htm
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get bit nozzle
		String strSql = "SELECT nozzleQty, nozzleSize FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) and bitrunUid =:bitrunUid";
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleQty = 0.00;
		Double nozzleSize = 0.00;
		Double TFATotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){
				Object objNozzleQty = thisNozzleResult[0];
				Object objNozzleSize = thisNozzleResult[1];
				
				if (objNozzleQty != null && objNozzleSize != null) {
					nozzleQty = Double.parseDouble(objNozzleQty.toString());
					nozzleSize = Double.parseDouble(objNozzleSize.toString());
					
					// no need convert because base is m then the result will be in m2, formula = pie j2 * number of nozzle
					TFATotal = TFATotal + (3.142 * ((nozzleSize / 2) * (nozzleSize / 2)) * nozzleQty);
				}
				
			}
		}
		else {
			commandBean.getSystemMessage().addWarning("TFA Calculation: Bit Nozzle value empty -> TFA = 0.0");
			return 0.0;
		}
		
		//RETURN BASE VALUE, NO NEED TO CONVERT TO SCREEN VALUE AS THIS CALCULATION IS DONE AFTER NODE PROCESSED
		return TFATotal;
		
	}
	
	/**
	 * To save TAF value 
	 * @param String BharunUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public void saveTFA (String BharunUid, CommandBean commandBean) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double TFATotal = 0.0;
		//get this bitrun object then set the value to it and save
		String strSql = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		Bitrun thisBitrun;
		if (lstResult.size() > 0){
			for(Object objBitrun: lstResult){
				thisBitrun = (Bitrun) objBitrun;
				TFATotal = this.calculateTFA (thisBitrun.getBitrunUid(), commandBean);
				thisBitrun.setTfa(TFATotal);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBitrun, qp);
			}
		}
		return;
	}
	
	
	
	
	/**
	 * To get BharunUid base on bitrunUid  
	 * @param String BitrunUid
	 * @return String bharunUid
	 * @throws Exception
	 */
	public String getBharunUidFormBitrun (String BitrunUid) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "FROM Bitrun a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bitrunUid = :BitrunUid";
		String[] paramsFields = {"BitrunUid"};
		String[] paramsValues = {BitrunUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.size() > 0){
			Object objBitrun = (Object) lstResult.get(0);
			if (objBitrun instanceof Bitrun) {
				Bitrun thisBitrun = (Bitrun) objBitrun;
				return thisBitrun.getBharunUid();
			}
		}
			
		return null;
	}
	
	
	/**
	 * Check for Well Name duplication in Well Wizard Screen.
	 * @param name
	 * @param operationType
	 * @param parentWellUid
	 * @return
	 * @throws Exception
	 */
	public boolean isWellNameDuplicate(String name, String operationType, String parentWellUid) throws Exception {
		if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(operationType)) {
			List list = new ArrayList();
			// no parent well is selected. could be Wellbore or Operation
			if(StringUtils.isNotBlank(parentWellUid)) {
				// Operation type is DRLLG, therefore it's a Wellbore
				if(operationType.startsWith(Constant.DRLLG)) {
					String[] paramNames = {"parentWellboreUid", "wellboreName"};
					String[] paramValues = {parentWellUid, name};
					list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and parentWellboreUid = :parentWellboreUid and wellboreName = :wellboreName", paramNames, paramValues);
				// else it's an Operation
				} else {
					String[] paramNames = {"wellboreUid", "operationName"};
					String[] paramValues = {parentWellUid, name};
					list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid and operationName = :operationName", paramNames, paramValues);
				}
			// else it's a Well
			} else if(operationType.startsWith(Constant.DRLLG) || StringUtils.isBlank(parentWellUid)) {
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Well where (isDeleted = false or isDeleted is null) and wellName = :wellName", "wellName", name);
			}
			if(list.size() > 0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check whether current record is valid for checking Well Name in Well Wizard screen.
	 * Preventing from checking itself.
	 * @param session
	 * @param name
	 * @param parentWellUid
	 * @param operationType
	 * @return
	 * @throws Exception
	 */
	public boolean isValidForCheckingWellNameDuplicate(UserSession session, String name, String parentWellUid, String operationType, boolean isNewRecord) throws Exception {
		parentWellUid 				= null2EmptyString(parentWellUid);
		String operationUid 		= session.getCurrentOperationUid();
		String currentParentWellUid = "";
		
		// editing an existing record
		if(! isNewRecord && StringUtils.isNotBlank(operationUid)) {
			Operation currentOperation 	= (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
			
			if(StringUtils.isNotBlank(currentOperation.getOperationCode()) && currentOperation.getOperationCode().startsWith(Constant.DRLLG)) {
				Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, currentOperation.getWellboreUid());
				currentParentWellUid = null2EmptyString(wellbore.getParentWellboreUid());
			} else {
				currentParentWellUid = null2EmptyString(currentOperation.getWellboreUid());
			}
			
			if(! StringUtils.equals(name, currentOperation.getOperationName()) ||
			   ! StringUtils.equals(parentWellUid, currentParentWellUid)) {
				return true;
			}
		// else this is a new record.
		} else {
			return true;
		}
		return false;
	}
	
	/**
	 * Convert null to empty string
	 * @param value
	 * @return
	 */
	public String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}
	
	/**
	 * Calculate the days on operation using activity duration.
	 * Turn on GWP to include days spent prior to spud in days on well calculation. Default to off
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysOnOperation(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		
		String strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and (a.dayPlus is null or a.dayPlus = 0) " +
				"and a.dailyUid = d.dailyUid " +
				"AND d.dayDate <= :userDate " +
				"AND d.operationUid = :thisOperationUid " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {selectedDaily.getDayDate(), operationUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Double totalDays = 0.0;
		
		Object a = (Object) lstResult.get(0);
		if (a != null) totalDays = Double.parseDouble(a.toString());
		
		//Turn on GWP to include days spent prior to spud in days on well calculation. Default to off
		if("1".equals(GroupWidePreference.getValue(selectedDaily.getGroupUid(), GroupWidePreference.GWP_DAYSONWELL_EXCLUDE_DAYS_SPENT_PRIOR_TO_SPUD)))
		{
			return totalDays;
		}else{
		
			strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
			String[] paramsFields1 = {"thisOperationUid"};
			Object[] paramsValues1= {operationUid};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
			if (lstResult.size() > 0){
				Operation thisOperation = (Operation) lstResult.get(0);			
					if (thisOperation.getDaysSpentPriorToSpud() != null) totalDays = totalDays + thisOperation.getDaysSpentPriorToSpud();
			}
		}
		return totalDays;		
	}
	
	/**
	 * Calculate Day Curve (+/-)
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double dayCurve(String wellUid,String wellboreUid,String operationUid,String dailyUid) throws Exception{
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double daysOnWell = 0.0;
		Double plannedDaysCompleted = 0.0;
		Double dayCurve = 0.0;
		
		daysOnWell = (Double)daysOnOperation(operationUid,dailyUid);
		String strSql = "SELECT plannedDaysCompleted FROM ReportDaily "
				+ "WHERE wellUid = :wellUid "
				+ "AND wellboreUid = :wellboreUid "
				+ "AND operationUid = :operationUid "
				+ "AND dailyUid = :dailyUid "
				+ "AND (isDeleted=false OR isDeleted IS NULL)";
		String[] paramsFields = {"wellUid", "wellboreUid","operationUid","dailyUid"};
		Object[] paramsValues = {wellUid,wellboreUid,operationUid,dailyUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Object a = lstResult.get(0);
		
		if(a!=null) {
			plannedDaysCompleted = Double.parseDouble(a.toString());
		}
		
		dayCurve = daysOnWell - plannedDaysCompleted;
		
		return dayCurve;
	}
	/**
	 * Calculate Time since start of sidetrack
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	 */
	
	public Double daysOnOperationSiderack(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		
		String strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and (a.dayPlus is null or a.dayPlus = 0) " +
				"and a.dailyUid = d.dailyUid " +
				"AND d.dayDate <= :userDate " +
				"AND d.operationUid = :thisOperationUid " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {selectedDaily.getDayDate(), operationUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Double totalDays = 0.0;
		
		Object a = (Object) lstResult.get(0);
		if (a != null) totalDays = Double.parseDouble(a.toString());
			return totalDays;		
	}
	/**
	 * Calculate the days on operation after spud date specify in operation page using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysFromSpudOnOperation(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			if (thisOperation.getSpudDate() != null){
				
				Date thisSpudDatetime = thisOperation.getSpudDate();
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				//GEt spud date in date only
				Calendar thisSpudDateTimeObj = Calendar.getInstance();
				Calendar thisSpudDateObj = Calendar.getInstance();
				
				thisSpudDateTimeObj.setTime(thisSpudDatetime);
				
				thisSpudDateObj.set(Calendar.YEAR, thisSpudDateTimeObj.get(Calendar.YEAR));
				thisSpudDateObj.set(Calendar.MONTH, thisSpudDateTimeObj.get(Calendar.MONTH));
				thisSpudDateObj.set(Calendar.DAY_OF_MONTH, thisSpudDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisSpudDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisSpudDateObj.set(Calendar.MINUTE, 0);
				thisSpudDateObj.set(Calendar.SECOND, 0);
				thisSpudDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from spud figure
				Double totalDays = 0.0;
				
				//if the date is after the spud date, then do the calc
				if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) >= 0)
				{
					//sum all hours after this date
					strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND (d.dayDate > :spudDate " +
							"AND d.dayDate <= :todayDate) " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields1 = {"spudDate", "todayDate", "thisOperationUid"};
					Object[] paramsValues1 = {thisSpudDateObj.getTime(), selectedDaily.getDayDate(), operationUid};
					
					List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
					
					
					Object a = (Object) lstResult1.get(0);
					if (a != null) totalDays = Double.parseDouble(a.toString());
					
					//get duration portion on the spud date it self
					Calendar thisSpudTimeObj = Calendar.getInstance();
					thisSpudTimeObj.setTimeInMillis(0);	
					thisSpudTimeObj.set(Calendar.HOUR_OF_DAY, thisSpudDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisSpudTimeObj.set(Calendar.MINUTE, thisSpudDateTimeObj.get(Calendar.MINUTE));
					
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :spudDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"spudDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisSpudDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF SPUD DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();								
								} else {
									if ((thisActivity.getStartDatetime().getTime() > thisSpudDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() > thisSpudDatetime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= thisSpudDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisSpudDatetime.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long spudTime = thisSpudDatetime.getTime();
										totalDays = totalDays + ((endTime - spudTime)/1000);
									}
								}
							}
						}
					}
				}
				//check if there is any days recorded in prev well that need to consider in days from spud - DaysSpuddedPreviousWell
				if (thisOperation.getDaysSpuddedPreviousWell() != null) totalDays = totalDays + thisOperation.getDaysSpuddedPreviousWell();
				
				return totalDays;
			}
		}
		
		return 0.0;
	}
	
	/**
	 * Calculate the days on operation after spud date specify in operation page using activity duration for procument kpi report.
	 * @param operationUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysFromSpudOnOperationProcurement(String operationUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			Date startDate=null;
			Date endDate=null;
			if(thisOperation.getSpudDate()!=null && thisOperation.getRigOffHireDate()!=null)
			{
				startDate = thisOperation.getSpudDate();
				endDate = thisOperation.getRigOffHireDate();
			}
			if(thisOperation.getSpudDate()!=null && thisOperation.getRigOffHireDate()==null)
			{
				startDate = thisOperation.getSpudDate();
				endDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "DESC");
			}
			if (startDate != null && endDate!=null){
				
				Date thisStartDatetime = startDate;
				
				
				//GEt spud date in date only
				Calendar thisStartDateTimeObj = Calendar.getInstance();
				Calendar thisStartDateObj = Calendar.getInstance();
				
				thisStartDateTimeObj.setTime(thisStartDatetime);
				
				thisStartDateObj.set(Calendar.YEAR, thisStartDateTimeObj.get(Calendar.YEAR));
				thisStartDateObj.set(Calendar.MONTH, thisStartDateTimeObj.get(Calendar.MONTH));
				thisStartDateObj.set(Calendar.DAY_OF_MONTH, thisStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisStartDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisStartDateObj.set(Calendar.MINUTE, 0);
				thisStartDateObj.set(Calendar.SECOND, 0);
				thisStartDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from spud figure
				Double totalDays = 0.0;
				
				//if the date is after the spud date, then do the calc
				if (endDate.compareTo(thisStartDateObj.getTime()) >= 0)
				{
					//sum all hours after this date
					strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND (d.dayDate > :startDate " +
							"AND d.dayDate <= :endDate) " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields1 = {"startDate", "endDate", "thisOperationUid"};
					Object[] paramsValues1 = {thisStartDateObj.getTime(), endDate, operationUid};
					
					List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
					
					
					Object a = (Object) lstResult1.get(0);
					if (a != null) totalDays = Double.parseDouble(a.toString());
					
					//get duration portion on the spud date it self
					Calendar thisSpudTimeObj = Calendar.getInstance();
					thisSpudTimeObj.setTimeInMillis(0);	
					thisSpudTimeObj.set(Calendar.HOUR_OF_DAY, thisStartDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisSpudTimeObj.set(Calendar.MINUTE, thisStartDateTimeObj.get(Calendar.MINUTE));
					
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :startDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"startDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisStartDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF SPUD DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();								
								} else {
									if ((thisActivity.getStartDatetime().getTime() > thisStartDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() > thisStartDatetime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= thisStartDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisStartDatetime.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long spudTime = thisStartDatetime.getTime();
										totalDays = totalDays + ((endTime - spudTime)/1000);
									}
								}
							}
						}
					}
				}
				//check if there is any days recorded in prev well that need to consider in days from spud - DaysSpuddedPreviousWell
				if (thisOperation.getDaysSpuddedPreviousWell() != null) totalDays = totalDays + thisOperation.getDaysSpuddedPreviousWell();
				
				return totalDays;
			}
		}
		
		return 0.0;
	}
	
	/**
	 * This function is use to get list of daily for a report type and order ASC
	 * @param operationUid, reportType
	 * @return List<ReportDaily> - list hold all the reportDaily object
	 * @throws Exception
	 */
	public List<ReportDaily> getDailyForOperationProcurement(String operationUid, String reportType, Date startDate, Date endDate) throws Exception{
		return this.getDailyForOperationProcurement(operationUid, reportType, "ASC", startDate, endDate);
	}
	
	/**
	 * This function is use to get list of daily for a report type and order ASC or DESC according to sortOrder parameter
	 * @param operationUid, reportType
	 * @param sortOrder - ASC or DESC
	 * @return List<ReportDaily> - list hold all the reportDaily object
	 * @throws Exception
	 */
	public List<ReportDaily> getDailyForOperationProcurement(String operationUid, String reportType, String sortOrder, Date startDate, Date endDate) throws Exception{
		
		if (reportType==null) reportType = "";
		
		String strSql = "SELECT rd FROM Daily d, ReportDaily rd WHERE (d.isDeleted = false or d.isDeleted is null) AND rd.reportDatetime>=:startDate AND rd.reportDatetime<=:endDate AND (rd.isDeleted = false or rd.isDeleted is null) AND rd.operationUid = :operationUid AND rd.reportType in (:reportType) AND d.dailyUid = rd.dailyUid AND d.operationUid = rd.operationUid ORDER BY d.dayDate " + sortOrder;
		String[] paramsFields = {"operationUid", "reportType","startDate","endDate"};		
		Object[] paramsValues = {operationUid, Arrays.asList(reportType.split(",")), startDate, endDate};
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if(list.size() > 0)  {
			return (List<ReportDaily>) list;			
		}
		return null;
	}
	
	public Double calculateCummulativeDayCost(Date startDate, Date endDate, String operationUid) throws Exception {
		Double cumdaycost=null;

		String queryString = "SELECT sum(daycost) FROM ReportDaily " +
		"WHERE (isDeleted=false or isDeleted is null) " +
		"AND operationUid=:operationUid " +
		"AND reportDatetime >=:startDate AND reportDatetime<=:endDate AND reportType='DDR'";
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
				new String[] {"startDate", "endDate", "operationUid"}, 
				new Object[] {startDate, endDate, operationUid});
		
		if(list.size()>0 && list!=null)
		{
			for (Object result : list) {
				if (result!=null) {
					cumdaycost = Double.parseDouble(result.toString());
				}
			}
		}
		return cumdaycost;
	}
	/**
	 * This function is use to get first/last day date in the particular operation based on the sorting order of the day date
	 * @param operationUid, reportType
	 * @param sortOrder - ASC or DESC
	 * @return date
	 * @throws Exception
	 */
	public Date getFirstOrLastDateFromOperation(String operationUid, String reportType, String sortOrder) throws Exception{
		
		Date firstRecordDate=null;
		
		String strSql = "SELECT rd.reportDatetime FROM Daily d, ReportDaily rd WHERE (d.isDeleted = false or d.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) AND rd.operationUid = :operationUid AND rd.reportType in (:reportType) AND d.dailyUid = rd.dailyUid AND d.operationUid = rd.operationUid ORDER BY d.dayDate " + sortOrder;
		String[] paramsFields = {"operationUid", "reportType"};		
		Object[] paramsValues = {operationUid, Arrays.asList(reportType.split(","))};
		List dayList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if(dayList.size() > 0 && dayList !=null)  {
			Object dayDateRecord = (Object) dayList.get(0);
			if(dayDateRecord!=null)
			{
				firstRecordDate = (Date) dayDateRecord;
				return firstRecordDate;
			}
		}
		return null;
	}
	/**
	 * Calculate the days on operation after spud date specify in operation page using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @param daysFromSpudByDateCalcType
	 * @return raw value in second
	 * @throws Exception
	 */
	
	public Double daysFromSpudOnOperationByDate(String operationUid, String dailyUid, String daysFromSpudByDateCalcType) throws Exception {

		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String queryString = "FROM Operation o, Daily d " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"AND (o.isDeleted = false or o.isDeleted is null) " +
				"AND o.operationUid=d.operationUid " +
				"AND d.dailyUid=:thisDailyUid " +
				"and o.operationUid=:thisOperationUid";
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"thisOperationUid","thisDailyUid"}, new Object[]{operationUid, dailyUid}, qp);
		if (lstResult.size()>0) {
			Operation thisOperation = (Operation) lstResult.get(0)[0];
			Daily thisDaily = (Daily) lstResult.get(0)[1];
			if (thisOperation.getSpudDate() != null && thisDaily.getDayDate()!=null){
				Calendar thisCalendar = Calendar.getInstance();
				if(daysFromSpudByDateCalcType==null){daysFromSpudByDateCalcType="";}
				
				//spud date
				thisCalendar.setTime(thisOperation.getSpudDate());
				if(daysFromSpudByDateCalcType.toUpperCase().equals(SPUD_DAY_INCLUDE_CURRENT_DAY) || StringUtils.isBlank(daysFromSpudByDateCalcType)){
					thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
					thisCalendar.set(Calendar.MINUTE, 0);
					thisCalendar.set(Calendar.SECOND , 0);
					thisCalendar.set(Calendar.MILLISECOND , 0);
				}
				long spudDatetime = thisCalendar.getTimeInMillis() / 1000;
				
				//current selected date
				thisCalendar.setTime(thisDaily.getDayDate());
				if(daysFromSpudByDateCalcType.toUpperCase().equals(SPUD_DAY_INCLUDE_CURRENT_DAY) || daysFromSpudByDateCalcType.toUpperCase().equals(SPUD_DAY_ACTUAL_DURATION)){
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
				    thisCalendar.set(Calendar.MINUTE, 59);
				    thisCalendar.set(Calendar.SECOND , 59);
				    thisCalendar.set(Calendar.MILLISECOND , 999);
				}
				long todayDatetime = (thisCalendar.getTimeInMillis() / 1000);
				
				return (todayDatetime - spudDatetime) * 1.0;
			}			
		}
		
		return 0.0;
	}
	
	/**
	 * Calculate the days on location for this operation after rig arrival date specify in operation page using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysOnLocationForOperation(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			if (thisOperation.getOnlocDateOnlocTime() != null){
				
				Date thisOnLocationDatetime = thisOperation.getOnlocDateOnlocTime();
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				//Get on location date in date only
				Calendar thisOnLocationDateTimeObj = Calendar.getInstance();
				Calendar thisOnLocationDateObj = Calendar.getInstance();
				
				thisOnLocationDateTimeObj.setTime(thisOnLocationDatetime);
				
				thisOnLocationDateObj.set(Calendar.YEAR, thisOnLocationDateTimeObj.get(Calendar.YEAR));
				thisOnLocationDateObj.set(Calendar.MONTH, thisOnLocationDateTimeObj.get(Calendar.MONTH));
				thisOnLocationDateObj.set(Calendar.DAY_OF_MONTH, thisOnLocationDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisOnLocationDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisOnLocationDateObj.set(Calendar.MINUTE, 0);
				thisOnLocationDateObj.set(Calendar.SECOND, 0);
				thisOnLocationDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from on location figure
				Double totalDays = 0.0;
				
				//if the date is after the on location date, then do the calc
				if (selectedDaily.getDayDate().compareTo(thisOnLocationDateObj.getTime()) >= 0)
				{
					//sum all hours after this date
					strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND (d.dayDate > :spudDate " +
							"AND d.dayDate <= :todayDate) " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields1 = {"spudDate", "todayDate", "thisOperationUid"};
					Object[] paramsValues1 = {thisOnLocationDateObj.getTime(), selectedDaily.getDayDate(), operationUid};
					
					List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
					
					
					Object a = (Object) lstResult1.get(0);
					if (a != null) totalDays = Double.parseDouble(a.toString());
					
					//get duration portion on the on location date it self
					Calendar thisOnLocationTimeObj = Calendar.getInstance();
					thisOnLocationTimeObj.setTimeInMillis(0);	
					thisOnLocationTimeObj.set(Calendar.HOUR_OF_DAY, thisOnLocationDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisOnLocationTimeObj.set(Calendar.MINUTE, thisOnLocationDateTimeObj.get(Calendar.MINUTE));
					
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :onLocDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"onLocDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisOnLocationDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF ON LOCATION DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > thisOnLocationDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() > thisOnLocationDatetime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= thisOnLocationDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisOnLocationDatetime.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long onLocTime = thisOnLocationDatetime.getTime();
										totalDays = totalDays + ((endTime - onLocTime)/1000);
									}
								}
							}
						}
					}
				}
				//check if there is any days recorded in prev well that need to consider in days - aysSpentPriorToSpud
				if (thisOperation.getDaysSpentPriorToSpud() != null) totalDays = totalDays + thisOperation.getDaysSpentPriorToSpud();
				
				return totalDays;
			}
		}
		
		return 0.0;
	}
			
	public Double daysFromRigMoveForOperation(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			if (thisOperation.getRigOnHireDate() != null){
				
				Date thisRigOnHireDate = thisOperation.getRigOnHireDate();
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				//Get on location date in date only
				Calendar thisRigOnHireDateTimeObj = Calendar.getInstance();
				Calendar thisRigOnHireDateObj = Calendar.getInstance();
				
				thisRigOnHireDateTimeObj.setTime(thisRigOnHireDate);
				
				thisRigOnHireDateObj.set(Calendar.YEAR, thisRigOnHireDateTimeObj.get(Calendar.YEAR));
				thisRigOnHireDateObj.set(Calendar.MONTH, thisRigOnHireDateTimeObj.get(Calendar.MONTH));
				thisRigOnHireDateObj.set(Calendar.DAY_OF_MONTH, thisRigOnHireDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisRigOnHireDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisRigOnHireDateObj.set(Calendar.MINUTE, 0);
				thisRigOnHireDateObj.set(Calendar.SECOND, 0);
				thisRigOnHireDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from on hire figure
				Double totalDays = 0.0;
				
				//if the date is after the on hire date, then do the calc
				if (selectedDaily.getDayDate().getTime() >= (thisRigOnHireDate.getTime()))
				{
					Long diffDate = selectedDaily.getDayDate().getTime() - thisRigOnHireDate.getTime();
					totalDays = diffDate.doubleValue()/1000;
					
					//get duration portion on the on location date it self
					Calendar thisRigOnHireTimeObj = Calendar.getInstance();
					thisRigOnHireTimeObj.setTimeInMillis(0);	
					thisRigOnHireTimeObj.set(Calendar.HOUR_OF_DAY, thisRigOnHireDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisRigOnHireTimeObj.set(Calendar.MINUTE, thisRigOnHireDateTimeObj.get(Calendar.MINUTE));
					
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :onMoveDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"onMoveDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisRigOnHireDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;

							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > thisRigOnHireDate.getTime()) && (thisActivity.getEndDatetime().getTime() > thisRigOnHireDate.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= thisRigOnHireDate.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisRigOnHireDate.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long onMoveTime = thisRigOnHireDate.getTime();
										totalDays = totalDays + ((endTime - onMoveTime)/1000);
									}
								}
							}
						}
					}
				}
				//check if there is any days recorded in prev well that need to consider in days - daysSpentPriorToSpud
				if (thisOperation.getDaysSpentPriorToSpud() != null) totalDays = totalDays + thisOperation.getDaysSpentPriorToSpud();
				
				return totalDays;
			}
		}
		
		return 0.0;
	}
	
public Double daysFromRigAcceptenceForOperation(String operationUid, String dailyUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			if (thisOperation.getRigAcceptanceDateTime() != null){
				
				Date thisRigAcceptenceDate = thisOperation.getRigAcceptanceDateTime();
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				//Get acceptance date in date only
				Calendar thisRigAcceptenceDateTimeObj = Calendar.getInstance();
				Calendar thisRigAcceptenceDateObj = Calendar.getInstance();
				
				thisRigAcceptenceDateTimeObj.setTime(thisRigAcceptenceDate);
				
				thisRigAcceptenceDateObj.set(Calendar.YEAR, thisRigAcceptenceDateTimeObj.get(Calendar.YEAR));
				thisRigAcceptenceDateObj.set(Calendar.MONTH, thisRigAcceptenceDateTimeObj.get(Calendar.MONTH));
				thisRigAcceptenceDateObj.set(Calendar.DAY_OF_MONTH, thisRigAcceptenceDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisRigAcceptenceDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisRigAcceptenceDateObj.set(Calendar.MINUTE, 0);
				thisRigAcceptenceDateObj.set(Calendar.SECOND, 0);
				thisRigAcceptenceDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from acceptance figure
				Double totalDays = 0.0;
				
				//if the date is after the acceptance date, then do the calc
				if (selectedDaily.getDayDate().getTime() >= (thisRigAcceptenceDate.getTime()))
				{
					Long diffDate = selectedDaily.getDayDate().getTime() - thisRigAcceptenceDate.getTime();
					totalDays = diffDate.doubleValue()/1000;
					
					//get duration portion on the rig acceptance date itself
					Calendar thisRigAcceptenceTimeObj = Calendar.getInstance();
					thisRigAcceptenceTimeObj.setTimeInMillis(0);	
					thisRigAcceptenceTimeObj.set(Calendar.HOUR_OF_DAY, thisRigAcceptenceDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisRigAcceptenceTimeObj.set(Calendar.MINUTE, thisRigAcceptenceDateTimeObj.get(Calendar.MINUTE));
					
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :onAcceptanceDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"onAcceptanceDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisRigAcceptenceDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;

							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > thisRigAcceptenceDate.getTime()) && (thisActivity.getEndDatetime().getTime() > thisRigAcceptenceDate.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= thisRigAcceptenceDate.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisRigAcceptenceDate.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long onAcceptanceTime = thisRigAcceptenceDate.getTime();
										totalDays = totalDays + ((endTime - onAcceptanceTime)/1000);
									}
								}
							}
						}
					}
				}
				//check if there is any days recorded in prev well that need to consider in days - daysSpentPriorToSpud
				if (thisOperation.getDaysSpentPriorToSpud() != null) totalDays = totalDays + thisOperation.getDaysSpentPriorToSpud();
				
				return totalDays;
			}
		}
		
		return 0.0;
	}
	
	/**
	 * Calculate the days since re-entry specify in operation page using activity duration if GWP set to Yes.
	 * If No will calculate from current report date to re-entry date from well operation.
	 * @param operationUid
	 * @param dailyUid
	 * @param groupUid
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysFromReEntryOperation(String operationUid, String dailyUid, String groupUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);		
		
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			if (thisOperation.getReentryDateReentryTime() != null){
				
				Date thisReEntryDatetime = thisOperation.getReentryDateReentryTime();
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				//GEt re-entry date in date only
				Calendar thisReEntryDateTimeObj = Calendar.getInstance();
				Calendar thisReEntryDateObj = Calendar.getInstance();
				
				thisReEntryDateTimeObj.setTime(thisReEntryDatetime);
				
				thisReEntryDateObj.set(Calendar.YEAR, thisReEntryDateTimeObj.get(Calendar.YEAR));
				thisReEntryDateObj.set(Calendar.MONTH, thisReEntryDateTimeObj.get(Calendar.MONTH));
				thisReEntryDateObj.set(Calendar.DAY_OF_MONTH, thisReEntryDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisReEntryDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisReEntryDateObj.set(Calendar.MINUTE, 0);
				thisReEntryDateObj.set(Calendar.SECOND, 0);
				thisReEntryDateObj.set(Calendar.MILLISECOND, 0);
				
				//this hold the days from re-entry figure
				Double totalDays = 0.0;
				
				//If GWP set to YES, will calculate days since re-entry using activity duration
				if("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_ACT_DUR_TO_CALC_DAYS_SINCE_RE_ENTRY)))
				{
					//If the current report date is after the re-entry date, then do the calc
					if (selectedDaily.getDayDate().compareTo(thisReEntryDateObj.getTime()) >= 0)
					{
						//sum all hours after this date
						strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (d.isDeleted = false or d.isDeleted is null) " +
								"and (a.isDeleted = false or a.isDeleted is null) " +
								"and (a.dayPlus is null or a.dayPlus = 0) " +
								"and a.dailyUid = d.dailyUid " +
								"AND (d.dayDate > :reEntryDate " +
								"AND d.dayDate <= :todayDate) " +
								"AND d.operationUid = :thisOperationUid " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null)";
						String[] paramsFields1 = {"reEntryDate", "todayDate", "thisOperationUid"};
						Object[] paramsValues1 = {thisReEntryDateObj.getTime(), selectedDaily.getDayDate(), operationUid};
						
						List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);						
						
						Object a = (Object) lstResult1.get(0);
						if (a != null) totalDays = Double.parseDouble(a.toString());
						
						//get duration portion on the re-entry date it self
						Calendar thisReEntryTimeObj = Calendar.getInstance();
						thisReEntryTimeObj.setTimeInMillis(0);	
						thisReEntryTimeObj.set(Calendar.HOUR_OF_DAY, thisReEntryDateTimeObj.get(Calendar.HOUR_OF_DAY));
						thisReEntryTimeObj.set(Calendar.MINUTE, thisReEntryDateTimeObj.get(Calendar.MINUTE));
						
						strSql = "SELECT a FROM Daily d, Activity a " +
								"WHERE (d.isDeleted = false or d.isDeleted is null) " +
								"and (a.isDeleted = false or a.isDeleted is null) " +
								"and (a.dayPlus is null or a.dayPlus = 0) " +
								"and a.dailyUid = d.dailyUid " +
								"AND d.dayDate = :reEntryDate " +
								"AND d.operationUid = :thisOperationUid " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null)";
						String[] paramsFields2 = {"reEntryDate", "thisOperationUid"};
						Object[] paramsValues2 = {thisReEntryDateObj.getTime(), operationUid};
						
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
						if (lstResult.size() > 0){
							for(Object objActivity: lstResult){
								Activity thisActivity = (Activity) objActivity;
								
								//IF RE ENTRY DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
								if (thisActivity.getDayPlus()!=null){
									if (thisActivity.getDayPlus()==1) {
										if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
									} else {
										if ((thisActivity.getStartDatetime().getTime() > thisReEntryDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() > thisReEntryDatetime.getTime())){
											if(thisActivity.getActivityDuration() != null)
												totalDays = totalDays + thisActivity.getActivityDuration();
										}
										else if ((thisActivity.getStartDatetime().getTime() <= thisReEntryDatetime.getTime()) && (thisActivity.getEndDatetime().getTime() >= thisReEntryDatetime.getTime())){
											Long endTime = thisActivity.getEndDatetime().getTime();
											Long reEntryTime = thisReEntryDatetime.getTime();
											totalDays = totalDays + ((endTime - reEntryTime)/1000);
										}
									}
								}
							}
						}
					}
				}
				//If GWP set to NO, will calculate days since re-entry from current report date to well operation re-entry date
				else
				{					
					Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
					if (daily == null) return null;
					Date todayDate = daily.getDayDate();
					if (todayDate != null){
						double daysLapsed = (todayDate.getTime() + 86399999 - thisReEntryDatetime.getTime())/1000; // count the time lapse as at 23:59 of the day						
						totalDays = daysLapsed;						
					}					
				}				
				return totalDays;				
			}
		}		
		return 0.0;
	}
	
	
	/**
	 * Calculate the days on operation using activity duration by operation type.
	 * @param wellboreUid
	 * @param dailyUid
	 * @param operationCode
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysOnOperationByType(String wellboreUid, String dailyUid, String operationCode) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (selectedDaily==null) return 0.0;
		Date todayDate = selectedDaily.getDayDate();
		
		String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) FROM Activity a , Daily d, Operation o " +
				" WHERE (a.isDeleted=false or a.isDeleted is null) " +
				" AND (d.isDeleted=false or d.isDeleted is null)" +
				" AND (o.isDeleted=false or o.isDeleted is null) " +
				" AND a.dailyUid=d.dailyUid " +
				" AND d.operationUid=o.operationUid " +
				" AND o.wellboreUid=:wellboreUid " +
				" AND (a.isSimop=false or a.isSimop is null) " +
				" AND (a.isOffline=false or a.isOffline is null)" +
				" and (a.dayPlus is null or a.dayPlus = 0) " +
				" AND d.dayDate<=:dayDate " +
				" AND o.operationCode like :operationCode ";
		String[] paramsFields = {"wellboreUid","dayDate", "operationCode"};
		Object[] paramsValues= {wellboreUid,todayDate,operationCode+"%"};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.get(0)!=null){
			if (lstResult.size()>0) {
			Double duration = Double.parseDouble(lstResult.get(0).toString());
			if (duration==null) duration = 0.0;					 
				return duration;
			}
		}
		return 0.0;		
	}

	/**
	 * Calculate the days on operation using activity duration by operation type.
	 * @param wellUid
	 * @param dailyUid
	 * @param operationCode
	 * @return raw value in second
	 * @throws Exception
	 */
	public Double daysOnOperationByTypeByWell(String wellUid, String dailyUid, String operationCode) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (selectedDaily==null) return 0.0;
		Date todayDate = selectedDaily.getDayDate();
		
		String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) FROM Activity a , Daily d, Operation o " +
				" WHERE (a.isDeleted=false or a.isDeleted is null) " +
				" AND (d.isDeleted=false or d.isDeleted is null)" +
				" AND (o.isDeleted=false or o.isDeleted is null) " +
				" AND a.dailyUid=d.dailyUid " +
				" AND d.operationUid=o.operationUid " +
				" AND o.wellUid=:wellUid " +
				" AND (a.isSimop=false or a.isSimop is null) " +
				" AND (a.isOffline=false or a.isOffline is null)" +
				" and (a.dayPlus is null or a.dayPlus = 0) " +
				" AND d.dayDate<=:dayDate " +
				" AND o.operationCode like :operationCode ";
		String[] paramsFields = {"wellUid","dayDate", "operationCode"};
		Object[] paramsValues= {wellUid,todayDate,operationCode+"%"};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.get(0)!=null){
			if (lstResult.size()>0) {
			Double duration = Double.parseDouble(lstResult.get(0).toString());
			if (duration==null) duration = 0.0;					 
				return duration;
			}
		}
		return 0.0;		
	}
	
	/**
	 * Calculate the days since dry hole for this operation within dry hole start and end date in operation page using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	*/
	public Double daysFromDryHole(String operationUid, String dailyUid, String groupUid) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues= {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Operation thisOperation = (Operation) lstResult.get(0);
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			if (thisOperation.getSpudDate() != null){
				
				Date getSpudDateTime = thisOperation.getSpudDate();
				
				//GEt on start dry hole date in date only
				Calendar thisSpudDateTimeObj = Calendar.getInstance();
				Calendar thisSpudDateObj = Calendar.getInstance();
				
				thisSpudDateTimeObj.setTime(getSpudDateTime);
				
				thisSpudDateObj.set(Calendar.YEAR, thisSpudDateTimeObj.get(Calendar.YEAR));
				thisSpudDateObj.set(Calendar.MONTH, thisSpudDateTimeObj.get(Calendar.MONTH));
				thisSpudDateObj.set(Calendar.DAY_OF_MONTH, thisSpudDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisSpudDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisSpudDateObj.set(Calendar.MINUTE, 0);
				thisSpudDateObj.set(Calendar.SECOND, 0);
				thisSpudDateObj.set(Calendar.MILLISECOND, 0);
			
				//this hold the days from on location figure
				Double totalDays = 0.0;
				
				//if the date is after the dry hole start date, then do the calc
				if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) >= 0)
				{
					if (thisOperation.getDryHoleEndDateTime() != null)
					{
						
						Date getDryHoleEndDateTime = thisOperation.getDryHoleEndDateTime();				
						
						//GEt end dry hole date in date only
						Calendar thisDryHoleEndDateTimeObj = Calendar.getInstance();
						Calendar thisDryHoleEndDateObj = Calendar.getInstance();
						
						thisDryHoleEndDateTimeObj.setTime(getDryHoleEndDateTime);
						
						thisDryHoleEndDateObj.set(Calendar.YEAR, thisDryHoleEndDateTimeObj.get(Calendar.YEAR));
						thisDryHoleEndDateObj.set(Calendar.MONTH, thisDryHoleEndDateTimeObj.get(Calendar.MONTH));
						thisDryHoleEndDateObj.set(Calendar.DAY_OF_MONTH, thisDryHoleEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
						thisDryHoleEndDateObj.set(Calendar.HOUR_OF_DAY, 0);
						thisDryHoleEndDateObj.set(Calendar.MINUTE, 0);
						thisDryHoleEndDateObj.set(Calendar.SECOND, 0);
						thisDryHoleEndDateObj.set(Calendar.MILLISECOND, 0);
						
						//if date is after dry hole end date
						if (selectedDaily.getDayDate().compareTo(thisDryHoleEndDateObj.getTime()) >= 0)
						{
							//sum all hours after this date
							strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
									"WHERE (d.isDeleted = false or d.isDeleted is null) " +
									"and (a.isDeleted = false or a.isDeleted is null) " +
									"and (a.dayPlus is null or a.dayPlus = 0) " +
									"and a.dailyUid = d.dailyUid " +
									"AND (d.dayDate > :dryHoleDate " +
									"AND d.dayDate < :dryHoleEndDate) " +
									"AND d.operationUid = :thisOperationUid " +
									"AND (a.isSimop=false or a.isSimop is null) " +
									"AND (a.isOffline=false or a.isOffline is null)";
							String[] paramsFields1 = {"dryHoleDate", "dryHoleEndDate", "thisOperationUid"};
							Object[] paramsValues1 = {thisSpudDateObj.getTime(), thisDryHoleEndDateObj.getTime(), operationUid};
							
							List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
							
							Object a = (Object) lstResult1.get(0);
							if (a != null) totalDays = Double.parseDouble(a.toString());
							
							//get duration portion on the end dry hole date it self
							Calendar thisDryHoleEndTimeObj = Calendar.getInstance();
							thisDryHoleEndTimeObj.setTimeInMillis(0);	
							thisDryHoleEndTimeObj.set(Calendar.HOUR_OF_DAY, thisDryHoleEndDateTimeObj.get(Calendar.HOUR_OF_DAY));
							thisDryHoleEndTimeObj.set(Calendar.MINUTE, thisDryHoleEndDateTimeObj.get(Calendar.MINUTE));
								
							strSql = "SELECT a FROM Daily d, Activity a " +
									"WHERE (d.isDeleted = false or d.isDeleted is null) " +
									"and (a.isDeleted = false or a.isDeleted is null) " +
									"and (a.dayPlus is null or a.dayPlus = 0) " +
									"and a.dailyUid = d.dailyUid " +
									"AND d.dayDate = :dryHoleDate " +
									"AND d.operationUid = :thisOperationUid " +
									"AND (a.isSimop=false or a.isSimop is null) " +
									"AND (a.isOffline=false or a.isOffline is null)";
							String[] paramsFields3 = {"dryHoleDate", "thisOperationUid"};
							Object[] paramsValues3 = {thisDryHoleEndDateObj.getTime(), operationUid};
							
							lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
							if (lstResult.size() > 0){
								for(Object objActivity: lstResult){
									Activity thisActivity = (Activity) objActivity;
									
									//IF DRY HOLE DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
									if (thisActivity.getDayPlus()!=null){
										if (thisActivity.getDayPlus()==1) {
											if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
										} else {
											if ((thisActivity.getStartDatetime().getTime() < getDryHoleEndDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() <= getDryHoleEndDateTime.getTime())){
												if(thisActivity.getActivityDuration() != null)
													totalDays = totalDays + thisActivity.getActivityDuration();
											}
											else if ((thisActivity.getStartDatetime().getTime() < getDryHoleEndDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > getDryHoleEndDateTime.getTime())){
												Long startTime = thisActivity.getStartDatetime().getTime();
												Long dryHoleEndTime = getDryHoleEndDateTime.getTime();
												totalDays = totalDays + ((dryHoleEndTime - startTime)/1000);
											}
										}
									}
								}
							}
						}
						else
						{
							//sum all hours after this date
							strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
									"WHERE (d.isDeleted = false or d.isDeleted is null) " +
									"and (a.isDeleted = false or a.isDeleted is null) " +
									"and (a.dayPlus is null or a.dayPlus = 0) " +
									"and a.dailyUid = d.dailyUid " +
									"AND (d.dayDate > :dryHoleDate " +
									"AND d.dayDate <= :todayDate) " +
									"AND d.operationUid = :thisOperationUid " +
									"AND (a.isSimop=false or a.isSimop is null) " +
									"AND (a.isOffline=false or a.isOffline is null)";
							String[] paramsFields1 = {"dryHoleDate", "todayDate", "thisOperationUid"};
							Object[] paramsValues1 = {thisSpudDateObj.getTime(), selectedDaily.getDayDate(), operationUid};
							
							List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
							
							
							Object a = (Object) lstResult1.get(0);
							if (a != null) totalDays = Double.parseDouble(a.toString());
						}
					}
					else
					{
						//sum all hours after this date
						strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (d.isDeleted = false or d.isDeleted is null) " +
								"and (a.isDeleted = false or a.isDeleted is null) " +
								"and (a.dayPlus is null or a.dayPlus = 0) " +
								"and a.dailyUid = d.dailyUid " +
								"AND (d.dayDate > :dryHoleDate " +
								"AND d.dayDate <= :todayDate) " +
								"AND d.operationUid = :thisOperationUid " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null)";
						String[] paramsFields1 = {"dryHoleDate", "todayDate", "thisOperationUid"};
						Object[] paramsValues1 = {thisSpudDateObj.getTime(), selectedDaily.getDayDate(), operationUid};
						
						List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
						
						
						Object a = (Object) lstResult1.get(0);
						if (a != null) totalDays = Double.parseDouble(a.toString());
					}
					
					//get duration portion on the start dry hole date it self
					Calendar thisDryHoleStartTimeObj = Calendar.getInstance();
					thisDryHoleStartTimeObj.setTimeInMillis(0);	
					thisDryHoleStartTimeObj.set(Calendar.HOUR_OF_DAY, thisSpudDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisDryHoleStartTimeObj.set(Calendar.MINUTE, thisSpudDateTimeObj.get(Calendar.MINUTE));
						
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :dryHoleDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"dryHoleDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisSpudDateObj.getTime(), operationUid};
					
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF DRY HOLE DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > getSpudDateTime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() >= getSpudDateTime.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long dryHoleStartTime = getSpudDateTime.getTime();
										totalDays = totalDays + ((endTime - dryHoleStartTime)/1000);
									}
								}
							}
						}
					}
				}
				return totalDays;
			}
		}
		
		return 0.0;
	}	
	
	/**
	 * Maintenance of dailyUid for all related objects while reportDaily change the date
	 * @param previousDailyUid currentDailyUid reportType
	 * @return 
	 * @throws Exception
	 */
	public void changeDailyUid(String previousDailyUid, String currentDailyUid, String currentReportDailyUid, String reportType, String operationUid) throws Exception {
		DailyUidMaintenanceSetting thisSetting =  DailyUidMaintenanceSetting.getConfiguredInstance();
		
		String strStandardSql = " SET dailyUid=:currentDailyUid, lastEditDatetime = :lastEditDatetime WHERE dailyUid=:previousDailyUid";
		String[] paramsFields = {"previousDailyUid", "currentDailyUid", "lastEditDatetime"};
		Object[] paramsValues = {previousDailyUid, currentDailyUid, new Date()};
		String strSql = "";
		String thisObjectName = "";
		
		if ("DGR".equalsIgnoreCase(reportType)) {
			List<String> objectToMaintain = thisSetting.getDgrSetting();
			for(Iterator i = objectToMaintain.iterator(); i.hasNext(); ){
				thisObjectName = (String) i.next();
				strSql = "UPDATE " + thisObjectName + strStandardSql;
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			}
		} else {
			//if not DGR should be any type of operational reports - group under ddr now
			List<String> objectToMaintain = thisSetting.getDdrSetting();
			for(Iterator i = objectToMaintain.iterator(); i.hasNext(); ){
				thisObjectName = (String) i.next();
				strSql = "UPDATE " + thisObjectName + strStandardSql;
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			}
		}
		
		strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :previousDailyUid AND reportDailyUid <> :thisreportDailyUid";
		String[] paramsFields2 = {"previousDailyUid", "thisreportDailyUid"};
		String[] paramsValues2 = {previousDailyUid, currentReportDailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		//clear daily object if no report daily tie to it anymore
		if (lstResult.isEmpty()) {
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Daily set isDeleted = true, sysDeleted = 4, lastEditDatetime = :lastEditDatetime where dailyUid = :dailyUid", new String[] {"dailyUid","lastEditDatetime"}, new Object[] {previousDailyUid, new Date()});
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			this.setStartAndLastOperationDate(operationUid);
		}
	}
	
	/**
	 * lookup last LTI event for the rig
	 * @param currentRigInformationUid currentDailyUid
	 * @return HSEInciedentObject
	 * @throws Exception
	 */
	public HseIncident lastLTI(String currentRigInformationUid, String currentDailyUid) throws Exception {
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date todayDate = daily.getDayDate();
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(todayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
		thisCalendar.set(Calendar.MINUTE, 59);
		thisCalendar.set(Calendar.SECOND , 59);
		thisCalendar.set(Calendar.MILLISECOND , 59);
		
		/* 20101129-0737-sthien */
		String includeHistoricalName = "";
		if(currentRigInformationUid!=null){		
			String strSql = "select historicalName from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid = :rigInformationUid";
			List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", currentRigInformationUid);
			if (lstResult.size()>0) {
				String historicalName = lstResult.get(0);
				if (StringUtils.isNotBlank(historicalName)) {
					includeHistoricalName = " OR rigInformationUid='" + historicalName + "'";
				}			
			}
		}
		
		String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and (rigInformationUid = :currentRigInformationUid" + includeHistoricalName + ") AND hseEventdatetime <= :currentDate AND isLostTimeIncident = 1 and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime DESC";
		String[] paramsFields2 = {"currentRigInformationUid", "currentDate"};
		Object[] paramsValues2 = {currentRigInformationUid, thisCalendar.getTime()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		if (lstResult.size() > 0){
			return (HseIncident) lstResult.get(0);
		}
		
		return null;
	}
	
	/**
	 * lookup next LTI event for the rig
	 * @param currentRigInformationUid currentDailyUid
	 * @return HSEInciedentObject
	 * @throws Exception
	 */
	public HseIncident nextLTI(String currentRigInformationUid, String currentDailyUid) throws Exception {
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date todayDate = daily.getDayDate();
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(todayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
		thisCalendar.set(Calendar.MINUTE, 59);
		thisCalendar.set(Calendar.SECOND , 59);
		thisCalendar.set(Calendar.MILLISECOND , 59);
		
		/* 20101129-0737-sthien */
		String includeHistoricalName = "";
		if(currentRigInformationUid!=null){		
			String strSql = "select historicalName from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid = :rigInformationUid";
			List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", currentRigInformationUid);
			if (lstResult.size()>0) {
				String historicalName = lstResult.get(0);
				if (StringUtils.isNotBlank(historicalName)) {
					includeHistoricalName = " OR rigInformationUid='" + historicalName + "'";
				}			
			}
		}
		
		String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and (rigInformationUid = :currentRigInformationUid" + includeHistoricalName + ") AND hseEventdatetime > :currentDate AND isLostTimeIncident = 1 and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime";
		String[] paramsFields2 = {"currentRigInformationUid", "currentDate"};
		Object[] paramsValues2 = {currentRigInformationUid, thisCalendar.getTime()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		if (lstResult.size() > 0){
			return (HseIncident) lstResult.get(0);
		}
		
		return null;
	}

	/**
	 * lookup last BOP date
	 * @param currentRigInformationUid currentDailyUid
	 * @return BopLog
	 * @throws Exception
	 */
	public BopLog lastBopDate(String currentRigInformationUid, String currentDailyUid) throws Exception {
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date todayDate = daily.getDayDate();
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(todayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
		thisCalendar.set(Calendar.MINUTE, 59);
		thisCalendar.set(Calendar.SECOND , 59);
		thisCalendar.set(Calendar.MILLISECOND , 59);
		
		String strSql = "SELECT bl FROM Bop b, BopLog bl WHERE (b.bopUid= bl.bopUid) and (b.isDeleted = false or b.isDeleted is null) and (bl.isDeleted = false or bl.isDeleted is null) and b.removeDate is null and b.rigInformationUid = :currentRigInformationUid AND bl.testingDatetime <= :currentDate ORDER BY bl.testingDatetime DESC";
		String[] paramsFields2 = {"currentRigInformationUid", "currentDate"};
		Object[] paramsValues2 = {currentRigInformationUid, thisCalendar.getTime()};		
				
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		if (lstResult.size() > 0){	
			//BopLog bopLog = (BopLog) lstResult.get(0);
			//Temporary commented this logic 20100303-0209-ltchai
			/*if (todayDate.after(bopLog.getNextTestDatetime())){
				return null;
			}else{
				return (BopLog) lstResult.get(0);
			}*/
			return (BopLog) lstResult.get(0);
		}
		
		return null;
	}
	
	/**
	 * lookup latest Evacuation Drill's occurrence date
	 * @param currentRigInformationUid currentDailyUid
	 * @return HseIncidentObject
	 * @throws Exception
	 */
	
	public HseIncident lastEvacDrill(String currentRigInformationUid, String currentDailyUid) throws Exception {		
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date todayDate = daily.getDayDate();
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(todayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
		thisCalendar.set(Calendar.MINUTE, 59);
		thisCalendar.set(Calendar.SECOND , 59);
		thisCalendar.set(Calendar.MILLISECOND , 59);

		String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) " +
				"AND rigInformationUid = :currentRigInformationUid " +
				"AND DATE(hseEventdatetime) <= :currentDate " +
				"AND incidentCategory='Evacuation Drill' " +
				"ORDER BY hseEventdatetime DESC";
		
		String[] paramsFields2 = {"currentRigInformationUid", "currentDate"};
		Object[] paramsValues2 = {currentRigInformationUid, thisCalendar.getTime()};

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		if (lstResult.size() > 0){
			return (HseIncident) lstResult.get(0);
		}
		
		return null;
	}	
	
	/**
	 * lookup last casing section
	 * @param currentWellboreUid currentDailyUid
	 * @return Object
	 * @throws Exception
	 */
	public Object lastCasingSection(String groupUid, String currentWellboreUid, String currentDailyUid) throws Exception {
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date todayDate = daily.getDayDate();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strExcludeLinerConds = "";
		if ("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_AUTO_LOOKUP_LAST_CASING_SECION_EXCLUDING_LINER_TYPE))) strExcludeLinerConds += " and sectionName not in ('liner') ";
		
		String strSql = "FROM CasingSection WHERE (isDeleted = false or isDeleted is null) " + strExcludeLinerConds + " and wellboreUid = :currentWellboreUid AND installStartDate < :currentDate ORDER BY installStartDate DESC";
		String[] paramsFields2 = {"currentWellboreUid", "currentDate"};
		Object[] paramsValues2 = {currentWellboreUid, DateUtils.addDays(todayDate, 1)};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		
		if (lstResult.size() > 0){
			return lstResult.get(0);
		}
		
		return null;
	}
	
	public Object lastLinerSection(String groupUid, String currentWellboreUid, String currentDailyUid) throws Exception {
        Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
        if (daily == null) {
            return null;
        }
        Date todayDate = daily.getDayDate();
        QueryProperties qp = new QueryProperties();
        qp.setUomConversionEnabled(false);
        String strIncludeLinerConds = "";
        if ("1".equals(GroupWidePreference.getValue((String)groupUid, (String)"autoLookupLastLinerSection"))) {
            strIncludeLinerConds = String.valueOf(strIncludeLinerConds) + " and sectionName in ('liner', 'product liner') ";
        }
        String strSql = "FROM CasingSection WHERE (isDeleted = false or isDeleted is null) " + strIncludeLinerConds + " and wellboreUid = :currentWellboreUid AND installStartDate <= :currentDate ORDER BY installStartDate DESC";
        String[] paramsFields2 = new String[]{"currentWellboreUid", "currentDate"};
        Object[] paramsValues2 = new Object[]{currentWellboreUid, todayDate};
        List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
        if (lstResult.size() > 0) {
            return lstResult.get(0);
        }
        return null;
    }
	
	public Object lastFITLOT(String currentOperationUid, String currentDailyUid, String testType, Double casingSize) throws Exception {
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily == null) return null;
		Date currentDate = daily.getDayDate();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		testType = StringUtils.isBlank(testType)?"LOT":testType;
		
		String[] paramsFields = {"currentOperationUid","currentDate","testType","casingSize"};
		Object[] paramsValues = {currentOperationUid,currentDate,testType,casingSize};
		String strSql = "Select lot FROM LeakOffTest lot, Daily d WHERE (lot.isDeleted = false or lot.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and lot.operationUid=:currentOperationUid and d.dailyUid=lot.dailyUid and d.dayDate<=:currentDate AND lot.testType=:testType AND lot.casingSize = :casingSize order by d.dayDate desc, lot.testDateTime desc, lot.testDepthTvdMsl desc";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.size() > 0){
			if(lstResult.get(0)!=null)
			{
				return lstResult.get(0);
			}
		}
		
		return null;
	}
	
	private class LeakOffTestComparator implements Comparator<LeakOffTest>{

		public int compare(LeakOffTest lot1, LeakOffTest lot2) {
			// TODO Auto-generated method stub
			try {
				Daily d1 = ApplicationUtils.getConfiguredInstance().getCachedDaily(lot1.getDailyUid());
				Daily d2 = ApplicationUtils.getConfiguredInstance().getCachedDaily(lot2.getDailyUid());
				return d1.getDayDate().compareTo(d2.getDayDate());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
		
	}
	
	/**
	 * lookup last casing section LOT
	 * @param currentCasingSectionUid
	 * @return double as of base UOM
	 * @throws Exception
	 */
	/*public Double lastCasingSectionLOT(String currentCasingSectionUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT df.fieldValueDouble FROM DynamicField df WHERE (isDeleted = false or isDeleted is null) and df.fieldKey = :currentCasingSectionUid and df.fieldUid = 'casing_section.lot'";
		String[] paramsFields1 = {"currentCasingSectionUid"};
		Object[] paramsValues1 = {currentCasingSectionUid};

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
		
		if (lstResult.size() > 0){
			if (lstResult.get(0) != null) return Double.parseDouble(lstResult.get(0).toString());
		}
		
		return null;
	}
	*/
	/**
	 * lookup last casing section LOT
	 * @param currentCasingSectionUid
	 * @return double
	 * @throws Exception
	 */
	/* public Double lastCasingSectionFIT(String currentCasingSectionUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT df.fieldValueDouble FROM DynamicField df WHERE (isDeleted = false or isDeleted is null) and df.fieldKey = :currentCasingSectionUid and df.fieldUid = 'casing_section.fit'";
		String[] paramsFields1 = {"currentCasingSectionUid"};
		Object[] paramsValues1 = {currentCasingSectionUid};

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
		
		if (lstResult.size() > 0){
			if (lstResult.get(0) != null) return Double.parseDouble(lstResult.get(0).toString());
		}
		
		return null;
	}
	*/
	/**
	 * Auto get the current date base on default GMP setting (GWP) or well GMT setting
	 * @param String groupUid, String wellUid
	 * @return Date
	 * @throws Exception
	 */
	public Date currentDateTime(String groupUid, String wellUid) throws Exception {
		Date currentDate =  new Date();
		Well currentWell;
		Double thisOffset = 0.0;
		
		thisOffset = Double.parseDouble(GroupWidePreference.getValue(groupUid, "mainOfficeGMTZone"));
		
		String strSql = "FROM Well WHERE wellUid = :currentWellUid";
		String[] paramsFields2 = {"currentWellUid"};
		Object[] paramsValues2 = {wellUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
			
		if (lstResult.size() > 0){
			currentWell = (Well) lstResult.get(0);	
			if (currentWell.getTzGmtOffset() != null) thisOffset = currentWell.getTzGmtOffset();			
		}
		
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(currentDate);
		thisCalendar.add(Calendar.HOUR_OF_DAY, (int) Math.round(thisOffset));
		return thisCalendar.getTime();
	}
	
	/**
	 * Get first day in operation
	 * @param String operationUid, String stockcode, String currentDailyUid
	 * @return String
	 * @throws Exception
	 */
	public String getOperationStockcodeFirstDay(String operationUid, String stockcode, String currentDailyUid) throws Exception {
		if (StringUtils.isBlank(currentDailyUid)) return "0";
		
		String strSql = "SELECT d.dailyUid FROM RigStock rs, Daily d WHERE (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') AND rs.stockCode = :stockCode and d.operationUid = :thisOperationUid ORDER BY d.dayDate ASC";
		
		String[] paramsFields = {"stockCode", "thisOperationUid"};
		Object[] paramsValues = {stockcode, operationUid};	
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Daily firstDay = null; 
		
		if (lstResult.size() > 0){
			String dailyUid = lstResult.get(0).toString();	
			firstDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);						
		}
		
		if (firstDay != null){
			 if(firstDay.getDailyUid().compareTo(currentDailyUid) == 0) return "1";
			 else return "0";
		}						
		else return "1";
	}
	
	/**
	 * Get first day in operation in each support vessle
	 * @param String operationUid, String supportVesselInformationUid, String stockcode, String currentDailyUid
	 * @return String
	 * @throws Exception
	 */
	public String getOperationSupportVesselStockcodeFirstDay(String operationUid, String supportVesselInformationUid, String stockcode, String currentDailyUid) throws Exception {
				
		String strSql = "SELECT d.dailyUid FROM RigStock rs, Daily d WHERE (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rs.dailyUid = d.dailyUid and rs.supportVesselInformationUid= :thisSupportVesselInformationUid AND rs.stockCode = :stockCode and rs.operationUid = :thisOperationUid ORDER BY d.dayDate ASC";
		
		String[] paramsFields = {"stockCode", "thisOperationUid", "thisSupportVesselInformationUid"};
		Object[] paramsValues = {stockcode, operationUid, supportVesselInformationUid};	
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Daily firstDay = null; 
		
		if (lstResult.size() > 0){
			String dailyUid = lstResult.get(0).toString();	
			firstDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);						
		}
		
		if (firstDay != null){
			 if(firstDay.getDailyUid().compareTo(currentDailyUid) == 0) return "1";
			 else return "0";
		}						
		else return "1";
	}
	
	/**
	 * Get Stockcode previous balance
	 * @param String operationUid, String stockcode, Daily currentDaily
	 * @return Double
	 * @throws Exception
	 */
	public Double getOperationStockcodePreviousBalance(String operationUid, String rigInformationUid, String stockcode, Daily currentDaily, String stockType) throws Exception {
		
		Date todayDate = currentDaily.getDayDate();
		Double previousBalance = 0.0;
				
		//get the last re-start date and quantity, then calculate quantity from that onward
		//String strSql = "select rs.amtInitial, rs.dailyUid from RigStock rs, Daily d, Operation o WHERE (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate <= :todayDate and rs.stockCode = :stockCode and rs.rigInformationUid = :rigInformationUid and (rs.amtInitial >= 0) ORDER BY d.dayDate DESC";
		//String[] paramsFields1 = {"todayDate", "stockCode", "rigInformationUid"};
		//Object[] paramsValues1 = new Object[3]; paramsValues1[0] = todayDate; paramsValues1[1] = stockcode; paramsValues1[2] = rigInformationUid;

		String strSql = "select rs.amtInitial, rs.dailyUid from RigStock rs, Daily d, Operation o WHERE (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.operationUid = :operationUid and (rs.amtInitial >= 0) and type = :stockType ORDER BY d.dayDate DESC";
		String[] paramsFields1 = {"todayDate", "stockCode", "operationUid", "stockType"};
		Object[] paramsValues1 = new Object[4]; paramsValues1[0] = todayDate; paramsValues1[1] = stockcode; paramsValues1[2] = operationUid; paramsValues1[3] = stockType;
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
		
		if (lstResult1.size() > 0) {
			// set the result into temp object 'b'
			Object[] b = (Object[]) lstResult1.get(0);			
			
			// default 'InitialBalance' to 0.0
			//Double initialBalance = 0.0;
			BigDecimal initialBalance = BigDecimal.ZERO;
			//if (b[0] != null && StringUtils.isNotBlank(b[0].toString())) initialBalance = Double.parseDouble(b[0].toString());
			if (b[0] != null && StringUtils.isNotBlank(b[0].toString()))
				initialBalance = new BigDecimal(b[0].toString());
			
			String lastResetDailyUid = b[1].toString();
			Daily lastResetDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(lastResetDailyUid);
		
		
			if (lastResetDaily != null)
			{
				// the sql query string
				//strSql = "SELECT sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust FROM RigStock rs, Daily d, Operation o where (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate >= :lastResetDate and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.rigInformationUid = :rigInformationUid";
				//String[] paramsFields = {"todayDate", "lastResetDate", "stockCode", "rigInformationUid"};
				//Object[] paramsValues = new Object[4]; paramsValues[0] = todayDate; paramsValues[1] = lastResetDaily.getDayDate(); paramsValues[2] = stockcode; paramsValues[3] = rigInformationUid;
				
				strSql = "SELECT sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust FROM RigStock rs, Daily d, Operation o where (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate >= :lastResetDate and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.operationUid = :operationUid and rs.type = :stockType";
				String[] paramsFields = {"todayDate", "lastResetDate", "stockCode", "operationUid", "stockType"};
				Object[] paramsValues = new Object[5]; paramsValues[0] = todayDate; paramsValues[1] = lastResetDaily.getDayDate(); paramsValues[2] = stockcode; paramsValues[3] = operationUid; paramsValues[4] = stockType;
				// load any existing records and sum it up
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				// set the result into temp object 'a'
				Object[] a = (Object[]) lstResult.get(0);
				
				// check for any record(s) or not						
				if (a[0] != null && a[1] != null && a[2] != null) {
					// if previous record(s) exist, set 'previousBalance' 
					//previousBalance = initialBalance + Double.parseDouble(a[0].toString()) - Double.parseDouble(a[1].toString()) + Double.parseDouble(a[2].toString());
					BigDecimal sum = initialBalance.add(new BigDecimal(a[0].toString())).subtract(new BigDecimal(a[1].toString())).add(new BigDecimal(a[2].toString()));
					previousBalance = Double.valueOf(sum.doubleValue());
				} else {
					//previousBalance = initialBalance;
					previousBalance = Double.valueOf(initialBalance.doubleValue());
				}
				
				//retrun previous balance if got reset occur
				return previousBalance;
			}
		}
		
		//if no lastResetDay
		// the sql query string
		//strSql = "select sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust from RigStock rs, Daily d, Operation o where (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.rigInformationUid = :rigInformationUid";
		//String[] paramsFields = {"todayDate", "stockCode", "rigInformationUid"};
		//Object[] paramsValues = new Object[3]; paramsValues[0] = todayDate; paramsValues[1] = stockcode; paramsValues[2] = rigInformationUid;
		
		strSql = "select sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust from RigStock rs, Daily d, Operation o where (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.operationUid = o.operationUid and rs.dailyUid = d.dailyUid and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid='') and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.operationUid = :operationUid and rs.type = :stockType";
		String[] paramsFields = {"todayDate", "stockCode", "operationUid", "stockType"};
		Object[] paramsValues = new Object[4]; paramsValues[0] = todayDate; paramsValues[1] = stockcode; paramsValues[2] = operationUid; paramsValues[3] = stockType;
		
		// load any existing records and sum it up
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		// set the result into object 'a'
		Object[] a = (Object[]) lstResult.get(0);				
		
		// check for any record(s) or not
		if (a[0] != null && a[1] != null && a[2] != null) {
			// if previous record(s) exist, set 'previousBalance' 
			//previousBalance = Double.parseDouble(a[0].toString()) - Double.parseDouble(a[1].toString()) + Double.parseDouble(a[2].toString());
			BigDecimal sum = new BigDecimal(a[0].toString()).subtract(new BigDecimal (a[1].toString())).add(new BigDecimal (a[2].toString()));
			previousBalance = Double.valueOf(sum.doubleValue());
		} 
			
		return previousBalance;
	}
	
	/**
	 * Get Stockcode previous balance
	 * @param String operationUid, String supportVesselInformationUid, String stockcode, Daily currentDaily
	 * @return Double
	 * @throws Exception
	 */
	public Double getOperationSupportVesselStockcodePreviousBalance(String operationUid, String supportVesselInformationUid, String stockcode, Daily currentDaily) throws Exception {
		
		Date todayDate = currentDaily.getDayDate();
		Double previousBalance = 0.0;
				
		//get the last re-start date and quantity, then calculate quantity from that onward
		String strSql = "select rs.amtInitial, rs.dailyUid from RigStock rs, Daily d, Operation o WHERE (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rs.dailyUid = d.dailyUid and d.operationUid = o.operationUid and rs.supportVesselInformationUid = :supportVesselInformationUid and d.dayDate < :todayDate and rs.stockCode = :stockCode and (rs.amtInitial >= 0) and rs.operationUid = :operationUid ORDER BY d.dayDate DESC";
		String[] paramsFields1 = {"todayDate", "stockCode", "supportVesselInformationUid", "operationUid"};
		Object[] paramsValues1 = new Object[4]; paramsValues1[0] = todayDate; paramsValues1[1] = stockcode; paramsValues1[2] = supportVesselInformationUid; paramsValues1[3] = operationUid;
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
		
		if (lstResult1.size() > 0) {
			// set the result into temp object 'b'
			Object[] b = (Object[]) lstResult1.get(0);			
			
			// default 'InitialBalance' to 0.0
			//Double initialBalance = 0.0;
			BigDecimal initialBalance = BigDecimal.ZERO;
			//if (b[0] != null && StringUtils.isNotBlank(b[0].toString())) initialBalance = Double.parseDouble(b[0].toString());	
			if (b[0] != null && StringUtils.isNotBlank(b[0].toString())) 
				initialBalance = new BigDecimal(b[0].toString());
			
			String lastResetDailyUid = b[1].toString();
			Daily lastResetDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(lastResetDailyUid);
		
			if (lastResetDaily != null)
			{
				// the sql query string
				strSql = "SELECT sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust, sum(rs.trfToRig) as trfToRig, sum(rs.trfToBeach) as trfToBeach  FROM RigStock rs, Daily d,Operation o WHERE (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rs.dailyUid = d.dailyUid and d.operationUid = o.operationUid and rs.supportVesselInformationUid =:supportVesselInformationUid and d.dayDate >= :lastResetDate and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.operationUid = :operationUid";
				String[] paramsFields = {"todayDate", "lastResetDate", "stockCode", "supportVesselInformationUid", "operationUid"};
				Object[] paramsValues = new Object[5]; paramsValues[0] = todayDate; paramsValues[1] = lastResetDaily.getDayDate(); paramsValues[2] = stockcode; paramsValues[3] = supportVesselInformationUid; paramsValues[4] = operationUid;
				
				// load any existing records and sum it up
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				// set the result into temp object 'a'
				Object[] a = (Object[]) lstResult.get(0);
				
				// check for any record(s) or not						
				if (a[0] != null && a[1] != null && a[2] != null && a[3] != null && a[4]!=null) {
					// if previous record(s) exist, set 'previousBalance' 
					//previousBalance = initialBalance + Double.parseDouble(a[0].toString()) - Double.parseDouble(a[1].toString()) + Double.parseDouble(a[2].toString()) - Double.parseDouble(a[3].toString());
					BigDecimal sum = initialBalance.add(new BigDecimal(a[0].toString())).subtract(new BigDecimal(a[1].toString())).add(new BigDecimal(a[2].toString())).subtract(new BigDecimal(a[3].toString())).subtract(new BigDecimal(a[4].toString()));
					previousBalance = Double.valueOf(sum.doubleValue());
				} else {
					//previousBalance = initialBalance;
					previousBalance = Double.valueOf(initialBalance.doubleValue());
				}
				
				//retrun previous balance if got reset occur
				return previousBalance;
			}
		}
		
		//if no lastResetDay
		// the sql query string
		strSql = "select sum(rs.amtStart) as amtstart, sum(rs.amtUsed) as amtused, sum(rs.amtDiscrepancy) as amtadjust, sum(rs.trfToRig) as trfToRig, sum(rs.trfToBeach) as trfToBeach  from RigStock rs, Daily d, Operation o WHERE (o.isDeleted = false or o.isDeleted is null) and (rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rs.dailyUid = d.dailyUid and d.operationUid = o.operationUid and rs.supportVesselInformationUid =:supportVesselInformationUid and d.dayDate < :todayDate and rs.stockCode = :stockCode and rs.operationUid = :operationUid";
		String[] paramsFields = {"todayDate", "stockCode", "supportVesselInformationUid", "operationUid"};
		Object[] paramsValues = new Object[4]; paramsValues[0] = todayDate; paramsValues[1] = stockcode; paramsValues[2] = supportVesselInformationUid; paramsValues[3] = operationUid;
		
		// load any existing records and sum it up
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		// set the result into object 'a'
		Object[] a = (Object[]) lstResult.get(0);				
		
		// check for any record(s) or not
		if (a[0] != null && a[1] != null && a[2] != null && a[3] != null  && a[4] != null) {
			// if previous record(s) exist, set 'previousBalance' 
			//previousBalance = Double.parseDouble(a[0].toString()) - Double.parseDouble(a[1].toString()) + Double.parseDouble(a[2].toString()) - Double.parseDouble(a[3].toString());
			BigDecimal sum = new BigDecimal(a[0].toString()).subtract(new BigDecimal(a[1].toString())).add(new BigDecimal(a[2].toString())).subtract(new BigDecimal(a[3].toString())).subtract(new BigDecimal(a[4].toString()));
			previousBalance = Double.valueOf(sum.doubleValue());
		} 
			
		return previousBalance;
	}
	
	public List<Daily> getActiveDaysForActiveOperationForReportAsAt(Date now) throws Exception {
		List<Daily> days = this.getDaysForReportAsAt(now);
		List<Daily> validDays=new ArrayList<Daily>();
		
		//Map<String,List<Daily>> list = new HashMap<String,List<Daily>>();
		for (Daily day:days)
		{
			Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
			if (operation.getStartDate()!=null && !isDailyRigRelease(day))
			{
				validDays.add(day);
			}
		}
		return validDays;
		
	}
	
	private boolean isLatestDailyRigRelease(Operation operation) throws Exception
	{
		List<Daily> days  = ApplicationUtils.getConfiguredInstance().getDaysInOperation(operation.getOperationUid());
		if (days.size()>0)
		{
			return this.isDailyRigRelease(days.get(days.size()-1));
		}
		return false;
	}
	
	private boolean isDailyRigRelease(Daily daily) throws Exception
	{
		List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT COUNT(*) FROM Activity WHERE (isDeleted = false or isDeleted is null) and dayPlus=0 and dailyUid=:dailyUid and taskCode='RR'", "dailyUid", daily.getDailyUid());
		if (result.size()>0)
		{
			Long count = (Long)result.get(0);
			if (count!=null & count >0)
				return true;
		}
		return false;
	}
	
	public List<Operation> getActiveOperationWithNoCurrentDay(Date now) throws Exception
	{
		return this.getActiveOperationWithNoCurrentDay(now, true);
	}
	
	public List<Operation> getActiveOperationWithNoCurrentDay(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations) throws Exception
	{
		return this.getActiveOperationWithNoCurrentDay(now, useLocalizedCutoffTimeLogicToPickOperations,true);
	}
	public List<Operation> getActiveOperationWithNoCurrentDay(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations, boolean excludeTightHoleOperation) throws Exception {
		List<Operation> results = new ArrayList<Operation>();
		List<Operation> listAllActiveOperation=ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Operation Where (isDeleted = false or isDeleted is null) and startDate is not null");
		
		Date currentDate = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
		Date minDate = DateUtils.addDays(currentDate, -2);
		Date maxDate = DateUtils.addDays(currentDate, 1);
		
		for (Operation op:listAllActiveOperation)
		{
			if (useLocalizedCutoffTimeLogicToPickOperations) {
				
				Boolean included=true;
				if (excludeTightHoleOperation && BooleanUtils.isTrue(op.getTightHole())) continue;
					
				Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(op.getWellboreUid());
				if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {
				
					Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(op.getWellUid());
					if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
						
						List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and dayDate between :minDate and :maxDate", new String[] { "minDate", "maxDate","operationUid" }, new Object[] { minDate, maxDate,op.getOperationUid() });
						if (days.size()>0)
						{
							Date wellCurrentDateTime = DateUtils.addHours(now, well.getTzGmtOffset() == null ? 0 : (int) Math.round(well.getTzGmtOffset()));
							Date wellCurrentDate = DateUtils.truncate(wellCurrentDateTime, Calendar.DAY_OF_MONTH);
							Date reportCutoffDateTime = DateUtils.addHours(wellCurrentDate, (int) Math.round(this.getReportCutoffTime(well.getGroupUid())));
							for( Daily day:days){
								if (reportCutoffDateTime.getTime() > wellCurrentDateTime.getTime()) {
									if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -2))) {
										included=false;
									}
								} else {
									if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -1))) {
										included=false;
									}
								}
							}
						}
					}
				}
				if (included && !results.contains(op) && !this.isLatestDailyRigRelease(op))
					results.add(op);
			}else{
				if (excludeTightHoleOperation && BooleanUtils.isTrue(op.getTightHole())) continue;
				
				Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(op.getWellboreUid());
				if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {
				
					Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(op.getWellUid());
					if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
						List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) AND dayDate = :selectedDate", new String[] { "selectedDate" }, new Object[] { now });
						if (days.size()>0)
							if (!results.contains(op))
								results.add(op);
					}
				}
			}
		}
		return results;
	}
	/**
	 * Returns a list of Daily objects that are valid (take tight hole,
	 * well/wellbore/operation isDeleted flag, GMT offset and report cutoff time
	 * into consideration) for report generation as at the specified date and
	 * time
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Daily> getDaysForReportAsAt(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations, boolean excludeTightHoleOperation, List operationTypes) throws Exception {
		List<Daily> results = new ArrayList<Daily>();
		
		//if is scheduled task we need to use the system wisdom to auto pick the reporting date for each well
		if (useLocalizedCutoffTimeLogicToPickOperations) {
			Date currentDate = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
			Date minDate = DateUtils.addDays(currentDate, -2);
			Date maxDate = DateUtils.addDays(currentDate, 1);
			
			List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) and dayDate between :minDate and :maxDate", new String[] { "minDate", "maxDate" }, new Object[] { minDate, maxDate });
			for (Daily day : days) {
				Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
				if (operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
					if (operationTypes!=null && operationTypes.size()>0) {
						if (!operationTypes.contains(operation.getOperationCode())) continue;
					}
					
					
					if (excludeTightHoleOperation && BooleanUtils.isTrue(operation.getTightHole())) continue;
					
					Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(day.getWellboreUid());
					if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {
					
						Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
						if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
							
							Date wellCurrentDateTime = DateUtils.addHours(now, well.getTzGmtOffset() == null ? 0 : (int) Math.round(well.getTzGmtOffset()));
							Date wellCurrentDate = DateUtils.truncate(wellCurrentDateTime, Calendar.DAY_OF_MONTH);
							Date reportCutoffDateTime = DateUtils.addHours(wellCurrentDate, (int) Math.round(this.getReportCutoffTime(well.getGroupUid())));
							
							if (reportCutoffDateTime.getTime() > wellCurrentDateTime.getTime()) {
								if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -2))) {
									results.add(day);
								}
							} else {
								if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -1))) {
									results.add(day);
								}
							}
						}
					}
				}
			}			
		} else { //else it will be manual fire task from screen, then we should use the date selected by user
			List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) AND dayDate = :selectedDate", new String[] { "selectedDate" }, new Object[] { now });
			
			for (Daily day : days) {
				Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
				if (operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
					
					if (excludeTightHoleOperation && BooleanUtils.isTrue(operation.getTightHole())) continue;
					
					Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(day.getWellboreUid());
					if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {					
						
						Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
						if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {							
							results.add(day);								
						}
					}
				}
			}
			
		}
		return results;
	}
	
	public static boolean checkIfActExistForCurrentDay (String dailyuid) throws Exception {
		List<Activity> activity = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Activity where (isDeleted = false or isDeleted is null) and dailyUid =: dailyUid", new String [] {"dailyUid"}, new Object[] {dailyuid});
		
		if (activity.size() > 0) {	
			return true;
		}
		return false;
		
	}
	
	
	//Custom to Detect if those Activty is not exist in between the recent day
	public List<Daily> getDaysForReportAsAtNoAct(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations, boolean excludeTightHoleOperation, List operationTypes) throws Exception {
		List<Daily> results = new ArrayList<Daily>();
		
		//if is scheduled task we need to use the system wisdom to auto pick the reporting date for each well
		if (useLocalizedCutoffTimeLogicToPickOperations) {
			Date currentDate = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
			Date minDate = DateUtils.addDays(currentDate, -2);
			Date maxDate = DateUtils.addDays(currentDate, 1);
			
			List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) and dayDate between :minDate and :maxDate", new String[] { "minDate", "maxDate" }, new Object[] { minDate, maxDate });
			
			
			if(days != null && days.size() > 0) {
				
				for (Daily day : days) {
					

					Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
					List<Activity> activity = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Activity where (isDeleted = false or isDeleted is null) and dailyUid =: dailyUid", new String [] {"dailyUid"}, new Object[] {day.getDailyUid()});
					
					//Check Activity On Existing Day
					if(checkIfActExistForCurrentDay(day.getDailyUid()) == true)
					{
						if (operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
							if (operationTypes!=null && operationTypes.size()>0) {
								if (!operationTypes.contains(operation.getOperationCode())) continue;
							}
							
							if (excludeTightHoleOperation && BooleanUtils.isTrue(operation.getTightHole())) continue;
							
							Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(day.getWellboreUid());
							if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {
							
								Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
								if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
									
									Date wellCurrentDateTime = DateUtils.addHours(now, well.getTzGmtOffset() == null ? 0 : (int) Math.round(well.getTzGmtOffset()));
									Date wellCurrentDate = DateUtils.truncate(wellCurrentDateTime, Calendar.DAY_OF_MONTH);
									Date reportCutoffDateTime = DateUtils.addHours(wellCurrentDate, (int) Math.round(this.getReportCutoffTime(well.getGroupUid())));
									
									if (reportCutoffDateTime.getTime() > wellCurrentDateTime.getTime()) {
										if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -2))) {
											results.add(day);
										}
									} else {
										if (DateUtils.isSameDay(day.getDayDate(), DateUtils.addDays(wellCurrentDate, -1))) {
											results.add(day);
										}
									}
								}
							}
						}
					}
					
				}
			}
		} else { //else it will be manual fire task from screen, then we should use the date selected by user
			List<Daily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) AND dayDate = :selectedDate", new String[] { "selectedDate" }, new Object[] { now });
			
			for (Daily day : days) {
				Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
				if (operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
					
					if (excludeTightHoleOperation && BooleanUtils.isTrue(operation.getTightHole())) continue;
					
					Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(day.getWellboreUid());
					if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {					
						
						Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
						if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {							
							results.add(day);								
						}
					}
				}
			}
			
		}
		return results;
	}
	
	
	public Date getOperationValidToday(String operationUid, Date now) throws Exception
	{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		return this.getOperationValidToday(operation, now, true);

	}
	
	public Date getOperationValidToday(String operationUid, Date now, boolean useLocalizedCutoffTimeLogicToPickOperations) throws Exception
	{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		return this.getOperationValidToday(operation, now, useLocalizedCutoffTimeLogicToPickOperations);

	}
	
	public Date getOperationValidToday(Operation operation,Date now, boolean useLocalizedCutoffTimeLogicToPickOperations) throws Exception
	{
		if (operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
						
			Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(operation.getWellboreUid());
			if (wellbore != null && (wellbore.getIsDeleted() == null || !wellbore.getIsDeleted())) {
			
				Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(operation.getWellUid());
				if (well != null && (well.getIsDeleted() == null || !well.getIsDeleted())) {
					Date wellCurrentDateTime = DateUtils.addHours(now, well.getTzGmtOffset() == null ? 0 : (int) Math.round(well.getTzGmtOffset()));
					Date wellCurrentDate = DateUtils.truncate(wellCurrentDateTime, Calendar.DAY_OF_MONTH);
					Date reportCutoffDateTime = DateUtils.addHours(wellCurrentDate, (int) Math.round(this.getReportCutoffTime(well.getGroupUid())));
					
					if (reportCutoffDateTime.getTime() > wellCurrentDateTime.getTime()) {
						return DateUtils.addDays(wellCurrentDateTime, -2);
					} else {
						return DateUtils.addDays(wellCurrentDateTime, -1);
					}
					
				}
			}
		}else{
			return now;
		}
		return null;
	}
	/**
	 * Returns a list of Daily objects that are valid (take
	 * well/wellbore/operation isDeleted flag, GMT offset and report cutoff time
	 * into consideration) for report generation as at the specified date and
	 * time
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Daily> getDaysForReportAsAt(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations) throws Exception {
		return getDaysForReportAsAt(now, useLocalizedCutoffTimeLogicToPickOperations, true);
	}
	
	public List<Daily> getDaysForReportAsAtNoAct(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations) throws Exception {
		return getDaysForReportAsAtNoAct(now, useLocalizedCutoffTimeLogicToPickOperations, true);
	}
	
	/**
	 * Returns a list of Daily objects that are valid (take
	 * well/wellbore/operation isDeleted flag, GMT offset and report cutoff time
	 * into consideration) for report generation as at the specified date and
	 * time
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Daily> getDaysForReportAsAt(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations, boolean excludeTightHoleOperation) throws Exception {
		return getDaysForReportAsAt(now, useLocalizedCutoffTimeLogicToPickOperations, excludeTightHoleOperation, null);
	}
	
	public List<Daily> getDaysForReportAsAtNoAct(Date now, boolean useLocalizedCutoffTimeLogicToPickOperations, boolean excludeTightHoleOperation) throws Exception {
		return getDaysForReportAsAtNoAct(now, useLocalizedCutoffTimeLogicToPickOperations, excludeTightHoleOperation, null);
	}
	
	/**
	 * Returns a list of Daily objects that are valid (take
	 * well/wellbore/operation isDeleted flag, GMT offset and report cutoff time
	 * into consideration) for report generation as at the specified date and
	 * time, this is to be called by scheduler
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Daily> getDaysForReportAsAt(Date now) throws Exception {
		return getDaysForReportAsAt(now, true);
	}

	public List<Daily> getDaysForReportAsAtNoAct(Date now) throws Exception {
		return getDaysForReportAsAtNoAct(now, true);
	}
	
	private double getReportCutoffTime(String groupUid) {
		try {
			String reportCutoffTime = GroupWidePreference.getValue(groupUid, "reportCutOffTime");
			if (StringUtils.isNotBlank(reportCutoffTime)) {
				return Double.parseDouble(reportCutoffTime);
			}
		} catch (Exception e) {
		}
		return 0;
	}

	public String getLastDailyUidInOperationByReportType(String operationUid, String reportType) throws Exception {
		String dailyUid = null;
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setFetchFirstRowOnly();
		
		String strSql = "select d.dailyUid from Daily d, ReportDaily rd where (d.isDeleted = false or d.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rd.operationUid = :operationUid and rd.reportType = :reportType and d.dailyUid = rd.dailyUid and d.operationUid = rd.operationUid order by d.dayDate desc";		
		String[] paramsFields = {"operationUid", "reportType"};
		String[] paramsValues = {operationUid, reportType};
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, queryProperties);
		
		if(list.size() > 0)  {
			Object b = (Object) list.get(0);
			dailyUid = (String) b.toString();
		}
		
		return dailyUid;
	}
	
	public String getLastDailyUidInWellboreByReportType(String wellboreUid, String reportType) throws Exception {
		String dailyUid = null;
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setFetchFirstRowOnly();
		
		String strSql = "select d.dailyUid from Daily d, ReportDaily rd where (d.isDeleted = false or d.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rd.wellboreUid = :wellboreUid and rd.reportType = :reportType and d.dailyUid = rd.dailyUid and d.operationUid = rd.operationUid order by d.dayDate desc";		
		String[] paramsFields = {"wellboreUid", "reportType"};
		String[] paramsValues = {wellboreUid, reportType};
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, queryProperties);
		
		if(list.size() > 0)  {
			Object b = (Object) list.get(0);
			dailyUid = (String) b.toString();
		}
		
		return dailyUid;
	}
	
	public String getCompleteOperationName(String groupUid, String operationUid) throws Exception {
		return this.getCompleteOperationName(groupUid, operationUid, null);
	}
	
	/**
	 * Return formatted name for an operation, in a way preferred by the user.
	 * @param groupUid
	 * @param operationUid
	 * @param cache - optional, can be provided when this function is called repetitively in a loop, to improve performance
	 * @return
	 * @throws Exception
	 */
	public String getCompleteOperationName(String groupUid, String operationUid, Map<String,String> cache) throws Exception {
		return this.getCompleteOperationName(groupUid, operationUid, cache, false);
	}
	
	public String getCompleteOperationName(String groupUid, String operationUid, Map<String,String> cache, Boolean useOperationNameDelimiter) throws Exception {
		return this.getCompleteOperationName(groupUid, operationUid, cache, useOperationNameDelimiter, null);
	}
	
	public String getCompleteOperationName(String groupUid, String operationUid, Map<String,String> cache, Boolean useOperationNameDelimiter, String customDelimiter) throws Exception {
		
		String delimiter = MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES;
		
		if(StringUtils.isNotBlank(customDelimiter)) delimiter = customDelimiter;
		
		String cache_key = groupUid + "." + operationUid;
		if(cache != null){
			if(cache.containsKey(cache_key)) return cache.get(cache_key);
		}
		
		String strSql = "select w.wellName, wb.wellboreName, o.operationName, wb.wellboreType, o.operationCode FROM Operation o, Well w, Wellbore wb WHERE (o.isDeleted = false or o.isDeleted is null) AND (w.isDeleted = false or w.isDeleted is null) AND (wb.isDeleted = false or wb.isDeleted is null) AND w.wellUid = wb.wellUid AND wb.wellboreUid = o.wellboreUid AND o.operationUid = :operationUid";		
		String[] paramsFields = {"operationUid"};
		String[] paramsValues = {operationUid};
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		String finalName = "";
		
		if(list.size() > 0)  {
			
			Object[] a = (Object[]) list.get(0);
			
			String opName = "";
			String wellName = "";
			String wbName = "";
			String wellboreType = "";
			String operationCode = "";
			
			if (a[0] != null) wellName = a[0].toString();
			if (a[1] != null) wbName = a[1].toString();			
			if (a[2] != null) opName = a[2].toString();
			if (a[3] != null) wellboreType = a[3].toString();
			if (a[4] != null) operationCode = a[4].toString();
			
			
			//check if GWP skip wellbore name when initial drilling is set on or not
			if ("0".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "ShowWellboreNameForInitialHole"))){
				if (StringUtils.isNotBlank(wellboreType) && "INITIAL".equalsIgnoreCase(wellboreType)){
					wbName = "";					
				}
			}
			
			if ("0".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "ShowOperationNameForDrilling"))){
				if (StringUtils.isNotBlank(operationCode) && operationCode.startsWith(Constant.DRLLG)){
					opName =  "";
				}
			}
			
			finalName = opName.trim();
			if (! opName.equals(wbName)) finalName = wbName.trim() + (useOperationNameDelimiter?delimiter:" ") + finalName;
			if (! wbName.equals(wellName)) finalName = wellName.trim() + (useOperationNameDelimiter?delimiter:" ") + finalName;	
			
		}
		
		if(cache != null){
			cache.put(cache_key, finalName);
		}
		
		return finalName;
	}
	
	/**
	 * This fuction is use to get the newly created report date  base on different report type
	 * @param OperationUid
	 * @param DailyUid
	 * @param reportType
	 * @param userSelection
	 * @return
	 * @throws Exception
	 */
	public Date getTodayReportDate(String OperationUid, String DailyUid, String reportType, UserSelectionSnapshot userSelection) throws Exception{
		
		String strSql;
		
		//check if it is not DDR
		if (!"DDR".equalsIgnoreCase(reportType)){
			//if GWP set to follow DDR date then use the current DDR date as reporting date, if the current DDR date already have the respective report resume to use the report typa last date +1
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "reportDGRUseWithDDR")) && "DGR".equalsIgnoreCase(reportType)){
				if (DailyUid != null){
					//check current report type, are there any report of this kind for this dailyUid
					strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportType=:thisReportType AND dailyUid=:thisDailyUid";
					String[] paramsFields1 = {"thisReportType", "thisDailyUid"};
					Object[] paramsValues1 = {reportType, DailyUid};
					List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
					
					//if the list is empty then the current date should be the selected date
					if (lstResult1.isEmpty()) {
						Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(DailyUid);
						if (selectedDaily.getDayDate() != null) return selectedDaily.getDayDate();						
					}
				}				
			}
		}
		
		
		strSql = "SELECT MAX(reportDatetime) FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportType=:thisReportType AND operationUid=:thisOperationUid GROUP BY operationUid";
		String[] paramsFields = {"thisReportType", "thisOperationUid"};
		Object[] paramsValues = {reportType, OperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Date thisLastDate = null;
		
		if (!lstResult.isEmpty())
		{
			Object b = (Object) lstResult.get(0);
			if (b != null) thisLastDate = (Date) b;
			
		}
		
		if (thisLastDate != null) {
			return DateUtils.addDays(thisLastDate, 1);
			
		} else {
			// TODO this is not time zone sensitive, fix this
			Date newDayDate = CommonUtil.getConfiguredInstance().currentDateTime(userSelection.getGroupUid(), userSelection.getWellUid());
			
			// get from operation's start date for report daily's date if operation is not null or empty.
			if(StringUtils.isNotBlank(userSelection.getOperationUid())) {
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
				if(operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
					if(operation.getStartDate() != null) newDayDate = operation.getStartDate();
				}
			}
			
			//need to reset time to 00:00 so will not effect date in future after add GMT offset
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(newDayDate);
			thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
			
			newDayDate = thisCalendar.getTime();
			
			return newDayDate;
		}
	}
	
	/**
	 * This function is use to get list of daily for a report type and order ASC
	 * @param operationUid, reportType
	 * @return List<ReportDaily> - list hold all the reportDaily object
	 * @throws Exception
	 */
	public List<ReportDaily> getDailyForOperation(String operationUid, String reportType) throws Exception{
		return this.getDailyForOperation(operationUid, reportType, "ASC");
	}
	
	/**
	 * This function is use to get list of daily for a report type and order ASC or DESC according to sortOrder parameter
	 * @param operationUid, reportType
	 * @param sortOrder - ASC or DESC
	 * @return List<ReportDaily> - list hold all the reportDaily object
	 * @throws Exception
	 */
	public List<ReportDaily> getDailyForOperation(String operationUid, String reportType, String sortOrder) throws Exception{
		
		if (reportType==null) reportType = "";

		String strSql = "SELECT rd FROM Daily d, ReportDaily rd WHERE (d.isDeleted = false or d.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) AND rd.operationUid = :operationUid AND rd.reportType in (:reportType) AND d.dailyUid = rd.dailyUid AND d.operationUid = rd.operationUid ORDER BY d.dayDate " + sortOrder;		
		String[] paramsFields = {"operationUid", "reportType"};		
		Object[] paramsValues = {operationUid, Arrays.asList(reportType.split(","))};
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if(list.size() > 0)  {
			return (List<ReportDaily>) list;			
		}
		
		return null;
	}
	
	public Double calculateCummulativeForRigStock(String operationUid, String stockcode, Daily currentDaily, String fieldname, String stockType) throws Exception {
	
		String[] paramsFields = {"todayDate", "operationUid", "stockCode", "stockType"};
		Object[] paramsValues = new Object[4]; 
		paramsValues[0] = currentDaily.getDayDate(); 
		paramsValues[1] = operationUid;
		paramsValues[2] = stockcode;
		paramsValues[3] = stockType;
		
		String strSql = "select sum(rs." + fieldname + ") as cumTotal from RigStock rs, Daily d where (rs.isDeleted = false or rs.isDeleted is null) " +
						" and (d.isDeleted = false or d.isDeleted is null) and rs.stockCode =:stockCode and d.dayDate <= :todayDate and" +
						" rs.operationUid = :operationUid and d.dailyUid = rs.dailyUid and rs.type = :stockType";
		
		if ("rigbulkstock".equalsIgnoreCase(stockType) || StringUtils.isBlank(stockType)) {
			strSql += " and (rs.type='rigbulkstock' OR rs.type=:stockType OR rs.type = '' OR rs.type is NULL) and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid = '')";
		} else {
			strSql += " and rs.type=:stockType and (rs.supportVesselInformationUid is null or rs.supportVesselInformationUid = '')";
		}

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Object a = (Object) lstResult.get(0);
		Double cumTotal = 0.00;
		if (a != null) cumTotal = Double.parseDouble(a.toString());
		
		return cumTotal;
	}
	
	public Double calculateCummulativeRigStockCost(String operationUid, String stockType, Daily currentDaily) throws Exception {
		Double cumCost = null;
		if (currentDaily==null || operationUid==null) return cumCost;
		if (stockType==null) stockType="";
		String queryString = "FROM RigStock r, Daily d " +
				"WHERE (d.isDeleted=false or d.isDeleted is null) " +
				"and (r.isDeleted=false or r.isDeleted is null) " +
				"and d.dayDate <= :todayDate " +
				"and r.operationUid=:operationUid " +
				"and r.dailyUid=d.dailyUid ";
		
		if ("rigbulkstock".equalsIgnoreCase(stockType) || StringUtils.isBlank(stockType)) {
			queryString += " and (r.type='rigbulkstock' OR r.type=:stockType OR r.type = '' OR r.type is NULL) and (r.supportVesselInformationUid is null or r.supportVesselInformationUid = '')";
		} else {
			queryString += " and r.type=:stockType and (r.supportVesselInformationUid is null or r.supportVesselInformationUid = '')";
		}
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
				new String[] {"todayDate","operationUid","stockType"},
				new Object[] {currentDaily.getDayDate(), operationUid, stockType}); 
		for (Object[] rec : lstResult) {
			RigStock stock = (RigStock) rec[0];
			if (stock.getItemcost()!=null && stock.getAmtUsed()!=null) {
				Double itemTotal = stock.getItemcost() * stock.getAmtUsed();
				if (stock.getDiscountPct()!=null) {
					itemTotal = itemTotal * (1-(stock.getDiscountPct()/100)); 
				}
				if (cumCost==null) cumCost = 0.0;
				cumCost += itemTotal; 
			}
		}
		
		return cumCost;
	}
	
	public Double calculateDailyRigStockCost(String operationUid, String stockType, Daily currentDaily) throws Exception {
		Double dailyTotal = null;
		if (currentDaily==null || operationUid==null) return dailyTotal;
		if (stockType==null) stockType="";
		String queryString = "FROM RigStock WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid and type=:stockType and (supportVesselInformationUid is null or supportVesselInformationUid = '')";
				
		List<RigStock> allStocks = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
				new String[] {"dailyUid","stockType"},
				new Object[] {currentDaily.getDailyUid(), stockType});
		for (RigStock stock : allStocks) {
			if (stock.getItemcost()!=null && stock.getAmtUsed()!=null) {
				Double itemTotal = stock.getItemcost() * stock.getAmtUsed();
				if (stock.getDiscountPct()!=null) {
					itemTotal = itemTotal * (1-(stock.getDiscountPct()/100)); 
				}
				if (dailyTotal==null) dailyTotal = 0.0;
				dailyTotal += itemTotal; 
			}
		}
		
		return dailyTotal;
	}
	
	public boolean isBhaRunNumberDuplicate(String name, String operationUid, boolean isNewRecord, String bharunUid) throws Exception {
		if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(operationUid)) {

			if (isNewRecord) {
				String[] paramNames = {"operationUid", "bharunNumber"};
				Object[] paramValues = {operationUid, name};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and bhaRunNumber = :bharunNumber", paramNames, paramValues);
				
				if(lstResult.size() > 0)  {
					return true;			
				}else {
					return false;
				}
			}else if (!isNewRecord && StringUtils.isNotBlank(bharunUid)) {
				String[] paramNames = {"operationUid", "bharunNumber", "bharunUid"};
				Object[] paramValues = {operationUid, name, bharunUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and bhaRunNumber = :bharunNumber and bharunUid != :bharunUid", paramNames, paramValues);
				
				if(lstResult.size() > 0)  {
					return true;			
				}else {
					return false;
				}
			}
			
		}
		return false;
	}
	
	public boolean isBitrunNumberDuplicate(String name, String operationUid, boolean isNewRecord, String bitrunUid) throws Exception {
		if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(operationUid)) {

			if (isNewRecord) {
				String[] paramNames = {"operationUid", "bitrunNumber"};
				Object[] paramValues = {operationUid, name};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bitrun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and bitrunNumber = :bitrunNumber", paramNames, paramValues);
				
				if(lstResult.size() > 0)  {
					return true;			
				}else {
					return false;
				}
			}else if (!isNewRecord && StringUtils.isNotBlank(bitrunUid)) {
				String[] paramNames = {"operationUid", "bitrunNumber", "bitrunUid"};
				Object[] paramValues = {operationUid, name, bitrunUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bitrun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and bitrunNumber = :bitrunNumber and bitrunUid !=:bitrunUid", paramNames, paramValues);
				
				if(lstResult.size() > 0)  {
					return true;			
				}else {
					return false;
				}
			}
			
		}
		return false;
	}
	
	public Daily createNewDaily (Date DayDate, String OperationUid, UserSession session, HttpServletRequest request) throws Exception  
	{
		String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate =:thisDayDate AND operationUid=:thisOperationUid";
		String[] paramsFields = {"thisDayDate", "thisOperationUid"};
		Object[] paramsValues = {DayDate, OperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			return (Daily) lstResult.get(0);
		}else{
			Daily thisDay = new Daily();
			thisDay.setDayNumber("-");
			thisDay.setDayDate(DayDate);
			thisDay.setDepthMdMsl(0.00);
			thisDay.setDepthTvdMsl(0.00);
			thisDay.setOperationUid(OperationUid);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDay);
			//D2ApplicationEvent.refresh();
			
			//update the last and start operation date in operation table when there is changed on daily table
			this.setStartAndLastOperationDate(OperationUid);
			return thisDay;
		}
	}
	
	public String loadFileManagerFileListForOIC(String operationUid, Integer maxFilesNumberToShow) throws Exception {
		Integer intCounter = 0;
		String filemanagerlist = "";
		
		List<FileManagerFiles> list = FileManagerUtils.getFilesForOIC(operationUid);
	
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		for(FileManagerFiles filemanagerfiles : list){
			
			String filePath = ApplicationConfig.getConfiguredInstance().getFileManagerRootPath() + "/";
			File file = new File(filePath + filemanagerfiles.getFileFullPathName() + "/" + filemanagerfiles.getFileManagerFilesUid());
			
			if(file.exists()) {
			
				String fileUid= URLEncoder.encode(this.nullToEmptyString(filemanagerfiles.getFileManagerFilesUid()), "utf-8");
				String fileName = URLEncoder.encode(this.nullToEmptyString(filemanagerfiles.getFileName()), "utf-8");
				
				String uploadedDate = "";
				if (filemanagerfiles.getUploadedDatetime() != null) {
					uploadedDate = URLEncoder.encode(df.format(filemanagerfiles.getUploadedDatetime()), "utf-8");
				}
				if (StringUtils.isBlank(filemanagerlist)) {
					filemanagerlist = "fileuid=" + fileUid + "&filename=" + fileName + "&date=" + uploadedDate;
				}else {
					filemanagerlist = filemanagerlist + ",fileuid=" + fileUid + "&filename=" + fileName + "&date=" + uploadedDate;
				}
				intCounter++;
				
				if (maxFilesNumberToShow !=null) {
					if (intCounter > maxFilesNumberToShow) break;
				}
			}
		}
		return filemanagerlist;
	}
	

	public String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	
	public Double calculateDaysSinceHseIncident(Date dayDate, Date hseIncidentEventDatetime, String groupUid) throws Exception {
		
		double timeLapsed = 0;
		
		if ("0".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "calculateHseDayLapsedIncludeDayOfIncident"))){
			timeLapsed = 0;
		}else {
			timeLapsed = 86399999; // count the time lapse as at 23:59 of the day
		}
		
		double usecLapsed = dayDate.getTime() + timeLapsed - hseIncidentEventDatetime.getTime(); 
		double daysLapsedInBase = usecLapsed / 1000; // important, usecLapsed must be of type double
		
		if (daysLapsedInBase < 0) {
			return 0.0;
		}
		
		return daysLapsedInBase;
	}
	
	/**
	 * This function is use to calculate the days since last BOP test from the report daily screen 
	 * @param dayDate, lastBopEventDatetime
	 * @return daysLapsedInBase
	 * @throws Exception
	 */
	public Double calculateDaysSinceLastBOP(Date dayDate, Date lastBopEventDatetime) throws Exception {
		
		double usecLapsed = dayDate.getTime() - lastBopEventDatetime.getTime(); 
		double daysLapsedInBase = usecLapsed / 1000; // important, usecLapsed must be of type double
		
		if (daysLapsedInBase < 0) {
			return 0.0;
		}
		
		return daysLapsedInBase;
	}
	
	
	public Double calculateActualDuration(String operationUid, Date startDate) throws Exception {
		double actualDurationDays = 0.0;
		Date latestDate = null;
		String strSql = "SELECT reportDatetime FROM ReportDaily where operationUid=:operationUid AND reportType!='DGR' AND (isDeleted=false or isDeleted is null) ORDER BY reportDatetime DESC";
		String[] paramsFields = {"operationUid"};
		Object[] paramsValues = {operationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (!lstResult.isEmpty()) {
			latestDate = (Date) lstResult.get(0);
			
			double actualDuration = latestDate.getTime() - startDate.getTime(); 
			actualDurationDays = actualDuration / 1000; // important, actualDuration must be of type double
			
			if (actualDurationDays < 0) {
				return 86400 - actualDurationDays *-1;
			}
			
			actualDurationDays = actualDurationDays + 86400;
			
			return actualDurationDays;
		} else {
			return actualDurationDays;
		}
		
	}
		
	public String getActiveDvdPlanUid(String operationUid) throws Exception {
		Date lastEditDate = null;
		String currentDvdPlanUid = null;
		
		String queryString = "FROM OperationPlanMaster " +
				"where (isDeleted=false or isDeleted is null) " +
				"and operationUid=:operationUid " +
				"and (dvdPlanStatus=true and dvdPlanStatus is not null)"; //check active
		List<OperationPlanMaster> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
		if (rs.size()==1) {
			//only one active plan
			OperationPlanMaster rec = rs.get(0);
			currentDvdPlanUid = rec.getOperationPlanMasterUid();
		} else if (rs.size()>1) {
			//move than 1 active plan
			for (OperationPlanMaster rec: rs) {
				if (lastEditDate==null) lastEditDate = rec.getLastEditDatetime();
				if (currentDvdPlanUid==null) currentDvdPlanUid = rec.getOperationPlanMasterUid();
				if (rec.getLastEditDatetime().compareTo(lastEditDate)>0) {
					lastEditDate = rec.getLastEditDatetime();
					currentDvdPlanUid = rec.getOperationPlanMasterUid();
				}
				
				//check detail
				Date detailDate = this.operationPlanPhaseGreaterDate(rec.getOperationPlanMasterUid(), lastEditDate);
				if (detailDate.compareTo(lastEditDate) > 0) {
					lastEditDate = detailDate;
					currentDvdPlanUid = rec.getOperationPlanMasterUid();
				}
			}
		} else {
			//inactive plans
			queryString = "FROM OperationPlanMaster " +
					"where (isDeleted=false or isDeleted is null) " +
					"and operationUid=:operationUid " +
					"and (dvdPlanStatus=false or dvdPlanStatus is null)";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
			if (rs.size()==1) {
				//only one inactive plan
				OperationPlanMaster rec = rs.get(0);
				currentDvdPlanUid = rec.getOperationPlanMasterUid();
			} else if (rs.size()>1) {
				//move than 1 inactive plan
				for (OperationPlanMaster rec: rs) {
					if (lastEditDate==null) lastEditDate = rec.getLastEditDatetime();
					if (currentDvdPlanUid==null) currentDvdPlanUid = rec.getOperationPlanMasterUid();
					if (rec.getLastEditDatetime().compareTo(lastEditDate)>0) {
						lastEditDate = rec.getLastEditDatetime();
						currentDvdPlanUid = rec.getOperationPlanMasterUid();
					}
					
					//check detail
					Date detailDate = this.operationPlanPhaseGreaterDate(rec.getOperationPlanMasterUid(), lastEditDate);
					if (detailDate.compareTo(lastEditDate) > 0) {
						lastEditDate = detailDate;
						currentDvdPlanUid = rec.getOperationPlanMasterUid();
					}
				}
			}
		}
		
		
		return currentDvdPlanUid;
	}
	
	private Date operationPlanPhaseGreaterDate(String operationPlanMasterUid, Date currentDate) throws Exception {
		String queryString = "FROM OperationPlanPhase where (isDeleted=false or isDeleted is null) and operationPlanMasterUid=:operationPlanMasterUid order by lastEditDatetime DESC";
		List<OperationPlanPhase> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationPlanMasterUid", operationPlanMasterUid);
		for (OperationPlanPhase rec : rs) {
			if (rec.getLastEditDatetime().compareTo(currentDate) > 0) {
				currentDate = rec.getLastEditDatetime(); 
			}
			
			queryString = "FROM OperationPlanTask where (isDeleted=false or isDeleted is null) and operationPlanPhaseUid=:operationPlanPhaseUid order by lastEditDatetime DESC";
			List<OperationPlanTask> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationPlanPhaseUid", rec.getOperationPlanPhaseUid());
			if (rs2.size()>0) {
				OperationPlanTask rec2 = rs2.get(0);
				if (rec2.getLastEditDatetime().compareTo(currentDate) > 0) {
					currentDate = rec2.getLastEditDatetime(); 
				}
			}
		}
		
		return currentDate;
	}
	
	public Double calculateDaysSinceLastRecordableIncident(Date dayDate, Date lastRecordableIncidentDatetime, String groupUid) throws Exception {
		double timeLapsed = 0;
		
		if ("0".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "calculateHseDayLapsedIncludeDayOfIncident"))){
			timeLapsed = 0;
		}else {
			timeLapsed = 86399999; // count the time lapse as at 23:59 of the day
		}
		
		double usecLapsed = dayDate.getTime() + timeLapsed - lastRecordableIncidentDatetime.getTime(); 
		double daysLapsedInBase = usecLapsed / 1000; // important, usecLapsed must be of type double
		
		if (daysLapsedInBase < 0) {
			return 0.0;
		}
		
		return daysLapsedInBase;
	}
	
	public Double calculateDaysToBopTest(Date dayDate, Date nextBopDatetime, String groupUid) throws Exception {
		
		double usecLapsed = nextBopDatetime.getTime() - dayDate.getTime();
		double daysLapsedInBase = usecLapsed / 1000; // important, usecLapsed must be of type double

		if (daysLapsedInBase < 0) {
			return 0.0;
		}
		
		return daysLapsedInBase;
	}
	
	public String getReportTypePriorityWhenDrllgNotExist(String operationUid)throws Exception {
		if (StringUtils.isNotBlank(operationUid)) {
			Operation currentOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
			
			if (currentOperation != null) {
				Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
				String operationType = currentOperation.getOperationCode();
				
				String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationType);
				if (strReportTypes != null) {
					String[] reportTypes = strReportTypes.split("[,]");
					for (String reportType : reportTypes) {
						String[] paramNames = {"reportType", "operationUid"};
						String[] paramValues = {reportType, operationUid};
						List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select count(*) from ReportDaily where (isDeleted = false or isDeleted is null) and reportType = :reportType and operationUid = :operationUid", paramNames, paramValues);
						if(list.size() > 0){
							if(((Long) list.get(0)) > 0){
								return reportType;
							}
						}
					}
				}
			}
				
			return "DDR"; // return ddr as default
		}
		
		return null;
	}
	
	public Double calculateAfeTotal(String costAfeMasterUid, String operationUid) throws Exception {
		Double afeTotal = null;
		String uid = null;
		Map<String, Double> conversionRates = new HashMap<String, Double>();
		
		CostAfeMaster costAfeMaster = (CostAfeMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CostAfeMaster.class, costAfeMasterUid);
		if(costAfeMaster != null && StringUtils.isNotBlank(costAfeMaster.getParentCostAfeMasterUid())) {
			uid = costAfeMaster.getParentCostAfeMasterUid();
		}else {
			uid = costAfeMasterUid;
		}
		
		String strSql = "FROM CostCurrencyLookup where (isDeleted = false or isDeleted is null) " +
				"AND costAfeMasterUid IN (" +
				"SELECT c.costAfeMasterUid from CostAfeMaster c, OperationAfe a " +
				"where (c.isDeleted = false or c.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and c.costAfeMasterUid=a.costAfeMasterUid " +
				"and a.operationUid=:operationUid )";
		String[] paramsFields = {"operationUid"};
		Object[] paramsValues = {operationUid};
		List<CostCurrencyLookup> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
//		List<CostCurrencyLookup> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//				"from CostCurrencyLookup where (isDeleted = false or isDeleted is null) " +
//				"and costAfeMasterUid=:costAfeMasterUid " +
//				"order by isPrimaryCurrency desc", 
//				"costAfeMasterUid", uid);
		for (CostCurrencyLookup costCurrencyLookup : rs ) {
			if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
				if (costCurrencyLookup.getConversionRate()>0.0) {
					conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
				}
			}
		}
		
		List<CostAfeDetail> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
				"FROM CostAfeDetail where (isDeleted=false or isDeleted is null) and costAfeMasterUid=:costAfeMasterUid", 
				"costAfeMasterUid", costAfeMasterUid);
		for (CostAfeDetail rec : result) {
			Double thisTotal = 0.0;
			if (rec.getItemCost()!=null && rec.getQuantity()!=null && rec.getEstimatedDays()!=null) {
				thisTotal = rec.getItemCost() * rec.getQuantity() * rec.getEstimatedDays();
				if (rec.getCurrency()!=null) {
					if (conversionRates.containsKey(rec.getCurrency())) {
						Double conversionRate = conversionRates.get(rec.getCurrency());
						thisTotal = Math.round(thisTotal / conversionRate * 100.0) / 100.0;
					}
				}
				if (afeTotal==null) afeTotal = 0.0;
				afeTotal += thisTotal;
			}
		}

		return afeTotal;
	}

	public Double calculateDailyCostFromCostNet(String dailyUid) throws Exception {
		Double dayCost = null;
		if (StringUtils.isBlank(dailyUid)) return null;
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			
			Map<String, Double> conversionRates = new HashMap<String, Double>();
			
			List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup c, OperationAfe a " +
					"where (c.isDeleted = false or c.isDeleted is null) " +
					"and (a.isDeleted = false or a.isDeleted is null) " +
					"and c.costAfeMasterUid=a.costAfeMasterUid " +
					"and a.operationUid=:operationUid " +
					"order by c.isPrimaryCurrency desc", "operationUid", daily.getOperationUid());
			for (Object[] rec : rs ) {
				CostCurrencyLookup costCurrencyLookup = (CostCurrencyLookup) rec[0];
				if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
					if (costCurrencyLookup.getConversionRate()>0.0) {
						conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
					}
				}
			}
			
			//Collect all available daycost in CostDailysheet
			//String queryString = "SELECT currency, sum((coalesce(itemCost,0.0) * coalesce(quantity,0.0)) + (coalesce(gstCurrency,0.0) + coalesce(pstCurrency,0.0))) FROM CostDailysheet " +
			String queryString = "SELECT currency, sum((coalesce(itemCost,0.0) * coalesce(quantity,0.0))) FROM CostDailysheet " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"AND dailyUid=:dailyUid " +
					"Group By currency ";
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", dailyUid);
			for (Object result : list) {
				Object[] rec = (Object[]) result;
				if (rec[1]!=null) {
					//calculate conversion rate
					Double thisTotal = Double.parseDouble(rec[1].toString());
					if (rec[0]!=null) {
						if (conversionRates.containsKey(rec[0].toString())) {
							Double conversionRate = conversionRates.get(rec[0].toString());
							thisTotal = Math.round(thisTotal / conversionRate * 100.0) / 100.0;
						}
					}
					if (dayCost==null) dayCost = 0.0;
					dayCost += thisTotal;
				}
			}
		
		}
		
		return dayCost;
	}
	
	public void updateDailyIsDeleted(String operationUid) throws Exception {  
		//clearDeletedDailyFromTown Ticket: 38409
		String strSql;
		strSql = "UPDATE Daily set isDeleted = true, sysDeleted = 4 WHERE dailyUid NOT IN (SELECT dailyUid FROM ReportDaily WHERE operationUid=:operationUid) AND operationUid=:operationUid";
		String[] paramsField = {"operationUid"};
		Object[] paramsValue = {operationUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsField, paramsValue);
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		this.setStartAndLastOperationDate(operationUid);
	}
	
	public void updateDailyCostFromCostNet(String dailyUid) throws Exception {  
		Double daycostFromCostNet = this.calculateDailyCostFromCostNet(dailyUid);
		
		if (daycostFromCostNet!=null){
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and dailyUid=:dailyUid";
			String[] paramNames = {"dailyUid"};
			Object[] paramValues = {dailyUid};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
	
			for (ReportDaily rdRec : reportDailyList) {
				rdRec.setDaycost(daycostFromCostNet);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rdRec);
				this.updateWitsmlChangeLog(rdRec);
			}
		}
	}
	
	private void updateWitsmlChangeLog(ReportDaily reportDaily) throws Exception {
		WitsmlChangeLog changeLog = new WitsmlChangeLog();
		changeLog.setObjectType("opsReport.costDailysheet");
		changeLog.setWellUid(reportDaily.getWellUid());
		changeLog.setWellboreUid(reportDaily.getWellboreUid());
		changeLog.setOperationUid(reportDaily.getOperationUid());
		changeLog.setTransactionType("update");
		changeLog.setTransactionDatetime(new Date());
		changeLog.setRecordPrimaryKey(reportDaily.getDailyUid());
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(changeLog);
	}
	
	//create duplicated function and remove the null checking condition to update daycost, avoid update daycost to null when stt and rft callback  
	public void updateDailyCostFromCostNetWithoutSTTCallback(String dailyUid) throws Exception {  
		Double daycostFromCostNet = this.calculateDailyCostFromCostNet(dailyUid);
		
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and dailyUid=:dailyUid";
			String[] paramNames = {"dailyUid"};
			Object[] paramValues = {dailyUid};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
	
			for (ReportDaily rdRec : reportDailyList) {
				rdRec.setDaycost(daycostFromCostNet);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rdRec);
			}
	}
	
	public Double calculateOperationCost(String operationUid) throws Exception {
		Double cumcost = null;
		
		String queryString = "FROM Daily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid order by dayDate desc";
		List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
		if (dailyList.size()>0) {
			cumcost = this.calculateCummulativeCost(dailyList.get(0).getDailyUid(), null);
		}
		
		return cumcost;
	}
	
	public Double calculateCummulativeCost(String dailyUid, String reportType) throws Exception { 
		Double cumcost = null;
		if (StringUtils.isBlank(dailyUid)) return null;
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			String operationUid = daily.getOperationUid();
			List OpsList = new ArrayList();
			OpsList = this.getAssociatedOpsList(operationUid);
			Date todayDate = null;
			if (daily!=null) todayDate = daily.getDayDate();
			
			for (Object ops: OpsList) {
				String opsUid = ops.toString();
				String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and dayDate<=:dayDate and operationUid=:operationUid order by dayDate desc";
				List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, 
						new String[] {"dayDate","operationUid"}, new Object[] {todayDate, opsUid});
				if (dailyList.size()>0) {
					String getDailyUid = dailyList.get(0).getDailyUid();
					Double opsTotal = CommonUtil.getConfiguredInstance().calculateCummulativeCostOps(getDailyUid, null);
					if (cumcost==null) cumcost = 0.0;
					if (opsTotal==null) opsTotal = 0.0;
					cumcost += opsTotal;
				}
			}
		}
		
		if (cumcost==null) cumcost = 0.0;
		return cumcost;
	}
	
	public List getAssociatedOpsList(String operationUid) throws Exception { 
		List opsList = new ArrayList();
		opsList.add(operationUid);
		Well well=ApplicationUtils.getConfiguredInstance().getCachedWellByOperation(operationUid);
		String wellUid=well.getWellUid();
		String StrSQL = "SELECT DISTINCT(b.operationUid) FROM CostAfeMaster a, OperationAfe b "
				+ "WHERE a.costAfeMasterUid = b.costAfeMasterUid "
				+ "AND (a.isDeleted=false or a.isDeleted is null) "
				+ "AND (b.isDeleted=false or b.isDeleted is null) "
				+ "AND b.wellUid=:wellUid "
				+ "AND b.costAfeMasterUid IN "
				+ "(SELECT costAfeMasterUid FROM OperationAfe WHERE (isDeleted=false or isDeleted is null) "
				+ "AND operationUid =:operationUid AND afe_type != 'PRICELIST' AND afe_type !='SO' ) ";
		String[] paramField ={"operationUid", "wellUid"};
		Object[] paramValue ={operationUid, wellUid};		
		List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(StrSQL, paramField, paramValue);
		for(String opsUid: rs){
			if (!opsList.contains(opsUid)) opsList.add(opsUid);
		}
		
		return opsList;
	}
	
	public Double calculateCummulativeCostOps(String dailyUid, String reportType) throws Exception {
		Double cumcost = null;
		if (StringUtils.isBlank(dailyUid)) return null;
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			if (daily.getDayDate()!=null) {
				
				List dailyList = new ArrayList();
				
				String queryString = "SELECT dailyUid FROM Daily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND operationUid=:operationUid " +
						"AND dayDate<=:dayDate ";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"operationUid", "dayDate"}, 
						new Object[] {daily.getOperationUid(), daily.getDayDate()});
				
				for (Object rec : list) {
					dailyList.add(rec.toString());
				}
				
				//Double amountSpentPriorToSpud  = null;
				Map<String, Double> reportDailyDayCosts = new HashMap<String, Double>();
				Map<String, Double> dailyCostDayCosts = new HashMap<String, Double>();
				Map<String, Double> conversionRates = new HashMap<String, Double>();
				
				// Amount Spent Prior to Spud 
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
				if (operation!=null) {
					if (operation.getAmountSpentPriorToSpud()!=null) cumcost = operation.getAmountSpentPriorToSpud();
					if (operation.getAmountSpentPriorToOperation()!=null) cumcost = operation.getAmountSpentPriorToOperation();
				}
				//if (amountSpentPriorToSpud!=null) cumcost = amountSpentPriorToSpud;
				
				//Collect all available daycost in ReportDaily
				if (reportType==null) reportType = this.getReportTypePriorityWhenDrllgNotExist(daily.getOperationUid());
				queryString = "SELECT dailyUid, sum(daycost) FROM ReportDaily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND dailyUid in (:dailyUid) " +
						"AND reportType=:reportType " +
						"AND daycost is not null " +
						"Group By dailyUid";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"dailyUid", "reportType"}, 
						new Object[] {dailyList, reportType});
				for (Object result : list) {
					Object[] rec = (Object[]) result;
					if (rec[0]!=null && rec[1]!=null) {
						reportDailyDayCosts.put(rec[0].toString(), Double.parseDouble(rec[1].toString()));
					}
					/*if (rec[2]!=null) {
						if (cumcost==null) cumcost = 0.0;
						cumcost += Double.parseDouble(rec[2].toString());
					}*/
				}
				
				//adjust cost
				queryString = "SELECT dailyUid, sum(costadjust) FROM ReportDaily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND dailyUid in (:dailyUid) " +
						"AND reportType=:reportType " +
						"AND costadjust is not null " +
						"Group By dailyUid";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"dailyUid", "reportType"}, 
						new Object[] {dailyList, reportType});
				for (Object result : list) {
					Object[] rec = (Object[]) result;
					if (rec[1]!=null) {
						if (cumcost==null) cumcost = 0.0;
						cumcost += Double.parseDouble(rec[1].toString());
					}
				}

				List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup c, OperationAfe a " +
						"where (c.isDeleted = false or c.isDeleted is null) " +
						"and (a.isDeleted = false or a.isDeleted is null) " +
						"and c.costAfeMasterUid=a.costAfeMasterUid " +
						"and a.operationUid=:operationUid " +
						"order by c.isPrimaryCurrency desc", "operationUid", daily.getOperationUid());
				for (Object[] rec : rs ) {
					CostCurrencyLookup costCurrencyLookup = (CostCurrencyLookup) rec[0];
					if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
						if (costCurrencyLookup.getConversionRate()>0.0) {
							conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
						}
					}
				}
				
				//Collect all available daycost in CostDailysheet
				//queryString = "SELECT dailyUid, currency, sum((coalesce(itemCost,0.0) * coalesce(quantity,0.0)) + (coalesce(gstCurrency,0.0) + coalesce(pstCurrency,0.0))) FROM CostDailysheet " +
				queryString = "SELECT dailyUid, currency, sum((coalesce(itemCost,0.0) * coalesce(quantity,0.0))) FROM CostDailysheet " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND dailyUid in (:dailyUid) " +
						"Group By dailyUid, currency ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", dailyList);
				for (Object result : list) {
					Object[] rec = (Object[]) result;
					if (rec[0]!=null && rec[2]!=null) {
						//calculate conversion rate
						Double thisTotal = Double.parseDouble(rec[2].toString());
						if (rec[1]!=null) {
							if (conversionRates.containsKey(rec[1].toString())) {
								Double conversionRate = conversionRates.get(rec[1].toString());
								thisTotal = Math.round(thisTotal / conversionRate * 100.0) / 100.0;
							}
						}
						
						if (dailyCostDayCosts.containsKey(rec[0].toString())) {
							thisTotal += dailyCostDayCosts.get(rec[0].toString());
						}
						dailyCostDayCosts.put(rec[0].toString(), thisTotal);
					}
				}
				
				for (Object rec : dailyList) {
					String thisDailyUid = rec.toString();
					if (dailyCostDayCosts.containsKey(thisDailyUid)) {
						if (cumcost==null) cumcost = 0.0;
						cumcost += dailyCostDayCosts.get(thisDailyUid);
					} else {
						if (reportDailyDayCosts.containsKey(thisDailyUid)) {
							if (cumcost==null) cumcost = 0.0;
							cumcost += reportDailyDayCosts.get(thisDailyUid);
						}
					}
				}
				
			}
		}
		
		return cumcost;
	}
	
	private Double calculateNormalAverageValue(String bharunUid, String dailyUid, String fieldname) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"dailyUid", "bharunUid"};
		Object[] paramsValues = new Object[2]; 
		paramsValues[0] = dailyUid;
		paramsValues[1] = bharunUid;
		
		String strSql = "SELECT AVG(" + fieldname + ") as avgValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
						" AND dailyUid = :dailyUid AND bharunUid =:bharunUid AND (excludeFromNewHole = false or excludeFromNewHole is null) GROUP BY bharunUid";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		Double avgValue = null;
		if (a != null) avgValue = Double.parseDouble(a.toString());
		
		return avgValue;
	}
	
	private Double getTotalValueFromDrillParam(String bharunUid, String dailyUid, String fieldname) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"dailyUid", "bharunUid"};
		Object[] paramsValues = new Object[2]; 
		paramsValues[0] = dailyUid;
		paramsValues[1] = bharunUid;
		
		String strSql = "SELECT SUM(" + fieldname + ") as sumValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
						" AND dailyUid = :dailyUid AND bharunUid =:bharunUid AND (excludeFromNewHole = false or excludeFromNewHole is null) GROUP BY bharunUid";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		Double sumValue = null;
		if (a != null) sumValue = Double.parseDouble(a.toString());
		
		return sumValue;
	}
	
	private Double getMinFromDrillParam(String bharunUid, String dailyUid, String fieldname) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"dailyUid", "bharunUid"};
		Object[] paramsValues = new Object[2]; 
		paramsValues[0] = dailyUid;
		paramsValues[1] = bharunUid;
		
		String strSql = "SELECT MIN(" + fieldname + ") as minValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
						" AND dailyUid = :dailyUid AND bharunUid =:bharunUid AND (excludeFromNewHole = false or excludeFromNewHole is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		Double minValue = null;
		if (a != null) minValue = Double.parseDouble(a.toString());
		
		return minValue;
	}
	
	private Double getMaxFromDrillParam(String bharunUid, String dailyUid, String fieldname) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"dailyUid", "bharunUid"};
		Object[] paramsValues = new Object[2]; 
		paramsValues[0] = dailyUid;
		paramsValues[1] = bharunUid;
		
		String strSql = "SELECT MAX(" + fieldname + ") as maxValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
						" AND dailyUid = :dailyUid AND bharunUid =:bharunUid AND (excludeFromNewHole = false or excludeFromNewHole is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		Double maxValue = null;
		if (a != null) maxValue = Double.parseDouble(a.toString());
		
		return maxValue;
	}
	
	public void copyValueFromDrilParam (String DailyUid) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get bharunUid from bharunDailySummary
		String strSql1 = "SELECT bds.bharunUid FROM BharunDailySummary bds, Bharun b WHERE b.bharunUid = bds.bharunUid AND (bds.isDeleted = false or bds.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) AND bds.dailyUid = :dailyUid";
		List BDSlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "dailyUid", DailyUid);
		
		if (BDSlist.size() > 0) {
			for(Object objResult: BDSlist){
				
				String bharunUid = objResult.toString();
				Double totalProgress = null;
				Double totalDuration = null;
				Double totalIADCDuration = null;
				Double totalKrevs = null;
				Double totalSlideHours = null;
				Double totalDurationRotated = null;
				Double totalDurationCirculated = null;
				Double totalAvgPercentSlide = null;
				Double totalAvgPercentRotated = null;
				Double totalAvgPercentCirculated = null;
				Double totalAvgHsi = null;
				
				Double wobMinForce = null;
				Double surfaceRpmMin = null;
				Double flowRateMin = null;
				Double standpipePressureMin = null;
				Double stringWeightRotaryMinForce = null;
				Double pickupWeightMinForce = null;
				Double slackOffWeightMinForce = null;
				
				Double wobMaxForce = null;
				Double surfaceRpmMax = null;
				Double flowRateMax = null;
				Double standpipePressureMax = null;
				Double stringWeightRotaryMaxForce = null;
				Double pickupWeightMaxForce = null;
				Double slackOffWeightMaxForce = null;
								
				String[] paramsFields = {"dailyUid", "bharunUid"};
				Object[] paramsValues = new Object[2]; 
				paramsValues[0] = DailyUid;
				paramsValues[1] = bharunUid;
				
				String strSql = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
								" AND dailyUid = :dailyUid AND bharunUid =:bharunUid AND (excludeFromNewHole = false or excludeFromNewHole is null)";

				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				if(lstResult.size() > 0) {
					//Get TOTAL value
					totalProgress = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "progress");
					totalDuration = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "duration");
					totalIADCDuration = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "iadcDuration");
					totalKrevs = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "krevs");
					totalSlideHours = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "slideHours");
					totalDurationRotated = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "durationRotated");
					totalDurationCirculated = this.getTotalValueFromDrillParam(bharunUid, DailyUid, "durationCirculated");
					
					//Get AVG value
					totalAvgPercentSlide = this.calculateNormalAverageValue(bharunUid, DailyUid, "percentSlide");
					totalAvgPercentRotated = this.calculateNormalAverageValue(bharunUid, DailyUid, "percentRotated");
					totalAvgPercentCirculated = this.calculateNormalAverageValue(bharunUid, DailyUid, "percentCirculated");
					totalAvgHsi = this.calculateNormalAverageValue(bharunUid, DailyUid, "hsi");
					
					//Get MIN value 
					wobMinForce = this.getMinFromDrillParam(bharunUid, DailyUid, "wobAvgForce");
					surfaceRpmMin = this.getMinFromDrillParam(bharunUid, DailyUid, "surfaceRpmAvg");
					flowRateMin = this.getMinFromDrillParam(bharunUid, DailyUid, "flowAvg");
					standpipePressureMin = this.getMinFromDrillParam(bharunUid, DailyUid, "standpipePressure");
					stringWeightRotaryMinForce = this.getMinFromDrillParam(bharunUid, DailyUid, "stringWeightRotaryAvgForce");
					pickupWeightMinForce = this.getMinFromDrillParam(bharunUid, DailyUid, "pickupWeightAvgForce");
					slackOffWeightMinForce = this.getMinFromDrillParam(bharunUid, DailyUid, "slackOffWeightAvgForce");
					
					//Get MAX value
					wobMaxForce = this.getMaxFromDrillParam(bharunUid, DailyUid, "wobAvgForce");
					surfaceRpmMax = this.getMaxFromDrillParam(bharunUid, DailyUid, "surfaceRpmAvg");
					flowRateMax = this.getMaxFromDrillParam(bharunUid, DailyUid, "flowAvg");
					standpipePressureMax = this.getMaxFromDrillParam(bharunUid, DailyUid, "standpipePressure");
					stringWeightRotaryMaxForce = this.getMaxFromDrillParam(bharunUid, DailyUid, "stringWeightRotaryAvgForce");
					pickupWeightMaxForce = this.getMaxFromDrillParam(bharunUid, DailyUid, "pickupWeightAvgForce");
					slackOffWeightMaxForce = this.getMaxFromDrillParam(bharunUid, DailyUid, "slackOffWeightAvgForce");
				} 
				
				String strSql3 = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:bharunUid AND dailyUid=:DailyUid";		
				String[] paramsFields3 = {"bharunUid", "DailyUid"};
				String[] paramsValues3 = {bharunUid, DailyUid};
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
				
				if (lstResult1.size() > 0){
					for(Iterator i = lstResult1.iterator(); i.hasNext(); ){
						Object obj_array = (Object) i.next();
						BharunDailySummary thisDailySummary = (BharunDailySummary) obj_array;
						
						thisDailySummary.setProgress(totalProgress);
						thisDailySummary.setDuration(totalDuration);
						thisDailySummary.setIADCDuration(totalIADCDuration);
						thisDailySummary.setKrevs(totalKrevs);
						thisDailySummary.setSlideHours(totalSlideHours);
						thisDailySummary.setDurationRotated(totalDurationRotated);
						thisDailySummary.setDurationCirculated(totalDurationCirculated);
						thisDailySummary.setPercentSlide(totalAvgPercentSlide);
						thisDailySummary.setPercentRotated(totalAvgPercentRotated);
						thisDailySummary.setPercentCirculated(totalAvgPercentCirculated);
						thisDailySummary.setHsi(totalAvgHsi);
						
						thisDailySummary.setWobMinForce(wobMinForce);
						thisDailySummary.setWobMaxForce(wobMaxForce);							
						thisDailySummary.setSurfaceRpmMin(surfaceRpmMin);
						thisDailySummary.setSurfaceRpmMax(surfaceRpmMax);
						thisDailySummary.setFlowRateMin(flowRateMin);
						thisDailySummary.setFlowRateMax(flowRateMax);
						thisDailySummary.setStandpipePressureMin(standpipePressureMin);
						thisDailySummary.setStandpipePressureMax(standpipePressureMax);
						thisDailySummary.setStringWeightRotaryMinForce(stringWeightRotaryMinForce);
						thisDailySummary.setStringWeightRotaryMaxForce(stringWeightRotaryMaxForce);
						thisDailySummary.setPickupWeightMinForce(pickupWeightMinForce);
						thisDailySummary.setPickupWeightMaxForce(pickupWeightMaxForce);
						thisDailySummary.setSlackOffWeightMinForce(slackOffWeightMinForce);
						thisDailySummary.setSlackOffWeightMaxForce(slackOffWeightMaxForce);
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDailySummary, qp);
					}
				}
			}	
		}
	}
	
	public void setStartAndLastOperationDate(String operationUid) throws Exception {
		
		if (StringUtils.isNotBlank(operationUid)) {
			String strSql = "select Max(dayDate), min(dayDate) from Daily where (isDeleted = false or isDeleted is null) and operationUid=:operationUid";
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			
			if (lstResult.size() > 0){
				Object[] objResult = (Object[]) lstResult.get(0);
				
				if (objResult!= null)  {
					strSql = "UPDATE Operation SET sysOperationStartDatetime=:startDate, sysOperationLastDatetime=:lastDate WHERE operationUid =:operationUid";
					String[] paramsFields = {"startDate", "lastDate", "operationUid"};
					Object[] paramsValues = {objResult[1], objResult[0], operationUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
				}	
		
			}
		}
	}

	
	public Double calcTotalValueFromDrilParamByOperation (String fieldname, String operationUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT SUM(" + fieldname + ") as totalValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
					"AND operationUid = :operationUid AND (excludeFromNewHole = false or excludeFromNewHole is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
		Object a = (Object) lstResult.get(0);
		Double totalValue = 0.00;
		if (a != null) totalValue = Double.parseDouble(a.toString());
		
		return totalValue;
		
	}
	
	public Double calcTotalValueFromDrilParamByDaily (String fieldname, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT SUM(" + fieldname + ") as totalValue FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) " +
					"AND dailyUid = :dailyUid AND (excludeFromNewHole = false or excludeFromNewHole is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
		Object a = (Object) lstResult.get(0);
		Double totalValue = 0.00;
		if (a != null) totalValue = Double.parseDouble(a.toString());
		
		return totalValue;
		
	}
	
	public Double calculateActivityDurationByOperation (String operationUid) throws Exception {
		
		String strSql = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"AND (dayPlus is null or dayPlus = 0) " +
					"AND operationUid = :operationUid " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
		Object a = (Object) lstResult.get(0);
		Double totalActivityDuration = 0.00;
		if (a != null) totalActivityDuration = Double.parseDouble(a.toString());
		
		return totalActivityDuration;
		
	}

	public Double calculateActivityDurationByDaily (String dailyUid) throws Exception {
		
		String strSql = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"AND (dayPlus is null or dayPlus = 0) " +
					"AND dailyUid = :dailyUid " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null)";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid);
		Object a = (Object) lstResult.get(0);
		Double totalActivityDuration = 0.00;
		if (a != null) totalActivityDuration = Double.parseDouble(a.toString());
		
		return totalActivityDuration;	
	}
	
	public Double spud_td_duration_simple_date_calc(Date spudDate, Date tdDateTime) throws Exception {
		double totalDays = 0.0;
		
		if (spudDate != null && tdDateTime !=null) {
			totalDays = (tdDateTime.getTime() - spudDate.getTime()) / 1000;
		}
		
		return totalDays;
	}
	
	/**
	 * This function is use to look for QC Status of a report base on dailyUid and report type 
	 * @param dailyUid, reportType
	 * @return string
	 * @throws Exception
	 */
	public String getReportDailyQCStatus(String dailyUid, String reportType) throws Exception {
		String strSql ="FROM ReportDaily WHERE dailyUid=:dailyUid AND reportType = :reportType"; 
		String[] paramsFields = {"dailyUid", "reportType"};
		String[] paramsValues = {dailyUid, reportType};						
		List<ReportDaily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (rs.size() > 0) {
			ReportDaily thisRD = rs.get(0);
			
			//get the lookup code
			String uri = "xml://reportDaily.qcflag?key=code&value=label";
			Map<String, LookupItem> QCFlagLookup = LookupManager.getConfiguredInstance().getLookup(uri, null, null);
			
			if (QCFlagLookup.get(thisRD.getQcFlag())!=null) {
				return QCFlagLookup.get(thisRD.getQcFlag()).getValue().toString();
			}
			
		}
		return null;
	}
	
	/**
	 * This function is use to look for EUB code from the formation selected 
	 * @param formationName, basinUid
	 * @return string
	 * @throws Exception
	 */
	public String getEubCode(String formationName, String basinUid) throws Exception
	{
		if (formationName != null)
		{	
			if (basinUid != null)
			{
				String strSql = "select basinFormationUid from BasinFormation where (isDeleted = false or isDeleted is null) and basinUid = :basinUid and name = :name";
				String[] paramsFields = {"basinUid", "name"};
				Object[] paramsValues = {basinUid, formationName};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);	
				if (!lstResult.isEmpty())
				{
					Object thisResult = (Object) lstResult.get(0);
					if(thisResult != null) 
						{
							BasinFormation formation = (BasinFormation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(BasinFormation.class, thisResult.toString());
							
							if (formation != null && (formation.getIsDeleted() == null || !formation.getIsDeleted())) {	
								if (formation.getDescription() != null) {
									return formation.getDescription().toString();
								}
							}
						}
				}
			}
		}
		return null;
	}
	
	public Double calculateJetVelocity(String bharunUid, String dailyUid) throws Exception {
		Double jetvelocity = null;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) and a.bharunUid = :bharunUid AND a.dailyUid = :dailyUid AND a.dailyUid=r.dailyUid AND a.depthTopMdMsl IN (SELECT max(a.depthTopMdMsl) FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND a.bharunUid = :bharunUid AND a.dailyUid = :dailyUid AND r.depthMdMsl>=a.depthTopMdMsl AND a.dailyUid=r.dailyUid)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid", "dailyUid"}, new Object[]{bharunUid, dailyUid}, qp);
		Object flowResult = (Object) lstResult.get(0);
		double flowAvg = 0.00;
		if (flowResult != null){
			flowAvg = Double.parseDouble(flowResult.toString());
		}
		
		strSql = "select bitrunUid FROM Bitrun where (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
		if (lstResult.size()>0) {
			Double totalNozzleSize = 0.0;
			for (Object bitrunUid : lstResult) {
				strSql = "FROM BitNozzle WHERE (isDeleted=false or isDeleted is null) and bitrunUid=:bitrunUid";
				List<BitNozzle> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid.toString(), qp);
				for (BitNozzle rec : lstResult2) {
					if (rec.getNozzleSize()!=null && rec.getNozzleQty()!=null) {
						Double nozzleSize = Math.pow(rec.getNozzleSize() / 0.0007937, 2);
						Integer quantity = rec.getNozzleQty();
						totalNozzleSize += (nozzleSize * quantity);
					}
					
				}
			}
		
			if (totalNozzleSize>0.0) jetvelocity = ((417.2 * flowAvg / 0.0000630902) / totalNozzleSize) * 0.30480000000000003340608;
		}
		
		return jetvelocity;
	}
	
	public void calcCumulativeBitProgressDuration(String dailyUid, String operationUid) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double bitDepthTop = 0.0;
		Double bitDepthBottom = 0.0;
		Double bitHourIn = 0.0;
		Double bitHourOut = 0.0;

		//use serial number to identify the same bit if it has re-run
		String strSql = "select b.serialNumber from Bitrun b, BharunDailySummary bds where (b.isDeleted = false or b.isDeleted is null) " +
		 	"and (bds.isDeleted = false or bds.isDeleted is null) and bds.dailyUid=:dailyUid and b.bharunUid=bds.bharunUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid);
		
		if (lstResult.size() > 0) {
			for (Object recResult : lstResult) {
				// serial number could be null, check for nullability [ryau]
				if (recResult != null) {
					bitDepthTop = 0.0;
					bitDepthBottom = 0.0;
					bitHourIn = 0.0;
					bitHourOut = 0.0;
					
					String serialNumber = recResult.toString();
					
					String sql1 = "select bharunUid from Bitrun where serialNumber=:serialNumber and operationUid=:operationUid and (isDeleted = false or isDeleted is null)";
					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql1, new String[]{"operationUid", "serialNumber"}, new Object[] {operationUid, serialNumber});
					
					if (list.size() > 0) {
						String sql2 ="select daily.dayDate, bharunDailySummary.bharunDailySummaryUid from BharunDailySummary bharunDailySummary, Daily daily where bharunDailySummary.bharunUid " +
								"IN (:bharunList) and (bharunDailySummary.isDeleted = false or bharunDailySummary.isDeleted is null) and " +
								"(daily.isDeleted = false or daily.isDeleted is null) and bharunDailySummary.dailyUid = daily.dailyUid " +
								"order by daily.dayDate";
						
						List<Object[]>  list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "bharunList", list);
						
						if (list2.size() > 0){
							for (Object[] rec: list2){
								String sql3 = "select sum(bharunDailySummary.progress) as totalProgress, sum(bharunDailySummary.duration) as totalDuration from BharunDailySummary bharunDailySummary, Daily daily " +
										"where (bharunDailySummary.isDeleted = false or bharunDailySummary.isDeleted is null) and (daily.isDeleted = false or daily.isDeleted is null) and " +
										"daily.dayDate <:selectedDate and bharunDailySummary.dailyUid = daily.dailyUid and bharunDailySummary.bharunUid IN ( :bharunlist )";
								List <Object[]> list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql3, new String[] {"selectedDate", "bharunlist"}, new Object[] {rec[0], list} ,qp);
								
								if (list3.size() >0){
									Object[] a = (Object[]) list3.get(0);
									
									if (a[0] !=null) {
										bitDepthTop = Double.parseDouble(a[0].toString());
									}
									
									if(a[1] !=null) {
										bitHourIn =  Double.parseDouble(a[1].toString());
									}
								}
								
								String thisBhaDailySummary = rec[1].toString();
								
								//get all the bharun daily summary which using the bit to update 
								String strSql1 = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunDailySummaryUid =:thisBhaDailySummary";		
								List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "thisBhaDailySummary", thisBhaDailySummary, qp);
								
								if (lstResult1.size() > 0){
									
									BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult1.get(0);
									//save bit depth drilled in /out
									thisBhaDaily.setBitDepthDrilledIn(bitDepthTop);
									if (thisBhaDaily.getProgress() !=null)  {
										bitDepthBottom = bitDepthTop + thisBhaDaily.getProgress();
									}else {
										bitDepthBottom = bitDepthTop;
									}
									thisBhaDaily.setBitDepthDrilledOut(bitDepthBottom);
									
									//save bit hour in / out
									thisBhaDaily.setBitHoursIn(bitHourIn);
									if (thisBhaDaily.getDuration() !=null)  {
										bitHourOut = bitHourIn + thisBhaDaily.getDuration();
									}else {
										bitHourOut = bitHourIn;
									}
									
									thisBhaDaily.setBitHoursOut(bitHourOut);
									
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily, qp);
	
								}	
									
							}
						}
					}
				}
			}
		}

	}

	public Double getCumNPTHrsToDate(String operationUid, String dailyUid) throws Exception {
		Double cumHrs = 0.0;
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			if (daily.getDayDate()!=null) {
				List dailyList = new ArrayList();
				
				//collect all dailyUids
				String queryString = "SELECT dailyUid FROM Daily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND operationUid=:operationUid " +
						"AND dayDate<=:dayDate ";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"operationUid", "dayDate"}, 
						new Object[] {daily.getOperationUid(), daily.getDayDate()});
				for (Object rec : list) {
					dailyList.add(rec.toString());
				}
				String strSql = "SELECT SUM(a.activityDuration) FROM Activity a WHERE (a.isDeleted = false or a.isDeleted is null)" +
						 		" AND a.dailyUid IN (:dailyUid) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))" +
						 		" AND (a.dayPlus IS NULL OR a.dayPlus=0)" +
						 		" AND a.classCode IN ('NPT','TP','TU')";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyList);
				Object a = (Object) lstResult.get(0);
				if (a != null) cumHrs = Double.parseDouble(a.toString());			
			}
		}
		return cumHrs;
	}
	
	public String getCurrentMD(String thisOperationUid) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String strSql = "SELECT MAX(depthMdMsl) FROM ReportDaily WHERE operationUid = :operationUid AND reportType = 'DDR' AND (isDeleted IS NULL OR isDeleted = '')";
		List listCurrentMD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", thisOperationUid, qp);
		if (listCurrentMD.size() > 0) {
			Object thisResultCurrentMD = (Object) listCurrentMD.get(0);
			if(thisResultCurrentMD != null)
			{
				return thisResultCurrentMD.toString();
			}
		}
		return null;
	}
	
	public String getCurrentTVD(String thisOperationUid) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String strSql = "SELECT MAX(depthTvdMsl) FROM ReportDaily WHERE operationUid = :operationUid AND reportType = 'DDR' AND (isDeleted IS NULL OR isDeleted = '')";
		List listCurrentTVD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", thisOperationUid, qp);
		if (listCurrentTVD.size() > 0) {
			Object thisResultCurrentTVD = (Object) listCurrentTVD.get(0);
			if(thisResultCurrentTVD != null)
			{			
				return thisResultCurrentTVD.toString();
			}
		}
		return null;
	}

	public String getKBelevation(String wellUid) throws Exception
	{
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT MIN(sysOperationStartDatetime), operationUid, defaultDatumUid, spudDate, rigOffHireDate " +
				"FROM Operation WHERE (isDeleted = false or isDeleted is null) AND wellUid = :wellUid AND operationCode like '%DRLLG%' " +
				"GROUP BY operationUid,defaultDatumUid,spudDate,rigOffHireDate,sysOperationStartDatetime ORDER BY sysOperationStartDatetime", "wellUid", wellUid);
			if (list.size() > 0) {
				Object[] thisResult = (Object[]) list.get(0);
				if(thisResult[2] != null) {
					return thisResult[2].toString();
				}
			}
		return null;
	}
	
	public Double getGeology24HrsProgress(String operationUid, String dailyUid, ReportDaily thisReport,CommandBean commandBean) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		Double geologyProgress = null;
		
		if (thisReport ==null) return null;
		
		Double depth0600MdMsl = (Double) thisReport.getDepth0600MdMsl();
		
		if (depth0600MdMsl!=null) {
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, ReportDaily.class, "depth0600MdMsl");
			thisConverterField.setBaseValueFromUserValue(depth0600MdMsl);
			depth0600MdMsl = thisConverterField.getBasevalue();
		
		
			if (StringUtils.isNotBlank(dailyUid)) {		
				
				String[] paramsFields2 = {"operationUid", "dailyUid"};
		 		Object[] paramsValues2 = new Object[2];
		 		paramsValues2[0] = operationUid; 
				paramsValues2[1] = dailyUid;
				
				List<ReportDaily> reportdaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily " +
						"WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid and dailyUid= :dailyUid and " +
						"reportType='DGR'", paramsFields2, paramsValues2, qp);
				
				if(reportdaily.size() > 0){
					
					if (reportdaily.get(0).getDepth0600MdMsl() !=null ){
						geologyProgress = depth0600MdMsl - reportdaily.get(0).getDepth0600MdMsl();
					}
					
				}
				
			}else { //day # 1 no previous day
				
				String currentDailyUid = thisReport.getDailyUid();
				List<ReportDaily> ddrReportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily " +
						"WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid and dailyUid= :dailyUid and " +
						"reportType='DDR'", new String[] {"operationUid", "dailyUid"}, new Object[] {operationUid, currentDailyUid}, qp);
				
				if(ddrReportDaily.size() > 0){
					if (ddrReportDaily.get(0).getDepth0600MdMsl() !=null ){
						geologyProgress = depth0600MdMsl - ddrReportDaily.get(0).getDepth0600MdMsl();
					}
				}
			}
			
			if (geologyProgress !=null) {
				thisConverterField.setReferenceMappingField(ReportDaily.class, "geology24hrProgress");
				thisConverterField.setBaseValue(geologyProgress);
				thisConverterField.addDatumOffset();
				geologyProgress = thisConverterField.getConvertedValue();
				return geologyProgress;
			}
		}
		
		return null;
	}
	
	public Double getDriller24HrsProgress(String operationUid, String dailyUid, ReportDaily thisReport, CommandBean commandBean) throws Exception
	{	
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		Double progress = null;
		
		if (thisReport ==null) return null;

		Double depthMdMsl = (Double) thisReport.getDepthMdMsl();
		
		if (depthMdMsl!=null) {
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");
			thisConverterField.setBaseValueFromUserValue(depthMdMsl);
			depthMdMsl = thisConverterField.getBasevalue();
		
		
			if (StringUtils.isNotBlank(dailyUid)) {
				
				String[] paramsFields2 = {"operationUid", "dailyUid","reportType"};
		 		Object[] paramsValues2 = new Object[3];
				
		 		paramsValues2[0] = operationUid; 
				paramsValues2[1] = dailyUid;
				paramsValues2[2] = thisReport.getReportType();
			 	
				List<ReportDaily> reportdaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily " +
						"WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid and dailyUid= :dailyUid and " +
						"reportType= :reportType", paramsFields2, paramsValues2, qp);
				
				if(reportdaily.size() > 0){
					if (reportdaily.get(0).getDepthMdMsl() !=null ){
						progress = depthMdMsl - reportdaily.get(0).getDepthMdMsl();
					}
				}
				
			}else { //day # 1 no previous day
			
				List<Operation> thisOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation " +
						"WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid", "operationUid", operationUid, qp);
				
				
				if(thisOperation.size() > 0){
					if (thisOperation != null) {
				 		if (thisOperation.get(0).getSpudMdMsl() != null) {
				 			progress = depthMdMsl - thisOperation.get(0).getSpudMdMsl();
				 		}else {
				 			List<Wellbore> thisWellbore = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Wellbore " +
									"WHERE (isDeleted = false or isDeleted is null) and wellboreUid= :wellboreUid", "wellboreUid", thisOperation.get(0).getWellboreUid(), qp);
					
							if(thisWellbore.size() > 0){
								if (thisWellbore !=null) {
					 				if (thisWellbore.get(0).getKickoffMdMsl() != null) {
					 					progress = depthMdMsl - thisWellbore.get(0).getKickoffMdMsl();
					 				}
					 			}
							}
								
				 		}
					}
				}
			}
			
			if (progress !=null) {
				thisConverterField.setReferenceMappingField(ReportDaily.class, "progress");
				thisConverterField.setBaseValue(progress);
				thisConverterField.addDatumOffset();
				progress = thisConverterField.getConvertedValue();
				return progress;
			}
		}
		
		return null;
		
	}
	
	public void updateFirstDay24HrsProgress (String operationUid, Double depth, CommandBean commandBean, Object object) throws Exception 
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		
		Double depthMdMsl = depth;
		
		if (depthMdMsl!=null) {
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean);
			if (object instanceof Wellbore) {
				thisConverterField = new CustomFieldUom(commandBean, Wellbore.class, "kickoffMdMsl");
			}else if (object instanceof Operation){
				thisConverterField = new CustomFieldUom(commandBean, Operation.class, "spudMdMsl");
			}
			
			thisConverterField.setBaseValueFromUserValue(depthMdMsl);
			depthMdMsl = thisConverterField.getBasevalue();
		
			if (operationUid !=null) {
				Double progress = null;
				List<ReportDaily> reportdaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid " +
			 			"and reportType <> 'DGR' order by reportDatetime ASC", "operationUid", operationUid ,qp);
				
				if(reportdaily.size() > 0){
					
					ReportDaily rd = reportdaily.get(0);
					
					if (rd.getDepthMdMsl() != null) {
						
						progress = rd.getDepthMdMsl() - depthMdMsl;
						rd.setProgress(progress);						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rd, qp);
						
				 	
					}
				}
			}
		}
	 	
	}

	public void UpdateBharunDateInDateOut(String previousDailyUid, String currentDailyUid, String reportType) throws Exception
	{
		//only update if day date changed for DDR report daily
		if ("DDR".equalsIgnoreCase(reportType)){			
			
			String[] paramsFields = {"previousDailyUid"};
			String[] paramsValues = {previousDailyUid};
			String strSql = "";
			
			//Update bharun dailyidIn to new dailyUid and timeIn to new day date + time in
			strSql = "FROM Bharun WHERE (isDeleted = false or isDeleted is null) and dailyidIn = :previousDailyUid";
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0) {
				
				// serial number could be null, check for nullability [ryau]
				for(Object objBharun: lstResult){
					Bharun thisBharun = (Bharun) objBharun;
					Date thisDateIn = null;
					
					if (thisBharun != null) {
						String bharunUid = thisBharun.getBharunUid();
						Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
						
						if(thisBharun.getTimeIn() != null){					
							
							if(CommonDateParser.parse(thisDaily.getDayDate().toString()) != null)
							{
								thisDateIn = CommonDateParser.parse(thisDaily.getDayDate().toString());
							}
							else
							{
								thisDateIn = (Date) thisDaily.getDayDate();
							}
							Calendar dateIn = Calendar.getInstance();
							if(thisDateIn != null){
								dateIn.setTime(thisDateIn);
								if(PropertyUtils.getProperty(thisBharun, "timeIn") != null)
								{
									Calendar TimeInDatetime = Calendar.getInstance();
									TimeInDatetime.setTime((Date) PropertyUtils.getProperty(thisBharun, "timeIn"));
									TimeInDatetime.set(Calendar.YEAR, dateIn.get(Calendar.YEAR));
									TimeInDatetime.set(Calendar.MONTH, dateIn.get(Calendar.MONTH));
									TimeInDatetime.set(Calendar.DAY_OF_MONTH, dateIn.get(Calendar.DAY_OF_MONTH));
									PropertyUtils.setProperty(thisBharun, "timeIn", TimeInDatetime.getTime());
								}
							}
						}else{
							PropertyUtils.setProperty(thisBharun, "timeIn", thisDaily.getDayDate());
						}
						Date timeIn = null;
						timeIn = (Date) thisBharun.getTimeIn();
						
						String[] paramsFields2 = {"currentDailyUid", "timeIn", "bharunUid"};
						Object[] paramsValues2 = new Object[3]; paramsValues2[0] = currentDailyUid; paramsValues2[1] = timeIn; paramsValues2[2] = bharunUid;
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE Bharun SET dailyidIn = :currentDailyUid, timeIn = :timeIn where bharunUid = :bharunUid", paramsFields2, paramsValues2);
					}
				}			
			}
			
			//Update bharun dailyidOut to new dailyUid and timeOut to new day date + time out
			strSql = "FROM Bharun WHERE (isDeleted = false or isDeleted is null) and dailyidOut = :previousDailyUid";
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult2.size() > 0) {
				// serial number could be null, check for nullability [ryau]
				for(Object objBharun: lstResult2){
					Bharun thisBharun = (Bharun) objBharun;
					Date thisDateOut = null;
					
					if (thisBharun != null) {
						String bharunUid = thisBharun.getBharunUid();
						Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
						
						if(thisBharun.getTimeOut() != null){					
							
							if(CommonDateParser.parse(thisDaily.getDayDate().toString()) != null)
							{
								thisDateOut = CommonDateParser.parse(thisDaily.getDayDate().toString());
							}
							else
							{
								thisDateOut = (Date) thisDaily.getDayDate();
							}
							Calendar dateOut = Calendar.getInstance();
							if(thisDateOut != null){
								dateOut.setTime(thisDateOut);
								if(PropertyUtils.getProperty(thisBharun, "timeIn") != null)
								{
									Calendar TimeInDatetime = Calendar.getInstance();
									TimeInDatetime.setTime((Date) PropertyUtils.getProperty(thisBharun, "timeIn"));
									TimeInDatetime.set(Calendar.YEAR, dateOut.get(Calendar.YEAR));
									TimeInDatetime.set(Calendar.MONTH, dateOut.get(Calendar.MONTH));
									TimeInDatetime.set(Calendar.DAY_OF_MONTH, dateOut.get(Calendar.DAY_OF_MONTH));
									PropertyUtils.setProperty(thisBharun, "timeIn", TimeInDatetime.getTime());
								}
							}
						}else{
							PropertyUtils.setProperty(thisBharun, "timeOut", thisDaily.getDayDate());
						}
						Date timeOut = null;
						timeOut = (Date) thisBharun.getTimeOut();
						
						String[] paramsFields2 = {"currentDailyUid", "timeOut", "bharunUid"};
						Object[] paramsValues2 = new Object[3]; paramsValues2[0] = currentDailyUid; paramsValues2[1] = timeOut; paramsValues2[2] = bharunUid;
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE Bharun SET dailyidOut = :currentDailyUid, timeOut = :timeOut where bharunUid = :bharunUid", paramsFields2, paramsValues2);
					}
				}
			}				
		}
	}
	
	public void calcPlannedDurationFromOperationPlanPhase(String operationUid) throws Exception
	{
		if (operationUid==null) return;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String planMasterUid = this.getActiveDvdPlanUid(operationUid);
		
		String[] paramNames = {"operationUid", "planMasterUid"};
		Object[] paramValues = {operationUid, planMasterUid};
		
		String queryString = "SELECT SUM(opp.p50Duration) as totalDuration FROM OperationPlanPhase opp, OperationPlanMaster opm " +
				"WHERE opm.operationPlanMasterUid = opp.operationPlanMasterUid AND " +
				"(opp.isDeleted=false or opp.isDeleted is null) AND (opm.isDeleted=false or opm.isDeleted is null) AND " +
				"opm.operationUid=:operationUid AND opp.operationPlanMasterUid=:planMasterUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);		
		
		Object a = (Object) lstResult.get(0);
		Double totalDuration = null;
		if (a != null) totalDuration = Double.parseDouble(a.toString());
		
		if (totalDuration != null) {
			String strSql = "UPDATE Operation SET plannedDuration =:totalDuration WHERE operationUid =:operationUid";
			String[] paramsFields = {"totalDuration", "operationUid"};
			Object[] paramsValues = {totalDuration, operationUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
	}
	
	public Basin getDefaultBasin() throws Exception
	{
		List<Basin> basinList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Basin where (isDeleted is null or isDeleted = false) and isDefault = true");
		if (basinList.size()>0)
			return basinList.get(0);
		return null;
	}
	
	public String showTimeAs2400(Object getTime){
		
		if (getTime == null) return "";
		
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		
		if (("23:59").equalsIgnoreCase((String) timeFormatter.format(getTime))) {
			return "24:00";
		}else {
			return (String) timeFormatter.format(getTime);
		}
		
	}
	
//  20100321-1223-ekhoo - EFC Cost
	public void UpdateReportDailyEFC(String currentOperationUid, String currentDailyUid) throws Exception
	{
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		Double totalActualHours = 0.0;
	//	totalActualHours = this.calculateActivityDurationByDaily(currentDailyUid);
		
		Double EFCtotalCost = 0.0;
		Double EFCotherCost = 0.0;
		Double totalEFCVessel = null;
		Double HourlyCost = 0.0;
		
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisDailyUid";
		String[] paramsFields2 = {"thisDailyUid"};
		Object[] paramsValues2= {currentDailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		if (lstResult.size() > 0){
			ReportDaily thisReportDaily = (ReportDaily) lstResult.get(0);
			
			
			String strvSql = "select sum(b.activityDuration) as duration from ReportDaily a, Activity b where a.dailyUid=b.dailyUid and (a.isDeleted is null or a.isDeleted = False) " +
			          "and a.operationUid=b.operationUid and (b.isDeleted is null or b.isDeleted = False) and (b.carriedForwardActivityUid is null or b.carriedForwardActivityUid ='' ) " +
			          "AND (b.isSimop=false or b.isSimop is null) AND (b.isOffline=false or b.isOffline is null) and a.reportDatetime <= :todayDate and b.operationUid = :operationUid";
			paramsFields2 = new String[] {"todayDate","operationUid"};
			paramsValues2 = new Object[] {thisReportDaily.getReportDatetime(),currentOperationUid};
			
			List lstvResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strvSql, paramsFields2, paramsValues2);
			Object v = (Object) lstvResult.get(0);			
			if (v != null) totalActualHours = Double.parseDouble(v.toString());
						
			if (thisReportDaily.getDurationToCompletion() != null) totalActualHours = totalActualHours + thisReportDaily.getDurationToCompletion();
			
			Operation currentOperation 	= (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, currentOperationUid);
			
			HourlyCost = currentOperation.getOperationVesselHourlyCost();
			if (HourlyCost != null)
			{
				DecimalFormat nf = new DecimalFormat();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);	
				
				// plus 0.001 to make sure number is round to 1 when third fraction digit is 5 for example 0.625, without this answer is 0.62. with plus 0.001, the answer is 0.63
				totalEFCVessel = Double.parseDouble(nf.format((totalActualHours / (3600*24)) + 0.001)) * HourlyCost;
				EFCotherCost = thisReportDaily.getEfcOtherCost();
				if (EFCotherCost != null) EFCtotalCost = totalEFCVessel + EFCotherCost;
				else EFCtotalCost = totalEFCVessel;
				
				if (totalEFCVessel != null) {
					String strSql1 = "UPDATE ReportDaily SET efcVesselCost =:thisEFCVessel ," +
									 "efcTotalCost =:thisEFCtotalCost " +
									 "WHERE dailyUid =:thisDailyUid";
					String[] paramsFields = {"thisEFCVessel", "thisEFCtotalCost", "thisDailyUid"};
					Object[] paramsValues = {totalEFCVessel, EFCtotalCost, currentDailyUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);
				}
			} else{
				totalEFCVessel = null;
				EFCtotalCost = null;
				String strSql1 = "UPDATE ReportDaily SET efcVesselCost =:thisEFCVessel ," +
				 "efcTotalCost =:thisEFCtotalCost " +
				 "WHERE dailyUid =:thisDailyUid";
				String[] paramsFields = {"thisEFCVessel", "thisEFCtotalCost", "thisDailyUid"};
				Object[] paramsValues = {totalEFCVessel, EFCtotalCost, currentDailyUid};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);
			}
		}
	}
	
	
//  20110309-0608-sften  - cum Actual Vessel Cost
	//calculate cum. actual vessel cost	 = Actual Daily Vessel Cost * total activity duration to date in day
	public Double CalculatecumActualVesselCost(String currentOperationUid, String currentDailyUid) throws Exception
	{
		Double cumActualVesselCost = null;
		Double ActualDaystoCost = 0.;
				
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
			
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisDailyUid";
		String[] paramsFields = {"thisDailyUid"};
		Object[] paramsValues= {currentDailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			
			ReportDaily thisReportDaily = (ReportDaily) lstResult.get(0);			
			
			if (thisReportDaily.getActualVesselCost()!=null) ;
			{
				cumActualVesselCost = thisReportDaily.getActualVesselCost();				
				
				if (cumActualVesselCost != null)
				{
					String strvSql = "select sum(b.activityDuration) as duration from ReportDaily a, Activity b where a.dailyUid=b.dailyUid and (a.isDeleted is null or a.isDeleted = False) " +
					          "and a.operationUid=b.operationUid and (b.isDeleted is null or b.isDeleted = False) and (b.carriedForwardActivityUid is null or b.carriedForwardActivityUid ='' ) " +
					          "AND (b.isSimop=false or b.isSimop is null) AND (b.isOffline=false or b.isOffline is null) and a.reportDatetime <= :todayDate and b.operationUid = :operationUid";
					paramsFields = new String[] {"todayDate","operationUid"};
					paramsValues = new Object[] {thisReportDaily.getReportDatetime(),currentOperationUid};
					List lstvResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strvSql, paramsFields, paramsValues);
					Object v = (Object) lstvResult.get(0);
					
					if (v != null)
					{							
						DecimalFormat nf = new DecimalFormat();
						nf.setMaximumFractionDigits(2);
						nf.setMinimumFractionDigits(2);	
						
						// plus 0.001 to make sure number is round to 1 when third fraction digit is 5 for example 0.625, without this answer is 0.62. with plus 0.001, the answer is 0.63
						ActualDaystoCost = Double.parseDouble(nf.format((Double.parseDouble(v.toString()) / (24.0 * 3600.0)) + 0.001));
					}
					cumActualVesselCost *=  ActualDaystoCost; 
				}
			}
		}
		
		return cumActualVesselCost;
	}


	public Double getDailyDayCost(Date dayDate, UserSession session) throws Exception {
		
		if (dayDate !=null) {			
			Date selectedDate =dayDate;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(selectedDate);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			selectedDate =calendar.getTime();
			
			String strSql = "Select dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate =:selectedDayDate and operationUid =:operationUid";							
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String [] {"selectedDayDate", "operationUid"}, new Object[] {selectedDate, session.getCurrentOperationUid()});
			Double dailyCost = null;
			if (lstResult.size()>0)
			{
				Object objDaily = lstResult.get(0);
				dailyCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(objDaily.toString());
				if (dailyCost==null)
				{
					ReportDaily rd=CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, objDaily.toString());
					if (rd!=null)
					{
						dailyCost = rd.getDaycost();
					}
				}
				
			}
			return dailyCost;
		}
		return null;
	}
				
	public void calculateCostIncurred(Date failureDatetime, Double dailyCost, String toolType) throws Exception {
		
		if (failureDatetime !=null) {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(failureDatetime);
			
			String strSql2 = "FROM EquipmentFailure WHERE (isDeleted = '' or isDeleted is null) and dateFailStart =:selectedDayDate and toolType=:toolType";							
			List <EquipmentFailure>lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String [] {"selectedDayDate","toolType"},new Object[] {calendar2.getTime(),toolType}, qp);

			if (!lstResult2.isEmpty()) {
				for (EquipmentFailure eq : lstResult2) {
					Double failureDuration = eq.getFailDuration();
					String equipmentFailureUid = eq.getEquipmentFailureUid();
					
					Double estimatedFailureCost = null;
					if (dailyCost!=null && failureDuration !=null)
					{
						double duration = failureDuration / 3600;
						estimatedFailureCost = dailyCost / 24 * duration;
					}
						
					String strSqlUpdate = "UPDATE EquipmentFailure SET estimatedFailureCost =:estimatedFailureCost WHERE equipmentFailureUid =:equipmentFailureUid and toolType=:toolType";	
					String[] paramNames = {"estimatedFailureCost", "equipmentFailureUid","toolType"};
					Object[] paramValues = {estimatedFailureCost, equipmentFailureUid, toolType};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
				
				}
			}
		}
	}
	
	
	
	public Double getBaseValue(CustomFieldUom thisConverter, Class classType,String field, Double value) throws Exception
	{
		thisConverter.setReferenceMappingField(classType, field);
		thisConverter.setBaseValueFromUserValue(value,false);
		return thisConverter.getBasevalue();
	}
	
	// 20100614-0742-anoeb - Move calculateEMW to CommonUtil 
	
	
	// refer to ticket #20100715-0843-mlley
	public void calculatePumpFlowRate(CommandBean commandBean, Object object) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (object instanceof RigPumpParam) {
			RigPumpParam pump = (RigPumpParam) object;
			Double flowRate = 0.00; // flow rate
			Double vps = 0.00; // volume per stroke
			Double spm = 0.00; // stroke per minute
			Double liner = 0.00; //liner diameter
			Double rodDiam = 0.00; //rod diameter
			Double sLength = 0.00; //stroke length
			Double efficiency = 0.00; //efficiency
			String pumpType = null;
			
			if (pump.getPumptype() != null) {
				pumpType = pump.getPumptype();
			} else return;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			// get Liner Diameter value
			if (pump.getLiner() != null) {
				liner = this.getBaseValue(thisConverter, RigPumpParam.class, "liner", pump.getLiner());
				// convert the value to inch
				liner = liner / 0.0254;
			}
			
			if (pump.getRodDiam() != null) {
				rodDiam = this.getBaseValue(thisConverter, RigPumpParam.class, "rodDiam", pump.getRodDiam());
				// convert the value to inch
				rodDiam = rodDiam / 0.0254;
			}
			
			// get Stroke Length value
			if (pump.getStroke() != null) {
				sLength = this.getBaseValue(thisConverter, RigPumpParam.class, "stroke", pump.getStroke());
				// convert the value to inch
				sLength = sLength / 0.0254;
			}
			
			if (pump.getSpm() != null) {
				spm = this.getBaseValue(thisConverter, RigPumpParam.class, "spm", pump.getSpm());
			}
			
			// get Efficiency value
			if (pump.getEfficiency() != null) {
				efficiency = this.getBaseValue(thisConverter, RigPumpParam.class, "efficiency", pump.getEfficiency());
			} else {
				// based on spec, if efficient=null, set it to 1
				efficiency = 1.00;
			}
			
			//get volume per stroke
			if("1".equals(GroupWidePreference.getValue(pump.getGroupUid(), "calculatePumpStrokeVolume"))){
				//force the vps result to be in base (m3/stk) value. Calculated vps is in bbl/stk
				vps = this.calculateStrokeVolume(pumpType, liner, rodDiam, sLength, efficiency)* 0.1589873;
				
			}else {
				if (pump.getOutputVolumeStk() !=null){
					vps = this.getBaseValue(thisConverter, RigPumpParam.class, "outputVolumeStk", pump.getOutputVolumeStk());
				}	
			}
			
			//must change vps to proper unit for calculation usage (bbl/stk)
			thisConverter.setReferenceMappingField(RigPumpParam.class, "outputVolumeStk");
			thisConverter.setBaseValue(vps);
			thisConverter.changeUOMUnit("BarrelPerStroke");
			
			Double dblVps = thisConverter.getConvertedValue();
			
			flowRate = this.calculatePumpFlowRate(dblVps, spm);
	
			String strSql = "FROM RigPumpParam WHERE (isDeleted = false or isDeleted is null) and rigPumpParamUid =:rigPumpParamUid";		
			String[] paramsFields = {"rigPumpParamUid"};
			String[] paramsValues = {pump.getRigPumpParamUid()};
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			if (lstResult1.size() > 0){			
				RigPumpParam thisRigPumpParam = (RigPumpParam) lstResult1.get(0);
				thisRigPumpParam.setVolumeFlowRateOut(flowRate);
				thisRigPumpParam.setOutputVolumeStk(vps);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRigPumpParam, qp);		
			}		
		}
		
		if (object instanceof RigSlowPumpParam) {
			RigSlowPumpParam thisRigSlowPumpParam = (RigSlowPumpParam) object;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			Double flowRate = 0.00; // flow rate
			Double vps = 0.00; // volume per stroke
			Double spm = 0.00; // stroke per minute
			
			if (thisRigSlowPumpParam.getFlow()==null || "".equals(thisRigSlowPumpParam.getFlow())){
				
				if (thisRigSlowPumpParam.getRigPumpParamUid()!= null){
					vps = RigPumpUtils.getVolumeStroke(thisRigSlowPumpParam.getRigPumpParamUid());
				}
				
				if (thisRigSlowPumpParam.getRate()!=null) {
					spm = this.getBaseValue(thisConverter, RigSlowPumpParam.class, "spm", thisRigSlowPumpParam.getRate());
				}

				flowRate = this.calculatePumpFlowRate(vps, spm);

				thisConverter.setReferenceMappingField(RigSlowPumpParam.class, "flow");
				thisConverter.setBaseValue(flowRate);	
				flowRate = thisConverter.getConvertedValue();
				
				String strSqlUpdate = "UPDATE RigSlowPumpParam SET flow =:flow WHERE rigSlowPumpParamUid =:rigSlowPumpParamUid";	
				String[] paramNames = {"flow", "rigSlowPumpParamUid"};
				Object[] paramValues = {flowRate, thisRigSlowPumpParam.getRigSlowPumpParamUid()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);	
			}
			
		}
	}
	
	// refer to ticket #20100715-0843-mlley
	private Double calculatePumpFlowRate(Double vps, Double spm) throws Exception {
		Double flowRate = null;
		flowRate = vps * spm * 0.00264978;
		return flowRate;
	}
	
	public void setBBNameToDynamicAttribute(String operationUid, String dynAttrName, CommandBeanTreeNode node) throws Exception
	{
		List<String> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select remoteHost from SysLogSendToTown Where (isDeleted is null or isDeleted = false) and operationUid=:operationUid and (remoteHost is not null or remoteHost <>'') order by transactionDatetime desc","operationUid",operationUid);
		if (results.size()>0)
		{
			String host = results.get(0);
			if (StringUtils.isNotBlank(host))
			{
				String name=host;
				if (host.contains("."))
				{
					String[] parts = host.split("[.]");
					name=parts[0];
				}
				node.getDynaAttr().put(dynAttrName,name);
			}
		}
	}
	
	
	public Map calculateLastSurveyDepthAndAngle (String thisOperationUid, String dailyUid, CommandBean commandBean) throws Exception {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		Double depthMdMslValue = null;
		Double depthTvdMslValue = null;
		Double inclinationAngle = null;
		Double azimuthAngle = null;
		Double depthTvdMslDaily= null;
		Double depthMdMslDaily= null;
		
		String ReportdailySql = "Select depthMdMsl, depthTvdMsl FROM ReportDaily where dailyUid=:dailyUid and (isDeleted is null or isDeleted='') and reportType != 'DGR'";
		String[] paramsLabels1 = {"dailyUid"};
		Object[] paramsData1 = {dailyUid};
		
		List lstReportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ReportdailySql, paramsLabels1, paramsData1, qp);
 		if (lstReportDaily.size()>0) {
			
 			Object[] ReportDailyRecord = (Object[]) lstReportDaily.get(0);
			if (ReportDailyRecord[0] !=null) depthMdMslDaily = Double.parseDouble(ReportDailyRecord[0].toString());
			if (ReportDailyRecord[1] !=null) depthTvdMslDaily = Double.parseDouble(ReportDailyRecord[1].toString());
 		}
		// get depth md
 		
 		if (depthMdMslDaily!=null) {
			String SurveyStationSql = "Select max(ss.depthMdMsl) AS depthMdMslMax FROM SurveyStation ss, SurveyReference sr" +
					" WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND " +
					"ss.surveyReferenceUid = sr.surveyReferenceUid AND sr.operationUid=:operationUid AND " +
					"(sr.isPlanned is null or sr.isPlanned !='1') AND ss.depthMdMsl <=:depthMdMslDaily";
			String[] paramsLabels = {"operationUid", "depthMdMslDaily"};
			Object[] paramsData = {thisOperationUid, depthMdMslDaily};
	 		List lstSurveyStationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SurveyStationSql, paramsLabels, paramsData, qp);
			
	 		if (lstSurveyStationResult.size()>0) {
				Object a = lstSurveyStationResult.get(0);
				if (a !=null) depthMdMslValue = Double.parseDouble(a.toString());
				thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "lastMdMsl");
				if (thisConverter.isUOMMappingAvailable())
				{
					if(depthMdMslValue!=null)
					{
						thisConverter.setBaseValue(depthMdMslValue);
						depthMdMslValue = thisConverter.getConvertedValue();
					}
				}
			}

	 		//get inclination, azimuth
	 		QueryProperties qp2 = new QueryProperties();		
			qp2.setUomConversionEnabled(false);
			qp2.setFetchFirstRowOnly();
			
	 		String sql = "Select ss.inclinationAngle, ss.azimuthAngle FROM SurveyStation ss, SurveyReference sr " +
					"WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND " +
					"ss.surveyReferenceUid = sr.surveyReferenceUid AND sr.operationUid=:operationUid AND " +
					"(sr.isPlanned is null or sr.isPlanned !='1') AND ss.depthMdMsl <=:depthMdMslDaily ORDER BY ss.depthMdMsl DESC ";
			String[] paramsFields = {"operationUid", "depthMdMslDaily"};
			Object[] paramsValues = {thisOperationUid, depthMdMslDaily};
			
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp2);
			if (list.size() > 0){
				for (Object[] rec : list) {
					if(rec[0] != null) inclinationAngle = Double.parseDouble(rec[0].toString());
					if(rec[1] != null) azimuthAngle = Double.parseDouble(rec[1].toString());
				}
			}	
 		}
 		
 		//get depth tvd
 		if (depthTvdMslDaily !=null){
 			String SurveyStationSql2 = "Select max(ss.depthTvdMsl) AS depthTvdMslMax FROM SurveyStation ss, SurveyReference sr " +
 					"WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND " +
 					"ss.surveyReferenceUid = sr.surveyReferenceUid AND sr.operationUid=:operationUid AND " +
 					"(sr.isPlanned is null or sr.isPlanned !='1') AND ss.depthTvdMsl <=:depthTvdMslDaily";
 			String[] paramsLabels2 = {"operationUid", "depthTvdMslDaily"};
 			Object[] paramsData2 = {thisOperationUid, depthTvdMslDaily};
 			List lstSurveyStationResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SurveyStationSql2, paramsLabels2, paramsData2, qp);
 			if (lstSurveyStationResult2.size()>0) {
 				Object b = lstSurveyStationResult2.get(0);


 				if (b !=null) depthTvdMslValue = Double.parseDouble(b.toString());
 				thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "lastTvdMsl");
 				if (thisConverter.isUOMMappingAvailable())
 				{
 					if(depthTvdMslValue!=null)
 					{
 						thisConverter.setBaseValue(depthTvdMslValue);
 						depthTvdMslValue = thisConverter.getConvertedValue();
 					}
 				}
 			}
 		}
 		
 		thisConverter.setReferenceMappingField(SurveyStation.class, "depthMdMsl");
 		result.put("uomDepthMdMsl", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "depthTvdMsl");
 		result.put("uomDepthTvdMsl", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "inclinationAngle");
 		result.put("uomInclinationAngle", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "azimuthAngle");
 		result.put("uomAzimuthAngle", thisConverter.getUOMMapping());
 		result.put("depthMdMsl", depthMdMslValue);
 		result.put("depthTvdMsl", depthTvdMslValue);
 		result.put("inclinationAngle", inclinationAngle);
 		result.put("azimuthAngle", azimuthAngle);
		
		return result;
	}
	
	public void setLastSurveyDepth (String thisOperationUid, String dailyUid, CommandBean commandBean) throws Exception {

		Map map = this.calculateLastSurveyDepthAndAngle(thisOperationUid, dailyUid, commandBean);
		
		Object depthMdMslValue = map.get("depthMdMsl");
		Object depthTvdMslValue = map.get("depthTvdMsl");
		Object inclinationAngle = map.get("inclinationAngle");
		Object azimuthAngle = map.get("azimuthAngle");
		
		String strSql = "UPDATE ReportDaily SET lastMdMsl =:lastMdMsl, inclinationAngle =:inclinationAngle, azimuthAngle =:azimuthAngle, lastTvdMsl =:lastTvdMsl WHERE dailyUid = :thisReportDailyUid AND reportType='DGR'";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"lastMdMsl", "inclinationAngle", "azimuthAngle", "lastTvdMsl", "thisReportDailyUid"}, new Object[] {depthMdMslValue,inclinationAngle,azimuthAngle,depthTvdMslValue,dailyUid});
	}
	
	
	public Map calculateLastSurveyDepthAndAngleByDayDepth(String thisOperationUid, String dailyUid, String currentDailyUid, Locale locale) throws Exception {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		Double depthMdMslValue = null;
		Double depthTvdMslValue = null;
		Double inclinationAngle = null;
		Double azimuthAngle = null;
		
		Date todayDate = null;
		Daily currentDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, currentDailyUid);
		todayDate = currentDaily.getDayDate();
 		
 		if (todayDate!=null) {
 			
 			// get depth md, inclination angle, azimuth angle
 			String SurveyStationSql = "Select ss.depthMdMsl, ss.inclinationAngle, ss.azimuthAngle FROM SurveyStation ss, SurveyReference sr, ReportDaily rd " +
 					"WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND (rd.isDeleted='' or rd.isDeleted is null) " +
 					"AND ss.surveyReferenceUid = sr.surveyReferenceUid AND (sr.isPlanned is null or sr.isPlanned !='1') AND ss.dailyUid = rd.dailyUid " +
 					"AND sr.operationUid =:operationUid AND rd.reportDatetime <=:todayDate ORDER BY rd.reportDatetime DESC, ss.depthMdMsl DESC";
 			
 			String[] params = {"operationUid","todayDate"};
 			Object[] paramsValue = {thisOperationUid, todayDate};
 			
 			List lstSurveyStationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SurveyStationSql, params, paramsValue, qp);
			
	 		if (lstSurveyStationResult.size()>0) {
				Object[] surveyResult = (Object[])lstSurveyStationResult.get(0);
				if (surveyResult != null){
					if(surveyResult[0] != null)
						depthMdMslValue = Double.parseDouble(surveyResult[0].toString());
					if(surveyResult[1] != null)
						inclinationAngle = Double.parseDouble(surveyResult[1].toString());
					if(surveyResult[2] != null)
						azimuthAngle = Double.parseDouble(surveyResult[2].toString());
				}	
			}

	 		
	 		//get depth tvd
	 		String SurveyTvdMslSql = "Select ss.depthTvdMsl FROM SurveyStation ss, SurveyReference sr, ReportDaily rd " +
 					"WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND (rd.isDeleted='' or rd.isDeleted is null) " +
 					"AND ss.surveyReferenceUid = sr.surveyReferenceUid AND (sr.isPlanned is null or sr.isPlanned !='1') AND ss.dailyUid = rd.dailyUid " +
 					"AND sr.operationUid =:operationUid AND rd.reportDatetime <=:todayDate ORDER BY rd.reportDatetime DESC, ss.depthTvdMsl DESC";
 			
 			String[] params1 = {"operationUid","todayDate"};
 			Object[] paramsValue1 = {thisOperationUid, todayDate};
 			
 			List lstSurveyTvdResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SurveyTvdMslSql, params1, paramsValue1, qp);
			
	 		if (lstSurveyTvdResult.size()>0) {
				Object surveyTvdResult = lstSurveyTvdResult.get(0);
				if (surveyTvdResult !=null){
					depthTvdMslValue = Double.parseDouble(surveyTvdResult.toString());
				}
	 		}
 		}
 		
 		CustomFieldUom thisConverter= new CustomFieldUom(locale);
 		thisConverter.setReferenceMappingField(SurveyStation.class, "depthMdMsl");
 		result.put("uomDepthMdMsl", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "depthTvdMsl");
 		result.put("uomDepthTvdMsl", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "inclinationAngle");
 		result.put("uomInclinationAngle", thisConverter.getUOMMapping());
 		thisConverter.setReferenceMappingField(SurveyStation.class, "azimuthAngle");
 		result.put("uomAzimuthAngle", thisConverter.getUOMMapping());
 		result.put("depthMdMsl", depthMdMslValue);
 		result.put("depthTvdMsl", depthTvdMslValue);
 		result.put("inclinationAngle", inclinationAngle);
 		result.put("azimuthAngle", azimuthAngle);

 		return result;
	}
	
	public void setLastSurveyDepthByDayDepth (String thisOperationUid, String dailyUid, UserSession session) throws Exception {
		Map map = this.calculateLastSurveyDepthAndAngleByDayDepth(thisOperationUid, dailyUid, session.getCurrentDailyUid(), session.getUserLocale());
		
		Object depthMdMslValue = map.get("depthMdMsl");
		Object depthTvdMslValue = map.get("depthTvdMsl");
		Object inclinationAngle = map.get("inclinationAngle");
		Object azimuthAngle = map.get("azimuthAngle");
 
 		String strSql = "UPDATE ReportDaily SET lastMdMsl =:lastMdMsl, inclinationAngle =:inclinationAngle, azimuthAngle =:azimuthAngle, lastTvdMsl =:lastTvdMsl WHERE dailyUid = :thisReportDailyUid AND reportType='DGR'";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"lastMdMsl", "inclinationAngle", "azimuthAngle", "lastTvdMsl", "thisReportDailyUid"}, new Object[] {depthMdMslValue,inclinationAngle,azimuthAngle,depthTvdMslValue,dailyUid});

	}

	public String getParentDvdOperationUid(String operationUid, String parentDvdOperationUid) throws Exception {
		String dvdUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
		OperationPlanMaster dvd = null;
		if (dvdUid!=null) {
			dvd = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdUid);
			if (dvd!=null && dvd.getDvdParentOperationUid()!=null) {
				parentDvdOperationUid += ("".equals(parentDvdOperationUid)?"":",") + "'" + dvd.getDvdParentOperationUid() + "'";
				this.getParentDvdOperationUid(dvd.getDvdParentOperationUid(),parentDvdOperationUid);
			}
		}		
		return parentDvdOperationUid;
	}	
	
	public String getMaxDeviationDepth(UserSelectionSnapshot userSelection) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM SurveyReference WHERE (isDeleted is null or isDeleted = false) and wellboreUid =:thisWellboreUid and ((isPlanned is null) or (isPlanned = false))";
		String[] paramsFields = {"thisWellboreUid"};
		Object[] paramsValues = {userSelection.getWellboreUid()};
		
		Double maxInclinationAngle = null;
		Double maxDepth = null;
		String Output = "";
		List<SurveyReference> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		for (SurveyReference surveyReference : lstResult) {
			String surveyReferenceUid = surveyReference.getSurveyReferenceUid();
			String strSql2 = "FROM SurveyStation WHERE (isDeleted is null or isDeleted = false) and surveyReferenceUid =:surveyReferenceUid";
			String[] paramsFields2 = {"surveyReferenceUid"};
			Object[] paramsValues2 = {surveyReferenceUid};
			List<SurveyStation> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
			for (SurveyStation surveyStation : lstResult2) {
				if (maxInclinationAngle==null)
				{
					maxInclinationAngle = surveyStation.getInclinationAngle();
					maxDepth = surveyStation.getDepthMdMsl();
				}else{
					Double incl = 0.0;
					if (surveyStation.getInclinationAngle() !=null) incl = surveyStation.getInclinationAngle();
					if (maxInclinationAngle < incl){
						maxInclinationAngle = incl;
						maxDepth = surveyStation.getDepthMdMsl();
					}else{
						if (maxInclinationAngle.equals(incl)){
							if (maxDepth < surveyStation.getDepthMdMsl()){
								maxDepth = surveyStation.getDepthMdMsl();
							}	
						}
					}
				}
			}
		}	
		if (maxInclinationAngle !=null) {
			CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale() , SurveyStation.class, "inclinationAngle");
			thisConverter.setBaseValue(maxInclinationAngle);
			Output = thisConverter.formatOutputPrecision() + thisConverter.getUomSymbol();
			Output = Output + " @ ";
			thisConverter = new CustomFieldUom(userSelection.getLocale(), SurveyStation.class, "depthMdMsl");
			thisConverter.setBaseValue(maxDepth);
			Output = Output + thisConverter.formatOutputPrecision() + thisConverter.getUomSymbol();
		}
		
		return Output;
		
	}	
	
	/**
	 * Method to calculate duration between 2 datetime value; special handling for 23:59:59
	 * @param startDatetime 
	 * @param endDatetime 
	 * @param duration in seconds 
	 * @throws Exception
	 */
	public Long calculateDuration(Date startTime, Date endTime) throws Exception {
		
		if(startTime != null && endTime != null){
			
			boolean needToAddBack = false;
			boolean needToMinus = false;
			
			//CALCULATE DURATION		
						
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(endTime);
			
			if (thisCalendar.get(Calendar.SECOND) == 59) { // if end is 23:59:59
				needToAddBack = true;
			}
			
			Calendar thisStartCalendar = Calendar.getInstance();
			thisStartCalendar.setTime(startTime);
			if (thisStartCalendar.get(Calendar.SECOND) == 59) { // if start is 23:59:59
				needToMinus = true;				
			}
			
			Long duration = (endTime.getTime() - startTime.getTime()) / 1000;
			
			//add back 1 second if case of end 23:59:59
			if (needToAddBack) duration = duration + 1;
			if (needToMinus) duration = duration - 1;
			
			return duration;

		}
		return null;
	}
	
	
	public Double getAvgValue(Double maxValue, Double minValue) throws Exception {
		
		Double avgValue = null;
		if (maxValue !=null && minValue !=null) {
			avgValue = (maxValue + minValue) /2;
		}else {
			if (maxValue !=null) avgValue = maxValue;
			else if (minValue !=null) avgValue = minValue;
			else avgValue = null;
		}
		
		return avgValue;
	}
	
	public void updateActivityDate(ReportDaily rd) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Activity> actList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Activity Where (isDeleted is null or isDeleted = false) and dailyUid=:dailyUid","dailyUid",rd.getDailyUid(),qp);
		for (Activity act: actList)
		{
			act.setStartDatetime(setDate(act.getStartDatetime(),rd.getReportDatetime()));
			act.setEndDatetime(setDate(act.getEndDatetime(),rd.getReportDatetime()));
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(act, qp);
		}
	}
	
	public Date setDate(Date currentDateTime, Date newDate)
	{
		Calendar calNew = Calendar.getInstance();
		calNew.setTime(newDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDateTime);
		cal.set(Calendar.YEAR, calNew.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, calNew.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, calNew.get(Calendar.DAY_OF_MONTH));
		return cal.getTime();
		
	}
	
	/**
	 * Method to read the defined pattern and format it accordingly  
	 * @param pattern 
	 * @param userSelection
	 * @param operation
	 * @param gmtOffset
	 * @throws Exception
	 */
	public String formatDisplayPrefix(String pattern, UserSelectionSnapshot userSelection, Operation operation, Daily daily, Double gmtOffset) throws Exception {
		
		Pattern name_pattern = Pattern.compile("\\$\\{.*?\\}"); 
		
		Matcher m = name_pattern.matcher(pattern);
		StringBuffer sb = new StringBuffer();
		 
		while (m.find()) {
			String key = m.group().substring(2, m.group().length() - 1);
			String value = parseDisplayPrefixParam(key, userSelection, operation, daily, gmtOffset);
			if(value != null) m.appendReplacement(sb, value.replace("\\", "\\\\").replace("$", "\\$"));
		}
		m.appendTail(sb);
		 
		return sb.toString();
	}
	
	/**
	 * Method to get value for formatDisplayPrefix method
	 * @param key 
	 * @param userSelection 
	 * @param operation 
	 * @param gmtOffset
	 * @throws Exception
	 */
	public String parseDisplayPrefixParam(String key, UserSelectionSnapshot userSelection, Operation operation, Daily daily, Double gmtOffset) throws Exception {
		
		if ("well".equalsIgnoreCase(key)) {
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
			if (well !=null) return null2EmptyString(well.getWellName());
			
		}else if ("date".equalsIgnoreCase(key)) {
			if(daily==null) daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid()); 
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			return null2EmptyString(dateFormat.format(daily.getDayDate()));
			
		}else if ("currentDate".equalsIgnoreCase(key)){
			Date currentDate = new Date();
			if (gmtOffset !=null) {
				currentDate = this.currentDateTimeWithGMTOffset(operation.getGroupUid(), gmtOffset);
			}else {
				currentDate = this.currentDateTime(operation.getGroupUid(),operation.getWellUid());
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return null2EmptyString(dateFormat.format(currentDate));
			
		}else if ("operationType".equalsIgnoreCase(key)){
			return null2EmptyString(operation.getOperationCode());
			
		}else if ("operationCode".equalsIgnoreCase(key)){
			return null2EmptyString(operation.getOperationalCode());
		}
		
		return null;
	
	}
	
	/**
	 * Method to set an Alternative value to Dynamic Attribute with Alternative Unit
	 * @param node 
	 * @param clazz 
	 * @param propertyName 
	 * @param unitId New Unit Id
	 * @param precision New Precision
	 * @param dynAttrName Dynamic Attribute Name to be assigned
	 * @param userValue 
	 * @throws Exception
	 */
	public void setAlternativeUnitAsDynAttr(CommandBeanTreeNode node, Class clazz, String propertyName, String unitId, Integer precision, String dynAttrName, Double userValue) throws Exception
	{
		if (userValue !=null)
		{
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), clazz, propertyName);
			thisConverter.setBaseValueFromUserValue(userValue);
			thisConverter.changeUOMUnit(unitId,precision);
			node.setCustomUOM("@"+dynAttrName, thisConverter.getUOMMapping());
			node.getDynaAttr().put(dynAttrName, thisConverter.formatOutputPrecision());
		}
	}
	
	 /* Auto get the current date base on default GMP setting (GWP) or well GMT setting
	 * @param String groupUid, Double gmtOffset
	 * @return Date
	 * @throws Exception
	 */
	public Date currentDateTimeWithGMTOffset(String groupUid, Double gmtOffset) throws Exception {
		Date currentDate =  new Date();
		Double thisOffset = 0.0;
		
		thisOffset = Double.parseDouble(GroupWidePreference.getValue(groupUid, "mainOfficeGMTZone"));
		
		if (gmtOffset!=null) thisOffset = gmtOffset;
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(currentDate);
		thisCalendar.add(Calendar.HOUR_OF_DAY, (int) Math.round(thisOffset));
		return thisCalendar.getTime();
	}
	
	public static String nullSafeToString(Object value){
		if(value == null) return null;
		return value.toString();
	}
	
	/**
	 * Static Method to set primary ket of object to null
	 * @param clazz
	 * @param object
	 * @throws Exception
	 */
	public static void setPrimaryKeyToNull(Class clazz, Object object) throws Exception {
		setPrimaryKey(clazz,object,null);
	}
	
	/**
	 * static method to get the primary key value from the object
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static String getPrimaryKey(Object object) throws Exception
	{
		String primaryKey = ApplicationUtils.getConfiguredInstance().getIdentifierPropertyName(object.getClass());
		return PropertyUtils.getSimpleProperty(object, primaryKey).toString();
	}
	/**
	 * static method to set primary key value for the object with the value provided
	 * @param clazz
	 * @param object
	 * @param primaryKeyValue
	 * @throws Exception
	 */
	public static void setPrimaryKey(Class clazz, Object object, String primaryKeyValue) throws Exception {
		String primaryKey = ApplicationUtils.getConfiguredInstance().getIdentifierPropertyName(clazz);
		PropertyUtils.setSimpleProperty(object, primaryKey, primaryKeyValue);
	}
	
	/**
	 * get daily report generated for email attachment
	 * @param operationUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public String[] getDailyReportGenerated(String operationUid, String dailyUid) throws Exception{
		String[] selectReportFile = new String[2];
		String thisReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		String[] paramsFields = {"thisOperation", "reportFilesReportType", "dailyUid"};
		String[] paramsValues = {operationUid, thisReportType, dailyUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rf.reportType=:reportFilesReportType and rf.dailyUid=:dailyUid ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();

				ReportFiles thisReportFile = (ReportFiles) obj_array[0];
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());				
			  
				if(report_file.exists()){
					selectReportFile[0] = thisReportFile.getDisplayName() + "." + getFileExtension(thisReportFile.getReportFile()) ;
					selectReportFile[1] = ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile().toString();
					return selectReportFile;
				}
			}
		}
		return null;
		
	}
	/**
	 * get file extension
	 * @param file
	 * @throws Exception
	 */
	public String getFileExtension(String file){
		int i = file.lastIndexOf(".");
		if(i != -1){
			return file.substring(i + 1);
		}else{
			return null;
		}
	}
	/**
	 * Returns a list of dynamic attribute names that are defined in the form, and starts with the given prefix
	 * @param commandBean
	 * @param node
	 * @param request
	 * @param prefix
	 * @param className
	 * @return a list of result<String>
	 */
	public Set<String> getDynaAttrNames(CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request, String prefix, String className) {
		Set<String> results = new HashSet<String>();
		try {
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT) {
				// there is no request parameter from DEPOT, so using node.getDynaAttr() instead of request.getParameterNames();
				for (Map.Entry<String, Object> entry : node.getDynaAttr().entrySet()) {
					String dynaAttrName = "@" + entry.getKey();
					if (dynaAttrName.startsWith(prefix)) {
						results.add(dynaAttrName);
					}
				}
			} else {
				// get binded dynamic attribute names from request
				for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
					String paramName = (String) e.nextElement();
					int index = paramName.indexOf(".dynaAttr[");
					if (index > 0) {
						String dynaAttrName = "@" + paramName.substring(index + 10, paramName.indexOf(']', index));
						if (dynaAttrName.startsWith(prefix)) {
							results.add(dynaAttrName);
						}
					}
				}
			}
			
			// when flex client is in use, only modified dynamic attributes are binded, so we need to get them from FlexClientAdaptorForCommandBean
			Set<String> dynaAttrNames = ((BaseCommandBean) commandBean).getFlexClientAdaptor().getDefinedFieldNames(className);
			if (dynaAttrNames != null) {
				for (String dynaAttrName : dynaAttrNames) {
					if (dynaAttrName.startsWith(prefix)) {
						results.add(dynaAttrName);
					}
				}
			}
		} catch(Exception e) {}
		return results;
	}
	/**
	 * Method to convert string value to double value 
	 * @param text
	 * @param locale
	 * @return result in double
	 */
	public double toDouble(String text, Locale locale) {
		if (StringUtils.isNotBlank(text)) {
			try {
				return (Double) NumberUtils.parseNumber(text, Double.class, NumberFormat.getInstance(locale));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	/**
	 * Method used to set property when getting value from dynamic attribute (String to respective type)
	 * @param locale
	 * @param bean
	 * @param name
	 * @param value
	 */
	public void setProperty(Locale locale, Object bean, String name, Object value) throws Exception {
		java.lang.Class type = PropertyUtils.getPropertyType(bean, name);
		if (type != null) {
			if (type == java.util.Date.class) {
				if (value instanceof java.sql.Timestamp) {
					PropertyUtils.setProperty(bean, name, new Date(((java.sql.Timestamp) value).getTime()));
				} else if (value instanceof java.lang.String) {
					if (StringUtils.isNotBlank((String) value)) {
						PropertyUtils.setProperty(bean, name, CommonDateParser.parse((String) value));
					} else {
						PropertyUtils.setProperty(bean, name, null);
					}
				} else {
					PropertyUtils.setProperty(bean, name, value);
				}
			} else if (type == java.lang.Double.class) {
				if (value instanceof java.lang.String) {
					if (StringUtils.isNotBlank((String) value)) {
						PropertyUtils.setProperty(bean, name, toDouble((String) value, locale));
					} else {
						PropertyUtils.setProperty(bean, name, null);
					}
				} else {
					PropertyUtils.setProperty(bean, name, value);
				}
			} else if (type == java.lang.Boolean.class) {
				if (value instanceof java.lang.String) {
					if (StringUtils.isNotBlank((String) value)) {
						if ("1".equals(value)) value = "true";
						PropertyUtils.setProperty(bean, name, Boolean.valueOf((String) value));
					} else {
						PropertyUtils.setProperty(bean, name, null);
					}
				} else {
					PropertyUtils.setProperty(bean, name, value);
				}
			} else {
				PropertyUtils.setProperty(bean, name, value);
			}
		}
	}
	
	/**
	 * Method used to handle how the field property should be set after copying from another object 
	 * set null for excluded fields (if any), convert value with uom and datum, set primary key to null for new record
	 * @param commandBean
	 * @param clazz
	 * @param bean
	 * @param thisConverter
	 * @param excludedFields 
	 */
	public void setPropertyForNewCopiedRecord(CommandBean commandBean, Class clazz, Object bean, CustomFieldUom thisConverter, List <String> excludedFields) throws Exception {
		Map<String, Object> beanFields = PropertyUtils.describe(bean);
		for (Map.Entry entry : beanFields.entrySet()) 
		{	
			if (excludedFields !=null && excludedFields.contains(entry.getKey().toString())){	
				if (PropertyUtils.isWriteable(bean, entry.getKey().toString()))
					PropertyUtils.setProperty(bean, entry.getKey().toString(), null);
			}else {
				java.lang.Class type = PropertyUtils.getPropertyType(bean, entry.getKey().toString());
				if (type != null) {					
					Object value = PropertyUtils.getProperty(bean, entry.getKey().toString());
					if (value!=null) {
						//if the type is Double 
						if (type == java.lang.Double.class) {						
									
							thisConverter.setReferenceMappingField(clazz, entry.getKey().toString());
							if (thisConverter.isUOMMappingAvailable()) {
								thisConverter.setBaseValueFromUserValue((Double)value);
								//add datum offset if it is datum related field
								if (thisConverter.getUOMMapping().isDatumConversion()) {
									thisConverter.addDatumOffset();						
								}
								
								//set value as converted value 
								if (PropertyUtils.isWriteable(bean, entry.getKey().toString()))
									PropertyUtils.setProperty(bean, entry.getKey().toString(), thisConverter.getConvertedValue());							
							}
												
						}else{ // not double type
							if (PropertyUtils.isWriteable(bean, entry.getKey().toString()))
								PropertyUtils.setProperty(bean, entry.getKey().toString(), value);				
						}
					}else { // if value = null
						if (PropertyUtils.isWriteable(bean, entry.getKey().toString()))
							PropertyUtils.setProperty(bean, entry.getKey().toString(), null);
					}
				}
			}
		}
		//set primaryKey to null for new record
		CommonUtil.setPrimaryKeyToNull(clazz, bean);
	}
	
	public Boolean isStartGreaterEndDate(Date start, Date end) throws Exception { 
		if(start != null && end != null){
			if(start.getTime() > end.getTime()){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Method to get previous/last operation end date
	 * @param wellboreUid
	 * @throws Exception
	 * @return dailyUid
	 */
	public String getPreviousOperationEndDate(String wellboreUid) throws Exception{	
		String strSql = "SELECT d.dailyUid from Operation op, Daily d WHERE (op.isDeleted = false or op.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) AND op.operationUid = d.operationUid AND op.wellboreUid = :wellboreUid order by d.dayDate DESC";
		String[] paramsFields = {"wellboreUid"};
		Object[] paramsValues = {wellboreUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object daily = (Object) lstResult.get(0);
			if (daily != null && StringUtils.isNotBlank(daily.toString()))
				return daily.toString();
		}
		return null;
	}
	
	/**
	 * Method to calculate pump stroke volume
	 * @param pumpType, linerDiam, rodDiam, strokeLength, efficiency
	 * @throws Exception
	 * @return vps
	 */
	public Double calculateStrokeVolume(String pumpType, Double linerDiam, Double rodDiam, Double strokeLength, Double efficiency) throws Exception {
		Double vps = 0.00; // volume per stroke
		// calculate vps base on pump type (either Duplex or Triplex)
		if ("triplex".equals(pumpType)) {
			vps = 0.000243 * Math.pow(linerDiam, 2) * strokeLength * efficiency;
		} else if ("duplex".equals(pumpType)) {
			vps = 0.000162 * strokeLength * ((2 * Math.pow(linerDiam, 2)) - Math.pow(rodDiam, 2)) * efficiency;
		}
		return vps;
	}
	
	public Double calculateCumulativeDowntime(String dailyUid, String reportType) throws Exception {
		Double cumOpsDowntimeDuration = null;
		if (StringUtils.isBlank(dailyUid)) return null;
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			if (daily.getDayDate()!=null) {
				
				List dailyList = new ArrayList();
				
				//collect all dailyUids
				String queryString = "SELECT dailyUid FROM Daily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND operationUid=:operationUid " +
						"AND dayDate<=:dayDate ";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"operationUid", "dayDate"}, 
						new Object[] {daily.getOperationUid(), daily.getDayDate()});
				for (Object rec : list) {
					dailyList.add(rec.toString());
				}
				
				Map<String, Double> reportDailyNptHrs = new HashMap<String, Double>();

				//Collect all available dailyNptHrs in ReportDaily
				if (reportType==null) reportType = this.getReportTypePriorityWhenDrllgNotExist(daily.getOperationUid());
				queryString = "SELECT dailyUid, sum(dailyNptHrs) FROM ReportDaily " +
						"WHERE (isDeleted=false or isDeleted is null) " +
						"AND dailyUid in (:dailyUid) " +
						"AND reportType=:reportType " +
						"Group By dailyUid";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[] {"dailyUid", "reportType"}, 
						new Object[] {dailyList, reportType});
				for (Object result : list) {
					Object[] rec = (Object[]) result;
					if (rec[0]!=null && rec[1]!=null) {
						reportDailyNptHrs.put(rec[0].toString(), Double.parseDouble(rec[1].toString()));
					}
				}
				
				for (Object rec : dailyList) {
					String thisDailyUid = rec.toString();
					if (reportDailyNptHrs.containsKey(thisDailyUid)) {
						if (cumOpsDowntimeDuration==null) cumOpsDowntimeDuration = 0.0;
						cumOpsDowntimeDuration += reportDailyNptHrs.get(thisDailyUid);
					}
				}
			}
		}
		
		return cumOpsDowntimeDuration;
	}
	
	/**
	 * Calculate the dry hole duration per day using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @param isDryHoleNpt
	 * @return raw value in second
	 * @throws Exception
	*/
	public Double durationFromDryHolePerDay(String operationUid, String dailyUid, String groupUid, Date spudDate, Date dryHoleEndDate, boolean isDryHoleNpt) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql;

		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (spudDate != null){
				
			Date getSpudDateTime = spudDate;
				
			//GEt on start dry hole date in date only
			Calendar thisSpudDateTimeObj = Calendar.getInstance();
			Calendar thisSpudDateObj = Calendar.getInstance();
			
			thisSpudDateTimeObj.setTime(getSpudDateTime);
			
			thisSpudDateObj.set(Calendar.YEAR, thisSpudDateTimeObj.get(Calendar.YEAR));
			thisSpudDateObj.set(Calendar.MONTH, thisSpudDateTimeObj.get(Calendar.MONTH));
			thisSpudDateObj.set(Calendar.DAY_OF_MONTH, thisSpudDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisSpudDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisSpudDateObj.set(Calendar.MINUTE, 0);
			thisSpudDateObj.set(Calendar.SECOND, 0);
			thisSpudDateObj.set(Calendar.MILLISECOND, 0);
		
			//this hold the days from on location figure
			Double totalDays = null;
			
			//if the date is after the dry hole start date, then do the calc
			if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) >= 0)
			{
				if (dryHoleEndDate != null)
				{	
					Date getDryHoleEndDateTime = dryHoleEndDate;				
					
					//GEt end dry hole date in date only
					Calendar thisDryHoleEndDateTimeObj = Calendar.getInstance();
					Calendar thisDryHoleEndDateObj = Calendar.getInstance();
					
					thisDryHoleEndDateTimeObj.setTime(getDryHoleEndDateTime);
					
					thisDryHoleEndDateObj.set(Calendar.YEAR, thisDryHoleEndDateTimeObj.get(Calendar.YEAR));
					thisDryHoleEndDateObj.set(Calendar.MONTH, thisDryHoleEndDateTimeObj.get(Calendar.MONTH));
					thisDryHoleEndDateObj.set(Calendar.DAY_OF_MONTH, thisDryHoleEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
					thisDryHoleEndDateObj.set(Calendar.HOUR_OF_DAY, 0);
					thisDryHoleEndDateObj.set(Calendar.MINUTE, 0);
					thisDryHoleEndDateObj.set(Calendar.SECOND, 0);
					thisDryHoleEndDateObj.set(Calendar.MILLISECOND, 0);
					
					//if date is after dry hole end date
					if (selectedDaily.getDayDate().compareTo(thisDryHoleEndDateObj.getTime()) >= 0)
					{
						//If Current date is the same as dry hole end date
						if (selectedDaily.getDayDate().compareTo(thisDryHoleEndDateObj.getTime()) == 0) {
							strSql = "SELECT a FROM Daily d, Activity a " +
									"WHERE (d.isDeleted = false or d.isDeleted is null) " +
									"and (a.isDeleted = false or a.isDeleted is null) " +
									"and (a.dayPlus is null or a.dayPlus = 0) " +
									"and a.dailyUid = d.dailyUid " +
									"AND d.dayDate = :dryHoleDate " +
									"AND d.operationUid = :thisOperationUid " +
									(isDryHoleNpt ? " AND a.internalClassCode in ('TP', 'TU')" : "") +
									"AND (a.isSimop=false or a.isSimop is null) " +
									"AND (a.isOffline=false or a.isOffline is null)";
							String[] paramsFields3 = {"dryHoleDate", "thisOperationUid"};
							Object[] paramsValues3 = {thisDryHoleEndDateObj.getTime(), operationUid};
							
							List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
							if (lstResult.size() > 0){
								totalDays = 0.0;
								for(Object objActivity: lstResult){
									Activity thisActivity = (Activity) objActivity;
									
									//IF DRY HOLE DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
									if (thisActivity.getDayPlus()!=null){
										if (thisActivity.getDayPlus()==1) {
											if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
										} else {
											if ((thisActivity.getStartDatetime().getTime() < getDryHoleEndDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() <= getDryHoleEndDateTime.getTime())){
												if(thisActivity.getActivityDuration() != null)
													totalDays = totalDays + thisActivity.getActivityDuration();
											}
											else if ((thisActivity.getStartDatetime().getTime() < getDryHoleEndDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > getDryHoleEndDateTime.getTime())){
												Long startTime = thisActivity.getStartDatetime().getTime();
												Long dryHoleEndTime = getDryHoleEndDateTime.getTime();
												totalDays = totalDays + ((dryHoleEndTime - startTime)/1000);
											}
										}
									}
								}
							}
						}
					} else {
						//sum all hours for current date
						strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (d.isDeleted = false or d.isDeleted is null) " +
								"and (a.isDeleted = false or a.isDeleted is null) " +
								"and (a.dayPlus is null or a.dayPlus = 0) " +
								"and a.dailyUid = d.dailyUid " +
								"and a.dailyUid = :thisDailyUid " +
								"AND d.operationUid = :thisOperationUid " +
								(isDryHoleNpt ? " AND a.internalClassCode in ('TP', 'TU')" : "") +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null)";
						String[] paramsFields1 = {"thisDailyUid", "thisOperationUid"};
						Object[] paramsValues1 = {dailyUid, operationUid};
						
						List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
						
						
						Object a = (Object) lstResult1.get(0);
						if (a != null) totalDays = Double.parseDouble(a.toString());
					}
				}
				
				//get duration portion on the start dry hole date it self
				if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) == 0) {
					Calendar thisDryHoleStartTimeObj = Calendar.getInstance();
					thisDryHoleStartTimeObj.setTimeInMillis(0);	
					thisDryHoleStartTimeObj.set(Calendar.HOUR_OF_DAY, thisSpudDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisDryHoleStartTimeObj.set(Calendar.MINUTE, thisSpudDateTimeObj.get(Calendar.MINUTE));
					
					totalDays = 0.0;
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :dryHoleDate " +
							"AND d.operationUid = :thisOperationUid " +
							(isDryHoleNpt ? " AND a.internalClassCode in ('TP', 'TU')" : "") +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"dryHoleDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisSpudDateObj.getTime(), operationUid};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF DRY HOLE DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDays = totalDays + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > getSpudDateTime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDays = totalDays + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() >= getSpudDateTime.getTime())){
										Long endTime = thisActivity.getEndDatetime().getTime();
										Long dryHoleStartTime = getSpudDateTime.getTime();
										totalDays = totalDays + ((endTime - dryHoleStartTime)/1000);
									}
								}
							}
						}
					}
				}
			}
			return totalDays;
		}

		return 0.0;
	}
	
	public void createDvdSummary(String operationUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
	    qp.setUomConversionEnabled(false);
	    qp.setDatumConversionEnabled(false);
	    
	    if(operationUid == null) return;
	    
	    Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
	    List<Object[]> summaryList = new ArrayList<Object[]>();
	    String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
	    String strSql = null;
	    List dailyLists = null;
	    
	    if(dailyUid != null){
	    	strSql = "FROM ReportDaily where (isDeleted = FALSE OR isDeleted IS NULL) " +
					 "AND reportType = :reportType " +
					 "AND dailyUid = :dailyUid";
			String[] paramsFields = {"reportType","dailyUid"};
			Object[] paramsValues = {reportType, dailyUid};
			dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
	    }else{
	    	strSql = "FROM ReportDaily where (isDeleted = FALSE OR isDeleted IS NULL) " +
					 "AND operationUid = :operationUid " +
					 "AND reportType = :reportType " +
					 "ORDER BY reportDatetime";
			String[] paramsFields = {"operationUid","reportType"};
			Object[] paramsValues = {operationUid,reportType};
			dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
	    }
	    
	    if(!dailyLists.isEmpty()){			
			for(Iterator i=dailyLists.iterator(); i.hasNext();){
				ReportDaily reportDaily = (ReportDaily)i.next();
				String thisdailyUid = reportDaily.getDailyUid();
				
				strSql = "SELECT activityDuration, depthMdMsl, classCode, endDatetime FROM Activity " +
						 "WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
						 "AND dailyUid = :dailyUid " +
						 "AND operationUid = :operationUid " +
						 "AND ((dayPlus IS NULL OR dayPlus=0) AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
						 "ORDER BY dayPlus, endDatetime";
				String[] paramsFields2 = {"dailyUid", "operationUid"};
				Object[] paramsValues2 = {thisdailyUid, operationUid};
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				if (!lstResult.isEmpty()){			
					Double cumDuration = 0.0;
					boolean firstResult = true;
					int loopCount = 0;
					int sameCount = 0;
					Date tempEndDatetime = null;
					
					for(Object objResult: lstResult){
						loopCount++;
						
						Object[] activity = (Object[]) objResult;
						
						Object[] point = new Object[5]; // 0 = duration, 1 = depth, 2 = classCode, 3 = endDatetime, 4 = dailyUid 
						
						Double duration = null;
						Double depthMdMsl = null;
						String classCode = null;
						Date endDatetime = null;
						Integer action = null;
						
						if (activity[0] != null && StringUtils.isNotBlank(activity[0].toString())){
							duration = Double.parseDouble(activity[0].toString());
						}
						
						if (activity[1] != null && StringUtils.isNotBlank(activity[1].toString())){
							depthMdMsl = Double.parseDouble(activity[1].toString());
						}
						
						if (activity[2] != null && StringUtils.isNotBlank(activity[2].toString())){
							classCode = activity[2].toString();
						}
						
						if (activity[3] != null && StringUtils.isNotBlank(activity[3].toString())){
							endDatetime = (Date) activity[3];
						}
						
						if(firstResult){
							point[0] = duration;
							point[1] = depthMdMsl;
							point[2] = classCode;
							point[3] = endDatetime;
							point[4] = reportDaily.getDailyUid();
							// Add point
							summaryList.add(point);
							firstResult = false;
						}else{
							Object[] lastRecord = summaryList.get(summaryList.size() - 1);
							Double lastDepth = null;
							String lastClassCode = null;
							
							if(lastRecord[1] != null){
								lastDepth = Double.parseDouble(lastRecord[1].toString());
							}
							
							if(lastRecord[2] != null){
								lastClassCode = lastRecord[2].toString();
							}
							
							if(classCode == null && depthMdMsl == null){
								if(lastClassCode == null && lastDepth == null){
									action = 1;
								}else{
									if(sameCount != 0){
										action = 2;
									}
								}
							}else{
								if(classCode != null && depthMdMsl == null){
									if(lastClassCode != null && lastDepth ==null){
										if(classCode.equals(lastClassCode)){
											action = 1;
										}else{
											if(sameCount != 0){
												action = 2;
											}
										}
									}else{
										if(sameCount != 0){
											action = 2;
										}
									}
								}else if(classCode == null && depthMdMsl != null){
									if(lastClassCode == null && lastDepth != null){
										if(depthMdMsl.equals(lastDepth)){
											action = 1;
										}else{
											if(sameCount != 0){
												action = 2;
											}
										}
									}else{
										if(sameCount != 0){
											action = 2;
										}
									}
								}else{
									if(depthMdMsl.equals(lastDepth) && classCode.equals(lastClassCode)){
										action = 1;
									}else{
										if(sameCount != 0){
											action = 2;
										}
									}
								}
							}
							
							if(action != null){
								if(action.equals(1)){
									if(duration != null){
										cumDuration += duration;
									}
									tempEndDatetime = endDatetime;
									sameCount++;
								}
							}
							
							if(loopCount == lstResult.size()){
								if(sameCount > 0){
									if(action.equals(1)){
										action = 3;
									}
								}else{
									action = null;
								}
							}
							
							if(action == null){
								point[0] = duration;
								point[1] = depthMdMsl;
								point[2] = classCode;
								point[3] = endDatetime;
								point[4] = reportDaily.getDailyUid();
								// Add point
								summaryList.add(point);
							}else if(action.equals(2)){
								Object[] previousPoint = new Object[5];
								previousPoint[0] = cumDuration;
								previousPoint[1] = lastDepth;
								previousPoint[2] = lastClassCode;
								previousPoint[3] = tempEndDatetime;
								previousPoint[4] = reportDaily.getDailyUid();
								// Add point
								summaryList.add(previousPoint);

								sameCount = 0;
								cumDuration = 0.0;
								tempEndDatetime = null;
																		
								point[0] = duration;
								point[1] = depthMdMsl;
								point[2] = classCode;
								point[3] = endDatetime;
								point[4] = reportDaily.getDailyUid();
								// Add point
								summaryList.add(point);
							}else if(action.equals(3)){
								point[0] = cumDuration;
								point[1] = lastDepth;
								point[2] = lastClassCode;
								point[3] = tempEndDatetime;
								point[4] = reportDaily.getDailyUid();
								// Add point
								summaryList.add(point);
							}
						}
					}
				}
			}
			
			if(!summaryList.isEmpty()){
				for(Object objResult: summaryList){
					Object[] point = (Object[]) objResult;
					Double tempDuration = null;
					Double tempDepth = null;
					String tempCode = null;
					Date tempEndDatetime = null;
					String tempDailyUid = null;
					
					if(point[0] != null){
						tempDuration = Double.parseDouble(point[0].toString());
					}
					
					if(point[1] != null){
						tempDepth = Double.parseDouble(point[1].toString());
					}
					
					if(point[2] != null){
						tempCode = point[2].toString();
					}
					
					if(point[3] != null){
						tempEndDatetime = (Date) point[3];
					}
					
					if(point[4] != null){
						tempDailyUid = point[4].toString();
					}
					
					DvdSummary dvdSummary = new DvdSummary();
					dvdSummary.setWellUid(operation.getWellUid());
					dvdSummary.setWellboreUid(operation.getWellboreUid());
					dvdSummary.setOperationUid(operation.getOperationUid());
					dvdSummary.setDailyUid(tempDailyUid);
					dvdSummary.setClassCode(tempCode);
					dvdSummary.setDepthMdMsl(tempDepth);
					dvdSummary.setActivityDuration(tempDuration);
					dvdSummary.setEndDatetime(tempEndDatetime);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dvdSummary, qp);						
				}
			}
		}
	}
	
	public void deleteExistingDvdSummary(String type, String uid) throws Exception{
		String uidType = null;
		if(("well").equals(type)){
			uidType = "wellUid";
		}else if(("wellbore").equals(type)){
			uidType = "wellboreUid";
		}else if(("operation").equals(type)){
			uidType = "operationUid";
		}else if(("daily").equals(type)){
			uidType = "dailyUid";
		}
		
		String strSql = "SELECT dvdSummaryUid FROM DvdSummary " +
						"WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
						"AND " + uidType + " = :uid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "uid", uid);

		if(!lstResult.isEmpty()){
			strSql = "Update DvdSummary SET isDeleted = TRUE, lastEditDatetime = :lastEditDatetime WHERE " + uidType + " = :uid AND (isDeleted IS FALSE OR isDeleted IS NULL)";
			String[] paramsFields = {"lastEditDatetime", "uid"};
			Object[] paramsValues = {new Date(), uid};
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
		}
	}
	
	public String getCountryFullName(String wellsname) throws Exception{
		String wellfname1 = null;
			String sql= "SELECT lookupLabel FROM CommonLookup WHERE lookupTypeSelection = 'country' AND shortCode =:wellsname";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "wellsname", wellsname);
			if(lstResult.size()>0)
				wellfname1 = (String) lstResult.get(0);
			return wellfname1;
	}
	
	public void updateDailyMonthlyCurrencyKey(String dailyUid, UOMMapping mapping) throws Exception {  

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		
		String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:thisDailyUid";
		String[] paramsFields = {"thisDailyUid"};
		Object[] paramsValues = {dailyUid};
		List<Daily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		for (Daily dailyRec : lstResult) {
			String monthlyCurrencyKey = sdf.format(dailyRec.getDayDate()) + mapping.getUnitShortHand();
			dailyRec.setMonthlyCurrencyKey(monthlyCurrencyKey);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dailyRec);
		}
	}
	
	/**
	 * 
	 * Port tour stamping from TNP
	 * 
	 **/
	public String tourAssignmentBasedOnTime(UserSession session, Date datetime, Boolean isActiveOnly) throws Exception{
		String rigTourUid = "";
		int time = convertToTimeInt (datetime);
		String include="";

		String riginformationUid = session.getCurrentRigInformationUid();
		ArrayList pF = new ArrayList(); 
		ArrayList pV = new ArrayList(); 

		pF.add("rigInformationUid");
		pV.add(riginformationUid);
		
		String[] paramsFields = (String[]) pF.toArray(new String [pF.size()]);
		Object[] paramsValues = pV.toArray();
		
		String strSql = "FROM RigTour WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND rigInformationUid=:rigInformationUid " + 
						include +
						"ORDER BY tourStartDateTime ASC";
		List<RigTour> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if(ls.size()>0){
			
			for(RigTour thisRigTour : ls){
				if(thisRigTour.getTourStartDateTime()!=null && thisRigTour.getTourEndDateTime()!=null){
					int rigTourStartTime = convertToTimeInt (thisRigTour.getTourStartDateTime()); 
					int rigTourEndTime = convertToTimeInt (thisRigTour.getTourEndDateTime());
					 
					if (rigTourStartTime<=rigTourEndTime) {
						if (rigTourStartTime<=time && time<rigTourEndTime) {
							return thisRigTour.getRigTourUid();
						}
					} else {
						if (rigTourStartTime<=time || time<rigTourEndTime) {
							return thisRigTour.getRigTourUid(); 
						}
					}
				}
			}
		}
		return rigTourUid;
	}
	
	public int convertToTimeInt(Date datetime){
		int time = (int) (datetime.getTime() % (24*60*60*1000L));
		return time;
	}

	/**
	 * FOR NOBLE - get Active Rig Tour Record
	 * @param rigInformationUid
	 * @param reportDailyUid
	 * @return
	 * @throws Exception
	 */
	public List<RigTour> getRigTour(String rigInformationUid, String dailyUid, String operationUid, Boolean isActiveOnly) throws Exception {
		String include="";
		
		ArrayList pF = new ArrayList(); 
		ArrayList pV = new ArrayList(); 

		pF.add("rigInformationUid");
		pV.add(rigInformationUid);
		
		String[] paramsFields = (String[]) pF.toArray(new String [pF.size()]);
		Object[] paramsValues = pV.toArray();
		
		if(rigInformationUid!=null){
			String queryString = "FROM RigTour WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid " + include +" ORDER BY sequence, tourStartDateTime";
			List<RigTour> rigTourList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsFields, paramsValues);
			return rigTourList;
		}
		return null;
	}
	
	public Date stampTimeFieldWithDate(String dailyUid, Date time) throws Exception {
		if( StringUtils.isBlank( dailyUid ) ) return time;
		Calendar today = Calendar.getInstance();
		Daily selectedDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if( selectedDay == null ) return time;
		
		today.setTime(selectedDay.getDayDate());

		if( time != null ){
			Calendar datetime = Calendar.getInstance();
			datetime.setTime(time);
			datetime.set(Calendar.YEAR, today.get(Calendar.YEAR));
			datetime.set(Calendar.MONTH, today.get(Calendar.MONTH));
			datetime.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH));
			return datetime.getTime();
		}
		return null;
	}
	
	/**
	 * Calculate spud to TD duration using activity duration.
	 * @param operationUid
	 * @param dailyUid
	 * @return raw value in second
	 * @throws Exception
	*/
	public Double spudToTdDurationDaily(String operationUid, String dailyUid, String groupUid, Date spudDate, Date tdDateTime) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql;

		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (spudDate != null){
				
			Date getSpudDateTime = spudDate;
				
			//Get spud date 
			Calendar thisSpudDateTimeObj = Calendar.getInstance();
			Calendar thisSpudDateObj = Calendar.getInstance();
			
			thisSpudDateTimeObj.setTime(getSpudDateTime);
			
			thisSpudDateObj.set(Calendar.YEAR, thisSpudDateTimeObj.get(Calendar.YEAR));
			thisSpudDateObj.set(Calendar.MONTH, thisSpudDateTimeObj.get(Calendar.MONTH));
			thisSpudDateObj.set(Calendar.DAY_OF_MONTH, thisSpudDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisSpudDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisSpudDateObj.set(Calendar.MINUTE, 0);
			thisSpudDateObj.set(Calendar.SECOND, 0);
			thisSpudDateObj.set(Calendar.MILLISECOND, 0);
		
			//this hold the total duration from spud to td figure
			Double totalDuration = null;
			
			//if the date is after the spud date, then do the calc
			if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) >= 0)
			{
				if (tdDateTime != null)
				{	
					//GEt TD date time in date only
					Calendar thisTdDateTimeObj = Calendar.getInstance();
					Calendar thisTdDateObj = Calendar.getInstance();
					
					thisTdDateTimeObj.setTime(tdDateTime);
					
					thisTdDateObj.set(Calendar.YEAR, thisTdDateTimeObj.get(Calendar.YEAR));
					thisTdDateObj.set(Calendar.MONTH, thisTdDateTimeObj.get(Calendar.MONTH));
					thisTdDateObj.set(Calendar.DAY_OF_MONTH, thisTdDateTimeObj.get(Calendar.DAY_OF_MONTH));
					thisTdDateObj.set(Calendar.HOUR_OF_DAY, 0);
					thisTdDateObj.set(Calendar.MINUTE, 0);
					thisTdDateObj.set(Calendar.SECOND, 0);
					thisTdDateObj.set(Calendar.MILLISECOND, 0);
					
					//if selected date is after or equal to td date
					if (selectedDaily.getDayDate().compareTo(thisTdDateObj.getTime()) >= 0)
					{
						//If selected date is the same date as td date date
						if (selectedDaily.getDayDate().compareTo(thisTdDateObj.getTime()) == 0) {
							strSql = "SELECT a FROM Daily d, Activity a " +
									"WHERE (d.isDeleted = false or d.isDeleted is null) " +
									"and (a.isDeleted = false or a.isDeleted is null) " +
									"and (a.dayPlus is null or a.dayPlus = 0) " +
									"and a.dailyUid = d.dailyUid " +
									"AND d.dayDate = :tdDate " +
									"AND d.operationUid = :thisOperationUid " +
									"AND (a.isSimop=false or a.isSimop is null) " +
									"AND (a.isOffline=false or a.isOffline is null)";
							String[] paramsFields3 = {"tdDate", "thisOperationUid"};
							Object[] paramsValues3 = {thisTdDateObj.getTime(), operationUid};
							
							List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
							if (lstResult.size() > 0){
								totalDuration = 0.0;
								for(Object objActivity: lstResult){
									Activity thisActivity = (Activity) objActivity;
									
									//IF TD Date happen ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
									if (thisActivity.getDayPlus()!=null){
										if (thisActivity.getDayPlus()==1) {
											if(thisActivity.getActivityDuration() != null) totalDuration = totalDuration + thisActivity.getActivityDuration();
										} else {
											if ((thisActivity.getStartDatetime().getTime() < tdDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() <= tdDateTime.getTime())){
												if(thisActivity.getActivityDuration() != null)
													totalDuration = totalDuration + thisActivity.getActivityDuration();
											}
											else if ((thisActivity.getStartDatetime().getTime() < tdDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > tdDateTime.getTime())){
												Long startTime = thisActivity.getStartDatetime().getTime();
												Long tdTime = tdDateTime.getTime();
												totalDuration = totalDuration + ((tdTime - startTime)/1000);
											}
										}
									}
								}
							}
						}
					} else {
						//sum all hours for current date
						strSql = "SELECT SUM(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (d.isDeleted = false or d.isDeleted is null) " +
								"and (a.isDeleted = false or a.isDeleted is null) " +
								"and (a.dayPlus is null or a.dayPlus = 0) " +
								"and a.dailyUid = d.dailyUid " +
								"and a.dailyUid = :thisDailyUid " +
								"AND d.operationUid = :thisOperationUid " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null)";
						String[] paramsFields1 = {"thisDailyUid", "thisOperationUid"};
						Object[] paramsValues1 = {dailyUid, operationUid};
						
						List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
						
						
						Object a = (Object) lstResult1.get(0);
						if (a != null) totalDuration = Double.parseDouble(a.toString());
					}
				}
				
				//get duration portion on the spud date it self
				if (selectedDaily.getDayDate().compareTo(thisSpudDateObj.getTime()) == 0) {
					Calendar thisTdDateTimeObj = Calendar.getInstance();
					thisTdDateTimeObj.setTimeInMillis(0);	
					thisTdDateTimeObj.set(Calendar.HOUR_OF_DAY, thisTdDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisTdDateTimeObj.set(Calendar.MINUTE, thisTdDateTimeObj.get(Calendar.MINUTE));
					
					totalDuration = 0.0;
					strSql = "SELECT a FROM Daily d, Activity a " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"and (a.isDeleted = false or a.isDeleted is null) " +
							"and (a.dayPlus is null or a.dayPlus = 0) " +
							"and a.dailyUid = d.dailyUid " +
							"AND d.dayDate = :tdDate " +
							"AND d.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					String[] paramsFields2 = {"tdDate", "thisOperationUid"};
					Object[] paramsValues2 = {thisSpudDateObj.getTime(), operationUid};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
					
					thisSpudDateObj.setTimeInMillis(0);	
					thisSpudDateObj.set(Calendar.HOUR_OF_DAY, thisSpudDateTimeObj.get(Calendar.HOUR_OF_DAY));
					thisSpudDateObj.set(Calendar.MINUTE, thisSpudDateTimeObj.get(Calendar.MINUTE));
					
					if (lstResult.size() > 0){
						for(Object objActivity: lstResult){
							Activity thisActivity = (Activity) objActivity;
							
							//IF TD DATE HAPPEN ON ACTIVITY DAY AND EXIST NEXT DAY ACTIVITY. COUNT ACTIVITY DURATION
							if (thisActivity.getDayPlus()!=null){
								if (thisActivity.getDayPlus()==1) {
									if(thisActivity.getActivityDuration() != null) totalDuration = totalDuration + thisActivity.getActivityDuration();
								} else {
									if ((thisActivity.getStartDatetime().getTime() > getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() > getSpudDateTime.getTime())){
										if(thisActivity.getActivityDuration() != null)
											totalDuration = totalDuration + thisActivity.getActivityDuration();
									}
									else if ((thisActivity.getStartDatetime().getTime() <= getSpudDateTime.getTime()) && (thisActivity.getEndDatetime().getTime() >= getSpudDateTime.getTime())){
										Long startTime = (long) 0;
										Long endTime = (long) 0;
										if (thisActivity.getStartDatetime().getTime() < getSpudDateTime.getTime()) {
											startTime = getSpudDateTime.getTime();
										} else {
											startTime = thisActivity.getStartDatetime().getTime();
										}
										if (thisActivity.getEndDatetime().getTime() > tdDateTime.getTime()) {
											endTime = tdDateTime.getTime();
										} else {
											endTime = thisActivity.getEndDatetime().getTime();
										}
										totalDuration = totalDuration + ((endTime - startTime)/1000);
									}
								}
							}
						}
					}
				}
			}
			return totalDuration;
		}

		return 0.0;
	}
	
	public Boolean checkForEnabledJob(String serviceKey) throws Exception {
		String hql = "FROM SchedulerJobDetail WHERE (isDeleted is null or isDeleted = false) AND serviceKey=:serviceKey AND isActive=1";
		List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"serviceKey"}, new Object[] {serviceKey});
		if (result.isEmpty()) {
			return false;
		} else {
			SchedulerJobDetail jobDetail = (SchedulerJobDetail)result.get(0);
			hql = "FROM ImportExportServerProperties WHERE serverPropertiesUid=:serverPropertiesUid AND (isDeleted is null or isDeleted = false)";
			List<?> servers = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"serverPropertiesUid"}, new Object[] {jobDetail.getServerPropertiesUid()});
			if(servers.size() > 0 && !((ImportExportServerProperties) servers.get(0)).getDataTransferType().equals("fbc")) {
				return false;
			}
		}
		return true;
	}
}
