package com.idsdatanet.d2.common.wellfilter; 

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.QueryFilter;
import com.idsdatanet.d2.core.model.QueryFilterCriteria;
import com.idsdatanet.d2.core.model.QueryFilterCriteriaCondition;
import com.idsdatanet.d2.core.model.QueryFilterParameter;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class wellFilterBuilderUtils extends BlazeRemoteClassSupport{
	
	private static final String CONST_WELL_SPECIFIC = "WELL_SPECIFIC";
	
	public String loadLookups(String currentView) {
		String xml = "<root/>";
		
		try {
			if(StringUtils.isBlank(currentView)){
				currentView = ModuleConstants.WELL_MANAGEMENT;
			}
			
			//operation codes
			String lookup = "";
			if(StringUtils.equals(currentView, ModuleConstants.WELL_MANAGEMENT) || StringUtils.equals(currentView, ModuleConstants.TOUR_MANAGEMENT)){
				lookup = "xml://operationtype?key=code&amp;value=label";
			}
			Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			//operationCode
			String sysCode = "well";
			
			String strSql = "SELECT operationCode FROM Operation WHERE (isDeleted=false or isDeleted is null) and (isCampaignOverviewOperation=false or isCampaignOverviewOperation is null) AND sysOperationSubclass='" + sysCode + "' group by operationCode";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (String value:list) {
				if (value==null) continue;
				if ("".equals(value)) continue;
				SimpleAttributes atts = new SimpleAttributes();
				if (lookupList.containsKey(value)) {
					atts.addAttribute("value", value);
					atts.addAttribute("label", lookupList.get(value).getValue().toString());
				} else {
					atts.addAttribute("value", value);
					atts.addAttribute("label", value);
				}
				writer.addElement("OperationType", "", atts);
			}
			
			if(StringUtils.equals(currentView, ModuleConstants.WELL_MANAGEMENT) || StringUtils.equals(currentView, ModuleConstants.TOUR_MANAGEMENT)){
				//rig
				strSql = "SELECT rigInformationUid FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) group by rigInformationUid";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				for (String value:list) {
					if (value==null) continue;
					if ("".equals(value)) continue;
					RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, value);
					SimpleAttributes atts = new SimpleAttributes();
					if (rig!=null) {
						atts.addAttribute("value", value);
						atts.addAttribute("label", rig.getRigName());
					} else {
						atts.addAttribute("value", value);
						atts.addAttribute("label", value);
					}
					writer.addElement("RigInformation", "", atts);
				}
				
				//field
				strSql = "SELECT field FROM Well WHERE (isDeleted=false or isDeleted is null) group by field";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				for (String value:list) {
					if (value==null) continue;
					if ("".equals(value)) continue;
					SimpleAttributes atts = new SimpleAttributes();
					atts.addAttribute("value", value);
					atts.addAttribute("label", value);
					writer.addElement("Field", "", atts);
				}
				//block
				strSql = "SELECT block FROM Well WHERE (isDeleted=false or isDeleted is null) group by block";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				for (String value:list) {
					if (value==null) continue;
					if ("".equals(value)) continue;
					SimpleAttributes atts = new SimpleAttributes();
					atts.addAttribute("value", value);
					atts.addAttribute("label", value);
					writer.addElement("Block", "", atts);
				}
				//country
				strSql = "SELECT country FROM Well WHERE (isDeleted=false or isDeleted is null) group by country";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				for (String value:list) {
					if (value==null) continue;
					if ("".equals(value)) continue;
					SimpleAttributes atts = new SimpleAttributes();
					atts.addAttribute("value", value);
					atts.addAttribute("label", value);
					writer.addElement("Country", "", atts);
				}
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	public String loadFilterList(String currentView) {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("filters");
			
			if(StringUtils.isBlank(currentView)){
				currentView = ModuleConstants.WELL_MANAGEMENT;
			}
			
			String strSql = "FROM QueryFilter where (isDeleted=false or isDeleted is null) and (userUid='' or userUid is null or userUid=:userUid) AND specificView = '" + currentView + "' order by name";
			
			List<QueryFilter> filterList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "userUid", this.getCurrentUserSession().getUserUid());
			for (QueryFilter filter : filterList) {
				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("uid", filter.getQueryFilterUid());
				atts.addAttribute("queryNameForSort", WellNameUtil.getPaddedStr(filter.getName()));
				Boolean isGeneric = false;
				Boolean wellSpecific = false;
				if (filter.getQueryFilterType()!=null) {
					if (filter.getQueryFilterType().equals(CONST_WELL_SPECIFIC)) {
						wellSpecific = true;
					}
				}
				if (filter.getUserUid()==null) {
					isGeneric = true;
				} else {
					if (filter.getUserUid().equals("")) {
						isGeneric = true;
					}
				}
				
				if (wellSpecific) {
					writer.startElement("specificWellFilter", atts);
				} else {
					writer.startElement(isGeneric?"genericFilter":"filter", atts);
				}
				writer.addElement("queryName", filter.getName());
				
				strSql = "select p.name, p.value from QueryFilterParameter p, QueryFilterCriteria c " +
						"WHERE (p.isDeleted=false or p.isDeleted is null) " +
						"AND (c.isDeleted=false or c.isDeleted is null) " +
						"AND c.queryFilterCriteriaUid=p.queryFilterCriteriaUid " +
						"AND c.queryFilterUid=:queryFilterUid";
				List<Object[]> values = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "queryFilterUid", filter.getQueryFilterUid());
				for (Object[] rec : values) {
					if (rec[0]!=null && rec[1]!=null) {
						writer.addElement(rec[0].toString(), rec[1].toString());
					}
				}
				writer.endElement();
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	/**
	  * Action method to load all the operations based on the filters used
	  * 
	  * @param queryString
	  * @param paramNames
	  * @param paramValues
	  * @return
	  */
	 public String loadOperations(String queryString, String[] paramNames, Object[] paramValues) {
	  String xml = "<root/>";
	  try {
		   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		   SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		   writer.startElement("root");
		   List<String> rs;
		   if (paramNames.length>0 && paramValues.length>0 && paramNames.length==paramValues.length) {
		    rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
		   } else {
		    System.out.println(queryString);
		    rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
		   }
		   for (String operationUid : rs) {
		    SimpleAttributes atts = new SimpleAttributes();
		    atts.addAttribute("operationUid", operationUid);
		    String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid);
		    atts.addAttribute("operationName", operationName);
		    atts.addAttribute("operationNameForSort", WellNameUtil.getPaddedStr(operationName));
		    writer.addElement("Operation", "", atts);
		   }
		   writer.close();
		   xml = bytes.toString();
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		  return xml;
	 }
	
	public Boolean saveFilter(String xmlCriterias, Boolean isWellSpecific, Boolean setUserUid) {
		try {
			
			SimpleXMLElement data = SimpleXMLElement.loadXMLString(xmlCriterias);
			String queryName = data.getText("queryName");
			if (queryName.equals("")) return false;
			
			QueryFilter filter = null;
			
			String strSql = "FROM QueryFilter WHERE (isDeleted=false or isDeleted is null) and name=:name";
			String[] paramFields = {"name"};
			Object[] paramValues = {queryName};
			if (setUserUid) {
				strSql += " AND userUid=:userUid";
				paramFields = new String[] {"name", "userUid"};
				paramValues = new String[] {queryName, this.getCurrentUserSession().getUserUid()};
			} else {
				strSql += " AND (userUid is null or userUid='')";
			}
			List<QueryFilter> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramFields, paramValues);
			if (list.size()>0) {
				filter = list.get(0);
			} else {
				filter = new QueryFilter();
			}
			filter.setName(queryName);
			String currentView = data.getAttribute("currentView");
			if(StringUtils.isBlank(currentView)){
				currentView = ModuleConstants.WELL_MANAGEMENT;
			}
			filter.setSpecificView(currentView);
			filter.setQueryFilterType((isWellSpecific)?CONST_WELL_SPECIFIC:null);
			if (setUserUid) filter.setUserUid(this.getCurrentUserSession().getUserUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filter);
			String queryFilterUid = filter.getQueryFilterUid();
			
			//save criteria
			QueryFilterCriteria filterCriteria = null;
			strSql = "FROM QueryFilterCriteria where (isDeleted=false or isDeleted is null) and queryFilterUid=:queryFilterUid";
			List<QueryFilterCriteria> listCriteria = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "queryFilterUid", queryFilterUid);
			if (listCriteria.size()>0) {
				filterCriteria = listCriteria.get(0);
			} else {
				filterCriteria = new QueryFilterCriteria();
				filterCriteria.setQueryFilterUid(queryFilterUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filterCriteria);
			}
			String queryFilterCriteriaUid = filterCriteria.getQueryFilterCriteriaUid();
			
			//delete criteria conditions
			strSql = "FROM QueryFilterCriteriaCondition WHERE (isDeleted=false or isDeleted is null) and queryFilterCriteriaUid=:queryFilterCriteriaUid";
			List<QueryFilterCriteriaCondition> listCondition = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "queryFilterCriteriaUid", queryFilterCriteriaUid);
			for (QueryFilterCriteriaCondition rec : listCondition) {
				rec.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}
			
			//delete criteria parameters
			strSql = "FROM QueryFilterParameter WHERE (isDeleted=false or isDeleted is null) and queryFilterCriteriaUid=:queryFilterCriteriaUid";
			List<QueryFilterParameter> listParams = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "queryFilterCriteriaUid", queryFilterCriteriaUid);
			for (QueryFilterParameter rec : listParams) {
				rec.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}
			
			//save criteria conditions and criteria parameters
			for(Iterator i = data.getChild().iterator(); i.hasNext(); ){
				SimpleXMLElement thisFilter = (SimpleXMLElement) i.next();
				if (thisFilter.getTagName().equals("filter")) {
					QueryFilterParameter filterParam = new QueryFilterParameter();
					filterParam.setQueryFilterCriteriaUid(queryFilterCriteriaUid);
					filterParam.setDataType(thisFilter.getAttribute("dataType"));
					filterParam.setName(thisFilter.getAttribute("name"));
					filterParam.setValue(thisFilter.getAttribute("value"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filterParam);
				} else if (thisFilter.getTagName().equals("condition")) {
					QueryFilterCriteriaCondition filterCondition = new QueryFilterCriteriaCondition();
					filterCondition.setQueryFilterCriteriaUid(queryFilterCriteriaUid);
					filterCondition.setClassName(thisFilter.getAttribute("className"));
					filterCondition.setJoinTables(thisFilter.getAttribute("joinTables"));
					filterCondition.setConditionClause(thisFilter.getAttribute("conditionClause"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filterCondition);
				}
			}
			
			//refresh cached data
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean deleteFilter(String queryFilterUid) {
		try {
			
			QueryFilter queryFilter = (QueryFilter) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(QueryFilter.class, queryFilterUid);
			if (queryFilter!=null) {
				
				List<QueryFilterCriteria> criteriaList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
						"FROM QueryFilterCriteria WHERE (isDeleted=false or isDeleted is NULL) and queryFilterUid=:queryFilterUid", "queryFilterUid", queryFilterUid);
				for (QueryFilterCriteria queryFilterCriteria : criteriaList) {

					List<QueryFilterCriteriaCondition> conditionList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							"FROM QueryFilterCriteriaCondition WHERE (isDeleted=false or isDeleted is NULL) and queryFilterCriteriaUid=:queryFilterCriteriaUid", "queryFilterCriteriaUid", queryFilterCriteria.getQueryFilterCriteriaUid());
					for (QueryFilterCriteriaCondition queryFilterCriteriaCondition : conditionList) {
						queryFilterCriteriaCondition.setIsDeleted(true);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(queryFilterCriteriaCondition);
					}
					
					List<QueryFilterParameter> paramsList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							"FROM QueryFilterParameter WHERE (isDeleted=false or isDeleted is NULL) and queryFilterCriteriaUid=:queryFilterCriteriaUid", "queryFilterCriteriaUid", queryFilterCriteria.getQueryFilterCriteriaUid());
					for (QueryFilterParameter queryFilterParameter : paramsList) {
						queryFilterParameter.setIsDeleted(true);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(queryFilterParameter);
					}

					queryFilterCriteria.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(queryFilterCriteria);
				}
				queryFilter.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(queryFilter);
				
				//refresh cached data
				ApplicationUtils.getConfiguredInstance().refreshCachedData();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean setDefaultWellFilter(String queryFilterUid) {
		try {
			this.getCurrentUserSession().getSystemSelectionFilter().setQueryFilterUid(queryFilterUid, true);
			
			User currentUser = ApplicationUtils.getConfiguredInstance().getCachedUser(this.getCurrentUserSession().getUserUid());
			if(StringUtils.equals(this.getCurrentUserSession().getCurrentView(), ModuleConstants.WELL_MANAGEMENT) || StringUtils.equals(this.getCurrentUserSession().getCurrentView(), ModuleConstants.TOUR_MANAGEMENT)){
				currentUser.setWellQueryFilterUid(queryFilterUid);
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentUser);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
