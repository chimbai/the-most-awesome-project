package com.idsdatanet.d2.common.scheduler.jobs;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.d2.filebridge.RestClientEndpoint;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeRESTJob implements D2Job {

	RestClientEndpoint fileBridgeRestClient = null;
	public FileBridgeRESTJob() {}
	public FileBridgeRESTJob (String serverPropertiesUid) throws Exception {
		if(StringUtils.isBlank(serverPropertiesUid)) throw new Exception("Server Properties NOT Set!!");
		ImportExportServerProperties server = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
		fileBridgeRestClient = new RestClientEndpoint(server.getUsername(), server.getPassword(), server.getEndPoint());
	}

	@Override
	public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
		//fail if websocket service is enabled
		String errorMsg = FileBridgeUtils.checkEnabledFileBridgeJob();
		if(StringUtils.isNotBlank(errorMsg)) {
			throw new Exception(errorMsg);
		}

		fileBridgeRestClient.run(userContext, jobResponse);
	}

	@Override
	public void dispose() {}

}