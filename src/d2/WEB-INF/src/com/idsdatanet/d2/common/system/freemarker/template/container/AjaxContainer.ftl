<!DOCTYPE HTML>
<#include "/core/d2Helper.ftl"/>
<#assign v_resources_path = "${contextPath}/vresources/cache/${systemStartupRandomValue}/htmllib"/>
<html>
	<head>
		<title>IDS DataNet 2.5</title>
		<link rel="icon" type="image/x-icon" href="images/favicon.ico">
		<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/d3-renderer.css">
		<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/d3-top-panel.css">
		<link rel="stylesheet" type="text/css" href="${v_resources_path}/jquery/css/jquery-ui-1.13.0.min.css">
		<link rel="stylesheet" type="text/css" href="${v_resources_path}/jquery/css/tipsy.css">
		<style type="text/css">
			body, html { font-family:helvetica,arial,sans-serif !important; background-color:#ffffff !important; font-size:90%; width:100%; height: 100%; margin: 0; overflow:hidden;}
			.top-status-label {color: #000000; font-size:13px; font-weight:bold;}
			.top-status-content {color: #6C7A89; font-size:13px; text-decoration: none; cursor: pointer; margin: 0; padding: 3px; border-style: solid; border-width: 1px; border-radius:2px; border-color:transparent;}
			.top-status-content:hover {color: #0E3062; background-color: #efefef; background-repeat: repeat-x; margin: 0; padding: 3px; text-decoration: none; border-style: solid; border-width: 1px; border-radius:2px; border-color:#808080; box-shadow: 1px 1px 1px #cecece; }
			.top-status-content-nohover {color: #6C7A89; font-size:13px; text-decoration: none; margin: 0; padding: 3px; border-style: solid; border-width: 1px; border-radius:2px; border-color:transparent;}
        </style>
		<script type="text/javascript" src="${v_resources_path}/jquery/jquery-3.6.0.min.js"></script>
		<script type="text/javascript" src="${v_resources_path}/jquery/jquery.blockUI.js"></script>
		<script type="text/javascript" src="${v_resources_path}/jquery/jquery-ui-1.13.0.min.js"></script>
		<script type="text/javascript" src="${v_resources_path}/jquery/jquery.tipsy.js"></script>
		<script type="text/javascript" src="${v_resources_path}/script/d3.js"></script>
		<script type="text/javascript" src="${v_resources_path}/script/d3-LanguageManager.js"></script>
		<script type="text/javascript" src="${v_resources_path}/script/d3-UIManager.js"></script>
		<script type="text/javascript" src="${v_resources_path}/script/d3-Common.js"></script>
		<script type="text/javascript" src="${v_resources_path}/script/d3-TopPanel.js"></script>
		<script type="text/javascript" src="${i18nlabel_js}"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				containerManager.setContextPath("${contextPath}");
				if(location.search) containerManager.setQueryString(location.search.substring(1));
				containerManager.startSessionValidityPing(${javascriptKeepAliveIntervalSeconds});
				containerManager.loadMainPage("${currentUrl}", true, null, {tabs: ${contentTabs.getTabsAsString()}});
			});
	    </script>
	</head>
	<body>
		<div id="mainContainer" style="width: 100%; height:100%;">
			<div id="topContentPane" style="border:0; padding:0px;">
				<div id="topPanel" style="padding:0; margin:0; border:0;">
					<table width="100%" cellpadding="0" cellspacing="0" style="border:0; padding:0; margin:0;">
						<tbody>
							<tr>
								<td><div id="mainMenuBar"></div></td>
								<td>
									<div id="userContextPane" style="float:right;">
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td id="customHelpLink" style="white-space:nowrap;border-bottom:5px solid rgb(0,188,212);cursor:pointer;padding:4px 10px 4px 10px;" class="topMenuItem"><a onclick="window.open('${d2ContainerUtils.getHelpFileUrl()}')">${bundle.getProperty("label.link.help")}</a></td>
												<td style="padding-left:10px;" class="D2TopRightMenuHeader">
													<img class="userContextMenuToggle" style="position:relative;height:28px;padding-right:3px;" src="${v_resources_path}/images/profile.png"/>
													<span id="userContextMenuContainer">
														<ul class="userContextMenu" >
															<li onclick="d2ScreenManager.submitRefresh();"><a class="white-link" href="javascript:void(0);">${bundle.getProperty("label.link.refresh")}</a></li>
															<li onclick="d2ScreenManager.toggleLogout();"><a class="white-link" href="javascript:void(0);">${bundle.getProperty("label.link.logout")}</a></li>
														</ul>
													</span>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div style="padding: 3px; margin-top: 3px; margin-bottom: 1px; ${controller.getShowMainSelectorPane()?string("","display:none;")}">
					<@outputTopStatusBar/>
            	</div>
			</div>
			<div id="mainCenterPane" style="padding:1px; padding-top:0px; padding-bottom:0px">
				<div id="mainTabsContainer" style="width: 100%; height: 100%;"></div>
			</div>
			<div id="operationSelectorDialog" title="Well/Operation Selector" style="display: none; padding-top: 10px">
				<@buildOperationSelectorDialog/>
	        </div>
		</div>
	</body>
</html>

<#macro buildOperationSelectorDialog>
	<table width="100%" cellpadding="2" cellspacing="2">
		<tr>
			<td>
				<div style="padding-bottom:1px; width:100%">
					<input id="operationSelector_search" style="width:100%; box-sizing: border-box; border: 1px solid #ccc; background:url('images/magglass.png') no-repeat scroll 3px 3px white; padding-left:18px" value="Search wells..." type="text" onclick="operationSelector.searchInputOnFocus(this)" onkeyup="operationSelector.searchTextKeyUp(event)">
	        		</input>
					<div id="operationSelector_msg" width="100%" style="color:red;"/>
				</div>
			</td>
		</tr>
		<tr style="display:none" id="operationSelector_operationTypeRow">
			<td width="100%">
				<div style="color:#1A5F88; fontSize: 16pt;"><b>Operation Type</b></div>
				<select id="operationSelector_operationType" style="width:100%" onchange="operationSelector.operationTypeSelected(this)">
				</select>
			</td>
		</tr>
		<tr style="display:none" id="operationSelector_campaignRow">
			<td width="100%">
				<div style="color:#1A5F88; fontSize: 16pt;"><b>Campaign</b></div>
				<select id="operationSelector_campaign" style="width:100%" onchange="operationSelector.campaignSelected(this)">
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span style="color:#1A5F88; fontSize: 16pt;"><b>Well</b></span>
				<select id="operationSelector_well" style="width:100%;" size="9" onchange="operationSelector.load(this, 'operationSelector_wellbore', 'wellbore', undefined);">
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span style="color:#1A5F88; fontSize: 16pt;"><b>Wellbore</b></span><br/>
				<select id="operationSelector_wellbore" style="width:100%;" size="4" onchange="operationSelector.load(this, 'operationSelector_operation', 'operation', undefined);">
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span style="color:#1A5F88; fontSize: 16pt;"><b>Operation</b></span><br/>
				<select id="operationSelector_operation" size="6" style="width:100%;" ondblclick="operationSelector.submit()">
				</select>
			</td>
		</tr>
	</table>
	<table width="100%" cellspacing="0" cellpadding="0" style="padding-top:8px; padding-bottom:4px">
		<tr>
			<td align="center">
				<button id="opSelectorConfirmBtn" class="PopupConfirmButton" type="button" onClick="operationSelector.submit()">${bundle.getProperty("button.confirm")}</button>
				<button id="opSelectorCancelBtn" class="PopupCancelButton" type="button" onClick="operationSelector.hide()">${bundle.getProperty("button.cancel")}</button>
			</td>
		</tr>
	</table>
	<div id="operationSelector_loadingPopup" style="top:0; left:0">Loading&nbsp;&nbsp;&nbsp;<img src="${v_resources_path}/images/ajax-loader.gif"/></div>
</#macro>

<#macro outputTopStatusBar>
	<label class="top-status-label" style="padding-left:5px">${bundle.getProperty("label.topPanel.wellOps")}</label>
	<span id="currentOperationLabelContainer"><label class="top-status-content-nohover" id="currentOperationLabel"></label></span>
	<label style="padding-left:12px" class="top-status-label">${bundle.getProperty("label.topPanel.day")}</label>
	<img id="prevDay" style="height:8px;padding-right:3px;cursor: pointer;" src="${v_resources_path}/images/back-arrow.png" onload="containerManager.updateArrowsDisplay(true);" onclick="containerManager.changeDay('daysAvailableDropdown', 'backward');"/>
	<label class="top-status-content" id="currentDayLabel" onclick="containerManager.showTopStatusBarSelection(true);"></label>
	<img id="nextDay" style="height:8px;padding-left:3px;cursor: pointer;" src="${v_resources_path}/images/forward-arrow.png" onload="containerManager.updateArrowsDisplay(true);" onclick="containerManager.changeDay('daysAvailableDropdown', 'forward');"/>
	<select id="daysAvailableDropdown" style="display:none" onchange="containerManager.onDayChanged(this)"></select>
	<label style="padding-left:12px" class="top-status-label">${bundle.getProperty("label.topPanel.datum")}</label>
	<label class="top-status-content" id="currentDatumLabel" onclick="containerManager.showTopStatusBarSelection(true)"></label>
	<select id="currentDatumsDropdown" style="display:none" onchange="containerManager.datumChanged(this)"></select>
	<label style="padding-left:12px" class="top-status-label">${bundle.getProperty("label.topPanel.uom")}</label>
	<label class="top-status-content" id="currentUomTemplateLabel" onclick="containerManager.showTopStatusBarSelection(true)"></label>
	<select id="currentUomTemplatesDropdown" style="display:none" onchange="containerManager.uomTemplateChanged(this)"></select>
</#macro>