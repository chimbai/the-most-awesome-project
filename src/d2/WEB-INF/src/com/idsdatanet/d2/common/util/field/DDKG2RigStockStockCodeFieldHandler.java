package com.idsdatanet.d2.common.util.field;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.LookupRigStock;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class DDKG2RigStockStockCodeFieldHandler extends GenericFieldHandler{
	private static final String CUBIC_METRE = "m\u00B3";
	
	private static class StockBrandAndUnit{
		final String stockBrand;
		final String unit;
		
		StockBrandAndUnit(String stockBrand, String unit){
			this.stockBrand = stockBrand;
			this.unit = unit;
		}
	}
	
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        if (value != null && value instanceof String && object != null && object instanceof RigStock) {
            RigStock rigStock = (RigStock) object;
            StockBrandAndUnit stockBrandAndUnit = parseStockBrandAndUnit((String) value);

            String sql = "FROM LookupRigStock WHERE (isDeleted = false OR isDeleted IS NULL) AND stockBrand LIKE :stockBrand AND storedUnit in (:storedUnitParameters) AND (operationCode = :operationCode or operationCode='' or operationCode is null) AND (type='rigbulkstock' OR type = '' OR type is NULL)";
            List<LookupRigStock> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"stockBrand", "storedUnitParameters", "operationCode"}, new Object[] {stockBrandAndUnit.stockBrand, this.getStoredUnitParameters(stockBrandAndUnit.unit), commandBean.getCurrentUserSession().getCurrentOperationType()});
            if (!list.isEmpty()) {
            	LookupRigStock lookup = list.get(0);
            	rigStock.setStoredUnit(lookup.getStoredUnit());
            	return lookup.getLookupRigStockUid();
            }else {
            	rigStock.setStoredUnit(stockBrandAndUnit.unit);
            	return stockBrandAndUnit.stockBrand;
            }
        }
        return value;
    }
   
    private List<String> getStoredUnitParameters(String unit){
    	List<String> list = new ArrayList<String>();
    	list.add(unit);
    	if(CUBIC_METRE.equalsIgnoreCase(unit)) {
    		list.add("M3");
    	}
    	return list;
    }
    
    private static StockBrandAndUnit parseStockBrandAndUnit(String value) {
    	int count = 0;
    	int startIndex = -1;
    	int endIndex = -1;
    	for(int i=value.length() - 1; i >= 0; --i) {
    		if(value.charAt(i) == ')') {
    			if(count == 0) {
    				endIndex = i;
    			}
    			count++;
    		}else if(value.charAt(i) == '(') {
    			if(count > 0) {
    				--count;
    				if(count == 0) {
    					startIndex = i;
    					break;
    				}
    			}
    		}
    	}
    	String stockBrand = null;
    	String unit = null;
    	if(startIndex != -1 && endIndex != -1) {
    		stockBrand = value.substring(0,startIndex).trim();
    		unit = value.substring(startIndex + 1, endIndex).trim();
    	}else {
    		stockBrand = value;
    	}
    	return new StockBrandAndUnit(stockBrand, unit);
    }
}
