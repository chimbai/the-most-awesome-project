package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.RegexUtil;
import com.idsdatanet.d2.core.model.LookupRigStock;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBlackRhinoRigStockStockCodeFieldHandler extends GenericFieldHandler {
    
    private static final String CUBIC_METRE = "m\u00B3";

    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        String rv = null;
        if (value != null && value instanceof String && object != null && object instanceof RigStock) {
            rv = value.toString();
            RigStock rigStock = (RigStock) object;
            String unitSymbol = null;

            int columnIndex = mapping.getUnitColumn();
            int rowIndex = cell.getRowIndex();
            Cell unitCell = book.getSheetAt(0).getRow(rowIndex).getCell(columnIndex);
            DataFormatter formatter = new DataFormatter();
            String cellValue = formatter.formatCellValue(unitCell, book.getCreationHelper().createFormulaEvaluator());
            if (StringUtils.isNotBlank(cellValue)) {
                unitSymbol = RegexUtil.parseUnitSymbol(cellValue);
            }

            // Reverse Lookup - not case sensitive & Unit Symbols must match (Special Case
            // for Cubic Metre)
            String sql = "FROM LookupRigStock WHERE (isDeleted = false OR isDeleted IS NULL) AND stockBrand LIKE :stockBrand ";
            List<LookupRigStock> lookupRigStockList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "stockBrand", value.toString());
            if (!lookupRigStockList.isEmpty()) {
                for (LookupRigStock lookupRigStock : lookupRigStockList) {
                    String thisUnitSymbol = lookupRigStock.getStoredUnit();
                    if ((StringUtils.equalsIgnoreCase(thisUnitSymbol, unitSymbol)) || ((StringUtils.equals(CUBIC_METRE, unitSymbol)) && StringUtils.equalsIgnoreCase(thisUnitSymbol, "M3"))) {
                        rv = lookupRigStock.getLookupRigStockUid();
                        rigStock.setStoredUnit(lookupRigStock.getStoredUnit());
                        return rv;
                    } else {
                    	String temp = " (" + unitSymbol + ")";
                    	rv = value.toString();
                    	rv = rv + temp;
                    }
                }
            }
            rigStock.setStoredUnit(RegexUtil.parseUnitSymbol(cellValue));
        }
        return rv;
    }
}