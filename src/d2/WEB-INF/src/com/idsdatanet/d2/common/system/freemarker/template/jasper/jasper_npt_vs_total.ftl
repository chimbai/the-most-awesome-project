<html>
    <head>
    	<style>
      		body {
        		margin: 0px;
        		padding: 0px;
      		}
    	</style>
		<script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
    	<script type="text/javascript" src="script/d3/schematic/jquery-1.11.2.js"></script>
    	<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery-latest.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery-ui-latest.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery.layout-latest.js"></script>
		<script type="text/javascript" src="script/generic/d3.js"></script>
		<script type="text/javascript" src="script/d3/schematic/underscore-min.js"></script>
		
    </head>
    <body>
    	<script >
    		authenticate = ${visualizejsUtils.getAuthentication()};

			$(document).ready(function () {
				
		        $('body').layout({
		            applyDefaultStyles: true,
	
		            west__resizable: true,
		            west__minSize: 420,
		            west__closable: false
		        });
		    });
    	</script>    	
    	<div id="mainContainer" class="ui-layout-center">
		</div>
		<div id="filterContainer" class="ui-layout-west">
		</div>
		<script type="text/javascript" src="script/jasper/JasperViewerEvent.js"></script>
		<script type="text/javascript" src="script/jasper/ReportViewer.js"></script>
		<script src="script/jasper/WellOperationFilter.js"></script>
		<script type="text/javascript" src="script/jasper/ReportToolbar.js"></script>
		<script src="script/jasper/report_10.js"></script>
    </body>
</html> 