package com.idsdatanet.d2.common.util.node;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.rigstock.RigStockCommandBeanListener;

public class OceanApexBcoRigStockNodeListener extends GenericNodeListener {
    
    @Override
    public void afterNodeCreate(Object object, BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, CommandBeanTreeNode node, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), RigStock.class, "amt_start");
        RigStock rigStock = (RigStock) object;
        RigStockCommandBeanListener listener = (RigStockCommandBeanListener) commandBeanListener;
        listener.stockCodeChanged(request, commandBean, node, thisConverter);

        Double previousBalance = (node.getDynaAttr().get("previousBalance") == null) ? 0.0 : Double.parseDouble(node.getDynaAttr().get("previousBalance").toString());
        Double endValue = (rigStock.getAmtDiscrepancy() == null) ? 0.0 : rigStock.getAmtDiscrepancy();
        Double amtReceived = (rigStock.getAmtStart() == null) ? 0.0 : rigStock.getAmtStart();
        Double amtUsed = (rigStock.getAmtUsed() == null) ? 0.0 : rigStock.getAmtUsed();
        double adjust = endValue - previousBalance - amtReceived + amtUsed;
        rigStock.setAmtDiscrepancy(adjust);
    }

    @Override
    public List<Object> afterAllObjectCreate(List<Object> objList, BaseCommandBean commandBean, ExcelImportTemplate template, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (objList == null || objList.isEmpty()) {
            return objList;
        }
        Integer sequence = 1;
        UserSelectionSnapshot userSelection = commandBean.getCurrentUserSelectionSnapshot();

        // Grab Biggest Sequence Value
        String sql = "FROM RigStock WHERE (isDeleted = false OR isDeleted is null) AND dailyUid=:dailyUid AND operationUid=:operationUid ORDER BY sequence DESC";
        List<RigStock> rigStockList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "dailyUid", "operationUid" }, new Object[] { userSelection.getDailyUid(), userSelection.getOperationUid() });
        if (!rigStockList.isEmpty()) {
            Integer seq = rigStockList.get(0).getSequence();
            if (seq != null) {
                sequence = seq + 1;
            }
        }
        for (Object object : objList) {
            if (object instanceof RigStock) {
                RigStock rigStock = (RigStock) object;
                rigStock.setSequence(sequence++); // Stamp Sequence
            }
        }
        return objList;
    }
    
}