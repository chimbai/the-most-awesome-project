<p>
Operation: <@showField node=form.root editmode=true editable=true dynamic="@operationUid" />
</p>
<input type="submit" value="Select Operation" />
<#if getDynamicAttribute(form.root, "operationUid")!="">
	<p>
	Day: <@showField node=form.root editmode=true editable=true dynamic="@dailyUid" />
	</p>
	<input type="submit" value="Select Day" />
</#if>


<#if form.root.list.ReportDaily?exists>
	<#list form.root.list.ReportDaily?values as item>
		<p>
		MD: ${getFieldFinalValue(item, "depthMdMsl")} ${getFieldUomSymbol(item, "depthMdMsl")}
		</p>
		<p>
		Sum. of Period: ${getFieldFinalValue(item, "reportPeriodSummary")}
		</p>
		<p>
		Current OP @ 0600: ${getFieldFinalValue(item, "reportCurrentStatus")}
		</p>
		<p>
		Planned OP:  ${getFieldFinalValue(item, "reportPlanSummary")}
		</p>
  	</#list>
</#if>

