<html>
<body>
<p>Dear Sir/Madam,</p> 

<p><i>**This is an automated message. Please do not reply to the address above.**</i></p>
<p><b>Profile Name: ${profileName}</b></p>
<p>These are the details of the above profile:</p>
<#if groupDetails?exists>
	<p>			
		<#list groupDetails?keys as groupUid>
			<table border="0" width="70%">
				<tr>
					<td width="35%">Group:&nbsp;${groupDetails[groupUid]["groupNumber"]}</th>
					<td width="35%" align="right">Match:&nbsp;${groupDetails[groupUid]["matchCriteria"]}</th>
				</tr>
			</table>
			<table border="1" width="70%">
				<tr>
					<th width="35%">Category</th>
					<th width="35%">Element</th>
				</tr>
				
				<#list groupDetails[groupUid].details?keys as detailsUid>
					<tr>
						<td width="35%">&nbsp;${groupDetails[groupUid].details[detailsUid]["category"]}</td>
						<td width="35%">&nbsp;${groupDetails[groupUid].details[detailsUid]["element"]}</td>
					</tr>
				</#list>
			</table>
			</br>
		</#list>		
	</p>
</#if>
<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>