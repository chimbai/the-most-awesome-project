<link href="styles/fileuploader.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	function hideUploadTab()
	{
		displayRow();
		$('#uploadbutton').show('fast');
		var row = document.getElementById("dataTable");
		row.style.display = '';
		row = document.getElementById("inforow2");
		row.style.display = '';
		document.form.submit();
	}
	function showUploadTab()
	{
		displayRow();
		$('#uploadbutton').hide('fast');
		var row = document.getElementById("dataTable");
		row.style.display = 'none';
		row = document.getElementById("inforow2");
		row.style.display = 'none';
	} 
	function showUploadTab2()
	{
		$('#uploadbutton').hide('fast');
		var row = document.getElementById("dataTable");
		row.style.display = 'none';
		$('#inforow1').hide('fast');
		row = document.getElementById("inforow1");
		row.style.display = 'none';
		row = document.getElementById("inforow2");
		row.style.display = 'none';
		$('#singleUpload').show('slow');
	} 
	function displayRow(){
		var row = document.getElementById("uploadSection");
		if (row.style.display == '') row.style.display = 'none';
		else row.style.display = '';
	}
	function radioChange(){
		var searchType = $("input[name='searchType']:checked").val();
		alert(searchType);
	}
	
	var upload_number = 2;
	function addRow() {
		var tbl = document.getElementById('tbl');
 	 	var lastRow = tbl.rows.length;
 	 	var iteration = lastRow;
  		var row = tbl.insertRow(lastRow);
  		
  		var cellLeft = row.insertCell(0);
		var textNode = document.createTextNode("File " + iteration);
		cellLeft.appendChild(textNode);
		
		var cells = row.insertCell(1);
		var file = document.createElement("input");
	 	file.setAttribute("type", "file");
	 	file.setAttribute("name", "attachment"+upload_number);
	 	cells.appendChild(file);
	 	
	 	var cells = row.insertCell(2);
	 	var ftype = document.createElement("select");
	 	var option;
	 	ftype.setAttribute("name", "fileType"+upload_number);
	 	ftype.options[0] = new Option('Private', '1');
	 	ftype.options[1] = new Option('Public', '0');
	 	cells.appendChild(ftype);
	 	
	 	var cells = row.insertCell(3);
	 	var descr = document.createElement("input");
	 	descr.setAttribute("type", "text");
	 	descr.setAttribute("size", "50");
	 	descr.setAttribute("name", "fileDes"+upload_number);
	 	cells.appendChild(descr);
	 	
		upload_number++;
	}
</script>

<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	document.form.encoding = "multipart/form-data";
});
</script> 

<@showFormTitleInContentPage classType="FileManagerFiles" />

<table class="root-action">
	<tr id="inforow1">
		<td width=10%>
			Please Select Operation 
		</td>
		<td width=20%>
			<@showField node=form.root editmode=true editable=true dynamic="@opsSelect" onchange="document.form.submit();" showEmptyValue=false/>
			<#assign selOps = getDynamicAttribute(form.root, "@opsSelect") />
			<input type="hidden" name="selectedOps" id="selectedOps" value="${selOps}"/>
		</td>
		<td align="right">
			Keywords: 
		</td>
		<td align="left">
			<@showField node=form.root dynamic="@keywords" editmode=true editable=true options="size='50'"/>
		</td>
		<td align="left">
			<#assign seltype = getDynamicAttribute(form.root, "searchTypes") />
			<#if seltype == "contain"> 
				<input type="radio" name="searchType" value="exact"> exact match &nbsp;
				<input type="radio" checked name="searchType" value="contain"> contains any
			<#else>
				<input type="radio" checked name="searchType" value="exact"> exact match &nbsp;
				<input type="radio" name="searchType" value="contain"> contains any
			</#if>
		</td>
		<td width=10%>
			<@showButton node=form.root action="filterButton" label="Search" onclick="d2ScreenManager.validateAndSubmitForm($(this), 'form', 'd2RootAction', true);"/>
		</td>
	</tr>
	<tr id="inforow2">
		<td colspan=5>
			<table>
				<tr>
					<td align="right">Date</td>
					<td align="left"><@showField node=form.root dynamic="@selectedDate" editmode=true editable=true/></td>
					<td align="right">Category</td>
					<td align="left"><@showField node=form.root dynamic="@category" editmode=true editable=true/></td>
					<td align="right">File Extension</td>
					<td align="left"><@showField node=form.root dynamic="@fileExt" editmode=true editable=true/></td>
					<td align="right">
						
					</td>
				</tr>

			</table>
		</td>
		<td width=10%>
			<#assign addAccess = getDynamicAttribute(form.root, "addAccessRight") />
			<#if addAccess == "1"> 
				<input type="button" onclick="showUploadTab2();" value="Upload" id="uploadbutton"/>
			</#if>
		</td>
	</tr>
	<tr>
		<td colspan=5 align="left">Operation Selected : <@showField node=form.root editmode=false editable=false dynamic="@operationName"/></td>
	</tr>
	<tr id="singleUpload" style="display: none;">
		<td colspan=5>
			<table id="tbl" border=1 width="50%">
				<tr>
					<td>#</td><td>File</td><td>Private/Public</td><td>Decsription</td>
				</tr>
				<tr>
					<td>File 1</td>
					<td><input type="file" name="attachment1" id="attachment"/></td>
					<td><select name="fileType1" id="fileType"><option value="1">Private</option><option value="0">Public</option></select></td>
					<td><input type="text" name="fileDes1" id="fileDes" size="50"></td>
				</tr>
			</table>
			<br>
			<table>
			<tr><td>
			<input type="button" onclick="addRow();" value="Add another File"/>	
			<@showButton node=form.root action="fileUploads" label="Start Upload" onclick="d2ScreenManager.validateAndSubmitForm($(this), 'form', 'd2RootAction', true);"/>
			<@showButton node=form.root action="Cancel" label="Cancel Upload" onclick="d2ScreenManager.validateAndSubmitForm($(this), 'form', 'd2RootAction', true);"/>
			</td></tr>
			</table>

		 <p id="Note"></p> 
		 <!--<input type="button" value="Add More file" onclick="addMoreFiles()" />-->
		
		 
		 </td>
		<td>
			<!--<@showButton node=form.root action="fileUploads" label="Upload" onclick="d2ScreenManager.validateAndSubmitForm($(this), 'form', 'd2RootAction', true);"/>-->
		</td>
	</tr>
	<tr id="uploadSection" style="display: none;">
		<td colspan=5>
			<div id="file-uploader">		
			<noscript>			
				<p>Please enable JavaScript to use file uploader.</p>
		
			</noscript>         
		</div>
		</td>
		<td><input type="button" onclick="hideUploadTab();" value="Close"/></td>
	</tr>
</table>


<table class="inner-table" id="dataTable">
	<tr>
		<th class="record-actions" width="1%"><@showRecordActionsHeader /></th>
		<th width="12%">File</th>
		<th width="10%">Operation</th>
		<th width="6%">Public/Private</th>
		<th>Description</th>
		<th width="12%">Category</th>
		<th width="6%">Size</th>
		<th width="10%">Uploaded By</th>
		<th width="6%">Uploaded On</th>
	</tr>
	<#if form.root.list.FileManagerFiles?exists>
		<#list form.root.list.FileManagerFiles?values as file>
			<tr <@template file/>>
				<td class="record-actions"><@showRecordActions node=file /></td>
				<td>
					<a class="blue-link" href="webservice/filemanagerservice.html?mainAction=download&_sys_filter_disable_output_compression&_showErrorPageOnAccessDenied&fileId=${getFieldValue(file,"fileManagerFilesUid")?url('utf-8')}">
						${getFieldValue(file, "fileName")}
					</a>
				</td>
				<td><@showField node=file field="attachToOperationUid" editable=false/></td>
				<td><@showField node=file field="isPrivate"/></td>
				<td><@showField node=file field="description" options="size='100'"/></td>
				<td><@showField node=file field="categoryPrimary"/></td>
				<td align="right"><@showField node=file field="@fileSize" editable=false/></td>
				<td><@showField node=file field="uploadedUserUid" editable=false/></td>
				<td><@showField node=file field="@UploadedDate" editable=false/></td>
			</tr>	
		</#list>
	</#if>
</table>
<script src="script/fileuploader.js" type="text/javascript"></script>
    <script>        
        function createUploader(){            
            var uploader = new qq.FileUploader({
                element: document.getElementById('file-uploader'),
                action: 'fileuploads.html',
                debug: true
            });  
            
            uploader.setParams({
			   opsUid: document.getElementById("selectedOps").value
			});         
        }
        
        // in your app create uploader as soon as the DOM is ready
        // don't wait for the window to load  
        window.onload = createUploader;     
    </script>   
    
<#macro showSelectionForImportExport>
	<div id="objConfigDiv">
	</div>
</#macro>