<html>
<body>
This is an <b>automated message</b>.
<br>
<#if missing?exists>
	<#list missing?keys as field>
		<br> The field ${field} is blank/null for the following operation: 
		<#list missing[field]?keys as operation>
			<br>${missing[field][operation]}
		</#list>
		<br>
	</#list>
</#if>

<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>