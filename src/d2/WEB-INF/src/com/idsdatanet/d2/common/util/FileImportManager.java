package com.idsdatanet.d2.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;

import net.sf.json.JSONObject;

public class FileImportManager extends DefaultBeanSupport {
    
    private static FileImportManager configuredInstance = null;
    public final String KEY_FILE = "file";
    public final String TEMP_UPLOAD_FILE_LOCATION = "WEB-INF/temp";
    
	public static FileImportManager getConfiguredInstance() throws Exception {
	    if (configuredInstance == null) configuredInstance = getSingletonInstance(FileImportManager.class);
        return configuredInstance;
	}

	public String importFile(HttpServletRequest request, HttpServletResponse response, String fileKey) throws Exception {
        File uploadedFile = this.getFileFromHttpServletRequest(request, fileKey);
        return this.saveFile(uploadedFile);
    }
	
//	private void writeOutput(JSONObject json, HttpServletResponse response) throws IOException{
//        response.setCharacterEncoding("utf-8");
//        Writer writer = response.getWriter();
//        json.write(writer);
//        writer.close();
//    }
	
	public File getFileFromHttpServletRequest(HttpServletRequest request, String fileKey) throws Exception {
        if (request == null || StringUtils.isBlank(fileKey)) return null;
        if (request instanceof MultipartHttpServletRequest) {
            return this.getFileFromMultipartHttpServletRequest((MultipartHttpServletRequest) request, fileKey);
        }
        return null;
    }
	
	public File getFileFromMultipartHttpServletRequest(MultipartHttpServletRequest multipartHttpServletRequest, String fileKey) throws Exception {
        if (multipartHttpServletRequest == null || StringUtils.isBlank(fileKey)) return null;
        return this.convertMultipartToFile(multipartHttpServletRequest.getFile(fileKey));
    }
	
	public File convertMultipartToFile(MultipartFile multipart) throws Exception {
        if(multipart == null) return null;
	    File convFile = new File(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + multipart.getOriginalFilename());
	    if (convFile.exists()) { convFile.delete(); }
        multipart.transferTo(convFile);
        return convFile;
    }
	
	public Object getNewObject(String className) throws Exception {
	    if (StringUtils.isNotBlank(className)) {
            Class<?> clazz = Class.forName("com.idsdatanet.d2.core.model." + className);
            Object o = clazz.newInstance();
            return o;
	    }
	    return null;
	}
	
	public String saveFile(File file) throws Exception {
	    String rootPath = ApplicationConfig.getConfiguredInstance().getServerRootPath();
	    String filePath = this.TEMP_UPLOAD_FILE_LOCATION + "/" + file.getName();
	    File fileDir = new File(new File(rootPath), filePath);
        fileDir.getParentFile().mkdirs();
	    FileInputStream fileInputStream = new FileInputStream(file);
	    Files.copy(fileInputStream, Paths.get(rootPath + filePath), StandardCopyOption.REPLACE_EXISTING);
	    fileInputStream.close();
	    file.delete(); // Temp file
	    return rootPath + filePath;
	}

}
