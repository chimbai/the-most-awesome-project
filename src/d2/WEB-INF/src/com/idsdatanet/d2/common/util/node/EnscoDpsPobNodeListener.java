package com.idsdatanet.d2.common.util.node;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class EnscoDpsPobNodeListener extends GenericNodeListener {
    
    // Special Behaviour:
    // CrewName: Take Family Name and Given Name and make it "Family, Given".
    // Nationality: If Australia, National. Else, Expat
    // Pax: Default at 1

    @Override
    public Object beforeNodeCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return object;
    }
    
    @Override
    public Object afterObjectCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (object != null && object instanceof PersonnelOnSite) {
            PersonnelOnSite pob = (PersonnelOnSite) object;
            pob.setPax(1); // Default value as specified in specification
            return pob;
        }
        return object;
    }
    
    @Override
    public List<Object> afterAllObjectCreate(List<Object> objList, BaseCommandBean commandBean, ExcelImportTemplate template, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (objList == null || objList.isEmpty()) {
            return objList;
        }

        List<Object> filteredObjectList = new LinkedList<>();
        for (Object object : objList) {
            if (object instanceof PersonnelOnSite) {
                PersonnelOnSite pob = (PersonnelOnSite) object;

                // Resolve Crew Name ("nameLast, nameFirst")
                String nameLast = pob.getNameLast();
                String nameFirst = pob.getNameFirst();
                if (StringUtils.isBlank(nameLast) && StringUtils.isBlank(nameFirst)) {
                    continue;
                } else {
                    String crewName = "";
                    if (StringUtils.isNotBlank(nameLast) && StringUtils.isNotBlank(nameFirst)) {
                        crewName = nameLast + ", " + nameFirst;
                    } else if (StringUtils.isNotBlank(nameLast) && StringUtils.isBlank(nameFirst)) {
                        crewName = nameLast;
                    } else if (StringUtils.isBlank(nameLast) && StringUtils.isNotBlank(nameFirst)) {
                        crewName = nameFirst;
                    }
                    pob.setCrewName(crewName);
                    filteredObjectList.add(object);
                }

                // Resolve Nationality
                String nationality = pob.getNationality();
                if (StringUtils.isNotBlank(nationality)) {
                    if (StringUtils.equals("Australia", nationality)) {
                        pob.setNationality("local");
                    } else {
                        pob.setNationality("expat");
                    }
                }
            }
        }
        
        return filteredObjectList;
    }
    
}