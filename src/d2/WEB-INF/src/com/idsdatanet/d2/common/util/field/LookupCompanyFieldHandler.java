package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class LookupCompanyFieldHandler extends GenericFieldHandler {
    
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        if (value != null && value instanceof String) {
            // Reverse Lookup - not case sensitive
            String sql = "FROM LookupCompany WHERE (isDeleted = false OR isDeleted IS NULL) AND companyName LIKE :companyName ";
            List<LookupCompany> lookupCompanyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "companyName", (String) value);
            if (!lookupCompanyList.isEmpty()) {
                return lookupCompanyList.get(0).getLookupCompanyUid();
            }
        }
        return value;
    }
}