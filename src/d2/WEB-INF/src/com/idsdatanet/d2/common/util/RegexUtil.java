package com.idsdatanet.d2.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.spring.DefaultBeanSupport;

public class RegexUtil extends DefaultBeanSupport {
    
    public static final String REGEX_DOUBLE = "^[0-9.]*";
    public static final String REGEX_PLUS = "^\\+*";
    public static final String REGEX_DOUBLE_WITH_PLUS_AND_MINUS = "^\\-*\\+*\\d+\\.*\\d*\\s*";
    public static final String REGEX_NON_DOUBLE = "[^0-9\\.-]+";
    public static final String REGEX_NON_DOUBLE_FRONT_OF_STRING = "^[^0-9*]+";
    public static final String REGEX_NON_DOUBLE_FRONT_OF_STRING_EXCLUDE_PLUS_AND_MINUS = "^[^0-9\\+\\-*]+";
    public static final String REGEX_NON_DOUBLE_FRONT_OF_STRING_EXCLUDE_MINUS = "^[^0-9\\-*]+";

    public static Object parseType(Object value, String type) {
        return RegexUtil.parseType(value, type, false);
    }

    public static Object parseType(Object value, String type, boolean ignoreTrim) {
        String string = value.toString();
        if (!ignoreTrim) { // Only on String fields Of Course
            string = string.trim();
        }
        Object rv = string;

        if (StringUtils.equals(type, "Double") || StringUtils.equals(type, "DoubleIgnoreUnit")) {
            rv = RegexUtil.parseDouble(string);
        } else if (StringUtils.equals(type, "DoubleWithUnit")) {
            rv = RegexUtil.parseDoubleWithUnit(string);
        } else if (StringUtils.equals(type, "Integer")) {
            rv = RegexUtil.parseInteger(string);
        } else if (StringUtils.equals(type, "Boolean")) {
            rv = RegexUtil.parseBoolean(string);
        }
        return rv;
    }

    public static Double parseDouble(String string) {
        try {
            String str = string.replaceAll(REGEX_PLUS, "").trim();
            Pattern pattern = Pattern.compile(REGEX_DOUBLE);
            Matcher matcher = pattern.matcher(str);
            if (matcher.find()) {
                String v = matcher.group();
                return Double.parseDouble(v);
            } else {
                return null;
            }
            // String v = pattern.matcher(str).replaceAll("").trim();
        } catch (Exception e) {
            return null;
        }
    }

    public static String parseDoubleWithUnit(String string) {
        Pattern pattern = Pattern.compile(REGEX_NON_DOUBLE_FRONT_OF_STRING_EXCLUDE_MINUS);
        return pattern.matcher(string).replaceAll("").trim();
    }
    
    public static Integer parseInteger(String string) {
        try {
            Pattern pattern = Pattern.compile(REGEX_NON_DOUBLE);
            String v = pattern.matcher(string).replaceAll("").trim();
            return Integer.parseInt(v);
        } catch (Exception e) {
            return null;
        }
    }

    public static String parseUnitSymbol(String string) {
        Pattern pattern = Pattern.compile(REGEX_DOUBLE_WITH_PLUS_AND_MINUS);
        String s = pattern.matcher(string).replaceAll("").trim();
        s = s.replaceAll(" ", "");
        return s;
    }
    
    public static Boolean parseBoolean(String string) {
    	if (string != null && string instanceof String) {
             if("1".equals(string) || "yes".equalsIgnoreCase((String) string) || "true".equalsIgnoreCase((String) string)) {
             	return true;
             } else if("0".equals(string) || "no".equalsIgnoreCase((String) string) || "false".equalsIgnoreCase((String) string)) {
             	return false;
             }
        }
    	return Boolean.parseBoolean(string);
    }

}
