<html>
<body>
Dear Sir/Madam, 

<p>This message was sent to notify that you are responsible for a Non Conformance or there are changes to the Non Conformance under your responsibility:</p>
<p>NCR #: NCR-${ncr.eventNumber}</p>

		<p>Click <a href="${ncr.hyperLink}">here</a> for further action.</p>

<p>Thanks,</p>
<p><b>IDS Support Group</b></p>
</body>
</html>