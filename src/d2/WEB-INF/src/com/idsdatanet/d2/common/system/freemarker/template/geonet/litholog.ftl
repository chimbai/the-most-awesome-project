<@showFormTitleInContentPage classType="Litholog" />

<table border="0">
	<tr align="center">
		<th style="font-weight: bold;font-family:Verdana,Arial;font-size: 11px;padding: 3px;">
			Filter Litholog :  From Depth
			<@showField node=form.root editmode=true dynamic="@fromDepth"/>
			To Depth
			<@showField node=form.root editmode=true dynamic="@toDepth"/>
			and Main Lithology is 
		</th>
		<th>
			<@showField node=form.root editmode=true dynamic="@mainLithology"/>
		</th>
		<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th class="buttons">
			<@showButton node=form.root action="filterButton" label="Refresh" onclick="d2ScreenManager.validateAndSubmitForm($(this), 'form', 'd2RootAction', true);" />
		</th>
	</tr>
</table>

<table class="inner-table">
	
	<tr>
		<th class="record-actions" width="2%" rowspan="3"><@showRecordActionsHeader /></th>
		<th colspan="12" rowspan="2" style="text-align:center">
			Lithology
		</th>
		<th class="label" colspan="7" style="text-align:center">
			Grains
		</th>
		<th class="label" colspan="13" rowspan="2" style="text-align:center">
			Grain Size  & Characteristics
		</th>
		<th class="label" colspan="6" rowspan="2" style="text-align:center">
			Cements
		</th>
		<th class="label" colspan="12" rowspan="2" style="text-align:center">
			Accessories
		</th>
		<th class="label" colspan="2" rowspan="2" align="center" valign="center">
			<img src="./images/litholog/visual.gif" align="center">
		</th>
		<th class="label" rowspan="3" align="center" valign="bottom">
			<img src="./images/litholog/hc_shows.gif" align="center">
		</th>
		<th class="label" rowspan="3" style="text-align:center">
			Longhand Description
		</th>
	</tr>
	<tr>
		<th class="label" colspan="3" style="text-align:center">
			Calcareous
		</th>
		<th class="label" colspan="3" style="text-align:center">
			Siliciclastic
		</th>
		<th class="label" style="text-align:center">
			%
		</th>
	</tr>
	<tr>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/depth.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/bit_no.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/sample_reliability.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/main_lithology.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/qualifier.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/of_rock.gif">
		</th>
		<th class="label" align="center" valign="bottom" colspan="2" style="text-align:center">
			<img src="./images/litholog/colour.gif">
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/hardness.gif">
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/fracture.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/clay.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/silt.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/sand.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/clay.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/silt.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/sand.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/total.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/very_fine.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/fine.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/medium.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/coarse.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/very_coarse.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/granular.gif">
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/total.gif">
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/rounding.gif">
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/sorting.gif">
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/sphericity2.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type1.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type2.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type3.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type1.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type2.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type3.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type4.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type5.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type6.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/type.gif">
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			<img src="./images/litholog/percentage.gif">
		</th>	
	</tr>
    
    <tr>
		<th class="label" align="center" valign="bottom" style="text-align:center">
		
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "depthFromMdMsl")}
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" colspan="1" style="text-align:center">
			
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" align="center" colspan="2" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainCalcerousClayPct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainCalcerousSiltPct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainCalcerousSandPct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSilicicClayPct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSilicicSiltPct")}			
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSilicicSandPct")}		
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeVfinePct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeFinePct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeMediumPct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeCoarsePct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeVcoarsePct")}
		</th>		
		<th class="label" align="center" valign="bottom" style="text-align:center">
			${getFieldUomSymbol("Litholog", "grainSizeGranularPct")}
		</th>		
		<th class="label" colspan="1" align="center" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			
		</th>		
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" colspan="2" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>	
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		<th class="label" align="center" valign="bottom" style="text-align:center">
			
		</th>
		
	</tr>
	
	<#if form.root.list.Litholog?exists>
		<#list form.root.list.Litholog?values as item>
			<tr <@template item/>>
				<td>
					<@showRecordActions node=item />
				</td>
				<td>
					<@showField node=item field="depthFromMdMsl" options="size=3" showUOM=false/>
				</td>
				<td>
					<@showField node=item field="bitrunUid" />
				</td>
				<td>
					<@showField node=item field="sampleReliability" />
				</td>
				<td>
					<@showField node=item field="mainLithology" />
				</td>
				<td>
					<@showField node=item field="modifier" />
				</td>
				<td>
					<@showField node=item field="rockPct" options="size=3" showUOM=false/>
				</td>
				<td>
					<@showField node=item field="colour" fieldtype="textarea" options="cols='20' rows='5'"/>
				</td>
				<td>
					<img src="./images/litholog/button_down.png" title="wh = white
gy = grey
blk = black
rd = red
pk = pink
pu = purple
orng = orange
brn = brown
yel = yellow
grn = green
ol = olive
bl = blue
cl = colourless
multi = multicoloured
trans = translucent
 
lt = light
dk = dark
mod = moderate
vgt = variegated">
				</td>
				<td style="border-right-style: dotted; border-right-width: 0">
					<@showField node=item field="hardness" />
				</td>	
				<td>
					<@showField node=item field="hardnessRange" />
				</td>		
				<td>
					<@showField node=item field="fracture" />
				</td>	
				<td>
					<@showField node=item field="fractureRange" />
				</td>		
				<td>
					<@showField node=item field="grainCalcerousClayPct" options="size=3" showUOM=false/>
				</td>
				<td>
					<@showField node=item field="grainCalcerousSiltPct" options="size=3" showUOM=false/>
				</td>	
				<td>
					<@showField node=item field="grainCalcerousSandPct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSilicicClayPct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSilicicSiltPct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSilicicSandPct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainTotalPct" editable=false showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSizeVfinePct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSizeFinePct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSizeMediumPct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSizeCoarsePct" options="size=3" showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainSizeVcoarsePct" options="size=3" showUOM=false/>	
				</td>		
				<td>
					<@showField node=item field="grainSizeGranularPct" options="size=3" showUOM=false/>	
				</td>		
				<td>
					<@showField node=item field="grainSizeTotalPct" editable=false showUOM=false/>
				</td>		
				<td>
					<@showField node=item field="grainCharRounding" />
				</td>		
				<td>
					<@showField node=item field="grainCharRoundingRange" />
				</td>		
				<td>
					<@showField node=item field="grainCharSorting" />
				</td>		
				<td>
					<@showField node=item field="grainCharSortingRange" />
				</td>		
				<td>
					<@showField node=item field="grainCharSphericity" />
				</td>	
				<td>
					<@showField node=item field="grainCharSphericityRange" />
				</td>	
				<td>
					<@showField node=item field="cmt01Type" />	
				</td>
				<td>
					<@showField node=item field="cmt01Pct" options="size=3" showUOM=false/>	
				</td>
				<td>
					<@showField node=item field="cmt02Type" />
				</td>	
				<td>
					<@showField node=item field="cmt02Pct" options="size=3" showUOM=false/>
				</td>
				<td bgcolor="#99CCFF">
					<@showField node=item field="cmt03Type" />
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="cmt03Pct" options="size=3" showUOM=false/>
				</td>	
				<td>
					<@showField node=item field="acc01Type" />
				</td>	
				<td>
					<@showField node=item field="acc01Pct" options="size=3" showUOM=false/>
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc02Type" />
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc02Pct" options="size=3" showUOM=false/>
				</td>	
				<td>
					<@showField node=item field="acc03Type" />
				</td>	
				<td>
					<@showField node=item field="acc03Pct" options="size=3" showUOM=false/>
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc04Type" />
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc04Pct" options="size=3" showUOM=false/>
				</td>	
				<td>
					<@showField node=item field="acc05Type" />
				</td>	
				<td>
					<@showField node=item field="acc05Pct" options="size=3" showUOM=false/>
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc06Type" />
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="acc06Pct" options="size=3" showUOM=false/>
				</td>	
				<td>
					<@showField node=item field="pore01Type" />
				</td>	
				<td>
					<@showField node=item field="pore01Pct" options="size=3" showUOM=false/>
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="hcShows" />
				</td>	
				<td bgcolor="#99CCFF">
					<@showField node=item field="longhandDescr" fieldtype="textarea" options="cols='40' rows='5'"/>
				</td>	
			</tr>
			
			
		</#list>
		<tr><td colspan="55" class="child-title"><div class="buttons bottom-button align-left"><@showButton node=form.root class="Litholog" isAdd=true label="Add Litholog"/></div></td></tr>
	</#if>
</table>

<#macro showSelectionForImportExport>
	<div id="objConfigDiv">
	</div>
</#macro>
