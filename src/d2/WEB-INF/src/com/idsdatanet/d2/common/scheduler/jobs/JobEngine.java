package com.idsdatanet.d2.common.scheduler.jobs;

import java.util.List;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;

public class JobEngine extends DefaultBeanSupport {
	
	private static JobEngine configuredInstance = null;
	public static JobEngine getConfiguredInstance() {
		if (configuredInstance == null) {
			try {
				configuredInstance = getSingletonInstance(JobEngine.class);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return configuredInstance;
	}
	
	public void sendInBackground(String jobPool, D2Job d2Job, boolean singleJobRunning) {
		try {
			List<JobContext> jobList = JobServer.getConfiguredInstance().getPendingOrRunningJobs(jobPool);
			if (singleJobRunning && jobList != null) {
				for (JobContext j : jobList) {
					int jobStatus = j.getJobStatus();
					if (jobStatus == D2Job.JOB_STATUS_IN_QUEUE || jobStatus == D2Job.JOB_STATUS_STARTED) {
						return;
					}
				}
			}
			JobContext job = JobServer.getConfiguredInstance().createJob(jobPool, d2Job, UserContext.getUserContext());
			JobServer.getConfiguredInstance().submitJob(job);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendInBackground(String jobPool, D2Job d2Job) {
		this.sendInBackground(jobPool, d2Job, false);
	}
}
