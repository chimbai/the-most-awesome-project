<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
<div id="embedContainer" style="height:100%;"></div>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
<script type="text/javascript" src="${v_resources_path}/powerbi/powerbi.min.js" charset="utf-8"></script>

<script type="text/javascript">
    // Global variables
    var _embedReportId;
    <#if reportId?exists>
        <#list reportId as key,value>
	    	<#list reportId as key,value>
	    	_embedReportId = '${value}';
	    	</#list>
	    </#list>
    </#if>        
    var _embedUrl = "https://app.powerbi.com/reportEmbed?reportId=_embedReportId&groupId=93162cbb-5c66-4820-9a59-450e296bdb8f&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWVhc3QtYXNpYS1yZWRpcmVjdC5hbmFseXNpcy53aW5kb3dzLm5ldCJ9";

	var embedValue;
    <#if embedToken?exists>
        <#list embedToken as key,value>
	    	<#list embedToken as key,value>
	    	embedValue = '${value}';
	    	</#list>
	    </#list>
    </#if>
    
    function showReport() {    
        var models = window['powerbi-client'].models;
        var permissions = models.Permissions.All;
        
        var config = {
            type: 'report',
            tokenType: 1,
            accessToken: embedValue,
            embedUrl: _embedUrl,
            id: _embedReportId,
            permissions: permissions,
            settings: {
                filterPaneEnabled: true,
                navContentPaneEnabled: true
            }
        };
        
        var embedContainer = document.getElementById("embedContainer");
        var report = powerbi.embed(embedContainer, config);
        report.off("loaded");
        report.on("loaded", function () {
            console.log("Loaded");
        });
        report.off("rendered");
        report.on("rendered", function () {
            console.log("Rendered");
        });
        report.on("error", function (event) {
            console.log(event.detail);
        
            report.off("error");
        });
        report.off("saved");
    }

    $(document).ready(function() {
        showReport();
    });
</script>