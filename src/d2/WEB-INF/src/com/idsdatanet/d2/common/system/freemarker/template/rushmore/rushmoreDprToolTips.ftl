<@showFormTitleInContentPage />

<table class="inner-table">
	<tr>
		<td>
			<table class="inner-table">
				<tr>
					<th class="label" width="10%">Column</td>
					<th class="label" width="20%">Title</td>
					<th class="label" width="70%">Definitions and Instructions<br/><i>(Click on the column title to go to the data input column)</i></td>
				</tr>
				<tr>
					<td class="value">1</td>
					<td class="value">Country</td>
					<td class="value">
						Give the name of the country in which the well is located.<br/>
						&nbsp;<br/>
						If the well is in a shared 'zone' between two or more countries such as Australia/Timor, Nigeria/Sao Tome, Kuwait/Saudi Arabia etc. please note this in column 1.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">2</td>
					<td class="value">Formal well name</td>
					<td class="value">
					 	Enter the official name of the well, as used by regulatory bodies.<br/>
						Please do not give abbreviated names - they can be shown in column 3.<br/>
						&nbsp;<br/>
						If this well was re-named after it was drilled please give its previous well name in the further details column (Column 120).<br/>
					</td>
				</tr>
				<tr>
					<td class="value">3</td>
					<td class="value">"In house" or common well name</td>
					<td class="value">
						If the well is known by a different name or an abbreviated name in your company, please enter it in this column.<br/>
						&nbsp;<br/>
						For Gulf of Mexico wells, enter the prospect name here if it is different from its official name.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">4</td>
					<td class="value">Field / basin / area name</td>
					<td class="value">
						Enter the field or basin name, as relevant.<br/>
						&nbsp;<br/>
						For wells in Australia / New Zealand, use this column to enter the basin number.<br/>
						&nbsp;<br/>
						For wells in the Gulf of Mexico, use this column to enter the area code.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">5</td>
					<td class="value">Block number</td>
					<td class="value">
						Enter the block number where relevant. <br/>
					</td>
				</tr>
				<tr>
					<td class="value">6</td>
					<td class="value">Platform name</td>
					<td class="value">
						If the well was drilled from, or over, a platform, please enter the platform name.<br/>
						If this was a land well drilled from a pad, please enter the pad number.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">7 - 14</td>
					<td class="value">Geographical latitude / longitude</td>
					<td class="value">
						Enter the latitude and longitude of the well in degrees, minutes and seconds.<br/> 
						Use the N/S column to show whether north or south of the equator.<br/>
						Use the E/W column to show whether east or west of the Greenwich meridian.<br/>
						&nbsp;<br/>
						This should be the location of the well head rather than the location of TD.<br/>
						If the location of the well is very confidential, you may give an approximate indication of the location.<br/>
						&nbsp;<br/>
						If you are entering many wells within the same field you may enter a single set of coordinates at the centre of the field for all wells.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">15</td>
					<td class="value">Drilling contractor</td>
					<td class="value">
						Enter the name of the drilling contractor / rig contractor. <br/>
						If more than one contractor was used, please give all the names, separated by commas.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">16</td>
					<td class="value">Rig name</td>
					<td class="value">
						Enter the name of the rig used to drilled the well. <br/>
						If more than one rig was used, please enter the names of all the rigs separated by commas.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">17</td>
					<td class="value">Owner drilled</td>
					<td class="value">
						Enter one of the following codes to show who drilled this well:<br/>
						&nbsp;<br/>
						1: Your company designed and drilled this well (employing a drilling contractor) on its own behalf.<br/>
						2: The well was designed and drilled for you by a drilling or management contractor, under a turnkey contract, or similar.<br/>
						3: The well was designed and drilled for you by a drilling or management contractor, under a non-turnkey contract.<br/>
						4: The well was designed and drilled for you by another operator.<br/>
						5: The well was designed by you but drilled by another operator on your behalf.<br/>
						6: Your company designed and drilled this well for another operator.<br/>
						7: Your company drilled this well for another operator, to their design.<br/>
						8. The well was designed by you, but drilled by a drilling or management contractor under a non-turnkey contract.<br/>
						&nbsp;<br/>
						If you have entered any of the codes from 4 - 8, please give the name of the other operator in Further Details (Column 120)<br/>
					</td>
				</tr>
				
				<tr>
					<td class="value">18</td>
					<td class="value">Hole Type</td>
					<td class="value">
						Please enter one of the following codes to indicate the Hole type:<br/>
						N - New well<br/>
						G - Geological sidetrack well<br/>
						S - Slot recovery / slot enhancement well.<br/>
						&nbsp;<br/>
						A New well (N) is a well planned and drilled from a spud point at the seabed or bottom of cellar.<br/>
						&nbsp;<br/>
						A Geological sidetrack (G) is a well planned and drilled from the bore of another well in order to achieve a geological objective.<br/>
						A planned geological sidetrack is one that is planned and engineered prior to spud, and will often have a separate AFE to its parent well.<br/>
						&nbsp;<br/>
						A Slot recovery or slot enhancement well (S) is a new well which kicks off from some point in the bore of a previously used (either producing or injecting) well.<br/>
						This may be<br/>
						- a well drilled from a subsea template slot / casing used previously by a production / injection well which is now abandoned<br/>
						- a deepening of a production / injection well<br/>
						- an exploratory branch is drilled from a production / injection well<br/>
					</td>
				</tr>
				<tr>
					<td class="value">19</td>
					<td class="value">Locator well</td>
					<td class="value">
						Enter "Y" if this well was planned and drilled as a locator well, otherwise enter "N".<br/> 
						Do not leave this field blank.<br/>
						&nbsp;<br/>
						A Locator well is a well designed to penetrate the reservoir in order to accurately locate its position or carry out other geological investigation, and then to be sidetracked for production purposes.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">20</td>
					<td class="value">Multi-lateral</td>
					<td class="value">
						Enter 'Y' if this well is a multi-lateral well, otherwise enter 'N'.<br/>
						&nbsp;<br/>
						A multilateral well is one with two or more producing or injecting bores connected together down hole and produced via a single wellhead.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">21</td>
					<td class="value">Junction type</td>
					<td class="value">
						For multi-lateral wells enter a code to show the multi-lateral junction type (as defined by TAML)<br/>
						&nbsp;<br/>
						1.   Open / unsupported junction<br/>
						2.   Mother-bore cased and cemented, lateral open<br/>
						3.   Mother-bore cased and cemented, lateral cased but not cemented<br/>
						4.   Mother-bore and lateral cased and cemented<br/>
						5.   Pressure integrity at junction with packer elements<br/>
						6.   Pressure integrity at junction - casing seal<br/>
    					 	 or<br/>
     						 Down hole splitter - large main bore with 2 smaller wellbores of equal size<br/>
						&nbsp;<br/>
						If your well has more than one junction type, please enter them all separated by commas.<br/>
						&nbsp;<br/>
					 	For further information on TAML classification see <a href="http://www.taml-intl.org/?q=node/76" target="_blank">http://www.taml-intl.org/?q=node/76</a><br/>
					</td>
				</tr>
				<tr>
					<td class="value">22</td>
					<td class="value">Number of laterals</td>
					<td class="value">
						For multi-lateral wells please enter the number of laterals which are producing, or capable of producing.<br/>
						Examples:<br/>
						- for a multi-lateral well with a producing 'parent hole' and one producing lateral you enter '2'.<br/>
						&nbsp;<br/>
						- Where the 'parent hole' is not capable of producing but there are four producing laterals enter '4'.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">23</td>
					<td class="value">Number of contingency geological sidetracks</td>
					<td class="value">
						Enter the number of contingency geological sidetracks drilled in this well, or zero if there are no contingency geological sidetracks.<br/>
						&nbsp;<br/>
						A contingency geological sidetrack is a short sidetrack which is planned prior to spud, but only as a contingency. It is drilled in order to regain the reservoir, in a thin or highly faulted reservoir. It is normally part of the parent AFE.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">24</td>
					<td class="value">Number of mechanical sidetracks</td>
					<td class="value">
						Enter the number of mechanical sidetracks in this well, or zero if there are no mechanical sidetracks.<br/>
						&nbsp;<br/>
					 	A mechanical sidetrack (also called a technical sidetrack or bypass) is an unplanned sidetrack drilled to pass an obstruction or to improve the well path.<br/>
					 </td>
				</tr>
				<tr>
					<td class="value">25</td>
					<td class="value">Re-spudded</td>
					<td class="value">
						Enter 'R' if this is the second attempt to drill this well. The first attempt did not reach its objective because of a fault or failure of the drilling group and was <b>entirely</b> abandoned.<br/>
						&nbsp;<br/>
						All time spent drilling the original well should be reported as NPT (cols 112-119) in the re-spudded well.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">26</td>
					<td class="value">Original well name for re-spud well</td>
					<td class="value">
						If this is a re-spudded well (marked 'R' in previous column), please enter the name of the well which failed to reach its geological objective and was abandoned.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">27</td>
					<td class="value">Well type</td>
					<td class="value">
						Please enter one of the codes below to indicate what type of well this is:<br/>
						&nbsp;<br/>
						E : Exploration<br/>
						A : Appraisal or Delineation (includes SAGD 'core hole' wells)<br/>
						D : Development or Production<br/>
						&nbsp;<br/>
						If a development well is drilled beyond its development TD in order to explore further target zones (but this work is carried out within 1 AFE) the data may be entered as 1 row with well type "development".<br/>
						In this situation, please give an explanation in the further details column (Column 120).<br/>
					</td>
				</tr>
				<tr>
					<td class="value">28</td>
					<td class="value">Play type</td>
					<td class="value">
						Please enter one of the codes below to describe the well:<br/>
						&nbsp;<br/>
						<b>H</b> : Conventional hydrocarbon<br/>
					 	<b>C</b> : Coal bed methane / coal seam gas<br/>
						<b>D</b> : Steam-Assisted Gravity Drainage (SAGD)<br/>
						<b>GA</b> : Geothermal - hot sedimentary aquifers<br/>
						<b>GE</b> : Geothermal - enhanced geothermal systems<br/>
						<b>GV</b> : Geothermal - volcanic<br/>
						<b>S</b> : Shale oil or shale gas<br/>
						<b>O</b> : Other  - please give details in further details column (column 120)<br/>
						&nbsp;<br/>
					</td>
				</tr>
				<tr>
					<td class="value">29</td>
					<td class="value">High Pressure</td>
					<td class="value">
						If this well is in a formation with pore pressures of 10,000 psi or greater, please enter "HP" into this column.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">30</td>
					<td class="value">High Temperature</td>
					<td class="value">
						If the temperature of this well at TD is 300 degrees F or higher (150 degrees C), please enter "HT" into this column.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">31</td>
					<td class="value">Rig type</td>
					<td class="value">
						Please enter a code to indicate what type of rig was used to drill the well. If more than one rig type was used, please enter all the appropriate codes in the order they were used in, separated by commas.<br/>
						<b>Land rigs</b><br/>
						&nbsp;&nbsp;LA: Land rig (rental)<br/>
						&nbsp;&nbsp;LO: Land rig (owned by Operator)<br/>
						&nbsp;&nbsp;HR: Heli-rig<br/>
						&nbsp;&nbsp;OL: Other type of land rig - Please give details in further details (Column 120)<br/>
						<b>Shallow barge rigs</b><br/>
						&nbsp;&nbsp;SU: Submersible (a barge type vessel that floats into place, but sits on bottom during drilling)<br/>
						&nbsp;&nbsp;BA: Barge (a barge type vessel that is floating during drilling)<br/>
						<b>Offshore rigs</b><br/>
						&nbsp;&nbsp;JK: Jack-up<br/> 
						&nbsp;&nbsp;JP: Jack-up positioned over, and drilling through, a platform<br/>
						&nbsp;&nbsp;HP: Hydraulic workover unit on, and drilling through, a platform rig<br/>
						&nbsp;&nbsp;PL: Platform - rigid leg<br/>
						&nbsp;&nbsp;PT: Platform - tethered leg (TLP)<br/>
						&nbsp;&nbsp;PS: Spar platform rig<br/>
						&nbsp;&nbsp;SP: Permanently moored vessel for production with integrated drilling facilities (e.g. FDPSO)<br/>
						&nbsp;&nbsp;TB: Platform tender-assisted barge type vessel <br/>
						&nbsp;&nbsp;TS: Platform tender-assisted semi-sub type vessel<br/>
						&nbsp;&nbsp;TJ: Platform tender-assisted jack-up type vessel<br/>
						&nbsp;&nbsp;SS: Semi-Submersible <br/>
						&nbsp;&nbsp;DS: Drillship<br/>
						&nbsp;&nbsp;OO: Other type of offshore rig - Please give details in further details (Column 120)<br/>
						&nbsp;<br/>
						If the rig is a <b>dual activity rig</b>, show <b>(2)</b>, including the brackets, after the rig type code.<br/>
						E.g. SS (2) for a dual activity semi-submersible rig.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">32</td>
					<td class="value">Drilling method</td>
					<td class="value">
						Please enter one or more of the codes below to indicate the methods used to drill this well.<br/>
						For example, for a well which was drilled using a top drive to reservoir, and the reservoir drilled using coil tubing, you would enter 'TC'.<br/>
						&nbsp;<br/> 
						<i>(As most drilling of <b>new</b> wells will use either rotary kelly drive or rotary top drive we would expect to see "R" or "T" given in the list of codes.)</i><br/>
						&nbsp;<br/>
						R : Rotary kelly driven<br/>
						T : Rotary top drive driven<br/>
						A : Rotary steerable<br/>
						M : Non-rotary steerable (mud motor)<br/>
						C : Coil tubing<br/>
						H : Hammer drilling<br/>
						P : Percussion drilling<br/>
						Y : Through-tubing rotary drilling<br/>
						B : Through-tubing coil tubing drilling<br/>
						O : Other method - please give details in further details (Column 120)<br/>
						&nbsp;<br/>
						For <u>Rotary Casing Drilling</u>, please use these codes:<br/>
						&nbsp;&nbsp;DX if used to install 1 or more casings, but <b>not</b> the casing in the final hole section<br/>
						&nbsp;&nbsp;DZ if used to install the casing string in the final hole section<br/> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. there was no further drilling after this casing was installed.)<br/>
						&nbsp;<br/>
						For <u>Down hole Motor Casing Drilling</u>, please use these codes:<br/>
					 	&nbsp;&nbsp;EX if used to install 1 or more casings, but <b>not</b> the casing in the final hole section<br/>
					 	&nbsp;&nbsp;EZ if used to install the casing string in the final hole section <br/>
					 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e. there was no further drilling after this casing was installed.)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">33</td>
					<td class="value">Units of measurement</td>
					<td class="value">
						If your data has been reported in metres please enter 'M', or if you have used feet please enter 'F'.<br/>
						Please ensure the Time Depth data and Time Depth charts are entered in the same units of measurement.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">34</td>
					<td class="value">Water depth / land well drill floor elevation</td>
					<td class="value">
						<u>For offshore wells:</u><br/>
						- Please enter the water depth.<br/>
						&nbsp;<br/>
					 	<u>For land wells:</u><br/>
						- Optionally, enter the drill floor elevation above sea level.<br/>
						(This will be shown in a separate field on the website.)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">35</td>
					<td class="value">Spud depth</td>
					<td class="value">
						Please enter the depth of the spud point, measured from the rotary kelly bushing (RKB) on the rotary table. Spud depths are defined below:<br/>
						&nbsp;<br/>
					 	<u>For new offshore wells:</u><br/>
						- The spud point is at the sea bed (not at the bottom of any pre-set or batch-set casings).<br/>
						&nbsp;<br/>
						<u>For new land wells </u><br/>
						- The spud point it is at the bottom of the cellar - not at the bottom of any casing pre-set during site preparation.<br/>
					 	&nbsp;<br/>
						<u>For geological sidetrack / slot recovery wells </u><br/>
						- Spud is taken as the first new hole section drilled<br/> 
						- In a cased hole this will be the start of drilling outside the milled-out window by the drilling assembly<br/>
						&nbsp;<br/>
						<u>For shared / split conductors or "side by side" wells:</u><br/>
						- if this is the second of 2 new wells drilled through a shared conductor please click below for help<br/>
					</td>
				</tr>
				<tr>
					<td class="value">36</td>
					<td class="value">MTD of final well bore</td>
					<td class="value">
						<u>For all wells except locator or multilateral wells:</u><br/>
						- Please enter the measured depth from the rotary table to the end of the well (TD) along the final well bore (including any sump or rat hole).<br/>
					 	&nbsp;<br/>
					 	<u>For locator wells:</u><br/>
						- The MTD is measured along the final well bore, from the rotary table to the TD of the sidetrack through the reservoir.<br/>
						&nbsp;<br/>
						<u>For multilateral wells:</u><br/>
						- The MTD is measured along the well bore from the rotary table to the end of the parent hole, plus the combined distances from the junction point to the TD of each lateral.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">37</td>
					<td class="value">Unused lengths due to contingency geological sidetracks</td>
					<td class="value">
						This only applies to wells with Contingency Geological Sidetracks.<br/>
						Please enter the total unused footage which occurred due to contingency geological sidetracks (not technical sidetracks).<br/>
					</td>
				</tr>
				<tr>
					<td class="value">38</td>
					<td class="value">Locator parent hole unused length</td>
					<td class="value">
						This only applies to Locator wells.<br/>
						&nbsp;<br/>
						Please enter the footage from the kick-off point of the sidetrack to the TD of the pilot hole.<br/>
						This is the 'unused' or abandoned footage, which is not part of the final well bore.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">39</td>
					<td class="value">TVD</td>
					<td class="value">
						Please enter the vertical depth from the rotary table to the end of the well (TD).<br/>
						&nbsp;<br/>
						<u>For locator wells:</u><br/>
						- The vertical depth is measured to the TD of the sidetrack, not to the TD of the parent hole.<br/>
						&nbsp;<br/>
						<u>For wells with contingency geological sidetracks</u><br/> 
						- This is measured to the end of the final well bore.<br/>
						&nbsp;<br/>
						<u>For multilateral wells</u><br/> 
						- The vertical depth is measured to the TD of the deepest lateral drilled.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">40</td>
					<td class="value">Sub salt</td>
					<td class="value">
						Please enter 'Y' if this well was drilled through a salt formation and 'N' if not.<br/>
						&nbsp;<br/>
						<u>For geological sidetracks or slot recovery wells that kick off <b>below</b> the salt formation</u><br/>
						If this well was not drilled through salt, this is not defined as "subsalt" for the purposes of the DPR.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">41</td>
					<td class="value">TVD at start of salt</td>
					<td class="value">
					 	If the well was drilled through one or more salt formations, please enter the vertical depth from the rotary table to the start of first drilling into salt.
					</td>
				</tr>
				<tr>
					<td class="value">42</td>
					<td class="value">TVD at end of salt</td>
					<td class="value">
						If the well was drilled through one or more salt formations, please enter the vertical depth from the rotary table to the point where you finally drilled out of salt.<br/>
					 	&nbsp;<br/>
						<u>For wells drilled through two or more salt formations</u><br/>
						- give the number of salt zones and the total vertical length of non-salt sections in the further details column (column 120).<br/>
					</td>
				</tr>
				<tr>
					<td class="value">43</td>
					<td class="value">Complex geometry</td>
					<td class="value">
						If, in your opinion, this well has complex geometry please enter 'Y', or enter 'N' if not.<br/> 
						If you enter 'Y', please provide a well path diagram when you submit this well.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">44</td>
					<td class="value">Maximum angle in degrees</td>
					<td class="value">
						Please enter the maximum angle of any hole section drilled in the final well bore (e.g. not in an abandoned section).<br/>
						The maximum angle may be outside the reservoir section - for instance in an 'S' shape well.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">45</td>
					<td class="value">Total length of horizontal sections</td>
					<td class="value">
						Please enter the total combined length of any sections of the final well bore which were drilled at an angle of 85 degrees or greater.<br/>
						This may include sections both within and outside the reservoir.<br/>
						&nbsp;<br/>
						<u>For multilateral wells</u><br/>
						Give the total horizontal length for all laterals drilled<br/>
						&nbsp;<br/>
						<u>For geological sidetracks or slot recovery wells</u><br/>
						- only count horizontal sections below the kick-off point.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">46</td>
					<td class="value">Final drill bit size in inches</td>
					<td class="value">
						Please enter the size (in inches) of the final drill bit used across the reservoir or final formation drilled.<br/> 
						If this well was drilled with a bi-centred bit please give the final (larger) hole size.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">47 - 57</td>
					<td class="value">Pre-existing casing strings</td>
					<td class="value">
					 	<b><u>Geological sidetrack and Slot recovery wells.</u></b><br/>
					 	&nbsp;<br/>
						Please enter, in decimal inches, the sizes of conductor and casing strings that were in place prior to drilling the new hole for the slot recovery or geological sidetrack.<br/>
						Give each string size in a new column.<br/>
						&nbsp;<br/>
						<u>For combination strings:</u><br/>
						- Please enter the sizes in one column using a forward slash (/) to indicate the larger and smaller sizes, e.g.: 10.25/9.625<br/>
						&nbsp;<br/>
						<u>For expandable casings:</u><br/>
						- Please enter the expanded size followed by the letter "e" e.g. "9.625e"<br/>
						&nbsp;<br/>
						<u>For shared / split conductors or "side by side" wells:</u><br/>
						- if this is a geological sidetrack or slot recovery well drilled from a well with a split conductor please note this in the comments column.<br/>
						- if this is the second of 2 new wells drilled through a shared conductor please click below for help<br/>
					</td>
				</tr>
				<tr>
					<td class="value">58</td>
					<td class="value">Conductor casing</td>
					<td class="value">
						<b><u><i>New and Batch set wells only</i></u></b><br/>
						&nbsp;<br/>
						Please enter the size of the conductor casing (also known as drive pipe / stove pipe / structural casing / conductor pipe)<br/>
						You should give this size even though the conductor may have been installed by the civil engineering or platform construction contractor during site preparation or platform installation.<br/>
						&nbsp;<br/>
					 	<u>For shared / split conductors or "side by side" wells:</u><br/>
						- If this conductor is a "splitter" conductor, where two or more wells drilled from surface share the same conductor casing, please add the letter "s" after the conductor size.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">59</td>
					<td class="value">Conductor installed by drilling rig?</td>
					<td class="value">
						<b><u><i>New and Batch set wells only</i></u></b><br/>
						Please enter 'Y' if the conductor casing was installed by a drilling rig, or 'N' if not.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">60 - 69</td>
					<td class="value">New casing strings and drilling liners</td>
					<td class="value">
						Please enter, in decimal inches, the sizes of all casings which were installed during the drilling of this well.<br/>
						&nbsp;<br/>
						<u>Do not include</u><br/>
						- any casing or liner which was run after the well reached TD.<br/>
						- the conductor casing, as this is entered in column 58.<br/>
						&nbsp;<br/>
						<u>For combination strings:</u><br/>
						- Please enter the sizes in one column using a forward slash (/) to indicate the larger and smaller sizes, e.g.: 10.25/9.625<br/>
						&nbsp;<br/>
						<u>For expandable casings:</u><br/>
						- Please enter the expanded size followed by the letter "e" e.g. "9.625e"<br/>
						&nbsp;<br/>
					 	<!-- <u>For shared / split conductors or "side by side" wells:</u><br/> -->
					</td>
				</tr>
				<tr>
					<td class="value">70</td>
					<td class="value">Pressure balance</td>
					<td class="value">
						Please enter one of the codes below to indicate how this well was drilled with respect to pressure differential<br/>
						&nbsp;<br/>
						M : Managed-pressure drilling (MPD)<br/>
						U : Under-balanced, not MPD<br/>
						N : Over-balanced (Not MPD and not underbalanced)<br/>
						&nbsp;<br/>
					 	A well is drilled in under-balanced mode when the pressure of the fluid column is designed to be less than that of the expected formation pressure, allowing the well to flow during drilling.<br/>
						This is sometimes called 'drilling for kicks' and requires special equipment at the well site for control and safety purposes.
					</td>
				</tr>
				<tr>
					<td class="value">71</td>
					<td class="value">Drilling fluid</td>
					<td class="value">
					 	Please enter one or more codes to describe the drilling fluid(s) used in this well.<br/>
					 	&nbsp;<br/>
						W : Water Based Mud,<br/> 
						O : Oil Based Mud, <br/>
						S : Synthetic Mud, <br/>
						E : Ester Mud, <br/>
						M : Mist, <br/>
						F : Foam, <br/>
						A : Air, <br/>
						B : Brine.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">72</td>
					<td class="value">Mud weight units</td>
					<td class="value">
						Please enter one of the codes below to indicate the units which you use for reporting mud weights:<br/>
						&nbsp;<br/>
					 	sg - specific gravity (sg)<br/>
						gcm3 - grams per cubic centimetre (gm / cm3)<br/>
						kgL - kilograms per litre (kg / L)<br/>
						&nbsp;<br/>
						gL - grams per litre (gm / L)<br/>
						kgm3 - kilograms per cubic metre (Kg / m3) <br/>
						kpam - kilopascal per metre (kPa / m)<br/>
						&nbsp;<br/>
						ppg - pounds per gallon (ppg)<br/>
						pbbl - pounds per barrel (lb / bbl)<br/>
						&nbsp;<br/>
						pf3 - pounds per cubic foot (lb / ft3)<br/>
						psif - pounds per square inch per foot (psi / ft)<br/>
						psi100f - pounds per square inch per 100 ft (psi / 100ft)<br/>
						&nbsp;<br/>
					 	O - Other (specify in further details)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">73</td>
					<td class="value">Mud weight at TD</td>
					<td class="value">
						If mud has been used as the circulation fluid, please enter the mud weight at TD.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">74</td>
					<td class="value">Maximum mud weight</td>
					<td class="value">
						If mud has been used as the circulation fluid, please enter the maximum mud weight in any section of the well bore.<br/>
						Please do not give "pad" (pump and dump) mud weight .<br/>
					</td>
				</tr>
				<tr>
					<td class="value">75</td>
					<td class="value">Cuttings disposal</td>
					<td class="value">
					 	Please enter the methods used for cuttings disposal:<br/>
						R : Re-injection, <br/>
						S : Ship to shore / transport to landfill,<br/> 
						D : Discharge at site. <br/>
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">Coring, logging and under-reaming: The times given in columns 76 to 80 must also be counted in the dry hole days</th>
				</tr>
				<tr>
					<td class="value">76</td>
					<td class="value">Coring days</td>
					<td class="value">
						Please enter the number of days spent using a core barrel to drill out, or trying to drill out, core samples before TD was reached.<br/> 
						If no coring was carried out, please enter "0".<br/>
						&nbsp;<br/>
						Coring time <u>starts</u> with circulation prior to POOH to core, and <u>ends</u> just after drilling out the core rat hole.<br/> 
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time during coring activities.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Time spent on sidewall coring.<br/>
						- Coring sidetracks which were drilled after TD.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">77</td>
					<td class="value">Coring interval</td>
					<td class="value">
					 	Please enter the total length of well bore cored.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">78</td>
					<td class="value">Logging days not at TD</td>
					<td class="value">
						Please enter the number of days spent on logging, or trying to log, <u>before</u> TD was reached.<br/> 
						If no logging was carried out please enter "0".<br/>
						&nbsp;<br/>
						Logging time <u>starts</u> with circulation prior to POOH to log, and <u>ends</u> when the logging equipment is rigged down.<br/> 
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time during logging activities.<br/>
						- Time spent sidewall coring before TD.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Logging Whilst Drilling (LWD) / Formation Evaluation Whilst Drilling (FEWD) time.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">79</td>
					<td class="value">Logging days at TD</td>
					<td class="value">
						Please enter the number of days spent on logging, or trying to log, <u>after</u> TD was reached.<br/> 
						If no logging was carried out at TD, please enter "0".<br/>
						&nbsp;<br/>
						Logging time <u>starts</u> with circulation prior to POOH to log, and <u>ends</u> when the logging equipment is rigged down.<br/>
						&nbsp;<br/> 
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time during logging activities.<br/>
						- Time spent sidewall coring before TD.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Time spent depth logging for running guns.<br/>
						- Cased hole logging / evaluation at TD.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">80</td>
					<td class="value">Reaming / under-reaming (hole opening or widening) days</td>
					<td class="value">
						Enter the number of days spent reaming / under-reaming in any section <b>in order to enlarge or <u>open up</u> the hole</b>, where this is a <b>separate</b> operation, carried out before the start of completion operations.<br/>
						&nbsp;<br/> 
						This time includes making up the reaming / under-reaming bit, RIH, and carrying out the reaming / under-reaming operation.<br/> 
						The time ends when the reaming / under-reaming bit is returned to surface.<br/>
						&nbsp;<br/>
						If no under reaming was carried out, please enter "0".<br/>
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Time spent solely on <b>hole conditioning</b>.<br/>
						- Days spent reaming / under-reaming if this is done concurrently with drilling.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">81</td>
					<td class="value">Reamed / under-reamed interval(s)</td>
					<td class="value">
						Please enter the total length of well bore which was opened up or enlarged by reaming / under-reaming, where this was done as a separate operation to hole-making.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Length reamed / under-reamed if this is done concurrently with drilling.<br/>
						- Operations which were carried out purely for hole conditioning.<br/>
						- Do not count any hole section more than once.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">82</td>
					<td class="value">FEWD</td>
					<td class="value">
						Enter 'Y' if Formation Evaluation While Drilling (FEWD) was employed, or 'N' if not.<br/>
						&nbsp;<br/> 
						<u>FEWD includes:</u><br/>
						- resistivity and gamma-ray,<br/>
						- Logging while drilling (LWD)<br/>
						&nbsp;<br/>
						<u>FEWD excludes:</u><br/>
						'measurement while drilling' for positional / geometrical information<br/>
					</td>
				</tr>
				<tr>
					<td class="value">83</td>
					<td class="value">Age of deepest formation accessed</td>
					<td class="value">
						Please enter the one of the codes below to indicate the deepest formation accessed.<br/> 
						This may not be the same as the target formation.<br/>
						&nbsp;<br/>
						This data must be given for all development wells.<br/> 
						For exploration and appraisal wells you may enter 'N/A' if this data is very confidential.<br/>
						&nbsp;<br/>
						QHol : Quaternary Holocene<br/>
						QPlei : Quaternary Pleistocene<br/>
						TPlio : Tertiary Pliocene<br/>
						TMio : Tertiary Miocene<br/>
						TOli : Tertiary Oligocene<br/>
						TEoc : Tertiary Eocene<br/>
						TPal : Tertiary Palaeocene<br/>
						CUpp : Cretaceous Upper <br/>
						CLow : Cretaceous Lower<br/>
						JUpp : Jurassic Upper <br/>
						JMid : Jurassic Middle <br/>
						JLow : Jurassic Lower<br/>
						Tri : Triassic<br/>
						PUpp : Permian Upper<br/>
						PLow : Permian Lower<br/>
						Carb : Carboniferous<br/>
						Dev : Devonian<br/>
						Sil : Silurian<br/>
						Ordo : Ordovician<br/>
						Cam : Cambrian<br/>
						Pcam : Pre-Cambrian<br/>
						Other : please specify in further details (Column 120)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">84</td>
					<td class="value">New techniques</td>
					<td class="value">
						Please enter codes to indicate which of the following new techniques were used in drilling this well.<br/>
						(This field is optional)<br/>
						&nbsp;<br/>
						L : Low rheology mud<br/>
						R : Onshore operation centres for control of drilling processes<br/>
						S : Slimhole well (as defined by operator)<br/>
						W : Wired drill pipe / intelligent drill pipe<br/>
						X : Expandable casing used<br/>
						O : Other - please specify in further details (Column 120)
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">Pre-spud times: The times given in columns 85 - 88 are not included in the dry hole days</th>
				</tr>
					<tr>
					<td class="value">85</td>
					<td class="value">Rig move time</td>
					<td class="value">
						<b><u><i>Land wells only.</i></u></b><br/> 
						Enter the rig move time, in days.<br/>
						&nbsp;<br/>
						This is often the time between rig release on the previous well and spud of the current well.<br/>
						It can be operator-defined, provided the definition is given in the further details column (Column 120).<br/>
						&nbsp;<br/>
						This time is not counted in the dry hole days.<br/>
						&nbsp;<br/>
						This field is optional.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">86</td>
					<td class="value">Rig move within field?</td>
					<td class="value">
						<b><u><i>Land wells only.</i></u></b><br/> 
						If the rig move time is given in previous column, please enter "Y" if the rig move took place within the field, or "N" if the rig was moved from outside the field.<br/>
						&nbsp;<br/>
						This value is only required if you have entered the rig move time in the previous column.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">87</td>
					<td class="value">Geological sidetrack pre-spud & whipstock days</td>
					<td class="value">
						<b><u><i>Geological sidetrack wells only.</i></u></b><br/> 
						Please enter the number of days pre-spud spent setting a whipstock and / or drilling a ledge, and preparing to drill the geological sidetrack.<br/>
						&nbsp;<br/>
						This <u>starts</u> with picking up the whipstock to run in hole, or picking up the directional drilling assembly, and <u>ends</u> when drilling begins on the first new hole outside the milled window.<br/>
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Time spent on setting a whipstock on a slot recovery well.<br/>
						- Time spent on whipstock setting or ledge drilling time during the drilling of locator wells, multi-lateral wells, or wells with contingency / mechanical sidetracks.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">88</td>
					<td class="value">Slot recovery / slot enhancement pre-spud days</td>
					<td class="value">
						<u><b><i>Slot recovery / enhancement wells only.</i></b></u><br/>
						Please enter the time in days from the start of rig operations to recover the well, up until the spud of the new hole. This is also known as "decompletion" time.<br/>
						&nbsp;<br/>
						This may include operations such as killing the well, cutting and pulling the tubing, and setting the whipstock. Time spent to plug and abandon the original hole using the rig should also be included here.<br/>
						&nbsp;<br/> 
						<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Days spent on pre-spud operations without the rig. (see next column)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">89</td>
					<td class="value">Offline slot recovery operations</td>
					<td class="value">
						<u><b><i>Slot recovery / enhancement wells only. </i></b></u><br/>
						If all or part of slot recovery pre-spud operations were carried out off-line (i.e. without the rig being present) please enter "Y", otherwise enter "N".<br/> 
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">The dry hole period</th>
				</tr>
				<tr>
					<td class="value">90</td>
					<td class="value">Batch set and campaign drilled wells</td>
					<td class="value">
						Please use the following codes to describe your well:<br/>
						(See below for definitions)<br/>
					 		B: The well was "batch set / pre-set" by your company<br/>
							T: The well was "batch set / pre-set" under a turnkey or similar contract<br/>
					 		C: This well was "campaign drilled" by your company <br/>
					 		D: This well was "campaign drilled" under a turnkey or similar contract<br/>
					 		N: None of the above<br/>
					 	&nbsp;<br/>
						<b><u>Batch setting:</u></b><br/>
						Batch setting, or pre-setting, applies to wells which are pre-planned to have one or more hole sections drilled and cased, then before TD is reached, the drilling rig that did this work moves off location.<br/>
						&nbsp;<br/>
						Drilling and casing is then continued to TD at a later time, either with a different rig or with the same rig.<br/>
						&nbsp;<br/>
					 	However, when conductor casings and subsequent casings are installed by a <b>civil engineering or platform construction contractor</b> during site preparation or platform installation, this is not defined as batch setting.<br/>
					 	&nbsp;<br/>
						<u><b>Campaign Drilled:</b></u><br/>
						This applies to a group of new wells, where the drilling rig will drill and case the top section of each well one-by-one, then drill and case the second section of each well, and so on until all the wells have reached TD in a continuous operation.<br/>
						&nbsp;<br/>
						At least 2 hole sections are drilled in this way.<br/> 
						<i>Please note that a drilling campaign does not mean that a well is "Campaign drilled".<br/> 
						Do not use this field just to indicate that a well was part of a drilling campaign.</i><br/>
					</td>
				</tr>
				<tr>
					<td class="value">91</td>
					<td class="value">Spud date</td>
					<td class="value">
						Please enter the date that drilling started on the well (<i>dd-MMM-yy</i>)<br/>
						&nbsp;<br/>
					 	<u>New wells (including locator wells and batch set / campaign drilled wells):</u><br/>
						- Spud date is the date of spud at the seabed or cellar.<br/>
						&nbsp;<br/>
						<u>New wells where conductor casing was installed by a drilling rig before the start of drilling</u><br/>
						(i.e. by pile driving or other similar method)<br/>
						- spud date is the date when the conductor penetrated the seabed or bottom of cellar.<br/>
						&nbsp;<br/>
						<u>New wells where conductor casing was not installed by a drilling rig</u><br/>
						- spud date is the date drilling began out of the pre-installed casing.<br/>
						&nbsp;<br/> 
						<u>Geological sidetrack and Slot recovery wells:</u><br/>
						- Spud date is the date that the first new hole was drilled out of the pre-existing casing (with a drilling assembly, not a milling assembly).<br/>
						&nbsp;<br/>
						<u>For shared / split conductors or "side by side" wells:</u><br/>
						- if this is the second of 2 new wells drilled through a shared conductor please click below for help.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">92</td>
					<td class="value">Dry hole days</td>
					<td class="value">
						Please enter the number of days spent on drilling operations.<br/> 
						This begins at spud, and ends either when the bit is returned to the drill floor after TD, or at the end of logging / under-reaming at TD, if these operations were carried out (see below)<br/>
						&nbsp;<br/>
						<u><i>If logging was carried out at TD:</i></u><br/>
						- The dry hole days end when the logging tools are returned to the drill floor after TD.<br/>
						&nbsp;<br/>
						<u><i>If hole opening was carried out after TD:</i></u><br/>
						- The dry hole days end when the under-reaming equipment has been rigged down.<br/>
						&nbsp;<br/>
						<u><b>Include:</b></u><br/>
						- Days spent batch setting, pre-installing or pre-setting casings, when carried out by a <b>drilling</b> rig.<br/>
						- Non Productive time and Waiting on Weather (WOW) time during the dry hole period.<br/>
						- time spent in hole opening (but <b>not</b> hole conditioning) after TD is reached<br/>
					 	- time spent drilling a narrow gauge hole to check for shallow gas or shallow water hazards, (but only if the shallow gas / water hole and the main well were drilled by the same rig.)<br/>
						- time spent technical / mechanical sidetracking (bypassing) for any reason, including as a result of problems experienced in the completions phase that were caused in the drilling phase (for instance, hole geometry from high dogleg / hole instability).<br/> 
					 	- time spent running riser, BOP nipple up / nipple down, BHA running / tripping where these occur as part of planned batch setting or campaign drilling or any other <b>planned</b> suspension during the dry hole period.<br/>
					 	- Time spent in multi-lateral wells to set a whipstock or to create a ledge in order to drill ahead in a new direction.<br/>
					 	- Time spent to retrieve a whipstock, where this occurs before the dry hole end date. However, in a multilateral well if the whipstock is also used to install a liner or screens in a lateral, the time is not counted in the DPR.<br/>
					 	- For locator wells and wells with contingency geological sidetracks include all the time from spud to the end of TD logging on the final well bore, except for the exclusions listed below.<br/>
					 	&nbsp;<br/>
						<u><b>Do not include:</b></u><br/>
					 	- Time spent running the riser and installing the BOP <b>before</b> the start of drilling a new well.<br/>
						- Time spent on setting the conductor, if this operation was carried out by the civil engineering or platform construction contractor during site preparation or platform installation.<br/>
						- Time spent on hole conditioning after TD is reached<br/>
						- Time spent on running and cementing a production liner <b>after</b> TD.<br/>
						- Time during which the well was on suspension<br/>
						- Time spent on suspension and re-entry operations for an <b>unplanned</b> suspension.<br/>
						- Time spent on plug and abandonment operations after TD is reached<br/> 
						- Time spent on completion operations.<br/>
						- Time for suspensions due to extreme weather conditions (e.g. hurricanes / cyclones) during which the rig is shutdown.<br/>
						- Time to drill a narrow gauge hole to check for shallow gas, where this is not drilled by the rig which drilled the main well.<br/>
						- Time spent on a drill stem test (DST) except where this occurs out of the reservoir (e.g. checking for shallow gas).<br/>
						- For multi-lateral wells, any operational time spent on 'completion activities' (such as running a production liner) after a branch or lateral has reached TD.<br/>
						- Time spent on wiper trips prior to running a completion assembly.<br/>
						- Time spent on the installation of a horizontal / spool tree during the drilling phase.<br/>
						&nbsp;<br/>
						<u>For shared / split conductors or "side by side" wells:</u><br/>
						- if this is the second of 2 new wells drilled through a shared conductor please click below for help on how to allocate the time.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">93</td>
					<td class="value">Date of end of dry hole period</td>
					<td class="value">
						Please enter the date that drilling operations ended. (<i>dd-MMM-yy</i>)<br/>
						This will either be when the bit is returned to the drill floor after TD, or at the end of logging / under-reaming at TD, if these operations were carried out (see below)<br/>
						&nbsp;<br/>
						<i><u>If logging was carried out at TD:</u></i><br/>
						- The dry hole period ends when the logging tools are returned to the drill floor after TD.<br/>
						&nbsp;<br/>
					 	<i><u>If hole opening (under-reaming) was carried out after TD:</u></i><br/>
						- The dry hole period ends when the under-reaming equipment has been rigged down.<br/>
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">The time given in column 95 is <b>not</b> included in the dry hole days</th>
				</tr>
					<tr>
					<td class="value">94</td>
					<td class="value">Number of well suspensions</td>
					<td class="value">
					 	Please enter the number of times that the well was suspended within the dry hole period (i.e. between spud and TD). If no suspensions took place, please enter '0'.<br/>
					 	&nbsp;<br/>
						<u>Count as a suspension if:</u><br/>
						- a plug is set<br/>
						- the rig moves off location<br/>
						- drilling operations are stopped to allow for planned rig upgrade / maintenance.<br/>
						&nbsp;<br/>
						<u>Do not count:</u><br/>
						- the suspension of a land well after batch setting or pre-setting surface casing by the civils / site preparation contractor.<br/> 
					</td>
				</tr>
				<tr>
					<td class="value">95</td>
					<td class="value">Days spent on suspension / re-entry operations</td>
					<td class="value">
						Please enter the number of days which were spent on operations of suspending and re-entering this well, for suspensions which occurred during the dry hole period.<br/>
						&nbsp;<br/>
						<b>Include</b><br/> 
						- any non-productive time and waiting on weather time that occurred during suspension and re-entry.<br/>
						- if the well was suspended more than once give the total number of days for suspension and re-entry operations.<br/>
						&nbsp;<br/>
						<b>Do not include</b><br/> 
						- the number of days during which the well was suspended (the elapsed time)<br/>
						- times for suspensions which occurred before the spud date or after the dry hole end date<br/>
						&nbsp;<br/>
						<u>For planned suspensions:</u><br/>
						- The suspension operation starts when the first additional operation takes place in order to suspend the well and stops when the rig starts moving off location.<br/>
					 	- The re-entry operation starts at rig arrival on location and stops when operations are back where they were prior to the start of suspension.<br/>
					 	- Exclude the time spent running riser, BOP nipple up / nipple down, BHA running / tripping ( because it is counted in the dry hole days).<br/>
					 	&nbsp;<br/>
						<u>For unplanned suspensions</u><br/> 
						- The suspension operation starts at the first unplanned additional operation that took place in order to suspend the well and stops when the rig starts moving off location.<br/>
					 	- The re-entry operation starts at rig arrival on location and stops when operations are back where they were prior to the start of suspension.<br/>
						- Include time spent on riser, BOP and BHA operations.<br/>
						- Give the reason for the unplanned suspension in the further details column (Column 120)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">96</td>
					<td class="value">Total days at well site</td>
					<td class="value">
						Please enter the total number of days from rig arrival on location <b>on this well</b> until rig released from location.<br/> 
						If more than one rig was used in the construction of this well, give the total days for all rigs used on the well.<br/>
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Rig time spent on completions operations, suspension /re-entry operations or plug and abandonment operations, as reported in column 103.<br/>
						- Geological sidetrack or Slot recovery pre-spud days, as reported in columns 87-88.<br/>
						- Time spent on suspension and re-entry operations, as reported in column 95.<br/>
						- Mooring / de-mooring time, as reported in columns 108 - 109.<br/> 
						- Rig time spent on well testing or other operations covered by column 104<br/>
					 	- Time spent drilling a well to check for shallow gas or water hazards, even though drilled from a location some distance from the main well.<br/>
					 	&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Rig move time.<br/>
						- Any rig time for construction, production or accommodation duties.<br/>
						- Time when the well was suspended<br/>
						&nbsp;<br/>
						<u>What to do if you cannot provide the data as defined, for one of the reasons given below:</u><br/>
						- the well construction is not finished when you submit the drilling data.<br/>
					 	&nbsp;&nbsp;&nbsp;&gt; Leave this field blank and we will ask for the data at a later date.<br/>
					 	&nbsp;<br/>
						- you do not have times for batch-setting or other operations carried out a long time ago,<br/> 
						or by a different operator.<br/> 
					 	&nbsp;&nbsp;&nbsp;&gt; Give what you have and explain what is missing in the further details column.<br/>
					 	&nbsp;<br/>
					 	- You have a completed well with one or more associated geological sidetracks on separate rows of data, and you are unable to allocate "total well site" time to individual sidetracks.<br/>
					 	&nbsp;&nbsp;&nbsp;&gt; Enter the "total well site days" of the well and sidetracks on the row of the completed well bore.<br/>
					 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For the other well bores / sidetrack(s) 'total well site days' and 'dry hole days' can hold the same value.<br/>
					 </td>
				</tr>
				<tr>
					<td class="value">97</td>
					<td class="value">Dry Hole Cost</td>
					<td class="value">
						Please enter, in millions, the cost of operations which occurred during the 'dry hole days' period.<br/>
						Report this value in the currency that your company uses.<br/>
					 	If you do not have the final costs please give provisional / preliminary costs.<br/>
					 	&nbsp;<br/>
					 	<b><u>Include:</u></b><br/>
					 	- General overheads, base operations, staff overheads, incentive payments, logging, transport, materials, materials supply, marine vessel support, marine supply base, port facility and warehousing, as indicated in your overhead costs sheet.<br/>
					 	- Costs incurred during the batch-set, pre-install or pre-set period, provided this is done by the drilling rig.<br/>
						- The cost rig rental for the dry hole period, including supply boats and diesel fuel costs.<br/>
						- The cost of wellheads - except in re-entries, where the original wellhead is re-used.<br/>
						- The cost of drilling a narrow gauge hole as part of this well, to check for shallow gas or shallow water hazards.<br/>
						- The cost of drilling a narrow gauge hole some distance from this well, to check for shallow gas or shallow water hazards, <u>provided it is drilled by the same rig that drilled this well.</u><br/>
						- The cost of technical and mechanical sidetracks (bypasses)<br/>
						- The costs incurred on any contingency geological sidetracks, or the sidetrack within a locator well.<br/>
						- the cost for any riser / BOP / BHA operations which are included in the dry hole days.<br/>
						- The cost, up to the end of drilling, of installing casing / liner in the final hole section <u>if it is installed by casing drilling.</u><br/>
						- The rental cost of equipment held on the rig during drilling of exploration wells, as a contingency in the event of a commercial discovery, may be included.<br/>
						&nbsp;<br/>
					 	<u><b>Do not include:</b></u><br/>
					 	- Costs for well design and programming.<br/>
					 	- Costs for site survey and preparation / re-instatement.<br/>
					 	- Costs for rig mobilisation and demobilisation.<br/>
					 	- The cost of setting a conductor if this operation was carried out under a "civils" (site preparation) contract.<br/>
					 	- The cost of drilling a narrow gauge hole some distance from this well, to check for shallow gas or shallow water hazards, if this was drilled by a different rig to the one which drilled this well.<br/>
					 	- Completion and well test costs, including production strings / liners installed after TD, Xmas trees and completion equipment.<br/>
					 	- The costs of long-term daily rental of completions equipment held at the well site during the drilling phase.<br/>
					 	- The costs of installing of a horizontal / spool tree.<br/>
					 	- Costs for well suspension and re-entry, or plug and abandonment.<br/>
					 	&nbsp;<br/>
					 	<u>For shared / split conductors or "side by side" wells:</u><br/> 
						- if this is the second of 2 new wells drilled through a shared conductor please click below for help on how to allocate the costs.<br/>
						&nbsp;<br/>
					 	For a general guide of how you might allocate your costs please download this file:<br/>
					 	<a href="http://www.rushmorereviews.org/wbook/cprdpr/Cost-allocation-guidelines.zip">Cost allocation guidelines.zip</a></br>
					</td>
				</tr>
				<tr>
					<td class="value">98</td>
					<td class="value">Preliminary or final cost</td>
					<td class="value">
						Enter 'P' if the dry hole cost is preliminary and you expect to revise it at a later date, otherwise enter 'F' if the cost is final.<br/>
						If you give preliminary costs we will contact you at a later date to ask for the final costs.<br/>
						&nbsp;<br/>
					 	If final field costs are within +/-10% of the final ledger costs (also called booked accounting costs) they may be used as final costs for the purposes of this review.
					 </td>
				</tr>
				<tr>
					<td class="value">99</td>
					<td class="value">Completeness of dry hole cost</td>
					<td class="value">
						If you have entered a complete dry hole cost figure as defined in column 38 enter 'Y'.<br/>
						&nbsp;<br/>
						If you have entered a complete dry hole cost figure as defined in column 38, except that you cannot give the batch set or pre-set costs, enter 'B'.<br/>
						&nbsp;<br/>
					 	If some other costs are missing from the dry hole cost (other than the batch-setting costs) please enter 'N' and provide a list of the missing costs in Further details (col 120)<br/>
					 	&nbsp;<br/>
					 	This column should not be used to indicate that costs are still preliminary, but to show that you are unable to get some costs.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">100</td>
					<td class="value">Total well cost</td>
					<td class="value">
						Please enter, in millions, the total costs associated with this well, including the items excluded from the dry hole cost.<br/>
						Report this value in the currency that your company uses.<br/>
						&nbsp;<br/>
					 	<u>Include:</u><br/>
					 	- All the costs included in the dry hole cost<br/>
					 	- Costs for well design and programming.<br/>
					 	- Costs for site survey and preparation / re-instatement.<br/>
					 	- Rig move costs and civil works costs.<br/>
					 	- Costs for rig mobilisation and demobilisation.<br/>
					 	- Costs of a narrow gauge hole drilled by a separate rig some distance away from the main well before the main well was drilled, to assess shallow gas or water hazards.<br/>
					 	- Completion and well testing costs, including production strings and liners installed after TD logging, trees and completion equipment.<br/>
					 	- Costs for well suspension, re-entry and plug and abandonment.<br/>
						&nbsp;<br/>
					 	<u>Do not include:</u><br/>
					 	- Costs relating to any contingent wells that were not drilled, or contingencies on an individual well (for example, additional casing which was available but not used.)<br/>
					 	&nbsp;<br/>
					 	If the well is completed but the completion cost is not available at the time that you submit the drilling data we will not publish the total cost. We will ask for the completion cost at a later date.<br/>
					 	&nbsp;<br/>
					 	If a well has geological sidetracks but total well costs cannot be spread across the individual sidetracks the total cost may be allocated to the bore which was completed. Other bores can show the total cost as equal to the dry hole cost.<br/>
					 	&nbsp;<br/>
						If you are unable to provide the data exactly as defined, please enter what you have available and note any differences / omissions in the further details column (Column 120).<br/>
						&nbsp;<br/>
					 	For a general guide of how you might allocate your costs please download this file:<br/>
					 	<a href="http://www.rushmorereviews.org/wbook/cprdpr/Cost-allocation-guidelines.zip">Cost allocation guidelines.zip</a><br/>
					</td>
				</tr>
					<tr>
					<td class="value">101</td>
					<td class="value">Currency</td>
					<td class="value">
					 	Enter the currency you have used for reporting your well costs. E.g.<br/>
						&nbsp;<br/>
						USD : US dollars<br/>
						GBP : British Pounds <br/>
						EUR : Euros<br/>
						NOK : Norwegian Kroner<br/>
						DKK : Danish Kroner<br/>
						AUD : Australian Dollars<br/>
						NZD : New Zealand Dollars<br/>
						NGN : Nigerian Naira<br/>
						BND : Brunei Dollars<br/>
						&nbsp;<br/>
					 	For comparative performance charting purposes we will convert all costs into US dollars using the average Interbank Rate for the month in which the dry hole period ended.<br/>
						&nbsp;<br/>
						We will use the Olsen and Associates website www.oanda.com to obtain these rates
					</td>
				</tr>
				<tr>
					<td class="value">102</td>
					<td class="value">Current well status</td>
					<td class="value">
						Enter one of the following codes to show the status of the well at the time this data is submitted.<br/>
						&nbsp;<br/>
						CO : <b>permanently</b> completed or in process of being <b>permanently</b> completed<br/> 
						CS : <b>permanently</b> completed and subsequently suspended<br/>
						&nbsp;<br/>
						WT: well test (DST) or <b>temporary</b> completion in progress, final well status unknown at this time<br/>
						WC: well test (DST) or <b>temporary</b> completion in progress, well will be permanently completed later<br/>
						&nbsp;<br/>
						SUC : unplanned suspension or temporary abandonment : expect to complete well<br/>
						SUP : unplanned suspension or temporary abandonment : expect to P&A well<br/>
						SUU : unplanned suspension or temporary abandonment : final well status uncertain<br/>
						&nbsp;<br/>
						SPC : planned suspension or temporary abandonment : expect to complete well<br/>
						SPP : planned suspension or temporary abandonment : expect to P&A well<br/>
						SPU : planned suspension or temporary abandonment : final well status uncertain<br/>
						&nbsp;<br/>
						PP : planned plug and abandonment<br/> 
						PU : unplanned plug and abandonment but respud not necessary<br/>
						PS : plugged back down hole to allow geological sidetrack to be spudded<br/>
						&nbsp;<br/>
						RS : well completely abandoned back to its spud point with a re-spud necessary to achieve the objective of this well due to a fault of drilling group during drilling of this well<br/>
						&nbsp;<br/>
						RG : well completely abandoned back to its spud point with a re-spud necessary to achieve the objective of this well due to geological reasons and not fault of drilling group<br/>
						&nbsp;<br/>
						O : other, please specify in further details (Column 120)
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">The times given in columns 103 - 111 are <b>not</b> included in the dry hole days</th>
				</tr>
				<tr>
					<td class="value">103</td>
					<td class="value">PA, SU or completion days</td>
					<td class="value">
						<u><b>Plug and Abandonment days (for well status PP, PU, PS, RS or RG):</b></u><br/>
						If the well was plugged and abandoned after reaching TD enter the number of days spent on this operation, including circulation and clean-up time.<br/>
						&nbsp;<br/>
						(Where a well and sidetrack(s) are drilled as part of one AFE the operator may choose whether to allocate the time for abandoning the upper section of the hole, above the sidetrack kick-off point, to the first well or the last.)<br/>
						&nbsp;<br/>
						<b><u>Suspension days (for well status SUC, SUP, SUU, SPC, SPP or SPU):</u></b><br/>
						If the well was suspended or temporarily abandoned after reaching TD enter the number of days spent on this operation.<br/>
						&nbsp;<br/>
						<u><b>Completion rig days after TD (for well status CO or CS):</b></u><br/>
						If the completion operations have finished, enter the total rig days spent on completion operations.<br/>
						&nbsp;<br/>
					 	<i>"Completion operations" include:<br/> 
						running production liner or casing after TD;<br/> 
						wellbore preparation / clean-up<br/>
						installing sand control equipment;<br/> 
						running completion tubing and hanger;<br/>
						installing Xmas tree;<br/>
						stimulating; perforating;<br/> 
						move-off operations (subsea completions).</i><br/>
						&nbsp;<br/>
					 	If the completion operations are still in progress leave this field blank. We will ask you for the completion time at a later date.<br/>
					 	&nbsp;<br/>
					 	<u>In all cases, include:</u><br/>
					 	- Non Productive time and Waiting on Weather time<br/>
					 	&nbsp;<br/>
					 	<u>Do not include:</u><br/>
						- de-mooring or de-mobilisation time,<br/>
						- time when the well was suspended,<br/>
						- time for running a temporary completion or DST (well status WT or WC)<br/>
						- well testing (well status WT or WC)<br/>
					</td>
				</tr>
				<tr>
					<td class="value">104</td>
					<td class="value">Other operations days (after TD)</td>
					<td class="value">
						Please enter the number of days spent on other operations between the end of the dry hole period and rig release which have not been included in the previous columns.<br/>
						&nbsp;<br/>
					 	<u>Include:</u><br/>
						- Non Productive time and Waiting on Weather time.<br/>
						- Time spent installing a temporary completion / DST<br/>
						- rig time spent well testing<br/>
					 	- Time spent on running a liner in a well with final status of "suspended" - i.e. where the production liner was run after TD and then the well was suspended, include the time to run the liner here.<br/>
						- any de-mooring (or mooring) time after the end of the dry hole period.<br/>
						- any time spent tidying up the sea bed<br/>
						&nbsp;<br/>
					 	Please provide details of any operations which you have included here in Comments (col 121).<br/>
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">
						<b>Mooring details: Columns 105 to 111 only apply to offshore wells drilled by floating rigs.</b><br/>
						<font color="red">This data must be provided for all mobile offshore rigs that utilise a mooring system.</font>
					</th>
				</tr>
				<tr>
					<td class="value">105</td>
					<td class="value">"Date of end of well operations<br/>
					(Floating rigs only)"</td>
					<td class="value">
						Enter the date operations on this well were finished and the rig was ready to be released.<br/> 
						<i>(dd-MMM-yy)</i><br/>
						&nbsp;<br/>
						If well operations are still in progress when the DPR data was submitted, leave this field blank, and we will ask for the date at a later time.<br/>
						&nbsp;<br/>
					 	<i>This field applies to offshore wells only, and is only required if the rig will be de-mooring / moving away at the end of this well.</i><br/>
					</td>
				</tr>
				<tr>
					<td class="value">106</td>
					<td class="value">"WOW before de-mooring / move-off<br/>
						(Floating rigs only)"</td>
					<td class="value">
						If the start of rig de-mooring or rig move-off was delayed by poor weather conditions (WOW), please enter the number of days of WOW.<br/>
						&nbsp;<br/> 
						This is WOW after the end of well operations i.e. after the date given in the previous column.<br/>
						&nbsp;<br/>
					 	Do not include here any WOW that you have included elsewhere in the workbook, or WOW during the de-mooring operation itself, as this is given in column 110.<br/>
					 	&nbsp;<br/>
						If well operations are not yet finished leave this field blank and we will collect the data later.<br/>
						&nbsp;<br/>
					 	<i>This field applies to offshore wells only, and is only required if the rig will be de-mooring / moving away at the end of this well.</i><br/>
					</td>
				</tr>
				<tr>
					<td class="value">107</td>
					<td class="value">Rig mooring system</td>
					<td class="value">
						Please enter the appropriate code to indicate how the rig was moored.<br/>
						This is not required for jack-up or platform rigs.<br/>
						&nbsp;<br/>
						DP : Dynamic positioning<br/>
						C : Conventional<br/>
						PM : Pre-moored<br/>  
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Previously moored to drill an earlier well in the same or an adjacent location -<br/> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no need to move anchors)<br/>
						TL : Taut-leg<br/> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Only requires hook-up to pre-set anchors)<br/>
						O : Other<br/>
					</td>
				</tr>
				<tr>
					<td class="value">108</td>
					<td class="value">Days spent mooring</td>
					<td class="value">
						Enter the total number of days spent mooring.<br/>
						<i>Leave this field blank for pre-moored rigs, rigs with dynamic positioning, and for platform or jack-up rigs.</i><br/>
						&nbsp;<br/>
						Mooring starts at start to drop first anchor and ends when the rig is fully moored.<br/>
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time or Waiting on Weather time.<br/>
						- time spent hooking up a rig with a taut-leg mooring system.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Time spent preparing to moor before the start of actual mooring.<br/>
						- Time spent positioning a rig which was previously moored in order to drill an earlier well.<br/>
						&nbsp;<br/>
						If more than one rig is used give the cumulative time spent mooring.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">109</td>
					<td class="value">Days spent de-mooring</td>
					<td class="value">
						Enter the total number of days spent de-mooring.<br/>
						<i>Leave this field blank for rigs with dynamic positioning, and for platform or jack-up rigs.</i><br/>
						&nbsp;<br/>
						De-mooring begins at start to raise first anchor and ends when rig fully de-moored and ready to move away.<br/>
						&nbsp;<br/>
						<u>Include:</u><br/>
						- Non Productive time or Waiting on Weather time.<br/>
						- time spent un-hooking a rig with a taut-leg mooring system.<br/>
						&nbsp;<br/>
						<u>Do not include:</u><br/>
						- Times for pulling the riser.<br/>
						&nbsp;<br/>
						If more than one rig is used give the cumulative time spent de-mooring.<br/>
						&nbsp;<br/>
						When a well and sidetrack(s) are drilled in a single program or AFE the final de-mooring should be reported against the last sidetrack drilled.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">110</td>
					<td class="value">WOW during mooring</td>
					<td class="value">
						Enter the number of days spent waiting on weather during mooring operations.<br/>
						Include time spent waiting on sea conditions (including loop currents).<br/>
					</td>
				</tr>
				<tr>
					<td class="value">111</td>
					<td class="value">WOW during de-mooring</td>
					<td class="value">
						Enter the number of days spent waiting on weather during de-mooring operations.<br/>
						Include time spent waiting on sea conditions (including loop currents).<br/>
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3"><b><u>All wells</u> : Non Productive time (NPT) and Waiting on Weather time during the dry hole period.</b><br/>
						The times given in columns 112 - 113 must also be included in dry hole days.
					</th>
				</tr>
				<tr>
					<td class="value" colspan="3">
						Non Productive time (NPT) is time spent on un-planned or un-scheduled events, except "waiting on weather", as defined by your company.<br/>
						Time spent technical / mechanical sidetracking (bypassing) is considered to be NPT until the bit arrives at the equivalent point it was at before the problem occurred.
					</td>
				</tr>
				<tr>
					<td class="value">112</td>
					<td class="value">Waiting on weather during dry hole days</td>
					<td class="value">
						Enter the total days spent waiting on weather during the dry hole period.<br/> 
						&nbsp;<br/>
						<u>Include</u><br/>
						- time spent waiting on sea conditions (including loop currents) during the dry hole period.<br/>
						&nbsp;<br/>
						<u>Do not include</u><br/>
						- Extreme weather conditions (e.g. hurricanes / cyclones etc.) which result in rig shutdown.<br/> 
						These are counted as unplanned suspensions.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">113</td>
					<td class="value">Non-productive time (excluding WOW) during dry hole days</td>
					<td class="value">
						Enter the total days of non-productive time, however your company defines it, during the dry hole period. This may also be called trouble time / down time / interruption time etc.<br/>
						&nbsp;<br/>
					 	<u>Include</u><br/>
						- NPT during batch setting / pre-setting, coring and logging operations.<br/>
						&nbsp;<br/> 
						<u>Do not include</u><br/>
						- 'waiting on weather' time, or 'waiting on sea conditions' or time lost due to loop currents.<br/>
					</td>
				</tr>
				<tr>
					<th class="label" colspan="3">The non-productive time given in the previous column should also be reported in the following categories .<br/>
						i.e. The values in columns 114 - 118 should add up to the total NPT given in column 113.	
					</th>
				</tr>
				<tr>
					<td class="value">114</td>
					<td class="value">NPT due to rig contractor</td>
					<td class="value">
						Enter the number of non-productive days due to Rig Contractor equipment, personnel or procedures, during the dry hole period.<br/>
					 	For guidelines on types of NPT which might be included in this section<br/>
					 </td>
				</tr>
				<tr>
					<td class="value">115</td>
					<td class="value">NPT due to service company</td>
					<td class="value">
					 	Enter the number of non-productive days due to service company equipment, personnel or procedures during the dry hole period.<br/>
					 	For guidelines on types of NPT which might be included in this section<br/>
					</td>
				</tr>
				<tr>
					<td class="value">116</td>
					<td class="value">NPT due to operator problems</td>
					<td class="value">
					 	Enter the number of non-productive days due to the operating company equipment, personnel or procedures during the dry hole period.<br/>
					 	For guidelines on types of NPT which might be included in this section<br/>
					</td>
				</tr>
				<tr>
					<td class="value">117</td>
					<td class="value">NPT due to external problems</td>
					<td class="value">
					 	Enter the number of non-productive days due to External factors, outside the direct control of Operator, Rig or Service Company, during the dry hole period.<br/>
					 	For guidelines on types of NPT which might be included in this section
					</td>
				</tr>
				<tr>
					<td class="value">118</td>
					<td class="value">NPT due to down hole problems</td>
					<td class="value">
					 	Enter the number of non-productive days due to down hole operational, mechanical or geological problems during the dry hole period.<br/>
					 	For guidelines on types of NPT which might be included in this section<br/>
					</td>
				</tr>
				<tr>
					<td class="value">119</td>
					<td class="value">Main NPT event(s) in dry hole period</td>
					<td class="value">
					 	Please enter details of any NPT events during the dry hole period which caused non-productive time of one day or more.<br/>
					 	&nbsp;<br/>
					 	Please use the standard codes, followed by the time reported in days and parts of days.<br/>
					 	&nbsp<br/>
					 	If several shorter NPT events of the same type result in one or more days of NPT these should be reported with the one code.<br/>
					 	&nbsp;<br/>
					 	If a number of different problems result from one initial cause, all the NPT should be put against the code corresponding to the initial problem.<br/>
					 	&nbsp;<br/>
					 	<b><u>Example</u></b><br/>
					 	A well has problems with MWD tools on 2 occasions, one instance lasting 18 hours and one lasting 12 hours. On the same well stuck pipe results in twist-off, fishing and a technical sidetrack, amounting to 3 days of NPT.<br/>
					 	The NPT on this well is reported as MW:1.25D, SP:3D<br/>
					</td>
				</tr>
					<tr>
					<td class="value">120</td>
					<td class="value">Further details</td>
					<td class="value">
						Enter here details required to explain data given in any of the preceding columns<br/> 
						(e.g. if you entered 'Other' in a previous field, explain here what the 'other' is.)<br/>
						&nbsp;<br/>
						Please give the column number(s) of the field(s) to which your explanations apply.<br/>
						&nbsp;<br/>
					 	Use this field to give the previous well name, if the well has been re-named. For example, this may happen when an exploration well is re-designated as a development well.<br/>
					 	&nbsp;<br/>
						Also use this field to give the reason for, and duration of, any unplanned suspension.
					</td>
				</tr>
				<tr>
					<td class="value">121</td>
					<td class="value">Comments</td>
					<td class="value">
						Note any other significant features or aspects to the drilling of the well that will help other participants understand the data.<br/>
						&nbsp;<br/>
						Please make comments as self-explanatory as possible.<br/>
					</td>
				</tr>
				<tr>
					<td class="value">122</td>
					<td class="value">Comments 2</td>
					<td class="value">Additional comments</td>
				</tr>
				<tr>
					<th class="label" colspan="3">The following data field is only required for US wells, and will be ignored for other wells.</th>
				</tr>
				<tr>
					<td class="value">123</td>
					<td class="value">API well number</td>
					<td class="value">Enter the API well number</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

