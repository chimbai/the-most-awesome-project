package com.idsdatanet.d2.common.scheduler.jobs;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * Force Password change when scheduler is run.
 * Exclude IDS Admin (managed by Network), IDS User (LDAP user, password not stored by system)
 * Scheduler should be set to run once everyday
 * 
 * @author mhchai
 *
 */
public class CheckChangePassword {

	private Log logger = LogFactory.getLog(this.getClass());
	private String passwordValidDays;

	public String getPasswordValidDays() {
		return passwordValidDays;
	}

	public void setPasswordValidDays(String passwordValidDays) {
		this.passwordValidDays = passwordValidDays;
	}

	public void run() throws Exception{
		
		try {
			List<User> users = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from User where (isDeleted = false OR isDeleted is null)");
			
			Date currentDate = new Date();
			for (User user: users) {
				if (user.getPasswordChangedDate() != null) {
					long differenceInMillis = currentDate.getTime() - user.getPasswordChangedDate().getTime();
					//if days difference > 60 / day configured, set password changed required
					
					if (passwordValidDays == null || "".equals(passwordValidDays)) {
						passwordValidDays = "60";
					}
					if ((differenceInMillis / (1000 * 60 * 60 * 24)) > Integer.valueOf(passwordValidDays)) {
						user.setPasswordChangeRequired(Boolean.TRUE);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(user);
					}
				}
			}
			
		} catch (Exception ex) {
			this.logger.error(ex.getMessage(), ex);
		}
	}
	
}