<@showFormTitleInContentPage classType="Operation" />

<table class="inner-table">
	<#if form.root.list.Operation?exists>
		<#list form.root.list.Operation?values as item>
			<tr <@template item/>>
				
				<td>
					  <table width="100%">
						        	<tr>
										<th class="label" width="20%">Label</th>
										<th class="label" width="35%">Item</th>		
										<th class="label" width="45%">Uid</th>
										
									</tr>
									<tr>
										<th class="label" width="20%">Well Name</th>							
										<td class="value" width="35%"><@showField node=item dynamic="@wellName"/></td>
										<td class="value" width="45%"><@showField node=item field="wellUid"/></td>
									</tr>
									<tr>
										<th class="label" width="20%">Wellbore Name</th>
										<td class="value" width="35%"><@showField node=item dynamic="@wellboreName"/></td>							
										<td class="value" width="45%"><@showField node=item field="wellboreUid"/></td>
									</tr>
									<tr>
										<th class="label" width="20%">Operation Name</th>
										<td class="value" width="35%"><@showField node=item field="operationName"/></td>							
										<td class="value" width="45%"><@showField node=item field="operationUid"/></td>
									</tr>
									<tr>
										<th class="label" width="20%">Day (Date)</th>
										<td class="value" width="35%"><@showField node=item dynamic="@dayNumber"/><@showField node=item dynamic="@dayDate"/></td>							
										<td class="value" width="45%"><@showField node=item dynamic="@dailyUid"/></td>
									</tr>
					</table>
				</td>
			</tr>											
		</#list>
	</#if>	
</table>

