package com.idsdatanet.d2.common.scheduler.jobs;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.util.DocParserUtils;
import com.idsdatanet.depot.d2.filebridge.FileBridgeRestClientPoint;
import com.idsdatanet.depot.d2.filebridge.FileBridgeWebSocketClientPoint;
import com.idsdatanet.depot.d2.filebridge.RestClientEndpoint;
import com.idsdatanet.depot.d2.filebridge.WebSocketClientEndpoint;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;
import com.idsdatanet.depot.d2.thirdparty.DocParser;

public class FileBridgeJob implements D2Job {

	DocParser docParser = null;
	RestClientEndpoint fileBridgeRestClient = null;
	WebSocketClientEndpoint fileBridgeWebSocketClientPoint = null;
	
	public FileBridgeJob() {}
	public FileBridgeJob (SchedulerJobDetail jobDetail) throws Exception {
		String serverPropertiesUid = jobDetail.getServerPropertiesUid();
		if(StringUtils.isNotBlank(serverPropertiesUid)) {
			ImportExportServerProperties server = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
			if(server.getDataTransferType().equals("fbc")) {
				if(FileBridgeUtils.FILEBRIDGE_JOB_VERSION.equals(server.getDepotVersion())) {
					docParser = DocParserUtils.getConfiguredInstance().getDocParserProperties();
					docParser.setBaseUrl(server.getEndPoint());
					docParser.setUsername(server.getUsername());
					docParser.setPassword(server.getPassword());
				} else if(FileBridgeUtils.FILEBRIDGE_2x_JOB_REST_VERSION.equals(server.getDepotVersion())) {
					if(FileBridgeRestClientPoint.getConfiguredInstance().getClientEndpoint() == null) {
						FileBridgeRestClientPoint.getConfiguredInstance().setClientEndpoint(new RestClientEndpoint(server.getUsername(), server.getPassword(), server.getEndPoint()));
					} else {
						FileBridgeRestClientPoint.getConfiguredInstance().getClientEndpoint().setBaseUrl(server.getEndPoint());
						FileBridgeRestClientPoint.getConfiguredInstance().getClientEndpoint().setUsername(server.getUsername());
						FileBridgeRestClientPoint.getConfiguredInstance().getClientEndpoint().setPassword(server.getPassword());
					}
					
					fileBridgeRestClient = FileBridgeRestClientPoint.getConfiguredInstance().getClientEndpoint();
				} else if(FileBridgeUtils.FILEBRIDGE_2x_JOB_WS_VERSION.equals(server.getDepotVersion())) {
					if(StringUtils.isBlank(jobDetail.getMiscConfig())) throw new Exception("Socket configuration NOT Set!!");
					if(FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection() == null) {
						FileBridgeWebSocketClientPoint.getConfiguredInstance().setActiveWsConnection(new WebSocketClientEndpoint());
					}
					
					FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection().updateConn(server, jobDetail.getMiscConfig());
					fileBridgeWebSocketClientPoint = FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection();
				}
			}
		} else {
			docParser = DocParserUtils.getConfiguredInstance().getDocParserProperties();
		}
		
	}
	
	@Override
	public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
		/*String errorMsg = FileBridgeUtils.checkEnabledFileBridgeJob();
		if(StringUtils.isNotBlank(errorMsg)) {
			throw new Exception(errorMsg);
		}*/
		
		if(docParser != null) {
			if (!docParser.isRunning()) {
				try {
					docParser.start();
					docParser.collectOfflineFilesIfAny();
					docParser.sendToParser();
					docParser.receiveFromParser(userContext.getUserSelection(), jobResponse);
				} finally {
					docParser.stop();
				}
			}
		} else if(fileBridgeRestClient != null) {
			fileBridgeRestClient.run(userContext, jobResponse);
		} else if(fileBridgeWebSocketClientPoint != null) {
			fileBridgeWebSocketClientPoint.run(userContext, jobResponse);
		}
	}

	@Override
	public void dispose() {}

}