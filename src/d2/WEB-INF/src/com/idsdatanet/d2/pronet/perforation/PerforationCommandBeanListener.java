package com.idsdatanet.d2.pronet.perforation;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.Perforation;
import com.idsdatanet.d2.core.model.PerforationGunConfiguration;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PerforationCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(Perforation.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			//GET PERFORATION GUN CONFIGURATION DATA
			if("perforationGunConfigurationUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				
				String perforationGunConfigurationUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "perforationGunConfigurationUid");		
				PerforationUtils.populateGunDetailToDynaAttr(targetCommandBeanTreeNode, "perforationGunConfigurationUid = '" + perforationGunConfigurationUid + "'");

			}
		}
	}
	
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom converter = new CustomFieldUom(commandBean);
		
		// set units for standard fields in CasingSection.class
		for (Field field : PerforationGunConfiguration.class.getDeclaredFields()) {
			converter.setReferenceMappingField(PerforationGunConfiguration.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(Perforation.class, "@perfGunConf." + field.getName(), converter.getUOMMapping());
			}
		}
	}
}
