package com.idsdatanet.d2.pronet.keyPerformanceIndicator;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.FracSheet;
import com.idsdatanet.d2.core.model.KeyPerformanceIndicator;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class KeyPerformanceIndicatorDataNodeListener extends EmptyDataNodeListener{
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		//get operation type from Frac Sheet screen 
		if (obj instanceof KeyPerformanceIndicator){
			KeyPerformanceIndicator kpi = (KeyPerformanceIndicator) obj;
			
			FracSheet fracSheet = (FracSheet) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FracSheet.class, kpi.getFracSheetUid()); 
			if (fracSheet!=null){
				String fracSheetOperationType = fracSheet.getOperationType();
				node.getDynaAttr().put("fracOperationType", fracSheetOperationType);
				node.getDynaAttr().put("zoneNo", fracSheet.getJobNumber());
			}	
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof KeyPerformanceIndicator){
			KeyPerformanceIndicator kpi = (KeyPerformanceIndicator) obj;
			
			//get mobScore
			Double mobScore = KeyPerformanceIndicatorUtils.getScore(kpi.getMobFrequencyActual(), kpi.getMobFrequencyDesigned());
			kpi.setMobScore(mobScore);
			
			//get Rig Up Score
			Double rigUpScore = KeyPerformanceIndicatorUtils.getScore(kpi.getRigUpDurationActual(), kpi.getRigUpDurationDesigned());
			kpi.setRigUpDurationScore(rigUpScore);
			
			//get Rig down  Score
			Double rigDownScore = KeyPerformanceIndicatorUtils.getScore(kpi.getRigDownDurationActual(), kpi.getRigDownDurationDesigned());
			kpi.setRigDownDurationScore(rigDownScore);
			
			//get demob score
			Double demobScore = KeyPerformanceIndicatorUtils.getScore(kpi.getDemobFrequencyActual(), kpi.getDemobFrequencyDesigned());
			kpi.setDemobScore(demobScore);
			
			//get prop score
			Double propScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getProppantPlacedActual(), kpi.getProppantPlacedDesigned());
			kpi.setProppantPlacedScore(propScore);
			
			//get conc score
			Double concScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getProppantConcentrationActual(), kpi.getProppantConcentrationDesigned());
			kpi.setProppantConcentrationScore(concScore);
			
			//get pump score
			Double pumpScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getPumpDurationActual(), kpi.getPumpDurationDesigned());
			kpi.setPumpDurationScore(pumpScore);
			
			//get zone score
			Double zoneScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getZoneActual(), kpi.getZoneDesigned());
			kpi.setZoneScore(zoneScore);
			
			//get npt score
			Double nptScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getNonProductiveDurationActual(), kpi.getNonProductiveDurationDesigned());
			kpi.setNonProductiveDurationScore(nptScore);
			
			//get move score
			Double moveScore = KeyPerformanceIndicatorUtils.getScore2(kpi.getInterwellMoveDurationActual(), kpi.getInterwellMoveDurationDesigned());
			kpi.setInterwellMoveDurationScore(moveScore);
			
			String reportDailyUid = KeyPerformanceIndicatorUtils.getReportDailyUid(kpi.getOperationUid(), kpi.getDailyUid());
			if (reportDailyUid != null){
				ReportDaily reportdaily = (ReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, reportDailyUid); 
				if (reportdaily != null){
					kpi.setOperatingCompanyRepresentative(reportdaily.getSupervisor());
				}
			}
			
			Double operationDuration = kpi.getOperatingDuration();
			Double personnelNumber = kpi.getPersonnelNumber();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, KeyPerformanceIndicator.class, "operatingDuration");
			if (kpi.getOperatingDuration() != null && kpi.getPersonnelNumber() != null){
				thisConverter.setBaseValueFromUserValue(operationDuration);
				operationDuration = thisConverter.getBasevalue();
				
				thisConverter.setReferenceMappingField(Activity.class, "activityDuration");
				thisConverter.setBaseValue(operationDuration);
				operationDuration = thisConverter.getConvertedValue();
				kpi.setTotalManHours(operationDuration * personnelNumber );
			}
			else {
				kpi.setTotalManHours(null);
			}		
			
		}
	}
}
