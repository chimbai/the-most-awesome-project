package com.idsdatanet.d2.pronet.filtrationJob;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;


import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.FiltrationFluidProperties;
import com.idsdatanet.d2.core.model.FiltrationJob;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * All common utility related to Rig Stock
 *
 */

public class FiltrationJobUtil {
	
	/**
	 * Method for save the stock records (FluidStock, SupplementaryStock)
	 * @param commandBean
	 * @param userSession
	 * @param aclManager
	 * @param request
	 * @param node
	 * @param key
	 * @throws Exception Standard Error Throwing Exception
	 * @return new ActionHandlerResponse
	 */
	
	public static void saveFiltrationJob(CustomFieldUom thisConverterCart, CustomFieldUom thisConverterDe, CustomFieldUom thisConverterVol, FiltrationJob thisFiltrationJob, CommandBeanTreeNode node) throws Exception{
		
		// select statement for FiltrationFluidProperties where FiltrationJob uid = child uid
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSqlVol = "SELECT SUM(filteredVolume) as totalVolume " +
			"FROM FiltrationFluidProperties " + 
			"WHERE (isDeleted is null or isDeleted=false) " +
			"AND filtrationJobUid=:filtrationJobUid";
		
		String[] paramsFields = {"filtrationJobUid"};
		Object[] paramsValues = {thisFiltrationJob.getFiltrationJobUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlVol, paramsFields, paramsValues, qp);
		
		Double totalVolume = 0.0;
		
		if (lstResult!=null && lstResult.size() > 0){
			if(lstResult.get(0) != null) {
				totalVolume = Double.parseDouble(lstResult.get(0).toString());
			}
		}
		
		thisConverterVol.setBaseValue(totalVolume);
		if (thisConverterVol.isUOMMappingAvailable()){
			node.setCustomUOM("@totalFilteredVolume", thisConverterVol.getUOMMapping());
		}
		node.getDynaAttr().put("totalFilteredVolume", thisConverterVol.getConvertedValue());
		
		String strSqlDe = "SELECT SUM(diatomaceousEarthMass) as totalDe " +
		"FROM FiltrationFluidProperties " + 
		"WHERE (isDeleted is null or isDeleted=false) " +
		"AND filtrationJobUid=:filtrationJobUid";
	
		List lstResultDe = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDe, paramsFields, paramsValues, qp);
		
		Double totalDe = 0.0;
		
		if (lstResultDe!=null && lstResultDe.size() > 0){
			if(lstResultDe.get(0) != null) {
				totalDe = Double.parseDouble(lstResultDe.get(0).toString());
			}
		}
		
		thisConverterDe.setBaseValue(totalDe);
		if (thisConverterDe.isUOMMappingAvailable()){
			node.setCustomUOM("@totalDiatomaceousEarthWeight", thisConverterDe.getUOMMapping());
		}
		node.getDynaAttr().put("totalDiatomaceousEarthWeight", thisConverterDe.getConvertedValue());
		
		String strSqlCart = "SELECT SUM(noOfCartridgeUsed) as totalCart " +
		"FROM FiltrationFluidProperties " + 
		"WHERE (isDeleted is null or isDeleted=false) " +
		"AND filtrationJobUid=:filtrationJobUid";
	
		List lstResultCart = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlCart, paramsFields, paramsValues, qp);
		
		Double totalCart = 0.0;
		
		if (lstResultCart!=null && lstResultCart.size() > 0){
			if(lstResultCart.get(0) != null) {
				totalCart = Double.parseDouble(lstResultCart.get(0).toString());
			}
		}
		
		thisConverterCart.setBaseValue(totalCart);
		if (thisConverterCart.isUOMMappingAvailable()){
			node.setCustomUOM("@totalCartridgeUsed", thisConverterCart.getUOMMapping());
		}
		node.getDynaAttr().put("totalCartridgeUsed", thisConverterCart.getConvertedValue());
	}
}