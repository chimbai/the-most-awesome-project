package com.idsdatanet.d2.pronet.productionString;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	
	private static final String MAPPING_SEPARATOR=".";
	private Map<String,String> autoUpdateFieldMappingList;
	private Boolean autoUpdateLatestLogToComponent = true;
	private String orderBy = "sc.sequence desc, sc.depthMdMsl";
	private Boolean calctallylength = false;
	private Boolean disablepulldatemsg = true;
	
	public void setAutoUpdateFieldMappingList(
			Map<String, String> autoUpdateFieldMappingList) {
		this.autoUpdateFieldMappingList = autoUpdateFieldMappingList;
	}

	public void setAutoUpdateLatestLogToComponent(
			Boolean autoUpdateLatestLogToComponent) {
		this.autoUpdateLatestLogToComponent = autoUpdateLatestLogToComponent;
	}
	
	public void setcalctallylength(
			Boolean calctallylength) {
		this.calctallylength = calctallylength;
	}
	
	public void setdisablepulldatemsg(
			Boolean disablepulldatemsg) {
		this.disablepulldatemsg = disablepulldatemsg;
	}
	
	public void setOrderBy(String order){
		this.orderBy = order;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(ProductionStringDetail.class)) return true;
		else return false;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(ProductionStringDetail.class)) {
			
			String productionStringUid = null;
			Object object = node.getData();
			
			if (object instanceof ProductionString) {
				ProductionString productionstring = (ProductionString) object;
				productionStringUid = productionstring.getProductionStringUid();
				
			}
			//UserSession userSession = UserSession.getInstance(request);
		
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) {
				//get previous operation end date
				String lastDailyUid = CommonUtil.getConfiguredInstance().getPreviousOperationEndDate(userSelection.getWellboreUid());
				if (lastDailyUid!=null){
					daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(lastDailyUid);
					if (daily == null)
						return null;
				}
				else
					return null;
			}
				
			String viewStringComponentMode = "";
			if (request != null) {
				viewStringComponentMode = request.getParameter("viewComponentMode");
			}
			String strSql = "";
			long timeLapsed = 86399999;
			Date newDate = new Date(daily.getDayDate().getTime() + timeLapsed);
			
			List items = null;
			if ("show_all".equalsIgnoreCase(viewStringComponentMode)){
				String[] paramNames1 = {"productionStringUid", "wellboreUid"};
				Object[] paramValues1 = {productionStringUid, userSelection.getWellboreUid()};
				
				strSql = "select sc.productionStringDetailUid, sc.sequence from ProductionString pd, ProductionStringDetail sc where (pd.isDeleted = false or pd.isDeleted is null) and (sc.isDeleted = false or sc.isDeleted is null) and " +
					"pd.productionStringUid = sc.productionStringUid and pd.wellboreUid =:wellboreUid and sc.productionStringUid =:productionStringUid order by " + this.orderBy;
				items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames1, paramValues1);		
			
			}else {
				strSql = "select sc.productionStringDetailUid, sc.sequence from ProductionString pd, ProductionStringDetail sc where (pd.isDeleted = false or pd.isDeleted is null) and (sc.isDeleted = false or sc.isDeleted is null) and " +
				"pd.productionStringUid = sc.productionStringUid and ((sc.installDateTime <=:newDate and sc.uninstallDateTime >=:currentDate) or (sc.installDateTime <=:newDate and sc.uninstallDateTime is null) or (sc.installDateTime is null and sc.uninstallDateTime is null)) " +
				"and pd.wellboreUid =:wellboreUid and sc.productionStringUid =:productionStringUid order by " + this.orderBy;
			
				String[] paramNames2 = {"currentDate","newDate", "wellboreUid", "productionStringUid"};
				Object[] paramValues2 = {daily.getDayDate(), newDate, userSelection.getWellboreUid(), productionStringUid};
				items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames2,paramValues2);		
				
			}
			
			List<Object> output_maps = new ArrayList<Object>();
		
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				ProductionStringDetail stringComponent = (ProductionStringDetail) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ProductionStringDetail.class, (String) obj_array[0].toString());
				output_maps.add(stringComponent);
			}
			return output_maps;
			
		}
		
		return null;
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {

		Object obj = node.getData();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if(daily == null) return;
		
		if (obj instanceof ProductionString){
			
			String viewStringComponentMode = "";
			if (request != null) {
				viewStringComponentMode = request.getParameter("viewComponentMode");
			}
			
			if ("show_all".equalsIgnoreCase(viewStringComponentMode)){			
				node.getDynaAttr().put("showAll", "1");
			}else {
				node.getDynaAttr().put("showAll", "0");
			}
			
			if(calctallylength) {
				ProductionString ps = (ProductionString)obj;
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				Double tallyLength = 0.00;
				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "tallyLength");
				
				String strSql = "Select tallyLength, numJoints FROM ProductionStringDetail " 
						+ "WHERE productionStringUid =:productionStringUid AND wellboreUid = :wellboreUid and (isDeleted is null or isDeleted = false) ";
				String[] paramsFields = { "productionStringUid", "wellboreUid"};
				Object[] paramsValues = {ps.getProductionStringUid(), userSelection.getWellboreUid()};
				List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
						strSql, paramsFields, paramsValues);
				
				if(lstResult!=null && lstResult.size()>0) {
					
					for(Object[] psd:lstResult) {
						
						if(psd[0]!=null && psd[1] != null)
						{
							Double length = Double.parseDouble(psd[0].toString());
							Double numjoints = Double.parseDouble(psd[1].toString());
							tallyLength += (length * numjoints);
						}
					}
				}
				thisConverter.setBaseValue(tallyLength);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalLength", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalLength", thisConverter.getConvertedValue());
			}
			
		}
		
		Date currentDate = daily.getDayDate();
		if (obj instanceof ProductionStringDetail) {
			
			ProductionStringDetail psdetail = (ProductionStringDetail)obj;

			//check if the components are being installed or removed
			String futureInstall = "";
			long timeLapsed = 86399999;
			Date newDate = new Date(currentDate.getTime() + timeLapsed);

			if (psdetail.getInstallDateTime() != null) {
				if (psdetail.getInstallDateTime().after(newDate)) {
					futureInstall = "1";
				}
			}
			node.getDynaAttr().put("futureInstall", futureInstall);
			
			String removed = "";
			if (psdetail.getUninstallDateTime() != null){
				if (psdetail.getUninstallDateTime().before(currentDate)) {
					removed ="1";
				}
			}
			
			node.getDynaAttr().put("uninstalled", removed);
			
//			if(calctallylength) {
//				
//				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
//				Double tallyLength = 0.0;
//				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "tallyLength");
//				
//				String strSql = "Select (tallyLength * numJoints) as StringLength FROM ProductionStringDetail " 
//						+ "WHERE productionStringUid =:productionStringUid AND wellboreUid = :wellboreUid and (isDeleted is null or isDeleted = false) ";
//				String[] paramsFields = { "productionStringUid", "wellboreUid"};
//				Object[] paramsValues = {psdetail.getProductionStringUid(), userSelection.getWellboreUid()};
//				List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//						strSql, paramsFields, paramsValues);
//				
//				if(lstResult!=null && lstResult.size()>0) {
//					
//					for(Object ps:lstResult) {
//						
//						if(ps!=null)
//						{
//							thisConverter.setBaseValue(Double.parseDouble(ps.toString()));
//							if (thisConverter.isUOMMappingAvailable()){
//								node.setCustomUOM("@totalstringLength", thisConverter.getUOMMapping());
//							}
//							tallyLength += thisConverter.getBasevalue();
//						}
//					}
//				}
//				node.getDynaAttr().put("totalLength", tallyLength);
//			}
		}
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		
		if (obj instanceof ProductionString){
			ProductionString productionString = (ProductionString) obj;
			if(daily != null)
			{
				productionString.setInstallDateTime(daily.getDayDate());
			}
		}
		
		if (obj instanceof ProductionStringDetail){
			ProductionStringDetail stringComponents = (ProductionStringDetail) obj;
			stringComponents.setNumJoints(1);
			if(daily != null)
			{
				stringComponents.setInstallDateTime(daily.getDayDate());
			}
		}
				
		if (obj instanceof ProductionStringDetailLog){
			ProductionStringDetailLog stringComponentLog = (ProductionStringDetailLog) obj;
			stringComponentLog.setNumJoints(1);
			if(daily != null)
			{
				stringComponentLog.setServiceDateTime(daily.getDayDate());
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		Object obj = node.getData();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		qp.setDatumConversionEnabled(true);
		
		
		
		//after saving new component data, auto creating first log for it 
		if (obj instanceof ProductionStringDetail && (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed)) {			
			ProductionStringDetail psdetail = (ProductionStringDetail)obj;			
			
			//check if user has added a log for the current Production String Detail.
			if( !isFirstLogAvailable( psdetail.getProductionStringDetailUid() ) ){
			ProductionStringDetailLog psDetailLog = new ProductionStringDetailLog();
			
			if (this.autoUpdateFieldMappingList!=null) {
				for (Iterator iter = this.autoUpdateFieldMappingList.entrySet().iterator(); iter.hasNext();) 
				{
					Map.Entry entry = (Map.Entry) iter.next();
					String key = entry.getKey().toString();
					String fieldToUpdate = entry.getValue().toString().split("["+MAPPING_SEPARATOR+"]")[1];
	
					Object value = PropertyUtils.getProperty(psdetail,fieldToUpdate);
					if (value !=null){
						java.lang.Class type = PropertyUtils.getPropertyType(psdetail, key);
						if (type == java.lang.Double.class){
							thisConverter.setReferenceMappingField(ProductionStringDetail.class, key);
							thisConverter.setBaseValueFromUserValue((Double) value);
							value = thisConverter.getBasevalue();
							
							thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, key);
							if (thisConverter.isUOMMappingAvailable()) {
								thisConverter.setBaseValue((Double)value);
								//add datum offset if it is datum related field
								if (thisConverter.getUOMMapping().isDatumConversion()) {
									thisConverter.addDatumOffset();						
								}
								value = thisConverter.getConvertedValue();
							}					
						}
					}
					PropertyUtils.setProperty(psDetailLog, key, value);
				}
			}
			psDetailLog.setProductionStringDetailUid(psdetail.getProductionStringDetailUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(psDetailLog, qp);
			}

		}
		
//		if(calctallylength) {
//			if (obj instanceof ProductionStringDetail) {
//				ProductionStringDetail psdetail1 = (ProductionStringDetail)obj;
//				Double tallyLength = 0.0;
//				
//				String welluid = psdetail1.getWellUid();
//				String country = null;
//				Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(welluid);
//				if (currentWell != null) {
//					country = currentWell.getCountry();
//				}
//				
//				if(session.getCurrentOperationType().equals("WKO") && country.equals("AT")){
//					
//					String strSql = "Select (tallyLength * numJoints) as StringLength FROM ProductionStringDetail " 
//							+ "WHERE productionStringUid =:productionStringUid AND wellboreUid = :wellboreUid and (isDeleted is null or isDeleted = false) ";
//					String[] paramsFields = { "productionStringUid", "wellboreUid"};
//					Object[] paramsValues = {psdetail1.getProductionStringUid(), session.getCurrentWellboreUid()};
//					List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//							strSql, paramsFields, paramsValues);
//					
//					if(lstResult!=null && lstResult.size()>0) {
//						
//						for(Object ps:lstResult) {
//							
//							if(ps!=null)
//							{
//								thisConverter.setBaseValue(Double.parseDouble(ps.toString()));
//								tallyLength += thisConverter.getBasevalue();
//							}
//						}
//					}
//				
//					String strSql1 = "FROM ProductionString " 
//							+ " WHERE productionStringUid =:productionStringUid AND wellboreUid = :wellboreUid and(isDeleted is null or isDeleted = false) ";
//					String[] paramsFields1 = { "productionStringUid", "wellboreUid"};
//					Object[] paramsValues1 = {psdetail1.getProductionStringUid(), session.getCurrentWellboreUid()};
//					List <ProductionString> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//							strSql1, paramsFields1, paramsValues1);
//					
//					if(lstResult1!=null && lstResult1.size()>0) {
//						ProductionString thisProductionString = lstResult1.get(0);
//						thisProductionString.setStringLength(tallyLength);
//						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisProductionString, qp);
//					}
//				}
//			}
//		}
		
		//update child top and bottom data if depthMdMsl value changed
		if (obj instanceof ProductionString) {
			if (!this.autoUpdateLatestLogToComponent) {
				if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoPopulateTopBottomWithSetDepth"))){
					ProductionString ps = (ProductionString)obj;	
					if (ps.getDepthMdMsl()!=null) {
						 Map<String, CommandBeanTreeNode> childNodes = node.getChild("ProductionStringDetail");
						 if(childNodes!=null){
							 List<CommandBeanTreeNode> allChildNodes =  new ArrayList(childNodes.values());
							 ProductionStringUtils.calculateTopAndBottomDepth(allChildNodes, node, thisConverter);
						 }
					}
				}

			}
			
			//boolean as true but omv customize bean as false
			if(disablepulldatemsg) {
				
				ProductionString psdetail = (ProductionString)obj;	
				String strSql = "FROM ProductionString " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND wellboreUid=:wellboreUid " +
				"AND uninstallDateTime is null";
				List<ProductionString> ps = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", psdetail.getWellboreUid());
				if(ps!=null & ps.size() > 1)
				{
					commandBean.getSystemMessage().addInfo("More than 1 recond found without Pull Date. No production string will be shown in the Well Summary (Diagram) Report");
				}
			}
			
		}
		
	}
	
	private Boolean isFirstLogAvailable(String productionStringDetailUid) throws Exception {
		// TODO Auto-generated method stub
		String strSql = "From ProductionStringDetailLog Where productionStringDetailUid = :productionStringDetailUid And (isDeleted = false or isDeleted is null)";
		String[] paramNames = {"productionStringDetailUid"};
		Object[] paramValues = {productionStringDetailUid};
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames,paramValues);
		
		if( result.size() > 0 ){
			return true;
		}else{
			return false;
		}
		
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		// check if start time greater than end time.
		if(obj instanceof ProductionString || obj instanceof ProductionStringDetail || obj instanceof ProductionStringDetailLog) {
			Date start = (Date) PropertyUtils.getProperty(obj, "installDateTime");
			Date end = (Date) PropertyUtils.getProperty(obj, "uninstallDateTime");
			
			// check if start time greater than end time.
			if(CommonUtil.getConfiguredInstance().isStartGreaterEndDate(start, end)){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "uninstallDateTime", "Start date cannot be greater than end time.");
				return;
				
			}
			
		}

		//for production string detail log
		Double totalLength = 0.0;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		if (obj instanceof ProductionStringDetailLog) {
			
			ProductionStringDetailLog detailLog = (ProductionStringDetailLog) obj;
			//set num of Joint to 1 if it is null
			if (detailLog.getNumJoints() == null) detailLog.setNumJoints(1);
			//calculate totalLength on detail log						
			if (detailLog.getLength() != null && detailLog.getNumJoints() != null) {	
				thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, "length");
				totalLength = ProductionStringUtils.calculateTotalLength(detailLog.getNumJoints(), detailLog.getLength(),thisConverter);
				
				thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, "totalLength");
				thisConverter.setBaseValue(totalLength);
				detailLog.setTotalLength(thisConverter.getConvertedValue());
			}
			else{
				detailLog.setTotalLength(null);
			}
			
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoCalculateDepthToatComponentServiceLog"))){
				if (detailLog.getDepthMdMsl() !=null && detailLog.getTotalLength()!=null){					
					detailLog.setBottomDepthMdMsl(ProductionStringUtils.calculateBottomDepth(detailLog.getDepthMdMsl(), detailLog.getTotalLength(),thisConverter));
				}else{
					detailLog.setBottomDepthMdMsl(null);
				}				
			}					
		}
		
		//for production string detail 
		if (obj instanceof ProductionStringDetail) {
			
			ProductionStringDetail detail = (ProductionStringDetail) obj;
			//set numOfJoint to 1 if it is null
			if (detail.getNumJoints() == null) detail.setNumJoints(1);
			//calculate total length 
			if (detail.getLength() != null && detail.getNumJoints() != null) { 	
				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "length");
				totalLength = ProductionStringUtils.calculateTotalLength(detail.getNumJoints(), detail.getLength(),thisConverter);
				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "totalLength");
				thisConverter.setBaseValue(totalLength);
				detail.setTotalLength(thisConverter.getConvertedValue());
			}
			else{
				detail.setTotalLength(null);
			}			
		}		
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
		
		if (this.autoUpdateLatestLogToComponent) {
			if ("ProductionStringDetailLog".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()){
				ProductionStringUtils.updateLastLogToComponent(this.autoUpdateFieldMappingList, status.getUpdatedNodes(), userSession, parent);
			}
		}else {
			if ("ProductionStringDetail".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()){
				//calculate cum. weight force 
				ProductionStringUtils.calculateCumulativeValue(status.getUpdatedNodes(), parent, thisConverter, "weightForce", "totalWeightForce");
				//calculate cum. volumeDisplaced
				ProductionStringUtils.calculateCumulativeValue(status.getUpdatedNodes(), parent, thisConverter, "volumeDisplaced", "totalVolumeDisplaced");
				//calculate cum. length 
				if ("0".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "productionstringdetailtotaljointsasmultiplier"))){
					ProductionStringUtils.calculateCumulativeValue(status.getUpdatedNodes(), parent, thisConverter, "length", "cumLength");
				}else {
					ProductionStringUtils.calculateCumulativeValue(status.getUpdatedNodes(), parent, thisConverter, "totalLength", "cumLength");
				}
				//auto calculate top and bottom depth based on set depth
				if("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "autoPopulateTopBottomWithSetDepth"))){
					ProductionStringUtils.calculateTopAndBottomDepth(status.getUpdatedNodes(), parent, thisConverter);
				}	
				
				//auto calculate tally Lenght
				if ("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PS_TALLY_LENGHT)))
				{
					ProductionString ps = (ProductionString) parent.getData();
					ProductionStringUtils.AutoCalTallyLength(userSession, parent.getCommandBean(), ps.getProductionStringUid()); 
				}
				if ("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "autoCalcTotalJoints"))){
					ProductionString ps = (ProductionString) parent.getData();
					ProductionStringUtils.calculateTotalNoOfComponentsInTally(userSession, thisConverter, ps.getProductionStringUid());
				}
			}
		}
	}
	
}