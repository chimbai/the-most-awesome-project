package com.idsdatanet.d2.pronet.productionString;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {

		if(StringUtils.equals(targetClass, null)) {
			Object obj = node.getData();
			
			if (obj instanceof ProductionStringDetailLog) {
				if(StringUtils.equals(action, "edit"))  return false; 
			}
		}
		
		return true;
	}

	

}
