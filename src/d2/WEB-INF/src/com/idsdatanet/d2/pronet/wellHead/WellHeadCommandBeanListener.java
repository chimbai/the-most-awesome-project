package com.idsdatanet.d2.pronet.wellHead;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.WellHead;
import com.idsdatanet.d2.core.model.WellHeadDetail;
import com.idsdatanet.d2.core.model.WellHeadSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellHeadCommandBeanListener implements CommandBeanListener {
	
	private List<String> excludedFieldsOnCopyWellHead =null;
	private List<String> excludedFieldsOnCopyWellHeadSection = null;
	private List<String> excludedFieldsOnCopyWellHeadDetail = null;

	public void setExcludedFieldsOnCopyWellHead(
			List<String> excludedFieldsOnCopyWellHead) {
		this.excludedFieldsOnCopyWellHead = excludedFieldsOnCopyWellHead;
	}

	public void setExcludedFieldsOnCopyWellHeadSection(
			List<String> excludedFieldsOnCopyWellHeadSection) {
		this.excludedFieldsOnCopyWellHeadSection = excludedFieldsOnCopyWellHeadSection;
	}

	public void setExcludedFieldsOnCopyWellHeadDetail(
			List<String> excludedFieldsOnCopyWellHeadDetail) {
		this.excludedFieldsOnCopyWellHeadDetail = excludedFieldsOnCopyWellHeadDetail;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(WellHead.class)){
				if("@copyWellHeadFrom".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					UserSession session = UserSession.getInstance(request);
					String copyFromWellHeadUid = (String) targetCommandBeanTreeNode.getDynaAttr().get("copyWellHeadFrom");
					//create child node
					loopAllNodeAndAddWellheadChildrenToNode(commandBean, targetCommandBeanTreeNode, copyFromWellHeadUid, session);
				}
				
				commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
			}
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(WellHeadDetail.class)){
				if("topConnectionType".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					WellHeadDetail thisWellHeadDetail = (WellHeadDetail) targetCommandBeanTreeNode.getData();
					UserSession session = UserSession.getInstance(request);
					thisWellHeadDetail.setTopConnectionSizePressure(null);
					thisWellHeadDetail.setTopConnectionSize(null);
				}
				if("bottomConnectionType".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					WellHeadDetail thisWellHeadDetail = (WellHeadDetail) targetCommandBeanTreeNode.getData();
					UserSession session = UserSession.getInstance(request);
					thisWellHeadDetail.setBottomConnectionSizePressure(null);
					thisWellHeadDetail.setBottomConnectionSize(null);
				}
			}
		}
	
	}
	/**
	 * Method used to copy selected wellhead data to current node and child node
	 * @param commandBean
	 * @param targetNode
	 * @param wellHeadUid
	 * @param session
	 */

	private void loopAllNodeAndAddWellheadChildrenToNode(CommandBean commandBean, CommandBeanTreeNode targetNode, String wellHeadUid, UserSession session) throws Exception {
	
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);

		if(targetNode.getDataDefinition().getTableClass().equals(WellHead.class)){
			if (targetNode.getData() instanceof WellHead){
				WellHead wellhead = (WellHead) targetNode.getData();
				WellHead selectedWellHead = (WellHead) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellHead.class, wellHeadUid);
				PropertyUtils.copyProperties(wellhead, selectedWellHead);
				
				//after copy from selected wellhead, need to convert all Double value with UOM and Datum to current uom and datum. 
				//And set those excluded fields and primary key to null for creating a new record
				CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, WellHead.class, wellhead, thisConverter, this.excludedFieldsOnCopyWellHead);
				
				//query wellHeadSection data for selected wellhead
				List<WellHeadSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from WellHeadSection where (isDeleted = false or isDeleted is null) and wellHeadUid = :wellHeadUid", "wellHeadUid", wellHeadUid);
				if (list != null && list.size() > 0) {
					for (WellHeadSection wellheadSection : list) {					
						
						//query WellHeadDetail data for selected wellhead
						List<WellHeadDetail> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from WellHeadDetail where (isDeleted = false or isDeleted is null) and wellHeadSectionUid = :wellHeadSectionUid", "wellHeadSectionUid", wellheadSection.getWellHeadSectionUid());
						
						//after copy from selected wellhead, need to convert all Double value with UOM and Datum to current uom and datum. 
						//And set those excluded fields and primary key to null for creating a new record						
						CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, WellHeadSection.class, wellheadSection, thisConverter, this.excludedFieldsOnCopyWellHeadSection);
						CommandBeanTreeNode wellHeadSectionNode = targetNode.addCustomNewChildNodeForInput(wellheadSection);
						if (list2 != null && list2.size() > 0) {
							for (WellHeadDetail wellheadDetail : list2) {	
								
								//after copy from selected wellhead, need to convert all Double value with UOM and Datum to current uom and datum. 
								//And set those excluded fields and primary key to null for creating a new record
								CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, WellHeadDetail.class, wellheadDetail, thisConverter, this.excludedFieldsOnCopyWellHeadDetail);											
								wellHeadSectionNode.addCustomNewChildNodeForInput(wellheadDetail);
							}
						}
					}
				}
			}

		}
	}
	
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}


	
}
