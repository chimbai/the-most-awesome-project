package com.idsdatanet.d2.pronet.cavitation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CoalDescription;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CoalNameLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String strSql1 = "FROM Well WHERE wellUid=:wellUid and (isDeleted is null or isDeleted = false)";
		String selectedField ="";
		List <Well> lstWellResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "wellUid", userSelection.getWellUid());
		if(lstWellResult!=null && lstWellResult.size() > 0)
		{
			for(Object objWellResult: lstWellResult){
				Well wellRecords = (Well) objWellResult;
				selectedField = wellRecords.getField();
			}
		}
		String hql = "from CoalDescription where (isDeleted = false or isDeleted is null) and fieldDescription=:fieldDescription order by coalName";
		List <CoalDescription> lstCoalDescriptionResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "fieldDescription", selectedField);
		if(lstCoalDescriptionResult.size() >0 && lstCoalDescriptionResult!=null)
		{
			for(Object obj: lstCoalDescriptionResult)
				{
				   CoalDescription thisCoalDescription = (CoalDescription) obj;
					
					LookupItem item = new LookupItem(thisCoalDescription.getCoalName(), thisCoalDescription.getCoalName());
					result.put(thisCoalDescription.getCoalName(), item);
				}
			
		}
		return result;
	}

}