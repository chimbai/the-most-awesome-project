package com.idsdatanet.d2.pronet.fracFluidVolume;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.FracFluidVolume;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class FracFluidVolumeDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		Object object = node.getData();
		if (object instanceof FracFluidVolume) {
			FracFluidVolume rec = (FracFluidVolume) object;
			Double pumpFormationVolume = rec.getPumpFormationVolume();
			Double pumpSumpPitVolume = rec.getPumpSumpPitVolume();
			if (pumpFormationVolume!=null) {
				Double totalPumpFormationVolume = 0.0;
				if (commandBean.getRoot().getDynaAttr().containsKey("totalPumpFormationVolume")) {
					totalPumpFormationVolume = (Double) commandBean.getRoot().getDynaAttr().get("totalPumpFormationVolume");
				}
				totalPumpFormationVolume += pumpFormationVolume;
				commandBean.getRoot().getDynaAttr().put("totalPumpFormationVolume", totalPumpFormationVolume);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FracFluidVolume.class, "pumpFormationVolume");
				Double cumPumpFormationVolume = FracFluidVolumeUtils.getFluidVolumeCumulative(userSelection, rec.getOperationType(), "pumpFormationVolume");
				thisConverter.setBaseValue(cumPumpFormationVolume);
				node.getDynaAttr().put("cumPumpFormationVolume", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) {
					commandBean.getRoot().setCustomUOM("@totalPumpFormationVolume", thisConverter.getUOMMapping());
					node.setCustomUOM("@cumPumpFormationVolume", thisConverter.getUOMMapping());
				}
			}
			if (pumpSumpPitVolume!=null){
				Double totalPumpSumpPitVolume = 0.0;
				if (commandBean.getRoot().getDynaAttr().containsKey("totalPumpSumpPitVolume")) {
					totalPumpSumpPitVolume = (Double) commandBean.getRoot().getDynaAttr().get("totalPumpSumpPitVolume");
				}
				totalPumpSumpPitVolume += pumpSumpPitVolume;
				commandBean.getRoot().getDynaAttr().put("totalPumpSumpPitVolume", totalPumpSumpPitVolume);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FracFluidVolume.class, "pumpSumpPitVolume");
				Double cumPumpSumpPitVolume = FracFluidVolumeUtils.getFluidVolumeCumulative(userSelection, rec.getOperationType(), "pumpSumpPitVolume");
				thisConverter.setBaseValue(cumPumpSumpPitVolume);
				node.getDynaAttr().put("cumPumpSumpPitVolume", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) {
					commandBean.getRoot().setCustomUOM("@totalPumpSumpPitVolume", thisConverter.getUOMMapping());
					node.setCustomUOM("@cumPumpSumpPitVolume", thisConverter.getUOMMapping());
				}
			}
			
		}

	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		commandBean.getRoot().getDynaAttr().put("totalPumpFormationVolume", 0.0);
		commandBean.getRoot().getDynaAttr().put("totalPumpSumpPitVolume", 0.0);
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
