package com.idsdatanet.d2.pronet.productionStringLine;

import java.util.List;

import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;

public class ProductionStringLineCommandBeanConfig extends CommandBeanConfiguration {

	@Override
	public ActionManager getActionManager() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener()
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommandBeanListener getCommandBeanListener() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommandBeanReportDataListener getCommandBeanReportDataListener()
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CommandBeanReportValidator> getCommandBeanReportValidators()
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataNodeListener getDataNodeListener() throws Exception {
		return new com.idsdatanet.d2.pronet.productionStringLine.ProductionStringLineDataNodeListener();
	}

	@Override
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataSummaryListener getDataSummaryListener() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler()
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(BaseCommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
