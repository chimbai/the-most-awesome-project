package com.idsdatanet.d2.pronet.cavitation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CoalDescription;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.SampleDescription;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CavitationCommandBeanListener extends EmptyCommandBeanListener{

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		//auto populate coal lookup based on items in lookup when add new 
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			if (request!=null){
				if(StringUtils.isNotBlank(request.getParameter("target_class"))){
					if ("SampleDescription".equals(request.getParameter("target_class"))){
						
						UserSession session = UserSession.getInstance(request);
						String lookupKey = "";
						LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
						String strSql1 = "FROM Well WHERE wellUid=:wellUid and (isDeleted is null or isDeleted = false)";
						String selectedField ="";
						List <Well> lstWellResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "wellUid", session.getCurrentWellUid());
						if(lstWellResult!=null && lstWellResult.size() > 0)
						{
							for(Object objWellResult: lstWellResult){
								Well wellRecords = (Well) objWellResult;
								selectedField = wellRecords.getField();
							}
						}
						String hql = "from CoalDescription where (isDeleted = false or isDeleted is null) and fieldDescription=:fieldDescription order by coalName";
						List <CoalDescription> lstCoalDescriptionResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "fieldDescription", selectedField);
						if(lstCoalDescriptionResult.size() >0 && lstCoalDescriptionResult!=null)
						{
							for(Object obj: lstCoalDescriptionResult)
								{
								   CoalDescription thisCoalDescription = (CoalDescription) obj;
									
									LookupItem item = new LookupItem(thisCoalDescription.getCoalName(), thisCoalDescription.getCoalName());
									result.put(thisCoalDescription.getCoalName(), item);
								}
							for (Map.Entry entry: result.entrySet()) {
								lookupKey = entry.getKey().toString();					
								SampleDescription cavitation = new SampleDescription();
								cavitation.setSampleType(lookupKey);
								//check if it is first day for cavitation
								String isFirstDay = CavitationUtils.isCavitationFirstDay(session.getCurrentOperationUid(), session.getCurrentDailyUid());	

								if (!"1".equals(isFirstDay)){
									//if not first day, populate depths with first day value
									
									
									CavitationUtils.populateFromFirstdayDepth(cavitation, session.getCurrentOperationUid(), lookupKey);
								}
								CommandBeanTreeNode node = commandBean.getRoot().addCustomNewChildNodeForInput(cavitation);
								node.getDynaAttr().put("isFirstDay", isFirstDay);
								
							}
							
						}
						else
						{
							
							SampleDescription cavitation = new SampleDescription();
							String isFirstDay = CavitationUtils.isCavitationFirstDay(session.getCurrentOperationUid(), session.getCurrentDailyUid());	
							CavitationUtils.populateFromFirstdayDepth(cavitation, session.getCurrentOperationUid(), lookupKey);
							CommandBeanTreeNode node = commandBean.getRoot().addCustomNewChildNodeForInput(cavitation);
							node.getDynaAttr().put("isFirstDay", isFirstDay);
							
						}
						
						
						commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
					}
				}
			}
		}
					
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SampleDescription.class, "dailyVolume");
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		
		//auto calculate total daily volume per day 
		Double totalDaily = CavitationUtils.calculateTotalDailyVolume(userSelection.getOperationUid(), daily, qp);		
		if (totalDaily !=null) {
			thisConverter.setBaseValue(totalDaily);
			commandBean.getRoot().getDynaAttr().put("cumDailyVolume", thisConverter.getConvertedValue());
		
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@cumDailyVolume", thisConverter.getUOMMapping());
				
			}
		}
		
		//auto calculate total cumulative volume to date
		thisConverter.setReferenceMappingField(SampleDescription.class, "totalVolume");
		Double cumTotal = CavitationUtils.calculateCummulativeTotalVolume(userSelection.getOperationUid(), daily, qp);
		if (cumTotal !=null) {
			thisConverter.setBaseValue(cumTotal);
			commandBean.getRoot().getDynaAttr().put("cumTotalVolume", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@cumTotalVolume", thisConverter.getUOMMapping());	
			}
		}
		
		//String isFirstDay = CavitationUtils.getOperationCavitationFirstDay(userSelection.getOperationUid(), userSelection.getDailyUid());	
		//commandBean.getRoot().getDynaAttr().put("isFirstDay", isFirstDay);
		
	}

}
