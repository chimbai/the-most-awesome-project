package com.idsdatanet.d2.pronet.coalDescription;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FieldLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String hql = "Select distinct(field) from Well where (isDeleted = false or isDeleted is null)";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if(list.size() >0)
		{
			for(Object obj: list)
				{
					if(obj!=null && StringUtils.isNotBlank(obj.toString()))
					{
						LookupItem item = new LookupItem(obj.toString(), obj.toString());
						result.put(obj.toString(), item);
					}
					
				}
			
		}
		return result;
	}

}