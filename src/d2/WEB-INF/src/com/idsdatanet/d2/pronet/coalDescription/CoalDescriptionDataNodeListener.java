package com.idsdatanet.d2.pronet.coalDescription;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CoalDescription;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoalDescriptionDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof CoalDescription) {
			CoalDescription thisCoalDescription = (CoalDescription) object;
			if(commandBean.getRoot().getDynaAttr().get("wellfield")!=null)
			{
				thisCoalDescription.setFieldDescription(commandBean.getRoot().getDynaAttr().get("wellfield").toString());
			}
			
		}
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(CoalDescription.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			String selectedField  = "";
			selectedField = (String) commandBean.getRoot().getDynaAttr().get("wellfield");
			if(selectedField==null || selectedField=="")
			{
				String strSql1 = "FROM Well WHERE wellUid=:wellUid and (isDeleted is null or isDeleted = false)";
				List <Well> lstWellResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "wellUid", userSelection.getWellUid());
				if(lstWellResult!=null && lstWellResult.size() > 0)
				{
					for(Object objWellResult: lstWellResult){
						Well wellRecords = (Well) objWellResult;
						selectedField = wellRecords.getField();
					}
					commandBean.getRoot().getDynaAttr().put("wellfield", selectedField);
				}
				
			}
			String strSql = "FROM CoalDescription WHERE fieldDescription= :wellfield and (isDeleted is null or isDeleted = false) ORDER BY sequence";
			List <CoalDescription> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellfield", selectedField);
			if(lstResult.size() > 0 && lstResult!=null)
			{
				for(Object objResult: lstResult){
					CoalDescription coalDescriptionRecords = (CoalDescription) objResult;
					output_maps.add(coalDescriptionRecords);
				}
				return output_maps;
			}
			
			
		}
		return null;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return true;
	}
}