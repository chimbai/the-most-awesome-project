package com.idsdatanet.d2.pronet.stimulation;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Stimulation;
import com.idsdatanet.d2.core.model.TargetZone;
import com.idsdatanet.d2.core.model.StimulationPump;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class StimulationUtils {
	public static void createStimulationBasedOnActivityRemarks(UserSelectionSnapshot userSelection, String moduleName, Activity thisActivity, Map<String, Object> fieldsMap) throws Exception {
		if (fieldsMap != null) {
			Stimulation stimulation = null;
			TargetZone targetzone = null;
			StimulationPump stimulationpump = null;

			String[] paramsFields = {"operationUid", "dailyUid", "activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(), thisActivity.getDailyUid(), thisActivity.getActivityUid()};
			String strSql = "FROM Stimulation WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid AND activityUid=:activityUid";
			List<Stimulation> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields, paramsValues);

			if (ls.size() == 0) {
				// create Stimulation
				stimulation = new Stimulation();
				stimulation.setDailyUid(thisActivity.getDailyUid());
				stimulation.setOperationUid(thisActivity.getOperationUid());
				stimulation.setActivityUid(thisActivity.getActivityUid());

				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(stimulation);

				if (stimulation.getStimulationUid() != null) {
					// create TargetZone
					targetzone = new TargetZone();
					targetzone.setStimulationUid(stimulation.getStimulationUid());
					targetzone.setOperationUid(thisActivity.getOperationUid());

					
					// create StimulationPump
					stimulationpump = new StimulationPump();
					stimulationpump.setStimulationUid(stimulation.getStimulationUid());
					stimulationpump.setOperationUid(thisActivity.getOperationUid());
					 

					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(targetzone);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(stimulationpump);
				}
			} else {
				stimulation = ls.get(0);
			}

			if ("stimulation".equalsIgnoreCase(moduleName)) {
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()) {
					if (StringUtils.isNotBlank(fieldEntry.getKey()))
						PropertyUtils.setProperty(stimulation, fieldEntry.getKey(), fieldEntry.getValue());
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(stimulation);

			} else if ("targetzone".equalsIgnoreCase(moduleName)) {
				if (stimulation != null) {
					String strSql2 = "FROM TargetZone WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND stimulationUid=:stimulationUid AND operationUid=:operationUid";
					paramsFields = new String[] {"stimulationUid", "operationUid"};
					paramsValues = new Object[] {stimulation.getStimulationUid(), thisActivity.getOperationUid(), thisActivity.getDailyUid()};
					List<TargetZone> lsTargetZone = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);

					if (lsTargetZone.size() == 0) {
						targetzone = new TargetZone();
						targetzone.setStimulationUid(stimulation.getStimulationUid());
						targetzone.setOperationUid(thisActivity.getOperationUid());

						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(targetzone);
					} else {
						targetzone = lsTargetZone.get(0);
					}

					for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()) {
						if (StringUtils.isNotBlank(fieldEntry.getKey()))
							PropertyUtils.setProperty(targetzone, fieldEntry.getKey(), fieldEntry.getValue());
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(targetzone);
				}
			} else if ("stimulationpump".equalsIgnoreCase(moduleName)) {
				if (stimulation != null) {
					String strSql3= "FROM StimulationPump WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND stimulationUid=:stimulationUid AND operationUid=:operationUid";
					paramsFields = new String[] {"stimulationUid", "operationUid"};
					paramsValues = new Object[] {stimulation.getStimulationUid(), thisActivity.getOperationUid(), thisActivity.getDailyUid()};
					List<StimulationPump> lsStimulationPump = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields, paramsValues);

					if (lsStimulationPump.size() == 0) {
						stimulationpump = new StimulationPump();
						stimulationpump.setStimulationUid(stimulation.getStimulationUid());
						stimulationpump.setOperationUid(thisActivity.getOperationUid());

						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(targetzone);
					} else {
						stimulationpump = lsStimulationPump.get(0);
					}

					for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()) {
						if (StringUtils.isNotBlank(fieldEntry.getKey()))
							PropertyUtils.setProperty(stimulationpump, fieldEntry.getKey(), fieldEntry.getValue());
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(stimulationpump);
				}
			}
		}
	}
}
