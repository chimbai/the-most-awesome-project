package com.idsdatanet.d2.pronet.operationSuspension;



import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.OperationSuspension;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;



public class OperationSuspensionDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		this.calcCumulativeCost(node);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		this.calcCumulativeCost(node); 
	}
	
	public void calcCumulativeCost(CommandBeanTreeNode node) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof OperationSuspension){
													
			OperationSuspension OperationSuspension = (OperationSuspension) obj;
			//re-calculate the cum cost daily based
			
			OperationSuspensionUtils.calcCumulativeCost(OperationSuspension.getOperationUid(), OperationSuspension.getDailyUid());
		}
	}
}


