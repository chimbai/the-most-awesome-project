package com.idsdatanet.d2.pronet.droppedInHole;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.DroppedInHole;
import com.idsdatanet.d2.core.model.DroppedInHoleLog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DroppedInHoleDataNodeListener extends EmptyDataNodeListener{
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof DroppedInHole && (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed)) {
			
			DroppedInHole droppedinhole = (DroppedInHole) obj;

			DroppedInHoleLog eventLog = new DroppedInHoleLog();
			eventLog.setDroppedInHoleUid(droppedinhole.getDroppedInHoleUid());
			
			if (node.getDynaAttr().get("eventType") != null){
				eventLog.setEventType((String) node.getDynaAttr().get("eventType"));
			}
			
			if (node.getDynaAttr().get("fishingTool") != null){
				eventLog.setFishingTool((String) node.getDynaAttr().get("fishingTool"));
			}
			
			Date eventStartDateTime = null;
			Boolean isUseClientMVC = ((BaseCommandBean)commandBean).getUseClientMVCRenderer();
			if(node.getDynaAttr().get("eventStartDateTime") instanceof java.util.Date){
				eventStartDateTime = (Date) node.getDynaAttr().get("eventStartDateTime");				
			}else{
				String startDateTime = (String) node.getDynaAttr().get("eventStartDateTime");
				if (isUseClientMVC){
					Long longEpochMs = Long.parseLong(startDateTime);
					eventStartDateTime = new Date(longEpochMs);
				}else{
					if(StringUtils.isNotBlank(startDateTime)){
						eventStartDateTime = CommonDateParser.parse(startDateTime);
					}
				}
			}
			eventLog.setEventStartDateTime(eventStartDateTime);
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(eventLog);
			
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof DroppedInHole){
			DroppedInHole droppedInHole = (DroppedInHole) obj;
		
			String strSql = "FROM DroppedInHoleLog WHERE droppedInHoleUid= :droppedInHoleUid and (isDeleted is null or isDeleted = false) ORDER BY eventStartDateTime DESC";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "droppedInHoleUid", droppedInHole.getDroppedInHoleUid());
			
			if (!lstResult.isEmpty()) {
				DroppedInHoleLog eventLog = (DroppedInHoleLog) lstResult.get(0);
				
				if (eventLog.getEventStartDateTime() != null) {
					node.getDynaAttr().put("eventStartDateTime", eventLog.getEventStartDateTime());
				}
				
				if (StringUtils.isNotBlank(eventLog.getEventType())) {
					node.getDynaAttr().put("eventType", eventLog.getEventType());
				}
				
				if (StringUtils.isNotBlank(eventLog.getFishingTool())) {
					node.getDynaAttr().put("fishingTool", eventLog.getFishingTool());
				}
			}
		}
	}

}
