package com.idsdatanet.d2.pronet.productionString;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringCommandBeanListener implements CommandBeanListener {
	private List<String> excludedFieldsOnCopyProductionString =null;
	private List<String> excludedFieldsOnCopyProductionStringDetail =null;
	
	public void setExcludedFieldsOnCopyProductionString(
			List<String> excludedFieldsOnCopyProductionString) {
		this.excludedFieldsOnCopyProductionString = excludedFieldsOnCopyProductionString;
	}
	public void setExcludedFieldsOnCopyProductionStringDetail(
			List<String> excludedFieldsOnCopyProductionStringDetail) {
		this.excludedFieldsOnCopyProductionStringDetail = excludedFieldsOnCopyProductionStringDetail;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(ProductionString.class)){
				if("@copyProductionStringFrom".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					UserSession session = UserSession.getInstance(request);
					String copyProductionStringUid = (String) targetCommandBeanTreeNode.getDynaAttr().get("copyProductionStringFrom");
					//create child node
					loopAllNodeAndAddChildrenToNode(commandBean, targetCommandBeanTreeNode, copyProductionStringUid, session);
				}
				
				commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
			}
		}
	}	
	/**
	 * Method used to copy selected production string data to current node and child node
	 * @param commandBean
	 * @param targetNode
	 * @param wellHeadUid
	 * @param session
	 */
	private void loopAllNodeAndAddChildrenToNode(CommandBean commandBean, CommandBeanTreeNode targetNode, String productionStringUid, UserSession session) throws Exception {	
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);

		if(targetNode.getDataDefinition().getTableClass().equals(ProductionString.class)){
			if (targetNode.getData() instanceof ProductionString){
				ProductionString productionString = (ProductionString) targetNode.getData();
				ProductionString selectedPs = (ProductionString) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ProductionString.class, productionStringUid);
				PropertyUtils.copyProperties(productionString, selectedPs);
				
				//after copy from selected production string, need to convert all Double value with UOM and Datum to current uom and datum. 
				//And set those excluded fields and primary key to null for creating a new record
				CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, ProductionString.class, productionString, thisConverter, this.excludedFieldsOnCopyProductionString);
						
				//query ProductionStringDetail data for selected production string
				List<ProductionStringDetail> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ProductionStringDetail where (isDeleted = false or isDeleted is null) and productionStringUid = :productionStringUid", "productionStringUid", productionStringUid);
				if (list != null && list.size() > 0) {
					for (ProductionStringDetail psDetail : list) {					
													
						//after copy from selected production string, need to convert all Double value with UOM and Datum to current uom and datum. 
						//And set those excluded fields and primary key to null for creating a new record						
						CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, ProductionStringDetail.class, psDetail, thisConverter, this.excludedFieldsOnCopyProductionStringDetail);
						targetNode.addCustomNewChildNodeForInput(psDetail);
					
					}
				}
			}

		}
	}
	
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}


	
	
}
