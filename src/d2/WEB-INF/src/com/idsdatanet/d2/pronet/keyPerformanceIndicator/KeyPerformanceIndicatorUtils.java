package com.idsdatanet.d2.pronet.keyPerformanceIndicator;

import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class KeyPerformanceIndicatorUtils {

	/**
	 * All common utility related to KPI.
	 * @author ekhoo
	 *
	 */
	
	/**
	 * Common Method to calculate score value from formula (designed div actual)
	 * @param actual
	 * @param designed
	 * @throws Exception
	 */
	public static Double getScore(Double actual, Double designed) throws Exception {
		
		if (actual == null) actual = 0.0;
		if (designed == null) designed = 0.0;
		
		Double score = 0.0;

		if (actual <=0 && designed > 0) { //If Actual = 0, Score = 100
			score = 100.0; 

		//If Designed = 0, Score = 0			
		//If Actual & Designed is empty or 0, Score to display 0
		}else if (designed <=0) {
			score = 0.0;
			
		}else {
			score = (designed / actual) * 100;
		}
			
		return score;
	}
	
	/**
	 * Common Method to calculate score value from formula 2 (actual div designed)
	 * @param actual
	 * @param designed
	 * @throws Exception
	 */
	public static Double getScore2(Double actual, Double designed) throws Exception {
		
		if (actual == null) actual = 0.0;
		if (designed == null) designed = 0.0;
		
		Double score = 0.0;

		if (actual <=0 && designed > 0) { //If Actual = 0, Score = 100
			score = 100.0; 

		//If Designed = 0, Score = 0			
		//If Actual & Designed is empty or 0, Score to display 0
		}else if (designed <=0) {
			score = 0.0;
			
		}else {
			score = (actual / designed) * 100;
		}
			
		return score;
	}
	
	
	/**
	 * Common Method to get report daily id
	 * @param operationUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public static String getReportDailyUid (String operationUid, String dailyUid) throws Exception{
		
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		
		String reportDailyUid = null;
		String[] paramsFields = {"thisOperationUid", "thisDailyUid", "reportType"};
 		Object[] paramsValues = new Object[3];
		paramsValues[0] = operationUid;
		paramsValues[1] = dailyUid;
		paramsValues[2] = reportType;
		
		String strSql = "select reportDailyUid FROM ReportDaily where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and reportType = :reportType and (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields,paramsValues);

		if (!lstResult.isEmpty()) {
			Object uid = (Object) lstResult.get(0);
			if (uid != null) reportDailyUid = uid.toString();
			
		}
		
		
		return reportDailyUid;
	}

}
