package com.idsdatanet.d2.pronet.stimulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.StimulationFluid;
import com.idsdatanet.d2.core.model.StimulationFluidAdditives;
import com.idsdatanet.d2.core.model.StimulationFluidBlend;
import com.idsdatanet.d2.core.model.StimulationInformation;
import com.idsdatanet.d2.core.model.StimulationPump;
import com.idsdatanet.d2.core.model.TargetZone;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class StimulationDataNodeListener extends EmptyDataNodeListener {
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		Object object = node.getData();
		if(object instanceof StimulationInformation) {
			Double designProppantPlaced = 0.00;
			Double actualProppantPlaced = 0.00;
			
			StimulationInformation thisStimulationInformation = (StimulationInformation) object;
			
			if(thisStimulationInformation.getDesignProppantPlaced() != null) {
				designProppantPlaced = this.getBaseValue(thisConverter, StimulationInformation.class, "designProppantPlaced", thisStimulationInformation.getDesignProppantPlaced());			
			}
			if(thisStimulationInformation.getActualProppantPlaced() != null) {
				actualProppantPlaced = this.getBaseValue(thisConverter, StimulationInformation.class, "actualProppantPlaced", thisStimulationInformation.getActualProppantPlaced());
			}
			
			Double percentDesignProppantPlaced = 0.0;
			if (designProppantPlaced == 0.00){
				percentDesignProppantPlaced = null;
			}
			else {
				percentDesignProppantPlaced = actualProppantPlaced / designProppantPlaced ;
			}
			
			if (percentDesignProppantPlaced != null){
				thisConverter.setReferenceMappingField(StimulationInformation.class, "percentDesignProppantPlaced");
				thisConverter.setBaseValue(percentDesignProppantPlaced);
				percentDesignProppantPlaced = thisConverter.getConvertedValue();
			}
			
			thisStimulationInformation.setPercentDesignProppantPlaced(percentDesignProppantPlaced);
		}
		if(object instanceof StimulationFluid) {
			StimulationFluid thisStimulationFluid = (StimulationFluid) object;
			thisStimulationFluid.setDailyUid(null);
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();

		if(object instanceof StimulationFluidBlend) {
			StimulationFluidBlend thisStimulationFluidBlend = (StimulationFluidBlend) object;
			String stimulationFluidUid = thisStimulationFluidBlend.getStimulationFluidUid();
			String strSql = "SELECT stimulationUid FROM StimulationFluid WHERE (isDeleted = false or isDeleted is null) AND stimulationFluidUid = :stimulationFluidUid";
			String[] paramsFields = {"stimulationFluidUid"};
			Object[] paramsValues = {stimulationFluidUid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty()) {
				String stimulationUid = (String) lstResult.get(0);
				thisStimulationFluidBlend.setStimulationUid(stimulationUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisStimulationFluidBlend);
			}

		}
		if(object instanceof StimulationFluidAdditives) {
			StimulationFluidAdditives thisStimulationFluidAdditives = (StimulationFluidAdditives) object;
			String stimulationFluidUid = thisStimulationFluidAdditives.getStimulationFluidUid();
			String strSql = "SELECT stimulationUid FROM StimulationFluid WHERE (isDeleted = false or isDeleted is null) AND stimulationFluidUid = :stimulationFluidUid";
			String[] paramsFields = {"stimulationFluidUid"};
			Object[] paramsValues = {stimulationFluidUid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty()) {
				String stimulationUid = (String) lstResult.get(0);
				thisStimulationFluidAdditives.setStimulationUid(stimulationUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisStimulationFluidAdditives);
			}
		}
	}
	
	public Double getBaseValue(CustomFieldUom thisConverter, Class classType,String field, Double value) throws Exception
	{
		thisConverter.setReferenceMappingField(classType, field);
		thisConverter.setBaseValueFromUserValue(value,false);
		return thisConverter.getBasevalue();
	}
}
