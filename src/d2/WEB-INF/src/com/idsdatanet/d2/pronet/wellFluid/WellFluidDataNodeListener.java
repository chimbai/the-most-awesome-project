package com.idsdatanet.d2.pronet.wellFluid;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.WellFluid;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellFluidDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	private String recordType;
		
	public void setRecordType(String value){
		this.recordType = value;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof WellFluid) {
			WellFluid thisWellFluid = (WellFluid)object;
			
			if(this.recordType != null)
			{
				//SET THIS FIELD AS "load" FOR LOAD FLUID
				thisWellFluid.setType(this.recordType);
			}
		}
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(WellFluid.class)) {
			
			UserSession userSession = UserSession.getInstance(request);
			
			String queryString = "SELECT w.wellFluidUid FROM WellFluid w, Daily d";
			
			if(this.recordType != null)
			{
				queryString += " WHERE w.operationUid = :operationUid AND type = '" + this.recordType + "'";
			}
			else
			{
				queryString += " WHERE w.operationUid = :operationUid AND (type IS NULL OR type = '')";
			}
						
			queryString += " AND (w.isDeleted IS NULL OR w.isDeleted = '') AND w.groupUid = :groupUid AND d.dailyUid = w.dailyUid ORDER BY d.dayDate, w.fluidType";
			
			String[] paramsFields = {"operationUid", "groupUid"};
			Object[] paramsValues = {userSession.getCurrentOperationUid(), userSession.getCurrentGroupUid()};
			List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsFields, paramsValues);
			if (!items.isEmpty()){
				List<Object> wellFluidItem = new ArrayList<Object>();
								
				for(Object obj:items)
				{
					String wellFluidUid = obj.toString();
					Object wellFluid = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellFluid.class, wellFluidUid);
					if(wellFluid != null) 
					{
						wellFluidItem.add(wellFluid);
					}
				}
				
				return wellFluidItem;
			}
		}
		
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
		
}
