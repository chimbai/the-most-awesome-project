package com.idsdatanet.d2.pronet.materialConsumption;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;

public class MaterialConsumptionDataNodeListener implements DataNodeLoadHandler {
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(RigStock.class)) {
												
			String strSql = "FROM RigStock rs,LookupRigStock lrs WHERE rs.operationUid = :operationUid AND rs.groupUid = :groupUid AND (rs.isDeleted IS NULL OR rs.isDeleted = '') AND rs.dailyUid= :dailyUid AND rs.type='filtrationmaterialstock' " +
							"AND rs.stockCode = lrs.lookupRigStockUid ORDER BY lrs.stockBrand";
			String[] paramNames =  {"operationUid", "groupUid","dailyUid"};
			Object[] paramValues = {userSelection.getOperationUid(), userSelection.getGroupUid(),userSelection.getDailyUid()};
						
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object[]  objResult: items){				
				RigStock rigStockObj = (RigStock) objResult[0];
				output_maps.add(rigStockObj);
			}
						
			return output_maps;
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	
	
}
