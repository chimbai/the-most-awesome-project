package com.idsdatanet.d2.pronet.materialConsumption;


import javax.servlet.http.HttpServletRequest;


import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.rigstock.RigStockUtil;


/* for sites which are using cascade lookup on stock name and unit (ticket 20110308-0915-scwong), please follow the field mapping as below:
 *  - map Stock Name column to @stockName
 *  - map Unit to stockCode (to keep the lookupRigStockUid in order to calculate the balance and cost) 
 *  - can refer to override commandbean in santos_csm_rig_stock under deployment folder and rigStock_alt_p1.xml
 */


public class MaterialConsumptionCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener{
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigStock.class)) {
				
				if("stockCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					stockCodeChanged(request, commandBean, targetCommandBeanTreeNode);
				}
			}
		}
	}

	private void stockCodeChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		if(Action.DELETE.equals(node.getAtts().getAction())) return; //why do we need to test for "delete" ? ssjong - 04/may/2009
		
		RigStock thisRigStock = (RigStock) node.getData();
		UserSession userSession = UserSession.getInstance(request);
		
		//check if today is the first day of operation or not to determine want to show the starting amount
		node.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationStockcodeFirstDay(userSession.getCurrentOperationUid(), thisRigStock.getStockCode(), userSession.getCurrentDailyUid()));
		RigStockUtil.setIsBatchDrillFlag(node, new UserSelectionSnapshot(UserSession.getInstance(request)));
	}	

}
