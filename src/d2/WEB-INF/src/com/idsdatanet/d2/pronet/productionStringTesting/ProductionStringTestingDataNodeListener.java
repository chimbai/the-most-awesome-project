package com.idsdatanet.d2.pronet.productionStringTesting;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionStringTesting;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionStringTestingDataNodeListener extends EmptyDataNodeListener{
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof ProductionStringTesting) {
			ProductionStringTesting stringtest = (ProductionStringTesting) object;
		
			String stringType = stringtest.getStringType();
			Double stringSize = stringtest.getStringSize();
			Double stringLength = stringtest.getStringLength();
			String stringBrand = stringtest.getStringBrand();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			//calculate cum. run hour for the strings with same string type, string size, string length and string band
			String[] paramsFields = {"wellboreUid", "stringType", "stringLength", "stringSize", "stringBrand"};
			Object[] paramsValues = {userSelection.getWellboreUid(), stringType, stringLength, stringSize, stringBrand}; 
			
			String strSql = "SELECT sum(runHour) as cumRunHour FROM ProductionStringTesting WHERE (isDeleted = false or isDeleted is null) " +
					"AND wellboreUid =:wellboreUid AND stringType=:stringType AND stringSize=:stringSize AND stringLength=:stringLength AND stringBrand=:stringBrand";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			Object a = (Object) lstResult.get(0);
			Double cumRunHour = 0.00;
			if (a != null) cumRunHour = Double.parseDouble(a.toString());
			
			thisConverter.setReferenceMappingField(ProductionStringTesting.class, "runHour");
			thisConverter.setBaseValue(cumRunHour);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumRunHour", thisConverter.getUOMMapping());
			}
			
			node.getDynaAttr().put("cumRunHour", thisConverter.getConvertedValue());
			
			//calculate cum. cut length for the strings with same string type, string size, string length and string band
			strSql = "SELECT sum(cutLength) as cumCutLength FROM ProductionStringTesting WHERE (isDeleted = false or isDeleted is null) " +
					"AND wellboreUid =:wellboreUid AND stringType=:stringType AND stringSize=:stringSize AND stringLength=:stringLength AND stringBrand=:stringBrand";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			Object b = (Object) lstResult.get(0);
			Double cumCutLength = 0.00;
			if (b != null) cumCutLength = Double.parseDouble(b.toString());
			thisConverter.setReferenceMappingField(ProductionStringTesting.class, "cutLength");
			thisConverter.setBaseValue(cumCutLength);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumCutLength", thisConverter.getUOMMapping());
			}

			node.getDynaAttr().put("cumCutLength", thisConverter.getConvertedValue());
			
			
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof ProductionStringTesting){
			ProductionStringTesting productionStringTesting = (ProductionStringTesting) obj;
			
			if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				if (daily != null) {
					productionStringTesting.setTestDateTime(daily.getDayDate());
					productionStringTesting.setSpoolDateTime(daily.getDayDate());
				}
			}
		}
	}
		

}
