package com.idsdatanet.d2.pronet.productionFluid;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.ProductionFluid;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class ProductionFluidDataNodeListener extends EmptyDataNodeListener {
	private String recordType;
		
	public void setRecordType(String value){
		this.recordType = value;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof ProductionFluid) {
			ProductionFluid thisProductionFluid = (ProductionFluid)object;
			
			if(this.recordType != null)
			{
				//SET THIS FIELD AS "aux" FOR AUXILIARY WATER PRODUCTION FLUID
				thisProductionFluid.setRecordType(this.recordType);
			}
		}
	}
}
