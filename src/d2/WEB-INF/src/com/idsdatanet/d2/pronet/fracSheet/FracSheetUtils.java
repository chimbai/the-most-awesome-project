package com.idsdatanet.d2.pronet.fracSheet;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.FracSheet;
import com.idsdatanet.d2.core.model.FracSheetDetail;
import com.idsdatanet.d2.core.model.KeyPerformanceIndicator;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;



/**
 * All common utility related to KPI.
 * @author ekhoo
 *
 */
public class FracSheetUtils {
	
	/**
	 * Common Method to calculate total prop pumped of the day 
	 * @param nodes, CommandBeanTreeNode parent
	 * @throws Exception
	 */
	public static void getTotalPropPumped(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent)  throws Exception{
		
		Double totalPropPumped = 0.0;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
		
		String fracSheetUid=null;
		if (parent.getData() instanceof FracSheet)
		{
			FracSheet fracsheet = (FracSheet)parent.getData();
			fracSheetUid = fracsheet.getFracSheetUid();
			
			for(CommandBeanTreeNode node:nodes)
			{
				if (node.getData() instanceof FracSheetDetail)
				{
					FracSheetDetail fsdetail =(FracSheetDetail) node.getData();
					if (fsdetail.getProppantPumpedWeight()!=null)
					{
						thisConverter.setReferenceMappingField(FracSheetDetail.class, "proppantPumpedWeight");
						thisConverter.setBaseValueFromUserValue(fsdetail.getProppantPumpedWeight());
						totalPropPumped+=thisConverter.getBasevalue();
					}
				}
			}
			
			String strSqlUpdate = "UPDATE KeyPerformanceIndicator SET proppantPlacedDesigned = :totalPropPumped where fracSheetUid =:fracSheetUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, new String[] {"fracSheetUid", "totalPropPumped"}, new Object[] {fracSheetUid, totalPropPumped}, qp);

		}
			
	}
	/**
	 * Common Method to calculate Max Prop conc of the day 
	 * @param CommandBeanTreeNode parent
	 * @throws Exception
	 */
	public static void getMaxPropConc(CommandBeanTreeNode parent) throws Exception{
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String fracSheetUid=null;
		if (parent.getData() instanceof FracSheet)
		{
			FracSheet fracsheet = (FracSheet)parent.getData();
			fracSheetUid = fracsheet.getFracSheetUid();
			
			Double maxProppantConc = null;
			String strSql = "Select MAX(fracDensityTo) FROM FracSheetDetail WHERE (isDeleted = false or isDeleted is null) and fracSheetUid =:fracSheetUid";		
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "fracSheetUid", fracSheetUid, qp);
			
			if (lstResult.size() > 0){
				if (lstResult.get(0) !=null)
				maxProppantConc = Double.parseDouble(lstResult.get(0).toString());
				// save value to proppantConcentrationDesigned for kpi with same fracsheetuid 
				String strSqlUpdate = "UPDATE KeyPerformanceIndicator SET proppantConcentrationDesigned = :maxProppantConc where fracSheetUid =:fracSheetUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, new String[] {"fracSheetUid", "maxProppantConc"}, new Object[] {fracSheetUid, maxProppantConc}, qp);
			}
			
	
		}
			
	}
	/**
	 * Common Method to calculate pumped duration
	 * @param CommandBeanTreeNode parent
	 * @throws Exception
	 */
	public static void getPumpDuration (CommandBeanTreeNode parent) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
		
		String fracSheetUid=null;
		if (parent.getData() instanceof FracSheet)
		{
			FracSheet fracsheet = (FracSheet)parent.getData();
			fracSheetUid = fracsheet.getFracSheetUid();

			String strSql = "Select MIN(fracDateTime), MAX(fracDateTime) FROM FracSheetDetail WHERE (isDeleted = false or isDeleted is null) and fracSheetUid =:fracSheetUid";		
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "fracSheetUid", fracSheetUid, qp);
			
			if (lstResult.size() > 0){	
				Object[] obj = lstResult.get(0);
				
				if (obj[0] !=null && obj[1]!=null) {
					Date start = (Date) obj[0];
					Date end = (Date) obj[1];
					//call to method to calculate duration base on 2 dates;
					Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
					thisConverter.setReferenceMappingField(KeyPerformanceIndicator.class, "pumpDurationDesigned");				
					thisConverter.setBaseValue(thisDuration);
					// save value to pumpDurationDesigned for kpi with same fracsheetuid 
					double pumpDuration = thisConverter.getBasevalue();
					String strSqlUpdate = "UPDATE KeyPerformanceIndicator SET pumpDurationDesigned = :pumpDuration where fracSheetUid =:fracSheetUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, new String[] {"fracSheetUid", "pumpDuration"}, new Object[] {fracSheetUid, pumpDuration}, qp);
				}
			}
		}

	}

}
