package com.idsdatanet.d2.pronet.slickline;

import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Slickline;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * All common utility related to Slickline
 * @author ekhoo
 *
 */
public class SlicklineUtil{
	/**
	 * Common Method for calculate cummulative cost 
	 * @param object
	 * @throws Exception Standard Error Throwing Exception
	 * @return cumCost
	 */
	public static Double calculateCumCost(Object object) throws Exception{
		
		Double cumCost = 0.0;
		
		if (object instanceof Slickline){
			Slickline slickline = (Slickline) object;
			
			String strSql = "select sum(cost) from Slickline where (isDeleted = false or isDeleted is null) " +
					"and operationUid=:operationUid and startDatetime <:currentSlickline order by startDatetime";
				
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
					new String[] {"operationUid", "currentSlickline"},
					new Object[] {slickline.getOperationUid(), slickline.getStartDatetime()});			
			
			if (lstResult.size()>0) {
				if (lstResult.get(0) != null) {	
					cumCost = Double.parseDouble(lstResult.get(0).toString());
				}
			}
			
			return cumCost + (slickline.getCost()==null?0.0:slickline.getCost());
		}
		
		return cumCost;
	}
	/**
	 * Common Method for updating slickline start date and end date when report daily date is changed
	 * @param rd
	 * @throws Exception Standard Error Throwing Exception
	 */
	public static void updateSlicklineDate(ReportDaily rd) throws Exception
	{
		List<Slickline> slkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Slickline Where (isDeleted is null or isDeleted = false) and dailyUid=:dailyUid","dailyUid",rd.getDailyUid());
		for (Slickline slk: slkList)
		{
			if (slk.getStartDatetime()!=null)
				slk.setStartDatetime(CommonUtil.getConfiguredInstance().setDate(slk.getStartDatetime(),rd.getReportDatetime()));
			if (slk.getEndDatetime()!=null)
				slk.setEndDatetime(CommonUtil.getConfiguredInstance().setDate(slk.getEndDatetime(),rd.getReportDatetime()));
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(slk);
		}
	}
}
