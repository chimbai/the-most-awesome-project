package com.idsdatanet.d2.pronet.wellProductionTest;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.WellProductionTest;
import com.idsdatanet.d2.core.model.WellProductionTestDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * All common utility related to WellProductionTest
 * @author ekhoo
 *
 */

public class WellProductionTestUtil {
	/**
	 * Common Method for re-calculate all the cumulative value to date if value changed on a particular day
	 * @param operationUid
	 * @param fieldName - field for calculating cum value 
	 * @param cumFieldName - field to update 
	 * @throws Exception
	 * */
	public static void calcCumulativeValueToDate(String operationUid, String fieldname, String cumFieldName) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double cumValue = null;
		
		//query1 : get a list of well production test UID for current operation  
		String sql = "select wellProductionTestUid from WellProductionTest where operationUid=:operationUid and (isDeleted = false or isDeleted is null)";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid",operationUid);
		
		if (list.size() > 0){
			//query2 : to query a list of date (based on dailyUid) and well production uid from query1 
			String strSql ="select d.dayDate, wt.wellProductionTestUid From WellProductionTest wt, Daily d where " +
				"(wt.isDeleted=false or wt.isDeleted is null) and " +
				"wt.wellProductionTestUid IN ( :lstdata ) and wt.dailyUid = d.dailyUid and " +
				"wt.operationUid=:operationUid order by d.dayDate";
			
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
					new String[] {"lstdata", "operationUid"}, new Object[] {list, operationUid});
			
			if (lstResult.size() > 0){
				//loop all the result from query2 to re-calculate the sum value to date and updated to the record 
				
				for (Object[] obj: lstResult){
					//get sum value where the well production test date is equal or before the date from query2
					String strSql2 = "select sum(wt." + fieldname + ") from WellProductionTest wt, Daily d where (wt.isDeleted = false or wt.isDeleted is null) " +
						"and (d.isDeleted = false or d.isDeleted is null) and wt.operationUid=:operationUid and " +
						"d.dayDate <=:currentRecordDate and wt.wellProductionTestUid IN ( :lstdata ) and " +
						"d.dailyUid=wt.dailyUid order by d.dayDate";
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,
							new String[] {"operationUid", "currentRecordDate", "lstdata"},
							new Object[] {operationUid, obj[0], list}, qp);			
					
					if (lstResult2.size()>0) {
						if (lstResult2.get(0) != null) {
							//sum value from query 
							cumValue = Double.parseDouble(lstResult2.get(0).toString());
						}
							
						//get wellproductionTestUid for current record 
						String wellProductionTestUid = obj[1].toString();
						
						//update the result to each of the record
						String strSql3 = "FROM WellProductionTest WHERE (isDeleted = false or isDeleted is null) and wellProductionTestUid =:wellProductionTestUid";		
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "wellProductionTestUid", wellProductionTestUid, qp);
						
						if (lstResult3.size() > 0){					
							WellProductionTest wellTesting = (WellProductionTest) lstResult3.get(0);
							
							/*cumfieldName is the field for update 
							  set cumValue to the cumFieldName field on the object 
							*/
							PropertyUtils.setProperty(wellTesting, cumFieldName, cumValue);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wellTesting, qp);
						}
					}
				}
			}	
		}
	}
	
	/**
	 * Common Method to get parent uid from parent node 
	 * @param node
	 * @throws Exception
	 * */
	
	public static String getParentUid(CommandBeanTreeNode node) throws Exception {
		String wellProductionTestUid = null;
		if (node.getData() instanceof WellProductionTest)
		{
			WellProductionTest wt =(WellProductionTest) node.getData();
			wellProductionTestUid = wt.getWellProductionTestUid();
		}
		
		return wellProductionTestUid;
	}
	
	/**
	 * Common Method to update average gas rate and water rate when there is changes (update or delete) from detail section
	 * @param session
	 * @param parent
	 * @throws Exception
	 * */
	public static void updateAvgGasAndWaterRate(UserSession session, String wellProductionTestUid, QueryProperties qp) throws Exception {
		
		Double avgGasRate = 0.00;
		Double avgWaterRate = 0.00;
		
		if (StringUtils.isNotBlank(wellProductionTestUid)) {
			avgGasRate = WellProductionTestUtil.calcAvgFromFlowBackDetail(wellProductionTestUid, session.getCurrentDailyUid(), "gasRateStandardVolume", qp);
			avgWaterRate = WellProductionTestUtil.calcAvgFromFlowBackDetail(wellProductionTestUid, session.getCurrentDailyUid(), "waterVolumeFlowRate" ,qp);
			
			String strSql1 = "UPDATE WellProductionTest SET gasRateStandardVolume =:avgGasRate, waterFlowRate=:avgWaterRate WHERE " +
							"wellProductionTestUid =:wellProductionTestUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, new String[] {"avgGasRate", "avgWaterRate", "wellProductionTestUid"}, new Object[] {avgGasRate, avgWaterRate, wellProductionTestUid}, qp);

		}

	}
	/**
	 * Common Method to update cum. water volume when there is changes (update or delete) from detail section
	 * @param nodes
	 * @param session
	 * @param parent
	 * @throws Exception
	 * */
	public static void updateCumWaterVolFromDetail(Collection<CommandBeanTreeNode> nodes, String wellProductionTestUid, QueryProperties qp, CustomFieldUom thisConverter) throws Exception
	{
		Double totalWaterVol = 0.00;
	
		for(CommandBeanTreeNode node:nodes){
			if (node.getData() instanceof WellProductionTestDetail){
				WellProductionTestDetail wtd =(WellProductionTestDetail) node.getData();
				if (wtd.getWaterVolume()!=null) {
					totalWaterVol+=CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, WellProductionTestDetail.class, "waterVolume", wtd.getWaterVolume());
				}
			}
			
		}
		if (StringUtils.isNotBlank(wellProductionTestUid)) {
			String strSql1 = "UPDATE WellProductionTest SET totalWaterPumped =:totalWaterVol WHERE wellProductionTestUid =:wellProductionTestUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, new String[] {"totalWaterVol", "wellProductionTestUid"}, new Object[] {totalWaterVol, wellProductionTestUid}, qp);
		}
	}
		
	/**
	 * Common Method to query average value within the well production test detail 
	 * @param wellProductionTestUid
	 * @param dailyUid
	 * @param fieldname
	 * @param qp
	 * @throws Exception
	 * */
	public static Double calcAvgFromFlowBackDetail(String wellProductionTestUid, String dailyUid, String fieldname, QueryProperties qp) throws Exception {

		String[] paramsFields = {"dailyUid", "wellProductionTestUid"};
		Object[] paramsValues = new Object[2]; 
		paramsValues[0] = dailyUid;
		paramsValues[1] = wellProductionTestUid;
		
		String strSql = "SELECT AVG(" + fieldname + ") as avgValue FROM WellProductionTestDetail WHERE (isDeleted = false or isDeleted is null) " +
						" AND dailyUid = :dailyUid AND wellProductionTestUid =:wellProductionTestUid";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		Double avgValue = null;
		if (a != null) avgValue = Double.parseDouble(a.toString());
		
		return avgValue;
	}
	
	/**
	 * Common Method to calculate Chock C based on orificeSize
	 * @param orificeSize
	 * @throws Exception
	 * */
	
	public static Double calculateChokeC(Double orificeSize) throws Exception {
		Double chokeC = 0.0;		
		chokeC = ((Math.pow(orificeSize/64, 2.0871)) * 26.616) * 18;
		return chokeC;
	}
	
	/**
	 * Common Method to calculate water volume with formula given
	 * @param waterRate
	 * @param timeStart
	 * @param timeEnd
	 * @param thisConverter
	 * @throws Exception
	 * */
	public static Double calculateWaterVolume(Double waterRate, Date timeStart, Date timeEnd, CustomFieldUom thisConverter) throws Exception {
		Double waterVol = 0.0;
		
		// get duration from start time and end time - always in hour			
		Long duration = CommonUtil.getConfiguredInstance().calculateDuration(timeStart, timeEnd);
		thisConverter.setReferenceMappingField(Activity.class, "activityDuration");
		thisConverter.setBaseValueFromUserValue(duration);
		thisConverter.changeUOMUnit("Hour");
		Double testDuration = thisConverter.getConvertedValue();
		
		//get water rate - always in bbl/d
		thisConverter.setReferenceMappingField(WellProductionTestDetail.class, "waterVolumeFlowRate");
		thisConverter.setBaseValueFromUserValue(waterRate);
		thisConverter.changeUOMUnit("BarrelsPerDay");
		Double waterFlowRate = thisConverter.getConvertedValue();
		
		//after calculate waterVol in bbl
		waterVol = (waterFlowRate * ((testDuration/24) / (60*24)));
		
		//need to covert to IDS base CubicMetre and get the converted value to save
		thisConverter.setReferenceMappingField(WellProductionTestDetail.class, "waterVolume");
		thisConverter.setBaseValueFromUserValue(waterVol);
		return thisConverter.getConvertedValue();
		
	}
	
	/**
	 * Time comparator to sort test start time or end time well production test detail
	 * @param CommandBeanTreeNode
	 * @throws Exception
	 * */
	public static class TimeComparator implements Comparator<CommandBeanTreeNode>{
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
			try{
				WellProductionTestDetail wtd1 = (WellProductionTestDetail) o1.getData();
				WellProductionTestDetail wtd2 = (WellProductionTestDetail) o2.getData();
				Date date1 = null;
				Date date2 = null;
				//check either use startDatetime or endDatetime to do sorting
				if(wtd1.getTestDateTime()!=null && wtd2.getTestDateTime()!=null){
					date1 = wtd1.getTestDateTime();
					date2 = wtd2.getTestDateTime();
					
				}else if (wtd1.getTestEndDateTime()!=null && wtd2.getTestEndDateTime()!=null){
					date1 = wtd1.getTestEndDateTime();
					date2 = wtd2.getTestEndDateTime();
				}
				else{
					date1 = null;
					date2 = null;
				}
				
				if(date1 == null && date2 == null){
					return 0;
				}else if(date1 == null){
					return -1;
				}else if(date2 == null){
					return 1;
				}else{
					return date1.compareTo(date2);
				}
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return 0;
			}
		}
	}
	/**
	 * Common Method to update well production test (value populated from report daily screen) when there is any changes on the related fields
	 * @param obj
	 * @param session
	 * @throws Exception
	 * */
	public static void updateFlowBackData(ReportDaily rd, UserSession session) throws Exception
	{
		List<WellProductionTest> wptResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From WellProductionTest Where (isDeleted is null or isDeleted = false) and dailyUid=:dailyUid","dailyUid",rd.getDailyUid());
		for (WellProductionTest wpt: wptResult)
		{
			wpt = WellProductionTestUtil.populateValueFromReportDaily(wpt, session, rd);			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wpt);
		}
	}
	
	/**
	 * Method to update value from report daily screen 
	 * @param obj
	 * @param session
	 * @throws Exception
	 * */
	public static WellProductionTest populateValueFromReportDaily(Object obj, UserSession session, ReportDaily rd) throws Exception {
		
		if (obj instanceof WellProductionTest) {
			WellProductionTest thisWellProductionTest = (WellProductionTest) obj;
			if (rd ==null) {
				rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, session.getCurrentDailyUid());
			}
			
			thisWellProductionTest.setOperatingCompanyRepresentative(rd.getSupervisor());
			thisWellProductionTest.setCost(rd.getDaycost());
			thisWellProductionTest.setContractor(rd.getRigInformationUid());
			
			//populate cumcost from report daily / costnet
			Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(session.getCurrentDailyUid(), null);
			thisWellProductionTest.setCumCost(cumCost);
			return thisWellProductionTest;
		}
		
		return null;
	}
		
}
