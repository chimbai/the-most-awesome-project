package com.idsdatanet.d2.pronet.perforation;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Perforation;
import com.idsdatanet.d2.core.model.PerforationGunConfiguration;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PerforationDataNodeListener extends EmptyDataNodeListener {

	/* isPopulateGunDetailByGunNumber added for Santos CSM ticket:20110524-1711-ysim 
	 * default value = true, as most of the client auto populate the gun detail by selecting gun number 
	 * Santos CSM using isPopulateGunDetailByGunNumber=false - allow to manual entry gun configuration detail from perforation screen
	 */
	private Boolean isPopulateGunDetailByGunNumber = true;
	
	/* Jira ticket: 16237 - set gun number to be null while doing copy data from yesterday function in start new day*/
	
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		//on carry record data, gun number will be set to null
		
		Object object = node.getData();
		
		if (object instanceof Perforation) {
			Perforation perforation = (Perforation)object;
			perforation.setPerforationGunConfigurationUid(null);
			
		}

	}
	
	public void setIsPopulateGunDetailByGunNumber(Boolean value) {
		this.isPopulateGunDetailByGunNumber = value;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Perforation) {
			Perforation thisPerforation = (Perforation) object;
			
			Double bottomMd = thisPerforation.getDepthBottomMdMsl();
			Double topMd = thisPerforation.getDepthTopMdMsl();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Perforation.class, "depthBottomMdMsl");
			//calculate interval
			Double interval = PerforationUtils.calculateInterval(topMd, bottomMd);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@interval", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("interval", interval);
			
			if (!this.isPopulateGunDetailByGunNumber){
				String perforationUid = thisPerforation.getPerforationUid();
				PerforationUtils.populateGunDetailToDynaAttr(node, "perforationUid = '" + perforationUid + "'");
			}else {
				//GET PERFORATION GUN CONFIGURATION DATA
				if(thisPerforation.getPerforationGunConfigurationUid() != null){				
					String perforationGunConfUid = thisPerforation.getPerforationGunConfigurationUid();				
					PerforationUtils.populateGunDetailToDynaAttr(node, "perforationGunConfigurationUid = '" + perforationGunConfUid + "'");		
				}
			}
			
			if (userSelection.getDailyUid() != null){
				Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());			
				Date todayDate = today.getDayDate();
				Date defaultDateTime = DateUtils.addHours(todayDate, 0);				
					
				thisPerforation.setPerforatedDateTime(defaultDateTime);		
			}
			
		}
	}
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof Perforation) {
			Perforation thisPerforation = (Perforation) object;
			
			Double gunShotDensity = null;
			Double shotTotal = null;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);		
			
			if(node.getDynaAttr().get("perfGunConf.gunShotsSpacer")!=null){
				if (StringUtils.isNotBlank(node.getDynaAttr().get("perfGunConf.gunShotsSpacer").toString()))
					gunShotDensity = Double.valueOf(node.getDynaAttr().get("perfGunConf.gunShotsSpacer").toString());
			}
			
			if (thisPerforation.getDepthBottomMdMsl() != null && thisPerforation.getDepthTopMdMsl() != null && gunShotDensity != null) {
				//calculation = (Bottom Depth - Top Depth ) * Shot Density
				shotTotal = PerforationUtils.calculateShotTotal(thisPerforation.getDepthTopMdMsl(), thisPerforation.getDepthBottomMdMsl(), gunShotDensity, thisConverter);
			}
			//no unit for calculatedShotTotal
			thisPerforation.setCalculatedShotTotal(shotTotal);
			
			if (thisPerforation.getDepthTopMdMsl() != null && thisPerforation.getDepthBottomMdMsl() != null){
				Boolean show = PerforationUtils.showBottomDepthMessage(thisPerforation.getDepthTopMdMsl(),  thisPerforation.getDepthBottomMdMsl(), thisConverter);
				if (show){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "depthBottomMdMsl", "Bottom Depth should be greater than Top Depth");
					return;
				}
			}	
		}
		
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
	
		if (!this.isPopulateGunDetailByGunNumber){
				
			Object object = node.getData();
			if (object instanceof Perforation) {
				Perforation thisPerforation = (Perforation) object;
				
				String perforationUid = thisPerforation.getPerforationUid();

				String strSql = "FROM PerforationGunConfiguration WHERE (isDeleted = false or isDeleted is null) and perforationUid =:perforationUid";
				List <PerforationGunConfiguration> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "perforationUid", perforationUid);
				if (lstResult.size() > 0){
					PerforationGunConfiguration gunDetails = (PerforationGunConfiguration) lstResult.get(0);					
					if (gunDetails !=null) {
						// do update 
						PerforationUtils.saveGunDetail(gunDetails, commandBean, node, request);
					}
				}else {
					PerforationUtils.doCreateNewGunConfig(perforationUid,commandBean, node, request);
					//do new 	
				}
			
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		if (!isPopulateGunDetailByGunNumber){
			//do delete
			Object object = node.getData();
			if (object instanceof Perforation) {
				Perforation thisPerforation = (Perforation) object;
				
				String perforationUid = thisPerforation.getPerforationUid();
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE PerforationGunConfiguration SET " +
						"isDeleted = true where perforationUid=:perforationUid", 
						new String[] {"perforationUid"}, new Object[] {perforationUid});
			}
		}
	}
	
	
}
