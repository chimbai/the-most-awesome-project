package com.idsdatanet.d2.pronet.productionStringLine;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.ProductionStringLine;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringLineDataNodeListener extends EmptyDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof ProductionStringLine){
			ProductionStringLine stringLine = (ProductionStringLine) obj;
			
			if (stringLine.getUninstallDateTime()!=null && stringLine.getUninstallDateTime().before(stringLine.getInstallDateTime())) {
				status.setContinueProcess(false, true);
				status.setFieldError(node, "uninstallDateTime", "Uninstall date/time cannot be earlier than Install date/time.");
			}

		}
	
	}
}
