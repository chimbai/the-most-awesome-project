package com.idsdatanet.d2.pronet.productionString;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class ProductionStringLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		
		param_names.add("wellboreUid"); 
		param_values.add(userSelection.getWellboreUid());
		
		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ProductionString where wellboreUid = :wellboreUid and (isDeleted = false or isDeleted is null) order by installDateTime", param_names, param_values); //.getObjects(CostAccountCodes.class);
		
		for (Object obj : list) {
			ProductionString thisProductionString = (ProductionString) obj;
			String key = thisProductionString.getProductionStringUid();
			String value = null;
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
			String startDateTime = null;
			String endDateTime = null;
			
			if (thisProductionString.getInstallDateTime()!=null){
				startDateTime = formatter.format(thisProductionString.getInstallDateTime());
			}
			
			if (thisProductionString.getUninstallDateTime() != null){
				endDateTime = formatter.format(thisProductionString.getUninstallDateTime());
			}
			
			if (startDateTime != null && endDateTime != null){
				value = thisProductionString.getStringNo() + " (" + startDateTime + " - " + endDateTime + ")";
			}else {
				value = thisProductionString.getStringNo();
			}
			
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
}
