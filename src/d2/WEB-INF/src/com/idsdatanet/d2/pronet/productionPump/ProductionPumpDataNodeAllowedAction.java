package com.idsdatanet.d2.pronet.productionPump;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.ProductionPumpParamComp;
import com.idsdatanet.d2.core.model.ProductionPumpParamCompLog;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionPumpDataNodeAllowedAction  implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if("add".equals(action)){
//			if(! node.getInfo().isNewlyCreated()){
//				if(! node.getInfo().isTemplateNode()){
//					if("BharunDailySummary".equals(targetClass)) {
//						if(Bharun.class.equals(node.getDataDefinition().getTableClass())){
//							Map child = node.getChild("BharunDailySummary");
//							if(child != null){
//								if(child.size() > 0) return false;
//							}
//						}
//					}
//				}
//			}
			if(! node.getInfo().isTemplateNode()){
				if("ProductionPumpParamCompLog".equals(targetClass)) {
					if(ProductionPumpParamComp.class.equals(node.getDataDefinition().getTableClass())){
						Map<String, CommandBeanTreeNode> compLog = node.getChild("ProductionPumpParamCompLog");
						if (compLog.size()>0){
							for (Iterator iter = compLog.entrySet().iterator(); iter.hasNext();)
							{  
								Map.Entry entry = (Map.Entry) iter.next();
								if (!"_empty_".equals(entry.getKey())) {
									CommandBeanTreeNode nodeComp=(CommandBeanTreeNode)entry.getValue();
									if (nodeComp.getData() instanceof ProductionPumpParamCompLog) {
										ProductionPumpParamCompLog productionPumpParamCompLog = (ProductionPumpParamCompLog) nodeComp.getData();
										if (productionPumpParamCompLog.getRemoveDatetime() == null) return false;
									}
								}
							}
							
						}
					}
					
				}
			}
		}
		return true;
	}
}
