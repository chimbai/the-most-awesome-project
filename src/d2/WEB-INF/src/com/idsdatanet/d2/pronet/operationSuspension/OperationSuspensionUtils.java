package com.idsdatanet.d2.pronet.operationSuspension;

import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.OperationSuspension;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


/**
 * All common utility related to Operation Suspension.
 * @author fmting
 *
 */
public class OperationSuspensionUtils {
	/**
	 * Common Method for calculate total daily cost per day
	 * @param operationUid, dailyid
	 * @throws Exception Standard Error Throwing Exception
	 * @return cumCost
	 */
	
	public static void calcCumulativeCost(String operationUid, String dailyid) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double cumCost = null;
		
		//query1 : to query a list of operation suspension uid which have daily cost as current edited record. 
		String sql = "select operationSuspensionUid from OperationSuspension where operationUid=:operationUid and (isDeleted = false or isDeleted is null)";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"operationUid"}, new Object[] {operationUid});
		
		if (list.size() > 0){
			//query2 : get a list of date (based on dailyuid) which have daily cost based on the list of uid from query1
			String strSql ="select d.dayDate, os.operationSuspensionUid From OperationSuspension os, Daily d where " +
				"(os.isDeleted=false or os.isDeleted is null) and " +
				"os.operationSuspensionUid in ( :typeList ) and os.dailyUid = d.dailyUid and " +
				"os.operationUid=:operationUid order by d.dayDate";
			
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
					new String[] {"typeList", "operationUid"}, new Object[] {list, operationUid});
			
			
			
			if (lstResult.size() > 0){
				
				//loop all the data that has cost - to recalculate and update all related records for cum. cost
				for (Object[] obj: lstResult){
					
					//sum the daily cost value where the date is equal or before current record date from query2 
					
					String strSql2 = "select sum(os.cost) as cumCost from OperationSuspension os, Daily d where (os.isDeleted = false or os.isDeleted is null) " +
					"and (d.isDeleted = false or d.isDeleted is null) and os.operationUid=:operationUid and " +
					"d.dayDate <=:todayDate and os.operationSuspensionUid IN ( :typeList ) and " +
					"d.dailyUid=os.dailyUid order by d.dayDate";
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,
							new String[] {"operationUid", "todayDate", "typeList"},
							new Object[] {operationUid, obj[0], list}, qp);			
						
					if (lstResult2.size()>0) {
						if (lstResult2.get(0) != null) {	
							cumCost = Double.parseDouble(lstResult2.get(0).toString());
						}
						//get operation suspension uid for updating 
						String operationSuspensionUid = obj[1].toString();
						
						//update the result value to selected operation suspension record 							
						String strSql3 = "FROM OperationSuspension WHERE (isDeleted = false or isDeleted is null) and operationSuspensionUid =:operationSuspensionUid";		
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "operationSuspensionUid", operationSuspensionUid, qp);
						
						if (lstResult3.size() > 0){								
							OperationSuspension operationSuspension = (OperationSuspension) lstResult3.get(0);
							operationSuspension.setCumCost(cumCost);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operationSuspension, qp);
						}
					}
				}
			}
				
		}
	
	}

}
			



