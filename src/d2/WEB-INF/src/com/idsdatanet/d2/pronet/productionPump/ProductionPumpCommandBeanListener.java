package com.idsdatanet.d2.pronet.productionPump;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.ProductionPumpParam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionPumpCommandBeanListener implements CommandBeanListener {

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}	
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
    	String wellUid="";
		Boolean enabled = false;	
		if(userSelection.getWellUid()!=null)
		{
			wellUid = userSelection.getWellUid();
		}
		String strSql = "FROM ProductionPumpParam WHERE (isDeleted = false or isDeleted is null) and wellUid =:wellUid ORDER BY installDatetime desc";
		List <ProductionPumpParam> lstResult = (List<ProductionPumpParam>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", wellUid);
		if (lstResult.size() > 0){
			ProductionPumpParam productionPump = lstResult.get(0);
			if(productionPump.getRemoveDatetime() != null){
				enabled = true;
			}else{
				enabled = false;
			}
		}else{
			enabled = true;
		}
		commandBean.getRoot().getDynaAttr().put("hideAddButton", enabled);
		
	}
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}


	
	
}
