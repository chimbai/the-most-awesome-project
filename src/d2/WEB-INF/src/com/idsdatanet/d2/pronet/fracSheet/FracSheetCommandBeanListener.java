package com.idsdatanet.d2.pronet.fracSheet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.FracSheet;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.pronet.perforation.PerforationUtils;

public class FracSheetCommandBeanListener implements CommandBeanListener {
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(FracSheet.class)){
				if("jobNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					FracSheet thisFracSheet = (FracSheet) targetCommandBeanTreeNode.getData();
					
					String perforationUid = PerforationUtils.getTopDepthPerforationUid(thisFracSheet.getJobNumber(), thisFracSheet.getWellboreUid(), thisFracSheet.getDailyUid());	
					thisFracSheet.setPerforationUid(perforationUid) ;
				}					
			}
		}		
	}
	
	
	
	public void init(CommandBean commandBean) throws Exception{
	}

	public void onCustomFilterInvoked(HttpServletRequest arg0,
			HttpServletResponse arg1, BaseCommandBean arg2, String arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
