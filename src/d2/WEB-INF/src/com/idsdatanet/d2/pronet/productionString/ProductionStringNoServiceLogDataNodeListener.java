package com.idsdatanet.d2.pronet.productionString;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringNoServiceLogDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor{
	
	private static final String MAPPING_SEPARATOR=".";
	private Map<String,String> autoUpdateFieldMappingList;
	private Boolean autoUpdateLatestLogToComponent = true;
	
	public void setAutoUpdateFieldMappingList(
			Map<String, String> autoUpdateFieldMappingList) {
		this.autoUpdateFieldMappingList = autoUpdateFieldMappingList;
	}

	public void setAutoUpdateLatestLogToComponent(
			Boolean autoUpdateLatestLogToComponent) {
		this.autoUpdateLatestLogToComponent = autoUpdateLatestLogToComponent;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(ProductionStringDetail.class)) return true;
		else return false;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		this.savebottomDepthMdMsl(node, thisConverter);
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		
		if (obj instanceof ProductionString){
			ProductionString productionString = (ProductionString) obj;
			if(daily != null)
			{
				productionString.setInstallDateTime(daily.getDayDate());
			}
		}
		
		this.savebottomDepthMdMsl(node, thisConverter);
		
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		Object obj = node.getData();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		qp.setDatumConversionEnabled(true);
		
		//after saving new component data, auto creating first log for it 
		if (obj instanceof ProductionStringDetail && (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed)) {			
			ProductionStringDetail psdetail = (ProductionStringDetail)obj;			
			
			//check if user has added a log for the current Production String Detail.
			if( !isFirstLogAvailable( psdetail.getProductionStringDetailUid() ) ){
			ProductionStringDetailLog psDetailLog = new ProductionStringDetailLog();
			
			if (this.autoUpdateFieldMappingList!=null) {
				for (Iterator iter = this.autoUpdateFieldMappingList.entrySet().iterator(); iter.hasNext();) 
				{
					Map.Entry entry = (Map.Entry) iter.next();
					String key = entry.getKey().toString();
					String fieldToUpdate = entry.getValue().toString().split("["+MAPPING_SEPARATOR+"]")[1];
	
					Object value = PropertyUtils.getProperty(psdetail,fieldToUpdate);
					if (value !=null){
						java.lang.Class type = PropertyUtils.getPropertyType(psdetail, key);
						if (type == java.lang.Double.class){
							thisConverter.setReferenceMappingField(ProductionStringDetail.class, key);
							thisConverter.setBaseValueFromUserValue((Double) value);
							value = thisConverter.getBasevalue();
							
							thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, key);
							if (thisConverter.isUOMMappingAvailable()) {
								thisConverter.setBaseValue((Double)value);
								//add datum offset if it is datum related field
								if (thisConverter.getUOMMapping().isDatumConversion()) {
									thisConverter.addDatumOffset();						
								}
								value = thisConverter.getConvertedValue();
							}					
						}
					}
					PropertyUtils.setProperty(psDetailLog, key, value);
				}
			}
			psDetailLog.setProductionStringDetailUid(psdetail.getProductionStringDetailUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(psDetailLog, qp);
			}
		}
		//update child top and bottom data if depthMdMsl value changed
		if (obj instanceof ProductionString) {
			if (!this.autoUpdateLatestLogToComponent) {
				if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoPopulateTopBottomWithSetDepth"))){
					ProductionString ps = (ProductionString)obj;	
					if (ps.getDepthMdMsl()!=null) {
						 Map<String, CommandBeanTreeNode> childNodes = node.getChild("ProductionStringDetail");
						 if(childNodes!=null){
							 List<CommandBeanTreeNode> allChildNodes =  new ArrayList(childNodes.values());
							 ProductionStringNoServiceLogUtils.calculateTopAndBottomDepth(allChildNodes, node, thisConverter);
						 }
					}
				}

			}
			
			ProductionString psdetail = (ProductionString)obj;	
			String strSql = "FROM ProductionString " +
			"WHERE (isDeleted=false or isDeleted is null) " +
			"AND wellboreUid=:wellboreUid " +
			"AND uninstallDateTime is null";
			List<ProductionString> ps = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", psdetail.getWellboreUid());
			if(ps!=null & ps.size() > 1)
			{
				commandBean.getSystemMessage().addInfo("More than 1 recond found without Pull Date. No production string will be shown in the Well Summary (Diagram) Report");
			}
		}
		
		//for production string detail 
		if (obj instanceof ProductionStringDetail) {
			Integer componentJoint = 0;
			Double componentTotalLength = 0.0;
			
			ProductionStringDetail detail = (ProductionStringDetail) node.getData();
			//set numOfJoint to 0 if it is null
			if (detail.getNumJoints() != null) componentJoint = detail.getNumJoints();
			
			//calculate total length 
			if (detail.getLength() != null && detail.getNumJoints() != null) { 	
				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "length");
				componentTotalLength = ProductionStringNoServiceLogUtils.calculateTotalLength(componentJoint, detail.getLength(),thisConverter);
				thisConverter.setReferenceMappingField(ProductionStringDetail.class, "totalLength");
				thisConverter.setBaseValue(componentTotalLength);
				detail.setTotalLength(thisConverter.getConvertedValue());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(detail);
			}
			else{
				detail.setTotalLength(componentTotalLength);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(detail);
			}
			
		}
		
		this.savebottomDepthMdMsl(node, thisConverter);
		
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		// check if start time greater than end time.
		if(obj instanceof ProductionString || obj instanceof ProductionStringDetail || obj instanceof ProductionStringDetailLog) {
			Date start = (Date) PropertyUtils.getProperty(obj, "installDateTime");
			Date end = (Date) PropertyUtils.getProperty(obj, "uninstallDateTime");
			
			// check if start time greater than end time.
			if(CommonUtil.getConfiguredInstance().isStartGreaterEndDate(start, end)){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "uninstallDateTime", "Start date cannot be greater than end time.");
				return;
				
			}
			
		}
		
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		this.savebottomDepthMdMsl(node, thisConverter);
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
		
		this.savebottomDepthMdMsl(parent, thisConverter);
		
	}
	
	private void savebottomDepthMdMsl(CommandBeanTreeNode node,  CustomFieldUom thisConverter) throws Exception {
		
		String productionStringUid = null;
		
		//Get Production String UID
		if (node.getData() instanceof ProductionString) {			
			ProductionString detail = (ProductionString) node.getData();						
			productionStringUid = detail.getProductionStringUid();	
		}
				
		if (node.getData() instanceof ProductionStringDetail) {			
			ProductionStringDetail detail = (ProductionStringDetail) node.getData();						
			productionStringUid = detail.getProductionStringUid();	
		}
		
		//Retrieve all production string detail records
		String[] paramNames = {"productionStringUid"};
		Object[] paramValues = {productionStringUid};		
		
		String strSql = "FROM ProductionStringDetail WHERE (isDeleted=false or isDeleted is null) and productionStringUid=:productionStringUid ORDER BY sequence";
		List<ProductionStringDetail> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames,paramValues);
		for (ProductionStringDetail productionStringDetail : list) {
			//Variables for the bottom depth calculation
			Double elevation_depth = 0.0;
			Double previous_bottom_depth = 0.0;
			Double bottom_depth = 0.0;
			Double top_depth = 0.0;
			
			String productionStringId = productionStringDetail.getProductionStringUid();
			Double seq = productionStringDetail.getSequence();			
			Double total_length = productionStringDetail.getTotalLength();
			if (total_length==null) total_length = 0.0;				 
			
			//Preset the install date time of the component (as this required for schematic to draw production string)
			String[] paramNames0 = {"productionStringUid"};
			Object[] paramValues0 = {productionStringId};
			
			String strSql0 = "FROM ProductionString ps WHERE (ps.isDeleted = false or ps.isDeleted is null) AND ps.productionStringUid =:productionStringUid";
			List<Object> lstResult0 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql0, paramNames0, paramValues0);
			
			if (lstResult0.size() > 0) {
				for (Object obj : lstResult0) {				
					ProductionString ps = (ProductionString) obj;
					
					if (ps.getInstallDateTime() !=null) {
						productionStringDetail.setInstallDateTime(ps.getInstallDateTime());
					}
				}
			}
			
			//retrieve required value for bottom depth calculation
			String[] paramNames1 = {"productionStringUid"};
			Object[] paramValues1 = {productionStringId};
			
			String strSql1 = "SELECT ps.elevation FROM ProductionString ps WHERE (ps.isDeleted = false or ps.isDeleted is null) AND ps.productionStringUid =:productionStringUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramNames1, paramValues1);
			
			if (lstResult.size()>0){
				if (lstResult.get(0)!=null) {
					elevation_depth = Double.parseDouble(lstResult.get(0).toString());
					if (elevation_depth==null) elevation_depth = 0.0;				 
					
				}
			}
			
			String[] paramNames2 = {"productionStringUid","sequence"};
			Object[] paramValues2 = {productionStringId,seq};
			
			String strSql2 = "SELECT psd.bottomDepthMdMsl FROM ProductionStringDetail psd WHERE (psd.isDeleted = false or psd.isDeleted is null) AND psd.productionStringUid =:productionStringUid AND psd.sequence < :sequence ORDER BY psd.sequence DESC";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2, QueryProperties.create().setFetchFirstRowOnly());
			
			if (lstResult2.size()>0){
				if (lstResult2.get(0)!=null) {
					previous_bottom_depth = Double.parseDouble(lstResult2.get(0).toString());
					if (previous_bottom_depth==null) previous_bottom_depth = 0.0;				 
					
				}
			}
			
			//thisConverter.setReferenceMappingField(ProductionStringDetail.class, "length");
			bottom_depth = ProductionStringNoServiceLogUtils.calculateDepth(previous_bottom_depth, elevation_depth, total_length);
			thisConverter.setReferenceMappingField(ProductionStringDetail.class, "bottomDepthMdMsl");
			thisConverter.setBaseValue(bottom_depth);
			productionStringDetail.setBottomDepthMdMsl(bottom_depth);
			
			//set Top Depth
			if (bottom_depth > 0) {
			top_depth = bottom_depth - total_length;
			}
			thisConverter.setReferenceMappingField(ProductionStringDetail.class, "depthMdMsl");
			thisConverter.setBaseValue(top_depth);
			productionStringDetail.setDepthMdMsl(top_depth);
			
			//save BottomDepthMdMsl & DepthMdMsl & Install Date Time
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(productionStringDetail);
		}			
	}
	
	private Boolean isFirstLogAvailable(String productionStringDetailUid) throws Exception {
		// TODO Auto-generated method stub
		String strSql = "From ProductionStringDetailLog Where productionStringDetailUid = :productionStringDetailUid And (isDeleted = false or isDeleted is null)";
		String[] paramNames = {"productionStringDetailUid"};
		Object[] paramValues = {productionStringDetailUid};
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames,paramValues);
		
		if( result.size() > 0 ){
			return true;
		}else{
			return false;
		}
		
	}
	
	
	
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}	
	
}