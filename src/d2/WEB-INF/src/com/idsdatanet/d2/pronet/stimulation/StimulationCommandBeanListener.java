package com.idsdatanet.d2.pronet.stimulation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.Perforation;
import com.idsdatanet.d2.core.model.Stimulation;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.pronet.perforation.PerforationUtils;

public class StimulationCommandBeanListener implements CommandBeanListener {
	public static final String MULTISELECT_ADD_NEW = "MultiSelectAddNew";
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	} 
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(Stimulation.class)){
				if("jobNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					Stimulation thisStimulation = (Stimulation) targetCommandBeanTreeNode.getData();
					
					UserSession session = UserSession.getInstance(request);
					
					Double depthBottomMdMsl = PerforationUtils.getMaxDepthBottomPerforation(thisStimulation.getJobNumber(), session.getCurrentWellboreUid(), session.getCurrentDailyUid());
					Double depthTopMdMsl = PerforationUtils.getMinDepthTopPerforation(thisStimulation.getJobNumber(),  session.getCurrentWellboreUid(), session.getCurrentDailyUid());				
					String perforationUid = PerforationUtils.getTopDepthPerforationUid(thisStimulation.getJobNumber(),  session.getCurrentWellboreUid(), session.getCurrentDailyUid());	
					
					if (depthTopMdMsl != null){
						thisConverter.setReferenceMappingField(Stimulation.class, "depthTopMdMsl");
						thisConverter.setBaseValue(depthTopMdMsl);
						depthTopMdMsl = thisConverter.getConvertedValue();
					}

					if (depthBottomMdMsl != null){
						thisConverter.setReferenceMappingField(Stimulation.class, "depthBottomMdMsl");
						thisConverter.setBaseValue(depthBottomMdMsl);
						depthBottomMdMsl = thisConverter.getConvertedValue();
					}
									
					thisStimulation.setDepthBottomMdMsl(depthBottomMdMsl);
					thisStimulation.setDepthTopMdMsl(depthTopMdMsl);
					thisStimulation.setPerforationUid(perforationUid) ;
					
					
				}
				
				
			}
		}
		
		
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (MULTISELECT_ADD_NEW.equals(invocationKey)) {
			String name = request.getParameter("name");
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("Lists");
			
			if("@perforationZone".equals(name)) {
				String[] newPerforationList = request.getParameterValues("list");
				UserSession session = UserSession.getInstance(request);
				String wellboreUid = session.getCurrentWellboreUid();
				if (newPerforationList!=null) {
					for (String perforationZone : newPerforationList) {
						// check duplicate
						String strSql = "FROM Perforation WHERE (isDeleted=false or isDeleted is null) AND wellboreUid=:wellboreUid AND perforationZone=:perforationZone";
						List<User> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"wellboreUid","perforationZone"}, new String[] {wellboreUid, perforationZone});
						if (rs.size()>0) {
							continue;
						}
						
						Perforation perforation = new Perforation();
						perforation.setPerforationZone(perforationZone);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforation);
						String key = perforation.getPerforationUid();
						
						SimpleAttributes atts = new SimpleAttributes();
						atts.addAttribute("key", key);
						atts.addAttribute("value", perforationZone);
						writer.addElement("item", "", atts);
					}
				}
			}
			writer.endAllElements();
			writer.close();
		}
		return;
	}
	
	public void init(CommandBean commandBean) throws Exception{
	}

}
