package com.idsdatanet.d2.pronet.wellboreProperties;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellborePropertiesDataNodeAllowedAction implements DataNodeAllowedAction {
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request) throws Exception {
		
			return false;
	}
}
