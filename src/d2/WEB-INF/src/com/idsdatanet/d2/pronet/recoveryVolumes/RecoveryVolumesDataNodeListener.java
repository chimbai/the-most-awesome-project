package com.idsdatanet.d2.pronet.recoveryVolumes;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.RecoveryVolumes;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RecoveryVolumesDataNodeListener  extends EmptyDataNodeListener {

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		if (node.getData() instanceof RecoveryVolumes) {
			RecoveryVolumes item = (RecoveryVolumes) node.getData();

			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RecoveryVolumes.class, "oilHauledInVolume");
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			
			List<RecoveryVolumes> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM RecoveryVolumes WHERE recoveryVolumesUid=:recoveryVolumesUid", "recoveryVolumesUid", item.getRecoveryVolumesUid(), queryProperties);
			if (result.size()>0) {
				RecoveryVolumes rawItem = result.get(0);

				Double totalDayOilRecovered = 0.0;
				Double totalDayH2oRecovered = 0.0;
				//calculate oil recovery daily total
				if (rawItem.getOilHauledInVolume()!=null) totalDayOilRecovered = rawItem.getOilHauledInVolume();
				if (rawItem.getOilHauledOutVolume()!=null) totalDayOilRecovered -= rawItem.getOilHauledOutVolume();
				if (rawItem.getOilNonRecoverableVolume()!=null) totalDayOilRecovered -= rawItem.getOilNonRecoverableVolume();
				thisConverter.setReferenceMappingField(RecoveryVolumes.class, "oilHauledInVolume");
				thisConverter.setBaseValue(totalDayOilRecovered);
				node.getDynaAttr().put("totalDayOilRecovered", thisConverter.getFormattedValue());

				//calculate h2o recovery daily total
				if (rawItem.getH2oHauledInVolume()!=null) totalDayH2oRecovered = rawItem.getH2oHauledInVolume();
				if (rawItem.getH2oHauledOutVolume()!=null) totalDayH2oRecovered -= rawItem.getH2oHauledOutVolume();
				if (rawItem.getH2oNonRecoverableInVolume()!=null) totalDayH2oRecovered -= rawItem.getH2oNonRecoverableInVolume();
				thisConverter.setReferenceMappingField(RecoveryVolumes.class, "h2oHauledInVolume");
				thisConverter.setBaseValue(totalDayH2oRecovered);
				node.getDynaAttr().put("totalDayH2oRecovered", thisConverter.getFormattedValue());
			}
			
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userSelection.getDailyUid());
			if (daily!=null) {
				Date dayDate = daily.getDayDate();
				if (dayDate!=null) {
					String operationUid = userSelection.getOperationUid();
					
					// calculate oil recovery cum total
					Double cumTotalOilRecovered = 0.0;
					Double totalOilHauledInVolume = this.calculateCummulativeTotal(operationUid, dayDate, "oilHauledInVolume", queryProperties);
					if (totalOilHauledInVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "oilHauledInVolume");
						thisConverter.setBaseValue(totalOilHauledInVolume);
						node.getDynaAttr().put("totalOilHauledInVolume", thisConverter.getFormattedValue());
						cumTotalOilRecovered = totalOilHauledInVolume;
					}
					
					Double totalOilHauledOutVolume = this.calculateCummulativeTotal(operationUid, dayDate, "oilHauledOutVolume", queryProperties);
					if (totalOilHauledOutVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "oilHauledOutVolume");
						thisConverter.setBaseValue(totalOilHauledOutVolume);
						node.getDynaAttr().put("totalOilHauledOutVolume", thisConverter.getFormattedValue());
						cumTotalOilRecovered -= totalOilHauledOutVolume;
					}
					
					Double totalOilNonRecoverableVolume = this.calculateCummulativeTotal(operationUid, dayDate, "oilNonRecoverableVolume", queryProperties);
					if (totalOilNonRecoverableVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "oilNonRecoverableVolume");
						thisConverter.setBaseValue(totalOilNonRecoverableVolume);
						node.getDynaAttr().put("totalOilNonRecoverableVolume", thisConverter.getFormattedValue());
						cumTotalOilRecovered -= totalOilNonRecoverableVolume;
					}

					thisConverter.setReferenceMappingField(RecoveryVolumes.class, "oilHauledInVolume");
					thisConverter.setBaseValue(cumTotalOilRecovered);
					node.getDynaAttr().put("cumTotalOilRecovered", thisConverter.getFormattedValue());

					
					// calculate oil recovery cum total
					Double cumTotalH2oRecovered = 0.0;
					Double totalH2oHauledInVolume = this.calculateCummulativeTotal(operationUid, dayDate, "h2oHauledInVolume", queryProperties);
					if (totalH2oHauledInVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "h2oHauledInVolume");
						thisConverter.setBaseValue(totalH2oHauledInVolume);
						node.getDynaAttr().put("totalH2oHauledInVolume", thisConverter.getFormattedValue());
						cumTotalH2oRecovered = totalH2oHauledInVolume;
					}
					
					Double totalH2oHauledOutVolume = this.calculateCummulativeTotal(operationUid, dayDate, "h2oHauledOutVolume", queryProperties);
					if (totalH2oHauledOutVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "h2oHauledOutVolume");
						thisConverter.setBaseValue(totalH2oHauledOutVolume);
						node.getDynaAttr().put("totalH2oHauledOutVolume", thisConverter.getFormattedValue());
						cumTotalH2oRecovered -= totalH2oHauledOutVolume;
					}
					
					Double totalH2oNonRecoverableInVolume = this.calculateCummulativeTotal(operationUid, dayDate, "h2oNonRecoverableInVolume", queryProperties);
					if (totalH2oNonRecoverableInVolume!=null) {
						thisConverter.setReferenceMappingField(RecoveryVolumes.class, "h2oNonRecoverableInVolume");
						thisConverter.setBaseValue(totalH2oNonRecoverableInVolume);
						node.getDynaAttr().put("totalH2oNonRecoverableInVolume", thisConverter.getFormattedValue());
						cumTotalH2oRecovered -= totalH2oNonRecoverableInVolume;
					}
					thisConverter.setReferenceMappingField(RecoveryVolumes.class, "h2oHauledInVolume");
					thisConverter.setBaseValue(cumTotalH2oRecovered);
					node.getDynaAttr().put("cumTotalH2oRecovered", thisConverter.getFormattedValue());
					
					
					//calculate gas volumes cum total
					String value = this.calculateCummulativeTotalWithFormattedValue(commandBean, operationUid, dayDate, "gasFlaredVolume", queryProperties);
					if (value!=null) node.getDynaAttr().put("totalGasFlaredVolume", value);
					value = this.calculateCummulativeTotalWithFormattedValue(commandBean, operationUid, dayDate, "gasVentedVolume", queryProperties);
					if (value!=null) node.getDynaAttr().put("totalGasVentedVolume", value);
					value = this.calculateCummulativeTotalWithFormattedValue(commandBean, operationUid, dayDate, "gasInlineVolume", queryProperties);
					if (value!=null) node.getDynaAttr().put("totalGasInlineVolume", value);
					value = this.calculateCummulativeTotalWithFormattedValue(commandBean, operationUid, dayDate, "gasFlaredVentedDuration", queryProperties);
					if (value!=null) node.getDynaAttr().put("totalGasFlaredVentedDuration", value);
				}				
			}
		}
		
	}
	
	private String calculateCummulativeTotalWithFormattedValue(CommandBean commandBean, String operationUid, Date dayDate, String fieldName, QueryProperties queryProperties) throws Exception {
		Double value = this.calculateCummulativeTotal(operationUid, dayDate, fieldName, queryProperties);
		if (value!=null) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RecoveryVolumes.class, fieldName);
			thisConverter.setBaseValue(value);
			return thisConverter.getFormattedValue();
		}
		return null;
	}
	
	private Double calculateCummulativeTotal(String operationUid, Date dayDate, String fieldName, QueryProperties queryProperties) throws Exception {
		String queryString = "SELECT sum(rv." + fieldName +") FROM RecoveryVolumes rv, Daily d " +
				"WHERE (rv.isDeleted=false or rv.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND (d.dailyUid=rv.dailyUid) " +
				"AND rv." + fieldName + " is not null " +
				"AND d.dayDate<=:dayDate " +
				"AND rv.operationUid=:operationUid ";
		List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
				new String[] {"dayDate", "operationUid"}, 
				new Object[] {dayDate, operationUid},
				queryProperties);
		if (result.size()>0) {
			if (result.get(0) != null) {
				return result.get(0);
			}
		}
		return null;
	}
	
}
