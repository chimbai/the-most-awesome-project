package com.idsdatanet.d2.pronet.wellHead;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.WellHeadDetailLog;
import com.idsdatanet.d2.core.model.WellHead;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellHeadListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	String uid = null;
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if(meta.getTableClass().equals(WellHead.class)) {
			String selectedWellhead  = (String)parentNode.getDynaAttr().get("wellHeadUid");
			String thisFilter = "";
	
			query.addParam("selectedWellhead", selectedWellhead);
			query.addParam("thisWellboreUid", userSelection.getWellboreUid());				
			thisFilter ="wellHeadUid =:selectedWellhead AND wellboreUid= :thisWellboreUid";
				
			return conditionClause.replace("{_custom_condition_}", thisFilter);
			
		} else if(meta.getTableClass().equals(WellHeadDetailLog.class)) {
	
			if (request.getParameter("uid") !=null){
				uid= request.getParameter("uid");
			}else {
				uid= request.getParameter("wellHeadDetailUid");
			}
			String thisFilter = "";
			query.addParam("detailUid", uid);
			thisFilter = "(wellHeadDetailUid = :detailUid)";
			return conditionClause.replace("{_custom_condition_}", thisFilter);
			
			
		}else {
			return null;
		}
		
		
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}
