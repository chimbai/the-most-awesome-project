package com.idsdatanet.d2.pronet.wellHead;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.WellHead;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellHeadLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		
		param_names.add("wellboreUid"); 
		param_values.add(userSelection.getWellboreUid());
		
		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from WellHead where wellboreUid = :wellboreUid order by installDate", param_names, param_values); //.getObjects(CostAccountCodes.class);
		for (Object obj : list) {
			WellHead thisWellHead = (WellHead) obj;
			String key = thisWellHead.getWellHeadUid();
		
			String value = null;
			if (thisWellHead.getUninstallDate() != null){
				 value = "Ref # " + thisWellHead.getReferenceNo()+ " (" + thisWellHead.getInstallDate() + " - " + thisWellHead.getUninstallDate() + ")";
			}else {
				 value = "Ref # " + thisWellHead.getReferenceNo() + " (" + thisWellHead.getInstallDate() + " - current)";
			}
			
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
}
