package com.idsdatanet.d2.pronet.cavitation;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.SampleDescription;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * All common utility related to Cavitation.
 * @author ekhoo
 *
 */
public class CavitationUtils {
	/**
	 * Common Method for calculate thickness between bottom depth and top depth 
	 * @param topDepth, bottomDepth
	 * @throws Exception Standard Error Throwing Exception
	 * @return thickness
	 */
	public static Double calculateThickness(Double topDepth, Double bottomDepth) throws Exception{	
		Double thickness = 0.0;
		if (topDepth !=null && bottomDepth !=null){
			thickness = bottomDepth - topDepth; 
			return thickness;
		}
		
		return null;
	}
	
	/**
	 * Common Method for calculate total daily volume per day
	 * @param operationUid, currentDaily
	 * @throws Exception Standard Error Throwing Exception
	 * @return dailyTotal
	 */
	public static Double calculateTotalDailyVolume(String operationUid, Daily currentDaily, QueryProperties qp) throws Exception {
		Double dailyTotal = null;
		if (currentDaily==null || operationUid==null) return dailyTotal;
		
		String queryString = "select sum(dailyVolume) FROM SampleDescription WHERE (isDeleted=false or isDeleted is null) and " +
							"dailyUid=:dailyUid";
				
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", currentDaily.getDailyUid(),qp);
		if (lstResult.size()>0) {
			if (lstResult.get(0) != null) {	
				dailyTotal = Double.parseDouble(lstResult.get(0).toString());
			}
		}
		
		return dailyTotal;
	}
	
	/**
	 * Common Method for calculate total cumulative volume to date for all coal type
	 * @param operationUid, currentDaily
	 * @throws Exception Standard Error Throwing Exception
	 * @return cumTotal
	 */
	public static Double calculateCummulativeTotalVolume(String operationUid, Daily currentDaily, QueryProperties qp) throws Exception {
		Double cumTotal = null;
		if (currentDaily==null || operationUid==null) return cumTotal;
		
		String queryString = "select sum(sd.dailyVolume) FROM SampleDescription sd, Daily d WHERE (sd.isDeleted=false or sd.isDeleted is null) and " +
							"(d.isDeleted=false or d.isDeleted is null) and sd.dailyUid = d.dailyUid " +
							"and d.dayDate <= :todayDate and sd.operationUid =:operationUid";
				
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
				new String[] {"todayDate", "operationUid"},
				new Object[] {currentDaily.getDayDate(), operationUid}, qp);
		if (lstResult.size()>0) {
			if (lstResult.get(0) != null) {	
				cumTotal = Double.parseDouble(lstResult.get(0).toString());
			}
		}
		
		return cumTotal;
	}
	
	
	/**
	 * Common Method for re-calculate and update cum. total volume for each type after saving data
	 * @param operationUid, type
	 * @throws Exception Standard Error Throwing Exception
	 * @return 
	 */
	public static void calcCumulativeVolumeByType(String operationUid, String type) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double cumVolume = null;
		
		//query1 : to query a list of cavitation uid which have same coal type as current edited record. 
		String sql = "select sampleDescriptionUid from SampleDescription where sampleType=:type and operationUid=:operationUid and (isDeleted = false or isDeleted is null)";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"operationUid", "type"}, new Object[] {operationUid, type});
		
		if (list.size() > 0){
			//query2 : get a list of date (based on dailyuid) which have same coal type based on the list of uid from query1
			String strSql ="select d.dayDate, sd.sampleDescriptionUid From SampleDescription sd, Daily d where " +
				"(sd.isDeleted=false or sd.isDeleted is null) and " +
				"sd.sampleDescriptionUid IN ( :typeList ) and sd.dailyUid = d.dailyUid and " +
				"sd.operationUid=:operationUid order by d.dayDate";
			
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
					new String[] {"typeList", "operationUid"}, new Object[] {list, operationUid});
			
			if (lstResult.size() > 0){
				
				//loop all the data with same coal type - to recalculate and update all related cavitation records for cum. total volume 
				for (Object[] obj: lstResult){
					
					//sum the daily volume value where the date is equal or before current record date from query2 
					String strSql2 = "select sum(sd.dailyVolume) from SampleDescription sd, Daily d where (sd.isDeleted = false or sd.isDeleted is null) " +
						"and (d.isDeleted = false or d.isDeleted is null) and sd.operationUid=:operationUid and " +
						"d.dayDate <=:currentRercordDate and sd.sampleDescriptionUid IN ( :typeList ) and " +
						"d.dailyUid=sd.dailyUid order by d.dayDate";
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,
							new String[] {"operationUid", "currentRercordDate", "typeList"},
							new Object[] {operationUid, obj[0], list}, qp);			
					
					if (lstResult2.size()>0) {
						if (lstResult2.get(0) != null) {	
							cumVolume = Double.parseDouble(lstResult2.get(0).toString());
						}
							
						//get cavitation uid for updating 
						String sampleDescriptionUid = obj[1].toString();
						
						//update the result value to selected cavitation record 							
						String strSql3 = "FROM SampleDescription WHERE (isDeleted = false or isDeleted is null) and sampleDescriptionUid =:sampleDescriptionUid";		
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "sampleDescriptionUid", sampleDescriptionUid, qp);
						
						if (lstResult3.size() > 0){								
							SampleDescription cavitation = (SampleDescription) lstResult3.get(0);
							cavitation.setTotalVolume(cumVolume);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cavitation, qp);
						}
						
					}
				}
			}
				
		}

	}
	
	/**
	 * Get if it is first day in operation for cavitation
	 * @param String operationUid, String currentDailyUid
	 * @return String
	 * @throws Exception
	 */
	public static String isCavitationFirstDay (String operationUid, String currentDailyUid) throws Exception {
				
		String strSql = "SELECT d.dailyUid FROM SampleDescription sd, Daily d WHERE (sd.isDeleted = false or sd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
				"and sd.dailyUid = d.dailyUid and sd.operationUid = :thisOperationUid ORDER BY d.dayDate ASC";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisOperationUid", operationUid);
		
		Daily firstDay = null; 
		
		if (lstResult.size() > 0){
			String dailyUid = lstResult.get(0).toString();	
			firstDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);						
		}
		
		if (firstDay != null){
			 if(firstDay.getDailyUid().compareTo(currentDailyUid) == 0) return "1";
			 else return "0";
		}						
		else return "1";
	}
	
	/**
	 * method to populate depth based on first day cavitation
	 * @param Object object, String operationUid, String sampleType
	 * @return String
	 * @throws Exception
	 */
	
	public static void populateFromFirstdayDepth(Object object, String operationUid, String sampleType) throws Exception {
		
		String strSql = "SELECT sd.topDepthMdMsl, sd.topDepthTvdMsl, sd.bottomDepthMdMsl, sd.bottomDepthTvdMsl, sd.thicknessMd, sd.thicknessTvd FROM SampleDescription sd, Daily d WHERE (sd.isDeleted = false or sd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
		"and sd.dailyUid = d.dailyUid and sd.operationUid = :thisOperationUid and sd.sampleType=:sampleType ORDER BY d.dayDate ASC";

		List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"thisOperationUid", "sampleType"}, new Object[]{operationUid, sampleType});
		
		if (lstResult.size() > 0){
			Object[] coalDepth = (Object[]) lstResult.get(0);
			PropertyUtils.setProperty(object, "topDepthMdMsl", coalDepth[0]);
			PropertyUtils.setProperty(object, "topDepthTvdMsl",  coalDepth[1]);
			PropertyUtils.setProperty(object, "bottomDepthMdMsl",  coalDepth[2]);
			PropertyUtils.setProperty(object, "bottomDepthTvdMsl",  coalDepth[3]);
			PropertyUtils.setProperty(object, "thicknessMd",  coalDepth[4]);
			PropertyUtils.setProperty(object, "thicknessTvd",  coalDepth[5]);
		}
	}

	
	
	
}
