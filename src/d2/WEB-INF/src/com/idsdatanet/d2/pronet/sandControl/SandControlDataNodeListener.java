package com.idsdatanet.d2.pronet.sandControl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.SandControlComponent;
import com.idsdatanet.d2.core.model.SandControlInfo;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SandControlDataNodeListener extends EmptyDataNodeListener{

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean, 
			TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, 
			UserSelectionSnapshot userSelection, 
			HttpServletRequest request) throws Exception {
		
		if("sandControlAltController".equals(commandBean.getControllerBeanName())) {
			if(node.getData() instanceof SandControlInfo) {
				autoPopulateWellFieldAndWellName(node, userSelection);
			}
		}
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean, 
			TreeModelDataDefinitionMeta meta, 
			CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, 
			HttpServletRequest request) throws Exception {
		
		if("sandControlAltController".equals(commandBean.getControllerBeanName())) {
			if(node.getData() instanceof SandControlInfo) {
				autoPopulateWellFieldAndWellName(node, userSelection);
			}
		}
	}
	
	private void autoPopulateWellFieldAndWellName(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		
		String sql = "SELECT wellName, field FROM Well WHERE wellUid=:wellUid and (isDeleted IS NULL OR isDeleted = false)";
		
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "wellUid", userSelection.getWellUid());
		
		if (result.size() > 0) {
			Object[] wellObj = result.get(0);
				
			// wellName
			if (wellObj[0] != null) {
				if(!(wellObj[0].toString().isEmpty())) {
					node.getDynaAttr().put("wellName", wellObj[0].toString());
				}
			}
				
			// field
			if (wellObj[1] != null) {
				if(!(wellObj[1].toString().isEmpty())) {
					node.getDynaAttr().put("field", wellObj[1].toString());
				}
			}
		}
	}
}