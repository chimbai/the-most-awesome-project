package com.idsdatanet.d2.pronet.productionPump;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.ProductionPumpParam;
import com.idsdatanet.d2.core.model.ProductionPumpParamComp;
import com.idsdatanet.d2.core.model.ProductionPumpParamCompLog;
import com.idsdatanet.d2.core.model.ProductionPumpParamCompServLog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionPumpDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof ProductionPumpParam)
		{
			ProductionPumpParam thisProductionPump = (ProductionPumpParam) object;			
			if(thisProductionPump.getRemoveDatetime() == null){
				commandBean.getSystemMessage().addInfo("Kindly enter Remove Date/Time for the previous pump in order to add new pump record.");
				return;
			}
		}
		if(object instanceof ProductionPumpParamCompLog)
		{
			ProductionPumpParamCompLog thisProductionPumpParamCompLog = (ProductionPumpParamCompLog) object;
						
			if(thisProductionPumpParamCompLog.getRemoveDatetime() == null){
				commandBean.getSystemMessage().addInfo("Kindly enter Remove Date for the previous installation in order to add new record.");
				return;
			}
		}
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {

	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
	}
	

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof ProductionPumpParam)
		{
			//Validation Code for DateIn > DateOut
			ProductionPumpParam thisProductionPump = (ProductionPumpParam) object;
						
			if(thisProductionPump.getInstallDatetime() != null && thisProductionPump.getRemoveDatetime() != null){
				if(thisProductionPump.getInstallDatetime().getTime() >= thisProductionPump.getRemoveDatetime().getTime()){
					status.setContinueProcess(false, true);
					commandBean.getSystemMessage().addError("Install date time cannot be greater than or same as end date time.");
					return;
				}
			}
		}
		if(object instanceof ProductionPumpParamComp)
		{
			ProductionPumpParam parentProductionPump = (ProductionPumpParam) node.getParent().getData(); 
			ProductionPumpParamComp thisProductionPumpParamComp = (ProductionPumpParamComp) object;
			if ("PSN".equals(thisProductionPumpParamComp.getComponentType())){
				String strSql = "FROM ProductionPumpParamComp WHERE (isDeleted = false or isDeleted is null) and "
						+ "productionPumpParamUid =:productionPumpParamUid and"
						+ (thisProductionPumpParamComp.getProductionPumpParamCompUid()!=null ? 
								 " productionPumpParamCompUid != '" + thisProductionPumpParamComp.getProductionPumpParamCompUid() + "' "
							:  " productionPumpParamCompUid is not null ")
						+ "and componentType='PSN'";
				String[] paramsFields = {"productionPumpParamUid"};
				Object[] paramsValues = {parentProductionPump.getProductionPumpParamUid(), };
				List <ProductionPumpParamComp> lstResult = (List<ProductionPumpParamComp>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (lstResult.size()>0){
					status.setContinueProcess(false, true);
					commandBean.getSystemMessage().addError("Only one Pump Seat Nipple is allowed per pump.");
					return;
				}
			}
		}
		
		if(object instanceof ProductionPumpParamCompLog)
		{
			ProductionPumpParamCompLog thisProductionPumpParamCompLog = (ProductionPumpParamCompLog) object;
			
			if(thisProductionPumpParamCompLog.getInstallDatetime() != null && thisProductionPumpParamCompLog.getRemoveDatetime() != null){
				if(thisProductionPumpParamCompLog.getInstallDatetime().getTime() > thisProductionPumpParamCompLog.getRemoveDatetime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "removeDatetime", "Remove Date cannot be earlier than Install Date.");
					return;
				}
			}
		}
		if(object instanceof ProductionPumpParamCompServLog)
		{
			ProductionPumpParamCompServLog thisProductionPumpParamCompServLog = (ProductionPumpParamCompServLog) object;
			
			if(thisProductionPumpParamCompServLog.getServiceDatetime() != null && thisProductionPumpParamCompServLog.getNextServiceDatetime() != null){
				if(thisProductionPumpParamCompServLog.getServiceDatetime().getTime() > thisProductionPumpParamCompServLog.getNextServiceDatetime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "nextServiceDatetime", "Next Service Date/Time cannot be earlier than Service Date/Time.");
					return;
				}
			}
		}
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> loadChildNodes(CommandBean arg0, TreeModelDataDefinitionMeta arg1, CommandBeanTreeNode arg2,
			UserSelectionSnapshot arg3, Pagination arg4, HttpServletRequest arg5) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}