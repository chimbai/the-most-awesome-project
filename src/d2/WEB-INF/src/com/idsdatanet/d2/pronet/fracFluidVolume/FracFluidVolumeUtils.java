package com.idsdatanet.d2.pronet.fracFluidVolume;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FracFluidVolumeUtils {

	public static Double getFluidVolumeCumulative(UserSelectionSnapshot userSelection, String operationType, String fieldName) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userSelection.getDailyUid());
			if (daily!=null) {
				String queryString = "SELECT sum(coalesce(f." + fieldName + ",0.0)) from FracFluidVolume f, Daily d " +
						"WHERE (f.isDeleted=false or f.isDeleted is NULL) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND d.dailyUid=f.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"AND d.dayDate<=:dayDate " +
						"AND f.operationType=:operationType ";
				List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[]{"operationUid","dayDate","operationType"}, 
						new Object[]{userSelection.getOperationUid(), daily.getDayDate(), operationType}, qp);
				if (list.size()>0) {
					if (list.get(0)!=null) {
						return Double.parseDouble(list.get(0).toString());
					}
				}
			}
		}
		
		return 0.0;
	}
}
