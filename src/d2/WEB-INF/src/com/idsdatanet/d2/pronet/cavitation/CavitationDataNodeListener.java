package com.idsdatanet.d2.pronet.cavitation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.SampleDescription;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CavitationDataNodeListener extends EmptyDataNodeListener {
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof SampleDescription){
			SampleDescription cavitation = (SampleDescription) obj;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			if (cavitation !=null){
				
				if (cavitation.getTopDepthMdMsl()!=null && cavitation.getBottomDepthMdMsl()!=null){
					//get base value for topMdMsl 
					thisConverter.setReferenceMappingField(SampleDescription.class, "topDepthMdMsl");
					thisConverter.setBaseValueFromUserValue(cavitation.getTopDepthMdMsl());
					Double topMdMsl = thisConverter.getBasevalue();
					
					//get base value for bottomDepthMdMsl 
					thisConverter.setReferenceMappingField(SampleDescription.class, "bottomDepthMdMsl");
					thisConverter.setBaseValueFromUserValue(cavitation.getBottomDepthMdMsl());
					Double bottomMdMsl = thisConverter.getBasevalue();
					
					//calculate thickness MD (bottom - top)
					Double thicknessMd = CavitationUtils.calculateThickness(topMdMsl, bottomMdMsl);
					
					//save thickness md after calculating
					thisConverter.setReferenceMappingField(SampleDescription.class, "thicknessMd");
					thisConverter.setBaseValue(thicknessMd);
					cavitation.setThicknessMd(thisConverter.getConvertedValue());
				}
				
				if(cavitation.getTopDepthTvdMsl()!=null && cavitation.getBottomDepthTvdMsl()!=null){
					
					//get base value for topDepthTvdMsl
					thisConverter.setReferenceMappingField(SampleDescription.class, "topDepthTvdMsl");
					thisConverter.setBaseValueFromUserValue(cavitation.getTopDepthTvdMsl());
					Double topTvdMsl = thisConverter.getBasevalue();
					
					//get base value for bottomDepthTvdMsl
					thisConverter.setReferenceMappingField(SampleDescription.class, "bottomDepthTvdMsl");
					thisConverter.setBaseValueFromUserValue(cavitation.getBottomDepthTvdMsl());
					Double bottomTvdMsl = thisConverter.getBasevalue();
					
					//calculate thickness TVD (bottom - top)
					Double thicknessTvd = CavitationUtils.calculateThickness(topTvdMsl, bottomTvdMsl);
					
					//save thickness Tvd after calculating
					thisConverter.setReferenceMappingField(SampleDescription.class, "thicknessTvd");
					thisConverter.setBaseValue(thicknessTvd);
					cavitation.setThicknessTvd(thisConverter.getConvertedValue());
				}
				
	
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		this.updateCumVolumeByType(node);
		
		Object obj = node.getData();
		if(obj instanceof SampleDescription){
			SampleDescription cavitation = (SampleDescription) obj;
			String isFirstDay = CavitationUtils.isCavitationFirstDay(session.getCurrentOperationUid(), session.getCurrentDailyUid());	
			if ("1".equals(isFirstDay)){
				
				String firstDayUid = cavitation.getDailyUid();
				
				if (StringUtils.isNotBlank(firstDayUid)){
					//if not first day, update depths value changed on first day to all subsequent days
					String strSql = "FROM SampleDescription where (isDeleted = false or isDeleted is null) " +
						"and dailyUid !=:firstDailyUid and operationUid = :thisOperationUid and sampleType=:sampleType";
					List <SampleDescription> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"thisOperationUid", "sampleType", "firstDailyUid"}, new Object[]{session.getCurrentOperationUid(), cavitation.getSampleType(), firstDayUid});
					
					for(SampleDescription sd: lstResult){
						CavitationUtils.populateFromFirstdayDepth(sd, sd.getOperationUid(), sd.getSampleType());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sd);
					}
					
					
				}
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		this.updateCumVolumeByType(node); 
	}
	
	/**
	 * method to update cum. volume by coal type
	 * @param node
	 * @throws Exception
	 */
	public void updateCumVolumeByType(CommandBeanTreeNode node) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof SampleDescription){
			SampleDescription cavitation = (SampleDescription) obj;
			//re-calculate the cum volume by coal 
			CavitationUtils.calcCumulativeVolumeByType(cavitation.getOperationUid(), cavitation.getSampleType());
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof SampleDescription){
			String isFirstDay = CavitationUtils.isCavitationFirstDay(userSelection.getOperationUid(), userSelection.getDailyUid());	
			node.getDynaAttr().put("isFirstDay", isFirstDay);
		}
	}
	
}
