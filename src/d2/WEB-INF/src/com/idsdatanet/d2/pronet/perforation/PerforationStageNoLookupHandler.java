package com.idsdatanet.d2.pronet.perforation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PerforationStageNoLookupHandler implements LookupHandler {	
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();	
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		
		param_names.add("wellboreUid"); 
		param_values.add(userSelection.getWellboreUid());
		
		param_names.add("dailyUid"); 
		param_values.add(userSelection.getDailyUid());
		
		if(userSelection.getDailyUid()!= null && userSelection.getWellboreUid() != null){
			
			String sql = "select DISTINCT jobNumber from Perforation where (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid and dailyUid=:dailyUid order by depthTopMdMsl";
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, param_names, param_values);	
			if (lstResult.size() > 0){
				for(Object objResult : lstResult){
					Object obj = (Object) objResult;
					if (obj !=null){
						result.put((String) obj.toString(), new LookupItem((String) obj.toString(), (String) obj.toString()));
					}
				
				}
			}	
		}
			

		return result;
	}
}

