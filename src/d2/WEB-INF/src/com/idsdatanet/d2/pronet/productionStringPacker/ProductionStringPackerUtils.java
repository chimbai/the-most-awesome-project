package com.idsdatanet.d2.pronet.productionStringPacker;

import com.idsdatanet.d2.core.model.ProductionStringPacker;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;

/**
 * All common utility related to Production String Packer.
 * @author Jackson
 *
 */
public class ProductionStringPackerUtils {

	/**
	 * Calculate Production String Packer length
	 * @param commandBean 
	 * @param object
	 * @throws Exception
	 */
	public static void calculatePackerLength(CommandBean commandBean, Object object) throws Exception{
		
		if (object instanceof ProductionStringPacker) {
			
			ProductionStringPacker stringPacker = (ProductionStringPacker) object;
			
			Double bottomDepth = null;
			Double topDepth = null;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
						
			if (stringPacker.getBottomDepthMdMsl()!=null && stringPacker.getTopDepthMdMsl()!=null){
				thisConverter.setReferenceMappingField(ProductionStringPacker.class, "bottomDepthMdMsl");
				thisConverter.setBaseValueFromUserValue(stringPacker.getBottomDepthMdMsl());
				bottomDepth = thisConverter.getBasevalue();
				
				thisConverter.setReferenceMappingField(ProductionStringPacker.class, "topDepthMdMsl");
				thisConverter.setBaseValueFromUserValue(stringPacker.getTopDepthMdMsl());
				topDepth = thisConverter.getBasevalue();
				
				thisConverter.setReferenceMappingField(ProductionStringPacker.class, "packerLength");
				Double packerLength = bottomDepth - topDepth;
				thisConverter.setBaseValue(packerLength);
				stringPacker.setPackerLength(thisConverter.getConvertedValue());
			} else {
				stringPacker.setPackerLength(null);
			}
			
		}

	}
}
