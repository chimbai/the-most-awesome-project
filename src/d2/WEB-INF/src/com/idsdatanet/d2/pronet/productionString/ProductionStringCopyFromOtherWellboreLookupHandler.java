package com.idsdatanet.d2.pronet.productionString;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.well.HierarchicalAccessibleWellboreLookupHandler;

public class ProductionStringCopyFromOtherWellboreLookupHandler extends HierarchicalAccessibleWellboreLookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		//get accessible wellbore list
		Map<String, LookupItem> wellboreList = super.getLookup(commandBean, node, userSelection, request, lookupCache);
			
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();

		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from ProductionString where (isDeleted = false or isDeleted is null) order by installDateTime DESC");
		
		if (list.size() > 0) {
			for (Object obj : list) {				
				ProductionString ps = (ProductionString) obj;
				String key = ps.getProductionStringUid();
				
				LookupItem lookupItem = wellboreList.get(ps.getWellboreUid());
				
				if (lookupItem !=null) {
					String installDate = "";
					String uninstallDate = "Current";
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
					
					if (ps.getInstallDateTime() !=null) 
						installDate = sdf.format(ps.getInstallDateTime());
				
					if (ps.getUninstallDateTime() != null)
						uninstallDate = sdf.format(ps.getUninstallDateTime());
			
					String value = lookupItem.getValue().toString() + " - " + installDate + " to " + uninstallDate;
					
					LookupItem item = new LookupItem(key, value);
					result.put(key, item);
				}
			}
		}
		return result;
	}
	
}
