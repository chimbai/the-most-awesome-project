package com.idsdatanet.d2.pronet.wellHead;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.WellHead;
import com.idsdatanet.d2.core.model.WellHeadDetail;
import com.idsdatanet.d2.core.model.WellHeadDetailLog;
import com.idsdatanet.d2.core.model.WellHeadSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class WellHeadComponentDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	private static final String MAPPING_SEPARATOR=".";
	private Map<String,String> autoUpdateFieldMappingList;

	public void setAutoUpdateFieldMappingList(
			Map<String, String> autoUpdateFieldMappingList) {
		this.autoUpdateFieldMappingList = autoUpdateFieldMappingList;
	}
	
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(WellHeadDetail.class)) return true;
		else return false;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(WellHeadDetail.class)) {
			
			String wellheadSectionUid = null;
			Object object = node.getData();
			
			if (object instanceof WellHeadSection) {
				WellHeadSection wellheadSection = (WellHeadSection) object;
				wellheadSectionUid = wellheadSection.getWellHeadSectionUid();
				
			}
						
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) {
				//get previous operation end date
				String lastDailyUid = CommonUtil.getConfiguredInstance().getPreviousOperationEndDate(userSelection.getWellboreUid());
				if (lastDailyUid!=null){
					daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(lastDailyUid);
					if (daily == null)
						return null;
				}
				else
					return null;
			}
				
			String viewWellheadDetailMode = "";
			if (request != null) {
				viewWellheadDetailMode = request.getParameter("viewWellheadDetailMode");
			}
			String strSql = "";
	
			List items = null;
			if ("show_all".equalsIgnoreCase(viewWellheadDetailMode)){
				strSql = "select wd.wellHeadDetailUid, wd.sequence from WellHead wh, WellHeadDetail wd, WellHeadSection ws where (wh.isDeleted = false or wh.isDeleted is null) and (wd.isDeleted = false or wd.isDeleted is null) and (ws.isDeleted = false or ws.isDeleted is null) and " +
					"wh.wellHeadUid = ws.wellHeadUid and ws.wellHeadSectionUid = wd.wellHeadSectionUid and wh.wellHeadUid = wd.wellHeadUid and wh.wellboreUid =:wellboreUid and wd.wellHeadSectionUid =:wellheadSectionUid order by ws.sectionCode,wd.sequence";
				
				String[] paramNames1 = {"wellheadSectionUid", "wellboreUid"};
				Object[] paramValues1 = {wellheadSectionUid, userSelection.getWellboreUid()};
				items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames1, paramValues1);		
			
			}else {
				strSql = "select wd.wellHeadDetailUid, wd.sequence from WellHead wh, WellHeadDetail wd, WellHeadSection ws where (wh.isDeleted = false or wh.isDeleted is null) and (wd.isDeleted = false or wd.isDeleted is null) and (ws.isDeleted = false or ws.isDeleted is null) and " +
					"wh.wellHeadUid = ws.wellHeadUid and ws.wellHeadSectionUid = wd.wellHeadSectionUid and wh.wellHeadUid = wd.wellHeadUid and ((wd.installDateTime <=:currentDate and wd.uninstallDateTime >=:currentDate) or (wd.installDateTime <=:currentDate and wd.uninstallDateTime is null) or (wd.installDateTime is null and wd.uninstallDateTime is null)) " +
					"and wh.wellboreUid =:wellboreUid and wd.wellHeadSectionUid =:wellheadSectionUid order by ws.sectionCode,wd.sequence";
			
				String[] paramNames2 = {"currentDate", "wellboreUid", "wellheadSectionUid"};
				Object[] paramValues2 = {daily.getDayDate(), userSelection.getWellboreUid(), wellheadSectionUid};
				items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames2,paramValues2);		
				
			}			
				
			List<Object> output_maps = new ArrayList<Object>();
		
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				WellHeadDetail wellheadDetail = (WellHeadDetail) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellHeadDetail.class, (String) obj_array[0].toString());
				output_maps.add(wellheadDetail);
			}
			return output_maps;			
		}
		
		return null;
	}

	//DataNodeListener
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object obj = node.getData();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if(daily == null) return;
		
		// for checking if the component is install or uninstalled
		if (obj instanceof WellHeadDetail){
			WellHeadDetail whdetail = (WellHeadDetail)obj;
			Date currentDate = daily.getDayDate();
			
			String futureInstall = "";			
			if (whdetail.getInstallDateTime() != null) {
				if (whdetail.getInstallDateTime().after(currentDate)) {
					futureInstall = "1";
				}
			}
			
			String uninstalled = "";
			if (whdetail.getUninstallDateTime() != null){
				if (whdetail.getUninstallDateTime().before(currentDate)) {
					uninstalled ="1";
				}
			}
			node.getDynaAttr().put("uninstalled", uninstalled);
			node.getDynaAttr().put("futureInstall", futureInstall);
		}
		
	}
		
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		// check if start time greater than end time.
		if(object instanceof WellHeadDetail) {
			Date start = (Date) PropertyUtils.getProperty(object, "installDateTime");
			Date end = (Date) PropertyUtils.getProperty(object, "uninstallDateTime");
			
			// check if start time greater than end time.
			if(CommonUtil.getConfiguredInstance().isStartGreaterEndDate(start, end)){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "uninstallDateTime", "Start date cannot be greater than end time.");
				return;
				
			}
			
		}

	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		Object obj = node.getData();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		qp.setDatumConversionEnabled(true);
		
		if(obj instanceof WellHeadDetail) {
			//stamp wellheadUid to wellHeadDetailSection
			WellHeadDetail thisWellHeadDetail = (WellHeadDetail) obj;	
			String connectionSize = thisWellHeadDetail.getTopConnectionSizePressure();
			String wellHeadDetailUid = thisWellHeadDetail.getWellHeadDetailUid();
			String lookupTypeSelection = "Wellhead > Connection/Thread Size (Flange/Hub)";
			String strSql1 = "select description FROM CommonLookup WHERE (isDeleted = 0 or isDeleted is null) AND lookupTypeSelection=:lookupTypeSelection AND shortCode = :topConnectionSizePressure";
			String[] paramsFields1 = {"lookupTypeSelection","topConnectionSizePressure"};
			Object[] paramsValues1 = {lookupTypeSelection,connectionSize};
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1,paramsValues1);
			
			if (!lstResult1.isEmpty()) {
				Object a = (Object) lstResult1.get(0);
				Double connectionSizeWell = 0.0;
				if (a != null) 
				{
					connectionSizeWell = Double.parseDouble(a.toString());
					thisWellHeadDetail.setTopConnectionSize(connectionSizeWell);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisWellHeadDetail);	 
				}
			}
		}
	}

}
