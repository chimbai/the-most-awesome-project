package com.idsdatanet.d2.pronet.slickline;

import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Slickline;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SlicklineDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	
		Object obj = node.getData();
		
		if (obj instanceof Slickline) {
		
			Slickline slickline = (Slickline) obj;
			
			//get cummulative cost for each node based start date time
			Double cumcost = SlicklineUtil.calculateCumCost(slickline);
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Slickline.class, "cost");

			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumcost", thisConverter.getUOMMapping());
			}
			
			thisConverter.setBaseValue(cumcost);
			node.getDynaAttr().put("cumcost", thisConverter.getConvertedValue());
		}
		populateRigInformationUid(node, userSelection);
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof Slickline) {
		
			Slickline slickline = (Slickline) obj;
			//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
			
			Calendar currentDate = Calendar.getInstance();
			Date thisCurrentDate = session.getCurrentDaily().getDayDate();
			currentDate.setTime(thisCurrentDate);
			
			if(slickline.getStartDatetime() != null)
			{
				Calendar startDatetime = Calendar.getInstance();
				startDatetime.setTime((Date) slickline.getStartDatetime());
				startDatetime.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
				startDatetime.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
				startDatetime.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH));
				slickline.setStartDatetime(startDatetime.getTime());
			}else {
				slickline.setStartDatetime(currentDate.getTime());
			}
			
			if(slickline.getEndDatetime() != null)
			{
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) slickline.getEndDatetime());
				endDatetime.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH));
				slickline.setEndDatetime(endDatetime.getTime());
			}else {
				slickline.setEndDatetime(currentDate.getTime());
			}
		}
		
		
	}
	
	/**
	 * Auto populate rigInformationUid to Slickline screen
	 * @param node
	 * @param userSelection
	 * @throws Exception
	 */
	public static void populateRigInformationUid(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception  {
		Object obj = node.getData();
		
		if (obj instanceof Slickline) {
			Object rs = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class,userSelection.getOperationUid());
			if(rs != null){
				Operation op = (Operation) rs;
				node.getDynaAttr().put("rigInformationUid", op.getRigInformationUid());
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		populateRigInformationUid(node, userSelection);

	}
	
	
}
