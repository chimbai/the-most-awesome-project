package com.idsdatanet.d2.pronet.swab;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Swab;
import com.idsdatanet.d2.core.model.SwabDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.UserSession;


/**
 * All common utility related to Swab and Swab Detail
 * @author Robin
 *
 */

public class SwabUtil {
	
	/**
	 * save tankStartSize equal to previous tankEndSize if tankStartSize is blank
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	
	public static void getPreviousTankEndSize(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		
		for(CommandBeanTreeNode node:nodes){
			if (node.getData() instanceof SwabDetail){
				SwabDetail thisSwabDetail = (SwabDetail) node.getData();
				
				if(thisSwabDetail.getTankStartSize()==null){
					// get parent node and loop through child to get each tankEndSize
					List<Double> tankEndSizeList = new ArrayList<Double>();
					CommandBeanTreeNode parentNode = node.getParent();
					for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
						Map items = (Map) i.next();
							
						for (Iterator j = items.values().iterator(); j.hasNext();) {
							CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
							if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(node.getData().getClass())) {
								Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
								if (isDeleted == null || !isDeleted.booleanValue()) {
									tankEndSizeList.add((Double) PropertyUtils.getProperty(childNode.getData(), "tankEndSize"));
								}
							}
						}
					}
					node.getDynaAttr().put("tankEndSize", tankEndSizeList);
					

					Double start = (Double) PropertyUtils.getProperty(node.getData(), "tankStartSize");
					Double end = (Double) PropertyUtils.getProperty(node.getData(), "tankEndSize");
					
					if(start == null && end != null) {
						Double cacheTankEndSize = 0.00;
						for(Double tankEndSize : tankEndSizeList) {
							Double currentTankEndSize = end;
							if(tankEndSize==null){
								
							}else{
								if(tankEndSize >= currentTankEndSize) {
									thisSwabDetail.setTankStartSize(cacheTankEndSize);
								}else{
									cacheTankEndSize = tankEndSize;
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * calculate sizeGained :: tankEndSize - tankStartSize
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	public static void calculateSizeGained(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		
		CustomFieldUom thisConverter = new CustomFieldUom(userSession.getUserLocale());
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		for(CommandBeanTreeNode node:nodes){
			if (node.getData() instanceof SwabDetail){
				SwabDetail swabdetail = (SwabDetail) node.getData();
				
				Double tankStartSize = 0.00;
				Double tankEndSize = 0.00;
				Double sizeGained = 0.0;
				
				if(swabdetail.getTankStartSize()!=null){
					tankStartSize = swabdetail.getTankStartSize();
				}
				if(swabdetail.getTankEndSize()!=null){
					tankEndSize = swabdetail.getTankEndSize();
				}
				
				thisConverter.setReferenceMappingField(SwabDetail.class, "tankStartSize");
				thisConverter.setBaseValueFromUserValue(tankStartSize);
				tankStartSize = thisConverter.getBasevalue();
				thisConverter.setReferenceMappingField(SwabDetail.class, "tankEndSize");
				thisConverter.setBaseValueFromUserValue(tankEndSize);
				tankEndSize = thisConverter.getBasevalue();
				
				if (tankEndSize!=null && tankStartSize!=null){
					sizeGained = tankEndSize - tankStartSize;
					
					thisConverter.setReferenceMappingField(SwabDetail.class, "sizeGained");
					thisConverter.setBaseValue(sizeGained);
					swabdetail.setSizeGained(thisConverter.getConvertedValue());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swabdetail);
				}
			}
		}
	}
	
	/**
	 * calculate bswVolume :: (fluidSwabbedVol * bswPercentage) / 100
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	public static void calculateBswVolume(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {

		Double bwsVolume = 0.00;
		CustomFieldUom thisConverter = new CustomFieldUom(userSession.getUserLocale());
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof SwabDetail)
			{
				SwabDetail swabdetail = (SwabDetail) node.getData();
				
				if (swabdetail.getFluidSwabbedVol() != null && swabdetail.getBswPercentage() !=null) {
					bwsVolume =  (swabdetail.getFluidSwabbedVol() * swabdetail.getBswPercentage()) / 100;

					thisConverter.setReferenceMappingField(SwabDetail.class, "bwsVolume");
					thisConverter.setBaseValue(bwsVolume);
					swabdetail.setBswVolume(thisConverter.getConvertedValue());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swabdetail);
					
				}else{
					swabdetail.setBswVolume(0.00);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swabdetail);
				}
			}
		}
	}
	
	/**
	 * calculate cumulative for fluidSwabbedVol, volGas, bswVolume and duration
	 * set cumFluidSwabbedVolume, cumGasVolume, cumBswVolume and cumOilVolume as dynamic attributes
	 * calculate oil amount (vol_oil) :: Formula = swabDetail.fluid_swabbed_vol - bsw_volume
	 * calculate total fluid :: cumFluidSwabbedVolume = totalVolOilForRun + totalVolBSWForRun
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	public static void calculateTotalVolume(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		Double totalSwabbedForRun = 0.00;
		Double totalVolOilForRun = 0.00;
		Double totalVolGasForRun = 0.00;
		Double totalVolBSWForRun = 0.00;
		Double totalDurationForRun = 0.0;
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		CustomFieldUom thisConverter = new CustomFieldUom(userSession.getUserLocale());
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof SwabDetail)
			{
				SwabDetail swabdetail = (SwabDetail) node.getData();
				//cumulative for fluidSwabbedVol, volGas, bswVolume and duration
				if (swabdetail.getFluidSwabbedVol() != null) {
					thisConverter.setReferenceMappingField(SwabDetail.class, "fluidSwabbedVol");
					thisConverter.setBaseValueFromUserValue(swabdetail.getFluidSwabbedVol());
					Double totalVolumeSwabbed = thisConverter.getBasevalue();
					totalSwabbedForRun += totalVolumeSwabbed;
				}
				
				if (swabdetail.getVolGas() !=null) {
					thisConverter.setReferenceMappingField(SwabDetail.class, "volGas");
					thisConverter.setBaseValueFromUserValue(swabdetail.getVolGas());
					Double totalVolGas = thisConverter.getBasevalue();
					totalVolGasForRun += totalVolGas;
				}
				
				if (swabdetail.getBswVolume() !=null) {
					thisConverter.setReferenceMappingField(SwabDetail.class, "bswVolume");
					thisConverter.setBaseValueFromUserValue(swabdetail.getBswVolume());
					Double totalBSWRecovered = thisConverter.getBasevalue();
					totalVolBSWForRun += totalBSWRecovered;
				}
				
				if (swabdetail.getDuration() !=null) {
					thisConverter.setReferenceMappingField(SwabDetail.class, "duration");
					thisConverter.setBaseValueFromUserValue(swabdetail.getDuration());
					Double totalDuration = thisConverter.getBasevalue();
					totalDurationForRun += totalDuration;
				}
				
				//calculate oil amount (vol_oil) :: Formula = swabDetail.fluid_swabbed_vol - bsw_volume
				Double oilAmount = 0.0;
				if (swabdetail.getFluidSwabbedVol() !=null && swabdetail.getBswPercentage() !=null) {
					
					Double bsw = swabdetail.getBswPercentage() / 100;
					oilAmount = swabdetail.getFluidSwabbedVol() * (1-bsw);
				
					if (oilAmount != null){
						thisConverter.setReferenceMappingField(Swab.class, "volOil");
						thisConverter.setBaseValue(oilAmount);
						swabdetail.setVolOil(thisConverter.getConvertedValue());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swabdetail);
					}
			
				}else{
					swabdetail.setVolOil(0.0);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swabdetail);
				}
				
				if (swabdetail.getVolOil() !=null) {
					thisConverter.setReferenceMappingField(SwabDetail.class, "volOil");
					thisConverter.setBaseValueFromUserValue(swabdetail.getVolOil());
					Double totalVolOil = thisConverter.getBasevalue();
					totalVolOilForRun += totalVolOil;	
				}
				
				//calculate cumFluidSwabbedVolume = totalVolOilForRun + totalVolBSWForRun
				Double cumFluidSwabbedVolume = 0.00;
				if (totalVolOilForRun!=null || totalVolBSWForRun!=null){
					if (totalVolOilForRun==null) totalVolOilForRun=0.00;
					if (totalVolBSWForRun==null) totalVolBSWForRun=0.00;
					
					cumFluidSwabbedVolume = totalVolOilForRun + totalVolBSWForRun;
				}

				String strSql1 = "UPDATE SwabDetail SET cumFluidSwabbedVolume=:cumFluidSwabbed, cumGasVolume =:cumVolGas, cumBswVolume=:cumVolBsw, cumOilVolume=:cumVolOil, totalFluidVolume=:cumFluidSwabbedVolume WHERE swabDetailUid =:swabDetailUid";
				String[] paramsFields = {"cumFluidSwabbed", "cumVolGas", "cumVolBsw", "cumVolOil", "cumFluidSwabbedVolume", "swabDetailUid"};
				Object[] paramsValues = {totalSwabbedForRun, totalVolGasForRun, totalVolBSWForRun, totalVolOilForRun, cumFluidSwabbedVolume, swabdetail.getSwabDetailUid()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);

			}
			
		}
		
		if (parent.getData() instanceof Swab){
			Swab swab = (Swab) parent.getData();
			
			if (totalSwabbedForRun != null) {
				thisConverter.setReferenceMappingField(Swab.class, "totalSwabVolume");
				thisConverter.setBaseValue(totalSwabbedForRun);
				swab.setTotalSwabVolume(thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(Swab.class, "totalOilVolume");
				thisConverter.setBaseValue(totalVolOilForRun);
				swab.setTotalOilVolume(thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(Swab.class, "totalGasVolume");
				thisConverter.setBaseValue(totalVolGasForRun);
				swab.setTotalGasVolume(thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(Swab.class, "totalBswRecovered");
				thisConverter.setBaseValue(totalVolBSWForRun);
				swab.setTotalBswRecovered(thisConverter.getConvertedValue());
				
				if ("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "autoCalcTotalSwabDuration"))) {
					thisConverter.setReferenceMappingField(Swab.class, "totalSwabDuration");
					thisConverter.setBaseValue(totalDurationForRun);
					swab.setTotalSwabDuration(thisConverter.getConvertedValue());
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(swab);
			}
		}
	}
	
	/**
	 * calculate oil rate :: (fluid_swabbed_vol * (1- bsw_percentage) / total duration of swab details
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	public static void calculateOilRate(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		CustomFieldUom thisConverter = new CustomFieldUom(userSession.getUserLocale());
		Double oilRate = 0.0;
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof SwabDetail)
			{
				SwabDetail swabdetail = (SwabDetail) node.getData();
				
				if (swabdetail.getFluidSwabbedVol() !=null && swabdetail.getBswPercentage()!=null && swabdetail.getDuration() !=null) {
					
					thisConverter.setReferenceMappingField(SwabDetail.class, "fluidSwabbedVol");
					thisConverter.setBaseValueFromUserValue(swabdetail.getFluidSwabbedVol());
					Double fluidSwabbedVol = thisConverter.getBasevalue();
					Double bsw = swabdetail.getBswPercentage() / 100;
					
					thisConverter.setReferenceMappingField(SwabDetail.class, "duration");
					thisConverter.setBaseValueFromUserValue(swabdetail.getDuration());
					Double duration = thisConverter.getBasevalue();
					
					if (duration > 0 && fluidSwabbedVol > 0) {
						oilRate = fluidSwabbedVol * (1- bsw) / duration;
					} else {
						oilRate = 0.0;
					}
					
					String strSql = "UPDATE SwabDetail SET oilRate=:oilRate where swabDetailUid =:swabDetailUid";
					String[] paramsFields = {"oilRate", "swabDetailUid"};
					Object[] paramsValues = {oilRate, swabdetail.getSwabDetailUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
				} else {
					oilRate = 0.0;	
					String strSql = "UPDATE SwabDetail SET oilRate=:oilRate where swabDetailUid =:swabDetailUid";
					String[] paramsFields = {"oilRate", "swabDetailUid"};
					Object[] paramsValues = {oilRate, swabdetail.getSwabDetailUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
				}
			}
		}
	}
	
	/**
	 * sum(fluidSwabbedVol), sum(volOil) and sum(volGas) for the whole operation
	 * @param nodes
	 * @param userSession
	 * @param parent
	 * @throws Exception
	 * @return null
	 */
	public static void calculateTotalVolumePerOper(Collection<CommandBeanTreeNode> nodes, UserSession userSession, CommandBeanTreeNode parent) throws Exception {

		if (parent.getData() instanceof Swab) {
			Swab thisSwab = (Swab) parent.getData();

			//Total For Operation
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);

			if (StringUtils.isNotBlank(userSession.getCurrentOperationUid())) {
				Daily currentDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
				String[] paramsFields2 = {"thisDayDate","thisOperationUid"};
				Object[] paramsValues2 = {currentDaily.getDayDate(),userSession.getCurrentOperationUid()};
				
				String strSql = "select sum(b.fluidSwabbedVol) as totalSwabbedForOper, sum(b.volOil) as totalVolOilForOper, sum(b.volGas) as totalVolGasForOper from Swab a, SwabDetail b, Daily c where c.dailyUid=a.dailyUid and c.dayDate <=:thisDayDate and a.operationUid=b.operationUid and a.operationUid =:thisOperationUid and a.swabUid=b.swabUid and (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (c.isDeleted = false or c.isDeleted is null)";
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				String strSql2 = "FROM Swab WHERE (isDeleted = false or isDeleted is null) " +
				"and operationUid = :thisOperationUid";
				String[] paramsFields1 = {"thisOperationUid"};
				String[] paramsValues1 = {userSession.getCurrentOperationUid()};
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields1, paramsValues1, qp);
				
				if (lstResult1.size() > 0){
					for(Object objSwab: lstResult1){
						Object[] b = (Object[]) lstResult.get(0);
						
						Double totalSwabbedForOper = 0.00;
						Double totalVolOilForOper = 0.00;
						Double totalVolGasForOper = 0.00;
						
						if (b[0] != null) totalSwabbedForOper = Double.parseDouble(b[0].toString());
						thisSwab.setTotalSwabbedVolumeForOperation(totalSwabbedForOper);
						
						if (b[1] != null) totalVolOilForOper = Double.parseDouble(b[1].toString());
						thisSwab.setTotalOilVolumeForOperation(totalVolOilForOper);
						
						if (b[2] != null) totalVolGasForOper = Double.parseDouble(b[2].toString());
						thisSwab.setTotalGasVolumeForOperation(totalVolGasForOper);
						
						Swab OperationSwab = (Swab) objSwab;
						
						if(OperationSwab.getDailyUid().equals(userSession.getCurrentDailyUid())){
							OperationSwab.setTotalSwabbedVolumeForOperation(totalSwabbedForOper);
							OperationSwab.setTotalOilVolumeForOperation(totalVolOilForOper);
							OperationSwab.setTotalGasVolumeForOperation(totalVolGasForOper);
						}
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(OperationSwab,qp);
					}
				}
			}	
		}
	}
	
	/**
	 * get the list of report daily
	 * @param operationUid
	 * @param dailyUid
	 * @throws Exception
	 * @return reportDaily
	 */
	public static Map< String, ReportDaily> reportDailyList = new HashMap<String, ReportDaily>();
	
	public static ReportDaily getReportDaily(String operationUid, String dailyUid) throws Exception {
		if (reportDailyList.containsKey(dailyUid)) return reportDailyList.get(dailyUid);
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		String sql = "FROM ReportDaily rd, Daily d " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"AND (rd.isDeleted = false or rd.isDeleted is null) " +
				"AND rd.dailyUid=d.dailyUid " +
				"AND d.dailyUid=:dailyUid " +
				"AND rd.reportType=:reportType";		
		String[] paramsFields = {"dailyUid","reportType"};
		Object[] paramsValues = {dailyUid, reportType};
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if (list.size()>0) {
			Object[] obj_array = list.get(0);
			ReportDaily reportDaily = (ReportDaily) obj_array[0]; 
			reportDailyList.put(dailyUid, reportDaily);
			return reportDaily;
		}
		return null;
	}
	
	/**
	 * get the day date 
	 * @param operationUid
	 * @param dailyUid
	 * @throws Exception
	 * @return String
	 */
	public static String getDayDate(String operationUid, String dailyUid) throws Exception{
		String selectedDayDate = "";
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		ReportDaily reportDaily = getReportDaily(operationUid, dailyUid);
		if (reportDaily!=null) {
			Date date = (Date) reportDaily.getReportDatetime();
			selectedDayDate = df.format(date).toString();						
		}
		return selectedDayDate;
	}
}
