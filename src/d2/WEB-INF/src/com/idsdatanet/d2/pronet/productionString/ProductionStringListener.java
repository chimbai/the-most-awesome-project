package com.idsdatanet.d2.pronet.productionString;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionStringListener extends EmptyDataNodeListener implements DataLoaderInterceptor {

	String uid = null;
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if(daily == null){
			return conditionClause.replace("{_custom_condition_}", "");
		}
	
		if(meta.getTableClass().equals(ProductionString.class)) {
			String selectedString  = (String)parentNode.getDynaAttr().get("productionStringUid");
			String thisFilter = "";
			
			//query.addParam("dailyDaydate", daily.getDayDate());
			query.addParam("selectedString", selectedString);
			query.addParam("thisWellboreUid", userSelection.getWellboreUid());
			//thisFilter = "((installDate <= :dailyDaydate or installDate =null) AND (uninstallDate >= :dailyDaydate or uninstallDate = null)) AND wellboreUid= :thisWellboreUid";				
			thisFilter ="productionStringUid =:selectedString AND wellboreUid= :thisWellboreUid";
				
			return conditionClause.replace("{_custom_condition_}", thisFilter);
				
		}else if(meta.getTableClass().equals(ProductionStringDetailLog.class)) {
	
			if (request.getParameter("uid") !=null){
				uid= request.getParameter("uid");
			}else {
				uid= request.getParameter("productionStringDetailUid");
			}
			String thisFilter = "";
			query.addParam("detailUid", uid);
			thisFilter = "(productionStringDetailUid = :detailUid)";
			return conditionClause.replace("{_custom_condition_}", thisFilter);
			
		}else {
			return null;
		}
		
		
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
}
