package com.idsdatanet.d2.pronet.wellHead;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.WellHeadDetail;
import com.idsdatanet.d2.core.model.WellHeadDetailLog;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellHeadUtils {
	private static final String MAPPING_SEPARATOR=".";
	
	/**
	 * Method to get update latest log data to component (based on the field configure in bean property)
	 * @param mapping
	 * @param nodes
	 * @param session
	 * @param parent
	 */
	public static void updateLastLogToComponent(Map<String,String> mapping, Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
		String wellHeadDetailUid = null;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		qp.setDatumConversionEnabled(true);
		
		QueryProperties qp2 = new QueryProperties();
		qp2.setUomConversionEnabled(false);
		qp2.setDatumConversionEnabled(false);
		CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale());
		if (parent.getData() instanceof WellHeadDetail)
		{
			WellHeadDetail whdetail = (WellHeadDetail) parent.getData();
			wellHeadDetailUid = whdetail.getWellHeadDetailUid();
			
			WellHeadDetailLog whdLog= WellHeadUtils.getLastestServiceLog(wellHeadDetailUid, qp2);// load raw
			if (whdLog!=null){

				for (Iterator iter = mapping.entrySet().iterator(); iter.hasNext();) 
				{
					Map.Entry entry = (Map.Entry) iter.next();
					String key = entry.getKey().toString();
					String fieldToUpdate = entry.getValue().toString().split("["+MAPPING_SEPARATOR+"]")[1];

					Object value = PropertyUtils.getProperty(whdLog,key);
					if (value !=null){
						java.lang.Class type = PropertyUtils.getPropertyType(whdLog, key);
						if (type == java.lang.Double.class){
							thisConverter.setReferenceMappingField(WellHeadDetail.class, key);
							if (thisConverter.isUOMMappingAvailable()) {
								thisConverter.setBaseValue((Double)value);
								if (thisConverter.getUOMMapping().isDatumConversion()) {
									thisConverter.addDatumOffset();						
								}
								value = thisConverter.getConvertedValue();
							}					
						}
					}
					
					PropertyUtils.setProperty(whdetail, fieldToUpdate, value);
				}
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(whdetail, qp);
		}
		
		
	}
	
	/**
	 * Method to get latest service log data based of service date time
	 * @param wellHeadDetailUid
	 * @param qp
	 * @return WellHeadDetailLog
	 */
	public static WellHeadDetailLog getLastestServiceLog(String wellHeadDetailUid, QueryProperties qp) throws Exception {

		String strSql = "FROM WellHeadDetailLog WHERE (isDeleted = false or isDeleted is null) " +
						" AND wellHeadDetailUid = :wellHeadDetailUid order by serviceDateTime DESC";

		List<WellHeadDetailLog> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellHeadDetailUid", wellHeadDetailUid, qp);
		
		if (lstResult.size() > 0) {
			WellHeadDetailLog whdlog = (WellHeadDetailLog) lstResult.get(0);
			return whdlog;
		}
		
		return null;
		
	}
	
	
}
