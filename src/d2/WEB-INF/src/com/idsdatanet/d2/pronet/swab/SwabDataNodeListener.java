package com.idsdatanet.d2.pronet.swab;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Swab;
import com.idsdatanet.d2.core.model.SwabDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SwabDataNodeListener extends EmptyDataNodeListener {
	
	private String testType = null;
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
			
		if (className.equalsIgnoreCase("SwabDetail") && status.isAnyNodeSavedOrDeleted()) {
			parent.sortChild("SwabDetail", new TimeComparator());
			SwabUtil.getPreviousTankEndSize(status.getUpdatedNodes(), userSession, parent);
			SwabUtil.calculateSizeGained(status.getUpdatedNodes(), userSession, parent);
			SwabUtil.calculateBswVolume(status.getUpdatedNodes(), userSession, parent);
			SwabUtil.calculateTotalVolume(status.getUpdatedNodes(), userSession, parent);
			SwabUtil.calculateOilRate(status.getUpdatedNodes(), userSession, parent);
			SwabUtil.calculateTotalVolumePerOper(status.getUpdatedNodes(), userSession, parent);
		}
		
	}
	
	private class TimeComparator implements Comparator<CommandBeanTreeNode>{
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
			try{
				SwabDetail swd1 = (SwabDetail) o1.getData();
				SwabDetail swd2 = (SwabDetail) o2.getData();
				Date date1 = null;
				Date date2 = null;
				//check either use startDatetime or endDatetime to do sorting
				if(swd1.getStartDatetime()!=null && swd2.getStartDatetime()!=null){
					date1 = swd1.getStartDatetime();
					date2 = swd2.getStartDatetime();
				}else if (swd1.getEndDatetime()!=null && swd2.getEndDatetime()!=null){
					date1 = swd1.getEndDatetime();
					date2 = swd2.getEndDatetime();
				}
				else{
					date1 = null;
					date2 = null;
				}
				
				if(date1 == null && date2 == null){
					return 0;
				}else if(date1 == null){
					return -1;
				}else if(date2 == null){
					return 1;
				}else{
					return date1.compareTo(date2);
				}
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return 0;
			}
		}
	}
	
	public void setTestType(String value) {
		this.testType = value;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof Swab) {
			Swab thisSwab = (Swab) object;
			
			if(this.testType != null)
			{
				thisSwab.setTestType(this.testType);
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
	
		if (object instanceof Swab) {		
			Swab thisSwab = (Swab) object;
			
			if (thisSwab.getDailyUid()!=null && thisSwab.getOperationUid()!=null){
				node.getDynaAttr().put("dayDate", SwabUtil.getDayDate(thisSwab.getOperationUid(),thisSwab.getDailyUid()));
				
			}
			
			//Total For Operation
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			if (StringUtils.isNotBlank(userSelection.getOperationUid())) {
				Daily currentDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				String[] paramsFields2 = {"thisDayDate","thisOperationUid"};
				Object[] paramsValues2 = {currentDaily.getDayDate(),userSelection.getOperationUid()};
				
				String strSql = "select sum(b.fluidSwabbedVol) as totalSwabbedForOper, sum(b.volOil) as totalVolOilForOper, sum(b.volGas) as totalVolGasForOper from Swab a, SwabDetail b, Daily c where c.dailyUid=a.dailyUid and c.dayDate <=:thisDayDate and a.operationUid=b.operationUid and a.operationUid =:thisOperationUid and a.swabUid=b.swabUid and (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (c.isDeleted = false or c.isDeleted is null)";
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				String strSql2 = "FROM Swab WHERE (isDeleted = false or isDeleted is null) " +
				"and operationUid = :thisOperationUid";
				String[] paramsFields1 = {"thisOperationUid"};
				String[] paramsValues1 = {userSelection.getOperationUid()};
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields1, paramsValues1, qp);
				
				if (lstResult1.size() > 0){
					for(Object objSwab: lstResult1){
						Object[] b = (Object[]) lstResult.get(0);
						
						Double totalSwabbedForOper = 0.00;
						Double totalVolOilForOper = 0.00;
						Double totalVolGasForOper = 0.00;
						
						if (b[0] != null) totalSwabbedForOper = Double.parseDouble(b[0].toString());
						thisSwab.setTotalSwabbedVolumeForOperation(totalSwabbedForOper);
						
						if (b[1] != null) totalVolOilForOper = Double.parseDouble(b[1].toString());
						thisSwab.setTotalOilVolumeForOperation(totalVolOilForOper);
						
						if (b[2] != null) totalVolGasForOper = Double.parseDouble(b[2].toString());
						thisSwab.setTotalGasVolumeForOperation(totalVolGasForOper);
						
						Swab OperationSwab = (Swab) objSwab;
						
						if(OperationSwab.getDailyUid().equals(userSelection.getDailyUid())){
							OperationSwab.setTotalSwabbedVolumeForOperation(totalSwabbedForOper);
							OperationSwab.setTotalOilVolumeForOperation(totalVolOilForOper);
							OperationSwab.setTotalGasVolumeForOperation(totalVolGasForOper);
						}
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(OperationSwab,qp);
					}
				}
			}
		} 
		
	}
	
	
	
	
	
}
