package com.idsdatanet.d2.pronet.productionPump;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.model.ProductionPumpParamComp;
import com.idsdatanet.d2.core.model.ProductionPumpParamCompLog;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class ProductionPumpUtils {
	
	public static Double getPSNData(ReportDaily reportdaily)  throws Exception{
		String productionPumpParamUid = reportdaily.getProductionPumpParamUid();
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(reportdaily.getReportDatetime());
		thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
		thisCalendar.set(Calendar.MINUTE, 0);
		thisCalendar.set(Calendar.SECOND , 0);
		thisCalendar.set(Calendar.MILLISECOND , 0);
		Date currentDate = thisCalendar.getTime();
		
		if (productionPumpParamUid!=null){
			String strSql = "FROM ProductionPumpParamComp WHERE (isDeleted = false or isDeleted is null) and productionPumpParamUid =:productionPumpParamUid and componentType='PSN'";
			List <ProductionPumpParamComp> lstResult = (List<ProductionPumpParamComp>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "productionPumpParamUid", productionPumpParamUid);
			
			if (lstResult.size() > 0){
				for (ProductionPumpParamComp pumpComp : lstResult){
					String productionPumpParamCompUid = pumpComp.getProductionPumpParamCompUid();
					String strSql2 = "FROM ProductionPumpParamCompLog WHERE (isDeleted = false or isDeleted is null) and productionPumpParamCompUid =:productionPumpParamCompUid "
							+ "AND installDatetime <=:installDatetime "
							+ "AND (removeDatetime >=:removeDatetime or removeDatetime=null) "
							+ "Order by installDatetime Desc";
					String[] paramsFields = {"productionPumpParamCompUid", "installDatetime", "removeDatetime"};
					Object[] paramsValues = {productionPumpParamCompUid, currentDate, currentDate};
					List <ProductionPumpParamCompLog> lstResult2 = (List<ProductionPumpParamCompLog>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
					if (lstResult2.size() > 0){
						ProductionPumpParamCompLog productionPumpParamCompLog = lstResult2.get(0);
						return productionPumpParamCompLog.getTopDepthMdMsl();
					}
				}
			}
		}
		return null;	
	}
	

}
