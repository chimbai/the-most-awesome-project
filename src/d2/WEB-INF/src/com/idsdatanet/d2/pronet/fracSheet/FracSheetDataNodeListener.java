package com.idsdatanet.d2.pronet.fracSheet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.FracSheet;
import com.idsdatanet.d2.core.model.FracSheetDetail;
import com.idsdatanet.d2.core.model.KeyPerformanceIndicator;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FracSheetDataNodeListener extends EmptyDataNodeListener{
	private String type;

	public void setType(String value){
		this.type = value;
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof FracSheet && BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed) {
			if (type != null && !"".equals(type)){
				String kpiType[] = type.split(","); 
				if (kpiType != null) {
					for (String type : kpiType) {
						this.createNewKPI(node, type);
					}
				}
			}
			else{
				this.createNewKPI(node, null);
			}	
		}

		if (obj instanceof FracSheetDetail && BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed){			
			if (type != null && !"".equals(type)){
				String kpiType[] = type.split(","); 
				if (kpiType != null) {
					for (String type : kpiType) {
						this.createNewKPI(node.getParent(), type);
					}
				}
			}
			else{
				this.createNewKPI(node.getParent(), null);
			}	
		}
		
	
	}
	
	public void createNewKPI(CommandBeanTreeNode node, String type) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof FracSheet) {
			FracSheet fracSheet = (FracSheet) obj;
			
			String condition = "";
			if (!"".equals(type) && type != null){
				condition = " and type='" + type + "'";
			}
			
			//check if kpi existed before create
			String strSql = "FROM KeyPerformanceIndicator WHERE fracSheetUid =:fracSheetUid " + condition;		
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "fracSheetUid", fracSheet.getFracSheetUid());
			
			if (lstResult.size() > 0){	
				
			}else {
				KeyPerformanceIndicator newKPI = new KeyPerformanceIndicator();
				newKPI.setFracSheetUid(fracSheet.getFracSheetUid());
				newKPI.setDailyUid(fracSheet.getDailyUid());
				newKPI.setType(type);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newKPI);
			}
			
		}
	}

	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj = node.getData();
		if(obj instanceof FracSheet) {
			FracSheet fracSheet = (FracSheet) obj;
			if (type != null && !"".equals(type)){
				String kpiType[] = type.split(","); 
				if (kpiType != null) {
					for (String type : kpiType) {
						this.deleteKpiIndicator(fracSheet.getFracSheetUid(), type);
					}
				}
			}
			else 
				this.deleteKpiIndicator(fracSheet.getFracSheetUid(), null);
		}
	
		
	}
	
	public void deleteKpiIndicator(String fracSheetUid, String type) throws Exception {
		if (StringUtils.isNotBlank(fracSheetUid)) {
			
			String condition = "";
			if (!"".equals(type) && type != null){
				condition = " and type='" + type + "'";
			}
			
			String strSql = "UPDATE KeyPerformanceIndicator SET isDeleted = true where fracSheetUid =:fracSheetUid" + condition;
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"fracSheetUid"}, new Object[] {fracSheetUid});
		}
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		if ("FracSheetDetail".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			FracSheetUtils.getTotalPropPumped(status.getUpdatedNodes(), session, parent);
			FracSheetUtils.getMaxPropConc(parent);
			FracSheetUtils.getPumpDuration(parent);
		}
	}
}
