package com.idsdatanet.d2.pronet.productionStringPacker;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.ProductionStringPacker;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringPackerDataNodeListener extends EmptyDataNodeListener{
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof ProductionStringPacker){
			
			//Calculate packer length
			ProductionStringPackerUtils.calculatePackerLength(commandBean, obj);
		}
	
	}

}