package com.idsdatanet.d2.pronet.productionString;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionStringAlt1CommandBeanListener implements CommandBeanListener {
	
	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
	}	
	/**
	 * Method used to copy selected production string data to current node and child node
	 * @param commandBean
	 * @param targetNode
	 * @param wellHeadUid
	 * @param session
	 */	
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		if (commandBean.getRoot().getList() != null) {	
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ProductionString").values()) {
				boolean clampNoRecords = false;
				boolean isAddNewClamp = false;
				boolean isDeleteAllClamp = true;
				
				if(node.getChild("ProductionStringFastenTool").values().isEmpty()) {
					clampNoRecords = true;
				}
				else {
					for (CommandBeanTreeNode clampNode: node.getChild("ProductionStringFastenTool").values()){	
						if(clampNode.getAtts().getAction().equals(Action.SAVE)) {
							isAddNewClamp = true;
						}
						if(!clampNode.getAtts().getAction().equals(Action.DELETE)) {
							isDeleteAllClamp = false;
						}
					}
				}
				if(!clampNoRecords && !isDeleteAllClamp) {
					runError(commandBean, node, isAddNewClamp);
				}
			}
		}		
	}
	
	public void runError(CommandBean commandBean, CommandBeanTreeNode node, boolean isAddNewClamp) throws Exception {

		boolean allflagedfordeletion = true;
		boolean componentHasRecords = false;
		
		if(node.getChild("ProductionStringAttached").values().isEmpty()) {
			if(isAddNewClamp) {
				addNewError(commandBean);
			}
		}
		else {
			for (CommandBeanTreeNode childNode: node.getChild("ProductionStringAttached").values()){				
				if(!childNode.getChild("ProductionStringAttachedComponent").values().isEmpty()) { //check for delete
					componentHasRecords = true;
					for(CommandBeanTreeNode componentNode: childNode.getChild("ProductionStringAttachedComponent").values()) {
						if(!componentNode.getAtts().getAction().equals(Action.DELETE)) {
							allflagedfordeletion = false;
						}
					} //if all components is delete but clamps is not
				}
			}
			if(isAddNewClamp && !componentHasRecords) {
				addNewError(commandBean);
			}
			if(allflagedfordeletion) {
				if(isAddNewClamp) {
					addNewError(commandBean);
				}
				else {
					deleteError(commandBean);
				}
			}
		}
	}
	
	public void addNewError(CommandBean commandBean) throws Exception {
		commandBean.getRoot().setDirty(true); 
		commandBean.getSystemMessage().addError("Clamps/Protectors can only be added if there is Attached String Component records" ,true, true);
		commandBean.setAbortFormProcessing(true);
	}
	
	public void deleteError(CommandBean commandBean) throws Exception {
		commandBean.getRoot().setDirty(true); 
		commandBean.getSystemMessage().addError("Please remove all Clamps/Protectors records before removing all Attached String Component records" ,true, true);
		commandBean.setAbortFormProcessing(true);
	}
	
	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
