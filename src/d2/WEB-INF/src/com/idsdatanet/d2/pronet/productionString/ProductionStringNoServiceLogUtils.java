package com.idsdatanet.d2.pronet.productionString;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProductionStringNoServiceLogUtils{
	private static final String MAPPING_SEPARATOR=".";

	/**
	 * Method to get update latest log data to component (based on the field configure in bean property)
	 * @param mapping
	 * @param nodes
	 * @param session
	 * @param parent
	 */
	public static void updateLastLogToComponent(Map<String,String> mapping, Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
		String productionStringDetailUid = null;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		qp.setDatumConversionEnabled(true);
		
		QueryProperties qp2 = new QueryProperties();
		qp2.setUomConversionEnabled(false);
		qp2.setDatumConversionEnabled(false);
		CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale());
		
		if (parent.getData() instanceof ProductionStringDetail)
		{
			ProductionStringDetail productionStringDetail =(ProductionStringDetail)parent.getData();
			productionStringDetailUid = productionStringDetail.getProductionStringDetailUid();
			ProductionStringDetailLog psdLog = ProductionStringUtils.getLastestServiceLog(productionStringDetailUid, qp2); //load raw
			
			if(psdLog!=null){
				for (Iterator iter = mapping.entrySet().iterator(); iter.hasNext();) 
				{
					Map.Entry entry = (Map.Entry) iter.next();
					String key = entry.getKey().toString();
					String fieldToUpdate = entry.getValue().toString().split("["+MAPPING_SEPARATOR+"]")[1];

					Object value = PropertyUtils.getProperty(psdLog,key);
					if (value !=null){
						java.lang.Class type = PropertyUtils.getPropertyType(psdLog, key);
						if (type == java.lang.Double.class){
							thisConverter.setReferenceMappingField(ProductionStringDetail.class, key);
							if (thisConverter.isUOMMappingAvailable()) {
								thisConverter.setBaseValue((Double)value);
								if (thisConverter.getUOMMapping().isDatumConversion()) {
									thisConverter.addDatumOffset();						
								}
								value = thisConverter.getConvertedValue();
							}					
						}
					}
					PropertyUtils.setProperty(productionStringDetail, fieldToUpdate, value);							

				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(productionStringDetail, qp);
			}
		}
			
	}
	
	/**
	 * Method to update existing sequence value and total no. of components in tally value for all wells
	 * @param operationUid
	 */
	public static void updateSequenceAndTotalNoOfComponentsInTally(String operationUid) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String queryString = "FROM ProductionString " +
		"WHERE (isDeleted=false or isDeleted is null) " +
		"AND operationUid=:operationUid order by installDateTime";
		
		List <ProductionString> productionStringList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid"}, new Object[]{operationUid}, qp);
		if(productionStringList!=null & productionStringList.size() > 0)
		{
			for(ProductionString productionStringData:productionStringList)
			{
				Double cumValue = 0.00;
				String productionStringUid = "";
				productionStringUid = productionStringData.getProductionStringUid();
				
				String queryPSD = "FROM ProductionStringDetail " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND productionStringUid=:productionStringUid order by depthMdMsl";
				Double seq = 0.0;
				List <ProductionStringDetail> productionStringDetailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryPSD, new String[]{"productionStringUid"}, new Object[]{productionStringUid}, qp);
				if(productionStringDetailList!=null & productionStringDetailList.size() > 0)
				{
					for(ProductionStringDetail productionStringDetailData:productionStringDetailList)
					{
						seq = seq + 1;
						String hql = "UPDATE ProductionStringDetail set sequence=:sequence, lastEditDatetime=:lastEditDatetime WHERE productionStringDetailUid=:productionStringDetailUid";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, new String[]{"sequence","lastEditDatetime","productionStringDetailUid"}, new Object[]{seq, new Date(), productionStringDetailData.getProductionStringDetailUid()}, qp);
					}
				}
				
				String queryProductionStringDetail = "FROM ProductionStringDetail " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND productionStringUid=:productionStringUid order by sequence";
				
				List <ProductionStringDetail> PSDList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryProductionStringDetail, new String[]{"productionStringUid"}, new Object[]{productionStringUid}, qp);
				if(PSDList.size() > 0 && PSDList!=null)
				{
					for(ProductionStringDetail psdetail:PSDList)
					{
						if(psdetail.getNumJoints()!=null)
						{
							cumValue+=psdetail.getNumJoints();
						}
						
						String hql = "UPDATE ProductionStringDetail set tallyJoints=:tallyJoints, lastEditDatetime=:lastEditDatetime WHERE productionStringDetailUid=:productionStringDetailUid";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, new String[]{"tallyJoints","lastEditDatetime","productionStringDetailUid"}, new Object[]{cumValue, new Date(), psdetail.getProductionStringDetailUid()}, qp);
					}
					
				}
				
			}
		}
		
	}
	/**
	 * Method to calculate Total No. of Components in Tally based on fieldname when any nodes updated
	 * @param userSession
	 * @param thisConverter
	 * @param productionStringUid
	 */
	public static void calculateTotalNoOfComponentsInTally(UserSession userSession, CustomFieldUom thisConverter, String productionStringUid) throws Exception
	{
		Double cumValue = 0.00;
		
		String StrSQL = "From ProductionStringDetail where (isDeleted = false or isDeleted is null) and " +
		"productionStringUid =:productionStringUid order by sequence , depthMdMsl asc";
		
		String[] paramNames = {"productionStringUid"};
		Object[] paramValues = {productionStringUid};
		List<ProductionStringDetail> items = null;
		items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(StrSQL, paramNames,paramValues);
		
		if(items!=null && items.size() > 0)
		{
			for(ProductionStringDetail productionStringDetail : items){
				if (productionStringDetail.getNumJoints()!=null){
					thisConverter.setReferenceMappingField(ProductionStringDetail.class, "numJoints");
					thisConverter.setBaseValueFromUserValue(productionStringDetail.getNumJoints());
					cumValue+=thisConverter.getBasevalue();
				}
				thisConverter.setBaseValue(cumValue);
				productionStringDetail.setTallyJoints(thisConverter.getConvertedValue());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(productionStringDetail);
			}
		}
	}

	/**
	 * Method to calculate total length based on num of joint
	 * @param numOfJoint
	 * @param length
	 * @return totalLength
	 */
	public static Double calculateTotalLength(Integer numOfJoint, Double length, CustomFieldUom thisConverter) throws Exception {
		Double totalLength = 0.0;
		
		if (length != null && numOfJoint != null) {
			
			thisConverter.setBaseValueFromUserValue(length);
			
			length = thisConverter.getBasevalue();
			
			totalLength = length * numOfJoint;				
		}
		
		return totalLength;
	}
	
	/**
	 * Method to calculate depth based on the elevation and total length
	 * @param depth
	 * @param length
	 * @return bottom_depth
	 */
	public static Double calculateDepth(Double previous_bottom_depth, Double elevation_depth, Double total_length) throws Exception {
		Double bottom_depth = 0.0;
		
		if (elevation_depth != 0 && total_length != 0 && previous_bottom_depth == 0) {
			/*
			thisConverter.setBaseValueFromUserValue(length);
			length = thisConverter.getBasevalue();
			
			thisConverter.setBaseValueFromUserValue(depth);
			depth = thisConverter.getBasevalue();
			*/
			bottom_depth = total_length + elevation_depth;				
		}
		else {
			/*
			thisConverter.setBaseValueFromUserValue(length);
			length = thisConverter.getBasevalue();
			
			thisConverter.setBaseValueFromUserValue(previous_bottom_depth);
			previous_bottom_depth = thisConverter.getBasevalue();
			*/
			bottom_depth = total_length + previous_bottom_depth;	
		}
		
		return bottom_depth;
	}
	
	/**
	 * Method to calculate cummulative value based on fieldname when any nodes updated
	 * @param nodes
	 * @param parent
	 * @param thisConverter
	 * @param fieldname
	 * @param fieldToUpdate
	 */
	public static void calculateCumulativeValue(Collection<CommandBeanTreeNode> nodes, CommandBeanTreeNode parent, CustomFieldUom thisConverter, String fieldname, String fieldToUpdate) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (parent.getData() instanceof ProductionString)
		{
			Double cumValue = 0.00;

			for(CommandBeanTreeNode node:nodes)
			{
				if (node.getData() instanceof ProductionStringDetail)
				{
					ProductionStringDetail psdetail=(ProductionStringDetail) node.getData();
					
					Double value = (Double) PropertyUtils.getProperty(psdetail, fieldname);
					if (value!=null)
					{
						thisConverter.setReferenceMappingField(ProductionStringDetail.class, fieldname);
						thisConverter.setBaseValueFromUserValue(value);
						
						cumValue+=thisConverter.getBasevalue();
					}
					
					thisConverter.setReferenceMappingField(ProductionStringDetail.class, fieldToUpdate);
					thisConverter.setBaseValue(cumValue);
					PropertyUtils.setProperty(psdetail, fieldToUpdate, thisConverter.getConvertedValue());
					
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(psdetail);
				}
			}
		}
	}
	/**
	 * Method to calculate top and bottom depth based on set depth
	 * @param nodes
	 * @param parent
	 * @param thisConverter
	 */
	public static void calculateTopAndBottomDepth(Collection<CommandBeanTreeNode> nodes, CommandBeanTreeNode parent, CustomFieldUom thisConverter) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (parent.getData() instanceof ProductionString)
		{
			ProductionString ps = (ProductionString) parent.getData();
			Double setDepth = 0.00;
			Double topDepth = 0.00;
			Double bottomDepth = 0.00;
			Double cumLength_raw = 0.00;
			//get set depth from production string table
			if (ps.getDepthMdMsl() !=null) setDepth = ps.getDepthMdMsl();
			
			thisConverter.setReferenceMappingField(ProductionString.class, "depthMdMsl");
			thisConverter.setBaseValueFromUserValue(setDepth);
			
			Double setDepth_raw = thisConverter.getBasevalue();

			for(CommandBeanTreeNode node:nodes)
			{
				Double length_raw = 0.00;
				if (node.getData() instanceof ProductionStringDetail)
				{
					ProductionStringDetail psdetail=(ProductionStringDetail) node.getData();
					if (psdetail.getCumLength()!=null){
						thisConverter.setReferenceMappingField(ProductionStringDetail.class, "cumLength");
						thisConverter.setBaseValueFromUserValue(psdetail.getCumLength());
						cumLength_raw = thisConverter.getBasevalue();
					}
					
					if (psdetail.getLength()!=null){
						thisConverter.setReferenceMappingField(ProductionStringDetail.class, "length");
						thisConverter.setBaseValueFromUserValue(psdetail.getLength());
						length_raw = thisConverter.getBasevalue();
					}
					
					//calculate top depth 
					//top = set depth - cum length
					topDepth = setDepth_raw - cumLength_raw;
										
					thisConverter.setReferenceMappingField(ProductionStringDetail.class, "depthMdMsl");
					thisConverter.setBaseValue(topDepth);					
					PropertyUtils.setProperty(psdetail, "depthMdMsl", thisConverter.getConvertedValue());
					
					//calculate bottom depth 
					//bottom depth = top depth + current length
					bottomDepth  = topDepth + length_raw;
					
					thisConverter.setReferenceMappingField(ProductionStringDetail.class, "bottomDepthMdMsl");
					thisConverter.setBaseValue(bottomDepth);
					PropertyUtils.setProperty(psdetail, "bottomDepthMdMsl", thisConverter.getConvertedValue());					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(psdetail);
				}
			}
		}
	}

	/**
	 * Method to calculate bottom depth based on top depth and total length
	 * @param topDepth
	 * @param totalLength
	 * @return bottomDepth
	 */
	public static Double calculateBottomDepth(Double topDepth, Double totalLength, CustomFieldUom thisConverter) throws Exception {
		Double bottomDepth = 0.0;
		
		if (topDepth != null && totalLength != null) {

			thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, "depthMdMsl");
			thisConverter.addDatumOffset();
			thisConverter.setBaseValueFromUserValue(topDepth);
			topDepth = thisConverter.getBasevalue();
			
			thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, "totalLength");
			thisConverter.setBaseValueFromUserValue(totalLength);
			totalLength = thisConverter.getBasevalue();
						
			bottomDepth = topDepth + totalLength;
			
			thisConverter.setReferenceMappingField(ProductionStringDetailLog.class, "bottomDepthMdMsl");
			thisConverter.setBaseValue(bottomDepth);
			return thisConverter.getConvertedValue(true);
		}
		
		return bottomDepth;
	}
	
}