package com.idsdatanet.d2.pronet.productionFluid;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ProductionFluid;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


/* for sites which are using cascade lookup on stock name and unit (ticket 20110308-0915-scwong), please follow the field mapping as below:
 *  - map Stock Name column to @stockName
 *  - map Unit to stockCode (to keep the lookupRigStockUid in order to calculate the balance and cost) 
 *  - can refer to override commandbean in santos_csm_rig_stock under deployment folder and rigStock_alt_p1.xml
 */


public class ProductionFluidCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener{
	private String stockType;
	private Boolean isCombined = false;
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(ProductionFluid.class)) {
				CustomFieldUom thisConverter = new CustomFieldUom(targetCommandBeanTreeNode.getCommandBean(), ProductionFluid.class, "amt_start");
				
				if("stockCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					stockCodeChanged(request, commandBean, targetCommandBeanTreeNode, thisConverter);
				}
			}
		}
	}

	private void stockCodeChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node, CustomFieldUom thisConverter) throws Exception {
		if(Action.DELETE.equals(node.getAtts().getAction())) return; //why do we need to test for "delete" ? ssjong - 04/may/2009
		
		ProductionFluid thisProductionFluid = (ProductionFluid) node.getData();
		
		//show the correct unit as well
		String stockcode = thisProductionFluid.getStockCode();
		
		if (StringUtils.isNotBlank(stockcode)) {
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select storedUnit from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
			if (lstResult.size() > 0) thisProductionFluid.setStockUnit(lstResult.get(0).toString());
		}
		
	}	

	@Override
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
	}
	
	public void setType( String type ){
		this.stockType = type;
	}
	
	public void setIsCombined( Boolean type ){
		this.isCombined = type;
	}
	
	/*
	 * To identify item cost should auto populate from which table
	 * value can be set as LookupRigStock , default (if property not set) will get item cost from Lookup Product List Table 
	 */

}
