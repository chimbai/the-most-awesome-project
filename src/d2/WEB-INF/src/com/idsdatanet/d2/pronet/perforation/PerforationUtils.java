package com.idsdatanet.d2.pronet.perforation;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Perforation;
import com.idsdatanet.d2.core.model.PerforationGunConfiguration;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * All common utility related to Perforation
 * @author ekhoo
 *
 */
public class PerforationUtils {
	/**
	 * common method to populate all fields from PerforationGunConfiguration table as dynamic attribute
	 * @param node
	 * @param strCondition
	 * @throws Exception
	 */
	public static void populateGunDetailToDynaAttr(CommandBeanTreeNode node, String strCondition) throws Exception {		
		
		String strSql = "FROM PerforationGunConfiguration WHERE (isDeleted = false or isDeleted is null) and " + strCondition;	
		List <PerforationGunConfiguration> lstResult =  ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		if (lstResult.size() > 0){
			PerforationGunConfiguration thisPerforationGunConf = (PerforationGunConfiguration) lstResult.get(0);
			if (thisPerforationGunConf!=null) {
				Map<String, Object> perfGunFieldValues = PropertyUtils.describe(thisPerforationGunConf);
				for (Map.Entry entry : perfGunFieldValues.entrySet()) {
					node.getDynaAttr().put("perfGunConf." + entry.getKey(), entry.getValue());
				}
			}
		}
	}
	
	/**
	 * common method to create new PerforationGunConfiguration object 
	 * @param perforationUid
	 * @param commandBean
	 * @param node
	 * @param request
	 * @throws Exception
	 */
	public static void doCreateNewGunConfig(String perforationUid, CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception {		
		PerforationGunConfiguration perfGunConfig = new PerforationGunConfiguration();
		perfGunConfig.setPerforationUid(perforationUid);
		saveGunDetail(perfGunConfig, commandBean, node, request);
	}
	
	/**
	 * common method to save data on dynamic attribute to PerforationGunConfiguration table
	 * @param object
	 * @param commandBean
	 * @param node
	 * @param request
	 * @throws Exception
	 */
	public static void saveGunDetail(Object object, CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		if (object !=null){
			String prefix = "@perfGunConf.";
			for (String dynaAttrName : CommonUtil.getConfiguredInstance().getDynaAttrNames(commandBean, node, request, prefix, "Perforation")) {
				String dynaAttrFieldName = dynaAttrName.substring(prefix.length());
				if(PropertyUtils.isWriteable(object, dynaAttrFieldName))
					CommonUtil.getConfiguredInstance().setProperty(commandBean.getUserLocale(), object, dynaAttrFieldName, node.getDynaAttr().get(dynaAttrName.substring(1)));
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(object);
		}
	}
	
	/**
	 * common method to calculate interval with top and bottom depth
	 * @param topMd
	 * @param bottomMd
	 * @throws interval
	 */
	public static Double calculateInterval(Double topMd, Double bottomMd) throws Exception {
		Double interval = 0.0;

		if (bottomMd == null) bottomMd =0.0;
		if (topMd == null) topMd =0.0;
		interval = bottomMd - topMd;
		
		return interval;
	}
	/**
	 * common method to calculate shot total 
	 * @param topDepthMdMsl
	 * @param bottomDepthMdMsl
	 * @param shotDensity
	 * @param thisConverter
	 * @throws shotTotal
	 */
	public static Double calculateShotTotal(Double topDepthMdMsl, Double bottomDepthMdMsl, Double shotDensity , CustomFieldUom thisConverter)throws Exception {
		Double shotTotal = 0.0;
		Double bottomMd = 0.0;
		Double topMd = 0.0;
		Double gunShotDensity = 0.0;
		Double interval = 0.0;
		
		thisConverter.setReferenceMappingField(Perforation.class, "depthBottomMdMsl");
		thisConverter.setBaseValueFromUserValue(bottomDepthMdMsl); 
		thisConverter.addDatumOffset();
		bottomMd = thisConverter.getBasevalue();
		
		thisConverter.setReferenceMappingField(Perforation.class, "depthTopMdMsl");
		thisConverter.setBaseValueFromUserValue(topDepthMdMsl); 
		thisConverter.addDatumOffset();
		topMd = thisConverter.getBasevalue();
		
		interval = PerforationUtils.calculateInterval(topMd, bottomMd);
		
		thisConverter.setReferenceMappingField(PerforationGunConfiguration.class, "gunShotsSpacer");
		thisConverter.setBaseValueFromUserValue(shotDensity); 
		gunShotDensity = thisConverter.getBasevalue();
		//formula
		shotTotal = interval * gunShotDensity;
		
		return shotTotal;
	}
	/**
	 * common method to check if top depth more/less than bottom depth
	 * @param topMd
	 * @param bottomMd
	 * @param thisConverter
	 * @throws display
	 */
	public static Boolean showBottomDepthMessage(Double topMd, Double bottomMd, CustomFieldUom thisConverter) throws Exception {
		Boolean display = false;
		
		thisConverter.setReferenceMappingField(Perforation.class, "depthBottomMdMsl");
		thisConverter.setBaseValueFromUserValue(bottomMd); 
		thisConverter.addDatumOffset();
		bottomMd = thisConverter.getBasevalue();
		
		thisConverter.setReferenceMappingField(Perforation.class, "depthTopMdMsl");
		thisConverter.setBaseValueFromUserValue(topMd); 
		thisConverter.addDatumOffset();
		topMd = thisConverter.getBasevalue();
		
		if (bottomMd == null) bottomMd =0.0;
		if (topMd == null) topMd =0.0;
		if (topMd > bottomMd) display = true;
		
		return display;
	}

	/**
	 * to get max top depth perforation
	 * @param jobNumber
	 * @param wellboreUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public static Double getMinDepthTopPerforation(String jobNumber, String wellboreUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String sql = "select min(depthTopMdMsl) from Perforation where (isDeleted = false or isDeleted is null) and jobNumber=:jobNumber and wellboreUid=:wellboreUid and dailyUid=:dailyUid ";
		
		List lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"jobNumber", "wellboreUid", "dailyUid"}, new Object[] {jobNumber, wellboreUid, dailyUid}, qp);
		if (lstResults.size() > 0) {
			for (Object lstResult : lstResults) {
				if (lstResult != null) {	
					return Double.parseDouble(lstResult.toString());
				}
				
			}
		}
		
		return null;
	}
	
	/**
	 * to get min bottom depth perforation
	 * @param jobNumber
	 * @param wellboreUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public static Double getMaxDepthBottomPerforation(String jobNumber, String wellboreUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String sql = "select max(depthBottomMdMsl) from Perforation where (isDeleted = false or isDeleted is null) and jobNumber=:jobNumber and wellboreUid=:wellboreUid and dailyUid=:dailyUid ";
		
		List lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"jobNumber", "wellboreUid", "dailyUid"}, new Object[] {jobNumber, wellboreUid, dailyUid}, qp);
		if (lstResults.size() > 0) {
			for (Object lstResult : lstResults) {
				if (lstResult != null) {	
					return Double.parseDouble(lstResult.toString());
				}
				
			}
		}
		
		return null;
	}
	
	/**
	 * to get top depth perforation uid 
	 * @param jobNumber
	 * @param wellboreUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public static String getTopDepthPerforationUid(String jobNumber, String wellboreUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String sql = "select perforationUid from Perforation where (isDeleted = false or isDeleted is null) and jobNumber=:jobNumber and wellboreUid=:wellboreUid and dailyUid=:dailyUid order by depthTopMdMsl DESC";
		
		List lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"jobNumber", "wellboreUid", "dailyUid"}, new Object[] {jobNumber, wellboreUid, dailyUid}, qp);
		if (lstResults.size() > 0) {
			Object id = (Object) lstResults.get(0);
			if (id != null) {	
				return id.toString();
			}
		}
		
		return null;
	}
	
	public static void createPerforationBasedOnActivityRemarks(UserSelectionSnapshot userSelection, String moduleName, Activity thisActivity, Map<String, Object> fieldsMap) throws Exception {
		if (fieldsMap != null) {
			Perforation perforation = null;
			PerforationGunConfiguration perforationgun = null;
			
			String[] paramsFields = {"wellboreUid", "activityUid"};
			Object[] paramsValues = {thisActivity.getWellboreUid(), thisActivity.getActivityUid()};
			String strSql = "FROM Perforation WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND wellboreUid=:wellboreUid AND activityUid=:activityUid";
			List<Perforation> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields, paramsValues);
			
			if (ls.size() == 0) {
				// create Perforation
				perforation = new Perforation();
				perforation.setWellboreUid(thisActivity.getWellboreUid());
				perforation.setOperationUid(thisActivity.getOperationUid());
				perforation.setActivityUid(thisActivity.getActivityUid());

				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforation);

				if (perforation.getPerforationUid() != null) {
					// create PerforationGun
					perforationgun = new PerforationGunConfiguration();
					perforationgun.setWellboreUid(thisActivity.getWellboreUid());
					perforationgun.setPerforationUid(perforation.getPerforationUid());
					perforationgun.setOperationUid(thisActivity.getOperationUid());	 

					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforationgun);
				}
			} else {
				perforation = ls.get(0);
			}
			
			if ("perforation".equalsIgnoreCase(moduleName)) {
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()) {
					if (StringUtils.isNotBlank(fieldEntry.getKey()))
						PropertyUtils.setProperty(perforation, fieldEntry.getKey(), fieldEntry.getValue());
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforation);

			} else if ("perforationgunconfiguration".equalsIgnoreCase(moduleName)) {
				if (perforation != null) {
					String strSql2 = "FROM PerforationGunConfiguration WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND perforationUid=:perforationUid AND wellboreUid=:wellboreUid AND operationUid=:operationUid";
					paramsFields = new String[] {"perforationUid", "wellboreUid", "operationUid"};
					paramsValues = new Object[] {perforation.getPerforationUid(), thisActivity.getWellboreUid(), thisActivity.getOperationUid()};
					List<PerforationGunConfiguration> lsPerforationGun = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);

					if (lsPerforationGun.size() == 0) {
						perforationgun = new PerforationGunConfiguration();
						perforationgun.setWellboreUid(thisActivity.getWellboreUid());
						perforationgun.setPerforationUid(perforation.getPerforationUid());
						perforationgun.setOperationUid(thisActivity.getOperationUid());

						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforationgun);
					} else {
						perforationgun = lsPerforationGun.get(0);
					}

					for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()) {
						if (StringUtils.isNotBlank(fieldEntry.getKey()))
							PropertyUtils.setProperty(perforationgun, fieldEntry.getKey(), fieldEntry.getValue());
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(perforationgun);
				}
			}
		}
	}
}
