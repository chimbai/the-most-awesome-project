package com.idsdatanet.d2.pronet.productionStringLine;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionStringLineCascadeLookupHandler implements CascadeLookupHandler {
	private URI lookup = null;
	
	public void setLookup(String value) throws Exception {
		this.lookup = new URI(value);
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception{
		return LookupManager.getConfiguredInstance().getLookup(this.lookup.toString(), userSelection, null);
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,CommandBeanTreeNode node, UserSelectionSnapshot userSelection,HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		return null;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(this.lookup.toString(), userSelection, null);
		LinkedHashMap<String, LookupItem> lookupResult = new LinkedHashMap<String, LookupItem>();
		LinkedHashMap<String, LookupItem> xmlLookupResult = new LinkedHashMap<String, LookupItem>();
		List<Object[]> mapList = new ArrayList();
		List<Object[]> xmlMapList = new ArrayList();
		
		for (Map.Entry<String, LookupItem> lookupItem: lookupList.entrySet()) {
			xmlMapList.add(new Object[] {lookupItem.getKey(), lookupItem.getValue()});
		}

		for(Iterator i = xmlMapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!xmlLookupResult.containsKey(item.getKey())) {
				xmlLookupResult.put(item.getKey(), item);
			}
		}
		
		String sql = "select productionStringDetailUid, componentType from ProductionStringDetail where (isDeleted = false or isDeleted is null) and wellboreUid =:wellboreUid order by componentType";
		List<Object[]> listComponentResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "wellboreUid" }, new String[] { userSelection.getWellboreUid()});
		if (listComponentResults.size() > 0) {
			for (Object[] listComponentResult : listComponentResults) {
				if (listComponentResult[0] != null && listComponentResult[1] != null) {
					mapList.add(new Object[] {listComponentResult[0].toString(), new LookupItem(listComponentResult[0].toString(), listComponentResult[1].toString())});
				}
			}
		}
		
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!lookupResult.containsKey(item.getKey())) {
				lookupResult.put(item.getKey(), item);
			}
		}
		
		List<ProductionStringDetail> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From ProductionStringDetail where (isDeleted = false or isDeleted is null) and wellboreUid =:wellboreUid order by productionStringUid", "wellboreUid", userSelection.getWellboreUid());
		String previous_id = null;
		List selected = new ArrayList();
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		Map<String, LookupItem> source_lookup = lookupResult;
		LinkedHashMap<String, LookupItem> lookupItemSet = new LinkedHashMap<String, LookupItem>();
		
		if (previous_id == null){
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(null);
			cascadeLookupSet.setLookup(xmlLookupResult);
			result.add(cascadeLookupSet);
		}
		
		for(ProductionStringDetail psd: rs){
			if(previous_id != null && (! previous_id.equals(psd.getProductionStringUid()))){
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(previous_id);
				lookupItemSet = this.filter(source_lookup, selected);
				lookupItemSet.putAll(xmlLookupResult);
				cascadeLookupSet.setLookup(lookupItemSet);
				
				result.add(cascadeLookupSet);
				selected.clear();
			}
			
			selected.add(psd.getProductionStringDetailUid());
			previous_id = psd.getProductionStringUid();
		}

		if(previous_id != null){
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(previous_id);
			
			lookupItemSet = this.filter(source_lookup, selected);
			lookupItemSet.putAll(xmlLookupResult);
			cascadeLookupSet.setLookup(lookupItemSet);
			result.add(cascadeLookupSet);
		}

		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);
		
		return result_in_array;
	
	}
	
	public LinkedHashMap<String, LookupItem> filter(Map<String, LookupItem> source, List<String> selected){
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		// try to retain the original order -ssjong
		if(selected.size() > 0){
			for(String source_key: source.keySet()){
				if(selected.contains(source_key)) {
					result.put(source_key, source.get(source_key));
				}
			}
		}

		return result;
	}

}
