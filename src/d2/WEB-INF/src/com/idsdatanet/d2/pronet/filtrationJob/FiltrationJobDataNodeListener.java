package com.idsdatanet.d2.pronet.filtrationJob;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.FiltrationFluidProperties;
import com.idsdatanet.d2.core.model.FiltrationJob;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FiltrationJobDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		CustomFieldUom thisConverterVol = new CustomFieldUom(commandBean, FiltrationFluidProperties.class, "filtered_volume");
		CustomFieldUom thisConverterDe = new CustomFieldUom(commandBean, FiltrationFluidProperties.class, "diatomaceous_earth_mass");
		CustomFieldUom thisConverterCart = new CustomFieldUom(commandBean, FiltrationFluidProperties.class, "no_of_cartridge_used");
		Object obj = node.getData();
		
		if (obj instanceof FiltrationJob){
			FiltrationJob thisFiltrationJob = (FiltrationJob) obj;
			FiltrationJobUtil.saveFiltrationJob(thisConverterCart, thisConverterDe, thisConverterVol, thisFiltrationJob, node); 
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		  if ((object instanceof FiltrationFluidProperties)) {
			  
			  FiltrationFluidProperties ffp = (FiltrationFluidProperties)object;
			  Date start = ffp.getStartDatetime();
			  Date end = ffp.getStopDatetime();
			  
			  if ((start != null) && (end != null) &&
			  (end.before(start))) {
				  status.setContinueProcess(false, true);
				  status.setFieldError(node, "stopDatetime", "Stop Time cannot be earlier than Start Time.");
				  return;
			  }
		  }
	}
	
}
