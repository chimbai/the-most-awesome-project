package com.idsdatanet.d2.pronet.wellProductionTest;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.WellProductionTest;
import com.idsdatanet.d2.core.model.WellProductionTestDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.pronet.wellProductionTest.WellProductionTestUtil.TimeComparator;

public class WellProductionTestDataNodeListener extends EmptyDataNodeListener{

	private String operationCode;
	
	/* requested by santos csm for Flowback screen used by FRAC operation only (ticket:20110524-1800-ysim)
	 * if the operation code is CMPLT_FRAC (property value configured on commandbean), there will be auto population and 
	 * and calculation for some of the fields. 
	 */
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WellProductionTest) {
			
			WellProductionTest thisWellProductionTest = (WellProductionTest) object;
			
			if (thisWellProductionTest.getEndDate() != null && thisWellProductionTest.getStartDate() != null) {
				if (thisWellProductionTest.getEndDate().before(thisWellProductionTest.getStartDate())) {
					status.setContinueProcess(false, true);
					status.setFieldError(node, "endDate", "End date can not be earlier than start date.");
					return;
				}
			}
		}
		
		//for frac only
		if ("CMPLT_FRAC".equalsIgnoreCase(this.operationCode)) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			if (object instanceof WellProductionTestDetail) {
				WellProductionTestDetail thisWellProductionTestDetail = (WellProductionTestDetail) object;
							
				Date startTime = thisWellProductionTestDetail.getTestDateTime();
				Date endTime = thisWellProductionTestDetail.getTestEndDateTime();
				
				if (startTime != null && endTime != null) {
					if (endTime.before(startTime)) {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "testEndDateTime", "End time can not be earlier than start time.");
						return;
					}
								
					Double waterVolume = 0.0;
					Double waterRate = 0.0;
					//calculate water volume
					if (thisWellProductionTestDetail.getWaterVolumeFlowRate()!=null) {
						waterRate = thisWellProductionTestDetail.getWaterVolumeFlowRate();
						waterVolume = WellProductionTestUtil.calculateWaterVolume(waterRate, startTime, endTime, thisConverter);
						thisWellProductionTestDetail.setWaterVolume(waterVolume);
					}
				}
				
				//calculate choke C 
				Double orificeSize = 0.0;
				orificeSize =  thisWellProductionTestDetail.getOrificeCoefficient();
				if(orificeSize!=null)
					thisWellProductionTestDetail.setChokeCoefficient(WellProductionTestUtil.calculateChokeC(orificeSize));
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		this.updateValueToDate(node);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		this.updateValueToDate(node);	
	}
	

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		if ("CMPLT_FRAC".equalsIgnoreCase(this.operationCode)) {
			UserSession session = UserSession.getInstance(request);
			Object object = node.getData();	
			WellProductionTestUtil.populateValueFromReportDaily(object, session, null);			
		}	
	}

	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		//only for frac
		if ("CMPLT_FRAC".equalsIgnoreCase(this.operationCode)) {
			
			CustomFieldUom thisConverter = new CustomFieldUom(userSession.getUserLocale());
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			if ("WellProductionTestDetail".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()){	
				parent.sortChild("WellProductionTestDetail", new TimeComparator());	
				
				String wellProductionTestUid = WellProductionTestUtil.getParentUid(parent);
				
				WellProductionTestUtil.updateAvgGasAndWaterRate(userSession, wellProductionTestUid ,qp);
				WellProductionTestUtil.updateCumWaterVolFromDetail(status.getUpdatedNodes(), wellProductionTestUid, qp, thisConverter);
			}
		}
	}
	
	/**
	 * Common Method to update cum. cost and cum. water rate after re-calculate
	 * @param node
	 * @throws Exception
	 * */
	public void updateValueToDate(CommandBeanTreeNode node)throws Exception {
		
		Object object = node.getData();		
		if (object instanceof WellProductionTest) {
			WellProductionTest thisWellProductionTest = (WellProductionTest) object;
			WellProductionTestUtil.calcCumulativeValueToDate(thisWellProductionTest.getOperationUid(), "cost", "cumCost");
			WellProductionTestUtil.calcCumulativeValueToDate(thisWellProductionTest.getOperationUid(), "waterFlowRate", "cumWaterFlowRate");
		}
	}
	
	
	
	
}
