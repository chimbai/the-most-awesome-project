(function(window){
	D3.inherits(LessonTicketSearchResult,D3.AbstractFieldComponent);
	
	function LessonTicketSearchResult() {
		D3.AbstractFieldComponent.call(this);
		this._columns = [
		     			{
		     				type: "recordAction", align:"center"
		     			},{
		     				type:"field",
		     				field:"lessonTicketNumber",
		     				align:"left",
		     				width:"5%",
		     				title:"LL #"
		     			},{
		     				type:"field",
		     				field:"rp2Date",
		     				align:"left",
		     				width:"5%",
		     				title:"Date"
		     			},{
		     				type:"field",
		     				field:"eventLocation",
		     				align:"left",
		     				width:"10%",
		     				title:"Well Name"
		     			},{
		     				type:"field",
		     				field:"campaignName",
		     				align:"left",
		     				width:"10%",
		     				title:"Campaign Name"
		     			},{
		     				type:"field",
		     				field:"truncateDescrLesson",
		     				align:"left",
		     				width:"22%",
		     				title:"Event/Lesson/Opportunity Description"
		     			},{
		     				type:"field",
		     				field:"truncateInvestigationFinding",
		     				align:"left",
		     				width:"23%",
		     				title:"Investigation Findings"
		     			},{
		     				type:"field",
		     				field:"truncateDescrPostEvent",
		     				align:"left",
		     				width:"25%",
		     				title:"Closure"
		     			}
		     		]
	}
	LessonTicketSearchResult.prototype.getFieldEditor = function (){
		if (!this._fieldRenderer) {
			this.initiateContainer();
			this.retrieveLessonList();
			this._fieldRenderer = this._ResContainer;
		}
		return this._fieldRenderer;
	}
	LessonTicketSearchResult.prototype.initiateContainer = function(){
		if (!this._ResContainer){
			var containerTemplate = "<div class='listAllReport-container'>" +
	 				"<table width='100%' id='myTable' class='SimpleGridTable reportListing listable tablesorter'>" +
	 				"<thead>"+this.getColumnHeader()+"</thead>" +
	 				"<tbody class='lists-TBODY'></tbody></table>"+
	 				"</div>";
			var container = $(containerTemplate);
			this._ResContainer = container.get(0);

			this._tbody = container.find(".lists-TBODY").get(0);
			this._table = container.find(".listable").get(0);
			
			this._selAllchk = container.find(".cbx").get(0);
			this._selAllchk.onclick = this.checkAll_onclick.bindAsEventListener(this);
		}
	}
	LessonTicketSearchResult.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			if (col.type == "field"){
				var thConfig = "<th style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
				config+=thConfig;
			}else{
				var cb = "<input type='checkbox' class='cbx'/>";
				var thConfig = "<th style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+cb+"</th>";
				config+=thConfig;
			}	
		}
		return config;
	}
	LessonTicketSearchResult.prototype.retrieveLessonList = function (){
		var params = {};
		
		params._invokeCustomFilter = "flexLessonPullReportLessonSearchResult";
		params.keywordsFilter = this._node.getFieldValue("@keywordsFilter");
		params.regionProfile = this._node.getFieldValue("@regionProfile");
		params.advancedSearchString = this._node.getFieldValue("@advancedSearchString");
		params.searchMethod = this._node.getFieldValue("@searchMethod");
		
		this._node.commandBeanProxy.customInvoke(params,this.populateSelectionList,this);
	}
	LessonTicketSearchResult.prototype.populateSelectionList = function(response){
		var current_selected = this._node.getFieldValue(LessonTicketPullNodeListener.FIELD_SELECTED_LESSONS);
		var doc = $(response);
		var collection = [];
		doc.find("lessonTicket").each(function(index,item){
			var newItem = {};
			newItem.lessonTicketUid = $(item).find("lessonTicketUid").text();
			newItem.operationUid = $(item).find("operationUid").text();
			newItem.operationName = $(item).find("operationName").text();	
			newItem.lessonTicketNumber = $(item).find("lessonTicketNumber").text();
			newItem.rp2Date = $(item).find("rp2Date").text();
			newItem.descrLesson = $(item).find("descrLesson").text();
			newItem.truncateDescrLesson = $(item).find("descrLesson").text();
			newItem.investigationFinding = $(item).find("investigationFinding").text();
			newItem.truncateInvestigationFinding = $(item).find("investigationFinding").text();
			newItem.contactPerson = $(item).find("contactPerson").text();
			newItem.descrPostEvent = $(item).find("descrPostEvent").text();
			newItem.truncateDescrPostEvent = $(item).find("descrPostEvent").text();
			newItem.lessonTicketNumberPaddedString = $(item).find("lessonTicketNumberPaddedString").text();
			newItem.lessonTicketPage = $(item).find("lessonTicketPage").text();
			newItem.eventLocation = $(item).find("eventLocation").text();
			newItem.campaignName = $(item).find("campaignName").text();
			
			var record = {};
			record.uid = $(item).find("lessonTicketUid").text();
			record.data = newItem;

			collection.push(record);
		});
		this._selectionListItems = collection;
		D3.UI.removeAllChildNodes(this._tbody);
		this._checkboxes = [];
		this.selectedLessonTicket();
		this.updateSelectionList();
	}
	LessonTicketSearchResult.prototype.updateSelectionList = function (){
		for (var i=0;i<this._selectionListItems.length;i++){
			var row = this._selectionListItems[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
		
	}
	LessonTicketSearchResult.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{
			var cb = document.createElement("input");
			cb.setAttribute("uid", record.uid);
			cb.setAttribute("type", "checkbox");
			cb.value = record.uid;
			cb.onclick = this.check_onclick.bindAsEventListener(this);
			this._checkboxes[record.uid] = cb;
			
			$(columnContainer).append(cb);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":cb};
		}
	}
	LessonTicketSearchResult.prototype.checkAll_onclick = function(event){
		var target = event.target;
		var checked = $(target).get(0).checked;
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = checked;
		}
		this.selectedLessonTicket();
	}
	LessonTicketSearchResult.prototype.check_onclick = function(event){
		var target = event.target;
		var checked = $(target).get(0).checked;
		if (checked==false) this._selAllchk.checked = false;
		var uid = $(target).get(0).value;
		this.selectedLessonTicket();
	}
	
	LessonTicketSearchResult.prototype.selectedLessonTicket = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue(LessonTicketPullNodeListener.FIELD_SELECTED_LESSONS, id_list);
	}
	LessonTicketSearchResult.prototype.getRecord = function(uid){
		for (var i=0;i<this._selectionListItems.length;i++){
			var record = this._selectionListItems[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	LessonTicketSearchResult.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		if (fieldName === "lessonTicketNumber"){
			var renderer= D3.UI.createElement({
				tag:"a",
				text:value,
				css:"hyperlinkField",
				data:{
					lessonTicketPage:record.data.lessonTicketPage,
					operationUid:record.data.operationUid,
					lessonTicketUid:record.data.lessonTicketUid,
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = data.lessonTicketPage + "?&gotowellop=" + data.operationUid + "&gotoFieldName=lessonTicketUid&gotoFieldUid=" + escape(data.lessonTicketUid);	
						window.open(url, "_blank", "", true);
					}
				},
				context:this
			});
			return renderer;
		}else{
			var span = document.createElement("span");
			span.className = "fieldRenderer";
			$(span).text(value);
		}
		return span;
	}
	LessonTicketSearchResult.prototype.refreshFieldEditor = function (){}
	
	D3.inherits(LessonTicketCombinedSearch,D3.AbstractFieldComponent);
	
	function LessonTicketCombinedSearch() {
		D3.AbstractFieldComponent.call(this);
	}
	
	LessonTicketCombinedSearch.prototype.getFieldRenderer = function (){
		if (!this._fieldRenderer) {
			this.initiateContainer();
			this.retrieveSectionList();
			this._fieldRenderer = this._Container;
		}
		return this._fieldRenderer;
	}
	
	LessonTicketCombinedSearch.prototype.initiateContainer = function(){
		if (!this._Container){
			var containerTemplate = "<div class='listAllReport-container'>" +
					"<table width='100%'><tr><td style='font-weight: bold;'> " +
					"Text Search: <input class='inputtext'> </input>" +
					"<button class='DefaultButton confirm' style='margin: 3px;'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
					"</td>"+
					"<td align='right'>"+ 
					"<button class='DefaultButton search' style='margin: 3px;'>"+D3.LanguageManager.getLabel("button.search")+"</button>"+
					"<button class='DefaultButton reset' style='margin: 3px;'>"+D3.LanguageManager.getLabel("button.reset")+"</button>"+
					"</td></tr></table>"+
	 				"<table width='100%' class='SimpleGridTable reportListing'>" +
	 				"<tbody class='lists-TBODY'></tbody></table>"+
	 				"<button class='DefaultButton search2' style='float:right;margin: 3px;'>"+D3.LanguageManager.getLabel("button.search")+"</button>"+
				"</div>";
			var container = $(containerTemplate);
			this._Container = container.get(0);

			this._tbody = container.find(".lists-TBODY").get(0);	
			
			this._btnReset = container.find(".reset").get(0);
			this._btnReset.onclick = this.btnReset_onClick.bindAsEventListener(this);
			this._btnSearch = container.find(".search").get(0);
			this._btnSearch.onclick = this.btnSearch_onClick.bindAsEventListener(this);
			this._btnSearch2 = container.find(".search2").get(0);
			this._btnSearch2.onclick = this.btnSearch_onClick.bindAsEventListener(this);
			this._inputField = container.find(".inputtext").get(0);
			this._btnConfirm = container.find(".confirm").get(0);
			this._btnConfirm.onclick = this.searchButtonClicked.bindAsEventListener(this);
		}
	}
	LessonTicketCombinedSearch.prototype.btnReset_onClick = function(){
		for (var key in this._checkboxes) {
			for (var keys in this._checkboxes[key].values) {
				this._checkboxes[key].values[keys].checked = false;
			}
		}
	}
	LessonTicketCombinedSearch.prototype.btnSearch_onClick = function(){
		this.processSelectedItem();
		this.retrieveLessonList();
	}
	
	LessonTicketCombinedSearch.prototype.searchButtonClicked = function(){
		var newDate = new Date();
		this._node.setFieldValue("@keywordsFilter",this._inputField.value);
		this._node.setFieldValue("@newDate", newDate.toString());
		this._node.setFieldValue("@searchMethod", "Simple&" + newDate.toString());
		this.retrieveLessonList();
	}
	LessonTicketCombinedSearch.prototype.processSelectedItem = function(){
		var selectedStr = "";
		var selectedElements = "";
		var newDate = new Date();
		for (var key in this._checkboxes) {
			if (selectedStr=="") selectedStr += key + "=";
			else selectedStr += "&" + key + "=";
			var selectedVal = "";
			for (var keys in this._checkboxes[key].values) {
				if (this._checkboxes[key].values[keys].checked == true){
					selectedVal += (selectedVal==""?keys:","+keys);
					selectedElements += (selectedElements==""?keys:","+keys);
				}
			}
			selectedStr += selectedVal;
		}
		this.selectedElementsString = selectedStr;
		this._node.setFieldValue("@selectedElements", selectedElements);
		this._node.setFieldValue("@advancedSearchString",selectedStr);
		this._node.setFieldValue("@newDate", newDate.toString());
		this._node.setFieldValue("@searchMethod", "Advanced&"+newDate.toString());
	}
	LessonTicketCombinedSearch.prototype.clearSelection = function(){
		this._node.dispose();
	}
	LessonTicketCombinedSearch.prototype.retrieveLessonList = function(){
		var params = {};
		
		params._invokeCustomFilter = "flexLessonPullReportLessonList";
		params.keywordsFilter = this._node.getFieldValue("@keywordsFilter");
		params.regionProfile = this._node.getFieldValue("@regionProfile");
		params.advancedSearchString = this._node.getFieldValue("@advancedSearchString");
		params.selectedElements = this._node.getFieldValue("@selectedElements");
		params.searchMethod = this._node.getFieldValue("@searchMethod");
		this._node.commandBeanProxy.customInvoke(params,this.saveSearchResultsLoaded,this);
	}
	LessonTicketCombinedSearch.prototype.saveSearchResultsLoaded = function(response){
		if (window.parent){
			window.parent.tabsManager.select("lessonsearchresult");
		}
	}
	LessonTicketCombinedSearch.prototype.retrieveSectionList = function(){
		var params = {};
		
		params._invokeCustomFilter = "lessonTicketCategoryElementList";
		this._node.commandBeanProxy.customInvoke(params,this.populateSelectionList,this);
	}
	LessonTicketCombinedSearch.prototype.populateSelectionList = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("category").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.uid = data.attr("uid");
			newItem.name = data.attr("name");
			newItem.values = [];
			var length = $(item).find('element').length;
			for (i=0; i<length; i++) {
				var its = $(item).find('element').get(i);
				var newValue = {};
				newValue.uid = $(its).attr('uid');
				newValue.name = $(its).attr('name');
				if($(its).attr('selected') == "1") {
					newValue.selected = $(its).attr('selected');
				}
				newItem.values.push(newValue);
			};
			var record = {};
			record.data = newItem;

			collection.push(record);
		});
		this._selectionListItems = collection;
		D3.UI.removeAllChildNodes(this._tbody);

		this.updateSelectionList();
	}
	LessonTicketCombinedSearch.prototype.updateSelectionList = function(){
		this._checkboxes = {};
		for (var i=0;i<this._selectionListItems.length;i++){
			var row = this._selectionListItems[i];
			var tr = document.createElement("tr");
			tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			var td = document.createElement("td");
			td.setAttribute("width", "200px");
			td.className="SimpleGridDataCell colorSet";
			var span = document.createElement("span");
			span.className = "fieldRenderer";
			$(span).text(row.data.name);
			td.appendChild(span);
			var td2 = document.createElement("td");
			td2.className="SimpleGridDataCell";
			var ul = document.createElement("ul");
			ul.style.margin = 0;
			ul.style.padding = 0;
			this._checkboxes[row.data.uid] = {};
			this._checkboxes[row.data.uid].values = {};
			for (var j=0;j<row.data.values.length;j++){
				var rows = row.data.values[j];
		
				var li = document.createElement("li");
				li.setAttribute("title", rows.name);
				var cb = document.createElement("input");
				cb.setAttribute("id", rows.uid);
				cb.setAttribute("cid", row.data.uid);
				cb.setAttribute("type", "checkbox");
				cb.value = rows.uid;
				if (rows.selected=="1"){
					cb.checked = true;
				}else{
					cb.checked = false;
				}
				this._checkboxes[row.data.uid].values[rows.uid] = cb;
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", rows.uid);
				label.appendChild(document.createTextNode(rows.name));
				li.appendChild(label);
				ul.appendChild(li);
			}
			td2.appendChild(ul);
			tr.appendChild(td);
			tr.appendChild(td2);
		}
	}
	LessonTicketCombinedSearch.prototype.refreshFieldRenderer = function (){}
	
	
	D3.inherits(LessonTicketPullNodeListener,D3.DefaultReportListener);
	function LessonTicketPullNodeListener()
	{
		D3.DefaultReportListener.call(this);
	}
	LessonTicketPullNodeListener.prototype.customizeReportBtnLable = function(){
		D3.DefaultReportListener.GENERATE_REPORT_AND_DOWNLOAD = "Generate Report";
	}
	
	LessonTicketPullNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "combinedSearch"){
			return new LessonTicketCombinedSearch();
		} else if (id=="lesson_ticket_search_result") {
			return new LessonTicketSearchResult();
		}else{
			return LessonTicketPullNodeListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	LessonTicketPullNodeListener.FIELD_SELECTED_LESSONS = "@selectedLessonTicket";
	LessonTicketPullNodeListener.FIELD_SELECTED_REGION = "@regionProfile";
	LessonTicketPullNodeListener.FIELD_SELECTED_EMAIL = "@emailAddress";
	
	D3.CommandBeanProxy.nodeListener = LessonTicketPullNodeListener;
})(window);