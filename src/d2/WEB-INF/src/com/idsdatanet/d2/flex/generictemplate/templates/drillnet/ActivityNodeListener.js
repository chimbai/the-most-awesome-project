(function(window){
	
	D3.inherits(ActivityUnwantedEventLinkField,D3.AbstractFieldComponent);
	function ActivityUnwantedEventLinkField()
	{
		D3.AbstractFieldComponent.call(this);
	}
	
	ActivityUnwantedEventLinkField.prototype.dispose = function()
	{
		ActivityUnwantedEventLinkField.uber.dispose.call(this);
		this._linkButton = null;
		this._editor = null;
	}
	
	ActivityUnwantedEventLinkField.prototype.getFieldRenderer = function() {
		if(!this._linkButton){
			
			this._linkButton = document.createElement("a");
			this._linkButton.appendChild(document.createTextNode("Add New Event"));
			//this._linkButton.setStyle("color", "red");
			//this._linkButton.setStyle("textDecoration", "underline");
			this._linkButton.onclick = this.pageRedirect.bindAsEventListener(this);
		}
		
		return this._linkButton;
	}
	
	ActivityUnwantedEventLinkField.prototype.getFieldEditor = function() {
		if (this._editor == null) {
			this._editor = document.createElement("span");
			this.setTextAlign(this._editor);
			//this._editor.setStyle("color","red");
			this._editor.appendChild(document.createTextNode("Unwanted Event only can be added after saving this record"));
		}

		return this._editor;
	}
	
	
	ActivityUnwantedEventLinkField.prototype.pageRedirect = function(event)
	{
		var redirect_url = this._node.commandBeanProxy.baseUrl + "unwantedevent.html?activityId=" + escape(this._node.getFieldValue("activityUid"));
		var redirect_msg ="Creating unwanted event...";
		this._node.commandBeanProxy.pageRedirect(redirect_url, redirect_msg);
	}
	
	
	D3.inherits(ActivityCustomTimeField,D3.TimeField);
	function ActivityCustomTimeField()
	{
		D3.TimeField.call(this);
	}
	
	ActivityCustomTimeField.prototype.getMinutes = function(field){
		var val = this._node.getFieldValue(field);
		val = val.substring(0,val.indexOf("GMT"));
		var date = new Date(val);
		if(isNaN(date.getTime())) return 0;
		
		if(date.getHours() == 23 && date.getMinutes() == 59 && date.getSeconds() == 59){
			return 24 * 60;
		}else{
			return (date.getHours() * 60) + date.getMinutes();
		}
	}
	
	ActivityCustomTimeField.prototype.onTimeChanged = function(selectedHour, selectedMinute) {
		var fieldName = this._fieldName;
		var startMinutes = 0;
		var endMinutes = 0;
	
		if (fieldName == "endDatetime") {
			endMinutes = (Number(selectedHour) * 60) + Number(selectedMinute);
			startMinutes = this.getMinutes("startDatetime");
		}else if(fieldName == "startDatetime"){
			startMinutes = (Number(selectedHour) * 60) + Number(selectedMinute);
			endMinutes = this.getMinutes("endDatetime");
		}else{
			return;
		}
		
		var duration = (endMinutes - startMinutes) / 60;
		if(duration < 0){
			this._node.setFieldValue("activityDuration", "");
		}else{
			this._node.setFieldValue("activityDuration", duration.toString());
		}
	};
	
	D3.inherits(ActivityDurationCustomField,D3.AbstractFieldComponent);
	function ActivityDurationCustomField()
	{
		D3.AbstractFieldComponent.call(this);
		this._editor = null;
	}
	
	ActivityDurationCustomField.prototype.dataChangeListener = function(event) {
		if (event.currentTarget == this._editor) {
			this.setFieldValueNow();
		}
	
	}
	ActivityDurationCustomField.prototype.getFieldEditor = function(){
		if (!this._editor) {
			this._editor = document.createElement("input");
			D3.UI.addClass(this._editor, "d3-textfield");
			this.setTextAlign(this._editor);
			this._editor.onblur = this.dataChangeListener.bindAsEventListener(this);
		}
		
		return this._editor;
	}
	
	ActivityDurationCustomField.prototype.refreshFieldEditor = function(){
		this._editor.value = this.getFieldValue();
		//this._editor.errorString = "";
	}
	
	ActivityDurationCustomField.prototype.setFieldValueNow = function() {
		this.setFieldValue(this._editor.value);
	}
	
	D3.inherits(ActivityNodeListener,D3.EmptyNodeListener);
	
	function ActivityNodeListener()
	{
		
	}
	ActivityNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id == "unwantedEventLink"){
			return new ActivityUnwantedEventLinkField();
		}else if (id=="ActivityCustomTimeField") {
			return new ActivityCustomTimeField();
		}else if (id == "activityDurationCustomField") {
			return new ActivityDurationCustomField();
		}
	}
	
	ActivityNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		
		var maxEndDateTime = null;
		var maxEndDateTime2 = null;
		var actObj = {};
	
		var fieldToCopy = ["classCode","phaseCode","taskCode","rootCauseCode","depthMdMsl","holeSize","jobTypeCode","businessSegment","sections","incidentNumber","incidentLevel","ofInterest"];
		
		var nextActs = parent.children["NextDayActivity"];
		var i=0;
		var j=0;
		if(node.simpleClassName() == "Activity")
		{
			var actNodes = parent.children["Activity"];
			
			for (i=0; i<actNodes.length;i++) 
			{
				var actNode = actNodes[i];
				var endDateTime = actNode.getFieldValue("endDatetime");
				var endDateTimeCon = null;
				if (endDateTime) {
					endDateTimeCon = endDateTime.substring(0,endDateTime.indexOf("GMT"));
				}
				if (endDateTimeCon != null) {
					var strEndCon = new Date(endDateTimeCon).format("hhnnss");
					var strmaxEnd = new Date(maxEndDateTime).format("hhnnss");
					if (!maxEndDateTime || (strEndCon >strmaxEnd)) {
						maxEndDateTime = endDateTimeCon;
					}
				}
				
				for (var j=0;j<fieldToCopy.length;j++)
				{
					node.setFieldValue(fieldToCopy[j],actNode.getFieldValue(fieldToCopy[j]));
				}
			}			
		}else{
			if(nextActs.length < 1)
			{
				var actNodes = parent.children["Activity"];
				for (i=0; i<actNodes.length;i++) 
				{
					var actNode = actNodes[i];
					for (var j=0;j<fieldToCopy.length;j++)
					{
						actObj[fieldToCopy[j]] = actNode.getFieldValue(fieldToCopy[j]);
					}
				}
			}
		}
		
		for (i=0; i<nextActs.length;i++) 
		{
			var actNode = nextActs[i];
			var endDateTime2 = actNode.getFieldValue("endDatetime");
			var endDateTimeCon2 = null;
			if (endDateTime2 != null) {
				endDateTime2.substring(0,endDateTime2.indexOf("GMT"));
			}
			
			if (endDateTimeCon2 != null) {
				var strEndCon = new Date(endDateTimeCon2).format("hhnnss");
				var strmaxEnd = new Date(maxEndDateTime2).format("hhnnss");
				if (!maxEndDateTime2 || (strEndCon > strmaxEnd)) {
					maxEndDateTime2 = endDateTimeCon2;
				}
			}
			
			if(node.simpleClassName() == "NextDayActivity")
			{
				for (var j=0;j<fieldToCopy.length;j++)
				{
					node.setFieldValue(fieldToCopy[j],actNode.getFieldValue(fieldToCopy[j]));
				}
			}
		}
			
		if(node.simpleClassName() == "NextDayActivity" && nextActs.length < 1)
		{
			for (var j=0;j<fieldToCopy.length;j++)
			{
				if (actObj[fieldToCopy[j]])
					node.setFieldValue(fieldToCopy[j],actObj[fieldToCopy[j]]);
			}
		}
			
		if (node.simpleClassName() == "Activity" && maxEndDateTime) {
			node.setFieldValue("startDatetime", maxEndDateTime+"GMT+0000");
			node.setFieldValue("endDatetime", maxEndDateTime+"GMT+0000");
		}
		
		if (node.simpleClassName() == "NextDayActivity" && maxEndDateTime2) {
			node.setFieldValue("startDatetime", maxEndDateTime2+"GMT+0000");
			node.setFieldValue("endDatetime", maxEndDateTime2+"GMT+0000");
		}	
	}
	
	ActivityNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration)
	{
		var btn = null;
		if (id == "add_next_day_activity" && parentNode.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "NextDayActivity")) {
			btn = document.createElement("button");
			$(btn).text(buttonDeclaration.label);
			return btn;
		}
		 
		return null;
	}
	ActivityNodeListener.prototype.customButtonTriggered = function(parentApplication, node, id, button)
	{
		if (id == "add_next_day_activity" ) {

			var nextDayDailyUid = node.commandBeanProxy.rootNode.getFieldValue("@nextDayDailyUid");
			var nextDayAvailable = node.commandBeanProxy.rootNode.getFieldValue("@nextDayAvailable");
			
			if (nextDayAvailable == "0" && nextDayDailyUid == "") {
				node.addNewChildNodeOfClass("NextDayActivity",true,true);
			} else if (nextDayAvailable == "0" && nextDayDailyUid != "") {
				var param = {};
				param.gotoday = nextDayDailyUid;
				param.newActivityRecordInputMode = "1";
				var url = D3.Common.appendQueryString(node.commandBeanProxy.baseUrl + "activity.html",param);
				node.commandBeanProxy.goToUrl(url,null);
			}
		}
		if (id == "auto_populate_remark" ) {
			node.commandBeanProxy.submitForServerSideProcess(node);
		}
	}
	D3.CommandBeanProxy.nodeListener = ActivityNodeListener;
	
})(window);