(function(window){
	
	// DepotSchedulerEvent
	D3.inherits(DepotSchedulerEvent, D3.Event);
	function DepotSchedulerEvent(name, nodeUid) {
		this.name = name;
		this.nodeUid = nodeUid;
	}
	
	DepotSchedulerEvent.getServerChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_SERVER_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getPolicyChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_POLICY_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getProjectChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getSiteChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_SITE_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getWellChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_WELL_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getWellboreChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_WELLBORE_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getOperationChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_OPERATION_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getContractorChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_CONTRACTOR_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.DEPOT_SERVER_CHANGED = "depotServerChanged";
	DepotSchedulerEvent.DEPOT_POLICY_CHANGED = "depotPolicyChanged";
	DepotSchedulerEvent.DEPOT_PROJECT_CHANGED = "depotProjectChanged";
	DepotSchedulerEvent.DEPOT_SITE_CHANGED = "depotSiteChanged";
	DepotSchedulerEvent.DEPOT_WELL_CHANGED = "depotWellChanged";
	DepotSchedulerEvent.DEPOT_WELLBORE_CHANGED = "depotWellboreChanged";
	DepotSchedulerEvent.DEPOT_OPERATION_CHANGED = "depotOperationChanged";
	DepotSchedulerEvent.DEPOT_CONTRACTOR_CHANGED = "depotContractorChanged";

	window.D3.DepotSchedulerEvent = DepotSchedulerEvent;
	

	// SchedulerEventDispatcher	
	D3.inherits(SchedulerEventDispatcher, D3.Events);
	function SchedulerEventDispatcher() {
		this._commandBean = null;
	}
	
	SchedulerEventDispatcher.prototype.fireServerChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getServerChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.firePolicyChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getPolicyChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireProjectChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getProjectChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireSiteChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getSiteChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireWellChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getWellChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireWellboreChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getWellboreChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireOperationChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getOperationChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireContractorChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getContractorChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireLoadStartedEvent = function() {
		this._commandBean.dispatchEvent(new D3.CommandBeanProxyEvent(D3.CommandBeanProxyEvent.LOAD_STARTED, null));
	}
	
	SchedulerEventDispatcher.prototype.fireLoadCompletedEvent = function() {
		var event = new D3.CommandBeanProxyEvent(D3.CommandBeanProxyEvent.LOAD_COMPLETED, null);
		event.operationType = D3.CommandBeanProxyEvent.OPERATION_TYPE_LOAD;
		this._commandBean.dispatchEvent(event);
	}
	
	window.D3.SchedulerEventDispatcher = SchedulerEventDispatcher;
	
})(window);
