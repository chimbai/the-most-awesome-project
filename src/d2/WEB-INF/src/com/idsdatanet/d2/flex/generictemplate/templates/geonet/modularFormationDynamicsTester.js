(function(window){
	
	D3.inherits(ModularFormationDynamicsTesterGraphLinkField, D3.AbstractFieldComponent);
	
	function ModularFormationDynamicsTesterGraphLinkField(config) {
		this._linkConfig = config;
		D3.AbstractFieldComponent.call(this);
	}
	
	ModularFormationDynamicsTesterGraphLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel(this._linkConfig.linkLabel),
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel(this._linkConfig.linkLabel)
				},
				callbacks:{
					onclick:function(event) {
					var _graphImage = D3.UI.createElement({
						tag:"img",
						attributes:{
							// w x h from flash equivalent
							width:"640",
							height:"480",
							src: this._node.commandBeanProxy.baseUrl + "graph.html?type="+ this._linkConfig.graphType +"&size=medium&uid=" + escape(this._node.getFieldValue("modularFormationDynamicsTesterUid")) + "&t=" + new Date().getTime()
						},
						context:this
					});
					var _popUpDiv = "<div></div>";
					this._popUpDialog = $(_popUpDiv);
					this._popUpDialog.append(_graphImage);
					this._popUpDialog.dialog({
						height:"auto",
						width:"660", // same as flash equivalent
						resizable:false,
						modal:true,
						title:D3.LanguageManager.getLabel(this._linkConfig.linkLabel)
					});
					}
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
	}

	ModularFormationDynamicsTesterGraphLinkField.prototype.refreshFieldRenderer = function (){
	}
	
	D3.inherits(ModularFormationDynamicsTesterNodeListener,D3.EmptyNodeListener);
	
	function ModularFormationDynamicsTesterNodeListener() { }
	
	
	ModularFormationDynamicsTesterNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "hydrostatic_pressure_graph_link"){
			var config = {
					graphType:"hydrostatic_pressure",
					linkLabel:"Hydrostatic Pressure Graph",	
			};
			return new ModularFormationDynamicsTesterGraphLinkField(config);		
		}else if(id == "formation_pressure_graph_link"){
			var config = {
					graphType:"formation_pressure",
					linkLabel:"Formation Pressure Graph",	
			};
			return new ModularFormationDynamicsTesterGraphLinkField(config);
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ModularFormationDynamicsTesterNodeListener;
	
})(window);