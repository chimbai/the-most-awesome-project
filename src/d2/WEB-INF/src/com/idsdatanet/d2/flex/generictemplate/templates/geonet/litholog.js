(function(window){
	
	D3.inherits(ColorTips,D3.AbstractFieldComponent);
	
	function ColorTips() {
		
	}
	ColorTips.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var tips = "wh = white \n" +
			"gy = grey \n " +
			"blk = black \n" +
			"rd = red \n" +
			"pk = pink \n" +
			"pu = purple \n" +
			"orng = orange \n" +
			"brn = brown \n" +
			"yel = yellow \n" +
			"grn = green \n" +
			"ol = olive \n" +
			"bl = blue \n" +
			"cl = colourless \n" +
			"multi = multicoloured \n" +
			"trans = translucent \n \n" +
			 
			"lt = light \n" +
			"dk = dark \n" +
			"mod = moderate \n" +
			"vgt = variegated";
			this._fieldRenderer = document.createElement("img");
			this._fieldRenderer.setAttribute("title", tips);
			this._fieldRenderer.setAttribute("src" , "./images/litholog/button_down.png");
		}
		return this._fieldRenderer;
	}
	ColorTips.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(FilterButton,D3.AbstractFieldComponent);
	
	function FilterButton() {
		
	}
	FilterButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var label = D3.LanguageManager.getLabel("label.link.refresh");
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				callbacks:{
					onclick:this.refreshButtonClicked
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	FilterButton.prototype.refreshButtonClicked = function() {
		var _this = this;
		if (validationChecking("@fromDepth", _this) && validationChecking("@toDepth", _this)){
			D3.UIManager.popup("Warning", "Invalid \"From Depth\" and \"To Depth\" value.");
		} else if (validationChecking("@fromDepth", _this)) {
			D3.UIManager.popup("Warning", "Invalid \"From Depth\" value.");
		} else if (validationChecking("@toDepth", _this)) {
			D3.UIManager.popup("Warning", "Invalid \"To Depth\" value.");
		} else {			
			var filter = this._node.getFieldValue("@fromDepth") + "," + this._node.getFieldValue("@toDepth") + ","+ this._node.getFieldValue("@mainLithology");
			this._node.setFieldValue("@filterButton",filter);
			this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@filterButton");
		}
	}

	function validationChecking(field, _this){		
		if (_this._node.modifiedFieldValuesMap[field] != null && _this._node.modifiedFieldValuesMap[field] != "" && !IsNumeric(_this._node.modifiedFieldValuesMap[field])){
			return true;
		} else {
			return false;
		}
	}
	
	FilterButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(LithologNodeListener,D3.EmptyNodeListener);
	
	/* ***************************************
	 	/^ match beginning of string
		-{0,1} optional negative sign
		\d* optional digits
		\.{0,1} optional decimal point
		\d+ at least one digit
		$/ match end of string
	 **************************************** */
	function IsNumeric(input){
	    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
	    return (RE.test(input));
	}
	
	function LithologNodeListener()
	{
		
	}
	
	LithologNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "filterButton"){
			return new FilterButton();
		} else if(id == "colorTips"){
			return new ColorTips();
		} else {
			return null;
		}
	}

	D3.CommandBeanProxy.nodeListener = LithologNodeListener;
})(window);