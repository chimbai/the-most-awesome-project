(function(window){
	D3.inherits(NpdReportNodeListener,D3.EmptyNodeListener);
	
	function NpdReportNodeListener()
	{
		
	}
	
	NpdReportNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		this._commandBeanProxy = commandBeanProxy;
		var newRootBtn = document.createElement("button"); 
		newRootBtn.className = "rootButtons";
		var labels = document.createTextNode(buttonDeclaration.label);
		newRootBtn.appendChild(labels);
		if (id == "npdPreview"){
			newRootBtn.onclick = this.previewReport.bindAsEventListener(this);
			return newRootBtn;
		} else if (id == "npdSendFile"){
			newRootBtn.onclick = this.sendReport.bindAsEventListener(this);
			return newRootBtn;
		} else if (id == "npdDownloadFile"){
			newRootBtn.onclick = this.downloadReport.bindAsEventListener(this);
			return newRootBtn;
		} else {
			return null;
		}
	}

	NpdReportNodeListener.prototype.previewReport = function(event) {
		this.validateAndLoad("preview");
	}
	NpdReportNodeListener.prototype.sendReport = function(event) {
		this.validateAndLoad("send");
	}
	NpdReportNodeListener.prototype.downloadReport = function(event) {
		this.validateAndLoad("download");
	}
	
	NpdReportNodeListener.prototype.validateAndLoad = function(componentId) {
		var unmappedActExists = this._commandBeanProxy.rootNode.getFieldValue("@unmappedActivityExists")
		this._service = this._commandBeanProxy.rootNode.getFieldValue("@service")
		this._versionKind = this._commandBeanProxy.rootNode.getFieldValue("@versionKind")
		if (unmappedActExists == "true") {
			alert("There are Activity codes which have no valid proprietary code mapping.\n\nPlease select the correct Main Op / Sub Op codes for each Activity.");
		} else if (this._versionKind == null || this._versionKind == "") {
			alert("Please select a Report Version.");
		} else {
			var params = {};
			params._invokeCustomFilter = "NpdReport";
			params.action = componentId;
			params.versionKind = this._versionKind;
			if (componentId=="download"){
				this.download();
			}else{
				this._commandBeanProxy.customInvoke(params,this.showPopup,this,"text");
			}
		}
	}
	NpdReportNodeListener.prototype.download = function(){
		var npdDownloadForm = document.getElementById("npdDownloadForm");
		if (!npdDownloadForm){
		var formSubmit = "<form id='npdDownloadForm' name='npdForm' action='npdreport.html' method='post'>" + 
			"<input type='hidden' name='action' value='download' />" +
			"<input type='hidden' name='_invokeCustomFilter' value='__ie.npd.export.customFilter' />" +
			"<input type='hidden' name='__mainformsubmission' value='1' />" +
			"<input type='hidden' name='versionKind' value='"+this._versionKind+"' />" +
		"</form>";
			npdDownloadForm = $(formSubmit);
			$('body').append(npdDownloadForm);
			npdDownloadForm.submit();
		}else{
			$(npdDownloadForm).submit();
		}
	}
	NpdReportNodeListener.prototype.showPopup = function(response){
		var npdTemplate = "<div>" +
		"</div>";
		var npdTemplate = $(npdTemplate);
		npdTemplate.append($(response));
		this._npdTemplate  = npdTemplate.get(0);
		$(this._npdTemplate).dialog({
			dialogClass: 'no-close ui-dialog-titlebar-close' ,
			closeonEscape:false,
			modal:true, 
			width:"900", 
			height:"600", 
			resizable:false,
			closable:false,
			open: function(event, ui) { 
	            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
	        },
			buttons: [
		           {
		              text: D3.LanguageManager.getLabel("button.close"),
		              click: function() {
		                $( this ).dialog( "close" );
		              }
		            }
		          ]
		});
		$(".ui-dialog-titlebar").hide();
	}
	
	D3.CommandBeanProxy.nodeListener = NpdReportNodeListener;
	
})(window);