(function(window) {
	function WellSummary(container, topContainer){
		this.commandBeanProxy = new D3.CommandBeanProxy();
		this._container = container;
		this.topContainer = topContainer;
		
	};
	WellSummary.prototype.createRootButtons = function() {
		new D3.RootButtons(this.commandBeanProxy, this, this.topContainer);
	};
	WellSummary.prototype.init = function(data){
		this.commandBeanModel = new D3.CommandBeanModel(data.template);
		this.createRootButtons();
		this.commandBeanProxy.commandBeanModel = this.commandBeanModel;
		this.commandBeanProxy.initiate(data);
		this.commandBeanProxy._reloadHtmlPageAfterPageCancel = true;
		this.commandBeanProxy.addEventListener(D3.CommandBeanProxyEvent.LOAD_STARTED, this.loadStarted, this);
		this.commandBeanProxy.addEventListener(D3.CommandBeanProxyEvent.LOAD_COMPLETED, this.loadCompleted, this);
		this._RootNode = this.commandBeanProxy.rootNode;
		this._Operation = this.commandBeanProxy.rootNode.children['Operation'].getItems()[0];
		this._operationUid = this._Operation.getFieldValue("operationUid");
		this._zRtwellhead = this._Operation.getFieldValue("zRtwellhead");
		this._hasPs = this._Operation.getFieldValue("@hasPs");
		
		this._Bharun = this._Operation.children['Bharun'].getItems();
		this._CasingSection = this._Operation.children['CasingSection'].getItems();
		this._CementJob = this._Operation.children['CementJob'].getItems();
		this._Core = this._Operation.children['Core'].getItems();
		this._DrillStemTest = this._Operation.children['DrillStemTest'].getItems();
		this._Formation = this._Operation.children['Formation'].getItems();
		this._formationNodeMapping = {};
		D3.forEach(this._Formation,function(formation){
			var formationUid = formation.getFieldValue("formationUid").replace(".",""); 	
			this._formationNodeMapping[formationUid] = formation;
		},this);
		this._GasReadings = this._Operation.children['GasReadings'].getItems();
		this._WirelineRun = this._Operation.children['WirelineRun'].getItems();

		this._imgpath = "images/formation";
		this._imageHeight = this._RootNode.getFieldValue("@imageHeight");
		var casingCount = this._RootNode.getFieldValue("@casingCount");
		this._wellDepth = this._RootNode.getFieldValue("@wellDepth");
		this._formationFirstDepth = this._RootNode.getFieldValue("@formationFirstDepth");
		this._formationDepthUOM = this.commandBeanProxy.getUomSymbol("Formation", "sampleTopMdMsl");
		this._casingCount = parseInt(casingCount) - 1;
		this._casingHeadWidth = (parseInt(this._casingCount) * 20) + 70;
		this._datumReferencePoint = this._RootNode.getFieldValue("@datumReferencePoint");
		this._datumUOM = this.commandBeanProxy.getUomSymbol("OpsDatum", "reportingDatumOffset");
		this._datumLabel = this._RootNode.getFieldValue("@datumLabel");
	}
	WellSummary.prototype.redirect = function(url){
		this.commandBeanProxy.pageRedirect(url,D3.LanguageManager.getLabel("page.redirect"));
	}
	WellSummary.prototype.showhide = function(typename){
		for (i=1;i<50;i++)
		{
			var x = document.getElementById(typename + "text_" + i);
			if (x){
				x.style.display = (x.style.display =="none"?"":"none");
			}else{
				break;
			}
		}
	}
	WellSummary.prototype.render = function(){
		var wellSummaryContainerTemplate = "<table class='BaseNodesGroupContainerHeader' cellspacing='0' cellpadding='0'>" +
			"<tbody>" +
			"<tr><td><div class='BaseNodesGroupContainerHeaderText'>"+D3.LanguageManager.getLabel("welldetail.WellSummary")+"</div></td></tr>" +
			"</tbody>" +
			"</table>" +
			"<table cellspacing='0' cellpadding='0'>" + 
			"<tr valign='top'>" + 
				"<td class='main'> " + 
					"<table class='main' cellspacing='0' cellpadding='0'>" + 
						this.getLeftSectionContain() +
					"</table>" + 
				"</td>" + 
				"<td class='main'>" + 
					this.getRightSectionContain() +
				"</td>" + 
			"</tr>" + 
		"</table>";
		var noOperationTemplate = this.getNoOperationTemplate();
		var wellSummaryContainer = $(wellSummaryContainerTemplate);
		if (this._operationUid!=null){
			this._container.appendChild(wellSummaryContainer.get(0));
			this._container.appendChild(wellSummaryContainer.get(1));
		}
		else this._container.appendChild(noOperationTemplate);
			 
	};
	WellSummary.prototype.getRightSectionContain = function(){
		var rightSection = "<table cellspacing='0'>" +
			"<tr><td><b>"+D3.LanguageManager.getLabel("title.welldetail")+"</b><br>&nbsp;</td></tr>" +
			"<tr><td>&nbsp;</td></tr>" +
		"</table>" +
		this.getWellDetails() +
		this.getFormationLegend() +
		this.getCasingLegend() +
		this.getPSLegend() +
		"";
		return rightSection;
	}
	WellSummary.prototype.getPSLegend = function(){
		var psLegend = "<br/>" +	
		(this._hasPs!="0"?
			"<table style='border:solid 1px #000000; text-align:left;' cellspacing='0'>"+
				"<tr>"+
					"<td colspan='2'><B><U>"+D3.LanguageManager.getLabel("title.psLegend")+"</U></B></td>"+
				"</tr>"+
				"<tr valign='top'>"+
					"<td style='background-color:#00AE23;width:30px;border:solid 1px #000000;'>&nbsp;</td>	"+						
					"<td>"+
						"<b>"+this.showField(this._Operation, "@ps_desc")+"</b> "+
						"@ Depth: "+this.showField(this._Operation, "@ps_tallyLenght")+
					"</td>"+
				"</tr>"+
			"</table>"
		:"");
		return psLegend;
	}
	
	WellSummary.prototype.getCasingLegend = function(){
		var casingLegend = "<br/>" +	
		"<table class='legend-table' cellspacing='0'>" +
			"<tr>" +
				"<td colspan='2'><B><U>"+D3.LanguageManager.getLabel("title.casingLegend")+"</U></B></td>" +
			"</tr>";
			D3.forEach(this._CasingSection.reverse(),function(casingSection){
				var height = casingSection.getFieldValue("@height"); 	
				var height_before = casingSection.getFieldValue("@height_before");
				var casingOd = casingSection.getLookup("casingOd").getLookupItem(casingSection.getFieldValue("casingOd")).label;
				casingOd = casingOd.replace("\"","&quot;"); 
				var casingSummary = casingSection.getFieldValue("casingSummary").replace("\"","&quot;"); 
				var bgColor = casingSection.getFieldValue("@bgColor");
				var shoeTopMdMsl = casingSection.getFieldValue("shoeTopMdMsl");
				var shoeTopMdMslUom = casingSection.getUomSymbol("shoeTopMdMsl");
				if (height!=""){
					casingLegend = casingLegend +
					"<tr valign='top'>" +
						"<td style='background-color:"+bgColor+";width:30px;border:solid 1px #000000;'>&nbsp;</td>" +						
						"<td>" +
							"<b>"+casingOd+"</b>" +
							(shoeTopMdMsl!=""?
							"@ Shoe Depth : " + this.showField(casingSection, "shoeTopMdMsl"):"") +
						"</td>" +
					"</tr>";
				}
			},this);
		casingLegend = casingLegend + "</table>";
		return casingLegend;
	}	
	
	WellSummary.prototype.submitForm = function(rootAction, showAlert){
		if (! this.commandBeanProxy.submitForm(rootAction)) {
			if(showAlert) alert("You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
			return false;
		}else{
			return true;
		}
	}
	WellSummary.prototype.loadCompleted = function(event) {
		this.hidePopup();
	}
	WellSummary.prototype.loadStarted = function(event){
		this.showPopup(D3.LanguageManager.getLabel("page.wait"), D3.LanguageManager.getLabel("page.submit"));
	}
	WellSummary.prototype.showPopup = function(title, msg, showCloseButton) {
		if(this.$popup && this.$popup.dialog("isOpen")){
			this.$popup.dialog("destroy");
		}
		if(! this.popupContentDiv){
			this.popupContentDiv = document.createElement("div");
			this.popupContentDiv.style.padding = "15px 10px";	
		}
		while(this.popupContentDiv.hasChildNodes()) this.popupContentDiv.removeChild(this.popupContentDiv.lastChild);
		this.popupContentDiv.appendChild(document.createTextNode(msg));
		this.$popup = $(this.popupContentDiv).dialog({title: title, minHeight: 10, modal:true, resizable: false, closeOnEscape: false});
		if(! showCloseButton){
			$(".ui-dialog-titlebar-close", this.$popup.dialog("widget")).hide();
		}
	}
	WellSummary.prototype.hidePopup = function(){
		this.$popup.dialog("destroy");
	}
	WellSummary.prototype.onFormationChanged = function(event){
		var select = event.currentTarget;
		var uuid = $(select).attr('uuid');
		D3.forEach($(".f_image" + uuid),function(items){
			var xx = "";
			items.css({background: "url(" + this._imgpath + "/" + $(select).get(0).value + ")"});
		},this);
		var node = this._formationNodeMapping[uuid];
		node.setFieldValue("formationImage",$(select).get(0).value);
		node.setEditMode("true",true);
	}
	WellSummary.prototype.getFormationLegend = function(){
		
		var formationLegend = "<br/>" +
		"<table class='legend-table' cellspacing='0'>" +
			"<tr>" +
				"<td colspan='4'><B><U>"+D3.LanguageManager.getLabel("title.formationLegend")+"</U></B></td>" +
			"</tr>";
			D3.forEach(this._Formation,function(formation){
				var formationUid = formation.getFieldValue("formationUid").replace(".",""); ; 	
				var height = formation.getFieldValue("@height");
				var sampleTopMdMsl = formation.getFieldValue("sampleTopMdMsl");
				var formationName = formation.getFieldValue("formationName");
				var formationImage = formation.getFieldValue("formationImage");
				if (height!="") {
					formationLegend = formationLegend +
					"<tr valign='top'>";
						if (formation.isAllowedAction("edit", "Formation")){
							formationLegend = formationLegend +
							"<td>" +
							"<select id='"+formationUid+"' class='formationRecord' uuid='"+formationUid+"' onchange='window.D3.WS.onFormationChanged(event)'>";
							D3.forEach(formation.getLookup("formationImage")._items, function(item) {
								formationLegend = formationLegend +
								"<option value='"+item.data+"' " +(item.data==formationImage?"SELECTED":"")+ ">"+item.label+"</option>";
							});
							formationLegend = formationLegend +
							"</select>"+
							"</td>";
						}
						formationLegend = formationLegend +
						"<td style='background:url("+this._imgpath+"/"+formationImage+");width:20px;' id='"+formationUid+"' class='f_image"+formationUid+"'>" +
							"&nbsp;" +
						"</td>" +
						"<td nowrap>"+formationName+" (" + sampleTopMdMsl + " " + this._formationDepthUOM + ")</td>" +
					"</tr>";
				}
			},this);
		formationLegend = formationLegend + "</table>";
		return formationLegend;
	}

	WellSummary.prototype.getWellDetails = function(){
			var welldetail = "<table class='legend-table' cellspacing='0'>" +
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.WellName")+"</b></td>"+
				"<td>"+ this._Operation.getFieldValue("@wellName")+ "</td>"+
			"</tr>"+
			"<tr>" +
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Operator")+"</b></td>" +
				"<td>"+this.showField(this._Operation, "opCo")+ "</td>" +
			"</tr>" +
			"<tr>" +
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Location")+"</b></td>" +
				"<td>" +
					this._Operation.getFieldValue("@locParish")+ " " +
					this._Operation.getFieldValue("@state")+ " " +
					this._Operation.getFieldValue("@country")+ 
			"</tr>" +
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Permit")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@govPermitNumber")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.SpudDate")+"</b></td>"+
				"<td>";
			
			var sdate = this.showField(this._Operation, "spudDate") ;
			var d = new Date(parseInt(sdate)); 
			var sd = new Date(d.valueOf() + d.getTimezoneOffset() * 60000);
			var ddate = $.datepick.formatDate("dd M yyyy", sd);
			var spuddate = ddate + " " + sd.getHours()+ ":"+sd.getMinutes();
			welldetail = welldetail + (sdate!=""?spuddate:"") +
			"</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Status")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@currentPhase")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.RigName")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "rigInformationUid")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.RigRT")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@currentDatum")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Latitude")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@latDeg")+ "&deg;"+
					this.showField(this._Operation, "@latMinute")+"'"+
					this.showField(this._Operation, "@latSecond")+"&quot;"+
					this.showField(this._Operation, "@latNs")+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Longitude")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@longDeg")+ "&deg;"+
					this.showField(this._Operation, "@longMinute")+"'"+
					this.showField(this._Operation, "@longSecond")+"&quot;"+
					this.showField(this._Operation, "@longEw")+"</td>"+
			"</tr>"+
			
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Easting")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@gridEw")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Northing")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@gridNs")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.UTMZone")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@locUtmZone")+ "</td>"+
			"</tr>"+
			
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.Spheroid")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@spheroidType")+ "</td>"+
			"</tr>"+
			
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.CartDatum")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "@gridDatum")+ "</td>"+
			"</tr>"+
			"<tr>"+
				"<td><b>"+D3.LanguageManager.getLabel("welldetail.AFECode")+"</b></td>"+
				"<td>"+this.showField(this._Operation, "afeNumber")+ "</td>"+
			"</tr>"+
		"</table>";
		return welldetail;
	}
	WellSummary.prototype.convertUTCDateToLocalDate = function(date) {
		return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds()));
	};
	WellSummary.prototype.getLeftSectionContain = function(){
		var leftSection = "<tr style='text-align:center' valign='top'>" + 
		"<td><b>"+D3.LanguageManager.getLabel("title.formation")+"</b></td>" + 
		"<td><b>"+D3.LanguageManager.getLabel("title.casing")+"</b></td>" + 
		"<td><b>&nbsp;</b></td>" + 
		(this._Bharun.length>0?"<td class='td-headers'><a href=\"javascript:window.D3.WS.showhide('bha')\" title='Click to show/hide BHA Run#'><b>"+D3.LanguageManager.getLabel("title.bharun")+"</b></a></td>":"") +
		(this._CementJob.length>0?"<td class='td-headers'><b><a href=\"javascript:window.D3.WS.showhide('cement')\" title='Click to show/hide Cement'><b>"+D3.LanguageManager.getLabel("title.cement")+"</b></a></td>":"") +
		(this._WirelineRun.length>0?"<td class='td-headers'><a href=\"javascript:window.D3.WS.showhide('wireline')\" title='Click to show/hide Logging Run#'><b>"+D3.LanguageManager.getLabel("title.wireline")+"</b></a></td>":"") +
		(this._Core.length>0?"<td class='td-headers'><a href=\"javascript:window.D3.WS.showhide('corerun')\" title='Click to show/hide Core No.#'><b>"+D3.LanguageManager.getLabel("title.core")+"</b></a></td>":"") +
		(this._DrillStemTest.length>0?"<td class='td-headers'><a href=\"javascript:window.D3.WS.showhide('dst')\" title='Click to show/hide DST No.'><b>"+D3.LanguageManager.getLabel("title.dst")+"</b></a></td>":"") +
		(this._GasReadings.length>0?"<td class='td-headers'><b>"+D3.LanguageManager.getLabel("title.gas")+"</b></th>":"") + 
		"<td>&nbsp;</td>" + 
		"</tr>"+
		"<tr style='text-align:center' valign='bottom' >"+
		"<td style='padding:0px;' colspan='3'>"+
			"<table width='100%' cellspacing='0' cellpadding='0'>"+
				"<tr>"+
					"<td style='padding:0px;' align='right'>"+
						"<div class='casing-cap' style='width: "+this._casingHeadWidth+"px;'>&nbsp;</div>"+
					"</td>"+
				"</tr>"+
			"</table>"+
		"</td>"+
		"<td>&nbsp;</td>"+
		(this._Bharun.length>0?"<td>&nbsp;</td>":"") +
		(this._CementJob.length>0?"<td>&nbsp;</td>":"") +
		(this._WirelineRun.length>0?"<td>&nbsp;</td>":"") +
		(this._Core.length>0?"<td>&nbsp;</td>":"") +
		(this._DrillStemTest.length>0?"<td>&nbsp;</td>":"") +
		(this._GasReadings.length>0?"<td>&nbsp;</td>":"") +
		"<td>&nbsp;</td>"+
		"</tr>"+
		"<tr valign='top'>" +
			this.getFormationSection() +
			this.getCasingSection() +
			this.getFormationRightSection() +
			this.getBHASection() +
			this.getCementSection() +
			this.getWirelineRunSection() +
			this.getCoreSection() +
			this.getDrillStemTestSection() +
			this.getGasReadingsSection() +
			"<td>&nbsp;</td>"+
		"</tr>";
		return leftSection; 
	}
	WellSummary.prototype.getGasReadingsSection = function(){
		var GasReadingsSection = "";
		if (this._GasReadings.length>0){
			GasReadingsSection = GasReadingsSection +
			"<td align='center' class='section-td'>" +
				"<img src='graph.html?type=gas_graph_well_summary&size=medium&wellDepth="+ this._wellDepth + "' style='height:{$gas_depth}px;cursor:pointer;' border='0' onclick=\"window.D3.WS.redirect('gasreadings.html')\">" +
			"</td>";
		}
		return GasReadingsSection;
	}
	WellSummary.prototype.getDrillStemTestSection = function(){
		var DSTSection = "";
		if (this._DrillStemTest.length>0){
			var counter = 0;
			DSTSection = "<td align='center' class='section-td'>" +
				"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
					"<tr valign='top'>" +
						"<td style='width:5;'>&nbsp;</td>";
						D3.forEach(this._DrillStemTest,function(dst){
							var height = dst.getFieldValue("@height");
							var height_before = dst.getFieldValue("@height_before");
							var testNum = dst.getFieldValue("testNum");
							var dailyUid = dst.getFieldValue("dailyUid");
							if (height!=""){
								counter = parseInt(counter) + 1;
								DSTSection = DSTSection +
								"<td style='padding:0px;text-align:center;'>" +
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
										"<tr>" +
											"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;' colspan='2'>&nbsp;</td>" +
										"</tr>" +
										"<tr title='Run #"+testNum+"' onclick=\"window.D3.WS.redirect('drillstemtest.html?gotoday="+dailyUid+"')\">" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='padding:0px;text-align:center;height:"+height+"px;width:2px;' title='Run #"+testNum+"' colspan='2'>" +
												"<div id='dsttext_"+counter+"' style='overflow:visible;display:none;height:"+height+"px;'>"+testNum+"</div>" +
											"</td>" +
										"</tr>" +
									"</table>" +
								"</td>";
							}
						},this);
						DSTSection = DSTSection +
						"<td style='width:5;'>&nbsp;</td>"+
					"</tr>"+
				"</table>"+
			"</td>";
		}
		return DSTSection
	}
	WellSummary.prototype.getCoreSection = function(){
		var CoreSection = "";
		if (this._Core.length>0){
			var counter = 0;
			CoreSection = "<td align='center' class='section-td'>" +
				"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
					"<tr valign='top'>" +
						"<td style='width:5;'>&nbsp;</td>";
						D3.forEach(this._Core,function(core){
							var height = core.getFieldValue("@height");
							var height_before = core.getFieldValue("@height_before");
							var coreNumber = core.getFieldValue("coreNumber");
							var dailyUid = core.getFieldValue("dailyUid");
							if (height!=""){
								counter = parseInt(counter) + 1;
								CoreSection = CoreSection +
								"<td style='padding:0px;text-align:center;'>" +
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
										"<tr>" +
											"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;' colspan='2'>&nbsp;</td>" +
										"</tr>" +
										"<tr title='Run #"+coreNumber+"' onclick=\"window.D3.WS.redirect('core.html?gotoday="+dailyUid+"')\">" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='padding:0px;text-align:center;height:"+height+"px;width:2px;' title='Run #"+coreNumber+"' colspan='2'>" +
												"<div id='coreruntext_"+counter+"' style='display:none;overflow:visible;height:"+height+"px;'>"+coreNumber+"</div>" +
											"</td>" +
										"</tr>" +
									"</table>" +
								"</td>";
							}
						},this);
						CoreSection = CoreSection +
						"<td style='width:5;'>&nbsp;</td>" +
					"</tr>" + 
				"</table>" +
			"</td>";
		}
		return CoreSection; 
	}
	WellSummary.prototype.getWirelineRunSection = function(){
		var WirelineRunSection = "";
		if (this._WirelineRun.length>0){
			var counter = 0;
			WirelineRunSection = "<td align='center' class='section-td'>" +
				"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
					"<tr valign='top'>" +
						"<td style='width:5;'>&nbsp;</td>";
						D3.forEach(this._WirelineRun,function(wirelineRun){
							var height = wirelineRun.getFieldValue("@height");
							var height_before = wirelineRun.getFieldValue("@height_before");
							var runNumber = wirelineRun.getFieldValue("runNumber");
							var suiteNumber = wirelineRun.getFieldValue("@suite_number");
							var wirelineLogStringDescr = wirelineRun.getFieldValue("wirelineLogStringDescr");
							var wirelineSuiteUid = wirelineRun.getFieldValue("wirelineLogStringDescr");
							var wirelineRunUid = wirelineRun.getFieldValue("wirelineRunUid");
							if (height!=""){
								counter = parseInt(counter) + 1;
								WirelineRunSection = WirelineRunSection + 
								"<td style='padding:0px;text-align:center;'>" +
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
										"<tr>" +
											"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;' colspan='2'>&nbsp;</td>" +
										"</tr>" +
										"<tr title='Suite #"+suiteNumber+" Run #"+runNumber+", Tool: "+wirelineLogStringDescr+"' onclick=\"window.D3.WS.redirect('wirelinerun.html')\">" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>" +
											"<td style='padding:0px;text-align:center;height:"+height+"px;width:2px;' title='Suite #"+suiteNumber+" Run #"+runNumber+", Tool: "+wirelineLogStringDescr+"' colspan='2' nowrap>" +
												"<div id='wirelinetext_"+counter+"' style='display:none;overflow:visible;height:"+height+"px;'>"+suiteNumber+"/"+runNumber+"</div>" +
											"</td>"+
										"</tr>"+
									"</table>"+
								"</td>";
							}
						},this);
						WirelineRunSection = WirelineRunSection + 
						"<td style='width:5;'>&nbsp;</td>" +
					"</tr>" +
				"</table>" +
			"</td>";
		}
		return WirelineRunSection;
	}
	WellSummary.prototype.getCementSection = function(){
		var CementSection = "";
		if (this._CementJob.length>0){
			CementSection = "<td align='center' class='section-td'>";
				var counter = 0;
				CementSection = CementSection +
				"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
					"<tr valign='top'>" +
						"<td style='width:5;'>&nbsp;</td>";
						D3.forEach(this._CementJob,function(cementJob){
							D3.forEach(cementJob.children['CementStage'].getItems(),function(cementStage){
								var height = cementStage.getFieldValue("@height");
								var height_before = cementStage.getFieldValue("@height_before");
								var jobNumber = cementJob.getFieldValue("jobNumber");
								var stageNumber = cementStage.getFieldValue("stageNumber");
								var dailyUid = cementJob.getFieldValue("dailyUid");

								if (height!="") {
									counter = parseInt(counter) + 1;
									CementSection = CementSection +
									"<td style='padding:0px;text-align:center;'>" +
										"<table style='padding:0px;' cellspacing='0' cellpadding='0'>"+
											"<tr>"+
												"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;' colspan='2'>&nbsp;</td>"+
											"</tr>"+
											"<tr title='Job #"+jobNumber+" Stage #"+stageNumber+"' onclick=\"window.D3.WS.redirect('cementjob.html?gotoday="+dailyUid+"')\">"+
												"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>"+
												"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;'>&nbsp;</td>"+
												"<td style='padding:0px;text-align:center;height:"+height+"px;width:2px;' title='Job #"+jobNumber+" Stage #"+stageNumber+"' colspan='2' nowrap>"+
													"<div id='cementtext_"+counter+"' style='display:none;overflow:visible;height:"+height+"px;'>"+jobNumber+"/"+stageNumber+"</div>" +
												"</td>"+
											"</tr>"+
										"</table>"+
									"</td>";
								}
							},this);
						},this);
						CementSection = CementSection +
						"<td style='width:5;'>&nbsp;</td>" +
					"</tr>" + 
				"</table>" + 
			"</td>";
		}
		return CementSection;
	}
	WellSummary.prototype.getBHASection = function(){
		var BHASection = "";
		if (this._Bharun.length>0){
			BHASection = "<td align='center' class='section-td'>" +
				"<table style='padding:0px;border:none;' cellspacing='0' cellpadding='0'>" +
					"<tr valign=''top'>" + 
						"<td style='padding:0px;width:5;'>&nbsp;</td>";
						var counter = 0;
						D3.forEach(this._Bharun,function(bharun){
							var height = bharun.getFieldValue("@height");
							var height_before = bharun.getFieldValue("@height_before");
							var bhaRunNumber = bharun.getFieldValue("bhaRunNumber");
							var bhaType = bharun.getFieldValue("bhaType");
							var dailyUid = bharun.getFieldValue("@dailyUid");

							if (height!=""){
								counter = parseInt(counter) + 1;
								BHASection += "<td style='padding:0px;text-align:center;'>"+
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>";
										if (parseInt(height_before) > 1){
											BHASection = BHASection + "<tr>" +
												"<td style='padding:0px;height:"+ height_before +"px;border-bottom:solid 1px #000000;width:4px;cursor:none;' " +
													"colspan='2'>&nbsp;</td>" +
											"</tr>";
										}
										if (parseInt(height) > 15){
											BHASection = BHASection + "<tr title='BHA #" + bhaRunNumber + ", BHA Type:" + bhaType + "' onclick=\"window.D3.WS.redirect('bharun.html?gotoday="+dailyUid+"')\">" +
												"<td style='padding:0px;height:" + height + "px;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:2px;' title='BHA #" + bhaRunNumber + ", BHA Type: " + bhaType + "'>" +
													"<div style='overflow: hidden; padding:0px;height:" + height + "px;cursor:pointer;'>&nbsp;</div>"+
												"</td>" +
												"<td style='padding:0px;height:" + height + "px;border-bottom:solid 1px #000000;text-align:center;width:2px;' title='BHA #" + bhaRunNumber + ", BHA Type: " + bhaType + "'>" +
													"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>"+
												"</td>"+
												"<td style='padding:0px;text-align:center;width:2px;' title='BHA #" + bhaRunNumber + ", BHA Type: "+ bhaType +"' colspan='2' nowrap>"+
													"<div id='bhatext_" + counter + "' style='display:none;'>"+ bhaRunNumber +"</div>" +
												"</td>" +
											"</tr>";
										}else{
											if (parseInt(height) > 1 ){
												BHASection = BHASection +
												"<tr title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' onclick=\"window.D3.WS.redirect('bharun.html?gotoday="+dailyUid+"')\">" +
													"<td style='padding:0px;height:"+ height +"px;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>"+
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>"+
													"</td>"+
													"<td style='padding:0px;height:"+ height +"px;border-bottom:solid 1px #000000;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>" +
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>"+
													"</td>"+
													"<td style='font-size:1pt;padding:0px;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' colspan='2' nowrap>"+
														"&nbsp;" +
													"</td>"+
												"</tr>"+
												"<tr title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' onclick=\"window.D3.WS.redirect('bharun.html?gotoday="+dailyUid+"')\">" +
													"<td style='padding:0px;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>" +
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>" +
													"</td>" +
													"<td style='padding:0px;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>" +
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>" +
													"</td>" + 
													"<td style='padding:0px;text-align:center;width:2px;height:"+ height +"px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' colspan='2' nowrap>" +
														"<div id='bhatext_"+ counter +"' style='display:none;height:"+ height +"px;overflow:visible;'>"+ bhaRunNumber +"</div>"+
													"</td>" +
												"</tr>";
											}else{
												BHASection = BHASection +
												"<tr title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' onclick=\"window.D3.WS.redirect('bharun.html?gotoday="+dailyUid+"')\">" +
													"<td style=padding:0px;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>" +
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>" +
													"</td>" +
													"<td style='padding:0px;text-align:center;width:2px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"'>" +
														"<div style='overflow: hidden; padding:0px;height:"+ height +"px;cursor:pointer;'>&nbsp;</div>" +
													"</td>" +
													"<td style='padding:0px;text-align:center;width:2px;height:"+ height +"px;' title='BHA #"+ bhaRunNumber +", BHA Type:"+ bhaType +"' colspan='2' nowrap>" +
														"<div id='bhatext_"+ counter +"' style='display:none;height:"+ height +"px;overflow:visible;'>"+ bhaRunNumber +"</div>" +
													"</td>" +
												"</tr>";
											}
										}
									BHASection = BHASection +"</table>";
								BHASection = BHASection +"</td>";
							}
						},this);
						BHASection = BHASection +"<td style='padding:0px;width:5;'>&nbsp;</td>"+
							"</tr>"+
						"</table>"+
					"</td>";
		}
		return BHASection;
	}
	
	WellSummary.prototype.getFormationRightSection = function(){
		var formationRightSection = "<td align='center' style='border:none;padding:0px;' onclick=\"window.D3.WS.redirect('formation.html')\">" +
				"<table style='padding:0px;width:50px;' cellspacing='0' cellpadding='0'>";
				if (this._Formation.length>0){
					if (this._formationFirstDepth!=""){
						formationRightSection = formationRightSection + "<tr valign='top'>";
						if (this._datumReferencePoint=="WH"){
							formationRightSection = formationRightSection +
							(this._datumLabel!=""?
								"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +
									this._datumLabel +
								"</td> " 
							:"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +
									"<span style='overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:"+this._zRtwellhead+"px;text-align:right; border-top:solid 1px #000000;'>" + 
									"0.0 " + this._Operation.getUomSymbol("zRtwellhead") +
									"</span>" +
								"</td>");
						}else{
							formationRightSection = formationRightSection +
							(this._zRtwellhead!=""?
								"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +
									this._zRtwellhead +
								"</td>":
								"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._formationFirstDepth+"px; text-align:right;' nowrap>" +
									"<span style='overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:"+this._formationFirstDepth+"px;text-align:right; border-top:solid 1px #000000;'>" + 
									"0.0 " + this._Operation.getUomSymbol("zRtwellhead")+
									"</span>" + 
								"</td>");
						}
						formationRightSection = formationRightSection + "</tr>";
					}
					
					D3.forEach(this._Formation,function(formation){
						var formationUid = formation.getFieldValue("formationUid").replace(".",""); ; 	
						var height = formation.getFieldValue("@height");
						var sampleTopMdMsl = formation.getFieldValue("sampleTopMdMsl");
						var formationName = formation.getFieldValue("formationName");
						var formationImage = formation.getFieldValue("formationImage");
						if (height!=""){
							formationRightSection = formationRightSection +
							"<tr valign='top'>" +
								"<td style='cursor:pointer; padding:0px;height:"+height+"px;text-align:center;border-top:dotted 1px #000000;' id='"+formationUid+"' class='"+formationUid+"' title='"+formationName + " @ " + sampleTopMdMsl +" "+ this._formationDepthUOM + "'>" +
									"<div style='overflow: hidden; padding:0px;padding-top:0px; height:"+height+"px;text-align:center;background:url("+this._imgpath+"/"+formationImage+");' class='f_image"+formationUid+"'>&nbsp;</div>" +
								"</td>" +
							"</tr>";
						}
					},this);
				}	
				formationRightSection = formationRightSection +
				"</table>"+
			"</td>";
		return formationRightSection;
	}
	WellSummary.prototype.getCasingSection = function(){
		var casingSectionContain  = "<td align='center' style='background-color:#aaaaaa;padding:0px;'>" +
				"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
					"<tr valign='top'>";
					if (this._CasingSection.length>0){
						D3.forEach(this._CasingSection,function(casingSection){
							var height = casingSection.getFieldValue("@height"); 	
							var height_before = casingSection.getFieldValue("@height_before");
							var casingOd = casingSection.getLookup("casingOd").getLookupItem(casingSection.getFieldValue("casingOd")).label;
							casingOd = casingOd.replace("\"","&quot;"); 
							var casingSummary = casingSection.getFieldValue("casingSummary").replace("\"","&quot;"); 
							var bgColor = casingSection.getFieldValue("@bgColor");
							var shoeTopMdMsl = casingSection.getFieldValue("shoeTopMdMsl");
							var shoeTopMdMslUom = casingSection.getUomSymbol("shoeTopMdMsl");
							
							if (height!=""){
								casingSectionContain = casingSectionContain +
								"<td style='cursor:pointer; padding:0px;text-align:center;' onclick=\"window.D3.WS.redirect('casingsection.html')\">" +
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
										((parseInt(height_before) > 0)?
										"<tr>" +
											"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;width:7px;'>&nbsp;</td>" +
										"</tr>":"") +
										"<tr valign='bottom'>"+
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-right:solid 1px #000000; border-top:solid 1px #000000;width:7px; background-color:"+bgColor+"; text-align:right;'" +
												"title='"+casingOd +" @ " + (shoeTopMdMsl!=''?"shoe depth " + shoeTopMdMsl + " " +shoeTopMdMslUom:"") + "\n" +
												"Casing - " + casingSummary +"'><img src='images/hangleft.gif'></td>"+
										"</tr>"+
									"</table>"+
								"</td>";
							}
						},this);
						
						var psTallyHeight = 0;
						var psTallyLenght = 0;
						var tallyLenghtUOM = "";
						var psDesc = "";
						var psTallyLenght = 0;
						if (this._hasPs!="0"){
							psTallyHeight = this._Operation.getFieldValue("@ps_tallyHeight");
							psTallyLenght = this._Operation.getFieldValue("@ps_tallyLenght");
							tallyLenghtUOM = this.commandBeanProxy.getUomSymbol("ProductionStringDetail", "tallyLength");
							psDesc = this._Operation.getFieldValue("@ps_desc").replace("\"","&quot;");
							psTallyLenght = this._Operation.getFieldValue("@ps_tallyLenght");
						}
						var imageHeightx = (parseInt(this._imageHeight) - parseInt(psTallyHeight));
						casingSectionContain = casingSectionContain +
						"<td style='padding:0px;text-align:center;'>" +
							"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
								((this._hasPs!="0")?
									"<tr  valign='bottom'>" +
										"<td style='cursor:pointer;font-size:1pt; padding:0px;height:"+psTallyLenght+"px;width:7px;background-color:#00AE23' nowrap  " +
											"onclick=\"window.D3.WS.redirect('productionstring.html')\" "+
											"title='Production String @ "+psTallyLenght+" "+ tallyLenghtUOM +" ("+ psDesc +")'>&nbsp;</td>"+
										"<td style='height:"+psTallyHeight+"px;background-color:#ffffff;width:16px;'>&nbsp;</td>" +
										"<td style='cursor:pointer;font-size:1pt; padding:0px;height:"+psTallyLenght+"px;width:7px;background-color:#00AE23' nowrap " +
											"onclick=\"window.D3.WS.redirect('productionstring.html')\" " +
											"title='Production String @ "+psTallyLenght+" "+ tallyLenghtUOM +" ("+ psDesc +")'>&nbsp;</td>"+
									"</tr>"+
									"<tr>"+
										"<td style='font-size:1pt; padding:0px;height:"+imageHeightx+"px;width:7px;background-color:#ffffff'>&nbsp;</td>" +
										"<td style='height:" + imageHeightx + "px;background-color:#ffffff;width:16px;'>&nbsp;</td>" +
										"<td style='font-size:1pt; padding:0px;height:"+imageHeightx+"px;width:7px;background-color:#ffffff' >&nbsp;</td>"+
									"</tr>"
								:"<tr><td style='height:" + this._imageHeight + "px;background-color:#ffffff;width:30px;'>&nbsp;</td></tr>") +
							"</table>" +
						"</td>";
						
						D3.forEach(this._CasingSection.reverse(),function(casingSection){
							var height = casingSection.getFieldValue("@height"); 	
							var height_before = casingSection.getFieldValue("@height_before");
							var casingOd = casingSection.getLookup("casingOd").getLookupItem(casingSection.getFieldValue("casingOd")).label;
							casingOd = casingOd.replace("\"","&quot;");
							var casingSummary = casingSection.getFieldValue("casingSummary").replace("\"","&quot;"); 
							var bgColor = casingSection.getFieldValue("@bgColor");
							var shoeTopMdMsl = casingSection.getFieldValue("shoeTopMdMsl");
							var shoeTopMdMslUom = casingSection.getUomSymbol("shoeTopMdMsl");
							if (height!=""){
								casingSectionContain = casingSectionContain +
								"<td style='cursor:pointer; padding:0px;text-align:center;' onclick=\"window.D3.WS.redirect('casingsection.html')\">" +
									"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" +
										((parseInt(height_before) > 0)?
										"<tr>" +
										"<td style='font-size:1pt; padding:0px;height:"+height_before+"px;width:7px;'>&nbsp;</td>" +
										"</tr>":"") +
										"<tr valign='bottom'>" +
											"<td style='font-size:1pt; padding:0px;height:"+height+"px;border-left:solid 1px #000000; border-top:solid 1px #000000;width:7px; background-color:"+bgColor+";' " +
												"title='" + casingOd + " @ " + (shoeTopMdMsl!=""?"shoe depth " + shoeTopMdMsl + " " + shoeTopMdMslUom:"") + "\n" + 
												"Casing - " + casingSummary + "'><img src='images/hangright.gif'></td>" +
										"</tr>"+
									"</table>"+
								"</td>";
							}
						},this);
						
					}
					casingSectionContain = casingSectionContain + "</tr>" +
				"</table>" +
			"</td>";
		return casingSectionContain; 
	}

	WellSummary.prototype.getFormationSection = function(){
		var formationSection  = "<td align='center' onclick=\"window.D3.WS.redirect('formation.html')\" >"+
			"<table style='padding:0px;' cellspacing='0' cellpadding='0'>" ;
		if (this._Formation.length>0){
			if (this._formationFirstDepth!=""){
				formationSection = formationSection + "<tr valign='top'>";
				if (this._datumReferencePoint=="WH"){
					if (this._datumLabel!=""){
						formationSection = formationSection +
						"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +
							this._datumLabel +
						"</td>";
					}else{ 
						formationSection = formationSection +
						"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +
							"<span style='overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:"+this._zRtwellhead+"px;text-align:right; border-top:solid 1px #000000;'>" + 
							"0.0 " + this._datumUOM +
							"</span>"+
						"</td>"; 
					}
				}else{
					if (this._zRtwellhead!=""){ 
						formationSection = formationSection +
						"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" + 
							this._zRtwellhead +
						"</td>";
					}else{ 
						formationSection = formationSection +
						"<td style='cursor:pointer; padding:0px; padding-top:1px; height:"+this._zRtwellhead+"px; text-align:right;' nowrap>" +  
							"<span style='overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:"+this._zRtwellhead+"px;text-align:right; border-top:solid 1px #000000;'>" +  
							"0.0 " + this.commandBeanProxy.getUomSymbol("Operation", "zRtwellhead") +
							"</span></td>";
					}
				}	
				formationSection = formationSection +
					"<td style='cursor:pointer; padding:0px;height:" +
					this._formationFirstDepth+"px;text-align:center;border-top:solid 1px #000000;border-right:solid 1px #000000;' nowrap>" +
						"<div style='overflow: hidden; padding:0px;padding-top:0px; height:"+this._formationFirstDepth+"px;text-align:center;'>&nbsp;</div>"+
					"</td>" +
					"<td style='cursor:pointer; padding:0px;height:"+this._formationFirstDepth+"px;text-align:center;border-top:dotted 1px #000000;width:50px;' nowrap>" +
						"<div style='overflow: hidden; padding:0px;padding-top:0px; height:"+this._formationFirstDepth+"px;text-align:center;'>&nbsp;</div>" +
					"</td>" +
				"</tr>";
			}
			
			var row = 0;
			var recordcount = 0;
			var formationSize = this._Formation.length;
			D3.forEach(this._Formation,function(formation){
				var formationUid = formation.getFieldValue("formationUid").replace(".",""); 	
				var height = formation.getFieldValue("@height");
				var sampleTopMdMsl = formation.getFieldValue("sampleTopMdMsl");
				var formationName = formation.getFieldValue("formationName");
				var formationImage = formation.getFieldValue("formationImage");
				recordcount = recordcount + 1;
				if (height!=""){
					formationSection = formationSection +
					"<tr valign='top'>"+
						"<td style='cursor:pointer; padding:0px;height:"+height+"px;text-align:right;' nowrap title='"+formationName + " @ " + sampleTopMdMsl + this._formationDepthUOM + "'>";
							
							if (formationSize > recordcount){
								
								if (parseInt(height) >= 10){
									if ((row % 2) == 0){
										formationSection = formationSection + "<div style='overflow: visible; scrolls: no; padding:0px;padding-top:0px; height:"+height+"px;text-align:left; border-top:solid 1px #000000;'>" +
										"<label style='background-color: #6699ff;'>" +sampleTopMdMsl + " </label>" + this._formationDepthUOM + 
											"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
										"</div>";
									} else {
										formationSection = formationSection + "<div style='overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:"+height+"px;text-align:left; border-top:solid 1px #000000;'>" + 
										"<label style='background-color: #6699ff;'>" +sampleTopMdMsl + " </label>" + this._formationDepthUOM + 
										"</div>";
									}
									row = row + 1;
								} else {
									formationSection = formationSection + "<div style='overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:"+height+"px;text-align:right; '>" +
										"&nbsp;</div>";
								}
							} else {	
								if ((row % 2) == 0){
									formationSection = formationSection +
									"<div class='formation-marking' style='overflow: visible; scrolls: no; padding:0px;padding-top:0px; height:"+height+"px;text-align:left; border-top:solid 1px #000000;'>" + 
									"<label style='background-color: #6699ff;'>" + sampleTopMdMsl + " </label>" + this._formationDepthUOM  + 
									"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
									"</div>";
								}else{
									formationSection = formationSection +
									"<div style='overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:"+height+"px;text-align:left; border-top:solid 1px #000000;'>" +
										"<label style='background-color: #6699ff;'>" +sampleTopMdMsl + " </label>" + this._formationDepthUOM + "</div>";
								}
							}
						formationSection = formationSection +
						"</td>"+
						"<td style='cursor:pointer; padding:0px;height:"+height+"px;text-align:center;border-top:solid 1px #000000;border-right:solid 1px #000000;' nowrap title='"+ formationName + " @ " + sampleTopMdMsl +" "+ this._formationDepthUOM + "'>" +
							"<div style='overflow: hidden; padding:0px;padding-top:0px; height:"+height+"px;text-align:center;'>&nbsp;</div>"+
						"</td>"+
						"<td style='cursor:pointer; padding:0px;height:"+height+"px;text-align:center;border-top:dotted 1px #000000;width:50px;' nowrap id='"+formationUid+"' class='"+formationUid+"'>" +
							"<div style='overflow: hidden; padding:0px;padding-top:0px; height:"+height+"px;text-align:center;background:url("+this._imgpath+"/"+formationImage+");' title='"+formationName + " @ " + sampleTopMdMsl + " " + this._formationDepthUOM + "' class='f_image"+formationUid+"'>&nbsp;</div>"+
						"</td>"+
					"</tr>";
					}
				},this);
		}
		formationSection = formationSection +
		"</table>"+
		"</td>";
	
		return formationSection;
	}

	WellSummary.prototype.showField = function(classData, field){
		if (classData.getFieldValue(field)!= ""){
			var value = classData.getFieldValue(field);
			if (classData.isLookup(field)){
				var lookup = classData.getLookup(field);
				if (lookup.getLookupItem(value)==null) return value;
				else return lookup.getLookupItem(value).label;
			}
			var uom = classData.getUomSymbol(field);
			if (uom!=null) return value + " " + uom;
			else return value;
		}
		return "";
	}
	WellSummary.prototype.getNoOperationTemplate = function(){
		var lbl = document.createElement("label");
		lbl.appendChild(document.createTextNode("No Operation Selected"));
		return lbl
	}
	WellSummary.prototype.refreshLayout = function(){
		// do nothing
	}
	window.D3.WellSummary = WellSummary;
})(window);