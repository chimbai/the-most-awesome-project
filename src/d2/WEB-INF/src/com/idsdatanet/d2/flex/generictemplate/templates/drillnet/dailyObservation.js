(function(window){
	D3.inherits(ReportLinkField,D3.AbstractFieldComponent);
	function ReportLinkField(){
		D3.AbstractFieldComponent.call(this);
		this.downloadLabel = D3.LanguageManager.getLabel("DailyObservation.download");
	}
	ReportLinkField.prototype.download = function(){
		var id = this.getFieldValue();
		if(id){
			this._node.commandBeanProxy.customInvoke({_invokeCustomFilter: "getReportDownloadFilename", id: id},this.startDownload,this,"json");
		}
	}
	ReportLinkField.prototype.startDownload = function(response){
		var url = "abaccess/dailyobservation/" + encodeURIComponent(response.filename) + "?_invokeCustomFilter=downloadReport&reportId=" + encodeURIComponent(this.getFieldValue());
		this._node.commandBeanProxy.openDownloadWindow(url);
	}
	ReportLinkField.prototype.getFieldRenderer = function(){
		if (!this._renderer){
			this._renderer = document.createElement("a");
			this._renderer.className = "reportLink";
			this._renderer.style.cursor = "pointer";
			var self = this;
			this._renderer.addEventListener("click", function(e){e.stopPropagation(); self.download();});
		}
		return this._renderer;
	}
	ReportLinkField.prototype.refreshFieldRenderer = function(){
		while(this._renderer.firstChild) this._renderer.removeChild(this._renderer.firstChild);
		if(this.getFieldValue()){
			this._renderer.appendChild(document.createTextNode(this.downloadLabel));
		}
	}
	
	D3.inherits(ReportListener,D3.EmptyNodeListener);
	function ReportListener()
	{
	}
	ReportListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (!this._commandBeanProxy){
			this._commandBeanProxy = commandBeanProxy;
			this.LABEL_GENERATE_REPORT = D3.LanguageManager.getLabel("button.generateReport");
			this.LABEL_GENERATE_REPORT_AND_SEND = D3.LanguageManager.getLabel("button.generateReportAndSend");
			this.LABEL_GENERATE_REPORT_PLEASE_WAIT = D3.LanguageManager.getLabel("button.generateReportPleaseWait");
		}
		if (id == "generateReport") {
			this._btnGenerateReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:this.LABEL_GENERATE_REPORT,
				callbacks:{
					onclick:this.generateReport
				},
				context:this
			});
			return this._btnGenerateReport;
		}else if (id == "generateAndSent") {
			this._btnGenerateAndSendReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:this.LABEL_GENERATE_REPORT_AND_SEND,
				callbacks:{
					onclick:this.generateAndSendReport
				},
				context:this
			});
			return this._btnGenerateAndSendReport;
		}
	}
	ReportListener.prototype.getSelectedRecords = function(){
		var result = new Array();
		var list = this._commandBeanProxy.rootNode.children["DailyObservation"].getItems();
		for(var i=0; i < list.length; ++i){
			if(list[i].getSelectedMode()){
				result.push({key: list[i].primaryKeyValue});
			}
		}
		if(result.length == 0){
			var selected = D3.Viewer.pageListingListView.getChildClassUIComponents()["DailyObservation"].currentSelectedRowRef;
			if(selected){
				result.push({key: selected.node.primaryKeyValue});
			}
		}
		return result;
	}
	ReportListener.prototype.submitReport = function(sendAfterGenerate, btn){
		if(btn.disabled) return;
		var selected = this.getSelectedRecords();
		if(selected.length == 0){
			alert("Please add Daily Observation before generate report.");
			return;
		}
		var params = {};
		params._invokeCustomFilter = "generate_report";
		if(sendAfterGenerate) params.sendAfterGenerate = "1";
		params.selected = JSON.stringify(selected);
		this._commandBeanProxy.customInvoke(params,this.statusCheckCompleted,this,"json");
		this.disableBtnAndShowGeneratingReport([btn]);
	}
	ReportListener.prototype.generateReport = function(){
		this.submitReport(false, this._btnGenerateReport);
	}
	ReportListener.prototype.generateAndSendReport = function(){
		this.submitReport(true, this._btnGenerateAndSendReport);
	}
	ReportListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "reportLink"){
			return new ReportLinkField();
		}
	}
	ReportListener.prototype.loadCompleted = function(commandBean) {
		this.checkingCurrentStatus();
	}
	ReportListener.prototype.checkingCurrentStatus = function() {
		this._commandBeanProxy.customInvoke({_invokeCustomFilter: "check_report_status"},this.statusCheckCompleted,this,"json");
	}
	ReportListener.prototype.updateLabel = function(btn, label){
		while(btn.firstChild) btn.removeChild(btn.firstChild);
		btn.appendChild(document.createTextNode(label));
	}
	ReportListener.prototype.disableBtnAndShowGeneratingReport = function(btns){
		for(var i=0; i < btns.length; ++i){
			if(!btns[i].disabled){
				this.updateLabel(btns[i], this.LABEL_GENERATE_REPORT_PLEASE_WAIT);
			}
		}
		this.disableAllReportButtons(true);
	}
	ReportListener.prototype.disableAllReportButtons = function(disable){
		this._btnGenerateReport.disabled = disable;
		this._btnGenerateAndSendReport.disabled = disable; 
	}
	ReportListener.prototype.setGeneratingReportInProgressAndCheckAgainLater = function(btns){
		this.disableBtnAndShowGeneratingReport(btns);
		var self = this;
		setTimeout(function(){self.checkingCurrentStatus()},1000);
	}
	ReportListener.prototype.updateCompletedJob = function(list, errorList){
		if(!list) return;
		var i, item, node;
		for(i=0; i < list.length; ++i){
			item = list[i];
			if(item.status == "completed"){
				node = this.findNode("DailyObservation", item.key)
				if(node) this.updateOriginalFieldValue(node, "@reportDownload", item.key);
			}else if(item.status == "error"){
				errorList.push("Error generating report: " + item.msg);
			}
		}
	}
	ReportListener.prototype.updateOriginalFieldValue = function(node, field, value){
		var f = node.fieldData[field];
		if(f){
			f.value = value;
			node.fireFieldValueChangedEvent.call(node, field);
		}
	}
	ReportListener.prototype.findNode = function(className, key){
		var list = this._commandBeanProxy.rootNode.children[className].getItems();
		for(var i=0; i < list.length; ++i){
			if(list[i].primaryKeyValue == key) return list[i];
		}
	}
	ReportListener.prototype.statusCheckCompleted = function(response) {
		var errorList = [];
		this.updateCompletedJob(response.gen_report.completed, errorList);
		this.updateCompletedJob(response.gen_and_send.completed, errorList);
		var running = [];
		var status = response.gen_report.status;
		if (status == "no_job") {
			this.updateLabel(this._btnGenerateReport, this.LABEL_GENERATE_REPORT);
		} else if (status == "job_running") {
			running.push(this._btnGenerateReport);
		}
		status = response.gen_and_send.status;
		if (status == "no_job") {
			this.updateLabel(this._btnGenerateAndSendReport, this.LABEL_GENERATE_REPORT_AND_SEND);
		} else if (status == "job_running") {
			running.push(this._btnGenerateAndSendReport);
		}
		if(running.length > 0){
			this.setGeneratingReportInProgressAndCheckAgainLater(running);
		}else{
			this.disableAllReportButtons(false);
		}
		if(response.error){
			errorList.push(response.error);
		}
		if(errorList.length > 0){
			if(!this._commandBeanProxy.systemMessage) this._commandBeanProxy.systemMessage = [];
			for(var i=0; i < errorList.length; ++i){
				this._commandBeanProxy.systemMessage.push({value: errorList[i], type:"3"});
			}
			D3.Viewer.showInlineSystemMessage();
		}
	}
	D3.CommandBeanProxy.nodeListener = ReportListener;
})(window);