(function(window){
	
	D3.inherits(ActivityRemarksInline,D3.AbstractFieldComponent);
	function ActivityRemarksInline() {
		D3.AbstractFieldComponent.call(this);
		this.numberFormatter = new D3.NumberBase();
	}
	
	ActivityRemarksInline.prototype.destroyFieldEditor = function(event) {
		this._fieldEditor = null;
	}
	
	ActivityRemarksInline.prototype.getFieldRenderer = function() {
		this._fieldRenderer = this.showFTRText();
		return this._fieldRenderer;
	}

	ActivityRemarksInline.prototype.showFTRText = function() {
		if (this._node.getFieldValue("additionalRemarks")) {
			var remark = this._node.getFieldValue("additionalRemarks");
			remark = remark.replace(/;/g, "\n")
			 
			var tableConfig = "<table style='white-space:pre-wrap;'><tr><td style='vertical-align:top;'>Fixed Text Remark:</td><td>" + remark + "</td></tr></table>";
			
			var renderer = $(tableConfig);
			this._renderer = renderer.get(0);
			return this._renderer;
		}
		return null;
	}

	ActivityRemarksInline.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) { 
			this._fieldEditor = this.createFTRButtonPanel();
			if (this._fieldEditor) {
				this._fieldEditor.setAttribute("style", "white-space:pre-wrap;");
			}
			
			this.triggerFTREditMode();
			
			var autoTrigger = false; 
			if (this._CustomInputList) {
				for (var i = 0; i < this._CustomInputList.length; i++) {
					var customCodeObj = this._CustomInputList[i];
					if (customCodeObj.originalValue) {
						autoTrigger = true;
					}
				}
				if (autoTrigger) {
					if (this._btnRetrieve) {
						this._btnRetrieve.click();
					}
				}
			}
		}
		return this._fieldEditor;
	}
	
	ActivityRemarksInline.prototype.guidGenerator = function() {
	    var S4 = function() {
	        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	     };
	     return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}

	ActivityRemarksInline.prototype.triggerFTREditMode = function() {
		if(this.allowFieldShow()) {
			if (this._container) {
				$(this._container).click();
			}
		}
	}
	
	ActivityRemarksInline.prototype.createFTRButtonPanel = function() {
		var fieldEditor = null;
		
		var labelFTR = D3.LanguageManager.getLabel("button.FTR");
		var labelClear = D3.LanguageManager.getLabel("button.clear");
		
		var divConfig = "<div class='inlineFTRTable'>" +
			"<table style='white-space:pre-wrap;'><tr><td style='vertical-align:top;'>Fixed Text Remark:</td><td>" + this._node.getFieldValue("additionalRemarks") + "</td></tr>" +		
				"<tr>" +
					"<td colspan='2' style='padding-left:3px;'><button class='DefaultButton buttonClear'>"+labelClear+"</button></td>" +
				"</tr>" +
			"</table>" + "</div>";
		
		if (this.allowFieldShow()) {
			var container = $(divConfig);
			this._container = container.get(0);
			var that = this;
			
			container.click(function() {
				that.populateRemarksEventHandler();	
			});
			
			this._btnClear = container.find(".buttonClear");
			this._btnClear.click(function() {
				that.clearEventHandler();
			});
			
			var outerDiv;
			if (this._divId) {
				var findId = '#' + this._divId;
				$(findId.toString()).empty();
				outerDiv = $(findId.toString()).get(0);
				if (!outerDiv) {		
					outerDiv = document.createElement("div");
					outerDiv.setAttribute("id", this._divId);
				}
			} else {
				outerDiv = this.initiateCreateFieldEditor();
			}
			outerDiv.appendChild(this._container);	
			fieldEditor = outerDiv;
		} else if(this._node.getFieldValue("additionalRemarks")!=null && this._node.getFieldValue("additionalRemarks")!=""){
			if(!this.allowFieldShow())
			{
				var container = $(divConfig);
				this._container = container.get(0);
				var that = this;
				container.click(function() {
					D3.UIManager.popup("Info", "This fixed text remark combination no longer exists for editing.");
				});
				this._btnClear = container.find(".buttonClear");
				this._btnClear.click(function() {
					that.clearEventHandler();
				});
				
				var outerDiv;
				if (this._divId) {
					var findId = '#' + this._divId;
					$(findId.toString()).empty();
					outerDiv = $(findId.toString()).get(0);
					if (!outerDiv) {		
						outerDiv = document.createElement("div");
						outerDiv.setAttribute("id", this._divId);
					}
				} else {
					outerDiv = this.initiateCreateFieldEditor();
				}
				outerDiv.appendChild(this._container);	
				fieldEditor = outerDiv;
			}
		}
		return fieldEditor;
	}
	
	ActivityRemarksInline.prototype.dispose = function() {
		this._FreeTextRemarksList = null;
		this._lookupModuleParams = null;
		this._lookupModuleConstant = null;
		this._lookupParamsUOM = null;
		this._lookupList = null;
		this._userInputComponentList = [];
		this._CustomInputList = null;
		this._customInputLookupList = null;
		this._customInputComponentList = null;
		this._customInputParams = null;
		this._customCodeParamKey = null;
		this._customCodeParamValue = null;
		this._customCodeParamUid = null;
		if (this._node.commandBeanProxy) {
			this._node.commandBeanProxy.removeEventListener(D3.CommandBeanProxyEvent.REDRAW_ROOT_BUTTONS);	
		}
		
		ActivityRemarksInline.uber.dispose.call(this);
	}
	
	ActivityRemarksInline.prototype.populateRemarksEventHandler = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksInline.prototype.clearEventHandler = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
		this._node.setFieldValue("@activityFreeTextRemark", "");
		
		var editor = this.cleanAndGetFieldEditor();
		editor = this.createFTRButtonPanel();
		
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnReset_onClick = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
		this._node.setFieldValue("@activityFreeTextRemark", "");
		
		this.clearInputFields();
	}
	
	ActivityRemarksInline.prototype.loadXMLCustomParameters = function(response) {
		//collect custom parameters
		this.loadWitsmlSettings(response);
		this.populateCustomInputList(response);
		if (this._customInputSize == 0) {
			this.populateRemarkList(response);
			this.generateInlineEditor();
		} else {
			this.generateCustomInlineEditor();
		}
		
	}
	
	ActivityRemarksInline.prototype.loadXMLFreeTextRemarks = function(response) {
		//collect free text remarks
		this.populateCustomInputParameterFromUser(response);
		this.populateRemarkList(response);
		this.generateInlineEditor();
	}
	
	ActivityRemarksInline.prototype.loadWitsmlSettings = function(response) {
		var elmIsWitsmlFtr = "";
		var doc = $(response);
		doc.find("witsmlSettings").each(function(index, element){
			var elm = $(this);
			elmIsWitsmlFtr = elm.find("isWitsmlFtr").first().text();
		});
		this._isWitsmlFtr = (elmIsWitsmlFtr == "true" ? true : false);
	}
	
	ActivityRemarksInline.prototype.populateRemarkList = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("freeTextRemarks").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("activityDescriptionMatrixUid").first().text();
			newItem.label = elm.find("remarks").first().text();
			
			collection.push(newItem);
		});

		this._FreeTextRemarksList = collection;
		if (this._FreeTextRemarksList.length == 1) {
			this.populateParamList(response);
		}
	}
	
	ActivityRemarksInline.prototype.populateCustomInputParameterFromUser = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("outputParameterArray").each(function(index, element){
			var elm = $(this);
			var skip = elm.find("skip").text();
			if (skip) {
				elm.find("item").each(function(index2, element2){
					var newItem = {};
					var childElm = $(this);
					newItem.index = childElm.find("index").first().text();
					newItem.label = childElm.find("label").first().text(); //parameter
					newItem.value = childElm.find("value").first().text(); //value
					newItem.customCodeUid = childElm.find("customCodeUid").first().text(); //customcodeuid
					collection.push(newItem);
				});
			}
		});
		this._customInputParams = collection;
	}
	
	ActivityRemarksInline.prototype.populateCustomInputList = function(response) {
		var collection = [];
		this._customInputLookupList = [];
		var that = this;
		
		var doc = $(response);
		var customParameter = doc.find("customParameter").get(0);
		this._customInputSize = $(customParameter).attr("size");
		if (this._customInputSize > 0) {
			doc.find("customItem").each(function(index,customItem){
				var newItem = {};
				var elm = $(this);
				newItem.sequence = elm.find("sequence").first().text();
				newItem.parameterKey = elm.find("parameterKey").first().text();
				newItem.label = elm.find("label").first().text();
				newItem.paramType = elm.find("paramType").first().text();
				newItem.dataType = elm.find("dataType").first().text();
				newItem.customCodeUid = elm.find("customCodeUid").first().text();
				newItem.precision = elm.find("precision").first().text();
				newItem.uomSymbol = elm.find("uomSymbol").first().text();
				newItem.lookupKey = elm.find("lookupKey").first().text();
				newItem.originalValue = elm.find("originalValue").first().text(); //if editing existing
				collection.push(newItem);
				
				var custItemLookup = elm.find("custItemLookup");
				if (custItemLookup != null) {
					var newLookupRef = new D3.LookupReference();
					var newLookup = new D3.LookupItem();
					newLookup.data = "";
					newLookup.label = "";
					newLookupRef.addLookupItem(newLookup);
					
					var idx = 0;
					
					elm.find("item").each(function(index2, element2){
						newLookup = new D3.LookupItem();
						var childElm = $(this);
						
						newLookup.data = childElm.find("key").first().text();
						newLookup.label = childElm.find("label").first().text();
						newLookupRef.addLookupItem(newLookup);
						idx++;
					});
					if (idx > 0) {
						var parameterKey = elm.find("parameterKey").first().text();
						that._customInputLookupList[parameterKey] = newLookupRef;
					}
				}

			});
		}
		
		this._CustomInputList = collection;
	}
	
	ActivityRemarksInline.prototype.populateParamList = function(response) {
		this._lookupModuleParams = [];
		this._lookupParamsUOM = [];
		this._lookupParamsPrecision = [];
		this._placeholder = [];
		this._prefix = [];
		this._suffix = [];
		
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		doc.find("paramDetail").each(function(index, element){
			var elm = $(this);
			idx = elm.find("sequence").first().text();
			
			var newItem = {};
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("dataType").first().text();
			newItem.originalValue = elm.find("paramValue").first().text();
			newItem.precision = elm.find("precision").first().text();
			
			that._lookupModuleParams[idx] = newItem;
			that._lookupParamsPrecision[idx] = elm.find("precision").first().text();
			that._lookupParamsUOM[idx] = elm.find("uomSymbol").first().text();
			that._placeholder[idx] = elm.find("placeholder").first().text();
			that._prefix[idx] = elm.find("prefix").first().text();
			that._suffix[idx] = elm.find("suffix").first().text();
		});
		
		this.populateConstantList(response);
		this.populateLookupList(response);
	}
	
	ActivityRemarksInline.prototype.populateConstantList = function(response) {
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		this._lookupModuleConstant = [];
		
		doc.find("constantDetail").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("paramValue").first().text();
			
			that._lookupModuleConstant[idx] = newItem;
			idx++;
		});
	}
	
	ActivityRemarksInline.prototype.populateLookupList = function(response) {
		var idx = 0;
		var lookupID = "";
		var that = this;
		this._lookupList = [];
		
		var doc = $(response);
		doc.find("lookup").each(function(index, element){
			var elm = $(this);
			lookupID = elm.attr("id");
			
			idx = 0;
			
			var lookupItem = new D3.LookupItem();
			lookupItem.data = "";
			lookupItem.label = "";
			var lookupRef = new D3.LookupReference();
			lookupRef.addLookupItem(lookupItem);
			
			elm.find("item").each(function(index2, element2){
				var childElm = $(this);
				lookupItem = new D3.LookupItem();
				lookupItem.data = childElm.find("key").first().text();
				lookupItem.label = childElm.find("label").first().text();
				lookupRef.addLookupItem(lookupItem);
				idx++;
			});
			
			if (idx > 0) {
				that._lookupList[lookupID] = lookupRef;
			}
		});
	}
	
	ActivityRemarksInline.prototype.generateInlineEditor = function() {
		var that = this;
		
		var btnReset = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.reset"),
				callbacks:{
					onclick:function(event) {
						this.btnReset_onClick(event);
					}
				},
				context:this
		}
		
		var btnWitsml = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("WITSML Import"),
				callbacks:{
					onclick:function(event) {
						this.btnWitsml_onClick(event);
					}
				},
				context:this
		}
		
		this.btnBackId = this.guidGenerator();
		var btnBack = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.back"),
				callbacks:{
					onclick:function(event) {
						this.btnBack_onClick(event);
					}
				},
				attributes:{"id": this.btnBackId},
				context:this
		}
		
		if (this._FreeTextRemarksList.length > 0) {
			var obj = this._FreeTextRemarksList[0];
			
			if (obj.label != null) {
				this._selectedRemark = obj.label;
			}
			this._panel = document.createElement("div");
			
			var dynamicObj = this.generateDynamicObject(this._selectedRemark);
			var buttons = null;
			
			if (this._customInputSize > 0) {
				var buttonContainer = document.createElement("div");
				buttonContainer.appendChild(D3.UI.createElement(btnBack));
				buttonContainer.appendChild(D3.UI.createElement(btnReset));
				if(this._isWitsmlFtr) buttonContainer.appendChild(D3.UI.createElement(btnWitsml));
				buttons = buttonContainer;
			}
			else {
				var buttonContainer = document.createElement("div");
				buttonContainer.appendChild(D3.UI.createElement(btnReset));
				if(this._isWitsmlFtr) buttonContainer.appendChild(D3.UI.createElement(btnWitsml));
				buttons = buttonContainer;
			}
			if (dynamicObj) {
				this._panel.appendChild(dynamicObj);
				var pnl = $(this._panel);
				this._inlineSecPanel = document.createElement("div");
				
				var editor = this.cleanAndGetFieldEditor();
				
				this._inlineSecPanel.appendChild(pnl.get(0));
				this._inlineSecPanel.appendChild(buttons);
				if (editor) editor.appendChild(this._inlineSecPanel);

				this._fieldEditor = editor;
			} else {
				var that = this;
				if (this.checkIfFormHasError()) {
					this.createCustomInlineEditor();
				} else {
					this._panel = document.createElement("div");
					this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
					var pnl = $(this._panel);
					var ok = {
						tag:"button",
						css:"DefaultButton",
						text:D3.LanguageManager.getLabel("button.ok"),
						callbacks:{
							onclick:function(event) {
								this.createCustomInlineEditor(event);
							}
						},
						context:this
						
					};
					var button = document.createElement("div");
					button.appendChild(D3.UI.createElement(ok));
					
					this._inlineSecPanel = document.createElement("div");
					
					var editor = this.cleanAndGetFieldEditor();
					
					this._inlineSecPanel.appendChild(pnl.get(0));
					this._inlineSecPanel.appendChild(button);
					if (editor) editor.appendChild(this._inlineSecPanel);
					
					this._fieldEditor= editor;

				}
			}
		} else {
			this._panel = document.createElement("div");
			this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
			var pnl = $(this._panel);
			var ok = {
					tag:"button",
					css:"DefaultButton",
					text:D3.LanguageManager.getLabel("button.ok"),
					callbacks:{
						onclick:function(event) {
							this.createCustomInlineEditor(event);
						}
					},
					context:this
				};
			var button = document.createElement("div");
			button.appendChild(D3.UI.createElement(ok));
			this._inlineSecPanel = document.createElement("div");
			var editor = this.cleanAndGetFieldEditor();
			this._inlineSecPanel.appendChild(pnl.get(0));
			this._inlineSecPanel.appendChild(button);
			if (editor) editor.appendChild(this._inlineSecPanel);
			
			this._fieldEditor = editor;
		}
	}
	
	ActivityRemarksInline.prototype.createCustomInlineEditor = function() {
		this.generateCustomInlineEditor();
	}
	
	ActivityRemarksInline.prototype.cleanAndGetFieldEditor = function() {
		var editor;
		if (this._divId) {
			var findId = '#' + this._divId.toString();
			$(findId.toString()).empty();
			editor = $(findId.toString()).get(0);
			if (!editor) {
				this.removeFTRPanel();
				editor = this._fieldEditor;
			}
		} else {
			this.removeFTRPanel();
			editor = this._fieldEditor;
		}
		return editor;
	}
	
	ActivityRemarksInline.prototype.removeFTRPanel = function() {
		$(this._fieldEditor).empty();
	}
	
	ActivityRemarksInline.prototype.initiateCreateFieldEditor = function() {
		this._divId = this.guidGenerator();	
		var outerDiv = document.createElement("div");
		outerDiv.setAttribute("id", this._divId);
		this._fieldEditor = outerDiv;
		return outerDiv;
	}
	
	ActivityRemarksInline.prototype.generateCustomInlineEditor = function() {
		var that = this;
		var btnRetrieve = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.retrieveFTR"),
				callbacks:{
					onclick:function(event) {
						that.btnRetrieve_onClick(event);
					}
				},
				context:this
		}
		
		var div = document.createElement("div");
		
		if (this._customInputSize > 0) {
				this._panelInitial = document.createElement("div");
				this._btnRetrieve = D3.UI.createElement(btnRetrieve);
				this._btnRetrieveId = this.guidGenerator();
				this._btnRetrieve.id = this._btnRetrieveId;
				var result = this.generateCustomInputFields();
				if (result) {
					this._panelInitial.appendChild(result);
					
					var pnl = $(this._panelInitial);
					this._inlinePanel = document.createElement("div");
					
					var editor = this.cleanAndGetFieldEditor();
					
					this._inlinePanel.appendChild(pnl.get(0));
					var buttons = document.createElement("div");
					buttons.appendChild(this._btnRetrieve);
					
					this._inlinePanel.appendChild(buttons);
					if (editor) editor.appendChild(this._inlinePanel);
					this._fieldEditor = editor;
				}
		}
		else {
			this.generateInlineEditor();
		}
		
		
	}
	
	ActivityRemarksInline.prototype.generateCustomInputFields = function() {
		var label = null;
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.setAttribute("style", "padding:1px;");
		
		table.appendChild(tr);
		tr.appendChild(td);
		var span = document.createElement("span");
		
		this._customInputComponentList = [];
		
		if (this._customInputSize > 0) {
			for (var i = 0; i < this._CustomInputList.length; i++) {
				var customCodeObj = this._CustomInputList[i];
				var value = "";
				label = document.createTextNode(customCodeObj.label.toString());
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px;");
				tr.appendChild(td);
				var embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width: auto;");
				span = document.createElement("span");
				span.setAttribute("style", "margin-left:2px; margin-right:2px;");
				span.appendChild(label);
				embedDiv.appendChild(span);
				td.appendChild(embedDiv);
				this.createCustomInputObject(customCodeObj.parameterKey, i, embedDiv);
			}
		}
		return table;
	}
	
	ActivityRemarksInline.prototype.generateDynamicObject = function(buffer) {

		this._userInputComponentList = [];
		
		if (buffer.length > 0) {
			var label = null;
			var textbox = null;
			var table = document.createElement("table");
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			var span = document.createElement("span");
			td.setAttribute("style", "padding:1px");
			
			var embedDiv = document.createElement("div");
			embedDiv.setAttribute("style", "width:auto;");
			
			var boxIndex = 0;
			if (buffer.indexOf(";") >= 0) {
				var temp = [];
				temp = buffer.split(";");
				for(var index = 0; index < temp.length; index++) {
					tr = document.createElement("tr");
					table.appendChild(tr);
					td = document.createElement("td");
					td.setAttribute("style", "padding:1px");
					tr.appendChild(td);
					
					embedDiv = document.createElement("div");
					embedDiv.setAttribute("style", "width:auto;");
						
					if ($.trim(temp[index].toString()).length > 0) {
						if (temp[index].indexOf("#") >= 0) {
							var temp1 = [];
							temp1 = temp[index].split(/(#)/g);
							
							for(var index1 = 0; index1 < temp1.length; index1++) {
								
								var toProcess = $.trim(temp1[index1].toString());
								var nonEmptyString = toProcess.length > 0;
								
								if (nonEmptyString) {
									if (toProcess == "#") {
										this.createUserInputObject(boxIndex, embedDiv);	
										if (this.triggerChangeDynaAttr == true) {
											this.setNodeDynaAttrFTR(this);
										}
										boxIndex++;
									} else {
										label = document.createTextNode(toProcess);
										span = document.createElement("span");
										span.setAttribute("style", "margin-left:2px; margin-right:2px;");
										span.appendChild(label);
										embedDiv.appendChild(span);
									}
								}
							}
						} else {
							this.setNodeDynaAttrFTR(this);
							label = document.createTextNode(temp[index].toString());
							span = document.createElement("span");
							span.setAttribute("style", "margin-left:2px; margin-right:2px;");
							span.appendChild(label);
							embedDiv.appendChild(span);
						}
					}
					td.appendChild(embedDiv);
				}
				
			} else {
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px");
				tr.appendChild(td);
				
				embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width:auto;");
				
				if ($.trim(buffer.toString()).length > 0) {
						
					if (buffer.indexOf("#") >= 0) {
						var temp1 = [];
						temp1 = buffer.split(/(#)/g);
							
						for(var index1 = 0; index1 < temp1.length; index1++) {
								
							var toProcess = $.trim(temp1[index1].toString());
							var nonEmptyString = toProcess.length > 0;
								
							if (nonEmptyString) {
								if (toProcess == "#") {
									this.createUserInputObject(boxIndex, embedDiv);	
									if (this.triggerChangeDynaAttr == true) {
										this.setNodeDynaAttrFTR(this);
									}
									boxIndex++;
								} else {
									label = document.createTextNode(toProcess);
									span = document.createElement("span");
									span.setAttribute("style", "margin-left:2px; margin-right:2px;");
									span.appendChild(label);
									embedDiv.appendChild(span);
								}
							}
						}
					} else {
						this.setNodeDynaAttrFTR(this);
						label = document.createTextNode(buffer.toString());
						span = document.createElement("span");
						span.setAttribute("style", "margin-left:2px; margin-right:2px;");
						span.appendChild(label);
						embedDiv.appendChild(span);
					}

					td.appendChild(embedDiv);
				}
			}
			
			return table;
		}
		return null;
	}
	
	ActivityRemarksInline.prototype.createCustomInputObject = function(parameterKey, i, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		var customCodeObj = this._CustomInputList[i];
		
		if (this._customInputLookupList.hasOwnProperty(parameterKey)) {
			var comboObject = document.createElement("select");
			comboObject.id = parameterKey;
			comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			
			lookupRef = this._customInputLookupList[parameterKey];
			
			if (lookupRef != null) {
				var option = document.createElement("option");

				D3.forEach(lookupRef.getActiveLookup(), function(item) {
					option = document.createElement("option");
					option.setAttribute("value", item.data);
					option.appendChild(document.createTextNode(item.label));
					comboObject.options.add(option);
				});
				
				if (customCodeObj.originalValue) {
					this.setSelectedValue(comboObject, customCodeObj.originalValue); //if edit existing	
				}
			}
			this._customInputComponentList.push(comboObject);
			embedDiv.appendChild(comboObject);
		} else {
			var textObject = document.createElement("input");
			textObject.type = "text";
			textObject.id = parameterKey;
			textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			if (customCodeObj.originalValue) {
				//if edit existing
				textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(customCodeObj.originalValue);
			}
			this._customInputComponentList.push(textObject);
			embedDiv.appendChild(textObject);
		}
		uom = customCodeObj.uomSymbol;
		if (uom.length > 0) {
			label = " " + uom;
			span = document.createElement("span");
			span.setAttribute("style", "margin-left:2px; margin-right:2px;");
			span.appendChild(document.createTextNode(label));
			embedDiv.appendChild(span);
		}
	}
	
	
	ActivityRemarksInline.prototype.checkIfFormHasError = function() {
		//brute force?
		var hasError = false;
		if (this._node) {
			if (this._node.getFieldErrors("activityDescription") != null) {
				hasError = true;
			}
		}
		return hasError;
	}
	
	ActivityRemarksInline.prototype.createUserInputObject = function(index, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		
		if (this._lookupModuleParams.length >= index) {
			var obj = this._lookupModuleParams[index];
			if (obj != null) {
				if (this._lookupList.hasOwnProperty(obj.value)) {
					var comboObject = document.createElement("select");
					comboObject.id = obj.value;
					comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					lookupRef = this._lookupList[obj.value];
					
					if (lookupRef != null) {
						var option = document.createElement("option");

						D3.forEach(lookupRef.getActiveLookup(), function(item) {
							option = document.createElement("option");
							option.setAttribute("value", item.data);
							option.appendChild(document.createTextNode(item.label));
							comboObject.options.add(option);
						});
						var that = this;
						comboObject.onchange = function(){that.setNodeDynaAttrFTR(that);};
						if (this.checkIfFormHasError()) {
							
						} else {
							if (obj.originalValue) {
								this.setSelectedValue(comboObject, obj.originalValue);
								this.triggerChangeDynaAttr = true;
							}
						}
					}
					this._userInputComponentList.push(comboObject);
					embedDiv.appendChild(comboObject);
				} else {
					var textObject = document.createElement("input");
					textObject.type = "text";
					textObject.id = obj.value;
					textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					textObject.setAttribute("placeholder", this._placeholder[index]);
					var textObjSize = 4;
					if (this._placeholder[index]) {
						textObjSize = this._placeholder[index].length;
					}
					textObject.setAttribute("size", textObjSize);
					if (this.checkIfFormHasError()) {
						
					} else {
						if (obj.originalValue) {
							//if edit existing
							textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(obj.originalValue);
							this.triggerChangeDynaAttr = true;
						}
					}
					var that = this;
					textObject.onkeyup = function() {that.setNodeDynaAttrFTR(that);};
					
					this._userInputComponentList.push(textObject);
					embedDiv.appendChild(textObject);
				}
				
				uom = this._lookupParamsUOM[index];
				if (uom.length > 0) {
					label = " " + uom;
					span = document.createElement("span");
					span.setAttribute("style", "margin-left:2px; margin-right:2px;");
					span.appendChild(document.createTextNode(label));
					embedDiv.appendChild(span);
				}
				
				return;
			}
		}
		
		label = "#";
		return;
	}
	
	ActivityRemarksInline.prototype.btnCancel_onClick = function(event){
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnCancelInitial_onClick = function(event){
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnBack_onClick = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
		this.loadXMLCustomParameters(response);
	}

	ActivityRemarksInline.prototype.checkInputEntered = function(that) {
		var hasEntered = false;
		
		for (var i=0; i < that._userInputComponentList.length; i++) {
			var uiObj = that._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			if (value) {
				hasEntered = true;
			}
		}
		return hasEntered;
	}
	
	ActivityRemarksInline.prototype.clearInputFields = function() {
		for (var i=0; i < this._userInputComponentList.length; i++) {
			var uiObj = this._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				uiObj.selectedIndex = 0;
				this._lookupModuleParams[i].originalValue = 0;
			} else {
				uiObj.value = "";
				this._lookupModuleParams[i].originalValue = "";
			}
		}
	}
	
	ActivityRemarksInline.prototype.setNodeDynaAttrFTR = function(that) {
		var outputList = "";
		var result = "";
		var errList = "";
		
		var hasEnteredSomething = that.checkInputEntered(that);

		if (hasEnteredSomething) {
			for (var i=0; i < that._userInputComponentList.length; i++) {
				var uiObj = that._userInputComponentList[i];
				var value = "";
				if (uiObj.type == "select-one") {
					value = uiObj.options[uiObj.selectedIndex].value;
				} else {
					value = uiObj.value;
				}
				
				result = that.validate(i, value);
				if (result.length > 0) {
					if (errList.length > 0) {
						errList += "\n";
					} 
					errList += result;
				}
				else {
					var obj = that._lookupModuleParams[i];
					if (obj != null) {
						if (outputList.length > 0) {
							outputList += "::";
						}
						var dataType = obj.label;
						var precision = that._lookupParamsPrecision[i];
						if (dataType == "double" && value.length != 0) {
							// Convert to raw number
							value = that.numberFormatter.parseNumberString(value.toString(), that._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, that._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
							
							// Process
							value = that.numberFormatter.formatRoundingWithPrecision(value, D3.NumberBase.NEAREST, precision);
							value = that.numberFormatter.formatThousands(value);
							value = that.numberFormatter.formatDecimal(value);
							value = that.numberFormatter.formatPrecision(value, precision);
							
							// Convert back to locale specific format
							value = that.numberFormatter.localizeNumberString(value.toString(), that._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, that._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
						}
						outputList = outputList + obj.value + ":=" + value;
					}
				}
			}
		}
		
		that.getCustomCodeInputEntered();
		outputList = that.appendConstantValue(outputList);

		if (errList.length == 0) {
			that.setFieldValue(outputList); //important to set @activityFreeTextRemark

			if (hasEnteredSomething) {
				var remark = that.populateFreeTextRemark();
				that._node.setFieldValue("additionalRemarks", remark.replace(/;/g, "\n"));
			} else {
				if ((this._selectedRemark.length > 0) && (this._selectedRemark.indexOf("#") <= 0)) {
					that._node.setFieldValue("additionalRemarks", this._selectedRemark.replace(/;/g, "\n"));
				}else{
					that._node.setFieldValue("additionalRemarks", "");
				}
			}
			that._node.setFieldValue("@customCodeParamKey", that._customCodeParamKey);
			that._node.setFieldValue("@customCodeParamValue", that._customCodeParamValue);
			that._node.setFieldValue("@customCodeParamUid", that._customCodeParamUid);
			
		} else {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		
	}
	
	ActivityRemarksInline.prototype.getCustomCodeInputEntered = function() {
		this._customCodeParamKey = "";
		this._customCodeParamValue = "";
		this._customCodeParamUid = "";
		
		if (this._customInputParams) {
			for (var j=0; j < this._customInputParams.length; j++) {
				var obj2 = this._customInputParams[j];
				var value2 = obj2.value;
				
				if (value2 != null && value2.length == 0) {
					value2 = "null";
				}
				if (this._customCodeParamKey.length > 0) {
					this._customCodeParamKey += ":.:.:";
				}
				if (this._customCodeParamValue.length > 0) {
					this._customCodeParamValue += ":.:.:";
				}
				if (this._customCodeParamUid.length > 0) {
					this._customCodeParamUid += ":.:.:";
				}
				this._customCodeParamKey += obj2.label;
				this._customCodeParamValue += value2;
				this._customCodeParamUid += obj2.customCodeUid;
			}
		}
	} 
	
	ActivityRemarksInline.prototype.btnRetrieve_onClick = function(event) {
		this.customCodeParams = this.getParamsFTR();
		
		var result = "";
		var errList = "";
		var parameterKeyArray = [];
		var valueArray = [];
		var customCodeUidArray = [];
		for (var i=0; i < this._customInputComponentList.length; i++) {
			var uiObj = this._customInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			result = this.validateCustomInput(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				}
				errList += result;
			}
			else {
				var obj = this._CustomInputList[i];
				if (obj != null) {
					parameterKeyArray[i] = obj.parameterKey;
					valueArray[i] = value;
					customCodeUidArray[i] = obj.customCodeUid;
				}
			}
		}
		
		if (errList.length != 0) {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		this.customCodeParams.outputParameterArray = parameterKeyArray;
		this.customCodeParams.outputValueArray = valueArray;
		this.customCodeParams.outputCustomCodeUidArray = customCodeUidArray;
		
		var response = this._node.commandBeanProxy.customInvokeSynchronous(this.customCodeParams);
		this.loadXMLFreeTextRemarks(response);

	}
	
	ActivityRemarksInline.prototype.validate = function(index, value) {
		var obj = this._lookupModuleParams[index];
		
		if (obj != null) {
			var dataType = obj.label;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.validateCustomInput = function(index, value) {
		var obj = this._CustomInputList[index];
		if (obj != null) {
			var dataType = obj.dataType;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.setSelectedValue = function(comboBox, selectedValue) {
		for (var i=0; i < comboBox.length; i++) {
			if (comboBox.options[i].value == selectedValue) {
				comboBox.selectedIndex = i;
				break;
			}
		}
	}
	
	ActivityRemarksInline.prototype.appendConstantValue = function(bufferList) {
		if (this._lookupModuleConstant != null) {
			var obj;
			
			for (var index = 0;index < this._lookupModuleConstant.length; index++) {
				obj = this._lookupModuleConstant[index];
				if (obj != null) {
					if (bufferList.length > 0) {
						bufferList += "::";
					} 
					bufferList += obj.value + ":=" + obj.label; 
				}
			}
		}
		return bufferList;
	}
	
	ActivityRemarksInline.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "collectCustomParameters";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksInline.prototype.getParamsFTR = function() {
		var params = {};
		params._invokeCustomFilter = "collectFreeTextRemarks";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksInline.prototype.populateFreeTextRemark = function(){
		var buffer = this._selectedRemark;
		if ((buffer.length>0) && (buffer.indexOf("#") >= 0)) {
			var uomSymbol = "";
			var strOutput = "";
			var componentIndex = 0;
			var temp = buffer.split("#");
			var uiObj = null;
			
			var lastIndex = temp.length - 1;
			for (var i=0; i<=lastIndex; i++) {
				if (i != lastIndex) {
					if (this._userInputComponentList.length > componentIndex) {
						uiObj = this._userInputComponentList[componentIndex];
						if (uiObj == null) {
							strOutput += temp[i].toString() + "#";
						} else {
							var value = "";
							if (uiObj.type == "select-one") {
								value = uiObj.options[uiObj.selectedIndex].label;
							} else {
								value = uiObj.value;
							}
							if (value) {
								strOutput += temp[i].toString();
								var obj = this._lookupModuleParams[componentIndex];
								var dataType = obj.label;
								var precision = this._lookupParamsPrecision[componentIndex];
								if (dataType == "double" && value.length != 0 && uiObj.type != "select-one") {
									var strValue = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strValue = this.numberFormatter.formatRoundingWithPrecision(strValue, D3.NumberBase.NEAREST, precision);
									strValue = this.numberFormatter.formatThousands(strValue);
									strValue = this.numberFormatter.formatDecimal(strValue);
									strValue = this.numberFormatter.formatPrecision(strValue, precision);
									strValue = this.numberFormatter.localizeNumberString(strValue.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strOutput += strValue;
								} else {
									strOutput += value;
								}
								if (this._lookupParamsUOM.length > componentIndex) {
									uomSymbol = this._lookupParamsUOM[componentIndex];
									if (uomSymbol !=""){
										strOutput += " " + uomSymbol;
									}
								}
							} else {
								//remove prefix and suffix of parameter if empty
								var processedWord = "";
								if (this._prefix) {
									processedWord = temp[i].toString().replace(this._prefix[componentIndex], "");
									strOutput += processedWord;
								}
								if (this._suffix) {
									processedWord = temp[i+1].toString().replace(this._suffix[componentIndex], "");
									temp[i+1] = processedWord;
								}
							}
						}
						componentIndex++;
					} else {
						strOutput += "#";
					}
				} else {
					strOutput += temp[i].toString();
				}
			}
			buffer = strOutput;
		}
		return buffer;
	}
	
	ActivityRemarksInline.prototype.allowFieldShow = function(){
		var showButton = this._node.getFieldValue("@showFTRButton") == "1" ? true : false;
		return showButton;
	}
	
	ActivityRemarksInline.prototype.refreshFieldEditor = function() {
	};
	
	ActivityRemarksInline.prototype.refreshFieldRenderer = function() {
		
	};
	
	D3.inherits(ActivityRemarksPopulation,D3.AbstractFieldComponent);
	
	function ActivityRemarksPopulation(){
		D3.AbstractFieldComponent.call(this);
		this.numberFormatter = new D3.NumberBase();
		
	}
	
	ActivityRemarksPopulation.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		
		var labelFTR = D3.LanguageManager.getLabel("button.FTR");
		var labelClear = D3.LanguageManager.getLabel("button.clear");
		var divConfig = "<div>" +
				"<table>" +
					"<tr>" +
						"<td style='padding-left:40px;'><button class='DefaultButton buttonFTR'>"+labelFTR+"</button></td>" +
						"<td style='padding-left:3px;'><button class='DefaultButton buttonClear'>"+labelClear+"</button></td>" +
					"</tr>" +
				"</table>" + "</div>";
		
		if (!this._fieldEditor && this.allowFieldShow()) {
			var container = $(divConfig);
			this._container = container.get(0);
			var that = this;
			this._btnFTR = container.find(".buttonFTR");
			this._btnFTR.click(function() {
				that.populateRemarksEventHandler();
			});
			
			this._btnClear = container.find(".buttonClear");
			this._btnClear.click(function() {
				that.clearEventHandler();
			});
			
			fieldEditor = this._container;
			this._fieldEditor = fieldEditor;
			
		} else if(this._node.getFieldValue("additionalRemarks")!=null && this._node.getFieldValue("additionalRemarks")!=""){
			//if(!this._fieldEditor == null && !this.allowFieldShow())
			if(!this._fieldEditor && !this.allowFieldShow())
			{
				var container = $(divConfig);
				this._container = container.get(0);
				var that = this;
				this._btnFTR = container.find(".buttonFTR");
				this._btnFTR.addClass("hide");
				
				this._btnClear = container.find(".buttonClear");
				this._btnClear.click(function() {
					that.clearEventHandler();
				});
				
				fieldEditor = this._container;
				//this._editor.addChild(this._editorClear);
				this._fieldEditor = fieldEditor;
			}
		}else {
			ActivityRemarksPopulation.uber.getFieldEditor.call(this);
		}
		return this._fieldEditor;
	}
	
	ActivityRemarksPopulation.prototype.dispose = function() {
		ActivityRemarksPopulation.uber.dispose.call(this);
		
		this._FreeTextRemarksList = null;
		this._lookupModuleParams = null;
		this._lookupModuleConstant = null;
		this._lookupParamsUOM = null;
		this._lookupList = null;
		this._userInputComponentList = null;
		this._CustomInputList = null;
		this._customInputLookupList = null;
		this._customInputComponentList = null;
		this._customInputParams = null;
		this._customCodeParamKey = null;
		this._customCodeParamValue = null;
		this._customCodeParamUid = null;
	}
	
	ActivityRemarksPopulation.prototype.populateRemarksEventHandler = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksPopulation.prototype.clearEventHandler = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
	}
	
	ActivityRemarksPopulation.prototype.loadXMLCustomParameters = function(response) {
		//collect custom parameters
		this.populateCustomInputList(response);
		if (this._customInputSize == 0) {
			this.populateRemarkList(response);
			this.generatePopUpWindow();
		} else {
			this.generateCustomInputPopUpWindow();
		}
	}
	
	ActivityRemarksPopulation.prototype.loadXMLFreeTextRemarks = function(response) {
		//collect free text remarks
		this.populateCustomInputParameterFromUser(response);
		this.populateRemarkList(response);
		this.generatePopUpWindow();
	}
	
	ActivityRemarksPopulation.prototype.populateRemarkList = function(response) {
		var collection = [];
		
		var doc = $(response);
		doc.find("freeTextRemarks").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("activityDescriptionMatrixUid").first().text();
			newItem.label = elm.find("remarks").first().text();
			
			collection.push(newItem);
		});
		this._FreeTextRemarksList = collection;
		if (this._FreeTextRemarksList.length == 1) {
			this.populateParamList(response);
		}
	}
	
	ActivityRemarksPopulation.prototype.populateCustomInputParameterFromUser = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("outputParameterArray").each(function(index, element){
			var elm = $(this);
			var skip = elm.find("skip").text();
			if (skip) {
				elm.find("item").each(function(index2, element2){
					var newItem = {};
					var childElm = $(this);
					newItem.index = childElm.find("index").first().text();
					newItem.label = childElm.find("label").first().text(); //parameter
					newItem.value = childElm.find("value").first().text(); //value
					newItem.customCodeUid = childElm.find("customCodeUid").first().text(); //customcodeuid
					collection.push(newItem);
				});
			}
		});
		this._customInputParams = collection;
	}
	
	ActivityRemarksPopulation.prototype.populateCustomInputList = function(response) {
		var collection = [];
		this._customInputLookupList = [];
		var that = this;
		
		var doc = $(response);
		var customParameter = doc.find("customParameter").get(0);
		this._customInputSize = $(customParameter).attr("size");
		

			if (this._customInputSize > 0) {
				doc.find("customItem").each(function(index,customItem){
					var newItem = {};
					var elm = $(this);
					newItem.sequence = elm.find("sequence").first().text();
					newItem.parameterKey = elm.find("parameterKey").first().text();
					newItem.label = elm.find("label").first().text();
					newItem.paramType = elm.find("paramType").first().text();
					newItem.dataType = elm.find("dataType").first().text();
					newItem.customCodeUid = elm.find("customCodeUid").first().text();
					newItem.precision = elm.find("precision").first().text();
					newItem.uomSymbol = elm.find("uomSymbol").first().text();
					newItem.lookupKey = elm.find("lookupKey").first().text();
					newItem.originalValue = elm.find("originalValue").first().text(); //if editing existing
					collection.push(newItem);
					
					var custItemLookup = elm.find("custItemLookup");
					if (custItemLookup != null) {
						var newLookupRef = new D3.LookupReference();
						var newLookup = new D3.LookupItem();
						newLookup.data = "";
						newLookup.label = "";
						newLookupRef.addLookupItem(newLookup);
						
						var idx = 0;
						
						elm.find("item").each(function(index2, element2){
							newLookup = new D3.LookupItem();
							var childElm = $(this);
							
							newLookup.data = childElm.find("key").first().text();
							newLookup.label = childElm.find("label").first().text();
							newLookupRef.addLookupItem(newLookup);
							idx++;
						});
						if (idx > 0) {
							var parameterKey = elm.find("parameterKey").first().text();
							//var obj = {};
							//obj[parameterKey] = newLookupRef;
							//that._customInputLookupList.push(obj);
							that._customInputLookupList[parameterKey] = newLookupRef;
						}
					}

				});
			}
			
			this._CustomInputList = collection;
	}
	
	ActivityRemarksPopulation.prototype.populateParamList = function(response) {
		this._lookupModuleParams = [];
		this._lookupParamsUOM = [];
		this._lookupParamsPrecision = [];
		this._placeholder = [];
		this._prefix = [];
		this._suffix = [];
		
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		doc.find("paramDetail").each(function(index, element){
			var elm = $(this);
			idx = elm.find("sequence").first().text();
			
			var newItem = {};
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("dataType").first().text();
			newItem.originalValue = elm.find("paramValue").first().text();
			newItem.precision = elm.find("precision").first().text();
			
			that._lookupModuleParams[idx] = newItem;
			that._lookupParamsPrecision[idx] = elm.find("precision").first().text();
			that._lookupParamsUOM[idx] = elm.find("uomSymbol").first().text();
			that._placeholder[idx] = elm.find("placeholder").first().text();
			that._prefix[idx] = elm.find("prefix").first().text();
			that._suffix[idx] = elm.find("suffix").first().text();
		});
		
		this.populateConstantList(response);
		this.populateLookupList(response);
	}
	
	ActivityRemarksPopulation.prototype.populateConstantList = function(response) {
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		that._lookupModuleConstant = [];
		doc.find("constantDetail").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("paramValue").first().text();
			
			that._lookupModuleConstant[idx] = newItem;
			idx++;
		});
	}
	
	ActivityRemarksPopulation.prototype.populateLookupList = function(response) {
		var idx = 0;
		var lookupID = "";
		var that = this;
		this._lookupList = [];
		
		var doc = $(response);
		doc.find("lookup").each(function(index, element){
			var elm = $(this);
			lookupID = elm.attr("id");
			
			idx = 0;
			
			var lookupItem = new D3.LookupItem();
			lookupItem.data = "";
			lookupItem.label = "";
			var lookupRef = new D3.LookupReference();
			lookupRef.addLookupItem(lookupItem);
			
			elm.find("item").each(function(index2, element2){
				var childElm = $(this);
				lookupItem = new D3.LookupItem();
				lookupItem.data = childElm.find("key").first().text();
				lookupItem.label = childElm.find("label").first().text();
				lookupRef.addLookupItem(lookupItem);
				idx++;
			});
			
			if (idx > 0) {
				that._lookupList[lookupID] = lookupRef;
			}
		});
	}
	
	ActivityRemarksPopulation.prototype.generatePopUpWindow = function() {
		var that = this;
		var btnOK = {
				text:D3.LanguageManager.getLabel("button.ok"),
				click:function(event){
					that.btnOK_onClick(event)
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				click:function(event) {
					that.btnCancel_onClick(event)
				}
		};
		var btnBack = {
				text:D3.LanguageManager.getLabel("button.back"),
				click:function(event) {
					that.btnBack_onClick(event)
				}
		}
		
		if (this._FreeTextRemarksList.length > 0) {
			var obj = this._FreeTextRemarksList[0];
			if (obj.label != null) {
				this._selectedRemark = obj.label;
			}
			if ((this._selectedRemark.length > 0) && (this._selectedRemark.indexOf("#") < 0)) {
				this.setFieldValue("");
				this._node.commandBeanProxy.addAdditionalFormRequestParams("auto_populate_description", this._selectedRemark);
				this.getCustomCodeInputEntered();
				this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_key", this._customCodeParamKey);
				this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_value", this._customCodeParamValue);
				this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);
			} else {
				
				this._panel = document.createElement("div");
				
				var dynamicObj = this.generateDynamicObject(this._selectedRemark);
				var buttons = null;
				
				if (this._customInputSize > 0) {
					buttons = [btnOK, btnBack, btnCancel];
				}
				else {
					buttons = [btnOK, btnCancel];
				}
				if (dynamicObj) {
					this._panel.appendChild(dynamicObj);
					var pnl = $(this._panel);
					
					pnl.dialog({
								resizable: true,
						        modal: true,
						        buttons: buttons,
						        dialogClass:"ftrPopup",
						        open: function(event, ui) { 
						            //hide close button.
						        	//$(".ui-dialog-titlebar-close", ui.dialog).hide();
						        	$(".ui-dialog-titlebar", ui.dialog).hide();
						        },
							});
				} else {
					var that = this;
					this._panel = document.createElement("div");
					this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
					var pnl = $(this._panel);
					var ok = {
						text:D3.LanguageManager.getLabel("button.ok"),
						click:function(){
							that.createCustomInputPopup();
							pnl.dialog('close');
						}
					};
					var button = [ok];
					pnl.dialog({
						modal:true,
						dialogClass:"ftrPopup",
						buttons: button
					});
				}
			}
		} else {
			this._panel = document.createElement("div");
			this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
			var pnl = $(this._panel);
			pnl.dialog({modal:true,dialogClass:"ftrPopup"});
		}
	}
	
	ActivityRemarksPopulation.prototype.createCustomInputPopup = function() {
		this.generateCustomInputPopUpWindow();
	}
	
	ActivityRemarksPopulation.prototype.generateCustomInputPopUpWindow = function() {
		var that = this;
		var btnRetrieve = {
				text:D3.LanguageManager.getLabel("button.retrieveFTR"),
				click:function(event){
					that.btnRetrieve_onClick(event)
				}
		}
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				click:function(event){
					that.btnCancelInitial_onClick(event)
				}
		};
		
		var div = document.createElement("div");
		
		if (this._customInputSize > 0) {
				this._panelInitial = document.createElement("div");
				
				var result = this.generateCustomInputFields();
				if (result) {
					this._panelInitial.appendChild(result);
					
					var pnl = $(this._panelInitial);
					pnl.dialog({
				        resizable: true,
				        modal: true,
				        buttons: [btnRetrieve,btnCancel],
				        dialogClass:"ftrPopup",
				        open: function(event, ui) { 
				            //hide close button.
				        	//$(".ui-dialog-titlebar-close", ui.dialog).hide();
							$(".ui-dialog-titlebar", ui.dialog).hide();
				        },
				    });
				}
		}
		else {
			this.generatePopUpWindow();
		}
		
		
	}
	
	ActivityRemarksPopulation.prototype.generateCustomInputFields = function() {
		var label = null;
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.setAttribute("style", "padding:1px;");
		
		table.appendChild(tr);
		tr.appendChild(td);
		var span = document.createElement("span");
		
		this._customInputComponentList = [];
		
		if (this._customInputSize > 0) {
			for (var i = 0; i < this._CustomInputList.length; i++) {
				var customCodeObj = this._CustomInputList[i];
				var value = "";
				label = document.createTextNode(customCodeObj.label.toString());
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px;");
				tr.appendChild(td);
				var embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width: auto;");
				span = document.createElement("span");
				span.setAttribute("style", "margin-left:2px; margin-right:2px;");
				span.appendChild(label);
				embedDiv.appendChild(span);
				td.appendChild(embedDiv);
				this.createCustomInputObject(customCodeObj.parameterKey, i, embedDiv);
			}
		}
		return table;
	}
	
	ActivityRemarksPopulation.prototype.generateDynamicObject = function(buffer) {

		this._userInputComponentList = [];
		
		if (buffer.length > 0) {
			var label = null;
			var textbox = null;
			var table = document.createElement("table");
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			var span = document.createElement("span");
			td.setAttribute("style", "padding:1px");
			
			var embedDiv = document.createElement("div");
			embedDiv.setAttribute("style", "width:auto;");
			
			var boxIndex = 0;
			if (buffer.indexOf(";") >= 0) {
				var temp = [];
				temp = buffer.split(";");
				for(var index = 0; index < temp.length; index++) {
					tr = document.createElement("tr");
					table.appendChild(tr);
					td = document.createElement("td");
					td.setAttribute("style", "padding:1px");
					tr.appendChild(td);
					
					embedDiv = document.createElement("div");
					embedDiv.setAttribute("style", "width:auto;");
						
					if ($.trim(temp[index].toString()).length > 0) {
						if (temp[index].indexOf("#") >= 0) {
							var temp1 = [];
							temp1 = temp[index].split(/(#)/g);
							
							for(var index1 = 0; index1 < temp1.length; index1++) {
								
								var toProcess = $.trim(temp1[index1].toString());
								var nonEmptyString = toProcess.length > 0;
								
								if (nonEmptyString) {
									if (toProcess == "#") {
										this.createUserInputObject(boxIndex, embedDiv);	 
										boxIndex++;
									} else {
										label = document.createTextNode(toProcess);
										span = document.createElement("span");
										span.setAttribute("style", "margin-left:2px; margin-right:2px;");
										span.appendChild(label);
										embedDiv.appendChild(span);
									}
								}
							}
						} else {
							label = document.createTextNode(temp[index].toString());
							span = document.createElement("span");
							span.setAttribute("style", "margin-left:2px; margin-right:2px;");
							span.appendChild(label);
							embedDiv.appendChild(span);
						}
					}
					td.appendChild(embedDiv);
				}
				
			} else {
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px");
				tr.appendChild(td);
				
				embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width:auto;");
				
				if ($.trim(buffer.toString()).length > 0) {
					//label = document.createTextNode(buffer.toString());
					//embedDiv.appendChild(label);
					//td.appendChild(embedDiv);
						
					if (buffer.indexOf("#") >= 0) {
						var temp1 = [];
						temp1 = buffer.split(/(#)/g);
							
						for(var index1 = 0; index1 < temp1.length; index1++) {
								
							var toProcess = $.trim(temp1[index1].toString());
							var nonEmptyString = toProcess.length > 0;
								
							if (nonEmptyString) {
								if (toProcess == "#") {
									this.createUserInputObject(boxIndex, embedDiv);	 
									boxIndex++;
								} else {
									label = document.createTextNode(toProcess);
									span = document.createElement("span");
									span.setAttribute("style", "margin-left:2px; margin-right:2px;");
									span.appendChild(label);
									embedDiv.appendChild(span);
								}
							}
						}
					} else {
						label = document.createTextNode(buffer.toString());
						span = document.createElement("span");
						span.setAttribute("style", "margin-left:2px; margin-right:2px;");
						span.appendChild(label);
						embedDiv.appendChild(span);
					}

					td.appendChild(embedDiv);
				}
			}
			return table;
		}
		return null;
	}
	
	ActivityRemarksPopulation.prototype.createCustomInputObject = function(parameterKey, i, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		var customCodeObj = this._CustomInputList[i];
		
		if (this._customInputLookupList.hasOwnProperty(parameterKey)) {
			var comboObject = document.createElement("select");
			comboObject.id = parameterKey;
			comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			
			lookupRef = this._customInputLookupList[parameterKey];
			
			if (lookupRef != null) {
				var option = document.createElement("option");

				D3.forEach(lookupRef.getActiveLookup(), function(item) {
					option = document.createElement("option");
					option.setAttribute("value", item.data);
					option.appendChild(document.createTextNode(item.label));
					comboObject.options.add(option);
				});
				
				this.setSelectedValue(comboObject, customCodeObj.originalValue); //if edit existing
			}
			this._customInputComponentList.push(comboObject);
			embedDiv.appendChild(comboObject);
		} else {
			var textObject = document.createElement("input");
			textObject.type = "text";
			textObject.id = parameterKey;
			textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			if (customCodeObj.originalValue) {
				//if edit existing
				textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(customCodeObj.originalValue);
			}
			this._customInputComponentList.push(textObject);
			embedDiv.appendChild(textObject);
		}
		uom = customCodeObj.uomSymbol;
		if (uom.length > 0) {
			label = " " + uom;
			span = document.createElement("span");
			span.setAttribute("style", "margin-left:2px; margin-right:2px;");
			span.appendChild(document.createTextNode(label));
			embedDiv.appendChild(span);
		}
	}
	
	ActivityRemarksPopulation.prototype.createUserInputObject = function(index, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		
		if (this._lookupModuleParams.length >= index) {
			var obj = this._lookupModuleParams[index];
			if (obj != null) {
				if (this._lookupList.hasOwnProperty(obj.value)) {
					var comboObject = document.createElement("select");
					comboObject.id = obj.value;
					comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					lookupRef = this._lookupList[obj.value];
					
					if (lookupRef != null) {
						var option = document.createElement("option");

						D3.forEach(lookupRef.getActiveLookup(), function(item) {
							option = document.createElement("option");
							option.setAttribute("value", item.data);
							option.appendChild(document.createTextNode(item.label));
							comboObject.options.add(option);
						});
						
						this.setSelectedValue(comboObject, obj.originalValue);
					}
					this._userInputComponentList.push(comboObject);
					embedDiv.appendChild(comboObject);
				} else {
					var textObject = document.createElement("input");
					textObject.type = "text";
					textObject.id = obj.value;
					textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					textObject.setAttribute("placeholder", this._placeholder[index]);
					var textObjSize = 4;
					if (this._placeholder[index]) {
						textObjSize = this._placeholder[index].length;
					}
					textObject.setAttribute("size", textObjSize);
					if (obj.originalValue) {
						//if edit existing
						textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(obj.originalValue);
					}
					this._userInputComponentList.push(textObject);
					embedDiv.appendChild(textObject);
				}
				
				uom = this._lookupParamsUOM[index];
				if (uom.length > 0) {
					label = " " + uom;
					span = document.createElement("span");
					span.setAttribute("style", "margin-left:2px; margin-right:2px;");
					span.appendChild(document.createTextNode(label));
					embedDiv.appendChild(span);
				}
				
				return;
			}
		}
		
		label = "#";
		return;
	}
	
	ActivityRemarksPopulation.prototype.btnCancel_onClick = function(event){
		$(this._panel).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.btnCancelInitial_onClick = function(event){
		$(this._panelInitial).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.btnBack_onClick = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		$(this._panel).dialog('close');
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksPopulation.prototype.btnOK_onClick = function(event) {
		var outputList = "";
		var result = "";
		var errList = "";
		
		for (var i=0; i < this._userInputComponentList.length; i++) {
			var uiObj = this._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			
			result = this.validate(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				} 
				errList += result;
			}
			else {
				var obj = this._lookupModuleParams[i];
				if (obj != null) {
					if (outputList.length > 0) {
						outputList += "::";
					}
					var dataType = obj.label;
					var precision = this._lookupParamsPrecision[i];
					if (dataType == "double" && value.length != 0) {
						// Convert to raw number
						value = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
						
						// Process
						value = this.numberFormatter.formatRoundingWithPrecision(value, D3.NumberBase.NEAREST, precision);
						value = this.numberFormatter.formatThousands(value);
						value = this.numberFormatter.formatDecimal(value);
						value = this.numberFormatter.formatPrecision(value, precision);
						
						// Convert back to locale specific format
						value = this.numberFormatter.localizeNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
					}
					
					outputList = outputList + obj.value + ":=" + value;
				}
			}
		}
		
		this.getCustomCodeInputEntered();
		outputList = this.appendConstantValue(outputList);
		
		if (errList.length == 0) {
			this.setFieldValue(outputList); //important to set @activityFreeTextRemark
			
			$(this._panel).dialog('close');
			
			this._node.commandBeanProxy.addAdditionalFormRequestParams("auto_populate_description", this.populateFreeTextRemark());
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_key", this._customCodeParamKey);
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_value", this._customCodeParamValue);
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_uid", this._customCodeParamUid);
			this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);			
		} else {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		
	}
	
	ActivityRemarksPopulation.prototype.getCustomCodeInputEntered = function() {
		this._customCodeParamKey = "";
		this._customCodeParamValue = "";
		this._customCodeParamUid = "";
		
		if (this._customInputParams) {
			for (var j=0; j < this._customInputParams.length; j++) {
				var obj2 = this._customInputParams[j];
				var value2 = obj2.value;
				
				if (value2 != null && value2.length == 0) {
					value2 = "null";
				}
				if (this._customCodeParamKey.length > 0) {
					this._customCodeParamKey += ":.:.:";
				}
				if (this._customCodeParamValue.length > 0) {
					this._customCodeParamValue += ":.:.:";
				}
				if (this._customCodeParamUid.length > 0) {
					this._customCodeParamUid += ":.:.:";
				}
				this._customCodeParamKey += obj2.label;
				this._customCodeParamValue += value2;
				this._customCodeParamUid += obj2.customCodeUid;
			}
		}
	} 
	
	ActivityRemarksPopulation.prototype.btnRetrieve_onClick = function(event) {
		var params = this.getParamsFTR();
		
		var result = "";
		var errList = "";
		var parameterKeyArray = [];
		var valueArray = [];
		var customCodeUidArray = [];
		for (var i=0; i < this._customInputComponentList.length; i++) {
			var uiObj = this._customInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			result = this.validateCustomInput(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				}
				errList += result;
			}
			else {
				var obj = this._CustomInputList[i];
				if (obj != null) {
					parameterKeyArray[i] = obj.parameterKey;
					valueArray[i] = value;
					customCodeUidArray[i] = obj.customCodeUid;
				}
			}
		}
		
		if (errList.length != 0) {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		params.outputParameterArray = parameterKeyArray;
		params.outputValueArray = valueArray;
		params.outputCustomCodeUidArray = customCodeUidArray;
		
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLFreeTextRemarks(response);

		$(this._panelInitial).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.validate = function(index, value) {
		var obj = this._lookupModuleParams[index];
		
		if (obj != null) {
			var dataType = obj.label;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksPopulation.prototype.validateCustomInput = function(index, value) {
		var obj = this._CustomInputList[index];
		if (obj != null) {
			var dataType = obj.dataType;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.btnWitsml_onClick = function() {
		this.queryBox = new JSEventsMessageBox (this._node, this);
		var startDate = new Date(parseInt(this._node.getFieldValue("startDatetime")));
		var startDateInMiliseconds = (startDate.getUTCHours() * 60 * 60 * 1000) + (startDate.getUTCMinutes() * 60 * 1000);
		var endDate = new Date(parseInt(this._node.getFieldValue("endDatetime")));
		var endDateInMiliseconds = (endDate.getUTCHours() * 60 * 60 * 1000) + (endDate.getUTCMinutes() * 60 * 1000);
		
		if((isNaN(startDateInMiliseconds) || isNaN(endDateInMiliseconds)) || (startDateInMiliseconds > endDateInMiliseconds)) alert("Activity start/end time is not valid.");
		else {
			this.queryBox.startQuery(startDateInMiliseconds, endDateInMiliseconds);
		}
	}
	
	ActivityRemarksPopulation.prototype.setSelectedValue = function(comboBox, selectedValue) {
		for (var i=0; i < comboBox.length; i++) {
			if (comboBox.options[i].value == selectedValue) {
				comboBox.selectedIndex = i;
				break;
			}
		}
	}
	
	ActivityRemarksPopulation.prototype.appendConstantValue = function(bufferList) {
		if (this._lookupModuleConstant != null) {
			var obj;
			
			for (var index = 0;index < this._lookupModuleConstant.length; index++) {
				obj = this._lookupModuleConstant[index];
				if (obj != null) {
					if (bufferList.length > 0) {
						bufferList += "::";
					} 
					bufferList += obj.value + ":=" + obj.label; 
				}
			}
		}
		return bufferList;
	}
	
	ActivityRemarksPopulation.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "collectCustomParameters";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksPopulation.prototype.getParamsFTR = function() {
		var params = {};
		params._invokeCustomFilter = "collectFreeTextRemarks";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksPopulation.prototype.populateFreeTextRemark = function(){
		var buffer = this._selectedRemark;
		if ((buffer.length>0) && (buffer.indexOf("#") >= 0)) {
			var uomSymbol = "";
			var strOutput = "";
			var componentIndex = 0;
			var temp = buffer.split("#");
			var uiObj = null;
			
			var lastIndex = temp.length - 1;
			for (var i=0; i<=lastIndex; i++) {
				if (i != lastIndex) {
					if (this._userInputComponentList.length > componentIndex) {
						uiObj = this._userInputComponentList[componentIndex];
						if (uiObj == null) {
							strOutput += temp[i].toString() + "#";
						} else {
							var value = "";
							if (uiObj.type == "select-one") {
								value = uiObj.options[uiObj.selectedIndex].label;
							} else {
								value = uiObj.value;
							}
							if (value) {
								strOutput += temp[i].toString();
								var obj = this._lookupModuleParams[componentIndex];
								var dataType = obj.label;
								var precision = this._lookupParamsPrecision[componentIndex];
								if (dataType == "double" && value.length != 0 && uiObj.type != "select-one") {
									var strValue = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strValue = this.numberFormatter.formatRoundingWithPrecision(strValue, D3.NumberBase.NEAREST, precision);
									strValue = this.numberFormatter.formatThousands(strValue);
									strValue = this.numberFormatter.formatDecimal(strValue);
									strValue = this.numberFormatter.formatPrecision(strValue, precision);
									strValue = this.numberFormatter.localizeNumberString(strValue.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strOutput += strValue;
								} else {
									strOutput += value;
								}
								if (this._lookupParamsUOM.length > componentIndex) {
									uomSymbol = this._lookupParamsUOM[componentIndex];
									strOutput += " " + uomSymbol;
								}
							} else {
								//remove prefix and suffix of parameter if empty
								var processedWord = "";
								if (this._prefix) {
									processedWord = temp[i].toString().replace(this._prefix[componentIndex], "");
									strOutput += processedWord;
								}
								if (this._suffix) {
									processedWord = temp[i+1].toString().replace(this._suffix[componentIndex], "");
									temp[i+1] = processedWord;
								}
							}
						}
						componentIndex++;
					} else {
						strOutput += "#";
					}
				} else {
					strOutput += temp[i].toString();
				}
			}
			buffer = strOutput;
		}
		return buffer;
	}
	
	ActivityRemarksPopulation.prototype.allowFieldShow = function(){
		var showButton = this._node.getFieldValue("@showFTRButton") == "1" ? true : false;
		return showButton;
	}
	
	ActivityRemarksPopulation.prototype.refreshFieldRenderer = function() {
		
	};
	
	
	D3.inherits(ActivityMarNodeListener,D3.EmptyNodeListener);
	
	function ActivityMarNodeListener()
	{
		this._copyFields = ["planReference","planTaskReference","planDetailReference","classCode","phaseCode","taskCode","rootCauseCode","depthMdMsl","holeSize","jobTypeCode","businessSegment",
		                    "sections","incidentNumber","incidentLevel","ofInterest","userCode","equipmentCode","subEquipmentCode",
		      				"lookupCompanyUid","contractRateCode","tripNumber","failureType","failureCause",
		      				"@showFTRButton","conveyanceMethod","stringToDepthMdMsl"];
	}
	
	
	ActivityMarNodeListener.prototype.getCustomFieldComponent = function(id, node){

		if (id=="ActivityRemarksPopulation"){
			return new ActivityRemarksPopulation();
		}else if (id=="ActivityRemarksInline") {
			return new ActivityRemarksInline();
		}else {
			return null;
		}
	}
	
	ActivityMarNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "reprocess_data" ) {
			var params ={};
			params._invokeCustomFilter = "reprocessData";
			node.commandBeanProxy.submitForm();
			node.commandBeanProxy.customInvoke(params,this.loaded,this);
		}else if (id == "clear_data" ) {
			var params ={};
			params._invokeCustomFilter = "clearData";
			node.commandBeanProxy.submitForm();
			node.commandBeanProxy.customInvoke(params,this.loaded,this);
		}
	}
	
	ActivityMarNodeListener.prototype.loaded = function(response) {
		location.reload(); 
	}

	ActivityMarNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		
		var btn = null;
		if (id == "reprocess_data") {
			btn = $("<button class='DefaultButton' style='margin:3px'>Reprocess Data</button>");
			return btn.get(0);
		}else if (id == "clear_data") {
			if (parentNode.commandBeanProxy.rootNode.getFieldValue("@username")=="jwong"){
				btn = $("<button class='DefaultButton' style='margin:3px'>Clear Data</button>");
				return btn.get(0);
			}
		}
		 
		return null;
	}

	D3.CommandBeanProxy.nodeListener = ActivityMarNodeListener;
	
})(window);