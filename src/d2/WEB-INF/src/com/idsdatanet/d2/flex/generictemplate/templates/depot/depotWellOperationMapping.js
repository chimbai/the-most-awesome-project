(function(window){

	D3.inherits(SchedulerTemplateComboBox,D3.ComboBoxField);

	function SchedulerTemplateComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
	}

	SchedulerTemplateComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		SchedulerTemplateComboBox.uber.dispose.call(this);
	}

	SchedulerTemplateComboBox.prototype.afterDataChanged = function(event) {
		this._onChanged(event);
	}

	SchedulerTemplateComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireSchedulerTemplateChangedEvent(this._node.uid);
	}
	
	D3.inherits(DepotWellComboBox,D3.ComboBoxField);

	function DepotWellComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
	}

	DepotWellComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		DepotWellComboBox.uber.dispose.call(this);
	}

	DepotWellComboBox.prototype.onTemplateChanged = function(event) {
		this._refreshField(event);
	}

	DepotWellComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveWellList();
		}		
	}

	DepotWellComboBox.prototype.clearList = function() {
		this._wellListLookup = null;
		this._node.setFieldValue("depotWellUid","");
		this.refreshFieldEditor();
	};

	DepotWellComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotWellName");
		this.setFieldRendererValue(renderedText);
	}

	DepotWellComboBox.prototype.retrieveWellList = function() {
		var params = {};
		params._invokeCustomFilter = "get_well_list";
		params.schedulerTemplateUid = this._node.getFieldValue("schedulerTemplateUid");
		if(params.schedulerTemplateUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.wellLoadCompleted, this);
		}
	}

	DepotWellComboBox.prototype.getLookupRef = function() {
		return this._wellListLookup;
	};

	DepotWellComboBox.prototype.wellLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.well") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('well');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text() + " (" + item.find("uid").text() +")";
					lookupReference.addLookupItem(newItem);
				});
				this._wellListLookup = lookupReference;
				this._node.setFieldValue("depotWellUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.well"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	DepotWellComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	DepotWellComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._wellListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("depotWellUid", lookupItem.data);
		this._node.setFieldValue("depotWellName", lookupItem.label);
		this._onChanged(event);
	}

	DepotWellComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireWellChangedEvent(this._node.uid);
	}

	D3.inherits(DepotWellboreComboBox,D3.ComboBoxField);

	function DepotWellboreComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
	}

	DepotWellboreComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		DepotWellboreComboBox.uber.dispose.call(this);
	}

	DepotWellboreComboBox.prototype.onTemplateChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellboreComboBox.prototype.onWellChanged = function(event) {
		this._refreshField(event);
	}

	DepotWellboreComboBox.prototype.clearList = function() {
		this._wellboreListLookup = null;
		this._node.setFieldValue("depotWellboreUid","");
		this.refreshFieldEditor();
	};

	DepotWellboreComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveWellboreList();
		}
	}

	DepotWellboreComboBox.prototype.getLookupRef = function() {
		return this._wellboreListLookup;
	};

	DepotWellboreComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotWellboreName");
		this.setFieldRendererValue(renderedText);
	}

	DepotWellboreComboBox.prototype.retrieveWellboreList = function() {
		var params = {};
		params._invokeCustomFilter = "get_wellbore_list";
		params.schedulerTemplateUid = this._node.getFieldValue("schedulerTemplateUid");
		params.depotWellUid = this._node.getFieldValue("depotWellUid");
		if(params.depotWellUid != "" && params.schedulerTemplateUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.wellboreLoadCompleted, this);
		}
	}

	DepotWellboreComboBox.prototype.wellboreLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.wellbore") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('wellbore');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item) {
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text() + " (" + item.find("uid").text() +")";
					lookupReference.addLookupItem(newItem);
				});

				this._wellboreListLookup = lookupReference;
				this._node.setFieldValue("depotWellboreUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.wellbore"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	DepotWellboreComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._wellboreListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("depotWellboreUid", lookupItem.data);
		this._node.setFieldValue("depotWellboreName", lookupItem.label);
		this._onChanged(event);
	}

	DepotWellboreComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireWellboreChangedEvent(this._node.uid);
	}

	DepotWellboreComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}
	
	D3.inherits(ExecuteButton,D3.AbstractFieldComponent);

	function ExecuteButton(dispatcher) {
		D3.AbstractFieldComponent.call(this);
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_WELLBORE_CHANGED, this.onWellboreChangedChanged, this);
	}

	ExecuteButton.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SCHEDULER_TEMPLATE_CHANGED, this.onTemplateChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_WELLBORE_CHANGED, this.onWellboreChangedChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		ExecuteButton.uber.dispose.call(this);
	}

	ExecuteButton.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.buildButton();
			this._fieldEditor.style.visibility = 'hidden';
		}
		return this._fieldEditor;
	}

	ExecuteButton.prototype.refreshFieldEditor = function() {
		this._fieldEditor.value = this.getFieldValue();
		this.refresh();
	}

	ExecuteButton.prototype.buildButton = function() {
		var btnExecuteConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("label.button.execute"),
				css:"DefaultButton",
				callbacks:{
					onclick:this.onclick
				},
				context:this
		};
		return D3.UI.createElement(btnExecuteConfig);
	}	

	ExecuteButton.prototype.onclick = function(event) {
		this._schedulerTemplateUid = this._node.getFieldValue("schedulerTemplateUid");
		this._depotWellOperationMappingUid = this._node.getFieldValue("depotWellOperationMappingUid");
		if (this._schedulerTEmplateUid != "" && this._depotWellOperationMappingUid != "") {
			var wellnameLabel = this._node.commandBeanProxy.rootNode.getFieldValue("@wellOperationName");
			var dayLabel = this._node.commandBeanProxy.rootNode.getFieldValue("@dayLabel");
			var schedulerType = this._node.getFieldValue("@schedulerType");
			var displayedMessage = null;

			if (schedulerType == ExecuteButton.CONST_GET_FROM_STORE) {
				displayedMessage = D3.LanguageManager.getLabel("message.1.getprompt.execute") + " " + wellnameLabel + " " + D3.LanguageManager.getLabel("message.2.getprompt.execute") + " " + dayLabel + ". \n" + D3.LanguageManager.getLabel("message.3.getprompt.execute");
			} else {
				displayedMessage = D3.LanguageManager.getLabel("message.1.addprompt.execute") + " " + wellnameLabel + " " + D3.LanguageManager.getLabel("message.2.addprompt.execute") + " \n" + D3.LanguageManager.getLabel("message.3.addprompt.execute");
			}
			if (confirm(displayedMessage)) {
				this.onconfirmDialogBox()
			}
		}
	}

	ExecuteButton.prototype.onconfirmDialogBox = function() {
		this._schedulerEventDispatcher.fireLoadStartedEvent();
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params, this.completed, this);
	}

	ExecuteButton.prototype.getParams = function(response) {
		var params = {};
		params._invokeCustomFilter = "run_depot_scheduler";
		params.schedulerTemplateUid = this._node.getFieldValue("schedulerTemplateUid");
		params.depotWellOperationMappingUid = this._node.getFieldValue("depotWellOperationMappingUid");
		return params;
	}

	ExecuteButton.prototype.completed = function(response) {
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
		this.popUp(response);
	}

	ExecuteButton.prototype.popUp = function(response) {
		var _popUpDiv = "<div></div>";
		this._popUpDialog = $(_popUpDiv);
		this.processResponse(this._popUpDialog, response);
		this._popUpDialog.dialog({
			height:"150",
			width:"500",
			resizable:false,
			modal:true,
		    buttons: [{
		    	text: "OK",
		    	"id": "btnOk",
		    	click: function() {
		    		$(this).dialog("close");
		    	}
		    }]
		});
		var okLabel = D3.LanguageManager.getLabel("label.ok.popup.execute");
		$("#btnOk").html('<span class="ui-button-text">'+ okLabel +'</span>');
	}

	ExecuteButton.prototype.processResponse = function(container, response) {
		var r = $(response);
		var status = r.find("result").attr("status");
		if(status == "Success") {
			container.attr("title", D3.LanguageManager.getLabel("title.success.popup.execute"));
			var headerMsg = D3.LanguageManager.getLabel("header.success.popup.execute");
			var htmlText = "<div><b><font color='#108112'>" + headerMsg + "</font></b></div>";
		} else {
			container.attr("title", D3.LanguageManager.getLabel("title.error.popup.execute"));
			var headerMsg = D3.LanguageManager.getLabel("header.error.popup.execute");
			var htmlText = "<div><b><font color='#C91919'>" + headerMsg + "</font></b></div>";

			r.find("error").each(function(index, item) {
				htmlText += "<br/>" + "<div><font color='#C91919'>" + $(item).text() + "</font></div>";
			});
		}
		container.append(htmlText);

		var dynaAttr = r.find("dynaAttr");
		var that = this;
		if(dynaAttr) {
			dynaAttr.children().each(function(index, item) {
				var i = $(item);
				that._node.setFieldValue(("@" + i[0].nodeName), i.text());
			});
			this._node.fireRefreshNodeEvent();
		}
	}

	ExecuteButton.prototype.onTemplateChanged = function(event) {
		this._uid = this._node.uid;
		if(this._uid == event.nodeUid) {
			this.refresh();
		}
	}

	ExecuteButton.prototype.onWellChanged = function(event) {
		this._uid = this._node.uid;
		if(this._uid == event.nodeUid) {
			this.refresh();
		}
	}

	ExecuteButton.prototype.onWellboreChangedChanged = function(event) {
		this._uid = this._node.uid;
		if(this._uid == event.nodeUid) {
			this.refresh();
		}
	}

	ExecuteButton.prototype.refresh = function() {
		if(this._node.isClientSideNewlyAdded())
			return;

		var wellUid = this._node.getFieldValue("depotWellUid");
		var wellboreUid = this._node.getFieldValue("depotWellboreUid");
		if ((wellUid && wellUid != "") && (wellboreUid && wellboreUid != "") && (this._fieldEditor)) {
			this._fieldEditor.style.visibility = '';
		} else {
			this._fieldEditor.style.visibility = 'hidden';
		}
	}

	ExecuteButton.CONST_GET_FROM_STORE = "getFromStore";
	ExecuteButton.CONST_ADD_TO_STORE = "addToStore";

	D3.inherits(RSDComboBox, D3.ComboBoxField);
	function RSDComboBox() {
		D3.ComboBoxField.call(this);
	}
	
	RSDComboBox.prototype.data = function(data) {
		EAComboBox.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange,this);
	};
	
	RSDComboBox.prototype.fieldValueChange = function (event){
		if ((event.fieldName=="logPurpose")){
			this.refreshFieldEditor();
		}
	}
	
	RSDComboBox.prototype.refreshFieldEditor = function() {
		var currentLog = this._node.getFieldValue("logPurpose");
		if (currentLog=="EXT_ACT" || currentLog=="PUBLISH"){	
			this._fieldEditor.style.display = "none";
			this._node.setFieldValue("rigStateDefinitionUid","");
		}else{
			this._fieldEditor.style.display = "";
		}
		RSDComboBox.uber.refreshFieldEditor.call(this);
	};
	
	D3.inherits(LogTextField, D3.TextInputField);
	function LogTextField() {
		D3.TextInputField.call(this);
	}
	
	LogTextField.prototype.data = function(data) {
		EAComboBox.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange,this);
	};
	
	LogTextField.prototype.fieldValueChange = function (event){
		if ((event.fieldName=="logPurpose")){
			this.refreshFieldEditor();
		}
	}
	
	LogTextField.prototype.refreshFieldEditor = function() {
		var currentLog = this._node.getFieldValue("logPurpose");
		if (currentLog=="DP_FTR"){	
			this._fieldEditor.style.display = "";
		}else{	
			this._fieldEditor.style.display = "none";
			this._node.setFieldValue("logUidOverride","");
		}
		LogTextField.uber.refreshFieldEditor.call(this);
	};
	
	D3.inherits(EAComboBox, D3.ComboBoxField);
	function EAComboBox() {
		D3.ComboBoxField.call(this);
	}
	
	EAComboBox.prototype.refreshFieldRenderer = function (){
		EAComboBox.uber.refreshFieldRenderer.call(this);
	//	this.setDropDown();
	}

	EAComboBox.prototype.setDropDown = function (){
		var currentLog = this._node.getFieldValue("logPurpose");
		if (currentLog=="EXT_ACT"){
			this.fieldContainer._editable = true;
		} else {
			this.fieldContainer._editable = true;
		}
	}
	
	EAComboBox.prototype.data = function(data) {
		EAComboBox.uber.data.call(this, data);
	//	this.setDropDown();
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange,this);
	};
	
	EAComboBox.prototype.fieldValueChange = function (event){
		if ((event.fieldName=="logPurpose")){
			this.refreshFieldEditor();
		}
	}
	
	EAComboBox.prototype.refreshFieldEditor = function() {
		var currentLog = this._node.getFieldValue("logPurpose");
		if (currentLog=="EXT_ACT"){
			this._fieldEditor.style.display = "";
			EAComboBox.uber.refreshFieldEditor.call(this);
		}else if (currentLog==""){ 
			this._fieldEditor.style.display = "none";
			this._node.setFieldValue("activityExtCodeDefUid","");
		}else{
			this._fieldEditor.style.display = "none";
			this._node.setFieldValue("activityExtCodeDefUid","");
			EAComboBox.uber.refreshFieldEditor.call(this);
		}

	};
	
	D3.inherits(DepotWellOperationMappingDataNodeListener,D3.EmptyNodeListener);

	function DepotWellOperationMappingDataNodeListener() { 
		if (!this.dispatcher) {
			this._dispatcher = new D3.SchedulerEventDispatcher();
		}
	}

	DepotWellOperationMappingDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		var commandBean = node.commandBeanProxy;
		this._dispatcher._commandBean = commandBean;

		if(id == "schedulerTemplate"){
			return new SchedulerTemplateComboBox(this._dispatcher);	
		} else if(id == "depotWell"){
			return new DepotWellComboBox(this._dispatcher);	
		} else if(id == "depotWellbore"){
			return new DepotWellboreComboBox(this._dispatcher);			
		} else if(id == "executeButton"){
			return new ExecuteButton(this._dispatcher);		
		} else if(id == "RSD"){
			return new RSDComboBox();	
		} else if(id == "LOG"){
			return new LogTextField();
		} else if(id == "EA"){
			return new EAComboBox();	
		} else {
			return null;
		}
	}

	DepotWellOperationMappingDataNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		node.setFieldValue("schedulerStatus","stop");
	}

	D3.CommandBeanProxy.nodeListener = DepotWellOperationMappingDataNodeListener;

})(window);