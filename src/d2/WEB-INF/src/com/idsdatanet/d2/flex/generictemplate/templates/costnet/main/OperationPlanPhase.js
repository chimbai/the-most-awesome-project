/**
 * CostAccountCodes Table store fields.
 */

(function(window){
	
	function OperationPlanPhase(){
		
		this.id = "";
		this.sequence = "";
		this.phaseCode = "";
		this.phaseName = "";
		this.displayName = "";
		this.use = null;
	}
	
	OperationPlanPhase.prototype.setDisplayName = function(){
		this.displayName = this.sequence + " " + this.phaseName;
	}
	
	OperationPlanPhase.prototype.run = function(){

	}
	
	window.OperationPlanPhase = OperationPlanPhase;
})(window);