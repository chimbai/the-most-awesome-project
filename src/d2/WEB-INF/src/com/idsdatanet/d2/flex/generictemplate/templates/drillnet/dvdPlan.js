(function(window){
	D3.inherits(FileAttachmentLink,D3.AbstractFieldComponent);
	
	function FileAttachmentLink (){
		D3.AbstractFieldComponent.call(this);
	}
	
	FileAttachmentLink.prototype.afterPropertiesSet = function() {
		FileAttachmentLink.uber.afterPropertiesSet.call(this);
		if (this.fieldContainer) {
			this.fieldContainer.triggerEditOnMouseClick(false);
		}
	};
	
	FileAttachmentLink.prototype.getFieldRenderer = function (){
		if(!this._fieldRenderer){
			this._fieldRenderer = FileAttachmentLink.uber.getFieldRenderer.call(this);
		}
		var hasAttachment = this.getFieldValue();
		this._fieldRenderer = D3.UI.createElement({
			tag:"a",
			text:(hasAttachment == true ? D3.LanguageManager.getLabel("OperationPlanMaster.hasFileAttachmentLink.label") : D3.LanguageManager.getLabel("OperationPlanMaster.noFileAttachmentLink.label")),
			callbacks:{
				onclick:function(event) {
					var redirectUrl = "dvdplan.html?tab=filemanager";
					this._node.commandBeanProxy.gotoUrl(redirectUrl, "Loading content");
				}
			},
			context:this
		});
		
		return this._fieldRenderer;
	}

	FileAttachmentLink.prototype.refreshFieldRenderer = function(value) {
		return;
	};
	
	D3.inherits(DVDPlanNodeListener, D3.EmptyNodeListener);
	
	
	function DVDPlanNodeListener(){
		
	}
	
	DVDPlanNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "FileAttachmentLink"){
			return new FileAttachmentLink();
		}else{
			return null;
		}
	}
	
	DVDPlanNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		if(!this.commandBeanProxy){
			this.commandBeanProxy = parentNode.commandBeanProxy;
		}
		if(id == "calculatePlanStartAndEndDateTime"){
			var config = {
					tag:"button",
					text:D3.LanguageManager.getLabel("OperationPlanPhase.calculatePlanStartAndEndDateTimeButton.label"),
					css:"DefaultButton",
					context:this
			};
			if(this.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)
				&& parentNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "OperationPlanPhase")){
				return this._calculatePlanStartAndEndDateTimeBtn = D3.UI.createElement(config);
			}
		}else if(id == "generateDvdPlanReport"){
			var config = {
					tag:"button",
					text:D3.LanguageManager.getLabel("OperationPlanMaster.generateDvdPlanReport.label"),
					css:"DefaultButton",
					context:this
			};
			return this._btnGenerateAndDownloadReport = D3.UI.createElement(config);
		} else{
			return null;
		}
	}
	
	DVDPlanNodeListener.prototype.customButtonTriggered = function(node, action, button) {
		if(action == "calculatePlanStartAndEndDateTime"){
			var operationPlanPhases = node.children["OperationPlanPhase"].getItems();
			for(var i in operationPlanPhases){
				operationPlanPhases[i].setEditMode(true, false);
				operationPlanPhases[i].setSubmitFlag(true);
			}
			node.commandBeanProxy.addAdditionalFormRequestParams("action", "calculatePlanStartAndEndDateTime");
			node.commandBeanProxy.submitForServerSideProcess(node);
		} else if (action == "generateDvdPlanReport"){
			node.commandBeanProxy.addAdditionalFormRequestParams("action", "generateDvdPlanReport");
			node.commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report_and_download", "1");
			node.commandBeanProxy.submitForServerSideProcess(node);
			this._generateReportAndDownloadStarted = true;
			this.callLaterToCheckStatus();
		}
	}
	
	DVDPlanNodeListener.prototype.getConfirmationMessage = function(commandBeanProxy) {
		if(this.isSaveActionSet(commandBeanProxy.rootNode)){
			return D3.LanguageManager.getLabel("OperationPlanMaster.saveConfirmationMessage");
		}
	}
	
	DVDPlanNodeListener.prototype.isSaveActionSet = function(node){
		var self = this;
		
		if(node.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_SAVE){
			return true;
		}
		
		if(node.children){
			for(var i in node.children){
				var items = node.children[i].getItems();
				for(var j in items){
					var childNode = items[j];
					var saveActionStatus = self.isSaveActionSet(childNode);
					if(saveActionStatus){
						return true;
					}
				}
			}
		}

		return false;
	}
	
	DVDPlanNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "lockDVDPlan") {
			if (!commandBeanProxy.rootNode.isAllowedAction("lockDVDPlan", "OperationPlanMaster")) {
				return null;
			}
			
			var config = {
					tag:"button",
					css:"rootButtons"
			};
			this._btnLockDVDPlan = D3.UI.createElement(config);
			this.updateLockDvDPlanBtnLabel(commandBeanProxy, this._btnLockDVDPlan);
			this._btnLockDVDPlan.onclick = this.onClickLockDVDPlan.bindAsEventListener(this);
			return this._btnLockDVDPlan;
		}
	}
	
	DVDPlanNodeListener.prototype.onClickLockDVDPlan = function(){
		this.commandBeanProxy.submitForm("lockDVDPlan");
	}
	
	DVDPlanNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		this.updateLockDvDPlanBtnLabel(commandBeanProxy, this._btnLockDVDPlan);
	}
	
	DVDPlanNodeListener.prototype.updateLockDvDPlanBtnLabel = function(commandBeanProxy, btn){
		
		if(btn == null || btn == undefined || commandBeanProxy == null){
			return;
		}

		var buttonLabel = D3.LanguageManager.getLabel("OperationPlanMaster.lockDvdPlan.label");
		var plans = commandBeanProxy.rootNode.children["OperationPlanMaster"].getItems();
		if(plans != null){
			for(var i in plans){
				var plan = plans[i];
				if (!plan.atts.isNewRecord) {
					if (plan.getFieldValue("isLocked") == "1") {
						buttonLabel = D3.LanguageManager.getLabel("OperationPlanMaster.unLockDvdPlan.label");
						break;
					}
				}
			}
		}
		$(btn).text(buttonLabel);
	}
	
	DVDPlanNodeListener.prototype.enableButton = function(button,value){
		if (value)
			$(button).removeAttr("disabled");
		else
			$(button).attr("disabled","disabled");
	}
	
	DVDPlanNodeListener.prototype.setReadyToGenerateAndDownload = function(){
		$(this._btnGenerateAndDownloadReport).text(D3.LanguageManager.getLabel("OperationPlanMaster.generateDvdPlanReport.label"));
		this.enableButton(this._btnGenerateAndDownloadReport,true);	
	}
	
	DVDPlanNodeListener.prototype.setGeneratingReportAndDownloadInProgressAndCheckAgainLater = function(){
		$(this._btnGenerateAndDownloadReport).text("Please wait...");
		this.enableButton(this._btnGenerateAndDownloadReport,false);	

		this.callLaterToCheckStatus();
	}
	
	DVDPlanNodeListener.prototype.callLaterToCheckStatus = function(){
		var self = this;
		setTimeout(function(){self.checkingCurrentStatus()},1000);
	}
	
	DVDPlanNodeListener.prototype.checkingCurrentStatus = function() {
		var params = {};
		params._invokeCustomFilter = "check_report_status";
		this.commandBeanProxy.customInvoke(params,this.statusCheckCompleted,this);

		if (this._btnGenerateAndDownloadReport != null) {
			this.enableButton(this._btnGenerateAndDownloadReport,false);
		}
	}
	
	DVDPlanNodeListener.prototype.statusCheckCompleted = function(response) {
		var doc = $(response);
		var code = doc.find("responseCode").text();
		if (this._generateReportAndDownloadStarted) {
			if (code == "no_job") {
				this._generateReportAndDownloadStarted = false;
				this.setReadyToGenerateAndDownload();
			} else if (code == "job_done") {
				this._generateReportAndDownloadStarted = false;
				var reportDownloadPath = $(response).find("reference").text();
				if (reportDownloadPath) {
					this.downloadFile(reportDownloadPath);
				}
				this.setReadyToGenerateAndDownload();
			} else if (code == "job_error") {
				this._generateReportAndDownloadStarted = false;
				this.setReadyToGenerateAndDownload();
			} else if (code == "job_running") {
				this.setGeneratingReportAndDownloadInProgressAndCheckAgainLater();
			}
		} else {
			if (code == "no_job" || code == "job_done" || code == "job_error") {
				if (this._btnGenerateAndDownloadReport != null) {
					this.setReadyToGenerateAndDownload();
				}
			} else if (code == "job_running") {
				this._generateReportAndDownloadStarted = true;
				this.setGeneratingReportInProgressAndCheckAgainLater();
			}
		}
		
	}
	
	DVDPlanNodeListener.prototype.downloadFile = function(reportDownloadPath) {
		var fileUrl = "abaccess/download/" + reportDownloadPath;
		this.commandBeanProxy.openDownloadWindow(fileUrl);
	}


	D3.CommandBeanProxy.nodeListener = DVDPlanNodeListener;
	
})(window);