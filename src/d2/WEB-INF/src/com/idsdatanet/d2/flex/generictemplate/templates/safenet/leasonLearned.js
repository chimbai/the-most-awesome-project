(function(window){
	
	D3.inherits(LeasonLearnedSendProfileButton,D3.AbstractFieldComponent);
	function LeasonLearnedSendProfileButton() {
		D3.AbstractFieldComponent.call(this);
	}
	LeasonLearnedSendProfileButton.prototype.getFieldRenderer = function() {
		var label = D3.LanguageManager.getLabel("SendProfile");
		if (!this._fieldRenderer) {
			this._fieldRenderer = document.createElement("button");
			this._fieldRenderer.setAttribute("type", "button");
			this._fieldRenderer.setAttribute("title", label);
			this._fieldRenderer.className = "DefaultButton";
			this._fieldRenderer.appendChild(document.createTextNode(label));
			this._fieldRenderer.onclick = this.sendProfile.bindAsEventListener(this);
		}
		
		return this._fieldRenderer;
	}
	LeasonLearnedSendProfileButton.prototype.sendProfile = function(event){
		this._node.commandBeanProxy.submitForServerSideProcess(this._node, "sendProfile");
	}
	LeasonLearnedSendProfileButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(LeasonLearnedNodeListener,D3.EmptyNodeListener);
	function LeasonLearnedNodeListener()
	{

	}
	LeasonLearnedNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "lessonLearnedSendProfile"){
			return new LeasonLearnedSendProfileButton();
		}else{
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = LeasonLearnedNodeListener;
})(window);