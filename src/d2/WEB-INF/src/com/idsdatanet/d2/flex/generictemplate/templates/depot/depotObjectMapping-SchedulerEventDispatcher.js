(function(window){
	
	// DepotSchedulerEvent
	D3.inherits(DepotSchedulerEvent, D3.Event);
	function DepotSchedulerEvent(name, nodeUid) {
		this.name = name;
		this.nodeUid = nodeUid;
	}
	
	DepotSchedulerEvent.getIdsTableChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getIdsFieldChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_IDS_FIELD_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.getDepotValueChangedEvent = function(nodeUid) {
		return new DepotSchedulerEvent(DepotSchedulerEvent.DEPOT_DEPOT_VALUE_CHANGED, nodeUid);
	}
	
	DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED = "depotIdsTableChanged";
	DepotSchedulerEvent.DEPOT_IDS_FIELD_CHANGED = "depotIdsFieldChanged";
	DepotSchedulerEvent.DEPOT_DEPOT_VALUE_CHANGED = "depotDepotValueChanged";

	window.D3.DepotSchedulerEvent = DepotSchedulerEvent;
	

	// SchedulerEventDispatcher	
	D3.inherits(SchedulerEventDispatcher, D3.Events);
	function SchedulerEventDispatcher() {
		this._commandBean = null;
	}
	
	SchedulerEventDispatcher.prototype.fireIdsTableChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getIdsTableChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireIdsFieldChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getIdsFieldChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireDepotValueChangedEvent = function(nodeUid) {
		this.dispatchEvent(D3.DepotSchedulerEvent.getDepotValueChangedEvent(nodeUid));
	}
	
	SchedulerEventDispatcher.prototype.fireLoadStartedEvent = function() {
		this._commandBean.dispatchEvent(new D3.CommandBeanProxyEvent(D3.CommandBeanProxyEvent.LOAD_STARTED, null));
	}
	
	SchedulerEventDispatcher.prototype.fireLoadCompletedEvent = function() {
		var event = new D3.CommandBeanProxyEvent(D3.CommandBeanProxyEvent.LOAD_COMPLETED, null);
		event.operationType = D3.CommandBeanProxyEvent.OPERATION_TYPE_LOAD;
		this._commandBean.dispatchEvent(event);
	}
	
	window.D3.SchedulerEventDispatcher = SchedulerEventDispatcher;
	
})(window);
