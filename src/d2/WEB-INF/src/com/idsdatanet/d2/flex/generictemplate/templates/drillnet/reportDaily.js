(function(window){
	D3.inherits(LockTourOrDayField,D3.HyperLinkField);
	
	function LockTourOrDayField(){
		D3.HyperLinkField.call(this);
	}
	
	LockTourOrDayField.prototype.data = function(data){
		LockTourOrDayField.uber.data.call(this,data);
		this._lockStatusField = this._config.statusField;
		this._userUidField = this._config.userUidField;
	
	}
	LockTourOrDayField.prototype.getFieldRenderer = function(){
		if (!this._renderer && this._node.getFieldValue(this._userUidField)){
			this._renderer = LockTourOrDayField.uber.getFieldRenderer.call(this);
			this._renderer.removeAttribute("target");
			this._renderer.removeAttribute("href");
			this._renderer.onclick = this.onclick.bindAsEventListener(this);
		}
		
					
		return this._renderer;
	}
	LockTourOrDayField.prototype.refreshLockStatus = function()
	{
		if (this._lockStatusField  && this._renderer)
		{
			var value = this._node.getFieldValue(this._lockStatusField);
			var label= null;
			if (value=="1")
			{
				label = D3.LanguageManager.getLabel("label.unsign");
			}else{
				label = D3.LanguageManager.getLabel("label.sign");
			}
			
			$(this._fieldRenderer).text(label);
		}
	}
	LockTourOrDayField.prototype.onclick = function(){
		if (!this._pnlPassword){
			var divConfig = "<div class='pnlPassword'><table>" +
					"<tr>" +
					"<td>"+D3.LanguageManager.getLabel("label.password")+"</td>" +
					"<td><input type='password' class='fieldEditor textInput'/></td>" +
					"<td><button class='DefaultButton buttonOK'>"+D3.LanguageManager.getLabel("button.ok")+"</td>" +
					"</tr></table></div>";
			this._pnlPassword = $(divConfig);
			this._txtPassword = this._pnlPassword.find(".textInput").get(0);
			this._btnOK = this._pnlPassword.find(".buttonOK").get(0);
			this._btnOK.onclick = this.btnOK_onclick.bindAsEventListener(this);
		}else{
			$(this._txtPassword).val("");
		}
		
		this._pnlPassword.dialog({width:250,height:80,modal:true,resizable:false,title:D3.LanguageManager.getLabel("label.pnlPassword.title")})
	}
	LockTourOrDayField.prototype.btnOK_onclick = function(){
		var password = $(this._txtPassword).val();
		$(this._pnlPassword).dialog("close");
		var params = {};
		params.userUid = this._node.getFieldValue(this._userUidField);
		params.password = password;
		params._invokeCustomFilter = "validatePassword";
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	LockTourOrDayField.prototype.loaded = function(response){
		var status = $(response).find("status").text();
		if (status == '1'){
			this.setFlag();
		}else{
			alert(D3.LanguageManager.getLabel("warning.invalidPassword"));
		}
	}
	
	LockTourOrDayField.prototype.setFlag = function()
	{
		this._node.commandBeanProxy.submitForServerSideProcess(this._node,this._lockStatusField);
	}
	LockTourOrDayField.prototype.refreshFieldRenderer = function(){
		this.refreshLockStatus();
	}
	
	D3.inherits(ReportDailyReculculateDaySinceButton,D3.AbstractFieldComponent);
	
	function ReportDailyReculculateDaySinceButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReportDailyReculculateDaySinceButton.prototype.getFieldEditor = function(){
		
		if (!this._fieldEditor){
			this._fieldEditor = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel("label.reCalculateDaysSinceLastIncident"),
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("tooltip.reCalculateDaysSinceLastIncident")
				},
				callbacks:{
					onclick:this.reCalc
				},
				context:this
			}) ;
		}			
		return this._fieldEditor;
	}
	
	ReportDailyReculculateDaySinceButton.prototype.reCalc = function(){
		var refField = this._config.refFields;
		var fieldName = this._config.calculateField;
	
		var currentDate = this._node.getFieldValue("@daydate");
		var lastIncidentDate = this._node.getFieldValue(refField);
		
		
		if ((currentDate) && (lastIncidentDate)) {
			var daysince = 0.0;
			var timelapsed = 0.0;
			
			if ("0" == this._node.getGWP("calculateHseDayLapsedIncludeDayOfIncident")) {
				timelapsed = 0.0;
			}else {
				timelapsed = 86399999;
			}
			daysince = (parseInt(currentDate) + timelapsed - parseInt(lastIncidentDate)) / 86400000;
			daysince = Math.round(daysince*100) / 100;
			
			if (daysince < 0) {
				daysince = 0.0;
			}
			
			this._node.setFieldValue(fieldName, daysince.toString());
		}else {
			this._node.setFieldValue(fieldName, "");
		}
	}
	
	D3.inherits(ReportDailyRigIssueLinkField,D3.HyperLinkField);
	
	function ReportDailyRigIssueLinkField(){
		D3.HyperLinkField.call(this);
	}
	ReportDailyRigIssueLinkField.prototype.refreshFieldRenderer = function(){
	}
	ReportDailyRigIssueLinkField.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		if (!this._fieldRenderer){
			fieldRenderer = ReportDailyRigIssueLinkField.uber.getFieldRenderer.call(this);
			fieldRenderer.appendChild(document.createTextNode(D3.LanguageManager.getLabel("label.rigIssueLink")));
			fieldRenderer.removeAttribute("target");
			fieldRenderer.removeAttribute("href");
			fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}else{
			fieldRenderer = ReportDailyRigIssueLinkField.uber.getFieldRenderer.call(this);
		}			
		return fieldRenderer;
	}
	ReportDailyRigIssueLinkField.prototype.onclick = function(){
		var redirect_url = "rigissue.html?rigInformationUid=" +this._node.getFieldValue("rigInformationUid");
		this._node.commandBeanProxy.gotoUrl(redirect_url,D3.LanguageManager.getLabel("label.redirecting"));
	}
	
	
	
	D3.inherits(StartNewDayCarryForwardCustomField,D3.AbstractFieldComponent);
	
	function StartNewDayCarryForwardCustomField()
	{
		D3.AbstractFieldComponent.call(this);
		this._isConfigured = false;
		this._showCustomLayout = false;
		this._localObjectName = "startnewdaycarryforwardselection";
		this._localObject = null;
		this._copyableOperationList = [];
		this._selectionList = [];
		this._dateSelectionList = [];
		this._dayOperationList = [];
		this._selectedList = [];
		this._moduleSelectionCheckboxes = [];
	}	
	
	StartNewDayCarryForwardCustomField.prototype.updateLocalObject = function(object){
			return window.localStorage.setItem(this._localObjectName,object);
		
	}
	
	StartNewDayCarryForwardCustomField.prototype.getLocalObject = function(firstTime){
		if (firstTime === undefined) firstTime = true;
		
		if (firstTime)
			return {};
		try {
			return window.localStorage.getItem(this._localObjectName);
		} catch (e) {
			if (!firstTime) return {};
		}
		return this.getLocalObject(false);
	}
	StartNewDayCarryForwardCustomField.prototype.getFieldEditor = function() {
		if (!this._editor) {
			this._editor = this.createEditor();
		}
		
		return this._editor;
	}
	
	StartNewDayCarryForwardCustomField.prototype.createEditor = function(){
		var container =  this.createOperationAndDateSelectionContainer();
		
		
		return container;
	}
	
	
	StartNewDayCarryForwardCustomField.prototype.createOperationAndDateSelectionContainer = function(){
		
		var tableConfig = "<table cellspacing='0' cellpadding='0' class='StartNewDayCarryForward-DateOperationContainer HtmlTableGrid'>" +
				"<tr>" +
				"<td>" +D3.LanguageManager.getLabel("label.startNewDayCarryForward")+"</td>" +
				(this._showCustomLayout?
				"<td><select style='width:0px' class='StartNewDayCarryForward-dateRangeList fieldEditor comboBox'/></td>" +
				"<td><select style='width:0px' class='StartNewDayCarryForward-dateOperationList fieldEditor comboBox'/></td>"
				:"")+
				"<td><select style='width:0px' class='StartNewDayCarryForward-copyableOperationList fieldEditor comboBox'/></td>"+
				"<td><button class='DefaultButton Refresh'>"+D3.LanguageManager.getLabel("button.StartNewDayCarryForward.refresh")+"</button></td>" +
				"</tr>" +
				"<tr>" +
				"<td colspan='"+(this._showCustomLayout?5:3)+"'>" +
				"<ul class='StartNewDayCarryForward-ModuleSelectionContainer' style='padding: 0px; margin: 0px; list-style-type: none;'></ul>" +
				"</td>" +
				"</tr>" +
				"</table>";
						;
		
		var table = $(tableConfig);
		this.table = table;
		if (this._showCustomLayout){
			var dateRangeList = table.find(".StartNewDayCarryForward-dateRangeList");
			this._comboDateRangeList = dateRangeList.get(0);
			this._comboDateRangeList.onchange = this.dateRangeList_onChange.bindAsEventListener(this);
			var dateOperationList = table.find(".StartNewDayCarryForward-dateOperationList");
			this._comboDateOperationList = dateOperationList.get(0);
			this._comboDateOperationList.onchange = this.dateOperationList_onChange.bindAsEventListener(this);
			
			
		}
		
		var copyableOperationList = table.find(".StartNewDayCarryForward-copyableOperationList");
		this._comboCopyableOperationList = copyableOperationList.get(0);
		this._comboCopyableOperationList.onchange = this.copyableOperationList_onChange.bindAsEventListener(this);
		
		var btnRefresh = table.find(".Refresh");
		this._btnRefresh = btnRefresh.get(0);
		this._btnRefresh.onclick = this.btnRefresh_onClick.bindAsEventListener(this);
		
		var moduleSelection = table.find(".StartNewDayCarryForward-ModuleSelectionContainer");
		this._moduleSelectionContainer = moduleSelection.get(0);
		D3.UIManager.addCalllater(function(){
			var containerWidth = $(this.fieldContainer.uiElement).width()-130;
			if (this._showCustomLayout){
				containerWidth = containerWidth/3;
				$(this._comboDateRangeList).width(containerWidth);
				$(this._comboDateOperationList).width(containerWidth);
			}
			$(this._comboCopyableOperationList).width(containerWidth);
			
		},this,null,true,100);
		
		
		return table.get(0);
	}
	StartNewDayCarryForwardCustomField.prototype.dateRangeList_onChange = function(){
		this._triggerFrom = "range";
		this.load();
	}
	StartNewDayCarryForwardCustomField.prototype.dateOperationList_onChange = function(){
		this._triggerFrom = "operation";
		this.load();
	}
	StartNewDayCarryForwardCustomField.prototype.copyableOperationList_onChange = function(){
		if (this._showCustomLayout){
			this._triggerFrom = "day";
		}
		this.load();
	}
	StartNewDayCarryForwardCustomField.prototype.btnRefresh_onClick = function(){
		this.load();
	}
	StartNewDayCarryForwardCustomField.prototype.updateSettings = function(){
		var showFilter = this._node.getGWP("copyDataFiltersWithDateRange");
		var excludeCopyDataFilters = this._node.getFieldValue("@excludeCopyDataFilters");
		
		if (showFilter =="1" && excludeCopyDataFilters!="1"){
			this._showCustomLayout = true;
		}else{
			this._showCustomLayout = false;
		}
			
		var rememberLast = this._node.getGWP("rememberLastCarryForwardSelection");
		this._rememberLastSelection = rememberLast=="1";
		
		var defaultSelection = this._node.getGWP("carryForwardDefaultSelection");
		if (defaultSelection){
			this._defaultSelection = defaultSelection.split(";");
		}else{
			this._defaultSelection = [];
		}
		
		this._isConfigured = true;
	}
	
	StartNewDayCarryForwardCustomField.prototype.data = function(data){
		if (this._localObject == null) this._localObject = this.getLocalObject();
		
		StartNewDayCarryForwardCustomField.uber.data.call(this,data);
		data.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE,this.fieldValueChange,this);
		if (!this._isConfigured)
			this.updateSettings();
		
		this._selectionList = [];
		if (this._localObject.selectedList == null || !this._rememberLastSelection){
			if (this._defaultSelection){
				for (var key in this._defaultSelection){
					this._selectedList[key] = this._defaultSelection[key].trim().toLowerCase();
				}
				
			}
			this._localObject.selectedList = this._selectedList;
		}else{
			this._selectedList = this._localObject.selectedList; 
		}
		
	}
	StartNewDayCarryForwardCustomField.prototype.load = function(){
		if(!this._node.getFieldValue("copyFromYesterdayUsed") ==  '1'){
			
			this._node.setAdditionalUserRequest("selected_carry_forward", null);
			$(this._btnRefresh).text(D3.LanguageManager.getLabel("button.refresh.loading"));
			$(this._btnRefresh).attr('disabled','disabled');
			this._selectionList = [];
			
			var urlParams = this.getParams();
			this._node.commandBeanProxy.customInvoke(urlParams,this.loaded,this);
			
			if (this._showCustomLayout){
				this.retrieveCopyableOperationList();
			}else{
				if (this._copyableOperationList.length <= 1) this.retrieveCopyableOperationList();
			}
		}
	}
	
	StartNewDayCarryForwardCustomField.prototype.getItemFromCopyableOperationList = function(dailyUid){
		for (var i=0;i<this._copyableOperationList.length;i++){
			var item = this._copyableOperationList[i];
			if (item.dailyUid === dailyUid)
				return item;
		}
		return null;
	}
	StartNewDayCarryForwardCustomField.prototype.checkModuleSelection_onclick = function(event){
		if (this._localObject == null) this._localObject = this.getLocalObject();
		var selectedItems = {};
		for (var i=0; i<this._moduleSelectionCheckboxes.length;i++){
			var checkbox = this._moduleSelectionCheckboxes[i];
			var value = $(checkbox).val();
			if ($(checkbox).attr("checked"))
				selectedItems[value] = true;
		}
		this._localObject.selectedList = selectedItems;
		this.updateLocalObject(this._localObject);
		this.setSelectedValue();
	}
	StartNewDayCarryForwardCustomField.prototype.updateSelectionList = function(){
		var container = this._moduleSelectionContainer;
		D3.UI.removeAllChildNodes(container);
		this._moduleSelectionCheckboxes = [];
		
		for (var i=0;i<this._selectionList.length;i++){
			var item = this._selectionList[i];
			li = document.createElement("li");
			
			var uid = this._node.uid + "_carryforwardselection_" + item.data;
			var cb = document.createElement("input");
			cb.setAttribute("id", uid);
			cb.setAttribute("type", "checkbox");
			cb.value = item.data;
			cb.onclick = this.checkModuleSelection_onclick.bindAsEventListener(this);
			if(item.selected){
				cb.setAttribute("checked", true);
			}
			li.appendChild(cb);
			this._moduleSelectionCheckboxes.push(cb);
			var label = document.createElement("label");
			label.setAttribute("for", uid);
			label.appendChild(document.createTextNode(item.label));
			li.appendChild(label);
			container.appendChild(li);
		}
		if (this._selectionList.length<5){
			var max = 5-this._selectionList.length;
			for (var j=0;j<max;j++){
				li = document.createElement("li");
				container.appendChild(li);
			}
		}
		$(this._btnRefresh).text(D3.LanguageManager.getLabel("button.refresh"));
		$(this._btnRefresh).removeAttr('disabled');
	}
	
	StartNewDayCarryForwardCustomField.prototype.updateComboCopyableOperationList = function(){
		var combo = this._comboCopyableOperationList;
		D3.UI.removeAllChildNodes(combo);
		for (var i=0;i<this._copyableOperationList.length;i++){
			var item = this._copyableOperationList[i];
			combo.appendChild(this.createOption(item.dailyUid,item.label,item.toolTip));
		}
	}
	
	
	StartNewDayCarryForwardCustomField.prototype.createOption = function(data, label,toolTip) {
		var option = document.createElement("option");
		option.value = data;
		if (toolTip)
			option.setAttribute("title", toolTip);
		option.appendChild(document.createTextNode(label)); // workaround for IE's odd bug
		return option;
	};
	
	StartNewDayCarryForwardCustomField.prototype.retrieveCopyableOperationList = function(){
		var params = {};
		params._invokeCustomFilter = "flexCarryForwardOtherOperationSelection";
		this._node.commandBeanProxy.customInvoke(params,this.operationListLoaded,this);
		
	}
	StartNewDayCarryForwardCustomField.prototype.operationListLoaded = function(response){
		this.populateOperationList(response);
	}
	
	
	StartNewDayCarryForwardCustomField.prototype.updateComboDateRangeList = function(){
		var combo = this._comboDateRangeList;
		D3.UI.removeAllChildNodes(combo);
		for (var i=0;i<this._dateSelectionList.length;i++){
			var item = this._dateSelectionList[i];
			combo.appendChild(this.createOption(item.value,item.label,item.toolTip));
		}
		
		
	}
	
	StartNewDayCarryForwardCustomField.prototype.updateComboDateOperationList = function(){
		var combo = this._comboDateOperationList;
		D3.UI.removeAllChildNodes(combo);
		for (var i=0;i<this._dayOperationList.length;i++){
			var item = this._dayOperationList[i];
			combo.appendChild(this.createOption(item.value,item.label,item.toolTip));
		}
		
		
	}
	
	StartNewDayCarryForwardCustomField.prototype.populateOpList = function(response){
		this._dayOperationList = [];

		var itemOperationInit = {};
		itemOperationInit.value = "__undefined__";
		itemOperationInit.label = D3.LanguageManager.getLabel("label.startNewDayCarryForward.Operation");
		this._dayOperationList.push(itemOperationInit);
		var self = this;
		$(response).find("Operation").each(function(index,item){
			
			var newItem = {};
			newItem.label = $(this).find("operationName").text();
			newItem.value = $(this).find("operationUid").text();
			self._dayOperationList.push(newItem);
		});
		this.updateComboDateOperationList();

	}
	
	StartNewDayCarryForwardCustomField.prototype.populateRangeList = function(){
		var dateRangeText = [0,1,7,30,365,1095,1825];
		this._dateSelectionList = [];
		var tooltip = D3.LanguageManager.getLabel("label.startNewDayCarryForward.Range.toolTip");
		for (var i=0; i<dateRangeText.length;i++){
			var range = dateRangeText[i];
			var newItem = {};
			newItem.value = range;
			newItem.label = D3.LanguageManager.getLabel("label.startNewDayCarryForward.Range."+range);
			if (range>=365)
				newItem.toolTip = tooltip;
			this._dateSelectionList.push(newItem);
		}
		this.updateComboDateRangeList();
	}
	
	StartNewDayCarryForwardCustomField.prototype.populateOperationList = function(response){
		if (!this._showCustomLayout){
			this._copyableOperationList = [];
			
			var itemInit = {};
			itemInit.dailyUid = "__undefined__";
			itemInit.label = D3.LanguageManager.getLabel("label.startNewDayCarryForward.PreviousDay");
			this._copyableOperationList.push(itemInit);
			var self = this;
			$(response).find('Daily').each(function(index,item){
				var newItem = {};
				newItem.dailyUid = $(this).find("dailyUid").text();
				newItem.dayDate = $(this).find("dayDate").text();
				newItem.rigInformationUid = $(this).find("rigInformationUid").text();
				newItem.operationUid = $(this).find("operationUid").text();
				newItem.label = $(this).find("dayNumber").text() + " (" + $(this).find("dayDate").text() + ") " + $(this).find("operationName").text();
				self._copyableOperationList.push(newItem);
			});
			this.updateComboCopyableOperationList();
		}else{
			if (this._dateSelectionList.length <= 1) this.populateRangeList();

			if (this._triggerFrom == "range") {
				this.populateOpList(response);
				this.populateDayList(response);
			}

			if (this._triggerFrom == "operation") {
				this.populateDayList(response);
			}
		}
	}
	
	StartNewDayCarryForwardCustomField.prototype.populateDayList = function(response){
		this._copyableOperationList = [];

		var itemInit = {};
		itemInit.label = D3.LanguageManager.getLabel("label.startNewDayCarryForward.Day");
		itemInit.dailyUid = "__undefined__";
		
		this._copyableOperationList.push(itemInit);

		var self = this;
		$(response).find('Daily').each(function(index,item){
			var newItem = {};
			newItem.dailyUid = $(this).find("dailyUid").text();
			newItem.dayDate = $(this).find("dayDate").text();
			newItem.rigInformationUid = $(this).find("rigInformationUid").text();
			newItem.operationUid = $(this).find("operationUid").text();
			newItem.label = $(this).find("dayNumber").text() + " (" + $(this).find("dayDate").text() + ")";
			self._copyableOperationList.push(newItem);
		});
		this.updateComboCopyableOperationList();
	}
	
	StartNewDayCarryForwardCustomField.prototype.loaded = function(response){
		this.populateSelectionList(response);
	}
	
	StartNewDayCarryForwardCustomField.prototype.setSelectedValue = function(){
		var  result = [];
		for (var i=0; i<this._moduleSelectionCheckboxes.length;i++){
			var item = this._moduleSelectionCheckboxes[i];
			if (item.checked)result.push(item.value);
		}
		this._node.setAdditionalUserRequest("selected_carry_forward", result);
		
		
		var dailyUid = $(this._comboCopyableOperationList).val();
		if (!dailyUid) dailyUid = "__undefined__";
		if (dailyUid && dailyUid !=="__undefined__") {
			var selected_item = this.getItemFromCopyableOperationList(dailyUid);
			this._node.setAdditionalUserRequest("selected_carry_from_dailyUid", selected_item.dailyUid);
			this._node.setAdditionalUserRequest("selected_carry_from_rigInformationUid", selected_item.rigInformationUid);
		}else{
			this._node.setAdditionalUserRequest("selected_carry_from_dailyUid", null);
			this._node.setAdditionalUserRequest("selected_carry_from_rigInformationUid", null);
		}
	}
	
	StartNewDayCarryForwardCustomField.prototype.populateSelectionList = function(response){
		if (this._localObject == null) this._localObject = this.getLocalObject();
		var self = this;
		$(response).find('item').each(function(index,item){
			var newItem = {};
			newItem.data = $(this).attr("value");
			newItem.label = $(this).attr("key");
			if (self.isDefaultSelectionAll() && (self._localObject.selectedList == null || self._rememberLastSelection)) {
				newItem.selected = true;
			} else {
				newItem.selected = self._selectedList.indexOf(newItem.data.trim().toLowerCase()) != -1;
			}
			self._selectionList.push(newItem);
		});
		this.updateSelectionList();
		this.setSelectedValue();
	}
	StartNewDayCarryForwardCustomField.prototype.isDefaultSelectionAll = function(){
		if (this._defaultSelection && this._defaultSelection.length == 1){
			var item = this._defaultSelection[0];
			if (item === "ALL")
				return true;
		}
		return false;
	}
	
	
	
	StartNewDayCarryForwardCustomField.prototype.getParams = function(){
		var params = {};
		params._invokeCustomFilter = "flexCarryForwardSelection";
		
		if (this._showCustomLayout) {
			var rangeType = $(this._comboDateRangeList).val();
			if (rangeType && rangeType !=="__undefined__") {
				params.rangeType = rangeType;
			}
			
			var selectedOperation = $(this._comboDateOperationList).val();
			if (selectedOperation && selectedOperation !=="__undefined__") {
				params.selectedOperation = selectedOperation;
			}
			
			var dailyUid = $(this._comboCopyableOperationList).val();
			if (dailyUid && dailyUid !=="__undefined__") {
				params.selectedDay = dailyUid;
			}
		}
		
		var dailyUid = $(this._comboCopyableOperationList).val();
		if (!dailyUid) dailyUid = "__undefined__";
		if (dailyUid) {
			if (dailyUid==="__undefined__"){
				var dayDate =  this._node.getFieldValue("@daydate");
				var date = new Date(parseInt(dayDate));
				
				if (!isNaN(date.getTime())) {
					var localDate = this.shiftTimeIntoLocalTimezone(date);
					params.userDate = $.datepick.formatDate("d M yyyy", localDate);
				}
				params.rigInformationUid = this._node.getFieldValue("rigInformationUid");
			}else{
				var item = this.getItemFromCopyableOperationList(dailyUid);
				params.userDate = item.dayDate;
				params.rigInformationUid = item.rigInformationUid;
				params.copyFromOperationUid = item.operationUid;
			}
		}
		
		
		return params;
	}
	
	StartNewDayCarryForwardCustomField.prototype.fieldValueChange = function(event){
		if (event.fieldName == "@daydate" || event.fieldName == "rigInformationUid") {
			this.load();
		}
	}
	
	StartNewDayCarryForwardCustomField.prototype.refreshFieldEditor = function(){
		this.load();
	}
	
	StartNewDayCarryForwardCustomField.prototype.showField = function(show) {
		if (!show) {
			this._editor.style.display = "none";
		} else {
			this._editor.style.display = "";
		}
	}
	
	D3.inherits(DwomMsgField,D3.TextInputField);
	
	function DwomMsgField(){
		D3.TextInputField.call(this);
	}
	
	DwomMsgField.prototype.getFieldRenderer = function(){
		
	}
	
	DwomMsgField.prototype.getFieldEditor = function(){
		if (!this._fieldEditor) {
			this._fieldEditor = this.buildtext();
			this._fieldEditor.setAttribute('style', 'color:gray');
		}
		return this._fieldEditor;
	}
	
	DwomMsgField.prototype.buildtext = function() {
		var btnExecuteConfig = {
				tag:"text",
				text:"MD is read-only, as this is a LAR-driven operation.",
				context:this
		};
		return D3.UI.createElement(btnExecuteConfig);
	}
	
	DwomMsgField.prototype.refreshFieldEditor = function() {
		
		
	} 
	
	D3.inherits(ReportDailyNodeListener,D3.EmptyNodeListener);
	
	function ReportDailyNodeListener()
	{
		isNewRecord = false;
	}
	
	ReportDailyNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "StartNewDayCarryForward"){
			this._startNewDayCarryForwardCustomField = new StartNewDayCarryForwardCustomField();
			return this._startNewDayCarryForwardCustomField;
		}else if(id == "ReportDailyRigIssueLink"){
			return new ReportDailyRigIssueLinkField();
		}else if (id == "recalculateDaysSince") {
			return new ReportDailyReculculateDaySinceButton();
		}else if (id == "lockTourOrDay"){
			return new LockTourOrDayField();
		}else if (id == "dwomMsg"){
			return new DwomMsgField();
		}
	}
	
	ReportDailyNodeListener.prototype.getConfirmationMessage = function(commandBeanProxy) {
		var message = null;
		var currentNodes = commandBeanProxy.rootNode.children["ReportDaily"].getItems();
		currentNodes.forEach(function(item, index){
			if(item.atts.editMode && !item.atts.isNewRecord && item._additionalUserRequestParams && item._additionalUserRequestParams.selected_carry_forward.length > 0) {
				message = "Copying data from previous days may result in replicating data that have already been manually created on the specified screen(s). Are you sure you want to continue?";
			}
		});
		return message;
	}
	
	ReportDailyNodeListener.prototype.allowFieldShowEdit = function(node, fieldXml){
		if(fieldXml.customField == "StartNewDayCarryForward"){
			var objects = $(node.commandBeanProxy.nodeListener._startNewDayCarryForwardCustomField.fieldContainer);
			if((!node.atts.editMode || node.getFieldValue("copyFromYesterdayUsed") == "1")) {
				objects[0]._parent.parentNode.style.display = "none";
				return false;
			} else {
				objects[0]._parent.parentNode.style.display = "";
			}
			if(node.atts.isNewRecord){
				this.isNewRecord = true;
			}
		}
		
		return true;
	}
	
	ReportDailyNodeListener.prototype.loadCompleted = function(commandbeanProxy) {
		if(commandbeanProxy.nodeListener._startNewDayCarryForwardCustomField === undefined) {

		}else {
			var objects = $(commandbeanProxy.nodeListener._startNewDayCarryForwardCustomField.fieldContainer);
			if(this.isNewRecord == undefined || !this.isNewRecord) {
				//$(".StartNewDayCarryForward-DateOperationContainer").closest(".HtmlTableRow").hide();
				objects[0]._parent.parentNode.style.display = "none";
			}
			commandbeanProxy._reloadHtmlPageAfterPageCancel = true;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ReportDailyNodeListener;
	
})(window);