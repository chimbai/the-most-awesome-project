(function(window){
	D3.inherits(ExportListener,D3.EmptyNodeListener);
	function ExportListener()
	{
	}
	ExportListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (!this._commandBeanProxy){
			this._commandBeanProxy = commandBeanProxy;
		}
		if (id == "generateReport") {
			this._btnGenerateReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:"Export",
				callbacks:{
					onclick:this.generateReport
				},
				context:this
			});
			return this._btnGenerateReport;
		}
	}
	ExportListener.prototype.getSelectedWells = function(){
		var list = this._commandBeanProxy.rootNode.children["DrillPlanExport"].getItems();
		var node = list[0];
		return node.getMultiSelectFieldValue("wells");
	}
	ExportListener.prototype.generateReport = function(){
		if(this._btnGenerateReport.disabled) return;
		var selectedWells = this.getSelectedWells();
		if(selectedWells.length == 0){
			alert("Please select one or more wells");
			return;
		}
		var params = {};
		params._invokeCustomFilter = "generate_report";
		params.selectedWells = JSON.stringify(selectedWells);
		this._commandBeanProxy.customInvoke(params,this.generateReportResponse,this,"json");
		this._btnGenerateReport.disabled = true;
		this.updateLabel(this._btnGenerateReport, "Please wait...");
	}
	ExportListener.prototype.generateReportResponse = function(response){
		this._btnGenerateReport.disabled = false;
		this.updateLabel(this._btnGenerateReport, "Export");
		if(response.error){
			alert(response.error);
			return;
		}
		if(response.status == "ok"){
			var url = "abaccess/drillplanexport/" + encodeURIComponent("IDS_SLB_-_DrillPlanExport.zip") + "?_invokeCustomFilter=downloadReport";
			this._commandBeanProxy.openDownloadWindow(url);
		}
	}
	ExportListener.prototype.updateLabel = function(btn, label){
		while(btn.firstChild) btn.removeChild(btn.firstChild);
		btn.appendChild(document.createTextNode(label));
	}
	D3.CommandBeanProxy.nodeListener = ExportListener;
})(window);