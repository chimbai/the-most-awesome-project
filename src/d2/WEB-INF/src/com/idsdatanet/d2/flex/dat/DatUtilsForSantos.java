package com.idsdatanet.d2.flex.dat;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DatUtilsForSantos extends BlazeRemoteClassSupport{

	private Locale getLocale(HttpServletRequest request) throws Exception {
		return UserSession.getInstance(request).getUserLocale();
	}
	
	public String queryActualPhasesSummary_forSantos(String[] operationUids) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			 
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			for (String operationUid : operationUids) {
				writer.startElement("datarow");
				
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
				writer.addElement("operationName", operationName);

				Map<String, Map> phases = new LinkedHashMap<String, Map>();
				operationUomContext.setOperationUid(operationUid);
				CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "daysSpentPriorToSpud", operationUomContext);
				writer.addElement("duration_uomSymbol", durationConverter.getUomSymbol());
				CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), OperationPlanPhase.class, "depthMdMsl", operationUomContext);
				writer.addElement("depth_uomSymbol", depthConverter.getUomSymbol());
				CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
				writer.addElement("cost_uomSymbol", costConverter.getUomSymbol());
				
				Double totalPlannedDuration = 0.0;
				Double totalActualDuration = 0.0;
				Double totalPlannedCost = 0.0;
				Double totalActualCost = 0.0;
				String costType = null; 
				String gwpvalue = null;
				Map<String, String> excludePhases = new HashMap<String, String>();
				
				String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
				String queryString = "select m.operationUid, m, p FROM OperationPlanMaster m, OperationPlanPhase p " +
						"WHERE (m.isDeleted=false or m.isDeleted is null) " +
						"AND (p.isDeleted=false or p.isDeleted is null) " +
						"AND m.operationPlanMasterUid=p.operationPlanMasterUid " + 
						"AND m.operationPlanMasterUid=:dvdPlanUid " +
						"ORDER BY sequence";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dvdPlanUid", dvdPlanUid, qp);
				for (Object[] rec : list) {
					OperationPlanMaster dvd = (OperationPlanMaster) rec[1];
					OperationPlanPhase phase = (OperationPlanPhase) rec[2];
					String phaseCode = StringUtils.isBlank(phase.getPhaseCode())?"-":phase.getPhaseCode();
					if (costType==null) {
						costType = dvd.getCostPhaseType();
						if (StringUtils.isNotBlank(costType)) {
							if ("CS".equalsIgnoreCase(costType)) {
								gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
							}else if ("PA".equalsIgnoreCase(costType)) {
								gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
							}
							if (StringUtils.isNotBlank(gwpvalue)) {
								for (String item:gwpvalue.toString().split(";")) {
									String thisCode = item.toString();
									excludePhases.put(thisCode, thisCode);
								}
							}
						}
					}
					Map<String, String> phaseItem = new HashMap<String, String>();
					
					phaseItem.put("phaseCode", phaseCode);
					phaseItem.put("phaseName", this.getPhaseName(phaseCode, true));
					phaseItem.put("comment", phase.getPhaseSummary());
					
					Double phaseDuration = phase.getP50Duration()==null?0.0:phase.getP50Duration();
					durationConverter.setBaseValue(phaseDuration);
					phaseItem.put("p50Duration", durationConverter.formatOutputPrecision());
					depthConverter.setBaseValue(phase.getDepthMdMsl()==null?0.0:phase.getDepthMdMsl());
					phaseItem.put("plannedDepth", depthConverter.formatOutputPrecision());
					phases.put(phaseCode, phaseItem);
					
					if (!excludePhases.containsKey(phaseCode)) {
						totalPlannedDuration += phaseDuration;
					}
				}
				
				queryString = "SELECT d.operationUid, coalesce(c.phaseCode,''), sum(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM CostDailysheet c, Daily d " +
						"WHERE (d.isDeleted=false or d.isDeleted is null) " +
						"AND (c.isDeleted=false or c.isDeleted is null) " +
						"AND c.dailyUid=d.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"GROUP BY d.operationUid, coalesce(c.phaseCode,'') " +
						"ORDER BY d.dayDate ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					String phaseCode = (String) rec[1];
					phaseCode = StringUtils.isBlank(phaseCode)?"-":phaseCode;
					
					Map<String, String> phaseItem = new HashMap<String, String>();
					if (phases.containsKey(phaseCode)) {
						phaseItem = phases.get(phaseCode);
					} else {
						phaseItem.put("phaseCode", phaseCode);
						phaseItem.put("phaseName", this.getPhaseName(phaseCode, true));
					}
					
					Double phaseTotal = (Double) rec[2];
					if (phaseTotal==null) phaseTotal = 0.0;
					costConverter.setBaseValue(phaseTotal);
					phaseItem.put("actualCost", costConverter.formatOutputPrecision());
					phases.put(phaseCode, phaseItem);
					
					totalActualCost += phaseTotal;
				}
				
				queryString = "SELECT m.operationUid, coalesce(c.phaseCode,''), sum(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0) * coalesce(c.estimatedDays,0.0)) FROM CostAfeMaster m, CostAfeDetail c " +
						"WHERE (m.isDeleted=false or m.isDeleted is null) " +
						"AND (c.isDeleted=false or c.isDeleted is null) " +
						"AND c.costAfeMasterUid=m.costAfeMasterUid " +
						"AND m.operationUid=:operationUid " +
						"GROUP BY m.operationUid, coalesce(c.phaseCode,'') ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					String phaseCode = (String) rec[1];
					phaseCode = StringUtils.isBlank(phaseCode)?"-":phaseCode;
					
					Map<String, String> phaseItem = new HashMap<String, String>();
					if (phases.containsKey(phaseCode)) {
						phaseItem = phases.get(phaseCode);
					} else {
						phaseItem.put("phaseCode", phaseCode);
						phaseItem.put("phaseName", this.getPhaseName(phaseCode, true));
					}
					
					Double phaseTotal = (Double) rec[2];
					if (phaseTotal==null) phaseTotal = 0.0;
					costConverter.setBaseValue(phaseTotal);
					phaseItem.put("plannedCost", costConverter.formatOutputPrecision());
					phases.put(phaseCode, phaseItem);
					
					if (!excludePhases.containsKey(phaseCode)) {
						totalPlannedCost += phaseTotal;
					}
				}
				queryString = "SELECT d.operationUid, coalesce(a.phaseCode,''), sum(coalesce(a.activityDuration,0.0)), max(coalesce(a.depthMdMsl,0.0)) FROM Activity a, Daily d " +
						"WHERE (d.isDeleted=false or d.isDeleted is null) " +
						"AND (a.isDeleted=false or a.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND a.activityDuration>0.0" +
						"GROUP BY d.operationUid, coalesce(a.phaseCode,'') " +
						"ORDER BY d.dayDate ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					String phaseCode = (String) rec[1];
					phaseCode = StringUtils.isBlank(phaseCode)?"-":phaseCode;
					
					Map<String, String> phaseItem = new HashMap<String, String>();
					if (phases.containsKey(phaseCode)) { 
						phaseItem = phases.get(phaseCode);
					} else {
						phaseItem.put("phaseCode", phaseCode);
						phaseItem.put("phaseName", this.getPhaseName(phaseCode, true));
					}
					
					Double actualDuration = (Double) rec[2];
					if (actualDuration==null) actualDuration = 0.0;
					durationConverter.setBaseValue(actualDuration);
					phaseItem.put("actualDuration", durationConverter.formatOutputPrecision());
					totalActualDuration += actualDuration; 
					
					Double depthMdMsl = (Double) rec[3];
					if (depthMdMsl==null) depthMdMsl = 0.0;
					depthConverter.setBaseValue(depthMdMsl);
					phaseItem.put("actualDepth", depthConverter.formatOutputPrecision());
					
					
					phases.put(phaseCode, phaseItem);
				}
				for(Map.Entry<String, Map> entry : phases.entrySet()) {
					SimpleAttributes attr = new SimpleAttributes();
					Map<String, String> fieldsMap = (Map<String, String>) entry.getValue();
					for (Map.Entry<String, String> field : fieldsMap.entrySet()) {
						attr.addAttribute(field.getKey(), field.getValue());
					}
					writer.addElement("phase", null, attr);
				}
				
				durationConverter.setBaseValue(totalPlannedDuration);
				writer.addElement("totalPlannedDuration", durationConverter.formatOutputPrecision());
				durationConverter.setBaseValue(totalActualDuration);
				writer.addElement("totalActualDuration", durationConverter.formatOutputPrecision());
				costConverter.setBaseValue(totalPlannedCost);
				writer.addElement("totalPlannedCost", costConverter.formatOutputPrecision());
				costConverter.setBaseValue(totalActualCost);
				writer.addElement("totalActualCost", costConverter.formatOutputPrecision());
				
				
				writer.endElement();
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	public String queryDailyPhasesTotal_forSantos(String[] operationUids) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			for (String operationUid : operationUids) {
				writer.startElement("datarow");
				
				Double totalWellCost = 0.0;
				Double totalWellDuration = 0.0; 
				
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
				writer.addElement("operationName", operationName);
				operationUomContext.setOperationUid(operationUid);
				CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration", operationUomContext);
				writer.addElement("duration_uomSymbol", durationConverter.getUomSymbol());
				CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
				writer.addElement("cost_uomSymbol", costConverter.getUomSymbol());
				
				Map<String, Map> dailyMap = new LinkedHashMap<String, Map>();
				
				String queryString = "SELECT d.dailyUid, d.dayDate, coalesce(a.phaseCode,''), coalesce(a.classCode,''), SUM(coalesce(a.activityDuration,0.0)) FROM Daily d, Activity a " +
						"WHERE (d.isDeleted=false or d.isDeleted is null) " +
						"AND (a.isDeleted=false or a.isDeleted is null) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"AND a.activityDuration>0.0 " +
						"GROUP BY d.dailyUid, d.dayDate, coalesce(a.phaseCode,''), coalesce(a.classCode,'') " +
						"ORDER BY d.dayDate";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					String dailyUid = (String) rec[0];
					String phaseCode = (String) rec[2];
					String classCode = (String) rec[3];
					Double duration = (Double) rec[4];
					
					Map<String, Object> dailyData = null;
					Map<String, Map> activityCodes = null;
					if (dailyMap.containsKey(dailyUid)) {
						dailyData = dailyMap.get(dailyUid);
						activityCodes = (Map<String, Map>) dailyData.get("ActivityCodes");
					} else {
						dailyData = new HashMap<String, Object>();
						dailyData.put("dailyUid", dailyUid);
						dailyData.put("dayDate", df.format((Date) rec[1]));
						ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
						if (reportDaily!=null) {
							//dailyData.put("dailyEndSummary", reportDaily.getDailyEndSummary()); //Status @ Midnight
							//dailyData.put("reportCurrentStatus", reportDaily.getReportCurrentStatus()); // Status @ 0600hrs
							dailyData.put("reportPeriodSummary", reportDaily.getReportPeriodSummary()); //24hr. Summary
						}
						activityCodes = new HashMap<String, Map>();
						dailyData.put("ActivityCodes", activityCodes);
						dailyMap.put(dailyUid, dailyData);
					}
					
					String key = "phase_" + phaseCode + "_" + classCode;
					String phaseName = this.getPhaseName(phaseCode, true);
					
					Map<String, Object> breakdownMap = new HashMap<String, Object>();
					durationConverter.setBaseValue(duration);
					breakdownMap.put("duration", durationConverter.getConvertedValue());
					breakdownMap.put("duration_text", durationConverter.formatOutputPrecision());
					breakdownMap.put("classCode", classCode);
					breakdownMap.put("phaseCode", phaseCode);
					breakdownMap.put("phaseName", phaseName);
					breakdownMap.put("cost_text", costConverter.formatOutputPrecision(0.0));
					breakdownMap.put("cost", 0.0);
					activityCodes.put(key, breakdownMap);
				}
				
				queryString = "SELECT d.dailyUid, coalesce(c.phaseCode,''), coalesce(c.classCode,''), SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM Daily d, CostDailysheet c " +
					"WHERE (d.isDeleted=false or d.isDeleted is null) " +
					"AND (c.isDeleted=false or c.isDeleted is null) " +
					"AND c.dailyUid=d.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.dailyUid, coalesce(c.phaseCode,''), coalesce(c.classCode,'') ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					String dailyUid = (String) rec[0];
					String phaseCode = (String) rec[1];
					String classCode = (String) rec[2];
					Double cost = (Double) rec[3];
					
					Map<String, Object> dailyData = null;
					Map<String, Map> activityCodes = null;
					if (dailyMap.containsKey(dailyUid)) {
						dailyData = dailyMap.get(dailyUid);
						activityCodes = (Map<String, Map>) dailyData.get("ActivityCodes");
					}		
					
					String key = "phase_" + phaseCode + "_" + classCode;
					Map<String, Object> breakdownMap = null;
					if (activityCodes.containsKey(key)) {
						breakdownMap = activityCodes.get(key);
					} else {
						breakdownMap = new HashMap<String, Object>();
						String phaseName = this.getPhaseName(phaseCode, true);
						durationConverter.setBaseValue(0.0);
						breakdownMap.put("duration", durationConverter.getConvertedValue());
						breakdownMap.put("duration_text", durationConverter.formatOutputPrecision());
						breakdownMap.put("classCode", classCode);
						breakdownMap.put("phaseCode", phaseCode);
						breakdownMap.put("phaseName", phaseName);
						activityCodes.put(key, breakdownMap);
					}
					costConverter.setBaseValue(cost);
					breakdownMap.put("cost", costConverter.getConvertedValue());
					breakdownMap.put("cost_text", costConverter.formatOutputPrecision());
				}
				
				
				for (Map.Entry<String, Map> entry : dailyMap.entrySet()) {
					Map<String, Object> dailyData = entry.getValue();
					SimpleAttributes attributes = new SimpleAttributes();
					for (Map.Entry<String, Object> entry2 : dailyData.entrySet())  {
						if (entry2.getValue() instanceof String) {
							attributes.addAttribute(entry2.getKey(), nullToEmptyString(entry2.getValue()));
						}
					}
					writer.startElement("Daily", attributes);
					Map<String, Map> activityCodes = (Map<String, Map>) dailyData.get("ActivityCodes");
					Double totalDailyCost = 0.0;
					Double totalDailyDuration = 0.0;
					if (activityCodes!=null) {
						for (Map.Entry<String, Map> entry2 : activityCodes.entrySet())  {
							Map<String, Object> breakdownMap = entry2.getValue();
							attributes = new SimpleAttributes();
							for (Map.Entry<String, Object> entry3 : breakdownMap.entrySet()) {
								attributes.addAttribute(entry3.getKey(), nullToEmptyString(entry3.getValue()));
								if ("cost".equals(entry3.getKey())) {
									totalDailyCost += (Double) entry3.getValue();
								} else if ("duration".equals(entry3.getKey())) {
									totalDailyDuration += (Double) entry3.getValue();
								}
							}
							
							writer.addElement("ActivityCodesBreakdown", null, attributes);
						}
					}
					writer.addElement("TotalDuration", durationConverter.formatOutputPrecision(totalDailyDuration));
					writer.addElement("TotalCost", costConverter.formatOutputPrecision(totalDailyCost));
					totalWellCost += totalDailyCost;
					totalWellDuration += totalDailyDuration; 
					writer.endElement();
				}

				writer.addElement("TotalDuration", durationConverter.formatOutputPrecision(totalWellDuration));
				writer.addElement("TotalCost", costConverter.formatOutputPrecision(totalWellCost));
				writer.endElement();
			}
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	
	public String queryAfeSummaryForVault_forSantos(String[] operationUids) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			for (String operationUid : operationUids) {
				writer.startElement("datarow");
				
				Double afeCost = 0.0;
				Double afeAdjustedCost = 0.0;
				Double actualCost = 0.0;
				
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
				writer.addElement("operationName", operationName);
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				if (operation!=null) {
					String rigInformationUid = operation.getRigInformationUid();
					if (StringUtils.isNotBlank(rigInformationUid)) {
						RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
						if (rig!=null) {
							writer.addElement("rigName", nullToEmptyString(rig.getRigName()));
						}
					}
				}
				
				
				operationUomContext.setOperationUid(operationUid);
				CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
				writer.addElement("cost_uomSymbol", costConverter.getUomSymbol());
				
				//AFE Original
				Map<String, Map> allItems = new HashMap<String, Map>();
				String queryString = "SELECT coalesce(d.accountCode,''), coalesce(d.shortDescription,''), SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) FROM CostAfeDetail d, CostAfeMaster c " +
						"WHERE (d.isDeleted=false or d.isDeleted is NULL) " +
						"AND (c.isDeleted=false or c.isDeleted is NULL) " +
						"AND d.costAfeMasterUid=c.costAfeMasterUid " +
						"AND c.operationUid=:operationUid " +
						"GROUP BY coalesce(d.accountCode,''), coalesce(d.shortDescription,'')";
				List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : rs) {
					String key = nullToEmptyString(rec[0]).trim() + "|" + nullToEmptyString(rec[1]).trim();
					Double itemTotal = (Double) rec[2];
					if (!allItems.containsKey(key)) {
						Map<String, Object> itemData = new HashMap<String, Object>();
						itemData.put("accountCode", nullToEmptyString(rec[0]));
						itemData.put("shortDescription", nullToEmptyString(rec[1]));
						costConverter.setBaseValue(itemTotal);
						itemData.put("afeItemTotal", costConverter.formatOutputPrecision());
						itemData.put("afeItemTotal_raw", itemTotal);
						itemData.put("afeAdjustedItemTotal_raw", 0.0);
						itemData.put("actualItemTotal_raw", 0.0);
						allItems.put(key, itemData);
						afeCost += itemTotal;
					}
				}
				 
				//AFE Adjusted
				queryString = "SELECT coalesce(d.accountCode,''), coalesce(d.shortDescription,''), SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) FROM CostAfeDetailAdjusted d, CostAfeMasterAdjusted c " +
						"WHERE (d.isDeleted=false or d.isDeleted is NULL) " +
						"AND (c.isDeleted=false or c.isDeleted is NULL) " +
						"AND d.costAfeMasterAdjustedUid=c.costAfeMasterAdjustedUid " +
						"AND c.operationUid=:operationUid " +
						"GROUP BY coalesce(d.accountCode,''), coalesce(d.shortDescription,'')";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : rs) {
					String key = nullToEmptyString(rec[0]).trim() + "|" + nullToEmptyString(rec[1]).trim();
					Double itemTotal = (Double) rec[2];
					
					Map<String, Object> itemData = new HashMap<String, Object>();
					if (allItems.containsKey(key)) {
						itemData = allItems.get(key);
					} else {
						itemData.put("accountCode", nullToEmptyString(rec[0]));
						itemData.put("shortDescription", nullToEmptyString(rec[1]));
						itemData.put("afeItemTotal_raw", 0.0);
						itemData.put("actualItemTotal_raw", 0.0);
					}
					costConverter.setBaseValue(itemTotal);
					itemData.put("afeAdjustedItemTotal", costConverter.formatOutputPrecision());
					itemData.put("afeAdjustedItemTotal_raw", itemTotal);
					allItems.put(key, itemData);
					afeAdjustedCost += itemTotal;
				}
				
				//Actual Cost
				queryString = "SELECT coalesce(c.accountCode,''), coalesce(c.afeShortDescription,''), SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM Daily d, CostDailysheet c " +
						"WHERE (d.isDeleted=false or d.isDeleted is NULL) " +
						"AND (c.isDeleted=false or c.isDeleted is NULL) " +
						"AND d.dailyUid=c.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"GROUP BY coalesce(c.accountCode,''), coalesce(c.afeShortDescription,'')";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : rs) {
					String key = nullToEmptyString(rec[0]).trim() + "|" + nullToEmptyString(rec[1]).trim();
					Double itemTotal = (Double) rec[2];
					
					Map<String, Object> itemData = new HashMap<String, Object>();
					if (allItems.containsKey(key)) {
						itemData = allItems.get(key);
					} else {
						itemData.put("accountCode", nullToEmptyString(rec[0]));
						itemData.put("shortDescription", nullToEmptyString(rec[1]));
						itemData.put("afeItemTotal_raw", itemTotal);
						itemData.put("afeAdjustedItemTotal_raw", 0.0);
					}
					costConverter.setBaseValue(itemTotal);
					itemData.put("actualItemTotal", costConverter.formatOutputPrecision());
					itemData.put("actualItemTotal_raw", itemTotal);
					allItems.put(key, itemData);
					actualCost += itemTotal;
				}
				
				List list = new LinkedList(allItems.entrySet());
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
						String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
						if (object1==null || object1==null || object2==null || object2==null) return 0;
						return (object1).compareTo(object2);
					}
				});
			    for (Iterator it = list.iterator(); it.hasNext();) {
			    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
			    	Map<String, Object> itemData = allItems.get(entry.getKey());
					SimpleAttributes attributes = new SimpleAttributes();
					attributes.addAttribute("accountCode", (String) itemData.get("accountCode"));
					attributes.addAttribute("shortDescription", (String) itemData.get("shortDescription"));
					attributes.addAttribute("afeItemTotal", (String) itemData.get("afeItemTotal"));
					attributes.addAttribute("afeAdjustedItemTotal", (String) itemData.get("afeAdjustedItemTotal"));
					attributes.addAttribute("actualItemTotal", (String) itemData.get("actualItemTotal"));
					
					attributes.addAttribute("afeAdjusted", nullToEmptyString(itemData.get("afeItemTotal")).equals(nullToEmptyString(itemData.get("afeAdjustedItemTotal")))?"false":"true");
					Double afeItemTotal_raw = (Double) itemData.get("afeItemTotal_raw");
					Double actualItemTotal_raw = (Double) itemData.get("actualItemTotal_raw");
					attributes.addAttribute("overSpent", (actualItemTotal_raw - afeItemTotal_raw > 0.0?"true":"false"));
					
					writer.addElement("item", null, attributes);
				}
				
				//total
				costConverter.setBaseValue(afeCost);
				writer.addElement("afeTotal",costConverter.formatOutputPrecision());
				costConverter.setBaseValue(afeAdjustedCost);
				writer.addElement("afeAdjustedTotal",costConverter.formatOutputPrecision());
				costConverter.setBaseValue(actualCost);
				writer.addElement("actualTotal",costConverter.formatOutputPrecision());
				costConverter.setBaseValue(afeCost - afeAdjustedCost);
				writer.addElement("afeDiff",costConverter.formatOutputPrecision());
				costConverter.setBaseValue(afeCost - actualCost);
				writer.addElement("actualDiff",costConverter.formatOutputPrecision());
				
				writer.endElement();
			}			
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
	
	private Double nullToZero(Double value) {
		if (value==null) return 0.0;
		return value;
	}
	
	private String getActivityCodeName(String className, String shortCode, Boolean showShortCode) {
		String codeName = shortCode;
		try {
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT name FROM "+className+" WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", shortCode);
			if (list.size()>0) {
				codeName = nullToEmptyString(list.get(0));
				if (showShortCode) {
					codeName += " (" + shortCode + ")";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return codeName;
	}
	
	private String getTaskName(String taskCode, Boolean showShortCode) {
		return this.getActivityCodeName("LookupTaskCode", taskCode, showShortCode);
	}
	
	private String getPhaseName(String phaseCode, Boolean showShortCode) {
		return this.getActivityCodeName("LookupPhaseCode", phaseCode, showShortCode);
	}
	
	private String getClassName(String classCode, Boolean showShortCode) {
		return this.getActivityCodeName("LookupClassCode", classCode, showShortCode);
	}
	
	public String queryWellVsFfc_forSantos(String[] operationUids) {
		String xml = "<root/>";
		try {
			SimpleDateFormat dtf = new SimpleDateFormat("dd MMM yyyy kk:mm");
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			/*Map<String, LookupItem> lookup = new HashMap<String, LookupItem>();
			try {
				UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
				lookup = LookupManager.getConfiguredInstance().getLookup("xml://casingsection.casing_size?key=code&value=label", userSelection, new LookupCache());
			} catch (Exception e) {
				System.out.println("Lookup not found.");
			}*/
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			Map<String, Map> allCostItems = new HashMap<String, Map>();
			Integer index = 0;
			String queryString = "select o.operationUid, o FROM Operation o, Wellbore wb, Well w " +
					"WHERE (o.isDeleted=false or o.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND o.wellboreUid=wb.wellboreUid " +
					"AND wb.wellUid=w.wellUid " +
					"AND o.operationUid in ('"+StringUtils.join(operationUids,"','")+"') " +
					"ORDER BY w.wellName, wb.wellboreName, o.operationName ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString, qp);
			for (Object[] rec : list) {
				SimpleAttributes attributes = new SimpleAttributes(); 
				String operationUid = (String) rec[0];
				operationUomContext.setOperationUid(operationUid);
				attributes.addAttribute("operationUid", "o_" + index);
				attributes.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
				
				Operation operation = (Operation) rec[1];
				Date spudDate = operation.getSpudDate();
				attributes.addAttribute("spudDate", spudDate==null?"-":dtf.format(spudDate));
				
				Date tdDate = operation.getTdDateTdTime();
				attributes.addAttribute("tdDate", tdDate==null?"-":dtf.format(tdDate));
				
				String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
				String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(operationUid, reportType);
				Double daysOnOperation = CommonUtil.getConfiguredInstance().daysOnOperation(operationUid, dailyUid);
				if (daysOnOperation==null) daysOnOperation = 0.0;
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Operation.class, "daysSpentPriorToSpud", operationUomContext);
				thisConverter.setBaseValue(daysOnOperation);
				attributes.addAttribute("daysOnOperation", thisConverter.getFormattedValue());
				
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
				if (reportDaily!=null) {
					String rigInformationUid = reportDaily.getRigInformationUid();
					if (rigInformationUid!=null) {
						List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM RigInformation WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid", "rigInformationUid", rigInformationUid, qp);
						if (rigList.size()>0) {
							attributes.addAttribute("rigName", nullToEmptyString(rigList.get(0).getRigName()));
						}
					}
				}
				
				queryString = "SELECT d.operationUid, max(rd.depthMdMsl), min(rd.lastCsgsize) FROM ReportDaily rd, Daily d " +
						"WHERE (rd.isDeleted=false or rd.isDeleted is NULL) " +
						"AND (d.isDeleted=false or d.isDeleted is NULL) " +
						"AND d.dailyUid=rd.dailyUid " +
						"AND rd.operationUid=:operationUid " +
						"AND rd.depthMdMsl is not null " +
						"AND rd.reportType=:reportType " +
						"GROUP BY d.operationUid ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid","reportType"}, new Object[]{operationUid, reportType}, qp);
				if (list.size()>0) {
					Double tdDepth = (Double) list.get(0)[1];
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "depthMdMsl", operationUomContext);
					thisConverter.setBaseValue(tdDepth);
					attributes.addAttribute("tdDepth", thisConverter.getFormattedValue());
					
					Double casingSize = (Double) list.get(0)[2];
					if (casingSize!=null) {
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "lastCsgsize", operationUomContext);
						thisConverter.setBaseValue(casingSize);
						attributes.addAttribute("casingSize", thisConverter.getFormattedValue());
					}
				}
				
				queryString = "SELECT d.operationUid, SUM(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted=false or a.isDeleted is NULL) " +
						"AND (d.isDeleted=false or a.isDeleted is NULL) " +
						"AND (a.isSimop=false or a.isSimop is NULL) " +
						"AND (a.isOffline=false or a.isOffline is NULL) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is NULL) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND (a.internalClassCode='TP' or a.internalClassCode='TU') " +
						"AND d.operationUid=:operationUid " +
						"GROUP BY d.operationUid";
				List<Object[]> durationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				Double troubleDuration = 0.0;
				if (durationList.size()>0) {
					troubleDuration = (Double) durationList.get(0)[1];
					if (troubleDuration==null) troubleDuration = 0.0;
				}
				thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration", operationUomContext);
				thisConverter.setBaseValue(troubleDuration);
				attributes.addAttribute("troubleDuration", thisConverter.getFormattedValue());
				
				Double totalCost = 0.0;
				thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
				queryString = "SELECT d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription, SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM CostDailysheet c, Daily d " +
					"WHERE (d.isDeleted=false or d.isDeleted is NULL) " +
					"AND (c.isDeleted=false or c.isDeleted is NULL) " +
					"AND d.dailyUid=c.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription ";
				List<Object[]> costList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] costItem : costList) {
					String accountCode = nullToEmptyString(costItem[1]);
					String afeShortDescription = nullToEmptyString(costItem[2]);
					String afeItemDescription = nullToEmptyString(costItem[3]);
					Double itemTotal = (Double) costItem[4];
					String key = accountCode + "::" + afeShortDescription + "::" + afeItemDescription;
					Map<String, Object> thisItem = new HashMap<String, Object>();
					if (allCostItems.containsKey(key)) {
						thisItem = allCostItems.get(key);
					} else {
						thisItem.put("accountCode", accountCode);
						thisItem.put("afeShortDescription", afeShortDescription);
						thisItem.put("afeItemDescription", afeItemDescription);
						allCostItems.put(key, thisItem);
					}
					thisConverter.setBaseValue(itemTotal);
					thisItem.put("o_" + index, thisConverter.formatOutputPrecision());
					totalCost += itemTotal;
				}
				thisConverter.setBaseValue(totalCost);
				attributes.addAttribute("totalCost", thisConverter.formatOutputPrecision());
				
				writer.addElement("Operation", null, attributes);
				
				index++;
			}
			
			List newlist = new LinkedList(allCostItems.entrySet());
			Collections.sort(newlist, new Comparator() {
				public int compare(Object o1, Object o2) {
					String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
					String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
					if (object1==null || object1==null || object2==null || object2==null) return 0;
					return (object1).compareTo(object2);
				}
			});
		    for (Iterator it = newlist.iterator(); it.hasNext();) {
		    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    	Map<String, Object> itemData = allCostItems.get(entry.getKey());
				SimpleAttributes attributes = new SimpleAttributes();
				for (Map.Entry<String, Object> itemDetail : itemData.entrySet()) {
					attributes.addAttribute(itemDetail.getKey(), nullToEmptyString(itemDetail.getValue()));
				}
				writer.addElement("CostItem", null, attributes);
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	
	public String queryAfeVsFfcByPhase_forSantos(String operationUid) {
		String xml = "<root/>";
		try {
			SimpleDateFormat dtf = new SimpleDateFormat("dd MMM yyyy HH:mm");
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			Map<String, Map> allPhases = new LinkedHashMap<String, Map>();
			SimpleAttributes opsAttrs = new SimpleAttributes();
			opsAttrs.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
			
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(operationUid, reportType);
			ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
			if (reportDaily!=null) {
				String rigInformationUid = reportDaily.getRigInformationUid();
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
				if (rig!=null) opsAttrs.addAttribute("rigName", rig.getRigName());
			}
			
			//load all phases
			String queryString = "SELECT m.operationUid, m, p FROM OperationPlanMaster m, OperationPlanPhase p " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (p.isDeleted=false or p.isDeleted is null) " +
					"AND m.operationPlanMasterUid=p.operationPlanMasterUid " +
					"AND m.operationUid=:operationUid " +
					"ORDER BY p.sequence";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				OperationPlanPhase phase = (OperationPlanPhase) rec[2];
				String phaseCode = nullToEmptyString(phase.getPhaseCode());
				phaseMap.put("phaseCode", phaseCode);
				phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
				allPhases.put("phase_"+phaseCode, phaseMap);
			}
			
			Date startDatetime = null;
			Date endDatetime = null;
			Double durationTotal = 0.0;
			Double maxDepth = 0.0;
			CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentUserSession().getUserLocale(), Activity.class, "activityDuration", operationUomContext);
			CustomFieldUom depthConverter = new CustomFieldUom(this.getCurrentUserSession().getUserLocale(), Activity.class, "depthMdMsl", operationUomContext);
			CustomFieldUom daysDurationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			
			queryString = "SELECT d.operationUid, a.phaseCode, MIN(d.dayDate), MAX(d.dayDate), SUM(coalesce(a.activityDuration,0.0)), max(coalesce(a.depthMdMsl,0.0)) " +
					"FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND d.dailyUid=a.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND a.activityDuration is not null and a.activityDuration>0.0 " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, a.phaseCode " +
					"ORDER BY d.dayDate, a.activityDuration ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[1]);
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				
				//get first entry time
				Date minDate = (Date) rec[2];
				queryString = "FROM Activity a, Daily d " +
						"WHERE (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND d.dailyUid=a.dailyUid " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND a.phaseCode=:phaseCode " +
						"AND d.operationUid=:operationUid " +
						"ORDER BY d.dayDate, a.dayPlus, a.startDatetime";
				List<Object[]> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid","phaseCode"}, new Object[]{operationUid, phaseCode}, qp);
				if (list2.size()>0) {
					Activity activity = (Activity) list2.get(0)[0];
					Date activityTime = activity.getStartDatetime();
					if (activityTime!=null) minDate.setTime(activityTime.getTime());  
				}
				
				phaseMap.put("startDatetime", dtf.format(minDate));
				if (startDatetime==null) startDatetime = minDate;
				if (endDatetime==null) endDatetime = minDate;
				if (startDatetime.after(minDate)) startDatetime = minDate;
				
				//get last entry time
				Date maxDate = (Date) rec[3];
				if (list2.size()>0) {
					Activity activity = (Activity) list2.get(list2.size()-1)[0];
					Date activityTime = activity.getEndDatetime();
					if (activityTime!=null) maxDate.setTime(activityTime.getTime());  
				}
				phaseMap.put("endDatetime", dtf.format(maxDate));
				if (endDatetime.before(maxDate)) endDatetime = maxDate;
				
				Double duration = (Double) rec[4];
				durationConverter.setBaseValue(duration);
				phaseMap.put("duration", durationConverter.getFormattedValue());
				daysDurationConverter.setBaseValue(duration);
				phaseMap.put("durationInDays", daysDurationConverter.getFormattedValue());
				durationTotal += duration;
				
				Double depthMdMsl = (Double) rec[5];
				depthConverter.setBaseValue(depthMdMsl);
				phaseMap.put("depthMdMsl", depthConverter.getFormattedValue());
				maxDepth = depthMdMsl;
			}
			
			//load all items from AFE
			Double afeTotal = 0.0;
			Map<String, Map> allCostItems = new HashMap<String, Map>();
			queryString = "SELECT d.accountCode, d.shortDescription, d.itemDescription, d.phaseCode, SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMaster m, CostAfeDetail d " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.costAfeMasterUid=d.costAfeMasterUid " +
					"AND m.operationUid=:operationUid " +
					"GROUP BY d.accountCode, d.shortDescription, d.itemDescription, d.phaseCode";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String accountCode = nullToEmptyString(rec[0]);
				String shortDescription = nullToEmptyString(rec[1]);
				String itemDescription = nullToEmptyString(rec[2]);
				String phaseCode = nullToEmptyString(rec[3]);
				Double itemTotal = (Double) rec[4];
				
				Double headerTotal = 0.0;
				String key1 = accountCode + "::" + shortDescription;
				String key2 = "rec_" + itemDescription;
				Map<String, Object> itemData = new HashMap<String, Object>();
				if (allCostItems.containsKey(key1)) {
					itemData = allCostItems.get(key1);
					if (itemData.containsKey("afe_phase_"+phaseCode)) headerTotal = (Double) itemData.get("afe_phase_"+phaseCode);
				} else {
					itemData.put("accountCode", accountCode);
					itemData.put("shortDescription", shortDescription);
					allCostItems.put(key1, itemData);
				}
				itemData.put("afe_phase_"+phaseCode, (headerTotal + itemTotal));
				afeTotal += itemTotal;
				
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				if (itemData.containsKey(key2)) {
					itemDetail = (Map<String, Object>) itemData.get(key2);
				} else {
					itemDetail.put("itemDescription", itemDescription);
					itemData.put(key2, itemDetail);
				}
				itemDetail.put("afe_phase_"+phaseCode, itemTotal);

				//add total to phase
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				Double phaseTotal = 0.0;
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
					if (phaseMap.containsKey("afeTotal")) phaseTotal = (Double) phaseMap.get("afeTotal"); 
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				phaseTotal += itemTotal;
				phaseMap.put("afeTotal", phaseTotal);
			}
			
			//load all items from AFE Adjusted
			Double afeAdjustedTotal = 0.0;
			queryString = "SELECT d.accountCode, d.shortDescription, d.itemDescription, d.phaseCode, SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMasterAdjusted m, CostAfeDetailAdjusted d " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.costAfeMasterAdjustedUid=d.costAfeMasterAdjustedUid " +
					"AND m.operationUid=:operationUid " +
					"GROUP BY d.accountCode, d.shortDescription, d.itemDescription, d.phaseCode";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String accountCode = nullToEmptyString(rec[0]);
				String shortDescription = nullToEmptyString(rec[1]);
				String itemDescription = nullToEmptyString(rec[2]);
				String phaseCode = nullToEmptyString(rec[3]);
				Double itemTotal = (Double) rec[4];
				
				Double headerTotal = 0.0;
				String key1 = accountCode + "::" + shortDescription;
				String key2 = "rec_" + itemDescription;
				Map<String, Object> itemData = new HashMap<String, Object>();
				if (allCostItems.containsKey(key1)) {
					itemData = allCostItems.get(key1);
					if (itemData.containsKey("afe_adjusted_phase_"+phaseCode)) headerTotal = (Double) itemData.get("afe_adjusted_phase_"+phaseCode);
				} else {
					itemData.put("accountCode", accountCode);
					itemData.put("shortDescription", shortDescription);
					allCostItems.put(key1, itemData);
				}
				itemData.put("afe_adjusted_phase_"+phaseCode, (headerTotal + itemTotal));
				afeAdjustedTotal += itemTotal;
				
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				if (itemData.containsKey(key2)) {
					itemDetail = (Map<String, Object>) itemData.get(key2);
				} else {
					itemDetail.put("itemDescription", itemDescription);
					itemData.put(key2, itemDetail);
				}
				itemDetail.put("afe_adjusted_phase_"+phaseCode, itemTotal);
			}
			
			//load all items from Daily Cost
			Double ffcTotal = 0.0;
			queryString = "SELECT c.accountCode, c.afeShortDescription, c.afeItemDescription, c.phaseCode, SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) " +
					"FROM Daily d, CostDailysheet c " +
					"WHERE (c.isDeleted=false or c.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND c.dailyUid=d.dailyUid " +
					"AND c.operationUid=:operationUid " +
					"GROUP BY c.accountCode, c.shortDescription, c.itemDescription, c.phaseCode";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String accountCode = nullToEmptyString(rec[0]);
				String shortDescription = nullToEmptyString(rec[1]);
				String itemDescription = nullToEmptyString(rec[2]);
				String phaseCode = nullToEmptyString(rec[3]);
				Double itemTotal = (Double) rec[4];
				
				Double headerTotal = 0.0;
				String key1 = accountCode + "::" + shortDescription;
				String key2 = "rec_" + itemDescription;
				Map<String, Object> itemData = new HashMap<String, Object>();
				if (allCostItems.containsKey(key1)) {
					itemData = allCostItems.get(key1);
					if (itemData.containsKey("actual_phase_"+phaseCode)) headerTotal = (Double) itemData.get("actual_phase_"+phaseCode);
				} else {
					itemData.put("accountCode", accountCode);
					itemData.put("shortDescription", shortDescription);
					allCostItems.put(key1, itemData);
				}
				itemData.put("actual_phase_"+phaseCode, (headerTotal + itemTotal));
				ffcTotal += itemTotal;
				
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				if (itemData.containsKey(key2)) {
					itemDetail = (Map<String, Object>) itemData.get(key2);
				} else {
					itemDetail.put("itemDescription", itemDescription);
					itemData.put(key2, itemDetail);
				}
				itemDetail.put("actual_phase_"+phaseCode, itemTotal);
				
				//add total to phase
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				Double phaseTotal = 0.0;
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
					if (phaseMap.containsKey("ffcTotal")) phaseTotal = (Double) phaseMap.get("ffcTotal"); 
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				phaseTotal += itemTotal;
				phaseMap.put("ffcTotal", phaseTotal);
			}
			
			List newlist = new LinkedList(allCostItems.entrySet());
			Collections.sort(newlist, new Comparator() {
				public int compare(Object o1, Object o2) {
					String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
					String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
					if (object1==null || object1==null || object2==null || object2==null) return 0;
					return (object1).compareTo(object2);
				}
			});
		    for (Iterator it = newlist.iterator(); it.hasNext();) {
		    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    	Map<String, Object> itemData = allCostItems.get(entry.getKey());
				SimpleAttributes attributes = new SimpleAttributes();
				Double phaseFfc = 0.0;
				Double phaseAfe = 0.0;
				Double phaseAfeAdjusted = 0.0;
				for (Map.Entry<String, Object> itemDetail : itemData.entrySet()) {
					if (itemDetail.getKey().indexOf("actual_phase_")==0 || itemDetail.getKey().indexOf("afe_adjusted_phase_")==0 || itemDetail.getKey().indexOf("afe_phase_")==0) {
						Double itemTotal = (Double) itemDetail.getValue();
						costConverter.setBaseValue(itemTotal);
						attributes.addAttribute(itemDetail.getKey(), costConverter.formatOutputPrecision());
						if (itemDetail.getKey().indexOf("actual_phase_")==0) {
							phaseFfc += itemTotal;
						} else if (itemDetail.getKey().indexOf("afe_adjusted_phase_")==0) {
							phaseAfe += itemTotal;
						} else if (itemDetail.getKey().indexOf("afe_phase_")==0) {
							phaseAfeAdjusted += itemTotal;
						}
					} else if (itemDetail.getValue() instanceof Map) {
						//to nothing at the moment
					} else {
						attributes.addAttribute(itemDetail.getKey(), nullToEmptyString(itemDetail.getValue()));
					}
				}
				costConverter.setBaseValue(phaseFfc);
				attributes.addAttribute("ffc", costConverter.formatOutputPrecision());
				costConverter.setBaseValue(phaseAfe);
				attributes.addAttribute("afe", costConverter.formatOutputPrecision());
				costConverter.setBaseValue(phaseAfeAdjusted);
				attributes.addAttribute("afeAdjusted", costConverter.formatOutputPrecision());
				
				writer.startElement("CostItem", attributes);
				
				//write description
				for (Map.Entry<String, Object> itemDetail : itemData.entrySet()) {
					if (itemDetail.getValue() instanceof Map) {
						attributes = new SimpleAttributes();
						Map<String, Object> detail = (Map<String, Object>) itemDetail.getValue();
						for (Map.Entry<String, Object> itemRecDetail : detail.entrySet()) {
							if (itemRecDetail.getKey().indexOf("actual_phase_")==0 || itemRecDetail.getKey().indexOf("afe_adjusted_phase_")==0 || itemRecDetail.getKey().indexOf("afe_phase_")==0) {
								Double itemTotal = (Double) itemRecDetail.getValue();
								costConverter.setBaseValue(itemTotal);
								attributes.addAttribute(itemRecDetail.getKey(), costConverter.formatOutputPrecision());
								if (itemRecDetail.getKey().indexOf("actual_phase_")==0) {
									phaseFfc += itemTotal;
								} else if (itemRecDetail.getKey().indexOf("afe_adjusted_phase_")==0) {
									phaseAfe += itemTotal;
								} else if (itemRecDetail.getKey().indexOf("afe_phase_")==0) {
									phaseAfeAdjusted += itemTotal;
								}
							} else {
								attributes.addAttribute(itemRecDetail.getKey(), nullToEmptyString(itemRecDetail.getValue()));
							}
						}
						writer.addElement("CostItemDetail", null, attributes);
					}
				}
				
				writer.endElement();
			}
			
		    //output phases
			for (Map.Entry<String, Map> entry : allPhases.entrySet()) {
				SimpleAttributes attributes = new SimpleAttributes();
				Map<String, Object> phaseMap = entry.getValue();
				Double diffTotal = 0.0;
				for (Map.Entry<String, Object> data : phaseMap.entrySet()) {
					if ("ffcTotal".equals(data.getKey())) {
						Double ffc = (Double)data.getValue();
						costConverter.setBaseValue(ffc);
						attributes.addAttribute(data.getKey(), costConverter.formatOutputPrecision());
						diffTotal -= ffc;
					} else if ("afeTotal".equals(data.getKey())) {
						Double afe = (Double)data.getValue();
						costConverter.setBaseValue(afe);
						attributes.addAttribute(data.getKey(), costConverter.formatOutputPrecision());
						diffTotal += afe;
					} else {
						attributes.addAttribute(data.getKey(), nullToEmptyString(data.getValue()));
					}
				}
				costConverter.setBaseValue(diffTotal);
				attributes.addAttribute("ffcDiff", costConverter.formatOutputPrecision());
				writer.addElement("Phase", null, attributes);
			}
			
			//finalize operation data
			opsAttrs.addAttribute("startDatetime", dtf.format(startDatetime));
			opsAttrs.addAttribute("endDatetime", dtf.format(endDatetime));
			
			durationConverter.setBaseValue(durationTotal);
			opsAttrs.addAttribute("duration", durationConverter.getFormattedValue());
			daysDurationConverter.setBaseValue(durationTotal);
			opsAttrs.addAttribute("durationInDays", daysDurationConverter.getFormattedValue());
			
			depthConverter.setBaseValue(maxDepth);
			opsAttrs.addAttribute("depthMdMsl", depthConverter.getFormattedValue());
			
			costConverter.setBaseValue(afeTotal);
			opsAttrs.addAttribute("afeTotal", costConverter.formatOutputPrecision());
			costConverter.setBaseValue(afeAdjustedTotal);
			opsAttrs.addAttribute("afeAdjustedTotal", costConverter.formatOutputPrecision());
			costConverter.setBaseValue(ffcTotal);
			opsAttrs.addAttribute("ffcTotal", costConverter.formatOutputPrecision());
			Double afeAdjustedDiff = afeTotal - afeAdjustedTotal;
			costConverter.setBaseValue(afeAdjustedDiff);
			opsAttrs.addAttribute("afeAdjustedDiff", costConverter.formatOutputPrecision());
			Double ffcDiff = afeTotal - ffcTotal;
			costConverter.setBaseValue(ffcDiff);
			opsAttrs.addAttribute("ffcDiff", costConverter.formatOutputPrecision());
			
			writer.addElement("Operation", null, opsAttrs);
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}

	public String queryProjectCostWellCard_forSantos(String operationUid) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			Map<String, Map> allPhases = new LinkedHashMap<String, Map>();
			SimpleAttributes opsAttrs = new SimpleAttributes();
			opsAttrs.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
	
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(operationUid, reportType);
			ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
			if (reportDaily!=null) {
				String rigInformationUid = reportDaily.getRigInformationUid();
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
				if (rig!=null) opsAttrs.addAttribute("rigName", rig.getRigName());
			}
			
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), OperationPlanPhase.class, "depthMdMsl", operationUomContext);
			opsAttrs.addAttribute("depth__uomSymbol",depthConverter.getUomSymbol());
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			opsAttrs.addAttribute("duration__uomSymbol",durationConverter.getUomSymbol());
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			opsAttrs.addAttribute("cost__uomSymbol",costConverter.getUomSymbol());
			
			Double actualSpudToTdDuration = 0.0; 
			Double afeSpudToTdDuration = 0.0;
			//load all phases
			String queryString = "SELECT m.operationUid, m, p FROM OperationPlanMaster m, OperationPlanPhase p " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (p.isDeleted=false or p.isDeleted is null) " +
					"AND m.operationPlanMasterUid=p.operationPlanMasterUid " +
					"AND m.operationUid=:operationUid " +
					"ORDER BY p.sequence";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				OperationPlanPhase phase = (OperationPlanPhase) rec[2];
				String phaseCode = nullToEmptyString(phase.getPhaseCode());
				phaseMap.put("phaseCode", phaseCode);
				phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
				
				durationConverter.setBaseValue(nullToZero(phase.getP50Duration()));
				phaseMap.put("afeDuration__raw", nullToZero(phase.getP50Duration()));
				phaseMap.put("afeDuration", durationConverter.formatOutputPrecision());
				phaseMap.put("diffDuration", durationConverter.formatOutputPrecision());
				
				depthConverter.setBaseValue(nullToZero(phase.getDepthMdMsl()));
				phaseMap.put("afeDepth", depthConverter.formatOutputPrecision());
				
				phaseMap.put("afeCost__raw", 0.0);
				allPhases.put("phase_"+phaseCode, phaseMap);
				
				//- spud to td
				if ("SH".equals(phaseCode) || "SC".equals(phaseCode) || "PH".equals(phaseCode)) {
					afeSpudToTdDuration += nullToZero(phase.getP50Duration());
				}
			}
			
			
			//load all phase budget
			queryString = "SELECT d.phaseCode, sum(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMaster m, CostAfeDetail d " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.costAfeMasterUid=d.costAfeMasterUid " +
					"AND m.operationUid=:operationUid " +
					"GROUP BY d.phaseCode ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[0]);
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					phaseMap.put("afeDuration__raw", 0.0);
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				phaseMap.put("afeCost__raw", nullToZero((Double) rec[1]));
				costConverter.setBaseValue(nullToZero((Double) rec[1]));
				phaseMap.put("afeCost", costConverter.formatOutputPrecision());
				phaseMap.put("diffCost", costConverter.formatOutputPrecision());
			}			
			//load all actual cost
			Double spudToTdCost = 0.0;
			queryString = "SELECT c.phaseCode, sum(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) " +
					"FROM CostDailysheet c, Daily d " +
					"WHERE (c.isDeleted=false or c.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND c.dailyUid=d.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY c.phaseCode ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[0]);
				Double diffCost = 0.0;
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
					if (phaseMap.containsKey("afeCost__raw")) diffCost = (Double) phaseMap.get("afeCost__raw");
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				costConverter.setBaseValue(nullToZero((Double) rec[1]));
				phaseMap.put("ffcCost", costConverter.formatOutputPrecision());
				
				diffCost -= nullToZero((Double) rec[1]);
				costConverter.setBaseValue(diffCost);
				phaseMap.put("diffCost", costConverter.formatOutputPrecision());
				
				//- spud to td
				if ("SH".equals(phaseCode) || "SC".equals(phaseCode) || "PH".equals(phaseCode)) {
					spudToTdCost += nullToZero((Double) rec[1]);
				}
			}			
			
			//load all actual depth and duration
			Double totalActualDuration = 0.0;
			Double convertedActualDepth = 0.0;
			queryString = "SELECT d.operationUid, a.phaseCode, sum(coalesce(a.activityDuration,0.0)), max(coalesce(a.depthMdMsl,0.0)) " +
					"FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.phaseCode!='' and a.phaseCode is not null) " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, a.phaseCode ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[1]);
				Double diffDuration = 0.0;
				if (allPhases.containsKey("phase_"+phaseCode)) {
					phaseMap = allPhases.get("phase_"+phaseCode);
					if (phaseMap.containsKey("afeDuration__raw")) diffDuration = (Double) phaseMap.get("afeDuration__raw");
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, true));
					allPhases.put("phase_"+phaseCode, phaseMap);
				}
				totalActualDuration += nullToZero((Double) rec[2]);
				durationConverter.setBaseValue(nullToZero((Double) rec[2]));
				phaseMap.put("actualDuration", durationConverter.formatOutputPrecision());
				
				diffDuration -= nullToZero((Double) rec[2]);
				durationConverter.setBaseValue(diffDuration);
				phaseMap.put("diffDuration", durationConverter.formatOutputPrecision());
				
				depthConverter.setBaseValue(nullToZero((Double) rec[3]));
				phaseMap.put("actualDepth", depthConverter.formatOutputPrecision());
				
				if (convertedActualDepth<depthConverter.getConvertedValue()) convertedActualDepth = depthConverter.getConvertedValue();
				
				//- spud to td
				if ("SH".equals(phaseCode) || "SC".equals(phaseCode) || "PH".equals(phaseCode)) {
					actualSpudToTdDuration += nullToZero((Double) rec[2]);
				}
			}			
			
		    //output phases
			for (Map.Entry<String, Map> entry : allPhases.entrySet()) {
				SimpleAttributes attributes = new SimpleAttributes();
				Map<String, Object> phaseMap = entry.getValue();
				for (Map.Entry<String, Object> data : phaseMap.entrySet()) {
					attributes.addAttribute(data.getKey(), nullToEmptyString(data.getValue()));
				}
				writer.addElement("Phase", null, attributes);
			}
			
			Double totalTroubleDuration = 0.0;
			Double totalUnprogrammedDuration = 0.0;
			durationConverter.setReferenceMappingField(Activity.class, "activityDuration", operationUomContext.getUomTemplateUid());
			Map<String, Object> phaseBreakDownMap = new LinkedHashMap<String, Object>();
			Map<String, Map> classBreakDownMap = new HashMap<String, Map>();
			queryString = "SELECT d.operationUid, a.phaseCode, a.classCode, sum(coalesce(a.activityDuration,0.0)), a.internalClassCode FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.phaseCode!='' and a.phaseCode is not null) " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, a.phaseCode, a.classCode, a.internalClassCode " +
					"ORDER BY d.dayDate ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> classMap = new HashMap<String, Object>();
				String classCode = nullToEmptyString(rec[2]);
				String internalClassCode = nullToEmptyString(rec[4]);
				Double classDuration = 0.0;
				if (classBreakDownMap.containsKey("class_"+classCode)) {
					classMap = classBreakDownMap.get("class_"+classCode);
					classDuration = (Double) classMap.get("duration__raw");
				} else {
					classMap.put("classCode", classCode);
					classMap.put("className", this.getClassName(classCode, false));
					classMap.put("duration_uomSymbol",durationConverter.getUomSymbol());
					classMap.put("cost_uomSymbol",costConverter.getUomSymbol());
					classMap.put("cost__raw",0.0);
					classMap.put("cost",costConverter.formatOutputPrecision(0.0));
					classBreakDownMap.put("class_"+classCode, classMap);
				}
				classDuration += nullToZero((Double) rec[3]);
				classMap.put("duration__raw", classDuration);
				durationConverter.setBaseValue(classDuration);
				classMap.put("duration", durationConverter.formatOutputPrecision());
				
				if ("TP".equals(internalClassCode) || "TU".equals(internalClassCode)) {
					totalTroubleDuration += nullToZero((Double) rec[3]);
				} else if ("U".equals(internalClassCode)) {
					totalUnprogrammedDuration += nullToZero((Double) rec[3]);
				}
				
				//phase class breakdown
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				Map<String, Object> phaseClassMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[1]);
				if (phaseBreakDownMap.containsKey("phase_"+phaseCode)) {
					phaseMap = (Map<String, Object>) phaseBreakDownMap.get("phase_"+phaseCode);
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, false));
					phaseBreakDownMap.put("phase_"+phaseCode, phaseMap);
				}
				if (phaseMap.containsKey("class_"+classCode)) {
					phaseClassMap = (Map<String, Object>) phaseMap.get("class_"+classCode);
				} else {
					phaseClassMap.put("classCode", classCode);
					phaseClassMap.put("className", this.getClassName(classCode, false));
					phaseClassMap.put("duration_uomSymbol",durationConverter.getUomSymbol());
					phaseClassMap.put("cost_uomSymbol",costConverter.getUomSymbol());
					phaseMap.put("class_"+classCode, phaseClassMap);
				}
				durationConverter.setBaseValue(nullToZero((Double) rec[3]));
				phaseClassMap.put("duration", durationConverter.formatOutputPrecision());
			}
			
			queryString = "SELECT d.operationUid, c.phaseCode, c.classCode, sum(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) " +
					"FROM CostDailysheet c, Daily d " +
					"WHERE (c.isDeleted=false or c.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND c.dailyUid=d.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, c.phaseCode, c.classCode " +
					"ORDER BY d.dayDate ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> classMap = new HashMap<String, Object>();
				String classCode = nullToEmptyString(rec[2]);
				Double classCost = 0.0;
				if (classBreakDownMap.containsKey("class_"+classCode)) {
					classMap = classBreakDownMap.get("class_"+classCode);
					classCost = (Double) classMap.get("cost__raw");
				} else {
					classMap.put("classCode", classCode);
					classMap.put("className", this.getClassName(classCode, false));
					classMap.put("duration_uomSymbol",durationConverter.getUomSymbol());
					classMap.put("cost_uomSymbol",costConverter.getUomSymbol());
					classMap.put("duration",durationConverter.formatOutputPrecision(0.0));
					classBreakDownMap.put("class_"+classCode, classMap);
				}
				classCost += nullToZero((Double) rec[3]);
				classMap.put("cost__raw", classCost);
				costConverter.setBaseValue(classCost);
				classMap.put("cost", costConverter.formatOutputPrecision());
				
				// phase class breakdown
				Map<String, Object> phaseMap = new HashMap<String, Object>();
				Map<String, Object> phaseClassMap = new HashMap<String, Object>();
				String phaseCode = nullToEmptyString(rec[1]);
				if (phaseBreakDownMap.containsKey("phase_"+phaseCode)) {
					phaseMap = (Map<String, Object>) phaseBreakDownMap.get("phase_"+phaseCode);
				} else {
					phaseMap.put("phaseCode", phaseCode);
					phaseMap.put("phaseName", this.getPhaseName(phaseCode, false));
					phaseBreakDownMap.put("phase_"+phaseCode, phaseMap);
				}
				if (phaseMap.containsKey("class_"+classCode)) {
					phaseClassMap = (Map<String, Object>) phaseMap.get("class_"+classCode);
				} else {
					phaseClassMap.put("classCode", classCode);
					phaseClassMap.put("className", this.getClassName(classCode, false));
					phaseClassMap.put("duration_uomSymbol",durationConverter.getUomSymbol());
					phaseClassMap.put("cost_uomSymbol",costConverter.getUomSymbol());
					phaseMap.put("class_"+classCode, phaseClassMap);
				}
				costConverter.setBaseValue(nullToZero((Double) rec[3]));
				phaseClassMap.put("cost", costConverter.formatOutputPrecision());
			}

			
		    //output class breakdown
			for (Map.Entry<String, Map> entry : classBreakDownMap.entrySet()) {
				SimpleAttributes attributes = new SimpleAttributes();
				Map<String, Object> phaseMap = entry.getValue();
				for (Map.Entry<String, Object> data : phaseMap.entrySet()) {
					attributes.addAttribute(data.getKey(), nullToEmptyString(data.getValue()));
				}
				writer.addElement("ClassBreakdown", null, attributes);
			}
			
		    //output phase/class breakdown
			for (Map.Entry<String, Object> entry : phaseBreakDownMap.entrySet()) {
				writer.startElement("PhaseBreakdown");
				Map<String, Object> phaseMap = (Map<String, Object>) entry.getValue();
				for (Map.Entry<String, Object> data : phaseMap.entrySet()) {
					if (data.getValue() instanceof Map) {
						SimpleAttributes attributes = new SimpleAttributes();
						for (Map.Entry<String, Object> data2 : ((Map<String, Object>)data.getValue()).entrySet()) {
							attributes.addAttribute(data2.getKey(), nullToEmptyString(data2.getValue()));
						}
						writer.addElement("Class", null, attributes);
					} else {
						writer.addElement(data.getKey(), nullToEmptyString(data.getValue()));
					}
				}
				writer.endElement();
			}
			
			//operation breakdown - GWP = "datProjectWellCardTaskCodes"
			Double totalWaitOnDuration = 0.0;
			String taskCodes = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "datProjectWellCardTaskCodes");
			String[] arTaskCodes = taskCodes.split(";");
			for (String taskCode : arTaskCodes) {
				SimpleAttributes attributes = new SimpleAttributes();
				Double totalDuration = 0.0;
				Double totalCost = 0.0;
				queryString = "select d.operationUid, d.dailyUid, a.classCode, a.phaseCode, a.taskCode, sum(coalesce(a.activityDuration, 0.0)) " +
						"FROM Daily d, Activity a " +
						"WHERE (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND d.dailyUid=a.dailyUid " +
						"AND a.taskCode=:taskCode " +
						"AND d.operationUid=:operationUid " +
						"GROUP BY d.operationUid, d.dailyUid, a.classCode, a.phaseCode, a.taskCode";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"taskCode", "operationUid"}, new Object[]{taskCode, operationUid}, qp);
				for (Object[] rec : list) {
					Double thisDuration = nullToZero((Double) rec[5]);
					if (thisDuration>0.0) {
						totalDuration += thisDuration;
						
						String thisDailyUid = nullToEmptyString(rec[1]);
						String thisClassCode = nullToEmptyString(rec[2]);
						String thisPhaseCode = nullToEmptyString(rec[3]);
						Double thisBreakdownDuration = 0.0;
						Double thisBreakdownCost = 0.0;
						queryString = "select a.dailyUid, sum(coalesce(a.activityDuration, 0.0)) FROM Activity a " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND a.dailyUid=:dailyUid " +
								"AND a.classCode=:classCode " +
								"AND a.phaseCode=:phaseCode " +
								"GROUP BY a.dailyUid";
						List<Object[]> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid", "classCode", "phaseCode"}, new Object[]{thisDailyUid, thisClassCode, thisPhaseCode}, qp);
						if (list2.size()>0) {
							Object[] rec2 = list2.get(0);
							thisBreakdownDuration = nullToZero((Double)rec2[1]);
						}
						
						queryString = "select c.dailyUid, sum(coalesce(c.itemCost, 0.0) * coalesce(c.quantity, 0.0)) FROM CostDailysheet c " +
								"WHERE (c.isDeleted=false or c.isDeleted is null) " +
								"AND c.dailyUid=:dailyUid " +
								"AND c.classCode=:classCode " +
								"AND c.phaseCode=:phaseCode " +
								"AND c.hourlyCalc=1 " +
								"GROUP BY c.dailyUid";
						list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid", "classCode", "phaseCode"}, new Object[]{thisDailyUid, thisClassCode, thisPhaseCode}, qp);
						if (list2.size()>0) {
							Object[] rec2 = list2.get(0);
							thisBreakdownCost = nullToZero((Double)rec2[1]);
						}
						if (thisBreakdownDuration>0.0) {
							thisBreakdownCost = thisDuration * thisBreakdownCost / thisBreakdownDuration;
							totalCost += thisBreakdownCost; 
						}
					}
				}
				if (totalDuration>0.0) {
					attributes.addAttribute("taskCode", taskCode);
					attributes.addAttribute("taskName", this.getTaskName(taskCode, false));
					
					totalWaitOnDuration += totalDuration;
					durationConverter.setBaseValue(totalDuration);
					attributes.addAttribute("duration", durationConverter.formatOutputPrecision());
					costConverter.setBaseValue(totalCost);
					attributes.addAttribute("cost", costConverter.formatOutputPrecision());
					writer.addElement("TaskBreakdown", null, attributes);
				}
			}
			
			//account code breakdown
			Map<String, Map> accountCodesMap = new HashMap<String, Map>();
			queryString = "SELECT m.operationUid, d.accountCode, d.shortDescription, d.itemDescription, sum(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMaster m, CostAfeDetail d " +
					"WHERE m.costAfeMasterUid=d.costAfeMasterUid " +
					"AND (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.operationUid=:operationUid " +
					"GROUP BY m.operationUid, d.accountCode, d.shortDescription, d.itemDescription";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String accountCode = nullToEmptyString(rec[1]);
				String shortDescription = nullToEmptyString(rec[2]);
				String itemDescription = nullToEmptyString(rec[3]);
				Double afeTotal = nullToZero((Double) rec[4]);
				String key = accountCode + "::" + shortDescription;
				Map<String, Object> accountCodeMap = new HashMap<String, Object>();
				Map<String, String> itemDescriptions = new HashMap<String, String>();
				if (accountCodesMap.containsKey(key)) {
					accountCodeMap = accountCodesMap.get(key);
					afeTotal += (Double)accountCodeMap.get("afeTotal");
					accountCodeMap.put("afeTotal", afeTotal);
					if (accountCodeMap.containsKey("itemDescription")) itemDescriptions = (Map<String,String>)accountCodeMap.get("itemDescription");
					if (StringUtils.isNotBlank(itemDescription)) itemDescriptions.put(itemDescription.trim(), itemDescription.trim());
				} else {
					accountCodeMap.put("accountCode", accountCode);
					accountCodeMap.put("shortDescription", shortDescription);
					if (StringUtils.isNotBlank(itemDescription)) itemDescriptions.put(itemDescription.trim(), itemDescription.trim());
					accountCodeMap.put("itemDescription", itemDescriptions);
					accountCodeMap.put("afeTotal", afeTotal);
					accountCodeMap.put("ffcTotal", 0.0);
					accountCodesMap.put(key, accountCodeMap);
				}
			}

			queryString = "SELECT d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription, sum(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) " +
					"FROM CostDailysheet c, Daily d " +
					"WHERE c.dailyUid=d.dailyUid " +
					"AND (c.isDeleted=false or c.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String accountCode = nullToEmptyString(rec[1]);
				String shortDescription = nullToEmptyString(rec[2]);
				String itemDescription = nullToEmptyString(rec[3]);
				Double ffcTotal = nullToZero((Double) rec[4]);
				String key = accountCode + "::" + shortDescription;
				Map<String, Object> accountCodeMap = new HashMap<String, Object>();
				Map<String, String> itemDescriptions = new HashMap<String, String>();
				if (accountCodesMap.containsKey(key)) {
					accountCodeMap = accountCodesMap.get(key);
					ffcTotal += (Double)accountCodeMap.get("ffcTotal");
					accountCodeMap.put("ffcTotal", ffcTotal);
					if (accountCodeMap.containsKey("itemDescription")) itemDescriptions = (Map<String,String>)accountCodeMap.get("itemDescription");
					if (StringUtils.isNotBlank(itemDescription)) itemDescriptions.put(itemDescription.trim(), itemDescription.trim());
				} else {
					accountCodeMap.put("accountCode", accountCode);
					accountCodeMap.put("shortDescription", shortDescription);
					if (StringUtils.isNotBlank(itemDescription)) itemDescriptions.put(itemDescription.trim(), itemDescription.trim());
					accountCodeMap.put("itemDescription", itemDescriptions);
					accountCodeMap.put("afeTotal", 0.0);
					accountCodeMap.put("ffcTotal", ffcTotal);
					accountCodesMap.put(key, accountCodeMap);
				}
			}
			List mapList = new LinkedList(accountCodesMap.entrySet());
			Collections.sort(mapList, new Comparator() {
				public int compare(Object o1, Object o2) {
					String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
					String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
					if (object1==null || object1==null || object2==null || object2==null) return 0;
					return (object1).compareTo(object2);
				}
			});
			Double wellAfeTotal = 0.0;
			Double wellFfcTotal = 0.0;
		    for (Iterator it = mapList.iterator(); it.hasNext();) {
		    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    	Map<String, Object> itemData = accountCodesMap.get(entry.getKey());
		    	SimpleAttributes attribute = new SimpleAttributes();
		    	Double afeTotal = 0.0;
		    	Double ffcTotal = 0.0;
		    	String descriptions = "";
		    	for (Map.Entry<String, Object> data : itemData.entrySet()) {
					if ("itemDescription".equals(data.getKey())) {
						Map<String, Object> itemDescriptions = (Map<String, Object>) data.getValue();
						for (Map.Entry<String, Object> item2 : itemDescriptions.entrySet()) {
							if (StringUtils.isBlank(descriptions)) {
								descriptions = "- " + nullToEmptyString(item2.getValue());
							} else {
								descriptions += "\n- " + nullToEmptyString(item2.getValue());
							}
						}
					} else if ("shortDescription".equals(data.getKey())) {
						if (StringUtils.isBlank(descriptions)) {
							descriptions = nullToEmptyString(data.getValue());
						} else {
							descriptions = nullToEmptyString(data.getValue()) + "\n" + descriptions;
						}
					} else if ("afeTotal".equals(data.getKey())) {
						afeTotal = nullToZero((Double) data.getValue());
						wellAfeTotal += afeTotal;
						costConverter.setBaseValue(afeTotal);
						attribute.addAttribute(data.getKey(), costConverter.formatOutputPrecision());
					} else if ("ffcTotal".equals(data.getKey())) {
						ffcTotal = nullToZero((Double) data.getValue());
						wellFfcTotal += ffcTotal;
						costConverter.setBaseValue(ffcTotal);
						attribute.addAttribute(data.getKey(), costConverter.formatOutputPrecision());
					} else {
						attribute.addAttribute(data.getKey(), nullToEmptyString(data.getValue()));
					}
		    	}		
		    	Double diffCost = afeTotal - ffcTotal;
		    	long percent = 0;
				costConverter.setBaseValue(diffCost);
				attribute.addAttribute("diffCost", costConverter.formatOutputPrecision());
		    	if (afeTotal>0.0 || afeTotal<0.0) {
		    		percent = Math.round(ffcTotal * 100 / afeTotal);
		    	}
		    	attribute.addAttribute("percent", percent+"");
		    	attribute.addAttribute("descriptions", descriptions);
		    	
		    	writer.addElement("CostItem", null, attribute);
		    }			
		    
			costConverter.setBaseValue(wellAfeTotal);
			opsAttrs.addAttribute("afeTotal", costConverter.formatOutputPrecision());
			costConverter.setBaseValue(wellFfcTotal);
			opsAttrs.addAttribute("ffcTotal", costConverter.formatOutputPrecision());
			
			Double wellDiffTotal = wellAfeTotal - wellFfcTotal;
			costConverter.setBaseValue(wellDiffTotal);
			opsAttrs.addAttribute("diffCost", costConverter.formatOutputPrecision());
			
			long percent = 0;
	    	if (wellAfeTotal>0.0 || wellAfeTotal<0.0) {
	    		percent = Math.round(wellFfcTotal * 100 / wellAfeTotal);
	    	}
	    	opsAttrs.addAttribute("percent", percent+"");
			
			//Project Performance Indicator
	    	DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(this.getCurrentHttpRequest()));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
	    	if (afeSpudToTdDuration>0.0) {
		    	//- Spud to TD vs AFE
	    		Double spudToTdVsAfe =  Math.round(actualSpudToTdDuration / afeSpudToTdDuration  * 100) / 100.0;
	    		opsAttrs.addAttribute("spudToTdVsAfe", formatter.format(spudToTdVsAfe));
	    	}
	    	
	    	if (convertedActualDepth>0.0 || convertedActualDepth<0.0) {
		    	//- Total Drilling Project $/ft
	    		Double totalDrillingProjectCostPerFeet = Math.round(wellFfcTotal * 100.0 / convertedActualDepth) / 100.0;
		    	opsAttrs.addAttribute("totalDrillingProjectCostPerFeet", formatter.format(totalDrillingProjectCostPerFeet));
		    	//- Spud to TD $/ft
		    	Double spudToTdCostPerFeet = Math.round(spudToTdCost * 100.0 / convertedActualDepth) / 100.0;
		    	opsAttrs.addAttribute("spudToTdCostPerFeet", formatter.format(spudToTdCostPerFeet));
	    	}
	    	
	    	if (totalActualDuration>0.0) {
		    	//- % NPT :Operational
		    	Double nptOperational = Math.round(totalTroubleDuration * 100.00 * 100.0 / totalActualDuration) / 100.0;
		    	opsAttrs.addAttribute("nptOperational", formatter.format(nptOperational));
		    	//- % NPT :Other
		    	Double nptOther = Math.round(totalUnprogrammedDuration * 100.00 * 100.0 / totalActualDuration) / 100.0;
		    	opsAttrs.addAttribute("nptOther", formatter.format(nptOther));
		    	//- % NPT :Total
		    	Double nptTotal = Math.round((totalTroubleDuration + totalUnprogrammedDuration) * 100.00 * 100.0 / totalActualDuration) / 100.0;
		    	opsAttrs.addAttribute("nptTotal", formatter.format(nptTotal));
		    	//- % NPT :Waiting On...
	    		Double nptWaitOn = Math.round(totalWaitOnDuration * 100.00 * 100.0 / totalActualDuration) / 100.0;
	    		opsAttrs.addAttribute("nptWaitOn", formatter.format(nptWaitOn));
	    	}
			
		    writer.addElement("Operation", null, opsAttrs);
		    
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}

	public String queryAFESummaryFromWells_forSantos(String operationUid) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
			Map<String, Map> categories = new HashMap<String, Map>();
			Map<String, LookupItem> categorySequenceLookup = LookupManager.getConfiguredInstance().getLookup("db://CostAccountCodesCategory?key=categoryCode&value=categoryDescription&order=sequence", userSelection, new LookupCache());
			Map<String, Map> allPhases = new LinkedHashMap<String, Map>();
			Map<String, String> phaseInCostType = new HashMap<String, String>();
			
			SimpleAttributes opsAttrs = new SimpleAttributes();
			opsAttrs.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
			
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), OperationPlanPhase.class, "depthMdMsl", operationUomContext);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			
			Double cumDuration = 0.0;
			Double spudToTdDuration = 0.0;
			Double spudToRigReleaseDuration = 0.0;
			
			Double maxDepth = 0.0;
			String costType = null;
			String operationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			String queryString = "select m.operationUid, m, p FROM OperationPlanMaster m, OperationPlanPhase p " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (p.isDeleted=false or p.isDeleted is null) " +
					"AND m.operationPlanMasterUid=p.operationPlanMasterUid " +
					"AND m.operationPlanMasterUid=:operationPlanMasterUid " +
					"ORDER BY p.sequence";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationPlanMasterUid", operationPlanMasterUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> phase = new HashMap<String, Object>();
				if (costType==null) {
					OperationPlanMaster planMaster = (OperationPlanMaster) rec[1];
					costType = nullToEmptyString(planMaster.getCostPhaseType());
					opsAttrs.addAttribute("costType", nullToEmptyString(costType));
					
					String gwpvalue = null;
					if ("CS".equalsIgnoreCase(costType)) {
						gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
					}else if ("PA".equalsIgnoreCase(costType)) {
						gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
					}
					
					if (StringUtils.isNotBlank(gwpvalue)) {
						for (String item:gwpvalue.toString().split(";")) {
							if (StringUtils.isNotBlank(item)) {
								phaseInCostType.put(item, item);
							}
						}
					}
				}
				OperationPlanPhase planPhase = (OperationPlanPhase) rec[2];
				String phaseCode = nullToEmptyString(planPhase.getPhaseCode());
				
				phase.put("phaseCode", phaseCode);
				phase.put("sequence", planPhase.getSequence());
				phase.put("phaseName", this.getPhaseName(phaseCode, false));
				phase.put("phaseSummary", planPhase.getPhaseSummary());
				
				depthConverter.setBaseValue(nullToZero(planPhase.getDepthMdMsl()));
				phase.put("plannedDepth", depthConverter.formatOutputPrecision());
				phase.put("plannedDepth_uomSymbol", depthConverter.getUomSymbol());
				if (maxDepth<depthConverter.getConvertedValue()) maxDepth = depthConverter.getConvertedValue();
				
				durationConverter.setBaseValue(nullToZero(planPhase.getP50Duration()));
				phase.put("plannedDuration", durationConverter.formatOutputPrecision());
				phase.put("plannedDuration_uomSymbol", durationConverter.getUomSymbol());
				
				if (!phaseInCostType.containsKey(phaseCode)) {
					cumDuration += nullToZero(planPhase.getP50Duration()); //add to cumulative if the phase is in the cost type
					durationConverter.setBaseValue(cumDuration);
					if ("SH".equals(phaseCode) || "SC".equals(phaseCode) || "PH".equals(phaseCode)) { // SH, SC, PH
						spudToTdDuration += nullToZero(planPhase.getP50Duration());
						spudToRigReleaseDuration += nullToZero(planPhase.getP50Duration());
					} else if ("EP".equals(phaseCode) || "PC".equals(phaseCode) || "PA".equals(phaseCode)) { //SH, SC, PH, EP, PC, PA 
						spudToRigReleaseDuration += nullToZero(planPhase.getP50Duration());
					}
				} else {
					durationConverter.setBaseValue(cumDuration +nullToZero(planPhase.getP50Duration())); // just add and display without adding to cummulative
				}
				phase.put("cumDuration", durationConverter.formatOutputPrecision());
				phase.put("cumDuration_uomSymbol", durationConverter.getUomSymbol());
				
				phase.put("phaseCost", 0.0);//default to 0.0
				phase.put("phaseCost_uomSymbol", costConverter.getUomSymbol());
				
				allPhases.put("phase_" + phaseCode, phase);
				
				
			}
			
			//account code listing
			queryString = "select m.operationUid, m.afeNumber, d.category, d.phaseCode, d.accountCode, d.shortDescription, d.itemDescription, sum(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMaster m, CostAfeDetail d " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.costAfeMasterUid=d.costAfeMasterUid " +
					"AND m.operationUid=:operationUid " +
					"GROUP BY m.operationUid, d.category, d.phaseCode, d.accountCode, d.shortDescription, d.itemDescription";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> categoryMap = new HashMap<String, Object>();
				Map<String, Map> costItems = new HashMap<String, Map>();
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				if (operation!=null && StringUtils.isNotBlank(operation.getAfeNumber())) {
					opsAttrs.addAttribute("afeNumber", operation.getAfeNumber());
				} else {
					String afeNumber = nullToEmptyString(rec[1]);
					opsAttrs.addAttribute("afeNumber", afeNumber);
				}
				String category = nullToEmptyString(rec[2]);
				String phaseCode = nullToEmptyString(rec[3]);
				String accountCode = nullToEmptyString(rec[4]);
				String shortDescription = nullToEmptyString(rec[5]);
				String itemDescription = nullToEmptyString(rec[6]);
				Double itemTotal = nullToZero((Double)rec[7]);
				
				Double totalCategoryCost = 0.0;
				Double totalCategoryPhaseCost = 0.0;
				if (categories.containsKey("category_"+category)) {
					categoryMap = categories.get("category_"+category);
					totalCategoryCost = (Double) categoryMap.get("totalCost");
					costItems = (Map<String, Map>) categoryMap.get("costItems");
					if (categoryMap.containsKey("phaseCode_"+phaseCode)) {
						totalCategoryPhaseCost = (Double) categoryMap.get("phaseCode_"+phaseCode); 
					}
				} else {
					categoryMap.put("categoryName", category);
					categoryMap.put("costItems", costItems);
					categoryMap.put("totalCost", 0.0);
					categories.put("category_"+category, categoryMap);
				}
				categoryMap.put("phaseCode_"+phaseCode, totalCategoryPhaseCost + itemTotal);
				if (!phaseInCostType.containsKey(phaseCode)) {
					categoryMap.put("totalCost", totalCategoryCost + itemTotal);
				}
				
				Map<String, Object> costItem = new HashMap<String, Object>();
				String itemKey = accountCode+"::"+shortDescription+"::"+itemDescription;
				Double lineItemTotal = 0.0;
				if (costItems.containsKey(itemKey)) {
					costItem = costItems.get(itemKey);
					lineItemTotal = (Double) costItem.get("totalCost");
				} else {
					costItem.put("accountCode", accountCode);
					costItem.put("shortDescription", shortDescription);
					costItem.put("itemDescription", itemDescription);
					costItem.put("totalCost", 0.0);
					costItems.put(itemKey, costItem);
				}
				costItem.put("phaseCode_"+phaseCode, itemTotal);
				if (!phaseInCostType.containsKey(phaseCode)) {
					costItem.put("totalCost", lineItemTotal + itemTotal);
				}
				
				if (allPhases.containsKey("phase_"+phaseCode)) {
					Map<String, Object> phase = (Map<String, Object>) allPhases.get("phase_"+phaseCode);
					Double phaseCost = (Double) phase.get("phaseCost");
					phaseCost += itemTotal;
					phase.put("phaseCost", phaseCost);
				}
			}
			
			//process output
			Double cumCost = 0.0;
			for (Map.Entry<String, Map> entry : allPhases.entrySet()) {
				SimpleAttributes attr = new SimpleAttributes();
				Map<String,Object> phase = (Map<String,Object>)entry.getValue();
				String phaseCode = nullToEmptyString(phase.get("phaseCode"));
				for (Map.Entry<String, Object> field : phase.entrySet()) {
					if ("phaseCost".equals(field.getKey())) {
						costConverter.setBaseValue(nullToZero((Double)field.getValue()));
						attr.addAttribute(field.getKey(), costConverter.formatOutputPrecision());
						if (!phaseInCostType.containsKey(phaseCode)) {
							cumCost += nullToZero((Double)field.getValue());
							costConverter.setBaseValue(nullToZero(cumCost));
						} else {
							costConverter.setBaseValue(cumCost + nullToZero((Double)field.getValue()));
						}
						attr.addAttribute("cumPhaseCost", costConverter.formatOutputPrecision());
						attr.addAttribute("cumPhaseCost_uomSymbol", costConverter.getUomSymbol());
					} else {
						attr.addAttribute(field.getKey(), nullToEmptyString(field.getValue()));
					}
				}
				writer.addElement("Phase", null, attr);
			}
			
			
			Map<String,String> categorySort = new LinkedHashMap<String,String>();
			for (Map.Entry<String, LookupItem> entry : categorySequenceLookup.entrySet()) {
				LookupItem categoryItem = entry.getValue();
				String category = nullToEmptyString(categoryItem.getValue());
				if (categories.containsKey(category)) categorySort.put(category,category);
			}
			
			List mapList = new LinkedList(categories.entrySet());
			Collections.sort(mapList, new Comparator() {
				public int compare(Object o1, Object o2) {
					String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
					String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
					if (object1==null || object1==null || object2==null || object2==null) return 0;
					return (object1).compareTo(object2);
				}
			});
		    for (Iterator it = mapList.iterator(); it.hasNext();) {
		    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    	if (!categorySort.containsKey(entry.getKey())) categorySort.put(entry.getKey(), entry.getKey());
		    }
		    
		    for (Map.Entry<String, String> category : categorySort.entrySet()) {
		    	if (categories.containsKey(category.getKey())) {
		    		Map<String, Object> categoryMap = categories.get(category.getKey());
		    		writer.startElement("Category");
		    		for (Map.Entry<String, Object> categoryData : categoryMap.entrySet()) {
		    			if (categoryData.getValue() instanceof Map) {
		    				if ("costItems".equals(categoryData.getKey())) {
		    					Map<String, Map> costItems = (Map<String, Map>) categoryData.getValue();
		    					mapList = new LinkedList(costItems.entrySet());
		    					Collections.sort(mapList, new Comparator() {
		    						public int compare(Object o1, Object o2) {
		    							String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
		    							String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
		    							if (object1==null || object1==null || object2==null || object2==null) return 0;
		    							return (object1).compareTo(object2);
		    						}
		    					});
		    				    for (Iterator it = mapList.iterator(); it.hasNext();) {
		    				    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    				    	Map<String, Object> itemData = costItems.get(entry.getKey());
		    				    	SimpleAttributes attr = new SimpleAttributes();
		    				    	for (Map.Entry<String, Object> field : itemData.entrySet()) {
		    				    		if (field.getValue() instanceof Double) {
		    			    				costConverter.setBaseValue(nullToZero((Double) field.getValue()));
		    			    				attr.addAttribute(field.getKey(), costConverter.formatOutputPrecision());
		    				    		} else {
		    				    			attr.addAttribute(field.getKey(), nullToEmptyString(field.getValue()));
		    				    		}
		    				    	}
		    				    	writer.addElement("CostItem", null, attr);
		    				    }

		    				}
		    			} else if (categoryData.getValue() instanceof Double) {
		    				costConverter.setBaseValue(nullToZero((Double) categoryData.getValue()));
		    				writer.addElement(categoryData.getKey(), costConverter.formatOutputPrecision());
		    			} else {
	    					writer.addElement(categoryData.getKey(), nullToEmptyString(categoryData.getValue()));
		    			}
		    		}
		    		writer.endElement();
		    	}
		    }
			
			//cost per feet
		    costConverter.setBaseValue(cumCost);
		    opsAttrs.addAttribute("totalCost", costConverter.formatOutputPrecision());
		    opsAttrs.addAttribute("totalCost_uomSymbol", costConverter.getUomSymbol());
		    		
			if (maxDepth>0.0 || maxDepth<0.0) {
				Double costPerFeet = Math.round(cumCost * 100.0 / maxDepth) / 100.0;
				opsAttrs.addAttribute("costPerFeet", costConverter.formatOutputPrecision(costPerFeet));
				opsAttrs.addAttribute("costPerFeet_uomSymbol", costConverter.getUomSymbol() + "/" + depthConverter.getUomSymbol());
			}
			
			durationConverter.setBaseValue(spudToTdDuration);
			opsAttrs.addAttribute("spudToTdDuration", durationConverter.getFormattedValue());
			durationConverter.setBaseValue(spudToRigReleaseDuration);
			opsAttrs.addAttribute("spudToRigReleaseDuration", durationConverter.getFormattedValue());
			durationConverter.setBaseValue(cumDuration);
			opsAttrs.addAttribute("rigMoveToRigReleaseDuration", durationConverter.getFormattedValue());
			
			writer.addElement("Operation", null, opsAttrs);
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	public String queryBhaOperationTimeBreakdown_forSantos(String[] operationUids) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
					
			for (String operationUid : operationUids) {
				writer.startElement("datarow");
		
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
				writer.addElement("operationName", operationName);
				operationUomContext.setOperationUid(operationUid);
				CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration", operationUomContext);
				writer.addElement("duration_uomSymbol", durationConverter.getUomSymbol());
				CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Bharun.class, "depthInMdMsl", operationUomContext);
				writer.addElement("depth_uomSymbol", depthConverter.getUomSymbol());
				Map<String, Map> bhaMap = new LinkedHashMap<String, Map>();
				
				String queryString = "select o.operationUid, a.bhaRunNumber, a.depthInMdMsl, a.depthOutMdMsl, a.bharunUid FROM Bharun a, BharunDailySummary b, Operation o, Daily d " +
				"WHERE a.operationUid=:operationUid " +
				"AND (a.isDeleted = false OR a.isDeleted IS NULL) " +
				"AND (b.isDeleted = false OR b.isDeleted IS NULL) " +
				"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
				"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
				"AND a.operationUid = o.operationUid " +
				"AND b.bharunUid = a.bharunUid " +
				"AND a.dailyidIn = d.dailyUid " +
				"ORDER BY d.dayDate";
				
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {			
					Double depthInMdMsl = 0.00;
					Double depthOutMdMsl = 0.00;
					Double depthInterval = 0.00;	
					
					String bhaRunNumber = (String) rec[1];
					bhaRunNumber = StringUtils.isBlank(bhaRunNumber)?"":bhaRunNumber;
					
					String bharunUid = (String) rec[4];
					bharunUid = StringUtils.isBlank(bharunUid)?"":bharunUid;
					
					depthInMdMsl = nullToZero((Double) rec[2]);
					depthOutMdMsl = nullToZero((Double) rec[3]);
					depthInterval = depthOutMdMsl - depthInMdMsl;
					
					Map<String, Object> bhaData = null;
					Map<String, Map> activityCodes_D = null;
					Map<String, Map> activityCodes_ND = null;
					
					if (bhaMap.containsKey(bharunUid)) {
						bhaData = bhaMap.get(bharunUid);
						activityCodes_D = (Map<String, Map>) bhaData.get("ActivityCodes_D");
						activityCodes_ND = (Map<String, Map>) bhaData.get("ActivityCodes_ND");
					} else {
						bhaData = new HashMap<String, Object>();							
						bhaData.put("bharunUid", bharunUid);
						bhaData.put("bhaRunNumber", bhaRunNumber);				
						depthConverter.setBaseValue(depthInMdMsl);
						bhaData.put("depthInMdMsl", depthConverter.formatOutputPrecision());
						depthConverter.setBaseValue(depthOutMdMsl);
						bhaData.put("depthOutMdMsl", depthConverter.formatOutputPrecision());
						depthConverter.setBaseValue(depthInterval);
						bhaData.put("depthInterval", depthConverter.formatOutputPrecision());
						
						activityCodes_D = new HashMap<String, Map>();	
						activityCodes_ND = new HashMap<String, Map>();	
						bhaData.put("ActivityCodes_D", activityCodes_D);
						bhaData.put("ActivityCodes_ND", activityCodes_ND);
						bhaMap.put("bha_" + bharunUid, bhaData);
					}
					
					Double totalDuration = 0.00;
					Double duration = 0.00;
					queryString = "select o.operationUid, coalesce(act.phaseCode,''), coalesce(o.operationCode,''), SUM(coalesce(act.activityDuration,0.0)) as totalDuration, coalesce(act.taskCode,'') FROM Bharun a, BharunDailySummary b, Operation o, Activity act, Daily d " +
						"WHERE a.bharunUid=:bharunUid " +
						"AND (a.isDeleted = false OR a.isDeleted IS NULL) " +
						"AND (b.isDeleted = false OR b.isDeleted IS NULL) " +
						"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
						"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
						"AND (act.isDeleted = false OR act.isDeleted IS NULL) " +
						"AND (act.isSimop=false or act.isSimop is null) " +
						"AND (act.isOffline=false or act.isOffline is null) " +
						"AND (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) " +
						"AND act.activityDuration>0.0 " +
						"AND a.operationUid = o.operationUid " +
						"AND b.bharunUid = a.bharunUid " +
						"AND act.dailyUid = d.dailyUid " +
						"AND act.dailyUid = b.dailyUid " +
						"AND act.taskCode <> 'SPUD' " +
						"AND act.phaseCode IN ('SH','IH','PH') " +
						"GROUP BY coalesce(act.phaseCode,''), coalesce(act.taskCode,'') " +
						"ORDER BY act.startDatetime";
					
					list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "bharunUid", bharunUid, qp);
					for (Object[] result : list) {
						duration = (Double) result[3];
						totalDuration = totalDuration + duration;	
						String taskCode = (String)result[4];
						String key = "bha_" + bharunUid + "_" + taskCode;
						durationConverter.setBaseValue(duration);
						String operationCode = (String)result[2];
						
						if (checkTaskCode(operationCode, taskCode)){ //list of task code
							Map<String, Object> breakdownMap_D = new HashMap<String, Object>(); 				
							breakdownMap_D.put("phaseCode", (String)result[1]);
							breakdownMap_D.put("taskCode", (String)result[4]);
							breakdownMap_D.put("duration", duration);
							breakdownMap_D.put("duration_text", durationConverter.formatOutputPrecision());
							activityCodes_D.put(key, breakdownMap_D);	
							bhaData.put("phaseCode", (String)result[1]);
						}	
						else{
							Map<String, Object> breakdownMap_ND = new HashMap<String, Object>(); 
							breakdownMap_ND.put("phaseCode", (String)result[1]);
							breakdownMap_ND.put("taskCode", (String)result[4]);
							breakdownMap_ND.put("duration", duration);	
							breakdownMap_ND.put("duration_text", durationConverter.formatOutputPrecision());
							activityCodes_ND.put(key, breakdownMap_ND);					
							bhaData.put("phaseCode", (String)result[1]);
						}
					}
					durationConverter.setBaseValue(totalDuration);			
					bhaData.put("totalDuration", durationConverter.formatOutputPrecision());			
				}
				
				
				for (Map.Entry<String, Map> entry : bhaMap.entrySet()) {
					Map<String, Object> bhaData = entry.getValue();
					SimpleAttributes attributes = new SimpleAttributes();
					for (Map.Entry<String, Object> entry2 : bhaData.entrySet())  {
						if (entry2.getValue() instanceof String) {
							attributes.addAttribute(entry2.getKey(), nullToEmptyString(entry2.getValue()));
						}
					}
					writer.startElement("Bha", attributes);
					Map<String, Map> activityCodes_D = (Map<String, Map>) bhaData.get("ActivityCodes_D");
					Map<String, Map> activityCodes_ND = (Map<String, Map>) bhaData.get("ActivityCodes_ND");
	
					Double totalDurationDrilling = 0.0;
					Double totalDurationNonDrilling = 0.0;
					
					if (activityCodes_D!=null) {
						for (Map.Entry<String, Map> entry2 : activityCodes_D.entrySet())  {
							Map<String, Object> breakdownMap = entry2.getValue();
							attributes = new SimpleAttributes();
							for (Map.Entry<String, Object> entry3 : breakdownMap.entrySet()) {
								attributes.addAttribute(entry3.getKey(), nullToEmptyString(entry3.getValue()));
								if ("duration".equals(entry3.getKey())) {
									totalDurationDrilling += (Double) entry3.getValue();
								}
							}						
							writer.addElement("Drill", null, attributes);				
						}
					}
					
					if (activityCodes_ND!=null) {
						for (Map.Entry<String, Map> entry2 : activityCodes_ND.entrySet())  {
							Map<String, Object> breakdownMap_ND = entry2.getValue();
							attributes = new SimpleAttributes();
							for (Map.Entry<String, Object> entry3 : breakdownMap_ND.entrySet()) {
								attributes.addAttribute(entry3.getKey(), nullToEmptyString(entry3.getValue()));
								if ("duration".equals(entry3.getKey())) {
									totalDurationNonDrilling += (Double) entry3.getValue();
								}
							}						
							writer.addElement("NDrill", null, attributes);				
						}
					}
					durationConverter.setBaseValue(totalDurationDrilling);
					writer.addElement("TotalDurationDrilling", durationConverter.formatOutputPrecision());
					durationConverter.setBaseValue(totalDurationNonDrilling);
					writer.addElement("TotalDurationNonDrilling", durationConverter.formatOutputPrecision());
					writer.endElement();
				}
				writer.endElement();
			}
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	private Boolean checkTaskCode(String operationCode, String taskCode){
		String[] taskCodeList = null;
		if (operationCode.equals("DRLLG")){
			 taskCodeList  = new String[] {"CDD", "CDE", "COR", "CCW", "DFS", "DM", "DH", "DHO", "DO", "D", "DC", "M", "MC", "MJ" };
		}
		else if (operationCode.equals("DRLLGPDI")){
			taskCodeList  = new String[]{"COR", "D", "DC", "CDD", "CDE", "DFS", "ST", "DM" };
		}
		else if (operationCode.equals("CTUU")){
			taskCodeList  = new String[]{"CDD", "CDE", "COR", "CCW", "DO", "DR", "DA", "DC", "ST", "UBD" };
		}
		
		if (taskCodeList!=null){
			for (int i=0;i < taskCodeList.length; i++){		
				if (taskCode.equals(taskCodeList[i].toString())){
					return true;
				}
			}
		}
		return false;
	}

	public String queryWellDailyCostSummary_forSantos(String operationUid) {
		String xml = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			SimpleAttributes opsAttrs = new SimpleAttributes();
			opsAttrs.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "depthMdMsl", operationUomContext);
			opsAttrs.addAttribute("depth_UomSymbol", depthConverter.getUomSymbol());
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration", operationUomContext);
			opsAttrs.addAttribute("duration_UomSymbol", durationConverter.getUomSymbol());
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			opsAttrs.addAttribute("cost_UomSymbol", costConverter.getUomSymbol());
			
			Double maxDepth = 0.0;
			Double totalDuration = 0.0;
			Double totalAfe = 0.0;
			Double totalAfeAdjusted = 0.0;
			Double totalFfc = 0.0;
			String spudDatetime = null;
			String rigInformationUid = null;
			Map<String, Map> allDailyMap = new LinkedHashMap<String, Map>();
			String queryString = "select d.operationUid, d.dailyUid, d.dayDate, a.phaseCode, min(a.startDatetime), max(a.depthMdMsl), sum(coalesce(a.activityDuration,0.0)) " +
					"FROM Daily d, Activity a " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND d.dailyUid=a.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.dailyUid, d.dayDate, a.phaseCode " +
					"ORDER by d.dayDate, a.dayPlus, a.startDatetime ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String dailyUid = nullToEmptyString(rec[1]);
				Date dayDate = (Date) rec[2];
				String phaseCode = nullToEmptyString(rec[3]);
				Date startDatetime = (Date) rec[4];
				Double depthMdMsl = nullToZero((Double) rec[5]);
				if (maxDepth<depthMdMsl) maxDepth = depthMdMsl;
				Double activityDuration = nullToZero((Double) rec[6]);
				totalDuration += activityDuration;
				if (activityDuration>0.0) {
					String key = "daily_" + dailyUid + "_" + phaseCode;
					Map<String, Object> dailyMap = new HashMap<String, Object>();
					if (allDailyMap.containsKey(key)) {
						dailyMap = allDailyMap.get(key);
					} else {
						dailyMap.put("dayDate", df.format(dayDate) + " " + tf.format(startDatetime));
						if (spudDatetime==null) spudDatetime = df.format(dayDate) + " " + tf.format(startDatetime);
						dailyMap.put("phaseCode", this.getPhaseName(phaseCode, false));
						depthConverter.setBaseValue(depthMdMsl);
						dailyMap.put("depthMdMsl", depthConverter.getFormattedValue());
						durationConverter.setBaseValue(activityDuration);
						dailyMap.put("activityDuration", durationConverter.getFormattedValue());
						dailyMap.put("dailyTotal", 0.0);
						ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
						if (reportDaily!=null && StringUtils.isNotBlank(reportDaily.getRigInformationUid())) rigInformationUid = reportDaily.getRigInformationUid();
						dailyMap.put("dayNumber", reportDaily==null?"-":nullToEmptyString(reportDaily.getReportNumber()));
						allDailyMap.put(key, dailyMap);
					}
				}
			}			
				
			
			//TODO: find exclude phases
			String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			String strCondition = "";
			if (StringUtils.isNotBlank(dvdPlanUid)) {
				OperationPlanMaster dvdPlan = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdPlanUid);
				if (dvdPlan!=null) {
					String costType = dvdPlan.getCostPhaseType();
					if (StringUtils.isNotBlank(costType)) {
						String gwpvalue = "";
			 			if ("CS".equalsIgnoreCase(costType)) {
							gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
						}else if ("PA".equalsIgnoreCase(costType)) {
							gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
						}
			 			
						if (StringUtils.isNotBlank(gwpvalue)) {
							String[] phases = gwpvalue.split(";");
							strCondition = " and d.phaseCode NOT in ('" + StringUtils.join(phases, "','") + "') ";
						}
					}
				}
			}
			
			Map<String, Map> allItemMap = new HashMap<String, Map>();
			queryString = "select m.operationUid, d.accountCode, d.shortDescription, d.itemDescription, SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMaster m, CostAfeDetail d " +
					"WHERE m.costAfeMasterUid=d.costAfeMasterUid " +
					"AND (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.operationUid=:operationUid " + strCondition +
					"GROUP BY m.operationUid, d.accountCode, d.shortDescription, d.itemDescription " +
					"ORDER BY d.accountCode, d.shortDescription, d.itemDescription";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> itemMap = new HashMap<String, Object>();
				Map<String, Map> itemDetailMap = new HashMap<String, Map>();
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				
				String accountCode = nullToEmptyString(rec[1]);
				String shortDescription = nullToEmptyString(rec[2]);
				String itemDescription = nullToEmptyString(rec[3]);
				Double itemTotal = nullToZero((Double) rec[4]);
				totalAfe += itemTotal; 
				String itemKey = accountCode + "::" + shortDescription;
				String itemDetailKey = accountCode + "::" + shortDescription + "::" + itemDescription;
				
				if (allItemMap.containsKey(itemKey)) {
					itemMap = allItemMap.get(itemKey);
					itemDetailMap = (Map<String, Map>) itemMap.get("itemDetails");
				} else {
					itemMap.put("accountCode", accountCode);
					itemMap.put("shortDescription", shortDescription);
					itemMap.put("itemDetails", itemDetailMap);
					allItemMap.put(itemKey, itemMap);
				}
				if (itemMap.containsKey("afeTotal")) { 
					itemMap.put("afeTotal", itemTotal + (Double)itemMap.get("afeTotal"));
				} else {
					itemMap.put("afeTotal", itemTotal);
				}
				
				if (itemDetailMap.containsKey(itemDetailKey)) {
					itemDetail = itemDetailMap.get(itemDetailKey);
				} else {
					itemDetail.put("accountCode", accountCode);
					itemDetail.put("shortDescription", shortDescription);
					itemDetail.put("itemDescription", itemDescription);
					itemDetailMap.put(itemDetailKey, itemDetail);
				}
				if (itemDetail.containsKey("afeTotal")) { 
					itemDetail.put("afeTotal", itemTotal + (Double)itemDetail.get("afeTotal"));
				} else {
					itemDetail.put("afeTotal", itemTotal);
				}
			}
			
			queryString = "select m.operationUid, d.accountCode, d.shortDescription, d.itemDescription, SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
					"FROM CostAfeMasterAdjusted m, CostAfeDetailAdjusted d " +
					"WHERE m.costAfeMasterAdjustedUid=d.costAfeMasterAdjustedUid " +
					"AND (m.isDeleted=false or m.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND m.operationUid=:operationUid " + strCondition +
					"GROUP BY m.operationUid, d.accountCode, d.shortDescription, d.itemDescription " +
					"ORDER BY d.accountCode, d.shortDescription, d.itemDescription";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> itemMap = new HashMap<String, Object>();
				Map<String, Map> itemDetailMap = new HashMap<String, Map>();
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				
				String accountCode = nullToEmptyString(rec[1]);
				String shortDescription = nullToEmptyString(rec[2]);
				String itemDescription = nullToEmptyString(rec[3]);
				Double itemTotal = nullToZero((Double) rec[4]);
				totalAfeAdjusted += itemTotal;
				String itemKey = accountCode + "::" + shortDescription;
				String itemDetailKey = accountCode + "::" + shortDescription + "::" + itemDescription;
				
				if (allItemMap.containsKey(itemKey)) {
					itemMap = allItemMap.get(itemKey);
					itemDetailMap = (Map<String, Map>) itemMap.get("itemDetails");
				} else {
					itemMap.put("accountCode", accountCode);
					itemMap.put("shortDescription", shortDescription);
					itemMap.put("itemDetails", itemDetailMap);
					allItemMap.put(itemKey, itemMap);
				}
				if (itemMap.containsKey("afeAdjustedTotal")) { 
					itemMap.put("afeAdjustedTotal", itemTotal + (Double)itemMap.get("afeAdjustedTotal"));
				} else {
					itemMap.put("afeAdjustedTotal", itemTotal);
				}
				
				
				if (itemDetailMap.containsKey(itemDetailKey)) {
					itemDetail = itemDetailMap.get(itemDetailKey);
				} else {
					itemDetail.put("accountCode", accountCode);
					itemDetail.put("shortDescription", shortDescription);
					itemDetail.put("itemDescription", itemDescription);
					itemDetailMap.put(itemDetailKey, itemDetail);
				}
				if (itemDetail.containsKey("afeAdjustedTotal")) { 
					itemDetail.put("afeAdjustedTotal", itemTotal + (Double)itemDetail.get("afeAdjustedTotal"));
				} else {
					itemDetail.put("afeAdjustedTotal", itemTotal);
				}
			}
			
			queryString = "select d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription, c.dailyUid, c.phaseCode, SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) " +
					"FROM CostDailysheet c, Daily d " +
					"WHERE (c.isDeleted=false or c.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND c.dailyUid=d.dailyUid " +
					"AND d.operationUid=:operationUid " +
					"GROUP BY d.operationUid, c.accountCode, c.afeShortDescription, c.afeItemDescription, c.dailyUid, c.phaseCode ";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				Map<String, Object> itemMap = new HashMap<String, Object>();
				Map<String, Map> itemDetailMap = new HashMap<String, Map>();
				Map<String, Object> itemDetail = new HashMap<String, Object>();
				
				String accountCode = nullToEmptyString(rec[1]);
				String shortDescription = nullToEmptyString(rec[2]);
				String itemDescription = nullToEmptyString(rec[3]);
				String dailyUid = nullToEmptyString(rec[4]);
				String phaseCode = nullToEmptyString(rec[5]);
				Double itemTotal = nullToZero((Double) rec[6]);
				totalFfc += itemTotal;
				String itemKey = accountCode + "::" + shortDescription;
				String itemDetailKey = accountCode + "::" + shortDescription + "::" + itemDescription;
				String itemDailyKey = "daily_" + dailyUid + "_" + phaseCode;
				
				if (allItemMap.containsKey(itemKey)) {
					itemMap = allItemMap.get(itemKey);
					itemDetailMap = (Map<String, Map>) itemMap.get("itemDetails");
				} else {
					itemMap.put("accountCode", accountCode);
					itemMap.put("shortDescription", shortDescription);
					itemMap.put("itemDetails", itemDetailMap);
					allItemMap.put(itemKey, itemMap);
				}
				if (itemMap.containsKey("ffcTotal")) { 
					itemMap.put("ffcTotal", itemTotal + (Double)itemMap.get("ffcTotal"));
				} else {
					itemMap.put("ffcTotal", itemTotal);
				}
				
				if (itemDetailMap.containsKey(itemDetailKey)) {
					itemDetail = itemDetailMap.get(itemDetailKey);
				} else {
					itemDetail.put("accountCode", accountCode);
					itemDetail.put("shortDescription", shortDescription);
					itemDetail.put("itemDescription", itemDescription);
					itemDetailMap.put(itemDetailKey, itemDetail);
				}
				if (itemDetail.containsKey("ffcTotal")) { 
					itemDetail.put("ffcTotal", itemTotal + (Double)itemDetail.get("ffcTotal"));
				} else {
					itemDetail.put("ffcTotal", itemTotal);
				}
				
				if (itemDetail.containsKey(itemDailyKey)) {
					itemDetail.put(itemDailyKey, itemTotal + (Double)itemDetail.get(itemDailyKey));
				} else {
					itemDetail.put(itemDailyKey, itemTotal);
				}
				
				if (allDailyMap.containsKey(itemDailyKey)) {
					Double previousTotal = (Double) allDailyMap.get(itemDailyKey).get("dailyTotal");
					allDailyMap.get(itemDailyKey).put("dailyTotal", previousTotal + itemTotal);
				}
			}
			
			for (Map.Entry<String, Map> entry : allDailyMap.entrySet()) {
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("key", entry.getKey());
				for(Map.Entry<String, Object> item : ((Map<String,Object>)entry.getValue()).entrySet()) {
					if ("dailyTotal".equals(item.getKey()))  {
						costConverter.setBaseValue(nullToZero((Double)item.getValue()));
						attr.addAttribute(item.getKey(), costConverter.formatOutputPrecision());
					} else {
						attr.addAttribute(item.getKey(), nullToEmptyString(item.getValue()));
					}
				}
				writer.addElement("Daily", null, attr);
			}
		
			//output
			List mapList = new LinkedList(allItemMap.entrySet());
			Collections.sort(mapList, new Comparator() {
				public int compare(Object o1, Object o2) {
					String object1 = ((Map.Entry<String, Object>) (o1)).getKey();
					String object2 = ((Map.Entry<String, Object>) (o2)).getKey();
					if (object1==null || object1==null || object2==null || object2==null) return 0;
					return (object1).compareTo(object2);
				}
			});
		    for (Iterator it = mapList.iterator(); it.hasNext();) {
		    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
		    	Map<String, Object> itemMap = allItemMap.get(entry.getKey());
		    	writer.startElement("CostItem");
		    	
		    	Map<String, Double> costItemTotal = new HashMap<String, Double>();
		    	
		    	for (Map.Entry<String, Object> itemEntry : itemMap.entrySet()) {
		    		if (itemEntry.getValue() instanceof Double) {
		    			costConverter.setBaseValue((Double) itemEntry.getValue());
		    			writer.addElement(itemEntry.getKey(), costConverter.formatOutputPrecision());
		    		} else if  (itemEntry.getValue() instanceof Map) {
		    			Map<String, Map> itemDetailMap = (Map<String, Map>) itemEntry.getValue();
		    			for (Map.Entry<String, Map> itemDetailEntry : itemDetailMap.entrySet()) {
		    				writer.startElement("ItemDetail");
		    				Map<String, Object> itemDetail = (Map<String, Object>) itemDetailEntry.getValue();
		    				for (Map.Entry<String, Object> detailEntry : itemDetail.entrySet()) {
		    		    		if (detailEntry.getValue() instanceof Double) {
		    		    			if (detailEntry.getKey().indexOf("daily_")==0) {
			    		    			if (costItemTotal.containsKey(detailEntry.getKey())) {
			    		    				Double thisItemTotal = (Double) costItemTotal.get(detailEntry.getKey());
			    		    				thisItemTotal += (Double) detailEntry.getValue();
			    		    				costItemTotal.put(detailEntry.getKey(), thisItemTotal);
			    		    			} else {
			    		    				costItemTotal.put(detailEntry.getKey(), (Double) detailEntry.getValue());
			    		    			}
		    		    			}
		    		    			costConverter.setBaseValue((Double) detailEntry.getValue());
		    		    			writer.addElement(detailEntry.getKey(), costConverter.formatOutputPrecision());
		    		    		} else {
		    		    			writer.addElement(detailEntry.getKey(), nullToEmptyString(detailEntry.getValue()));
		    		    		}
		    				}
		    				writer.endElement();
		    			}
		    		} else {
		    			writer.addElement(itemEntry.getKey(), nullToEmptyString(itemEntry.getValue()));	
		    		}
		    	}
		    	for (Map.Entry<String, Double> itemEntry : costItemTotal.entrySet()) {
	    			costConverter.setBaseValue((Double) itemEntry.getValue());
	    			writer.addElement(itemEntry.getKey(), costConverter.formatOutputPrecision());
		    	}
		    	writer.endElement();
		    }
			
		    durationConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
		    durationConverter.setBaseValue(totalDuration);
		    opsAttrs.addAttribute("spudToRigRelease", durationConverter.getFormattedValue());
		    depthConverter.setBaseValue(maxDepth);
		    opsAttrs.addAttribute("depthMdMsl", depthConverter.getFormattedValue());
		    opsAttrs.addAttribute("spudDatetime", spudDatetime);
		    
		    costConverter.setBaseValue(totalAfe);
		    opsAttrs.addAttribute("afeTotal", costConverter.formatOutputPrecision());
		    costConverter.setBaseValue(totalAfeAdjusted);
		    opsAttrs.addAttribute("afeAdjustedTotal", costConverter.formatOutputPrecision());
		    costConverter.setBaseValue(totalFfc);
		    opsAttrs.addAttribute("ffcTotal", costConverter.formatOutputPrecision());
		    
		    if (rigInformationUid!=null) {
				List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM RigInformation WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid", "rigInformationUid", rigInformationUid, qp);
				if (rigList.size()>0) {
					opsAttrs.addAttribute("rigName", nullToEmptyString(rigList.get(0).getRigName()));
				}
		    }
		    
		    writer.addElement("Operation", null, opsAttrs);
		    
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
		
	}
	
	public String queryHseSummary_forSantos(String[] operationUids, String selectedDateFrom, String selectedDateTo, Boolean hseSummaryBreakdown, Boolean hseOperationBreakdown, Boolean hseMonthly) {
		String xml = "<root/>";
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
					
			writer.startElement("datarow");
			Map<String, Map> hseMap = new LinkedHashMap<String, Map>();
			String groupUid= this.getCurrentUserSession().getCurrentGroupUid();
			
			String[] incidentCategories = this.getHseEventname();
			DecimalFormat df = new DecimalFormat("#.##");
		    DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		    String filterConditionByDate = "";
		    
		    //filter by date
		    if (!"".equals(selectedDateFrom)) filterConditionByDate += " AND d.dayDate >= '"+selectedDateFrom+" 00:00:00' ";
		    if (!"".equals(selectedDateTo)) filterConditionByDate += " AND d.dayDate <= '"+selectedDateTo+" 23:59:59' ";
			
		    //filter by operation
		    String filterConditionByWell = "";
		    if (operationUids !=null && operationUids.length>0){
		    	filterConditionByWell = " AND o.operationUid IN ('" + StringUtils.join(operationUids, "','") + "') " ;
		    }	   
		   
		    //HSE summary detail
		    if (hseSummaryBreakdown){
		    	
		    	Map<String, Double> totalIncidentCategory = new LinkedHashMap<String, Double>();
				for (String strIncidentCategory : incidentCategories) {
					totalIncidentCategory.put(strIncidentCategory, 0.0);
				}
		    	
		    	String queryString = "select r.rigName, rd.rigInformationUid, h.incidentCategory, sum(coalesce(h.numberOfIncidents,0.0)) " +
		    			" FROM HseIncident h, Daily d, ReportDaily rd, RigInformation r, Well w, Wellbore wb, Operation o " +
						"WHERE d.groupUid = :groupUid " +
						"AND (h.isDeleted = false OR h.isDeleted IS NULL) " +
						"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
						"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
						"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
						"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
						"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
						"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
						"AND w.wellUid=wb.wellUid " +
						"AND wb.wellboreUid=o.wellboreUid " +
						"AND o.operationUid=d.operationUid " +
						"AND d.dailyUid=rd.dailyUid " +
						"AND rd.reportType!='DGR' " +
						"AND h.dailyUid = d.dailyUid " +
						"AND date(h.hseEventdatetime) = date(d.dayDate) " +
						"AND h.incidentCategory in ('"+StringUtils.join(incidentCategories, "','")+"')" +
						filterConditionByDate +
						filterConditionByWell + 
						"AND r.rigInformationUid = rd.rigInformationUid " +
						"GROUP BY r.rigName, rd.rigInformationUid, h.incidentCategory " +
						"ORDER BY r.rigName";
				
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,"groupUid", groupUid, qp);
				for (Object[] rec : list) {					
					String rigName = nullToEmptyString(rec[0]);
					String rigInformationUid = nullToEmptyString(rec[1]);
					String incidentCategory = nullToEmptyString(rec[2]);
					Double numberOfIncidents = rec[3]==null?0.0:(Long)rec[3] * 1.0;
					
					Map<String, Object> hseData = new LinkedHashMap<String, Object>();
					Map<String, Object> hseEvent = new LinkedHashMap<String, Object>();
	
					// get HSE map
					if (hseMap.containsKey(rigInformationUid)) {
						hseData = hseMap.get(rigInformationUid);
					} else {
						hseData.put("rigInformationUid", rigInformationUid);
						hseData.put("rigName", rigName);				
						hseMap.put(rigInformationUid, hseData);
					}
					
					// get HSE event map
					if (hseData.containsKey("Event")) {
						hseEvent = (Map<String, Object>) hseData.get("Event");
					} else {
						for (String strIncidentCategory : incidentCategories) {
							hseEvent.put(strIncidentCategory, 0.0);
						}
						hseData.put("Event", hseEvent);
					}
					
					if (hseEvent.containsKey(incidentCategory)) {
						hseEvent.put(incidentCategory, numberOfIncidents);
					}
					
					if (totalIncidentCategory.containsKey(incidentCategory)) {
						Double totalIncident = (Double) totalIncidentCategory.get(incidentCategory);
						totalIncident += numberOfIncidents;
						totalIncidentCategory.put(incidentCategory, totalIncident);
					}
				}
				
				queryString = "select r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate), sum(coalesce(p.pax,0.0)) " +
							"FROM PersonnelOnSite p, Daily d, ReportDaily rd, RigInformation r, Well w, Wellbore wb, Operation o " +
							"WHERE d.groupUid = :groupUid " +
							"AND (p.isDeleted = false OR p.isDeleted IS NULL) " +
							"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
							"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
							"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
							"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
							"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
							"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
							"AND w.wellUid=wb.wellUid " +
							"AND wb.wellboreUid=o.wellboreUid " +
							"AND o.operationUid=d.operationUid " +
							"AND d.dailyUid=rd.dailyUid " +
							"AND rd.reportType!='DGR' " +
							"AND p.dailyUid = d.dailyUid " +
							filterConditionByDate +
							filterConditionByWell + 
							"AND r.rigInformationUid = rd.rigInformationUid " +
							"GROUP BY r.rigName, rd.rigInformationUid";
						
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				Map<String, Double> manHoursMap = new HashMap<String, Double>();
				
				Double totalManHours = 0.0;
				for (Object[] rec : list) {	
					String rigInformationUid = nullToEmptyString(rec[1]);
					Double pax = (rec[4]==null?0.0:(Long)rec[4] * 1.0);
					if(hseMap.containsKey(rigInformationUid)){
						totalManHours += pax * 12;
					}
					manHoursMap.put(rigInformationUid, pax * 12);
				}
				manHoursMap.put("totalManHours", totalManHours);
				totalIncidentCategory.put("Man Hours", manHoursMap.get("totalManHours"));
				
				//output to XML
				for (Map.Entry<String, Map> entry : hseMap.entrySet()) {
					Map<String, Object> hseData = entry.getValue();
					SimpleAttributes attributes = new SimpleAttributes();
					for (Map.Entry<String, Object> entry2 : hseData.entrySet())  {
						if (entry2.getValue() instanceof String) {
							attributes.addAttribute(entry2.getKey(), nullToEmptyString(entry2.getValue()));
						}
					}
					writer.startElement("Hse", attributes);
					Map<String, Object> hseEvent = (Map<String, Object>) hseData.get("Event");
					
					Double firstAid = (Double) hseEvent.get("First Aid");
					Double medicalTreatmentIncident = (Double) hseEvent.get("Medical Treatment Incident");
					Double lostTimeIncident = (Double) hseEvent.get("Lost Time Incident");
					Double hazard = (Double) hseEvent.get("Hazard");
					Double nearMiss = (Double) hseEvent.get("Near Miss");
					if ((firstAid + medicalTreatmentIncident + lostTimeIncident) > 0){
						Double ratio = (hazard + nearMiss)/(firstAid + medicalTreatmentIncident + lostTimeIncident);
						hseEvent.put("Ratio", ratio);
					}
					
					for (Map.Entry<String, Object> entry2 : hseEvent.entrySet())  {
						attributes = new SimpleAttributes();
						attributes.addAttribute("incidentCategory", entry2.getKey());
						attributes.addAttribute("totalNumberOfIncidents", df.format((Double)entry2.getValue()));
						if(entry2.getKey().equalsIgnoreCase("Man Hours")){
							attributes.addAttribute("incidentCategory", entry2.getKey());
							attributes.addAttribute("totalNumberOfIncidents", df.format(nullToZero((Double) manHoursMap.get(hseData.get("rigInformationUid")))));
						}
						writer.addElement("Event", null, attributes);				
					}
					writer.endElement();
				}
	
				//HSE summary footer
				writer.startElement("Total");
				for (Map.Entry<String, Double> entry : totalIncidentCategory.entrySet()){
					String incidentCategory = entry.getKey();
					Double totalIncident = entry.getValue();
					SimpleAttributes attributes = new SimpleAttributes();
					attributes.addAttribute("incidentCategory", incidentCategory);
					if ("Hazard".equals(incidentCategory)){
						incidentCategory = "Hazards"; 
					} else if ("First Aid".equals(incidentCategory)){
						incidentCategory = "FA";
					} else if ("Medical Treatment Incident".equals(incidentCategory)){
						incidentCategory = "MTI";
					} else if ("Lost Time Incident".equals(incidentCategory)){
						incidentCategory = "LTI";
					} else if ("Ratio".equals(incidentCategory)){
						Double firstAid = (Double) totalIncidentCategory.get("First Aid");
						Double medicalTreatmentIncident = (Double) totalIncidentCategory.get("Medical Treatment Incident");
						Double lostTimeIncident = (Double) totalIncidentCategory.get("Lost Time Incident");
						Double hazard = (Double) totalIncidentCategory.get("Hazard");
						Double nearMiss = (Double) totalIncidentCategory.get("Near Miss");
						if ((firstAid + medicalTreatmentIncident + lostTimeIncident) > 0){
							totalIncident = (hazard + nearMiss)/(firstAid + medicalTreatmentIncident + lostTimeIncident);
						}
					} else if ("Environmental Issue".equals(incidentCategory)){
						incidentCategory = "Env. Hazards";
					} else if ("Environmental Incident".equals(incidentCategory)){
						incidentCategory = "Env. Incident";
					} else if ("Safety Intervention".equals(incidentCategory)){
						incidentCategory = "Safety Inter.";
					}
					
					attributes.addAttribute("tableHeader", incidentCategory);		
					attributes.addAttribute("totalNumberOfIncidents", df.format(totalIncident));
					writer.addElement("Event", null, attributes);
				}
				writer.endElement();
		    }
			
			//HSE summary breakdown by operation
			if (hseOperationBreakdown){
				writer.startElement("Detail");
				
				String previousIncident = "";
				String[] eventList = this.getHseEventnameList();
				String queryString = "FROM Well w, Wellbore wb, Operation o, Daily d, ReportDaily rd, HseIncident h, RigInformation r " +
					"WHERE d.groupUid = :groupUid " +
					"AND (h.isDeleted = false OR h.isDeleted IS NULL) " +
					"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
					"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
					"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
					"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
					"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
					"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid = d.operationUid " +
					"and d.dailyUid=rd.dailyUid " +
					"AND d.dailyUid=h.dailyUid " +
					"AND rd.rigInformationUid=r.rigInformationUid " +
					"AND rd.reportType!='DGR' " +
					"AND h.numberOfIncidents>=0 " +
					"AND date(h.hseEventdatetime) = date(d.dayDate) " +
					"AND h.incidentCategory in ('"+StringUtils.join(eventList, "','")+"') " +
					filterConditionByDate + 
					filterConditionByWell + 
					"ORDER BY h.incidentCategory, w.wellName, wb.wellboreName, o.operationName, d.dayDate";
				
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				for (Object[] rec : list) {	
					Daily d = (Daily) rec[3];
					ReportDaily rd = (ReportDaily) rec[4];
					HseIncident hse = (HseIncident) rec[5];
					RigInformation rig = (RigInformation) rec[6];
					String incidentCategory = nullToEmptyString(hse.getIncidentCategory());
					if (StringUtils.isBlank(incidentCategory)) continue;
					
					if (!previousIncident.equals(incidentCategory)) {
						if (!"".equals(previousIncident)) writer.endElement();
						SimpleAttributes attributes = new SimpleAttributes();
						attributes.addAttribute("incidentCategory", incidentCategory);	
						writer.startElement("Category",attributes);
						previousIncident = incidentCategory;
					}
						
					SimpleAttributes attributes = new SimpleAttributes();
					attributes.addAttribute("rigName", rig.getRigName());
					attributes.addAttribute("hseShortdescription", nullToEmptyString(hse.getHseShortdescription()));
					attributes.addAttribute("operationName", nullToEmptyString(CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, d.getOperationUid())));
					attributes.addAttribute("dayNumber", nullToEmptyString(rd.getReportNumber()));				
					if (d.getDayDate()!=null) attributes.addAttribute("dayDate", formatter.format(d.getDayDate()));
					attributes.addAttribute("imsNumber", nullToEmptyString(hse.getImsNumber()));
					attributes.addAttribute("bodyPart", this.toProperCase(nullToEmptyString(hse.getBodyPart())));
					attributes.addAttribute("numberOfIncidents", nullToEmptyString(hse.getNumberOfIncidents()));
					if (hse.getHseEventdatetime()!=null) attributes.addAttribute("hseEventdatetime", formatter.format(hse.getHseEventdatetime()));
					attributes.addAttribute("description", nullToEmptyString(hse.getDescription()));		
					attributes.addAttribute("dailyUid", nullToEmptyString(hse.getDailyUid()));
					attributes.addAttribute("operationUid", nullToEmptyString(hse.getOperationUid()));
					writer.addElement("Rig", null, attributes);
				}
				if (!"".equals(previousIncident)) {
					writer.endElement();
				}
				writer.endElement();
			}
			
			//HSE summary by month
			if (hseMonthly){
				incidentCategories = this.getMonthlyHseEventName();
				writer.startElement("Monthly");
				
				Map<String, Map> HseMonthly = new LinkedHashMap<String, Map>();
				Map<String, String> allRigs = new LinkedHashMap<String, String>();
				String queryString = "select r.rigName, r.rigInformationUid FROM RigInformation r " +
						"WHERE r.groupUid = :groupUid " +
						"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
						"AND r.rigInformationUid in (select rd.rigInformationUid from ReportDaily rd WHERE (rd.isDeleted=false or rd.isDeleted is null)) " +
						"GROUP BY r.rigName, r.rigInformationUid " +
						"ORDER BY r.rigName";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				for (Object[] rec : list) {
					String rigName = nullToEmptyString(rec[0]);
					String rigInformationUid = nullToEmptyString(rec[1]);
					if (StringUtils.isNotBlank(rigName) && StringUtils.isNotBlank(rigInformationUid)) {
						allRigs.put(rigInformationUid, rigName);
					}
				}
				
		    	queryString = "select r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate) " +
		    			"FROM Daily d, ReportDaily rd, RigInformation r, Well w, Wellbore wb, Operation o " +
						"WHERE d.groupUid = :groupUid " +
						"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
						"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
						"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
						"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
						"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
						"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
						"AND w.wellUid=wb.wellUid " +
						"AND wb.wellboreUid=o.wellboreUid " +
						"AND o.operationUid=d.operationUid " +
						"AND d.dailyUid=rd.dailyUid " +
						"AND r.rigInformationUid = rd.rigInformationUid " +
						"AND rd.reportType!='DGR' " +
						filterConditionByDate +
						filterConditionByWell + 
						"GROUP BY r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate) " +
						"ORDER BY year(d.dayDate), month(d.dayDate), r.rigName";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				for (Object[] rec : list) {
					String rigName = nullToEmptyString(rec[0]);
					String rigInformationUid = nullToEmptyString(rec[1]);
					String yearMonth = getMonth((Integer)rec[2], (Integer)rec[3]);
					if (StringUtils.isBlank(yearMonth)) continue;
					
					Map<String, Map> hseMonthMap = new LinkedHashMap<String, Map>();
					Map<String, Object> hseRigMap = new LinkedHashMap<String, Object>();
					Map<String, Double> hseEventMap = new LinkedHashMap<String, Double>();
					Map<String, Double> hseTotalMap = new LinkedHashMap<String, Double>();
					
					if (HseMonthly.containsKey(yearMonth)) {
						hseMonthMap = HseMonthly.get(yearMonth);
					} else {
						HseMonthly.put(yearMonth, hseMonthMap);
						
						//create dummy rig records
						for (Map.Entry<String, String> entry : allRigs.entrySet()) {
							hseRigMap = new LinkedHashMap<String, Object>();
							hseRigMap.put("rigName", entry.getValue());
							hseRigMap.put("rigInformationUid", entry.getKey());
							hseMonthMap.put(entry.getKey(), hseRigMap);
							
							hseEventMap = new LinkedHashMap<String, Double>();
							for (String strCategory : incidentCategories) {
								hseEventMap.put(strCategory, 0.0);
							}
							hseRigMap.put("Event", hseEventMap);
						}
					}
					
					if (hseMonthMap.containsKey(rigInformationUid)) {
						hseRigMap = hseMonthMap.get(rigInformationUid);
					} else {
						hseRigMap.put("rigName", rigName);
						hseRigMap.put("rigInformationUid", rigInformationUid);
						hseMonthMap.put(rigInformationUid, hseRigMap);
					}
					
					if (hseRigMap.containsKey("Event")) {
						hseEventMap = (Map<String, Double>) hseRigMap.get("Event");
					} else {
						for (String strCategory : incidentCategories) {
							hseEventMap.put(strCategory, 0.0);
						}
						hseRigMap.put("Event", hseEventMap);
					}
					
					if (hseMonthMap.containsKey("Total")) {
						hseTotalMap = hseMonthMap.get("Total");
					} else {
						for (String strCategory : incidentCategories) {
							hseTotalMap.put(strCategory, 0.0);
						}
						hseMonthMap.put("Total", hseTotalMap);
					}
				}		    	
				
				
				
		    	queryString = "select r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate), sum(coalesce(p.pax,0.0)) " +
		    			"FROM PersonnelOnSite p, Daily d, ReportDaily rd, RigInformation r, Well w, Wellbore wb, Operation o " +
						"WHERE d.groupUid = :groupUid " +
						"AND (p.isDeleted = false OR p.isDeleted IS NULL) " +
						"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
						"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
						"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
						"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
						"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
						"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
						"AND w.wellUid=wb.wellUid " +
						"AND wb.wellboreUid=o.wellboreUid " +
						"AND o.operationUid=d.operationUid " +
						"AND d.dailyUid=rd.dailyUid " +
						"AND rd.reportType!='DGR' " +
						"AND p.dailyUid = d.dailyUid " +
						filterConditionByDate +
						filterConditionByWell + 
						"AND r.rigInformationUid = rd.rigInformationUid " +
						"GROUP BY r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate) " +
						"ORDER BY year(d.dayDate), month(d.dayDate), r.rigName";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				for (Object[] rec : list) {
					String rigName = nullToEmptyString(rec[0]);
					String rigInformationUid = nullToEmptyString(rec[1]);
					String yearMonth = getMonth((Integer)rec[2], (Integer)rec[3]);
					Double pax = rec[4]==null?0.0:(Long)rec[4] * 1.0;
					
					if (StringUtils.isBlank(yearMonth)) continue;
					
					Map<String, Map> hseMonthMap = new LinkedHashMap<String, Map>();
					Map<String, Object> hseRigMap = new LinkedHashMap<String, Object>();
					Map<String, Double> hseEventMap = new LinkedHashMap<String, Double>();
					Map<String, Double> hseTotalMap = new LinkedHashMap<String, Double>();
					
					if (HseMonthly.containsKey(yearMonth)) {
						hseMonthMap = HseMonthly.get(yearMonth);
					} else {
						HseMonthly.put(yearMonth, hseMonthMap);
					}
					
					if (hseMonthMap.containsKey(rigInformationUid)) {
						hseRigMap = hseMonthMap.get(rigInformationUid);
					} else {
						hseRigMap.put("rigName", rigName);
						hseRigMap.put("rigInformationUid", rigInformationUid);
						hseMonthMap.put(rigInformationUid, hseRigMap);
					}
					
					if (hseRigMap.containsKey("Event")) {
						hseEventMap = (Map<String, Double>) hseRigMap.get("Event");
					} else {
						for (String strCategory : incidentCategories) {
							hseEventMap.put(strCategory, 0.0);
						}
						hseRigMap.put("Event", hseEventMap);
					}
					if(hseEventMap.containsKey("Man Hours"))
						hseEventMap.put("Man Hours", pax * 12.0);
					
					if (hseMonthMap.containsKey("Total")) {
						hseTotalMap = hseMonthMap.get("Total");
					} else {
						for (String strCategory : incidentCategories) {
							hseTotalMap.put(strCategory, 0.0);
						}
						hseMonthMap.put("Total", hseTotalMap);
					}
					if (hseTotalMap.containsKey("Man Hours")) {
						Double total = nullToZero((Double) hseTotalMap.get("Man Hours"));
						total += (pax * 12.0);
						hseTotalMap.put("Man Hours", total);
					}
				}		    	
				
		    	queryString = "select r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate), h.incidentCategory, sum(coalesce(h.numberOfIncidents,0.0)) " +
		    			"FROM HseIncident h, Daily d, ReportDaily rd, RigInformation r, Well w, Wellbore wb, Operation o " +
						"WHERE d.groupUid = :groupUid " +
						"AND (h.isDeleted = false OR h.isDeleted IS NULL) " +
						"AND (r.isDeleted = false OR r.isDeleted IS NULL) " +
						"AND (rd.isDeleted = false OR rd.isDeleted IS NULL) " +
						"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
						"AND (o.isDeleted = false OR o.isDeleted IS NULL) " +
						"AND (wb.isDeleted = false OR wb.isDeleted IS NULL) " +
						"AND (w.isDeleted = false OR w.isDeleted IS NULL) " +
						"AND w.wellUid=wb.wellUid " +
						"AND wb.wellboreUid=o.wellboreUid " +
						"AND o.operationUid=d.operationUid " +
						"AND d.dailyUid=rd.dailyUid " +
						"AND rd.reportType!='DGR' " +
						"AND h.dailyUid = d.dailyUid " +
						"AND date(h.hseEventdatetime) = date(d.dayDate) " +
						"AND h.incidentCategory not in ('Man Hours')" +
						"AND h.incidentCategory in ('"+StringUtils.join(incidentCategories, "','")+"') " +
						filterConditionByDate +
						filterConditionByWell + 
						"AND r.rigInformationUid = rd.rigInformationUid " +
						"GROUP BY r.rigName, rd.rigInformationUid, year(d.dayDate), month(d.dayDate), h.incidentCategory " +
						"ORDER BY year(d.dayDate), month(d.dayDate), r.rigName";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "groupUid", groupUid, qp);
				for (Object[] rec : list) {
					String rigName = nullToEmptyString(rec[0]);
					String rigInformationUid = nullToEmptyString(rec[1]);
					String yearMonth = getMonth((Integer)rec[2], (Integer)rec[3]);
					String incidentCategory = nullToEmptyString(rec[4]);
					Double numberOfIncidents = rec[5]==null?0.0:(Long)rec[5] * 1.0;
					
					if (StringUtils.isBlank(yearMonth)) continue;
					if (StringUtils.isBlank(incidentCategory)) continue;
					
					Map<String, Map> hseMonthMap = new LinkedHashMap<String, Map>();
					Map<String, Object> hseRigMap = new LinkedHashMap<String, Object>();
					Map<String, Double> hseEventMap = new LinkedHashMap<String, Double>();
					Map<String, Double> hseTotalMap = new LinkedHashMap<String, Double>();
					
					if (HseMonthly.containsKey(yearMonth)) {
						hseMonthMap = HseMonthly.get(yearMonth);
					} else {
						HseMonthly.put(yearMonth, hseMonthMap);
					}
					
					if (hseMonthMap.containsKey(rigInformationUid)) {
						hseRigMap = hseMonthMap.get(rigInformationUid);
					} else {
						hseRigMap.put("rigName", rigName);
						hseRigMap.put("rigInformationUid", rigInformationUid);
						hseMonthMap.put(rigInformationUid, hseRigMap);
					}
					
					if (hseRigMap.containsKey("Event")) {
						hseEventMap = (Map<String, Double>) hseRigMap.get("Event");
					} else {
						for (String strCategory : incidentCategories) {
							hseEventMap.put(strCategory, 0.0);
						}
						hseRigMap.put("Event", hseEventMap);
					}
					hseEventMap.put(incidentCategory, numberOfIncidents);
					
					if (hseMonthMap.containsKey("Total")) {
						hseTotalMap = hseMonthMap.get("Total");
					} else {
						for (String strCategory : incidentCategories) {
							hseTotalMap.put(strCategory, 0.0);
						}
						hseMonthMap.put("Total", hseTotalMap);
					}
					if (hseTotalMap.containsKey(incidentCategory)) {
						Double total = nullToZero((Double) hseTotalMap.get(incidentCategory));
						total += numberOfIncidents;
						hseTotalMap.put(incidentCategory, total);
					}
				}
					
				for (Map.Entry<String, Map> entry : HseMonthly.entrySet()) {
					SimpleAttributes attributes = new SimpleAttributes();
					attributes.addAttribute("month", entry.getKey());	
					writer.startElement("Hse",attributes);
					
					Map<String, Map> hseMonthMap = entry.getValue();
					for (Map.Entry<String, Map> entry2 : hseMonthMap.entrySet()) {
						
						if ("Total".equals(entry2.getKey())) {
							Map<String, Double> hseTotalMap = (Map<String, Double>) entry2.getValue();
							writer.startElement("Total");
							for (Map.Entry<String, Double> entry3 : hseTotalMap.entrySet()) {
								attributes = new SimpleAttributes();
								attributes.addAttribute("incidentCategory", entry3.getKey());
								String incidentCategory = entry3.getKey();
								Double totalNumberOfIncidents = entry3.getValue();
								if ("Hazard".equals(incidentCategory)){
									incidentCategory = "Hazards"; 
								} else if ("First Aid".equals(incidentCategory)){
									incidentCategory = "FA";
								} else if ("Medical Treatment Incident".equals(incidentCategory)){
									incidentCategory = "MTI";
								} else if ("Lost Time Incident".equals(incidentCategory)){
									incidentCategory = "LTI";
								} else if ("Environmental Issue".equals(incidentCategory)){
									incidentCategory = "Env. Hazards";
								} else if ("Environmental Incident".equals(incidentCategory)){
									incidentCategory = "Env. Incident";
								} else if ("Safety Intervention".equals(incidentCategory)){
									incidentCategory = "Safety Inter.";
								} else if ("Ratio".equals(incidentCategory)) {
									Double firstAid = hseTotalMap.get("First Aid");
									Double medicalTreatmentIncident = hseTotalMap.get("Medical Treatment Incident");
									Double lostTimeIncident = hseTotalMap.get("Lost Time Incident");
									Double hazard = hseTotalMap.get("Hazard");
									Double nearMiss = hseTotalMap.get("Near Miss");
									if ((firstAid + medicalTreatmentIncident + lostTimeIncident) > 0){
										totalNumberOfIncidents = (hazard + nearMiss)/(firstAid + medicalTreatmentIncident + lostTimeIncident);
									}	
								}
								
								attributes.addAttribute("tableHeader", incidentCategory);
								attributes.addAttribute("totalNumberOfIncidents", df.format(totalNumberOfIncidents));
								writer.addElement("Event", "", attributes);
							}
							writer.endElement();
						} else {
							Map<String, Object> hseRigMap = (Map<String, Object>) entry2.getValue();
							Map<String, Double> hseEventMap = new LinkedHashMap<String, Double>();
							attributes = new SimpleAttributes();
							for (Map.Entry<String, Object> entry3 : hseRigMap.entrySet()) {
								if (entry3.getValue() instanceof String) {
									attributes.addAttribute(entry3.getKey(), nullToEmptyString(entry3.getValue()));
								} else if (entry3.getValue() instanceof Map) {
									hseEventMap = (Map<String, Double>) entry3.getValue();
								}
							}
							writer.startElement("Rig",attributes);
							for (Map.Entry<String, Double> entry4 : hseEventMap.entrySet()) {
								attributes = new SimpleAttributes();
								Double totalNumberOfIncidents = entry4.getValue();
								
								if ("Ratio".equals(entry4.getKey())) {
									Double firstAid = hseEventMap.get("First Aid");
									Double medicalTreatmentIncident = hseEventMap.get("Medical Treatment Incident");
									Double lostTimeIncident = hseEventMap.get("Lost Time Incident");
									Double hazard = hseEventMap.get("Hazard");
									Double nearMiss = hseEventMap.get("Near Miss");
									if ((firstAid + medicalTreatmentIncident + lostTimeIncident) > 0){
										totalNumberOfIncidents = (hazard + nearMiss)/(firstAid + medicalTreatmentIncident + lostTimeIncident);
									}	
								}
								
								attributes.addAttribute("incidentCategory", entry4.getKey());
								attributes.addAttribute("totalNumberOfIncidents", df.format(totalNumberOfIncidents));
								writer.addElement("Event", "", attributes);
							}
							writer.endElement();
						}
					}
					
					writer.endElement();
				}
			}
			
			writer.endElement();
	
			writer.close();
			xml = bytes.toString("utf-8");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	private String[] getMonthlyHseEventName(){
		String[] eventNameList = {"First Aid", "Medical Treatment Incident", "Lost Time Incident", "Hazard", "Near Miss", "Safety Intervention", "Non Work Related", "Well Control", "Environmental Issue", "Environmental Incident", "Environment Hazards", "Non Conformance", "Environmental Near Miss", "EHS Property Damage"};
		return eventNameList;
	}
	
	private String[] getHseEventname(){
		String[] eventNameList = {"Hazard", "Near Miss", "First Aid", "Medical Treatment Incident", "Lost Time Incident", "Ratio", "Environmental Issue", "Environmental Incident", "Man Hours", "Safety Intervention", "Well Control" };
		return eventNameList;
	}
	
	private String[] getHseEventnameList(){
		String[] eventNameList = {"Hazard", "Near Miss", "First Aid", "Medical Treatment Incident", "Lost Time Incident", "Environmental Issue", "Environmental Incident" };
		return eventNameList;
	}

	private String getMonth(Integer year, Integer month) throws ParseException{
		String[] strMonth = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		return ( month>strMonth.length ? "" :strMonth[month-1] + " " + year );
	}
	
	public String queryHseSummaryByDateWell_forSantos(String[] operationUids, String selectedDateFrom, String selectedDateTo, String[] hseEvents) {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			
			String conditionFilters = "";
			if (StringUtils.isNotBlank(selectedDateFrom)) conditionFilters += " and h.hseEventdatetime >= '"+selectedDateFrom+" 00:00:00' "; 
			if (StringUtils.isNotBlank(selectedDateTo)) conditionFilters += " and h.hseEventdatetime <= '"+selectedDateTo+" 23:59:59' "; 
			if (operationUids.length>0) conditionFilters += " AND d.operationUid IN ('" + StringUtils.join(operationUids, "','") + "') " ;
			if (hseEvents.length>0) conditionFilters += " AND h.incidentCategory IN ('" + StringUtils.join(hseEvents,"','") + "') ";
			
			Map<String, Map> hseMap = new LinkedHashMap<String, Map>();
			Map<String, ReportDaily> reportDailyMap = new HashMap<String, ReportDaily>();
			Map<String, String> rigMap = new HashMap<String, String>();
			String queryString = "FROM RigInformation WHERE (isDeleted=false or isDeleted is null)";
			List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
			for (RigInformation rig:rigList) {
				rigMap.put(rig.getRigInformationUid(), rig.getRigName());
			}
			
			queryString = "SELECT h, d FROM HseIncident h, Daily d, Operation o, Wellbore wb, Well w " +
					"WHERE (h.isDeleted=false or h.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.dailyUid=h.dailyUid) " +
					"AND d.operationUid=o.operationUid " +
					"AND o.wellboreUid=wb.wellboreUid " +
					"AND wb.wellUid=w.wellUid " +
					"AND date(h.hseEventdatetime) = date(d.dayDate) " +
					conditionFilters +
					"ORDER BY h.incidentCategory, w.wellName, wb.wellboreName, o.operationName, h.hseEventdatetime DESC";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
			for (Object[] rec : list) {
				HseIncident hse = (HseIncident) rec[0];
				Daily daily = (Daily) rec[1];
				if (StringUtils.isBlank(hse.getIncidentCategory())) continue;
				String rigName = "";
				String reportNumber = "";
				ReportDaily reportDaily = null;
				if (reportDailyMap.containsKey(daily.getDailyUid())) {
					reportDaily = reportDailyMap.get(daily.getDailyUid());
				} else {
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(daily.getOperationUid());
					queryString = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and reportType=:reportType";
					List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid","reportType"}, new Object[]{daily.getDailyUid(), reportType});
					if (reportDailyList.size()>0) {
						reportDaily = reportDailyList.get(0);
					} else {
						reportDaily = new ReportDaily();
					}
					if (reportDaily.getRigInformationUid()==null) {
						Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
						if (operation!=null) {
							reportDaily.setRigInformationUid(operation.getRigInformationUid());
						}
					}
					reportDailyMap.put(daily.getDailyUid(), reportDaily);
				}
				if (reportDaily!=null) {
					if (rigMap.containsKey(reportDaily.getRigInformationUid())) {
						rigName = rigMap.get(reportDaily.getRigInformationUid());
					}
					reportNumber = reportDaily.getReportNumber();
				}
				
				Map<String, Map> hseCategory = new LinkedHashMap<String, Map>(); 
				if (hseMap.containsKey(hse.getIncidentCategory())) {
					hseCategory = hseMap.get(hse.getIncidentCategory());
				} else {
					hseMap.put(hse.getIncidentCategory(), hseCategory);
				}
				
				Map<String, Object> hseData = new HashMap<String, Object>();
				hseData.put("wellName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), hse.getOperationUid()));
				hseData.put("rigName", rigName);
				hseData.put("reportNumber", reportNumber);
				hseData.put("dayDate", df.format(daily.getDayDate()));
				hseData.put("imsNumber", hse.getImsNumber());
				hseData.put("bodyPart", this.toProperCase(hse.getBodyPart()));
				hseData.put("eventNumber", hse.getNumberOfIncidents());
				hseData.put("eventDate", df.format(hse.getHseEventdatetime()));
				hseData.put("eventDesc", hse.getHseShortdescription());
				hseData.put("detail", hse.getDescription());
				hseCategory.put(hse.getHseIncidentUid(), hseData);
			}
			
			for (Map.Entry<String, Map> category : hseMap.entrySet()) {
				Integer totalEvents = 0;
				String incidentCategory = category.getKey();
				Map<String, Map> hseCategory = category.getValue();
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("incidentCategory", incidentCategory);
				writer.startElement("IncidentCategory", "", attr);
				for (Map.Entry<String, Map> detail : hseCategory.entrySet()) {
					attr = new SimpleAttributes();
					Map<String, Object> fields = detail.getValue();
					for (Map.Entry<String, Object> field : fields.entrySet()) {
						if ("eventNumber".equals(field.getKey())) totalEvents += (Integer) field.getValue();
						attr.addAttribute(field.getKey(), nullToEmptyString(field.getValue()).trim());
					}
					writer.addElement("HseIncident", "", attr);
				}
				writer.addElement("TotalIncidents", nullToEmptyString(totalEvents));
				writer.endElement();
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return xml;
	}
	
	public String queryDatHseSummary_forSantos(String[] operationUids, String[] hseEvents) {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");

			String queryString = "FROM RigInformation WHERE (isDeleted=false or isDeleted is null) order by rigName";
			List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
			for (RigInformation rig:rigList) {
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("name", rig.getRigName());
				attr.addAttribute("uid", rig.getRigInformationUid());
				writer.addElement("Rig", "", attr);
			}
			
			Map<String, Map> hseMap = new HashMap<String, Map>();
			queryString = "select h.incidentCategory, rd.rigInformationUid, sum(h.numberOfIncidents) " +
					"FROM HseIncident h, Daily d, ReportDaily rd " +
					"WHERE (h.isDeleted=false or h.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (rd.isDeleted=false or rd.isDeleted is null) " +
					"AND h.dailyUid=d.dailyUid " +
					"AND rd.dailyUid=d.dailyUid " +
					"AND h.numberOfIncidents is not null " +
					"AND rd.reportType!='DGR' " +
					"AND date(h.hseEventdatetime) = date(d.dayDate) " +
					"AND d.operationUid in ('"+ StringUtils.join(operationUids, "','")+ "') " +
					"AND h.incidentCategory in ('"+StringUtils.join(hseEvents, "','")+"') " +
					"GROUP BY h.incidentCategory, rd.rigInformationUid ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
			for (Object[] rec : list) {
				String incidentCategory = (String) rec[0];
				String rigInformationUid = (String) rec[1];
				Long totalEvents = (rec[2]==null?0:(Long)rec[2]);
				
				Map<String, Long> rigEventMap = new HashMap<String, Long>();
				if (hseMap.containsKey(incidentCategory)) {
					rigEventMap = hseMap.get(incidentCategory);
				} else {
					hseMap.put(incidentCategory, rigEventMap);
				}
				rigEventMap.put(rigInformationUid, totalEvents);
			}
			
			for (Map.Entry<String, Map> entry : hseMap.entrySet()) {
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("incidentCategory", entry.getKey());
				Map<String, Long> rigEventMap = entry.getValue();
				Long totalEvents = new Long(0);
				for (Map.Entry<String, Long> result : rigEventMap.entrySet()) {
					attr.addAttribute(result.getKey(), nullToEmptyString(result.getValue()));
					if (result.getValue()!=null) totalEvents += result.getValue();
				}
				attr.addAttribute("total", nullToEmptyString(totalEvents));
				writer.addElement("HseIncident", "", attr);
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	private String toProperCase(String s) {
		if (s==null) return "";
		String[] parts = s.split(" ");
		String camelCaseString = "";
		for (String part : parts) {
			if (StringUtils.isNotBlank(part)) {
				camelCaseString += (StringUtils.isNotBlank(camelCaseString)?" ":"") + part.substring(0,1).toUpperCase() + part.substring(1).toLowerCase();
			}
		}
		return camelCaseString;
	}
}
