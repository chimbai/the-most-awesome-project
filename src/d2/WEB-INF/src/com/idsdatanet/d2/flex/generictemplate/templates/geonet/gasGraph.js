(function(window){
	D3.inherits(GasGraphImg,D3.AbstractFieldComponent);
	function GasGraphImg(){
		D3.AbstractFieldComponent.call(this);
	}
	
	GasGraphImg.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var div = document.createElement("div");
			div.setAttribute("class", "center");
			var img = document.createElement("img");
			img.setAttribute("src", 'graph.html?type=gas_graph&size=medium');
			div.appendChild(img);
			this._fieldRenderer = div;
		}
		return this._fieldRenderer;
	}

	GasGraphImg.prototype.refreshFieldRenderer = function() {
	}
	
	D3.inherits(GasGraphNodeListener,D3.EmptyNodeListener);
	
	function GasGraphNodeListener() {}
	
	
	GasGraphNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "gas_graph_img"){
			return new GasGraphImg();
		} else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = GasGraphNodeListener;
	
})(window);