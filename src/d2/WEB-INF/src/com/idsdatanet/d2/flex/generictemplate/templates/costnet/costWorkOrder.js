(function(window){
	D3.inherits(CostItemsSelectionPopup,D3.AbstractFieldComponent);
	function CostItemsSelectionPopup(node) {
		D3.AbstractFieldComponent.call(this);
		this._node = node;
		this._columns = [
		    		     {
		    		    	 type: "recordAction",
		    		    	 width:"10px"
		    		     },   
		    		     {
		    		    	 type:"field",
		    		    	 field:"workOrderNumber",
		    		    	 width:"125px",
		    		    	 align:"center",
		    		         title:D3.LanguageManager.getLabel("pop.item.workorder")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"dayDate",
		    		    	 width:"103px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.dayDate")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"ticketNumber",
		    		    	 width:"103px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.ticket")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"itemDescription",
		    		    	 width:"103px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.description")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"accountCode",
		    		    	 width:"150px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.costcategory")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"itemTotal",
		    		    	 width:"103px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.itemTotal")
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"lineTotal",
		    		    	 width:"103px",
		    		    	 align:"center",
		    		    	 title:D3.LanguageManager.getLabel("pop.item.lineTotal")
		    		     }
		    		    ];
		this.cachedSelectedOperationList = [];
		this.cachedSelectedWorkOrder = [];
		this.showAllCostItems = true;
		this.showMsg(D3.LanguageManager.getLabel("label.pleasewait"), D3.LanguageManager.getLabel("label.loading"), false);
		this.showPopup();
	}
	
	CostItemsSelectionPopup.prototype.showPopup = function(){
		if (!this._container){
			this.initiateContainer();
		}
		this.hidePopup();
		$(this._container).dialog({title:D3.LanguageManager.getLabel("pop.item.title"),dialogClass: 'no-close' ,
			closeonEscape:false,modal:true, width:"1024", height:"500", resizable:false,closable:false});
	}
	CostItemsSelectionPopup.prototype.showMsg = function(title, msg, showCloseButton) {
		if(this.$popup && this.$popup.dialog("isOpen")){
			this.$popup.dialog("destroy");
		}
		if(! this.popupContentDiv){
			this.popupContentDiv = document.createElement("div");
			this.popupContentDiv.style.padding = "15px 10px";	
		}
		while(this.popupContentDiv.hasChildNodes()) this.popupContentDiv.removeChild(this.popupContentDiv.lastChild);
		this.popupContentDiv.appendChild(document.createTextNode(msg));
		this.$popup = $(this.popupContentDiv).dialog({title: title, minHeight: 10, resizable: false, closeOnEscape: false, modal:true,
			 open: function(event, ui) { 
		        	$(".ui-dialog-titlebar-close", ui.dialog).hide();
		        }
		});
		this.popupStartTime = new Date();
	};
	CostItemsSelectionPopup.prototype.hidePopup = function()
	{
		this.$popup.dialog("destroy");
	}
	CostItemsSelectionPopup.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container' style='overflow-y: hidden;'>" +
					"<div class='StandardButtonContainer'>" +
					    "<button class='RootButton SelectAll'>"+D3.LanguageManager.getLabel("button.selectAll")+"</button>" +
						"<button class='RootButton UnselectAll'>"+D3.LanguageManager.getLabel("button.unselectAll")+"</button>" +
			//			"<button class='RootButton hidecost'>"+D3.LanguageManager.getLabel("pop.item.hideCost")+"</button>" +
			//			"<button class='RootButton showCost'>"+D3.LanguageManager.getLabel("pop.item.showCost")+"</button>" +
						"<button class='RootButton Confirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
						"<button class='RootButton Close'>"+D3.LanguageManager.getLabel("button.close")+"</button>" +
					"</div><div style='overflow-x: hidden;overflow-y: visible;height:420px'>" +
	 				"<table class='SimpleGridTable reportListing'><thead class='costItemsHeader'><tr>"+this.getColumnHeader()+"</tr></thead><tbody class='costItemsList-TBODY'></tbody></table>"+
				"</div></div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			
			this._btnSelectAll = container.find(".SelectAll").get(0);
			this._btnSelectAll.onclick = this.btnSelectAll_onClick.bindAsEventListener(this);
			this._btnUnselectAll = container.find(".UnselectAll").get(0);
			this._btnUnselectAll.onclick = this.btnUnselectAll_onClick.bindAsEventListener(this);
			
//			this._btnHidecost = container.find(".hidecost").get(0);
//			this._btnHidecost.onclick = this.btnHidecost_onClick.bindAsEventListener(this);
//			this._btnShowCost = container.find(".showCost").get(0);
//			this._btnShowCost.onclick = this.btnShowCost_onClick.bindAsEventListener(this);
			
			this._btnConfirm = container.find(".Confirm").get(0);
			this._btnConfirm.onclick = this.btnConfirm_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
			
			this._header = container.find(".workOrderNumber").get(0);
			this._tbody = container.find(".costItemsList-TBODY").get(0);
			
			this.loadData();
			var xx = "";
//			$(this._btnHidecost).addClass("hide");
//			$(this._header).addClass("hide");
		}
	}
	CostItemsSelectionPopup.prototype.btnOpsSearch_onClick = function(){
		this.loadOperations();
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.btnWOSearch_onClick = function(){
		this.loadWorkOrders();
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.btnHidecost_onClick = function(){
		$(this._btnHidecost).addClass("hide");
		$(this._btnShowCost).removeClass("hide");
		$(this._header).addClass("hide");
		for (var key in this._tbody.children) {
			var item = this._tbody.children[key];
			$(item).addClass("hide");
		}
	}
	CostItemsSelectionPopup.prototype.btnShowCost_onClick = function(){
		$(this._btnShowCost).addClass("hide");
		$(this._btnHidecost).removeClass("hide");
		$(this._header).removeClass("hide");
		for (var key in this._tbody.children) {
			var item = this._tbody.children[key];
			$(item).removeClass("hide");
		}
	}
	CostItemsSelectionPopup.prototype.btnConfirm_onClick = function(){
		var hasDataSelected = false;
		for (var key in this._CostItems) {
			if (this._CostItems[key].selected){
				hasDataSelected = true;
			}
		}
		if (hasDataSelected) this.confirmSelected();
		else {
			D3.UIManager.popup("Select Cost Items", "Please select one or more cost items.");
		}
		$(this._container).dialog("close");
	}
	CostItemsSelectionPopup.prototype.btnClose_onClick = function(){
		$(this._container).dialog("close");
	}
	CostItemsSelectionPopup.prototype.btnSelectAll_onClick = function(){
		for (var key in this._CostItems) {
			if (!this._CostItems[key].selected){
				var item = this._CostItems[key];
				this.checkSelection(item,true);
				var uiComponents = item.field["_recordAction"]._uiComponents;
				uiComponents["check"].get(0).checked = true;
			}
		}
	}
	CostItemsSelectionPopup.prototype.btnUnselectAll_onClick = function(){
		for (var key in this._CostItems) {
			if (this._CostItems[key].selected){
				var item = this._CostItems[key];
				this.checkSelection(item,false);
				var uiComponents = item.field["_recordAction"]._uiComponents;
				uiComponents["check"].get(0).checked = false;
			}
		}
	}
	CostItemsSelectionPopup.prototype.btnWoCheckbox_onClick = function(e, data){
		if (this._woCheckboxes[data].checked){
			this.cachedSelectedWorkOrder.push(data);
		}else{
			var index = this.cachedSelectedWorkOrder.indexOf(data);
			if (index >-1) this.cachedSelectedWorkOrder.splice(index,1);
		}
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.loadData = function(){
		var params = {};
		params._invokeCustomFilter = "getAvailableCostItems";
		params.costWorkOrderUid = this._node.primaryKeyValue;
		params.lookupCompanyUid = this._node.getFieldValue("lookupCompanyUid");
		params.showAllCostItems = this.showAllCostItems;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var doc = $(response);
		var self = this;
		var collection = [];
		doc.find("CostDailysheet").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.costDailysheetUid = data.attr("costDailysheetUid");
			newItem.dayDate = data.attr("dayDate");
			newItem.dayDate__raw = data.attr("dayDate__raw");
			newItem.ticketNumber = data.attr("ticketNumber");
			newItem.accountCode = data.attr("accountCode");
			newItem.itemDescription = data.attr("itemDescription");
			newItem.chargeScope = data.attr("chargeScope");
			newItem.itemTotal = data.attr("itemTotal");
			newItem.lineTotal = data.attr("lineTotal");
			newItem.invoiceNumber = data.attr("invoiceNumber");
			newItem.workOrderNumber = data.attr("workOrderNumber");
			newItem.afeNumber = data.attr("afeNumber");

			var selected = false;
			var childGroup = self._node.children;
			if (childGroup!=null){
				if (childGroup.hasOwnProperty("CostWorkOrderDetail")) {
					for (var key in childGroup.CostWorkOrderDetail._items)
					{
						var childNode = childGroup.CostWorkOrderDetail._items[key];
						var costDailysheetUid = childNode.getFieldValue("costDailysheetUid"); 
						if (newItem.costDailysheetUid==costDailysheetUid) {
							selected = true;
							break;
						}
					};
				}
			}
			
			var record = {};
			record.data = newItem;
			record.uid = newItem.costDailysheetUid;
			record.selected = selected;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		D3.UI.removeAllChildNodes(this._tbody);
		collection.sort(function(a, b) {
		    return parseFloat(a.data.dayDate__raw) - parseFloat(b.data.dayDate__raw);
		});
		this._CostItems = collection;
		this.updateCostItemsList();
	}
	CostItemsSelectionPopup.prototype.updateCostItemsList = function(){
		for (var i=0;i<this._CostItems.length;i++){
			var row = this._CostItems[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	CostItemsSelectionPopup.prototype.generateDataColumn = function(elemTR,record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'/>");
			if (record.selected) checkbox.get(0).checked = true;
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.checkSelectionEvent.bindAsEventListener(this,record);
			$(columnContainer).append(checkbox);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox};
		}
	}
	CostItemsSelectionPopup.prototype.checkSelectionEvent = function(event,data){
		this.checkSelection(data, null);
	}
	CostItemsSelectionPopup.prototype.confirmSelected = function(){
		var childGroup = this._node.children;
		if (childGroup!=null){
			if (childGroup.hasOwnProperty("CostWorkOrderDetail")) {
				for (var key in childGroup.CostWorkOrderDetail._items)
				{
					var childNode = childGroup.CostWorkOrderDetail._items[key];
					childNode.removeNode();
				}
			}
		}
		for (var key in this._CostItems) {
			if (this._CostItems[key].selected){
				var children = this._node.addNewChildNodeOfClass("CostWorkOrderDetail");
				var item = this._CostItems[key];
				if (children._items.length > 0){
					var newChild = children._items[0];
					newChild.setFieldValue("costDailysheetUid", item.data["costDailysheetUid"]);
					newChild.setFieldValue("@dayDate", item.data["dayDate"]);
					newChild.setFieldValue("@ticketNumber", item.data["ticketNumber"]);
					newChild.setFieldValue("@accountCode", item.data["accountCode"]);
					newChild.setFieldValue("@itemDescription", item.data["itemDescription"]);
					newChild.setFieldValue("@chargeScope", item.data["chargeScope"]);
					newChild.setFieldValue("@cost", item.data["itemTotal"]);
					newChild.setFieldValue("@itemTotal", item.data["lineTotal"]);
					newChild.fireRefreshNodeEvent();
				}
			}
		}
	}
	CostItemsSelectionPopup.prototype.checkSelection = function(data,checked){
		if (checked!=null) data.selected = checked;
		else data.selected = !data.selected;
	}
	CostItemsSelectionPopup.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th class='SimpleGridHeaderCell "+(col.field?(col.field):"") + "' style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}

	CostItemsSelectionPopup.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	CostItemsSelectionPopup.prototype.getRecord = function(uid){
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	CostItemsSelectionPopup.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		var span = document.createElement("span");
		span.className = "fieldRenderer " +fieldName;
		$(span).text(value);
		return span;
	}
	
	D3.inherits(CostWorkOrderNodeListener,D3.EmptyNodeListener);
	
	function CostWorkOrderNodeListener() {
	}
	
	CostWorkOrderNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		if (id == "select_cost_items") {
			if (!parentNode.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "CostWorkOrderDetail") && !parentNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_EDIT)) {
				return null;
			}
			if (parentNode.atts.isNewRecord) {
				var btn = null;
				btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>")
				return btn.get(0);
			}
		}
		return null;
	}

	CostWorkOrderNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if(id == "select_cost_items"){
			return new CostItemsSelectionPopup(node);
		}
	}
	
	D3.CommandBeanProxy.nodeListener = CostWorkOrderNodeListener;
	
})(window);