(function(window){
	
	
	
	D3.inherits(OperationInformationCenterListener,D3.EmptyNodeListener);
	
	function OperationInformationCenterListener()
	{
		this.btnSnapshot = null;
	}
	
	OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH = D3.LanguageManager.getLabel("button.showGraph");
	OperationInformationCenterListener.SNAPSHOT_SHOW_DETAILS = D3.LanguageManager.getLabel("button.showDetails");
	
	OperationInformationCenterListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "snapshot") {
			return new OICSnapshot(this.btnSnapshot);
		} 
	}
	OperationInformationCenterListener.prototype.getCustomButtonForHeaderColumn = function(node,id){
		if (id == "showGraphOrDetails"){
			this.btnSnapshot = D3.UI.createElement("button",OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH,"DefaultButton",{label:OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH},{onclick:this.btnSnapshot_onClick});
			retutn this.btnSnapshot;
		}
		return null;
	}
	OperationInformationCenterListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		
		
	}
	OperationInformationCenterListener.prototype.btnSnapshot_onClick(event){
		var button = event.target;
		button.dispatchEvent(new CustomEvent(snapshotButtonClicked,button.label))
	}
	D3.CommandBeanProxy.nodeListener = OperationInformationCenterListener;
	
	
	
})(window);