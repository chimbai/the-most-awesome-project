(function(window){
		
	D3.inherits(wellControlStackNodeListener,D3.EmptyNodeListener);
	
	function wellControlStackNodeListener(){
		
	}

	wellControlStackNodeListener.prototype.getConfirmationMessage = function(commandBeanProxy) {		
		var msg = null;
		
		var deletePrompt = false;
		
		var wellControlStack = commandBeanProxy.rootNode.children["WellControlStack"].getItems();
		var existingOperationList = wellControlStack[0].getFieldValue("@affectedOperationList");
		D3.forEach(wellControlStack,function(node1){
			if (node1.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){ 
				deletePrompt = true; 
			}

			var wellControl = node1.children["WellControl"].getItems();
			D3.forEach(wellControl,function(node2){
				if (node2.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
					deletePrompt = true;
				}
				
				var WellControlComponent = node2.children["WellControlComponent"].getItems();
				D3.forEach(WellControlComponent,function(node3){
					if (node3.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
						deletePrompt = true;
					}
					
					var WellControlComponentTest = node3.children["WellControlComponentTest"].getItems();
					D3.forEach(WellControlComponentTest,function(node4){
						if (node4.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
							deletePrompt = true;
						}
					},this);
				},this);
			},this);
		},this);
		
		if (deletePrompt){
			msg = this.warningMessage(existingOperationList);
			
		}
		
		return msg;
	}
	
	wellControlStackNodeListener.prototype.checkDeleteAction = function(node, simpleClass, deletePrompt){
		var className = node.children[simpleClass].getItems();
		D3.forEach(className,function(node){
			if (node.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
				deletePrompt = true;
			}
		},this);
		return deletePrompt;
	}
	
	wellControlStackNodeListener.prototype.warningMessage = function(existingOperationList){
		msg = "The selected BOP may be associated to other operation(s) of this well. Deleting this BOP record will also remove the BOP record tied to all other operation(s) of this well. Are you sure you want to delete this BOP record? \n\n";
		msg += "Operations: \n\n";
		msg += existingOperationList;
		return msg;
	}
	
	
	D3.CommandBeanProxy.nodeListener = wellControlStackNodeListener;
	
})(window);