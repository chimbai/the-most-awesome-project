(function(window){

	D3.inherits(LessonTicketCategoryButton,D3.AbstractFieldComponent);
	
	function LessonTicketCategoryButton() {
		D3.AbstractFieldComponent.call(this);
		this._columns = [
		     			{
		     				type:"field",
		     				field:"lessonCategory",
		     				align:"left",
		     				width:"160px",
		     				title:D3.LanguageManager.getLabel("LT.header.Categories")
		     			},{
		     				type:"field",
		     				field:"lessonElements",
		     				align:"left",
		     				title:D3.LanguageManager.getLabel("LT.header.Elements")
		     			}
		     		]
	}
	
	LessonTicketCategoryButton.prototype.getFieldEditor = function() {
		var label = D3.LanguageManager.getLabel("LT.label");
		if (!this._editor) {
			this._editor = document.createElement("button");
			this._editor.setAttribute("type", "button");
			this._editor.className = "DefaultButton";
			this._editor.appendChild(document.createTextNode(label));
			this._editor.onclick = this.showPopup.bindAsEventListener(this);
		}
		
		return this._editor;
	}
	LessonTicketCategoryButton.prototype.showPopup = function(event){
		if (!this._container){
			this.initiateContainer();
		}
		this.loadLessonTicketCategory();
		var self = this;
		var btnAdd = {
				text:D3.LanguageManager.getLabel("LT.add"),
				"class":"popup_button",
				click:function(){
					self.btnAdd_onClick();
				}
		};
		var btnClose = {
				text:D3.LanguageManager.getLabel("button.close"),
				"class":"popup_button",
				click:function(){
	                $(this).dialog('close');
	                self._node.commandBeanProxy.reloadParentHtmlPage(true);
				}
		};
		
		$(this._container).dialog({ 
			title:D3.LanguageManager.getLabel("LT.title"),
			dialogClass: 'no-close' ,
			closeonEscape:false,modal:true, width:"725", height:"460", resizable:false,
			closable:false,buttons: [btnAdd,btnClose],});
	}
	LessonTicketCategoryButton.prototype.btnAdd_onClick = function(){
		if (!this._popUpContainer){
			this.initiatePopUpContainer();
		}
		this.retrieveSectionList();
		
		var self = this;
		var btnAdd = {
				text:D3.LanguageManager.getLabel("button.confirm"),
				"class":"popup_button",
				click:function(){
					self.btnConfirm_onClick();
				}
		};
		var btnUnSel = {
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				"class":"popup_button",
				click:function(){
					self.btnUsel_onClick();
				}
		};
		var btnClose = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				"class":"popup_button",
				click:function(){
	                $(this).dialog('close');
				}
		};
		
		$(this._popUpContainer).dialog({ 
			title:D3.LanguageManager.getLabel("LT.selElements"),
			dialogClass: 'no-close' ,
			closeonEscape:false,modal:true, width:"900", height:"550", resizable:false,
			closable:false,buttons: [btnAdd,btnClose,btnUnSel],});
	}
	LessonTicketCategoryButton.prototype.btnConfirm_onClick = function(){
		this.loading(true);
		var selectedStr = "";
		for (var key in this._checkboxes) {
			if (selectedStr=="") selectedStr += key + "=";
			else selectedStr += "&" + key + "=";
			var selectedVal = "";
			for (var keys in this._checkboxes[key].values) {
				if (this._checkboxes[key].values[keys].checked == true){
					selectedVal += (selectedVal==""?keys:","+keys);
				}
			}
			selectedStr += selectedVal;
		}
		this.selectedElementsString = selectedStr;
		this.retrieveLessonList();
	}
	LessonTicketCategoryButton.prototype.retrieveLessonList = function(){
		var params = {};
		
		params._invokeCustomFilter = "flexLessonDetailSave";
		params.selectedStr = this.selectedElementsString;
		params.lessonTicketUid = this._lessonTicketUid;
		this._node.commandBeanProxy.customInvoke(params,this.saveLessonCategoryElementLoaded,this);
	}
	LessonTicketCategoryButton.prototype.saveLessonCategoryElementLoaded = function(response){
		this.loading(false);
		$(this._popUpContainer).dialog('close');
		this.loadLessonTicketCategory();
	}
	LessonTicketCategoryButton.prototype.btnUsel_onClick = function(){
		for (var key in this._checkboxes) {
			for (var keys in this._checkboxes[key].values) {
				this._checkboxes[key].values[keys].checked = false;
			}
		}
	}
	LessonTicketCategoryButton.prototype.loading = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Loading...</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	LessonTicketCategoryButton.prototype.initiatePopUpContainer = function(){
		if (!this._popUpContainer){
			var containerPopUpTemplate = "<div class='listAllReport-container'>" +
	 				"<table width='100%' class='SimpleGridTable reportListing'><tbody class='lists-TBODY'></tbody></table>"+
				"</div>";
			var containerPopUp = $(containerPopUpTemplate);
			this._popUpContainer = containerPopUp.get(0);

			this._popUptbody = containerPopUp.find(".lists-TBODY").get(0);	
		}
	}
	LessonTicketCategoryButton.prototype.retrieveSectionList = function(){
		var params = {};
		
		params._invokeCustomFilter = "lessonTicketCategoryElementList";
		params.lessonTicketUid = this._lessonTicketUid;
		this._node.commandBeanProxy.customInvoke(params,this.populateSelectionList,this);
	}
	LessonTicketCategoryButton.prototype.populateSelectionList = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("category").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.uid = data.attr("uid");
			newItem.name = data.attr("name");
			newItem.values = [];
			var length = $(item).find('element').length;
			for (i=0; i<length; i++) {
				var its = $(item).find('element').get(i);
				var newValue = {};
				newValue.uid = $(its).attr('uid');
				newValue.name = $(its).attr('name');
				if($(its).attr('selected') == "1") {
					newValue.selected = $(its).attr('selected');
				}
				newItem.values.push(newValue);
			};
			var record = {};
			record.data = newItem;

			collection.push(record);
		});
		this._selectionListItems = collection;
		D3.UI.removeAllChildNodes(this._popUptbody);

		this.updateSelectionList();
	}
	LessonTicketCategoryButton.prototype.updateSelectionList = function(){
		this._checkboxes = {};
		for (var i=0;i<this._selectionListItems.length;i++){
			var row = this._selectionListItems[i];
			var tr = document.createElement("tr");
			tr.className="SimpleGridDataRow"; 
			this._popUptbody.appendChild(tr);
			var td = document.createElement("td");
			td.setAttribute("width", "200px");
			td.className="SimpleGridDataCell colorSet";
			var span = document.createElement("span");
			span.className = "fieldRenderer";
			$(span).text(row.data.name);
			td.appendChild(span);
			var td2 = document.createElement("td");
			td2.className="SimpleGridDataCell";
			var ul = document.createElement("ul");
			ul.style.margin = 0;
			ul.style.padding = 0;
			this._checkboxes[row.data.uid] = {};
			this._checkboxes[row.data.uid].values = {};
			for (var j=0;j<row.data.values.length;j++){
				var rows = row.data.values[j];
		
				var li = document.createElement("li");
				li.setAttribute("title", rows.name);
				var cb = document.createElement("input");
				cb.setAttribute("id", rows.uid);
				cb.setAttribute("cid", row.data.uid);
				cb.setAttribute("type", "checkbox");
				cb.value = rows.uid;
				if (rows.selected=="1"){
					cb.checked = true;
				}else{
					cb.checked = false;
				}
			//	cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[row.data.uid].values[rows.uid] = cb;
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", rows.uid);
				label.appendChild(document.createTextNode(rows.name));
				li.appendChild(label);
				ul.appendChild(li);
			}
			td2.appendChild(ul);
			tr.appendChild(td);
			tr.appendChild(td2);
		}
	}
	LessonTicketCategoryButton.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container'>" +
	 				"<table class='SimpleGridTable reportListing'><thead><tr class='SimpleGridHeaderRow'>"+this.getColumnHeader()+"</tr></thead>" +
	 				"<tbody class='list-TBODY'></tbody></table>"+
				"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);

			this._tbody = container.find(".list-TBODY").get(0);
			this._lessonTicketUid = this._node.getFieldValue("lessonTicketUid");		
		}
	}
	LessonTicketCategoryButton.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<td class='SimpleGridHeaderCell' style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</td>";
			config+=thConfig;
		}
		return config;
	}
	LessonTicketCategoryButton.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{

		}
	}
	
	LessonTicketCategoryButton.prototype.loadLessonTicketCategory = function(){
		var params = {};
		
		params._invokeCustomFilter = "getLessonTicketCategoryElementsList";
		params.lessonTicketUid = this._lessonTicketUid;
		this._node.commandBeanProxy.customInvoke(params,this.lessonTicketCategoryListLoaded,this);
	}
	LessonTicketCategoryButton.prototype.lessonTicketCategoryListLoaded = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("LessonCategoryElements").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.lessonCategory = data.attr("lessonCategory");
			newItem.lessonElements = data.attr("lessonElements");

			var record = {};
			record.data = newItem;
			record.uid = newItem.lessonCategory;
			record.selected = 0;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		this._serviceLogItems = collection;
		D3.UI.removeAllChildNodes(this._tbody);

		this.updateServiceLogList();
		this.updateRootContainer();
	}
	LessonTicketCategoryButton.prototype.updateServiceLogList = function(){
		for (var i=0;i<this._serviceLogItems.length;i++){
			var row = this._serviceLogItems[i];
			var tr = document.createElement("tr");
			tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	LessonTicketCategoryButton.prototype.updateRootContainer = function(){
		var hasAnythingToSubmit = false;
		for (var i=0;i<this._serviceLogItems.length;i++){
			var record = this._serviceLogItems[i];
			if (record.action)
				hasAnythingToSubmit = true;
		}
		if (hasAnythingToSubmit){
			$(this._standardButtonContainer).addClass("hide");
			$(this._confirmCancelButtonContainer).removeClass("hide");
		}else{
			$(this._standardButtonContainer).removeClass("hide");
			$(this._confirmCancelButtonContainer).addClass("hide");
		}
	}
	LessonTicketCategoryButton.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		var div = document.createElement("div");
		div.className = "fieldRenderer textAreaInputField fitToContent";
		$(div).text(value);
		return div;
	}
	LessonTicketCategoryButton.prototype.refreshFieldRenderer = function (){}
	LessonTicketCategoryButton.prototype.refreshFieldEditor = function() {}
	
	D3.inherits(SendLessonTicketButton,D3.AbstractFieldComponent);
	function SendLessonTicketButton() {
		D3.AbstractFieldComponent.call(this);
	}
	SendLessonTicketButton.prototype.getFieldRenderer = function() {
		var label = D3.LanguageManager.getLabel("LT.sendEmail");
		if (!this._fieldRenderer) {
			this._fieldRenderer = document.createElement("button");
			this._fieldRenderer.setAttribute("type", "button");
			this._fieldRenderer.setAttribute("title", label);
			this._fieldRenderer.className = "DefaultButton";
			this._fieldRenderer.appendChild(document.createTextNode(label));
			this._fieldRenderer.onclick = this.sendProfile.bindAsEventListener(this);
		}
		
		if(!this._node.commandBeanProxy.nodeListener.isSearchViewMode(this._node.commandBeanProxy)){
			return this._fieldRenderer;
		}else if(!this._node.commandBeanProxy.nodeListener.isDifferentGroup(this._node)){
			return this._fieldRenderer;
		}
	}
	
	SendLessonTicketButton.prototype.sendProfile = function(event){
		var url = window.location.origin + window.location.pathname;
		this._node.commandBeanProxy.addAdditionalFormRequestParams("baseUrl", url);
		this._node.commandBeanProxy.submitForServerSideProcess(this._node, "sendEmailNotification");
	}
	SendLessonTicketButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(CostTips,D3.AbstractFieldComponent);
	function CostTips() {
		D3.AbstractFieldComponent.call(this);
	}
	
	CostTips.prototype.getFieldEditor = function() {
		if (!this._fieldEditor)
			this._fieldEditor = this.createRmIcon();
		return this._fieldEditor;
	}
	
	CostTips.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer)
			this._fieldRenderer = this.createRmIcon();
		return this._fieldRenderer;
	}
	
	CostTips.prototype.createRmIcon = function() {
		var img = document.createElement("img");
		img.setAttribute("class", "riskMatrixIcon");
		img.setAttribute("src", "./images/schematics/Annotation/Annotation.png");
		img.onclick = this.onClickRiskMatrix.bindAsEventListener(this);
		return img;
	}
	
	CostTips.prototype.onClickRiskMatrix = function() {
		var _popUpDiv = "<div><img class='riskMatrix' src='./images/lessonticket/RiskMatrix-PotentialCost.jpg'/></div>";
		this._popUpDialog = $(_popUpDiv);
		$(this._popUpDialog).dialog({
			title:D3.LanguageManager.getLabel("LT.header.PotentialCost"),
			dialogClass:"riskMatrixPopUp",
			modal:true,
			width:($(window).width()*0.8),
			height:"auto",
			maxHeight:($(window).height()*0.95),
			resizable:true,
			draggable:false
		});
		
	}
	
	D3.inherits(PriorityTips,D3.AbstractFieldComponent);
	function PriorityTips() {
		D3.AbstractFieldComponent.call(this);
	}
	
	PriorityTips.prototype.getFieldEditor = function() {
		if (!this._fieldEditor)
			this._fieldEditor = this.createRmIcon();
		return this._fieldEditor;
	}
	
	PriorityTips.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer)
			this._fieldRenderer = this.createRmIcon();
		return this._fieldRenderer;
	}
	
	PriorityTips.prototype.createRmIcon = function() {
		var img = document.createElement("img");
		img.setAttribute("class", "riskMatrixIcon");
		img.setAttribute("src", "./images/schematics/Annotation/Annotation.png");
		img.onclick = this.onClickRiskMatrix.bindAsEventListener(this);
		return img;
	}
	
	PriorityTips.prototype.onClickRiskMatrix = function() {
		var _popUpDiv = "<div><img class='riskMatrix' src='./images/lessonticket/RiskMatrix-Priority.jpg'/></div>";
		this._popUpDialog = $(_popUpDiv);
		$(this._popUpDialog).dialog({
			title:D3.LanguageManager.getLabel("LT.header.Priority"),
			dialogClass:"riskMatrixPopUp",
			modal:true,
			width:($(window).width()*0.8),
			height:"auto",
			maxHeight:($(window).height()*0.95),
			resizable:true,
			draggable:false
		});
	}
	
	D3.inherits(WellDeliveryPhaseTips,D3.AbstractFieldComponent);
	function WellDeliveryPhaseTips() {
		D3.AbstractFieldComponent.call(this);
	}
	
	WellDeliveryPhaseTips.prototype.getFieldEditor = function() {
		if (!this._fieldEditor)
			this._fieldEditor = this.createRmIcon();
		return this._fieldEditor;
	}
	
	WellDeliveryPhaseTips.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer)
			this._fieldRenderer = this.createRmIcon();
		return this._fieldRenderer;
	}
	
	WellDeliveryPhaseTips.prototype.createRmIcon = function() {
		var img = document.createElement("img");
		img.setAttribute("class", "riskMatrixIcon");
		img.setAttribute("src", "./images/schematics/Annotation/Annotation.png");
		img.onclick = this.onClickWellDeliveryPhase.bindAsEventListener(this);
		return img;
	}
	
	WellDeliveryPhaseTips.prototype.onClickWellDeliveryPhase = function() {
		var _popUpDiv = "<div><img class='riskMatrix' src='./images/lessonticket/PhaseTips.jpg'/></div>";
		this._popUpDialog = $(_popUpDiv);
		$(this._popUpDialog).dialog({
			title:D3.LanguageManager.getLabel("Well Delivery Process"),
			dialogClass:"riskMatrixPopUp",
			modal:true,
			width:($(window).width()*0.8),
			height:"auto",
			maxHeight:($(window).height()*0.95),
			resizable:true,
			draggable:false
		});
	}
	
	D3.inherits(LessonTicketDetailNodeListener,D3.EmptyNodeListener);
	
	function LessonTicketDetailNodeListener()
	{
		
	}
	
	LessonTicketDetailNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "lessonTicketCategoryElement"){
			return new LessonTicketCategoryButton();
		} else if(id == "sendEmailNotification"){
			return new SendLessonTicketButton();
		} else if(id == "costTips"){
			return new CostTips();
		} else if(id == "priorityTips"){
			return new PriorityTips();
		} else if(id == "wellDeliveryPhaseTips"){
			return new WellDeliveryPhaseTips();
		} else{
			return null;
		}
	}
	
	LessonTicketDetailNodeListener.prototype.showRootButton = function(commandBeanProxy, btn){
		if(btn.textContent == D3.RootButtons.SELECT_ALL){
			return !this.isSearchViewMode(commandBeanProxy);
		}
	}
	
	LessonTicketDetailNodeListener.prototype.isSearchViewMode = function(commandBeanProxy){
		return (commandBeanProxy.getLayoutFilterFlag("searchViewMode") != null && commandBeanProxy.getLayoutFilterFlag("searchViewMode") == "true");
	}
	
	LessonTicketDetailNodeListener.prototype.isDifferentGroup = function(node){
		return (node.commandBeanProxy.getLayoutFilterFlag("differentGroup") != null && node.commandBeanProxy.getLayoutFilterFlag("differentGroup") == "true");
			   
	}
	
	LessonTicketDetailNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if(this.isSearchViewMode(commandBeanProxy)){
			commandBeanProxy.rootNode.setAdditionalUserRequest("gotoFieldUid", commandBeanProxy.gotoFieldUid);
			commandBeanProxy.rootNode.setAdditionalUserRequest("lessonReportType", commandBeanProxy.getLayoutFilterFlag("lessonReportType"));
			commandBeanProxy.rootNode.setAdditionalUserRequest("differentGroup", commandBeanProxy.getLayoutFilterFlag("differentGroup"));
		}
	}
	
	LessonTicketDetailNodeListener.prototype.beforeDataLoad = function(requestParams, commandBeanProxy) {
		if(this.isSearchViewMode(commandBeanProxy)){
			commandBeanProxy.rootNode.collectRequestParameters(requestParams, true, false, true);
		}
	}
	
	D3.CommandBeanProxy.nodeListener = LessonTicketDetailNodeListener;
})(window);