(function(window){
	
	D3.inherits(LeakOffTestGraphLink, D3.AbstractFieldComponent);
	
	function LeakOffTestGraphLink(id) {
		D3.AbstractFieldComponent.call(this);
	}
	
	LeakOffTestGraphLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel("label.leakOffTestGraphLink"),
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("tooltip.leakOffTestGraphLink")
				},
				callbacks:{
					onclick:function(event) {
					var _graphImage = D3.UI.createElement({
						tag:"img",
						attributes:{
							// w x h from flash equivalent
							width:"640",
							height:"480",
							src: this._node.commandBeanProxy.baseUrl + "graph.html?type=leakofftest&size=medium&uid=" + escape(this._node.getFieldValue("leakOffTestUid")) + "&t=" + new Date().getTime()
						},
						context:this
					});
					var _popUpDiv = "<div></div>";
					this._popUpDialog = $(_popUpDiv);
					this._popUpDialog.append(_graphImage);
					this._popUpDialog.dialog({
						height:"auto",
						width:"660", // same as flash equivalent
						resizable:false,
						modal:true,
						title:D3.LanguageManager.getLabel("popup.graph.title")
					});
					}
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
	}

	LeakOffTestGraphLink.prototype.refreshFieldRenderer = function (){
	}
	
	D3.inherits(LeakOffTestNodeListener,D3.EmptyNodeListener);
	
	function LeakOffTestNodeListener() { }
	
	
	LeakOffTestNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "leakOffTestGraphPopup"){
			return new LeakOffTestGraphLink(id);			
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = LeakOffTestNodeListener;
	
})(window);