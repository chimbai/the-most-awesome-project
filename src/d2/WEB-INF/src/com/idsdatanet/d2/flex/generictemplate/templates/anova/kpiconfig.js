(function(window){
	function isShowHoleSize(node) {
		var lookup = node.getLookup("kpiRepositoryUid");
		if(lookup){
			var item = lookup.getLookupItem(node.getFieldValue("kpiRepositoryUid"));
			if(item && item.filterProperties){
				return item.filterProperties["showHoleSize"] == "1";
			}
		}
		return false;
	};

	D3.inherits(HoleSizeValidator,D3.Validator);
	function HoleSizeValidator(){
	}
	HoleSizeValidator.prototype.doValidate = function() {
		if(this.source._showHoleSize){
			var value = this.source.valueToValidate();
			if(value && value.length > 0){
				return true;
			}else{
				this.addError("This field is required.");
				return false;
			}
		}else{
			this.source.clearSelectedValue();
			return true;
		}
	};
	
	D3.inherits(HoleSizeField,D3.MultiSelectField);
	function HoleSizeField(){
		D3.MultiSelectField.call(this);
	};
	HoleSizeField.prototype.getFieldValidator = function() {
		return new HoleSizeValidator();
	};
	HoleSizeField.prototype.updateShowHoleSizeFlag = function() {
		this._showHoleSize = isShowHoleSize(this._node);
	};
	HoleSizeField.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		HoleSizeField.uber.dispose.call(this);
	};
	HoleSizeField.prototype.fieldChangeListener = function(event) {
		if(event.fieldName == "kpiRepositoryUid"){
			this.updateShowHoleSizeFlag();
			if(this.isEditModeInUse()){
				this.refreshFieldEditor();
			}else{
				this.refreshFieldRenderer();
			}
		}
	};
	HoleSizeField.prototype.getEditorForCssError = function() {
		return this._fieldEditor;
	};
	HoleSizeField.prototype.getFieldEditor = function() {
		if(!this._editorContainer){
			HoleSizeField.uber.getFieldEditor.call(this);
			this._fieldEditor.className = this._fieldEditor.className + " required";
			var div = document.createElement("div");
			div.appendChild(this._fieldEditor);
			var altEditor = document.createElement("span");
			altEditor.appendChild(document.createTextNode("N/A"));
			div.appendChild(altEditor);
			this._altEditor = altEditor;
			this._editorContainer = div;
			
			this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		}
		return this._editorContainer;
	};
	HoleSizeField.prototype.refreshFieldEditor = function() {
		if(this._showHoleSize){
			this._fieldEditor.style.display = "";
			this._altEditor.style.display = "none";
			HoleSizeField.uber.refreshFieldEditor.call(this);
		}else{
			this._fieldEditor.style.display = "none";
			this._altEditor.style.display = "";
		}
	};
	HoleSizeField.prototype.clearSelectedValue = function() {
		var value = this.getSelectedValue();
		if(value && value.length > 0){
			this.setSelectedValueToField([]);
		}
	};
	HoleSizeField.prototype.getFieldRenderer = function() {
		if(!this._rendererContainer){
			HoleSizeField.uber.getFieldRenderer.call(this);
			var div = document.createElement("div");
			div.appendChild(this._fieldRenderer);
			var altRenderer = document.createElement("span");
			altRenderer.style.padding = "4px";
			altRenderer.appendChild(document.createTextNode("N/A"));
			div.appendChild(altRenderer);
			this._altRenderer = altRenderer;
			this._rendererContainer = div;
			this.updateShowHoleSizeFlag();
		}
		return this._rendererContainer;
	};
	HoleSizeField.prototype.refreshFieldRenderer = function() {
		if(this._showHoleSize){
			this._fieldRenderer.style.display = "";
			this._altRenderer.style.display = "none";
			HoleSizeField.uber.refreshFieldRenderer.call(this);
		}else{
			this._fieldRenderer.style.display = "none";
			this._altRenderer.style.display = "";
		}
	};
	
	D3.inherits(FilterLookupField,D3.ComboBoxField);
	function FilterLookupField(){
		D3.ComboBoxField.call(this);
	}
	FilterLookupField.prototype.getLookupRef = function() {
		var lookup = this._config.multiselectLookup || this._config.lookup;
		var i = lookup.indexOf(".");
		var className = lookup.substring(0,i);
		var field = lookup.substring(i + 1);
		if(this._config.multiselectLookup){
			return this._node.commandBeanProxy.getMultiSelect(className, field);
		}
		return this._node.commandBeanProxy.getLookup(className, field);
	};

	D3.inherits(KpiCustomCategoryField,D3.AbstractFieldComponent);
	function KpiCustomCategoryField(categoryClass){
		D3.AbstractFieldComponent.call(this);
		this.categoryClass = categoryClass;
	}
	KpiCustomCategoryField.prototype.createDialog = function(options){
		var dialog = document.createElement("div");
		var style = dialog.style;
		style.position = "absolute";
		style.backgroundColor = "#f2f2f2";
		style.boxShadow = "0px 3px 6px #949494";
		
		var titleBar = document.createElement("div");
		style = titleBar.style;
		style.padding = ".4em .8em"; 
		style.position = "relative";
		style.color = "#fff";
		style.backgroundColor = "#5c9ccc";
		style.cursor = "move";
		dialog.appendChild(titleBar);
		
		var titleText = document.createElement("div");
		style = titleText.style;
		style.whiteSpace = "nowrap";
		style.width = "90%";
		style.overflow = "hidden";
		style.textOverflow = "ellipsis";
		style.fontFamily = "Helvetica, Verdana, Arial";
		style.fontSize = "13px";
		titleBar.appendChild(titleText);
		
		var closeButton = document.createElement("button");
		closeButton.type = "button"
		style = closeButton.style;
		style.position = "absolute";
		style.right = ".5em";
		style.top = "50%";
		style.margin = "-10px 0 0 0";
		style.width = "20px";
		style.height = "20px";
		titleBar.appendChild(closeButton);
		
		var closeIcon = document.createElement("span");
		closeIcon.className = "ui-icon ui-icon-closethick";
		closeIcon.style.margin = "-1px 0 0 -6px";
		closeButton.appendChild(closeIcon);

		titleText.appendChild(document.createTextNode(options.title));
		closeButton.onclick = options.closeFunction;
		
		return dialog;
	};
	KpiCustomCategoryField.prototype.showPopup = function() {
		if(this.popup) return;
		
		var me = this;
		var closeFunction = function(){me.closePopup();};
		var kpiUnit = this.getLookupLabel("kpiRepositoryUid","@kpiUnit");
		var dialog = this.createDialog({
			closeFunction: closeFunction,
			title: "KPI Custom Value for " + this.getLookupLabel("kpiRepositoryUid","kpiRepositoryUid") + (kpiUnit ? " (" + kpiUnit + ")" : "")
		});

		var btnDiv = document.createElement("span")
		var btnClose = D3.UIManager.createRegularButton(D3.LanguageManager.getLabel("button.close"), {width: "60px"});
		btnClose.onclick = closeFunction;
		btnDiv.appendChild(btnClose);
		var headerDiv = document.createElement("div");
		headerDiv.style.textAlign = "center";
		headerDiv.style.padding = "5px 0";
		headerDiv.appendChild(btnDiv);
		dialog.appendChild(headerDiv);

		var model = this._node.commandBeanProxy.commandBeanModel.getLayoutModelForClass(this.categoryClass,"custom");
		var params = {
				simpleClassName: this.categoryClass, 
				commandBeanModel: this._node.commandBeanProxy.commandBeanModel,
				layoutModel: model,
				effectiveLayout: model
		};
		var grid = new D3.SimpleGrid(params);
		grid.setDataForBaseNodesGroupContainer(this._node, this.categoryClass, "default");
		
		var table = document.createElement("div");
		table.style.display = "table";
		table.style.width = "100%";
		grid.view.style.display = "table-cell";
		table.appendChild(grid.view);
		
		var scrollPane = document.createElement("div");
		scrollPane.style.overflow = "auto";
		scrollPane.style.boxSizing = "border-box";
		scrollPane.appendChild(table);
		dialog.appendChild(scrollPane);

		if(this._config.popupSize != "auto"){
			var dialogWidth = this._config.popupWidth || 500;
			var dialogHeight = this._config.popupHeight || 500;
			dialog.style.height = (dialogHeight > window.innerHeight - 20 ? window.innderHeight - 20 : dialogHeight) + "px";
			dialog.style.width = (dialogWidth > window.innerWidth - 20 ? window.innerWidth - 20 : dialogWidth) + "px";
		}
		
		var modal = document.createElement("div");
		modal.style.position = "fixed";
		modal.style.top = 0;
		modal.style.left = 0;
		modal.style.width = "100%";
		modal.style.height = "100%";
		modal.style.zIndex = 100;
		modal.style.backgroundColor = "rgba(0,0,0,0.2)";
		modal.appendChild(dialog);
		
		document.body.appendChild(modal);
		
		if(this._config.popupSize == "auto"){
			if(dialog.offsetWidth > window.innerWidth - 20) dialog.style.width = (window.innerWidth - 20) + "px";
			if(dialog.offsetHeight > window.innerHeight - 20) dialog.style.height = (window.innerHeight - 20) + "px";
		}
		
		var box = this._fieldRenderer.getBoundingClientRect();
		var left = box.left - dialog.offsetWidth;
		dialog.style.left = (left < 0 ? 10 : left) + "px";
		dialog.style.top = (box.top + dialog.offsetHeight > window.innerHeight ? window.innerHeight - dialog.offsetHeight - 10 : box.top) + "px";
		
		var resizeContent = function(){
			scrollPane.style.width = scrollPane.parentNode.clientWidth + "px";
			scrollPane.style.height = (scrollPane.parentNode.clientHeight - scrollPane.offsetTop) + "px";
		};
		
		resizeContent();
		
		$(dialog).draggable();
		$(dialog).resizable({
			handles: "n, e, s, w",
			resize: resizeContent,
			minWidth: btnDiv.offsetWidth,
			minHeight: scrollPane.offsetTop
		});
		
		this.monitorChanges(true);
		
		this.popup = {dialog: dialog, grid: grid, rootElement: modal, scrollPane: scrollPane};
	};
	KpiCustomCategoryField.prototype.monitorChanges = function(on){
		var childNodes = this._node.children[this.categoryClass];
		if(childNodes){
			childNodes.bindEvent(on, D3.CollectionEvent.COLLECTION_CHANGE, this.childNodesChange, this);
		}
	};
	KpiCustomCategoryField.prototype.childNodesChange = function(event){
		if (event.kind === D3.CollectionEvent.ADD) {
			var scrollPane = this.popup.scrollPane;
			if(scrollPane.scrollHeight > scrollPane.clientHeight) scrollPane.scrollTop = scrollPane.scrollHeight - scrollPane.clientHeight;
		}
	};
	KpiCustomCategoryField.prototype.retrieveSourceNodes = function(){
		var nodes = this._node.children[this.categoryClass];
		var result = new Array(nodes.getLength());
		for(var i=0; i < result.length; ++i){
			result[i] = nodes.getItemAt(i);
		}
		return result;
	};
	KpiCustomCategoryField.prototype.validatePopup = function(){
		var nodes = this.retrieveSourceNodes();
		var i;
		var result = true;
		for (i=0; i < nodes.length; ++i){
			var node = nodes[i];
			if(node.atts.editMode){
				node.preprocessBeforeFormSubmission(true);
				if(node.atts && node.atts.editMode){
					if(!node.isAllNodesValid()) result = false;
				}
			}
		}
		var j;
		for (i=0; i < nodes.length; ++i){
			var node = nodes[i];
			if(node.atts && node.atts.editMode){
				var customValue = node.getFieldValue("customValue");
				if(customValue){
					for (j=0; j < nodes.length; ++j){
						if(i != j){
							var n2 = nodes[j];
							if(!n2.disposed && customValue == n2.getFieldValue("customValue")){
								var validator = node.getNodeValidator("customValue");
								validator.addError("Duplicate record found");
								validator.dispatchValidationResult();
								result = false;
								break;
							}
						}
					}
				}
			}
		}
		return result;
	};
	KpiCustomCategoryField.prototype.closePopup = function(){
		if(!this.validatePopup()){
			alert("You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
			return;
		}
		this.refreshFieldRenderer();
		this.monitorChanges(false);
		this.popup.grid.dispose();
		$(this.popup.dialog).draggable("destroy");
		$(this.popup.dialog).resizable("destroy");
		document.body.removeChild(this.popup.rootElement);
		delete this.popup;
	};
	KpiCustomCategoryField.prototype.getLookupLabel = function(field, lookup) {
		var value = this._node.getFieldValue(field);
		var lookup = this._node.getLookup(lookup);
		if (lookup) {
			var lookupItem = lookup.getLookupItem(value);
			if (lookupItem) {
				return lookupItem.label;
			}
		}
		return "";		
	};
	KpiCustomCategoryField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = document.createElement("div");
			var me = this;
			this._fieldRenderer.onclick = function(){
				me.showPopup();
			}
		}
		return this._fieldRenderer;
	};
	KpiCustomCategoryField.prototype.findCustomValueLookup = function(node, value) {
		var lookup = node.getLookup("customValue");
		if (lookup) {
			var lookupItem = lookup.getLookupItem(value);
			if (lookupItem) {
				return lookupItem.label;
			}
		}
		return null;
	};
	KpiCustomCategoryField.prototype.refreshFieldRenderer = function() {
		while(this._fieldRenderer.firstChild) this._fieldRenderer.removeChild(this._fieldRenderer.firstChild);
		var nodes = this._node.children[this.categoryClass];
		var i = 0, div;
		var total = nodes.getLength();
		for (; i < total; ++i){
			var node = nodes.getItemAt(i);
			if(node.getNodeAction() != D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
				var value = node.getFieldValue("customValue");
				if(value){
					var lookup = this.findCustomValueLookup(node, value);
					div = document.createElement("div");
					var text = (lookup || value) + " = " + node.getFieldValue("@kpiTarget") + "/" + node.getFieldValue("@kpiTolerance");
					div.appendChild(document.createTextNode(text));
					this._fieldRenderer.appendChild(div);
				}
			}
		}
		div = document.createElement("div");
		div.style.cursor = "pointer";
		div.style.textDecoration = "underline";
		div.appendChild(document.createTextNode("Customise"));
		this._fieldRenderer.appendChild(div);
	};
	KpiCustomCategoryField.prototype.data = function(data) {
		KpiCustomCategoryField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
	};
	KpiCustomCategoryField.prototype.fieldChangeListener = function(event) {
		if(event.fieldName == "isInverted"){
			this.refreshFieldRenderer();
		}
	};
	KpiCustomCategoryField.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		KpiCustomCategoryField.uber.dispose.call(this);
	};
	
	D3.inherits(KpiRangeField,D3.AbstractFieldComponent);
	function KpiRangeField(invert){
		D3.AbstractFieldComponent.call(this);
		this._invertFlag = invert;
	}
	KpiRangeField.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		KpiRangeField.uber.dispose.call(this);
	};
	KpiRangeField.prototype.fieldChangeListener = function(event) {
		if(event.fieldName == "isInverted"){
			this._invertFlag = ("1" == this._node.getFieldValue("isInverted"));
		}
		if(event.fieldName == "@kpiTarget" || event.fieldName == "@kpiTolerance" || event.fieldName == "isInverted"){
			this.refreshFieldRenderer();
		}
	};
	KpiRangeField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var div = document.createElement("div");
			div.style.width = this._config.width + "px";
			div.style.height = this._config.height + "px";
			this._fieldRenderer = div;
			this._rangeWidth = this._config.width;
			this._tooltips = [this.createTooltip(div, 0), this.createTooltip(div, 1)];
			this._range1 = document.createElement("div");
			this._range1.className = "KpiRangeBar";
			div.appendChild(this._range1);
			this._range2 = document.createElement("div");
			this._range2.className = "KpiRangeBar";
			div.appendChild(this._range2);
			this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		}
		return this._fieldRenderer;
	};
	KpiRangeField.prototype.createTooltip = function(parent, index) {
		var arrow = document.createElement("span");
		arrow.className = index == 0 ? "KpiRangeTooltipArrow" : "KpiRangeTooltipArrowBottom"; 
		var textNode = document.createElement("span");
		textNode.className = index == 0 ? "KpiRangeTooltipText" : "KpiRangeTooltipTextBottom";
		arrow.appendChild(textNode);
		parent.appendChild(arrow);
		return {arrow: arrow, text: textNode};
	};
	KpiRangeField.prototype.updateTooltip = function(index, position, label) {
		var target = this._tooltips[index];
		target.arrow.style.left = (position - 6) + "px";
		while(target.text.firstChild) target.text.removeChild(target.text.firstChild);
		target.text.appendChild(document.createTextNode(label));
	};
	KpiRangeField.prototype.refreshFieldRenderer = function() {
		var v1Ori = this._node.getFieldValue("@kpiTarget");
		var v2Ori = this._node.getFieldValue("@kpiTolerance");
		var v1 = parseFloat(v1Ori);
		var v2 = parseFloat(v2Ori);
		if(isNaN(v1) || isNaN(v2)){
			this._fieldRenderer.className = "KpiRangeInvalid";
			return;
		}
		if(v1 > v2){
			var t = v1;
			v1 = v2;
			v2 = t;
			t = v1Ori;
			v1Ori = v2Ori;
			v2Ori = t;
		}
		this._fieldRenderer.className = "KpiRange";
		this._fieldRenderer.style.backgroundColor = (this._invertFlag ? "red" : "green");
		var total = v2 * 1.2;
		var p1 = Math.round(v1/total * this._rangeWidth);
		var p2 = Math.round(v2/total * this._rangeWidth);
		this.updateTooltip(0, p1, v1Ori);
		this.updateTooltip(1, p2, v2Ori);
		this._range1.style.width = p1 + "px";
		this._range1.style.backgroundColor = (this._invertFlag ? "green" : "red");
		this._range2.style.width = (p2 - p1) + "px";
		this._range2.style.left = p1 + "px";
		this._range2.style.backgroundColor = "#ebe238";
	};
	
	D3.inherits(KpiUnitField,D3.AbstractFieldComponent);
	function KpiUnitField(){
		D3.AbstractFieldComponent.call(this);
	}
	KpiUnitField.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		KpiUnitField.uber.dispose.call(this);
	};
	KpiUnitField.prototype.fieldChangeListener = function(event) {
		if(event.fieldName == "kpiRepositoryUid"){
			this.refreshFieldRenderer();
		}
	};
	KpiUnitField.prototype.data = function(data) {
		KpiUnitField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
	}
	KpiUnitField.prototype.getFieldValue = function() {
		var value = this._node.getFieldValue("kpiRepositoryUid");
		var lookup = this._node.getLookup(this._fieldName);
		if (lookup) {
			var lookupItem = lookup.getLookupItem(value);
			if (lookupItem) {
				return lookupItem.label;
			}
		}
		return "";		
	};
	
	D3.inherits(ApplyFilter,D3.AbstractFieldComponent);
	function ApplyFilter(){
		D3.AbstractFieldComponent.call(this);
	}
	ApplyFilter.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		ApplyFilter.uber.dispose.call(this);
	};
	ApplyFilter.prototype.fieldChangeListener = function(event) {
		var i;
		for(i=0; i < this._listenToFields.length; ++i){
			if(this._listenToFields[i] == event.fieldName){
				this._fieldRenderer.style.backgroundColor = "#fcebb8";
				break;
			}
		}
	};
	ApplyFilter.prototype.getFieldRenderer = function() {
		if(!this._fieldRenderer){
			this._fieldRenderer = D3.UIManager.createRegularButton(D3.LanguageManager.getLabel("KpiConfig.applyFilter"));
			var me = this;
			this._fieldRenderer.onclick = function(){
				me._node.commandBeanProxy.submitCurrentNodeAndReload(me._node, null, true);
			};
			this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
			var fields = this._config.listenToFields.split(",");
			var i;
			for(i=0; i < fields.length; ++i){
				fields[i] = fields[i].trim();
			}
			this._listenToFields = fields;
		}
		return this._fieldRenderer;
	};
	ApplyFilter.prototype.refreshFieldRenderer = function() {
	};

	D3.inherits(ClearFilter,D3.AbstractFieldComponent);
	function ClearFilter(){
		D3.AbstractFieldComponent.call(this);
	}
	ClearFilter.prototype.getFieldRenderer = function() {
		if(!this._fieldRenderer){
			this._fieldRenderer = D3.UIManager.createRegularButton(D3.LanguageManager.getLabel("KpiConfig.clearFilter"));
			var me = this;
			this._fieldRenderer.onclick = function(){
				for(var i=0; i < me._filterFields.length; ++i){
					me._node.setFieldValue(me._filterFields[i],"");
				}
				me._node.commandBeanProxy.submitCurrentNodeAndReload(me._node, null, true);
			};
			var fields = this._config.filterFields.split(",");
			for(var i=0; i < fields.length; ++i){
				fields[i] = fields[i].trim();
			}
			this._filterFields = fields;
		}
		return this._fieldRenderer;
	};
	ClearFilter.prototype.refreshFieldRenderer = function() {
	};

	D3.inherits(IsInvertedField,D3.ComboBoxField);
	function IsInvertedField(){
		D3.ComboBoxField.call(this);
	}
	IsInvertedField.prototype.swapTargetAndTolerance = function(node) {
		if(!node.atts.editMode) node.setEditMode(true);
		var target = node.getFieldValue("@kpiTarget");
		node.setFieldValue("@kpiTarget", node.getFieldValue("@kpiTolerance"));
		node.setFieldValue("@kpiTolerance", target);
	}
	IsInvertedField.prototype.setFieldValue = function(value) {
		if(!this._node) return;
		var old_isInverted = this._node.getFieldValue(this._fieldName) == "1";
		var new_isInverted = value == "1";
		if(old_isInverted != new_isInverted){
			this.swapTargetAndTolerance(this._node);
			if(this._node.children){
				var className, nodes, i;
				for(className in this._node.children){
					nodes = this._node.children[className];
					for(i=0; i < nodes.getLength(); ++i){
						this.swapTargetAndTolerance(nodes.getItemAt(i));
					}
				}
			}
		}
		IsInvertedField.uber.setFieldValue.call(this, value);
	};
	
	D3.inherits(KpiConfigListener,D3.EmptyNodeListener);
	function KpiConfigListener()
	{
	}
	KpiConfigListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "kpiUnit"){
			return new KpiUnitField();
		}else if(id == "kpiRange"){
			var sourceNode = (node.simpleClassName() == "KpiConfig" ? node : node.parent);
			return new KpiRangeField(sourceNode.getFieldValue("isInverted") == "1");
		}else if(id == "kpiCountry"){
			return new KpiCustomCategoryField("KpiCountry");
		}else if(id == "kpiCampaign"){
			return new KpiCustomCategoryField("KpiCampaign");
		}else if(id == "kpiRig"){
			return new KpiCustomCategoryField("KpiRig");
		}else if(id == "kpiWell"){
			return new KpiCustomCategoryField("KpiWell");
		}else if(id == "filterLookup"){
			return new FilterLookupField();
		}else if(id == "ApplyFilter"){
			return new ApplyFilter();
		}else if(id == "ClearFilter"){
			return new ClearFilter();
		}else if(id == "HoleSize"){
			return new HoleSizeField();
		}else if(id == "isInverted"){
			return new IsInvertedField();
		}
	};
	KpiConfigListener.prototype.showValidationError = function(validator, error){
		if(validator){
			validator.addError(error);
			validator.dispatchValidationResult();
		}
	};
	KpiConfigListener.prototype.validateNodeOnSubmit = function(node, validators, isValid){
		var result = true;
		var className = node.simpleClassName();
		if(className == "KpiConfig" || className == "KpiCountry" || className == "KpiCampaign" || className == "KpiRig" || className == "KpiWell"){
			var invert = (className == "KpiConfig" ? node : node.parent).getFieldValue("isInverted") == "1";
			var target = node.getFieldValue("@kpiTarget");
			var tolerance = node.getFieldValue("@kpiTolerance");
			if(target && tolerance){
				var targetValue = parseFloat(target);
				var toleranceValue = parseFloat(tolerance);
				if(!isNaN(targetValue) && !isNaN(toleranceValue)){
					var error = null;
					if(targetValue == toleranceValue){
						error = "Target and Tolerance value entered cannot be the same";
					}else if(invert && targetValue > toleranceValue){
						error = "Target must be smaller than Tolerance";
					}else if(!invert && targetValue < toleranceValue){
						error = "Target must be larger than Tolerance";
					}
					if(error){
						this.showValidationError(validators["@kpiTarget"], error);
						this.showValidationError(validators["@kpiTolerance"], error);
						result = false;
					}
				}
			}
		}
		if(className == "KpiConfig"){
			if(isShowHoleSize(node) && node.getFieldValue("context") == "rig"){
				this.showValidationError(validators["kpiRepositoryUid"], "This is a Hole Size KPI which is not Rig related");
				result = false;
			}
		}
		return result;
	};
	KpiConfigListener.prototype.getCustomRecordActionButton = function(id, node, commandBean, buttonDeclaration, buttonData) {
		if (id == "createRecord" && node.simpleClassName() == "KpiConfig") {
			var btn = document.createElement("button");
			btn.className = "RecordActionsButton CreateRecord";
			btn.type = "button";
			btn.style.outline = "none";
			var icon = document.createElement("i");
			icon.className = "fa fa-angle-down";
			btn.appendChild(icon);
			var container = document.createElement("span");
			container.style.position = "relative";
			container.style.display = "inline-block";
			container.style.verticalAlign = "top";
			container.appendChild(btn);
			var dropdownHolder = {};
			var showPopup = function(show){
				dropdownHolder.dropdown.style.display = (show ? "block" : "none");
				dropdownHolder.show = show ? true : false;
			};
			container.addEventListener("mouseleave", function(){
				if(dropdownHolder.show){
					dropdownHolder.active = false;
					setTimeout(function(){
						if(!dropdownHolder.active) showPopup(false);
					}, 500);
				}
			});
			container.addEventListener("mouseenter", function(){
				if(dropdownHolder.show) dropdownHolder.active = true;
			});
			var btnClick = function(){
				if(!dropdownHolder.dropdown){
					var dropdown = document.createElement("div");
					dropdown.style.position = "absolute";
					dropdown.style.top = "100%";
					dropdown.style.left = "0";
					dropdown.style.backgroundColor = "white";
					dropdown.style.border = "1px solid rgba(0,0,0,.15)";
					dropdown.style.borderRadius = "4px";
					dropdown.style.boxShadow = "0 6px 12px rgba(0,0,0,.175)";
					dropdown.style.cursor = "pointer";
					dropdown.style.zIndex = "100";
					var addAbove = document.createElement("div");
					addAbove.className = "RecordActionInsert";
					addAbove.style.padding = "8px 8px 4px 8px";
					addAbove.appendChild(document.createTextNode("Insert Above"));
					dropdown.appendChild(addAbove);
					var addBelow = document.createElement("div");
					addBelow.className = "RecordActionInsert";
					addBelow.style.padding = "4px 8px 8px 8px";
					addBelow.appendChild(document.createTextNode("Insert Below"));
					dropdown.appendChild(addBelow);
					addAbove.addEventListener("click", function(){
						var targetIndex = node.getCurrentNodeIndex();
						node.parent.addNewChildNodeOfClass(node.simpleClassName(), true, true, {targetNode:node, insert:"before", insertAtIndexOf:targetIndex, triggerNodeCreatedEvent:false});
						showPopup(false);
					});
					addBelow.addEventListener("click", function(){
						var targetIndex = node.getCurrentNodeIndex();
						node.parent.addNewChildNodeOfClass(node.simpleClassName(), true, true, {targetNode:node, insert:"after", insertAtIndexOf:++targetIndex, triggerNodeCreatedEvent:false});
						showPopup(false);
					});
					container.appendChild(dropdown);
					dropdownHolder.dropdown = dropdown;
					showPopup(true);
				}else{
					showPopup(!dropdownHolder.show);
				}
			};
			btn.addEventListener("click", btnClick);
			return container;
		}
		return null;
	};
	
	D3.CommandBeanProxy.nodeListener = KpiConfigListener;
})(window);