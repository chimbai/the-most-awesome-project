package com.idsdatanet.d2.flex.dat;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DatUtilsForOSL extends BlazeRemoteClassSupport{

	private Locale getLocale(HttpServletRequest request) throws Exception {
		return UserSession.getInstance(request).getUserLocale();
	}
		
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
	
	private class operationListComparator implements Comparator<String>{
		private UserSession userSession;
		public operationListComparator(UserSession userSession)
		{
			this.userSession = userSession;
		}
		public int compare(String o1, String o2) {
			
			try{
				o1 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), o1, null, true));
				o2 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), o2, null, true));
				
				if (o1 == null || o2 == null) return 0;
				return o1.compareTo(o2);
				
			} catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private class operationComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Map<String, Object> op1 = (Map<String, Object>) o1[1];
				Map<String, Object> op2 = (Map<String, Object>) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(op1.get("operationName").toString());
				String f2 = WellNameUtil.getPaddedStr(op2.get("operationName").toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	private String getPhaseName(String phaseCode) {
		String phaseName = phaseCode;
		try {
			if(StringUtils.isNotBlank(phaseCode)) { 
				List<LookupPhaseCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", phaseCode);
				if (list.size()>0) {
					LookupPhaseCode code = list.get(0);
					phaseName = code.getName() + " (" + phaseCode + ")";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return phaseName;
		
	}

	private String getTaskName(String taskCode) {
		String taskName = taskCode;
		try {
			if(StringUtils.isNotBlank(taskCode)) { 
				List<LookupTaskCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupTaskCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", taskCode);
				if (list.size()>0) {
					LookupTaskCode code = list.get(0);
					taskName = code.getName() + " (" + taskCode + ")";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taskName;
		
	}
	
	private String getRootCauseName(String rootCauseCode) {
		String rootCauseName = rootCauseCode;
		try {
			if(StringUtils.isNotBlank(rootCauseCode)) { 
				List<LookupRootCauseCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupRootCauseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", rootCauseCode);
				if (list.size()>0) {
					LookupRootCauseCode code = list.get(0);
					rootCauseName = code.getName() + " (" + rootCauseCode + ")";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootCauseName;
		
	}

	
	public String queryLessonTicketRP2OSL(String[] operations){
		String result = "<root/>";
		try{
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			String serviceTypeLookup="xml://company.service?key=code&amp;value=label";
			Map<String,LookupItem> serviceTypeMap = LookupManager.getConfiguredInstance().getLookup(serviceTypeLookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			
			for (String operationUid : operations) {
				
				operationUomContext.setOperationUid(operationUid);
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				
				//Lesson Ticket
				String lessonticketquery = "SELECT lt FROM LessonTicket lt " +
								  "WHERE (lt.isDeleted=false OR lt.isDeleted is null) " +
								  "AND lt.operationUid =:operationUid " +
								  "AND (lt.lessonReportType is null or lt.lessonReportType ='') " +
								  "ORDER BY lt.rp2Date";
				List lessonticketlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lessonticketquery, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
				if (lessonticketlist.size() > 0) {
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("wellName",CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
					writer.startElement("operation",attr);
					
					Collections.sort(lessonticketlist, new LessonTicketComparator());
					for (Object lessonticketobj:lessonticketlist){
						attr = new SimpleAttributes();
						LessonTicket lt = (LessonTicket) lessonticketobj;
						
						String WSPvalue = "No";
						String WBSvalue = "No";
						String Lvalue = "No";
						String DIvalue = "No";
						String SCPvalue = "No";
						String PPvalue = "No";
						String PALvalue = "No";
						String serviceType = "";
						String hideInReport = "";
						
						attr.addAttribute("lessonTicketNumber",nullToEmptyString(lt.getLessonTicketNumber()));
						
						if (lt.getRp2Date() != null){
							attr.addAttribute("rp2Date", nullToEmptyString(df.format((Date) lt.getRp2Date())));
						}
						
						attr.addAttribute("rp2Originator",nullToEmptyString(lt.getRp2Originator()));
						attr.addAttribute("lessonTitle",nullToEmptyString(lt.getLessonTitle()));
										
						attr.addAttribute("rootcauseCode",nullToEmptyString(lt.getRootcauseCode()));
						attr.addAttribute("rootcauseName", this.getRootCauseName(nullToEmptyString(lt.getRootcauseCode())));
										
						attr.addAttribute("taskCode",nullToEmptyString(lt.getTaskCode()));
						attr.addAttribute("taskName", this.getTaskName(nullToEmptyString(lt.getTaskCode())));
						
						attr.addAttribute("phaseCode",nullToEmptyString(lt.getPhaseCode()));
						attr.addAttribute("phaseName", this.getPhaseName(nullToEmptyString(lt.getPhaseCode())));
										
						attr.addAttribute("descrLesson",nullToEmptyString(lt.getDescrLesson()));
						attr.addAttribute("rp2Status",nullToEmptyString(lt.getRp2Status()));
						attr.addAttribute("approxValue",nullToEmptyString(lt.getApproxValue()));
						
						if(StringUtils.isNotBlank(lt.getServiceType())) {
							if(serviceTypeMap.containsKey(lt.getServiceType())) {
								serviceType = (String) serviceTypeMap.get(lt.getServiceType()).getValue();
							}
						}
						attr.addAttribute("serviceType",nullToEmptyString(serviceType));
						attr.addAttribute("responsibleparty",nullToEmptyString(lt.getResponsibleparty()));
						attr.addAttribute("descrPostevent",nullToEmptyString(lt.getDescrPostevent()));
						attr.addAttribute("descrEvent",nullToEmptyString(lt.getDescrEvent()));
						attr.addAttribute("embeddedMechanism",nullToEmptyString(lt.getEmbeddedMechanism()));
						
						String embeddedMechanismData = nullToEmptyString(lt.getEmbeddedMechanism());
						String[]  embeddedMechanismitems = embeddedMechanismData.trim().split("\\s+");
						for (String item : embeddedMechanismitems)
						{	
							if ("WSP".equals(item)) {
								WSPvalue = "Yes";								
							} 
							
							if ("WBS".equals(item)) {
								WBSvalue = "Yes";								
							} 
							
							if (item.matches("L")) {
								Lvalue = "Yes";
								attr.addAttribute("Logistic",Lvalue);
							} 
							
							if (item.matches("DI")) {
								DIvalue = "Yes";								
							} 
							
							if (item.matches("SCP")) {
								SCPvalue = "Yes";								
							}
							
							if (item.matches("PP")) {
								PPvalue = "Yes";								
							}
							
							if (item.matches("PAL")) {
								PALvalue = "Yes";								
							}
						}
						
						attr.addAttribute("WellSpecificProgramme",WSPvalue);
						attr.addAttribute("WBS",WBSvalue);
						attr.addAttribute("DailyInstruction",DIvalue);
						attr.addAttribute("ServiceCoProcedures",SCPvalue);
						attr.addAttribute("ParkerProcedures",PPvalue);
						attr.addAttribute("ParkerACtionList",PALvalue);
						
						attr.addAttribute("rp2Otherchallenges",nullToEmptyString(lt.getRp2Otherchallenges()));
						attr.addAttribute("rp2Needtodo",nullToEmptyString(lt.getRp2Needtodo()));
						attr.addAttribute("rp2Focalpoint",nullToEmptyString(lt.getRp2Focalpoint()));
						attr.addAttribute("rp2Actionby",nullToEmptyString(lt.getRp2Actionby()));
						
						if (lt.getHideInDdr() != null) {
							if(lt.getHideInDdr()) {
								hideInReport = "Yes";
							} else if (!lt.getHideInDdr()) {
								hideInReport = "No";
							}
						}
						attr.addAttribute("hideInDdr",hideInReport);
						attr.addAttribute("lastEditUserUid",nullToEmptyString(lt.getLastEditUserUid()));
						
						if (lt.getRp2Closeoutdate() != null){
							attr.addAttribute("rp2Closeoutdate", nullToEmptyString(df.format((Date) lt.getRp2Closeoutdate())));
						}
						
						attr.addAttribute("rp2Dsapproval",nullToEmptyString(lt.getRp2Dsapproval()));
						attr.addAttribute("lastEditDatetime",nullToEmptyString(lt.getLastEditDatetime()));
						
						writer.addElement("LessonLearned", null, attr);
					}								
					writer.endElement();
				}
			}
			writer.close();
			result = bytes.toString("utf-8");
		}catch (Exception e){
			 e.printStackTrace();
			 result = "<root/>";
		}	
			return result;
	}
	
	private class LessonTicketComparator implements Comparator<LessonTicket>{

		public int compare(LessonTicket lt1, LessonTicket lt2) {
			// TODO Auto-generated method stub
			try {
				String s1 = "";
				String s2 = "";
				if (StringUtils.isNotBlank(lt1.getLessonTicketNumber())) s1 = WellNameUtil.getPaddedStr(lt1.getLessonTicketNumber());
				if (StringUtils.isNotBlank(lt2.getLessonTicketNumber())) s2 = WellNameUtil.getPaddedStr(lt2.getLessonTicketNumber());
				return s1.compareTo(s2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
		
	}
	
	public String queryMudVolume_forOSL(String[] operations, Double[] holeSizes, String formations) {
		String result = "<root/>";
		
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;
			writer.startElement("root");
			SimpleAttributes items;
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(3);
			formatter.setMaximumFractionDigits(3);
			CustomFieldUom rdLastHolesizeConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "lastHolesize", operationUomContext);
			
			
			String operationsuidlist = "";
			String lastHolesizelist = "";
			List<String> lstOperations = new ArrayList<String>();
			for (String operationUid : operations) {
				lstOperations.add(operationUid);
				if(operationsuidlist=="") {
					operationsuidlist= "'" + operationUid + "'";
				} else {
					operationsuidlist+=",'" + operationUid + "'";
				}
			}
			//Query Filters
			for (Double lastHolesize : holeSizes) {
				
				if(lastHolesizelist.length()==0) {
					rdLastHolesizeConverter.setBaseValueFromUserValue(lastHolesize);
					lastHolesizelist= "'" + rdLastHolesizeConverter.getBasevalue() + "'";
				}
				else {
					rdLastHolesizeConverter.setBaseValueFromUserValue(lastHolesize);
					lastHolesizelist+=",'" + rdLastHolesizeConverter.getBasevalue() + "'";
				}
			}
			
			items = new SimpleAttributes();
			String lookup="xml://mudvolumedetail.type_lookup?key=code&amp;value=label"; 	
			String sublookup="xml://mudvolumedetail.label_lookup?key=code&amp;value=label";
			Map<String,LookupItem> catList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			for (Map.Entry<String, LookupItem> entry : catList.entrySet())
			{
				String catShortCode = entry.getKey();
				String catDescription = entry.getValue().getValue().toString();
				Map<String,LookupItem> subCatList = LookupManager.getConfiguredInstance().getCascadeLookup(sublookup, new UserSelectionSnapshot(this.getCurrentUserSession()),catShortCode , null);
				
				for (Map.Entry<String,LookupItem> entrySub : subCatList.entrySet())
				{
					
					String subCatShortCode = entrySub.getKey();
					String subCatDescription = entrySub.getValue().getValue().toString();
					if (subCatShortCode!= null) items.addAttribute("LocationShortCode", nullToEmptyString(subCatShortCode));
					if (subCatShortCode!= null) items.addAttribute("LocationDescription", nullToEmptyString(subCatDescription));
					if (subCatShortCode!= null) items.addAttribute("Type", nullToEmptyString(catShortCode));
					if (subCatShortCode!= null) items.addAttribute("TypeDescription", nullToEmptyString(catDescription));
					writer.addElement("VolumeType", "", items);
				}
				items.addAttribute("LocationShortCode", "dummy_totals");
				
				if ("vol".equals(catShortCode))items.addAttribute("LocationDescription", "Volume Total");
				if ("loss".equals(catShortCode))items.addAttribute("LocationDescription", "Loss Total");
				if ("addition".equals(catShortCode))items.addAttribute("LocationDescription", "Addition Total");
				items.addAttribute("Type", nullToEmptyString(catShortCode));
				items.addAttribute("TypeDescription", "Dummy Totals");
				writer.addElement("VolumeType", "", items);
			}
				
				
				attributes = new SimpleAttributes();				

				Collections.sort(lstOperations, new operationListComparator(this.getCurrentUserSession()));
				
				for(String operationUid : lstOperations){
					String mudVolumneUid = "";
					
					String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
					
					operationUomContext.setOperationUid(operationUid);
					
					CustomFieldUom depthMdMslConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "depthMdMsl", operationUomContext);
					CustomFieldUom mvdVolumeConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), MudVolumeDetails.class, "volume", operationUomContext);
					
					List<MudVolumes> datacheck;
					String[] paramsFields0 = {"operationUid"};
					Object[] paramsValues0 = {operationUid};
					String datachecksql = "SELECT mv.mudVolumesUid FROM MudVolumes mv, ReportDaily rd WHERE (mv.isDeleted = false or mv.isDeleted is null) and mv.operationUid =:operationUid AND (rd.isDeleted = false or rd.isDeleted is null) and rd.operationUid =mv.operationUid "+ (lastHolesizelist.length()>0?" AND rd.lastHolesize IN("+lastHolesizelist+")":"")+ (formations.length()>0?" AND rd.geology0600FormationSummary LIKE '%"+formations+"%'":"");
					datacheck = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(datachecksql, paramsFields0, paramsValues0, qp);
					
					if(datacheck != null && !datacheck.isEmpty()) {
						
						attributes.addAttribute("operationName", fullWellName);
						attributes.addAttribute("operationUid", operationUid);
						
						attributes.addAttribute("depthMdMslsymbol", depthMdMslConverter.getUomSymbol());
						attributes.addAttribute("lastHolesize", rdLastHolesizeConverter.getUomSymbol());
						attributes.addAttribute("volume", mvdVolumeConverter.getUomSymbol());
						writer.startElement("Operation", "", attributes);
					
						List<MudVolumes> mudVolumesLists;
						String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
						String[] paramsFields = {"operationUid","reportType"};
						Object[] paramsValues = {operationUid,reportType};
						String mudVolumesQuery = "SELECT rd.operationUid, rd.reportNumber, rd.reportDatetime, rd.lastHolesize, rd.depthMdMsl, mv.dailyUid, mv.mudVolumesUid FROM MudVolumes mv, ReportDaily rd WHERE (rd.isDeleted = false or rd.isDeleted is null) and (mv.isDeleted = false or mv.isDeleted is null) and mv.operationUid =:operationUid and mv.dailyUid=rd.dailyUid and rd.reportType=:reportType AND mv.operationUid=rd.operationUid "+ (lastHolesizelist.length()>0?" AND rd.lastHolesize IN("+lastHolesizelist+")":"")+(formations.length()>0?" AND rd.geology0600FormationSummary LIKE '%"+formations+"%'":"")+" Order by rd.reportDatetime,mv.checknumber";
						
						mudVolumesLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(mudVolumesQuery, paramsFields, paramsValues, qp);				
						
						if(mudVolumesLists != null && !mudVolumesLists.isEmpty()){
							
							for(Object objResultMudVolumes: mudVolumesLists){
								
								Double depthMdMslValue;
								
								items = new SimpleAttributes();
								Object[] objRstmudvolume = (Object[]) objResultMudVolumes;
										
								operationUomContext.setOperationUid(nullToEmptyString(objRstmudvolume[0]));
								
								String mudVolDailyUid=nullToEmptyString(objRstmudvolume[5]);
								
								String getGeologyFormationSummary="SELECT geology0600FormationSummary FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:mudVolDailyUid AND reportType='DGR' "+ (formations.length()>0?" AND geology0600FormationSummary LIKE '%"+formations+"%'":"");
								String[] paramsFields11 = {"mudVolDailyUid"};
								Object[] paramsValues12 = {mudVolDailyUid};
								List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(getGeologyFormationSummary,paramsFields11, paramsValues12);
								
								if(lstResult != null && lstResult.size() > 0) {
									Object thisResult = (Object) lstResult.get(0);
									if (thisResult != null) {
										String geology0600FormationSummary= nullToEmptyString(thisResult);
										items.addAttribute("geology0600FormationSummary", geology0600FormationSummary);
									}
								} else {									
									items.addAttribute("geology0600FormationSummary", "");
								} 
								
								String reportNumberValue;
								Double mvdVolume;
								Double rdLastHolesize;
								Date reportDatetimeValue;
								//Double depthMdMslValue;
								
								if (objRstmudvolume[1] != null) {
									reportNumberValue= nullToEmptyString(objRstmudvolume[1]);
									items.addAttribute("reportNumber", reportNumberValue);
									}
								if (objRstmudvolume[2] != null) {
									reportDatetimeValue= (Date) objRstmudvolume[2];
									items.addAttribute("reportDatetime", nullToEmptyString(reportDatetimeValue));
									}
								
								if (objRstmudvolume[3] != null) {																		
									rdLastHolesize= Double.parseDouble(nullToEmptyString(objRstmudvolume[3]));
									rdLastHolesizeConverter.setBaseValue(rdLastHolesize);
									items.addAttribute("rdLastHolesize",rdLastHolesizeConverter.formatOutputPrecision());
									
									}
								if (objRstmudvolume[4] != null) {
									
									depthMdMslValue= Double.parseDouble(nullToEmptyString(objRstmudvolume[4]));
									depthMdMslConverter.setBaseValue(depthMdMslValue);
									items.addAttribute("depthMdMsl",depthMdMslConverter.formatOutputPrecision());
									}
								
								writer.startElement("Day", "", items);
								items = new SimpleAttributes();								
								items.addAttribute("MudVolUid", nullToEmptyString(objRstmudvolume[6]));
								writer.startElement("mudvolumeUid", "", items);
								
								mudVolumneUid += "'" +nullToEmptyString(objRstmudvolume[6])+ "',";
								
								for (Map.Entry<String, LookupItem> entry : catList.entrySet())
								{
									items = new SimpleAttributes();
									String catShortCode = entry.getKey();
									Map<String,LookupItem> subCatList = LookupManager.getConfiguredInstance().getCascadeLookup(sublookup, new UserSelectionSnapshot(this.getCurrentUserSession()),catShortCode , null);
																		
									for (Map.Entry<String,LookupItem> entrySub : subCatList.entrySet())
									{
										
										String subCatShortCode = entrySub.getKey();
										String getMudVolumesUid=nullToEmptyString(objRstmudvolume[6]);
										String mudDetailLabel=nullToEmptyString(subCatShortCode);
										List<MudVolumeDetails> mudVolumesDetailsLists;
										
										String[] paramsFields1 = {"operationUid","getMudVolumesUid","mudDetailLabel"};
										Object[] paramsValues1 = {operationUid,getMudVolumesUid,mudDetailLabel};
										
										String mudVolumesQuery1 = "SELECT mvd.label,sum(mvd.volume),mvd.type,mv.dailyUid,mv.operationUid FROM MudVolumes mv, MudVolumeDetails mvd WHERE (mv.isDeleted is null OR mv.isDeleted = false) and (mvd.isDeleted is null OR mvd.isDeleted = false) and mv.mudVolumesUid=mvd.mudVolumesUid and mv.operationUid =:operationUid  and mv.mudVolumesUid=:getMudVolumesUid and mvd.label=:mudDetailLabel Group By mvd.label, mvd.type Order By mvd.type";
										mudVolumesDetailsLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(mudVolumesQuery1, paramsFields1, paramsValues1,qp);				
										if(mudVolumesDetailsLists != null && !mudVolumesDetailsLists.isEmpty()){
											for(Object objResultMudVolumeDetails: mudVolumesDetailsLists){		
												
												Object[] objRstmudvolumedetails = (Object[]) objResultMudVolumeDetails;
												items.addAttribute("Label", nullToEmptyString(subCatShortCode));
												mvdVolume= (Double) objRstmudvolumedetails[1];
												if (mvdVolume==null)
												{
													mvdVolume=0.0;
													mvdVolumeConverter.setBaseValue(mvdVolume);
													items.addAttribute("Volume", mvdVolumeConverter.formatOutputPrecision());
												}
												else
												{
													mvdVolumeConverter.setBaseValue(mvdVolume);
													//items.addAttribute("Volume",mvdVolumeConverter.getFormattedValue());
													items.addAttribute("Volume",mvdVolumeConverter.formatOutputPrecision());
													//items.addAttribute("Volume", mvdVolume.toString());
												}
												
												items.addAttribute("Type" , nullToEmptyString(catShortCode));
											}
										}
										else
										{
											items.addAttribute("Label", nullToEmptyString(subCatShortCode));
											mvdVolume=0.0;
											mvdVolumeConverter.setBaseValue(mvdVolume);
											items.addAttribute("Volume", mvdVolumeConverter.formatOutputPrecision());
											items.addAttribute("Type" , nullToEmptyString(catShortCode));
										}
										writer.addElement("mudvoldetails", "", items);
										
									}		
									
									items = new SimpleAttributes();
									String getMudVolumesUid=nullToEmptyString(objRstmudvolume[6]);
									String mudVolumeCode=nullToEmptyString(catShortCode);
									
									List<MudVolumes> GroupMudVolumesLists;
									String[] paramsFields111 = {"operationUid","getMudVolumesUid","mudVolumeCode"};
									Object[] paramsValues111 = {operationUid,getMudVolumesUid,mudVolumeCode};
									String GroupMudVolumesQuery = "SELECT sum(mvd.volume),mvd.type,mv.dailyUid FROM MudVolumes mv, MudVolumeDetails mvd WHERE (mv.isDeleted is null OR mv.isDeleted = false) and (mvd.isDeleted is null OR mvd.isDeleted = false) and mv.mudVolumesUid=mvd.mudVolumesUid and mv.operationUid =:operationUid  and mv.mudVolumesUid=:getMudVolumesUid and mvd.type=:mudVolumeCode Group By mvd.type Order By mvd.type";
									GroupMudVolumesLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(GroupMudVolumesQuery, paramsFields111, paramsValues111);					
									
									if(GroupMudVolumesLists != null && !GroupMudVolumesLists.isEmpty()){
										
										for(Object objResultMudVolumes1: GroupMudVolumesLists){							
											Object[] objRstmudvolume1 = (Object[]) objResultMudVolumes1;
											if (objRstmudvolume1[0] != null){
												mvdVolume= Double.parseDouble(nullToEmptyString(objRstmudvolume1[0]));
												mvdVolumeConverter.setBaseValue(mvdVolume);
												items.addAttribute("Volume",mvdVolumeConverter.formatOutputPrecision());
												
											}
											
										}
									}
									else {
										mvdVolume=0.0;
										mvdVolumeConverter.setBaseValue(mvdVolume);
										items.addAttribute("Volume", mvdVolumeConverter.formatOutputPrecision());
									}
									
									
									items.addAttribute("Label", "dummy_totals");									
									items.addAttribute("Type", nullToEmptyString(catShortCode));									
									writer.addElement("mudvoldetails", "", items);
									
								}
								writer.endElement(); // end mudvolumeUid
								
								writer.endElement(); // end Day
							}
							
							
						}
						if(mudVolumneUid.endsWith(",")) mudVolumneUid = mudVolumneUid.substring(0,mudVolumneUid.length() - 1);
						if(StringUtils.isBlank(mudVolumneUid)) mudVolumneUid = "''";
						Double mvdVolume;
						
						for (Map.Entry<String, LookupItem> entry : catList.entrySet())
						{
							String catShortCode = entry.getKey();
							
							Map<String,LookupItem> subCatList = LookupManager.getConfiguredInstance().getCascadeLookup(sublookup, new UserSelectionSnapshot(this.getCurrentUserSession()),catShortCode , null);
							
							//TOTAL VOLUME MUD VOLUME DETAIL EACH SUB CATEGORY
							for (Map.Entry<String,LookupItem> entrySub : subCatList.entrySet())
							{
								items = new SimpleAttributes();
								String subCatShortCode = entrySub.getKey();
								
								List<MudVolumes> GroupMudVolumesLists1;
								String[] paramsFields1112 = {"operationUid","subCatShortCode"};
								Object[] paramsValues1112 = {operationUid,subCatShortCode};
															
								String GroupMudVolumesQuery = "SELECT sum(mvd.volume),mvd.label FROM MudVolumes mv, MudVolumeDetails mvd WHERE (mv.isDeleted is null OR mv.isDeleted = false) and (mvd.isDeleted is null OR mvd.isDeleted = false) and mv.mudVolumesUid=mvd.mudVolumesUid and mv.operationUid =:operationUid and mvd.label=:subCatShortCode and mv.mudVolumesUid in ("+mudVolumneUid+") Group By mvd.label Order By mvd.type";
								GroupMudVolumesLists1   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(GroupMudVolumesQuery, paramsFields1112, paramsValues1112);								
								
								if(GroupMudVolumesLists1 != null && !GroupMudVolumesLists1.isEmpty()){
									for(Object objResultMudVolumes1: GroupMudVolumesLists1){							
										Object[] objRstmudvolume1 = (Object[]) objResultMudVolumes1;
										
										if (objRstmudvolume1[1] != null){
											items.addAttribute("Type", nullToEmptyString(catShortCode));
										}
										if (objRstmudvolume1[1] != null){
											items.addAttribute("Label", nullToEmptyString(subCatShortCode));
										}
										if (objRstmudvolume1[0] != null){
											mvdVolume= Double.parseDouble(nullToEmptyString(objRstmudvolume1[0]));
											mvdVolumeConverter.setBaseValue(mvdVolume);
											items.addAttribute("Volume",mvdVolumeConverter.formatOutputPrecision());
										}
									}
								}
								else {
									items.addAttribute("Type", nullToEmptyString(catShortCode));
									items.addAttribute("Label", nullToEmptyString(subCatShortCode));
									mvdVolume=0.0;
									mvdVolumeConverter.setBaseValue(mvdVolume);
									items.addAttribute("Volume", mvdVolumeConverter.formatOutputPrecision());
									
								}
								writer.addElement("TotalVolumes", "", items);
							}
							
							//TOTAL VOLUME FOR EACH GROUP (e.g vol, addition, loss)
							items = new SimpleAttributes();
							List<MudVolumes> GroupMudVolumesLists1;
							String[] paramsFields1112 = {"operationUid","catShortCode"};
							Object[] paramsValues1112 = {operationUid,catShortCode};
														
							String GroupMudVolumesQuery = "SELECT sum(mvd.volume),mvd.type FROM MudVolumes mv, MudVolumeDetails mvd WHERE (mv.isDeleted is null OR mv.isDeleted = false) and (mvd.isDeleted is null OR mvd.isDeleted = false) and mv.mudVolumesUid=mvd.mudVolumesUid and mv.operationUid =:operationUid and mvd.type=:catShortCode and mv.mudVolumesUid in ("+mudVolumneUid+") Group By mvd.type Order By mvd.type";
							GroupMudVolumesLists1   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(GroupMudVolumesQuery, paramsFields1112, paramsValues1112);								
							
							if(GroupMudVolumesLists1 != null && !GroupMudVolumesLists1.isEmpty()){
								for(Object objResultMudVolumes1: GroupMudVolumesLists1){							
									Object[] objRstmudvolume1 = (Object[]) objResultMudVolumes1;
									
									if (objRstmudvolume1[1] != null){
										items.addAttribute("Type", nullToEmptyString(catShortCode));
									}
									if (objRstmudvolume1[1] != null){
										items.addAttribute("Label", nullToEmptyString(catShortCode));
									}
									if (objRstmudvolume1[0] != null){
										mvdVolume= Double.parseDouble(nullToEmptyString(objRstmudvolume1[0]));
										mvdVolumeConverter.setBaseValue(mvdVolume);
										items.addAttribute("Volume",mvdVolumeConverter.formatOutputPrecision());
										
									}
								}
							}
							else {
								items.addAttribute("Type", nullToEmptyString(catShortCode));
								items.addAttribute("Label", nullToEmptyString(catShortCode));
								mvdVolume=0.0;
								mvdVolumeConverter.setBaseValue(mvdVolume);
								items.addAttribute("Volume", mvdVolumeConverter.formatOutputPrecision());
							}
							writer.addElement("TotalVolumes", "", items);
						}
						writer.endElement(); // end operation
					}
				}
				
			writer.endElement(); // end root
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	//Similar function as Query Bit By Hole size in HTML version
	public String queryBitByHoleSize_forOSL(String[] operations){
		String result = "<root/>";
		try
		{
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
			
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			String operationsuidlist = "";
			for (String operationUid : operations) {
				if(operationsuidlist=="")
					operationsuidlist= "'" + operationUid + "'";
				else
					operationsuidlist+=",'" + operationUid + "'";
			}
			
			String bitRunDiameterQuery = "SELECT distinct br.bitDiameter FROM Bitrun br WHERE (br.isDeleted = false or br.isDeleted is null) AND br.operationUid IN("+operationsuidlist+") ORDER BY br.bitDiameter DESC";
			List <Double> bitRunDiameter = ApplicationUtils.getConfiguredInstance().getDaoManager().find(bitRunDiameterQuery,qp);	
			
			Map<String, Object> bitRunDiameterLists = new LinkedHashMap<String, Object>();
			for (Double bitDiameterRec : bitRunDiameter) {
				if (result!=null)
				{	
					DecimalFormat Decimalformat = new DecimalFormat("#0.000000");
					String bitDiameter=CommonUtils.roundUpFormat(Decimalformat, Double.parseDouble(nullToEmptyString(bitDiameterRec)));
					
					Map<String, Object> OperationUidMap = new LinkedHashMap<String, Object>();
					for (String operationUid : operations) {
						String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
						
						List<String> bitrunUidList;
						String[] paramsFields = {"operationUid","bitDiameter"};
						Object[] paramsValues = {operationUid,Double.parseDouble(bitDiameter)};
						
						String bitRunQuery = "SELECT distinct br.bitrunUid from Bitrun br, Bharun bha WHERE (br.isDeleted = false or br.isDeleted is null) AND (bha.isDeleted = false or bha.isDeleted is null)  AND br.operationUid=:operationUid AND br.bitDiameter=:bitDiameter AND br.bharunUid=bha.bharunUid ORDER BY br.depthInMdMsl,br.depthOutMdMsl";
						bitrunUidList   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitRunQuery, paramsFields, paramsValues, qp);		
						Map<String, Object> bitrunUidMap = new LinkedHashMap<String, Object>();
						
						if(bitrunUidList.size()>0){
							for(String bitrunUid : bitrunUidList){
								
								List<String> bhadailyList;
								String[] bhadailyparamsFields = {"operationUid","bitrunUid","reportType"};
								Object[] bhadailyparamsValues = {operationUid,bitrunUid,reportType};
								
								String bhadailyQuery = "SELECT distinct bds.dailyUid from Bitrun br, Bharun bha,BharunDailySummary bds, Daily d, ReportDaily rd WHERE (d.dailyUid=bds.dailyUid) AND (rd.dailyUid=bds.dailyUid) AND (d.dailyUid=rd.dailyUid) AND (rd.reportType=:reportType) AND (d.isDeleted is null or d.isDeleted=false) AND (rd.isDeleted is null or rd.isDeleted=false) AND (br.isDeleted = false or br.isDeleted is null) AND (bha.isDeleted = false or bha.isDeleted is null)  AND br.operationUid=:operationUid AND br.bitrunUid=:bitrunUid AND br.bharunUid=bha.bharunUid AND bha.bharunUid=bds.bharunUid ORDER BY rd.reportDatetime";
								bhadailyList   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bhadailyQuery, bhadailyparamsFields, bhadailyparamsValues, qp);		
								Map<String, Object> DailyUidMap = new LinkedHashMap<String, Object>();
								if (bhadailyList.size()>0) {							
									for (String dailyUid : bhadailyList) {
										List<String> activityList;
										String[] ActivityparamsFields = {"operationUid","dailyUid"};
										Object[] ActivityparamsValues = {operationUid,dailyUid};
										
										String ActivityQuery = "SELECT a.activityUid from Activity a WHERE (a.isDeleted = false or a.isDeleted is null) AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) AND (isOffline IS NULL OR isOffline=false)AND a.operationUid=:operationUid AND a.dailyUid=:dailyUid ORDER BY a.startDatetime";
										activityList   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ActivityQuery, ActivityparamsFields, ActivityparamsValues, qp);		
										
										List<String> arrayActivityList = new ArrayList<String>();
										if(activityList.size() > 0) {
											for(String activityUid: activityList) {
												arrayActivityList.add(activityUid);
											}
											DailyUidMap.put(dailyUid,arrayActivityList);
										}
									}
									if (DailyUidMap.size() > 0) bitrunUidMap.put(bitrunUid,DailyUidMap);
								}
							}
							if (bitrunUidMap.size() > 0) OperationUidMap.put(operationUid,bitrunUidMap);
						}
					}
					bitRunDiameterLists.put(nullToEmptyString(bitDiameter), OperationUidMap);
				}
			}
			
			for (Map.Entry<String, Object>entryBitDiameter : bitRunDiameterLists.entrySet()) {
				String bitSizeLookup="xml://bitsize?key=code&amp;value=label"; 												
				Map<String,LookupItem> bitSizeMapList = LookupManager.getConfiguredInstance().getLookup(bitSizeLookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
				
				String bitDiameter="";
				if (bitSizeMapList.containsKey(entryBitDiameter.getKey())) {
					bitDiameter = bitSizeMapList.get(entryBitDiameter.getKey()).getValue().toString();
				} else {
					bitDiameter = entryBitDiameter.getKey();
				}
				
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("bitDiameter", bitDiameter);
				writer.startElement("Bit",attr);
				
				Map<String, Object> OperationMapList = new LinkedHashMap<String, Object>();
				OperationMapList=(Map<String, Object>) entryBitDiameter.getValue();
				Double countTotalDuration=0.0;
				Integer i = 0;
				for (Map.Entry<String, Object>entryOperation : OperationMapList.entrySet()) //key = operation
				{	
					String operationUid = entryOperation.getKey();
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
					String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid);
					
					CustomFieldUom depthInConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "depthInMdMsl",operationUomContext);
					CustomFieldUom depthOutConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "depthOutMdMsl",operationUomContext);
					CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration",operationUomContext);
					CustomFieldUom depthMdMslConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "depthMdMsl",operationUomContext);
					
					attr = new SimpleAttributes();
					attr.addAttribute("fullWellName", fullWellName);
					attr.addAttribute("operationUid", operationUid);
					attr.addAttribute("counter", i.toString());
					writer.startElement("Operation",attr);
					
					Map<String, Object> BitMapList = new LinkedHashMap<String, Object>();
					BitMapList=(Map<String, Object>) entryOperation.getValue();
					
					for (Map.Entry<String, Object>entryBit : BitMapList.entrySet()) // key=Bit
					{
						String bitrunUid=entryBit.getKey();
						List<Bitrun> bitrunNumberLists;
						String[] bitrunNumberparamsFields = {"operationUid","bitrunUid"};
						Object[] bitrunNumberparamsValues = {operationUid,bitrunUid};
						String bitrunNumberQuery = "SELECT br.operationUid, br.bitrunNumber,br.depthInMdMsl,br.depthOutMdMsl FROM " +
								"Bitrun br WHERE (br.isDeleted = false or br.isDeleted is null) AND br.operationUid=:operationUid AND br.bitrunUid=:bitrunUid";
						bitrunNumberLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitrunNumberQuery, bitrunNumberparamsFields, bitrunNumberparamsValues, qp);
						
						for(Object objResultbitrunRecords: bitrunNumberLists){
							Object[] objResultbitrunRecord = (Object[]) objResultbitrunRecords;
							String bitdepthInMdMsl = "0.0";
							String bitdepthOutMdMsl = "0.0";
							String bitNumber="";
							if (objResultbitrunRecord[1]!=null) bitNumber = objResultbitrunRecord[1].toString();
							if (objResultbitrunRecord[2]!=null)	bitdepthInMdMsl = objResultbitrunRecord[2].toString();
							if (objResultbitrunRecord[3]!=null) bitdepthOutMdMsl = objResultbitrunRecord[3].toString();
							
							depthInConverter.setBaseValue(Double.parseDouble(bitdepthInMdMsl));
							bitdepthInMdMsl = depthInConverter.formatOutputPrecision();
							
							depthOutConverter.setBaseValue(Double.parseDouble(bitdepthOutMdMsl));
							bitdepthOutMdMsl = depthOutConverter.formatOutputPrecision();
							
							attr = new SimpleAttributes();
							attr.addAttribute("bitNumber", bitNumber);
							attr.addAttribute("bitdepthInMdMsl", bitdepthInMdMsl.toString());
							attr.addAttribute("bitdepthOutMdMsl", bitdepthOutMdMsl.toString());
						}
						writer.startElement("BitRun",attr);
						
						Map<String, Object> DailyMapList = new LinkedHashMap<String, Object>();
						DailyMapList=(Map<String, Object>) entryBit.getValue();
						
						for (Map.Entry<String, Object>entryDaily : DailyMapList.entrySet()) //key= daily
						{
							String dailyUid=entryDaily.getKey();
							
							List<ReportDaily> reportDailyLists;
							String[] reportDailyparamsFields = {"operationUid","dailyUid","reportType"};
							Object[] reportDailyparamsValues = {operationUid,dailyUid,reportType};
							String reportDailyQuery = "SELECT d.dayDate,d.dayNumber FROM Daily d, ReportDaily rd " +
									"WHERE d.dailyUid = rd.dailyUid AND (d.isDeleted = false or d.isDeleted is null) " +
									"AND (rd.isDeleted = false or rd.isDeleted is null) AND d.operationUid=:operationUid " +
									"AND d.dailyUid=:dailyUid AND rd.reportType=:reportType";
							reportDailyLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailyQuery, reportDailyparamsFields, reportDailyparamsValues, qp);
							
							for(Object objResultreportDailyRecords: reportDailyLists){
								Object[] objResultreportDailyRecord = (Object[]) objResultreportDailyRecords;
								Date dayDate = (Date) objResultreportDailyRecord[0];
								String strDayDate = "";
								String dayNumber = nullToEmptyString(objResultreportDailyRecord[1]);
								
								strDayDate = df.format(dayDate);
								
								String currentDayActivityDuration = "";
								List<String> activityList = new ArrayList<String>();							
								activityList=(List<String>) entryDaily.getValue();
								String activityUidList="";
								if(activityList.size() > 0) {
									for(String activityUid: activityList){
										if(activityUidList=="")
											activityUidList= "'" + activityUid + "'";
										else
											activityUidList+=",'" + activityUid + "'";
									}
									
									String activityQuery = "SELECT sum(a.activityDuration) as duration FROM Activity a " +
											"WHERE (a.isDeleted = false or a.isDeleted is null) " +
											"AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) " +
											"AND (isOffline IS NULL OR isOffline=false) " +
											"AND a.activityUid IN ("+activityUidList+")";
									List <Double> activityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().find(activityQuery, qp);
									
									for (Double activityresult : activityLists) {
										Double activityDuration = 0.0;
										activityDuration=Double.parseDouble(nullToEmptyString(activityresult));
										durationConverter.setBaseValue(activityDuration);
										currentDayActivityDuration = durationConverter.formatOutputPrecision();
										countTotalDuration = countTotalDuration+Double.parseDouble(durationConverter.formatOutputPrecision());
									}
								}
								attr = new SimpleAttributes();
								attr.addAttribute("dayDate", strDayDate);
								attr.addAttribute("dayNumber", dayNumber);
								attr.addAttribute("currentDayActivityDuration", currentDayActivityDuration);
								writer.startElement("Day", attr);
								
								List<String> ActivityList = new ArrayList<String>();							
								ActivityList=(List<String>) entryDaily.getValue();
								activityUidList="";
								if(ActivityList.size() > 0) {
									for(String activityUid: ActivityList){
										if(activityUidList=="")
											activityUidList= "'" + activityUid + "'";
										else
											activityUidList+=",'" + activityUid + "'";
									}
									String ActivityQuery = "SELECT a.operationUid, a.startDatetime, a.endDatetime, a.classCode, a.phaseCode, a.jobTypeCode, " +
											"a.taskCode, a.rootCauseCode, a.depthMdMsl, a.activityDuration,a.activityDescription " +
											"FROM Activity a WHERE (a.isDeleted = false or a.isDeleted is null) " +
											"AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) " +
											"AND (isOffline IS NULL OR isOffline=false)AND a.activityUid IN ("+activityUidList+")";
									List<Object[]> ActivityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().find(ActivityQuery, qp);
									for(Object objResultActivityRecords: ActivityLists){
										Object[] objResultActivityRecord = (Object[]) objResultActivityRecords;
										Date startDatetime = null;
										Date endDatetime = null;
										String classCode="";
										String phaseCode="";
										String jobTypeCode="";
										String taskCode="";
										String rootCauseCode="";
										Double depthMdMsl=0.0;
										Double activityDuration=0.0;
										String activityDescription="";
										String strStartDateTime = "";
										String strEndDateTime = "";
										String strDepthMdMsl = "";
										String strActivityDuration = "";
										
										if (objResultActivityRecord[1]!=null) {
											startDatetime = (Date) objResultActivityRecord[1];
											strStartDateTime = tf.format(startDatetime);
										}
										if (objResultActivityRecord[2]!=null) {
											endDatetime = (Date) objResultActivityRecord[2];
											strEndDateTime = tf.format(endDatetime);
										}
										if (objResultActivityRecord[3]!=null) classCode = nullToEmptyString(objResultActivityRecord[3]);
										if (objResultActivityRecord[4]!=null) phaseCode = nullToEmptyString(objResultActivityRecord[4]);
										if (objResultActivityRecord[5]!=null) jobTypeCode = nullToEmptyString(objResultActivityRecord[5]);
										if (objResultActivityRecord[6]!=null) taskCode = nullToEmptyString(objResultActivityRecord[6]);
										if (objResultActivityRecord[7]!=null) rootCauseCode = nullToEmptyString(objResultActivityRecord[7]);
										if (objResultActivityRecord[8]!=null) {
											depthMdMsl = Double.parseDouble(objResultActivityRecord[8].toString());
											depthMdMslConverter.setBaseValue(depthMdMsl);
											strDepthMdMsl = depthMdMslConverter.formatOutputPrecision();
										}
										if (objResultActivityRecord[9]!=null) {
											activityDuration = Double.parseDouble(nullToEmptyString(objResultActivityRecord[9]));
											durationConverter.setBaseValue(activityDuration);
											strActivityDuration = durationConverter.formatOutputPrecision();
										}
										if (objResultActivityRecord[10]!=null) activityDescription = nullToEmptyString(objResultActivityRecord[10]);
										
										attr = new SimpleAttributes();
										attr.addAttribute("startDatetime", strStartDateTime);
										attr.addAttribute("endDatetime", strEndDateTime);
										attr.addAttribute("depthMdMsl", strDepthMdMsl);
										attr.addAttribute("activityDuration", strActivityDuration);
										attr.addAttribute("activityDescription", activityDescription);
										writer.startElement("Activity", attr);
										writer.addElement("classCode", classCode);
										writer.addElement("phaseCode", phaseCode);
										writer.addElement("jobTypeCode", jobTypeCode);
										writer.addElement("taskCode", taskCode);
										writer.addElement("rootCauseCode", rootCauseCode);
										writer.endElement(); //end Activity Start Element
									}
								}
								
								writer.endElement();//end Day Start Element
							}
						}
						writer.endElement(); //end BITRUN Start Element
					}
					i++;
					writer.addElement("depthInUom", "("+depthInConverter.getUomSymbol()+")");
					writer.addElement("depthOutUom", "("+depthOutConverter.getUomSymbol()+")");
					writer.addElement("durationUom", "("+durationConverter.getUomSymbol()+")");
					writer.addElement("depthUom", "("+depthMdMslConverter.getUomSymbol()+")");
					writer.endElement();//end Operation Start Element
				}
				writer.addElement("TotalDurationByBit", nullToEmptyString(countTotalDuration));
				writer.addElement("TotalOperation", i.toString());
				writer.endElement();// end Bit Start Element
			}
			
			writer.endElement();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			 e.printStackTrace();
			 result = "<root/>";
		}	
		System.out.println(result);
		return result;
	}
	
	public String queryActivityClassCodesBreakdownByOperationForOSL(String[] operations) throws Exception{
		
		String result = "<root/>";
		try{
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			//Variables
			double TotalP = 0.0;
			double TotalTP = 0.0;
			double TotalTU = 0.0;
			double TotalU = 0.0;
			double TotalHours = 0.0;
			
			String totalPDuration = "";			
			String totalTPDuration = "";
			String totalTUDuration = "";			
			String totalUDuration = "";			
			String totalDuration = "";
			String totalPDuration_raw = "";
			String totalTPDuration_raw = "";
			String totalTUDuration_raw = "";
			String totalUDuration_raw = "";
			String totalDuration_raw = "";
			String durationUom = "";
			//Main Container
			Map<String, Object> ActivityDurationList = new HashMap<String, Object>();
			
			for (String operationUid : operations) {
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				
				Map<String, Object> ActivityDurationListDetail = new HashMap<String, Object>();
				
				//Declaration
				String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);	
				operationUomContext.setOperationUid(operationUid);
				CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration",operationUomContext);
				
				double LineTotal = 0.0;
				double OpsTotalP = 0.0;
				double OpsTotalTP = 0.0;
				double OpsTotalTU = 0.0;
				double OpsTotalU = 0.0;
				
				durationUom = durationConverter.getUomSymbol();
				
				//Duration Breakdown 
				String activityQuery = "SELECT a.operationUid,a.internalClassCode,sum(a.activityDuration) AS actDuration, o.operationName " +
						"FROM Activity a, Operation o WHERE (a.isDeleted = false or a.isDeleted is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND (a.isOffline=0 or a.isOffline is null) AND (a.isSimop=0 or a.isSimop is null) " +
						"AND a.operationUid=o.operationUid " +
						"AND o.operationUid=:operationUid " +
						"AND (a.internalClassCode is not null AND a.internalClassCode <> '') " +
						"GROUP BY a.internalClassCode,a.operationUid ORDER BY o.operationName";		
				List <Object[]> ActivityDurationDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
				for (Object[] ActivityDurationDetailobj:ActivityDurationDetail){
					
					ActivityDurationList.put(operationUid, ActivityDurationListDetail);	
					ActivityDurationListDetail.put("operationName",nullToEmptyString(fullWellName));
					
					Double durationValue;
					durationValue= Double.parseDouble(nullToEmptyString(ActivityDurationDetailobj[2]));
						
					if ("P".equals(ActivityDurationDetailobj[1])){
						if (ActivityDurationDetailobj[2] != null){
							durationConverter.setBaseValue(durationValue);
							ActivityDurationListDetail.put("actDurationP", durationConverter.formatOutputPrecision());
							ActivityDurationListDetail.put("actDurationP_raw", durationConverter.getConvertedValue());
							OpsTotalP = OpsTotalP + (durationValue);
						}
					}
					
					if ("TP".equals(ActivityDurationDetailobj[1])){
						if (ActivityDurationDetailobj[2] != null){
							durationConverter.setBaseValue(durationValue);
							ActivityDurationListDetail.put("actDurationTP", durationConverter.formatOutputPrecision());
							ActivityDurationListDetail.put("actDurationTP_raw", durationConverter.getConvertedValue());
							OpsTotalTP = OpsTotalTP + durationValue;
						}
					}
					
					if ("U".equals(ActivityDurationDetailobj[1])){
						if (ActivityDurationDetailobj[2] != null){
							durationConverter.setBaseValue(durationValue);
							ActivityDurationListDetail.put("actDurationU", durationConverter.formatOutputPrecision());
							ActivityDurationListDetail.put("actDurationU_raw", durationConverter.getConvertedValue());
							OpsTotalU = OpsTotalU + durationValue;
						}
					}
					
					if ("TU".equals(ActivityDurationDetailobj[1])){
						if (ActivityDurationDetailobj[2] != null){
							durationConverter.setBaseValue(durationValue);
							ActivityDurationListDetail.put("actDurationTU", durationConverter.formatOutputPrecision());
							ActivityDurationListDetail.put("actDurationTU_raw", durationConverter.getConvertedValue());
							OpsTotalTU = OpsTotalTU + durationValue;
						}
					}
					ActivityDurationList.put(operationUid, ActivityDurationListDetail);	
				}
				LineTotal = OpsTotalP + OpsTotalTP + OpsTotalU + OpsTotalTU;
				TotalP = TotalP + OpsTotalP;
				TotalTP = TotalTP + OpsTotalTP;
				TotalTU = TotalTU + OpsTotalU;
				TotalU = TotalU + OpsTotalTU;
				TotalHours = TotalHours + LineTotal;
				durationConverter.setBaseValue(LineTotal);
				Double percentageP = 0.0;
				Double percentageTrouble = 0.0;
				Double percentageU = 0.0;
				if (LineTotal != 0) {
					percentageP = (OpsTotalP / LineTotal) * 100;
					percentageTrouble = ((OpsTotalTP + OpsTotalTU) / LineTotal) * 100;
					percentageU = (OpsTotalU / LineTotal) * 100;
				}
				 
				ActivityDurationListDetail.put("totalByOperation", durationConverter.formatOutputPrecision());
				ActivityDurationListDetail.put("totalByOperation_raw", durationConverter.getConvertedValue());
				ActivityDurationListDetail.put("durationUom", durationUom);
				ActivityDurationListDetail.put("percentageP", formatter.format(percentageP));
				ActivityDurationListDetail.put("percentageTrouble", formatter.format(percentageTrouble));
				ActivityDurationListDetail.put("percentageU", formatter.format(percentageU));
				
				durationConverter.setBaseValue(TotalP);
				totalPDuration = durationConverter.formatOutputPrecision();
				totalPDuration_raw = nullToEmptyString(durationConverter.getConvertedValue());
				durationConverter.setBaseValue(TotalTP);
				totalTPDuration = durationConverter.formatOutputPrecision();
				totalTPDuration_raw = nullToEmptyString(durationConverter.getConvertedValue());
				durationConverter.setBaseValue(TotalU);
				totalTUDuration = durationConverter.formatOutputPrecision();
				totalTUDuration_raw = nullToEmptyString(durationConverter.getConvertedValue());
				durationConverter.setBaseValue(TotalTU);
				totalUDuration = durationConverter.formatOutputPrecision();
				totalUDuration_raw = nullToEmptyString(durationConverter.getConvertedValue());
				durationConverter.setBaseValue(TotalHours);
				totalDuration = durationConverter.formatOutputPrecision();
				totalDuration_raw = nullToEmptyString(durationConverter.getConvertedValue());
			}
			
			List<Object[]> mapList = new ArrayList();
			for (Map.Entry<String, Object>item : ActivityDurationList.entrySet()) {
				mapList.add(new Object[] {item.getKey(), item.getValue()});
			}
			Collections.sort(mapList, new operationComparator());
			for(Iterator i = mapList.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				Map<String, Object> ActivityDurationDetail = (Map<String, Object>)obj_array[1];
				
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("fullWellName", nullToEmptyString(ActivityDurationDetail.get("operationName")));
				attr.addAttribute("actDurationP", nullToEmptyString(ActivityDurationDetail.get("actDurationP")));
				attr.addAttribute("actDurationTP", nullToEmptyString(ActivityDurationDetail.get("actDurationTP")));
				attr.addAttribute("actDurationU", nullToEmptyString(ActivityDurationDetail.get("actDurationU")));
				attr.addAttribute("actDurationTU", nullToEmptyString(ActivityDurationDetail.get("actDurationTU")));
				attr.addAttribute("totalByOperation", nullToEmptyString(ActivityDurationDetail.get("totalByOperation")));
				attr.addAttribute("actDurationP_raw", nullToEmptyString(ActivityDurationDetail.get("actDurationP_raw")));
				attr.addAttribute("actDurationTP_raw", nullToEmptyString(ActivityDurationDetail.get("actDurationTP_raw")));
				attr.addAttribute("actDurationU_raw", nullToEmptyString(ActivityDurationDetail.get("actDurationU_raw")));
				attr.addAttribute("actDurationTU_raw", nullToEmptyString(ActivityDurationDetail.get("actDurationTU_raw")));
				attr.addAttribute("totalByOperation_raw", nullToEmptyString(ActivityDurationDetail.get("totalByOperation_raw")));
				attr.addAttribute("durationUom", nullToEmptyString(ActivityDurationDetail.get("durationUom")));
				attr.addAttribute("percentageP", nullToEmptyString(ActivityDurationDetail.get("percentageP")));
				attr.addAttribute("percentageTrouble", nullToEmptyString(ActivityDurationDetail.get("percentageTrouble")));
				attr.addAttribute("percentageU", nullToEmptyString(ActivityDurationDetail.get("percentageU")));
				writer.startElement("datagrid",attr);
				
				writer.endElement();
			}
			
			SimpleAttributes totalAttr = new SimpleAttributes();
			totalAttr.addAttribute("totalP", totalPDuration);
			totalAttr.addAttribute("totalTP", totalTPDuration);
			totalAttr.addAttribute("totalTU", totalTUDuration);
			totalAttr.addAttribute("totalU", totalUDuration);
			totalAttr.addAttribute("totalDuration", totalDuration);
			totalAttr.addAttribute("totalP_raw", totalPDuration_raw);
			totalAttr.addAttribute("totalTP_raw", totalTPDuration_raw);
			totalAttr.addAttribute("totalTU_raw", totalTUDuration_raw);
			totalAttr.addAttribute("totalU_raw", totalUDuration_raw);
			totalAttr.addAttribute("totalDuration_raw", totalDuration_raw);
			totalAttr.addAttribute("durationUom", durationUom);
			writer.startElement("total", totalAttr);
			
			writer.close();
			result = bytes.toString("utf-8");
		}catch (Exception e){
			 e.printStackTrace();
			 result = "<root/>";
		}	
		return result;
	}
}
