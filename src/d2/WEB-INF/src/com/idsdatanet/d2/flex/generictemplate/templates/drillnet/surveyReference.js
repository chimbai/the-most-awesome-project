(function(window){
	
	D3.inherits(SurveyReferenceGraphLink, D3.AbstractFieldComponent);
	
	function SurveyReferenceGraphLink(id) {
		D3.AbstractFieldComponent.call(this);
	}
	
	SurveyReferenceGraphLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var surveyReferenceUid = this._node.getFieldValue("surveyReferenceUid");
			var type = "";
			if(this._config.customField)
				type = this._config.customField;
			var label = D3.LanguageManager.getLabel("graphlink.button.defaultlabel");
			if(this._config.label)
				label = this._config.label;
			
			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				css:"surveyReference-content div",
				context:this
			});
			this._linkButton = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("graphlink.button.title")
				},
				data:{
					surveyReferenceUid:surveyReferenceUid,
					type:type
				},
				callbacks:{
					onclick:function(event) {
						var _graphImage = D3.UI.createElement({
							tag:"img",
							attributes:{
								// w x h from flash equivalent
								width:"640",
								height:"480",
								src: this._node.commandBeanProxy.baseUrl + "graph.html?type=" + type + "&size=medium&uid=" + escape(this._node.getFieldValue("surveyReferenceUid")) + "&t=" + new Date().getTime()
							},
							context:this
						});
						var _popUpDiv = "<div></div>";
						this._popUpDialog = $(_popUpDiv);
						this._popUpDialog.append(_graphImage);
						this._popUpDialog.dialog({
							height:"auto",
							width:"660", // same as flash equivalent
							resizable:false,
							modal:true,
							title:D3.LanguageManager.getLabel("popup.graph.title")
						});
					}
				},
				context:this
			});
			this._fieldRenderer.appendChild(this._linkButton);
		}
		return this._fieldRenderer
	}
	
	SurveyReferenceGraphLink.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(WITSMLUpdateImport, D3.AbstractFieldComponent);

	function WITSMLUpdateImport(id) {
	    D3.AbstractFieldComponent.call(this);
	}

	WITSMLUpdateImport.prototype.getFieldRenderer = function() {
		if(!this._node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
        	return null;
        }
		if (!this._fieldRenderer) {
	        var surveyReferenceUid = this._node.getFieldValue("surveyReferenceUid");
	        var importParameters = this._node.getFieldValue("importParameters");
	        var cssDisplay = importParameters ? "" : "none";
	        var type = "";
	        
	        var buttonDeclaration = { 
					uid: surveyReferenceUid, 
					witsml: "SurveyReference",	
					refFields: "@schedulerTemplate"
				}; 	
			this.WITSMLImportButton = new D3.WITSMLImportButton(this._node.commandBeanProxy, buttonDeclaration, this._node);
	    
	        if(this._config.customField)
	            type = this._config.customField;
	        var label = D3.LanguageManager.getLabel("WITSML Update");
	        if(this._config.label)
	            label = this._config.label;
	        
	        this._fieldRenderer = D3.UI.createElement({
	            tag:"div",
	            css:"div",
	            context:this
	        });
	        
	        this._linkButton = D3.UI.createElement({
	            tag:"button",
	            text:label,
	            css:"DefaultButton",
	            attributes:{
	                title:D3.LanguageManager.getLabel("Get updates from WITSML server")
	            },
	            data:{
	                surveyReferenceUid:surveyReferenceUid,
	                type:type
	            },
	            callbacks:{
	                onclick:function(event) {
	                	this.WITSMLImportButton.doUpdateWitsml (surveyReferenceUid, importParameters);
	                }
	            },
	            context:this
	        });
            this._linkButton.style.display = cssDisplay;
	        this._fieldRenderer.appendChild(this._linkButton);
	    }
	    return this._fieldRenderer
	}

	WITSMLUpdateImport.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(SurveyReferenceNodeListener, D3.EmptyNodeListener);
	
	function SurveyReferenceNodeListener() {}
	
	SurveyReferenceNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		if (id == "re-calculate-survey-detail") {
			SurveyReferenceNodeListener.RECALCULATE_SURVEY_DETAIL = buttonDeclaration.label;
			var config = {
					tag:"button",
					text:SurveyReferenceNodeListener.RECALCULATE_SURVEY_DETAIL,
					css:"DefaultButton",
					context:this
			};
			this.btnSnapshot = D3.UI.createElement(config);
			return this.btnSnapshot;
		} else {
			return null;
		}
	}
	
	SurveyReferenceNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if (commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_NEW" || commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_UPDATE") {
			D3.UIManager.addCalllater(this.archive, this, [commandBeanProxy, commandBeanProxy.formSubmissionReason]);
		}
	};
	
	SurveyReferenceNodeListener.prototype.archive = function(commandBeanProxy, formSubmissionReason){
		var surveys = commandBeanProxy.rootNode.children["SurveyReference"];
		var nodes = surveys._items.slice(0, surveys._items.length);
		var editObjPrimaryKeys = [];
		for (var i = 0; i < nodes.length; i++){
			if(nodes[i].atts.editMode){
				editObjPrimaryKeys.push(nodes[i].primaryKeyValue);
			}
		}
		for (var i = 0; i < nodes.length; i++)
		{
			if(!nodes[i].atts.editMode ){
				if ((formSubmissionReason == "WITSML_IMPORT_NEW" && editObjPrimaryKeys.length > 0 && editObjPrimaryKeys.indexOf(nodes[i].primaryKeyValue) != -1) || formSubmissionReason == "WITSML_IMPORT_UPDATE")
					surveys.archive(nodes[i]);
			}
		}
		if (surveys._items.length == 0) surveys.restore();
	};
	
	SurveyReferenceNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "re-calculate-survey-detail") {
			for (var i=0; i<node.children["SurveyStation"].getLength(); i++) {
				var child = node.children["SurveyStation"].getItems()[i];
				var depthMdMsl = child.getFieldValue("depthMdMsl");
				var inclinationAngle = child.getFieldValue("inclinationAngle");
				var azimuthAngle = child.getFieldValue("azimuthAngle");
				if ((depthMdMsl && depthMdMsl != "") || (inclinationAngle && inclinationAngle != "") || (azimuthAngle && azimuthAngle != "")) {
					if (depthMdMsl == "" || inclinationAngle == "" || azimuthAngle == "") {
						alert("You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						return;
					}
				}
				child.setEditMode(true,false);
				child.setSubmitFlag(true);
			}
			node.commandBeanProxy.submitForServerSideProcess(node);
		}
	}
	
	SurveyReferenceNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		if (node.simpleClassName() == "SurveyStation" && parent.children["SurveyStation"].getLength() < 1) {
			node.setFieldValue("depthMdMsl", "0.0");
			node.setFieldValue("inclinationAngle", "0.0");
			node.setFieldValue("azimuthAngle", "0.0");
		}
	}
	
	SurveyReferenceNodeListener.prototype.getCustomFieldComponent = function(id, node) {
		if(id == "surveyReferenceTvdVsect") {
			return new SurveyReferenceGraphLink(id);
		} else if (id == "surveyReferenceNsEw" ) {
			return new SurveyReferenceGraphLink(id);
		} else if (id == "witsmlUpdateTrajectory" ) {
			return new WITSMLUpdateImport(id);
		} else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = SurveyReferenceNodeListener;
	
})(window);