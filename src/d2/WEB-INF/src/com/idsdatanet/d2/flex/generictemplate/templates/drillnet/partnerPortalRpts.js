(function(window){

	D3.inherits(ReportLinkField,D3.AbstractFieldComponent);
	
	function ReportLinkField(){
		D3.AbstractFieldComponent.call(this);
		this._enableZip = false;
		this._reportLink = null;
		this._zipButton = null;
		this._reportLabel = null;
		this._newIcon = null;
		
	}
	
	ReportLinkField.prototype.getNoOfReports = function(){
		var value =parseInt(this._node.getFieldValue("numberOfReport"));
		if (isNaN(value))
			return 0;
		return value;
	}
	
	ReportLinkField.prototype.reportLink_onClick = function(){
		var fileUrl = "abaccess/download/" + encodeURIComponent(D3.Common.replaceInvalidCharInFileName(this._node.getFieldValue("reportDisplayName"))) + "." + this._node.getFieldValue("fileExtension") + "?handlerId=ddr&reportId=" + encodeURIComponent(this._node.getFieldValue("reportFilesUid"));
		this._node.commandBeanProxy.openDownloadWindow(fileUrl);
	}
	ReportLinkField.prototype.zipButton_onClick = function(){
		var reportFilesUid = encodeURIComponent(this._node.getFieldValue("reportFilesUid"));
		var displayName = encodeURIComponent(D3.Common.replaceInvalidCharInFileName(this._node.getFieldValue("displayName")));
		var file_url = "abaccess/download/" + displayName + ".zip" + "?handlerId=ddr&zip=true&reportId=" + reportFilesUid + "&name=" + displayName + "." + this._node.getFieldValue("fileExtension");
		this._node.commandBeanProxy.openDownloadWindow(file_url);
	}
	
	ReportLinkField.prototype.refreshFieldRenderer = function(){
		$(this._reportLink).text(this.getFieldValue());
		
		if (this._node.getFieldValue("@newlyGeneratedFlag") == "1") {
			$(this._newIcon).removeClass("hide");
		}else{
			$(this._newIcon).addClass("hide");
		}
		var reportFilesUid = this._node.getFieldValue("reportFilesUid");
		var noOfReports = this.getNoOfReports();
		if (!reportFilesUid ||reportFilesUid === "" || !this._enableZip || noOfReports >1){
			$(this._zipButton).addClass("hide");
		}else{
			$(this._zipButton).removeClass("hide");
		}
		
		if (noOfReports>1){
			$(this._reportLink).addClass("hide");
			$(this._reportLabel).removeClass("hide");
			$(this._reportLabel).text(D3.LanguageManager.getLabel("label.noOfReports")+" "+noOfReports);
			$(this._newIcon).addClass("hide");
		}else{
			$(this._reportLabel).addClass("hide");
			$(this._reportLink).removeClass("hide");
		}
	}
	ReportLinkField.prototype.getFieldRenderer = function(){
		if (!this._renderer){
			
			var layout = "<div class='reportField'><a class='hyperlinkField reportLink'/>" +
				(this._enableZip?"<img class='zipIcon' title='Archived/Compressed file' alt=''/>":"")+
				"<button class='DefaultButton newReport'>New</button>" +
				"<span class='reportLabel hide'/></div>";
			var renderer = $(layout);
			
			this._reportLink = renderer.find(".reportLink").get(0);
			if (this.getNoOfReports()<2){
				this._reportLink.onclick = this.reportLink_onClick.bindAsEventListener(this);
			}
			if (this._enableZip){
				this._zipButton = renderer.find(".zipIcon").get(0);
				this._zipButton.onclick = this.zipButton_onClick.bindAsEventListener(this);
			}
			this._reportLabel = renderer.find(".reportLabel").get(0);
			this._newIcon = renderer.find(".newReport").get(0);
			
			this._renderer = renderer.get(0);
		}
		return this._renderer;
	}
	
	ReportLinkField.prototype.data = function(data){
		ReportLinkField.uber.data.call(this,data);
		this._enableZip = this._node.getGWP("enableZip") == "1";
	}
	
	
	D3.inherits(PartnetPortalRptListener,D3.EmptyNodeListener);
	
	function PartnetPortalRptListener()
	{
		
	}
	
	PartnetPortalRptListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "reportLink"){
			return new ReportLinkField();
		} 
	}
	
	PartnetPortalRptListener.prototype.downloadFile = function(reportDownloadPath) {
		var fileUrl = "abaccess/download/" + reportDownloadPath;
		this._commandBeanProxy.openDownloadWindow(fileUrl);
	}
	
	D3.PartnetPortalRptListener = PartnetPortalRptListener;
	D3.CommandBeanProxy.nodeListener = PartnetPortalRptListener;
})(window);