(function(window) {
    D3.inherits(ExcelImportTemplateNodeListener, D3.EmptyNodeListener);
    function ExcelImportTemplateNodeListener() {
        this.commandBeanProxy = null;
    }

    ExcelImportTemplateNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
        this.commandBeanProxy = commandBeanProxy;
        if (id == "import") {
            this._importBoxInit();
            var importBtn = document.createElement("button");
            importBtn.className = "rootButtons";
            importBtn.appendChild(document.createTextNode(D3.LanguageManager.getLabel("label.excelimport.button")));
            importBtn.onclick = this.onclickImport.bindAsEventListener(this);
            return importBtn;
        }
        return null;
    };
    
    ExcelImportTemplateNodeListener.prototype.showExcelImportWindow = function(){
        this.importBoxShow();
    };
    
    ExcelImportTemplateNodeListener.prototype._importBoxInit = function() {
        if(!this.box) {
            this.commandBeanProxy.rootNode.setFieldValue("@importTemplate", "");
            var _boxDiv = "<div class='popUpContainer'></div>";
            this.box = $(_boxDiv);
            this.importBoxBuild(this.box);
        }
    };
    
    ExcelImportTemplateNodeListener.prototype.importBoxShow = function() {
        this.box.dialog({
            width: "450",
            height: "130",
            title: D3.LanguageManager.getLabel("label.excelimport.title"),
            resizable: false,
            modal: true,
        });
        $(".ui-dialog-titlebar-close").show();
    };
    
    ExcelImportTemplateNodeListener.prototype.importBoxClose = function() {
        this.box.dialog("close");
        this.box = null;
        this._importBoxInit();
    };
    
    ExcelImportTemplateNodeListener.prototype.importBoxBuild = function(parent) {
        var tableCfg = "<table width='100%' height='100%'>" +
                "<tr>" +
                    "<td class='popUpLineOne' width='26%'/></td>" +
                    "<td class='popUpLineOne-BrowseButton' colspan='3'></td>" +
                "</tr>" +
                "<tr>" +
                    "<td class='popUpLineTwo-ImportTemplateLabel' width='26%'/>" +
                    "<td class='popUpLineTwo-ImportTemplateDropDown' colspan='2'/>" +
                    "<td class='popUpLineTwo-ImportButton' align='right'></td>" +
                "</tr>" +
                    "<tr>" +
                        "<td class='popUpLineThree-Text' colspan='4'></td>" +
                    "</tr>" +
                "</table>";
        parent.append(tableCfg);
        
        var labelText = D3.LanguageManager.getLabel("label.excelimport.select");
        var labelTemplate = D3.LanguageManager.getLabel("label.excelimport.template");
        
        var _importPopupLineOne = parent.find(".popUpLineOne").get(0);
        var _importPopupLineOneBrowseButton = parent.find(".popUpLineOne-BrowseButton").get(0);
        
        var _importPopupLineTwoImportTemplateLabel = parent.find(".popUpLineTwo-ImportTemplateLabel").get(0);
        this._importPopupLineTwoImportTemplateDropDown = parent.find(".popUpLineTwo-ImportTemplateDropDown").get(0);
        var _importPopupLineTwoImportButton = parent.find(".popUpLineTwo-ImportButton").get(0);
        
        var _importPopupLineThreeText = parent.find(".popUpLineThree-Text").get(0);
        
        this._excelImportLabelText = document.createElement("label");
        this._excelImportLabelText.appendChild(document.createTextNode(labelText));
        _importPopupLineOne.appendChild(this._excelImportLabelText);
        
        this.buildBrowseButton(_importPopupLineOneBrowseButton);
        
        this._excelImportLabelTemplate = document.createElement("label");
        this._excelImportLabelTemplate.appendChild(document.createTextNode(labelTemplate));
        _importPopupLineTwoImportTemplateLabel.appendChild(this._excelImportLabelTemplate);
        
        this.buildTemplateDropDown(this._importPopupLineTwoImportTemplateDropDown);
        this.buildImportButton(_importPopupLineTwoImportButton);
    };
    
    ExcelImportTemplateNodeListener.prototype.buildTemplateDropDown = function(container) {
        D3.UI.removeAllChildNodes(container);
        var lookupList = this.commandBeanProxy.rootNode.getLookup("@importTemplate");
        var select = document.createElement("select");
        var lookupCount = 0;
        select.id = "importTemplateSelect";
        $(select).on("change", {context: this}, this.onChangeTemplateDropDown);
        if(!lookupList)
            lookupList = new D3.LookupReference();
        D3.forEach(lookupList.getActiveLookup(), function(item) {
        	lookupCount++;
            option = document.createElement("option");
            option.setAttribute("value", item.data);
            option.appendChild(document.createTextNode(item.label));
            select.options.add(option);
        });
        var defaultTemplate = this.commandBeanProxy.commandBeanModel.template.root.customRootButtons.button.defaultTemplate;
        if(lookupCount == 2) {
        	select.selectedIndex = 1;
        	this.value = lookupList._items[1].data;
        	this.onChangeTemplateDropDown();
        } else if (defaultTemplate) {
        	for (var i = 1; i < lookupCount; i++) {
        		if (lookupList._items[i].data === defaultTemplate) {
        			select.selectedIndex = i;
        			this.value = lookupList._items[i].data;
                	this.onChangeTemplateDropDown();
                	break;
        		}
        	}
        }
        
        this.templateDropDownSelect = select;
        container.appendChild(this.templateDropDownSelect);
    };
    
    ExcelImportTemplateNodeListener.prototype.onChangeTemplateDropDown = function(e) {
    	var context = null;
    	if (e) {
	    	context = e.data.context;
	    } else if (this) {
	    	context = this;
	    }
    	if (context) {
    		context.selectedImportTemplate = this.value;
	    	context.commandBeanProxy.rootNode.setFieldValue("@importTemplate", this.value);
	        context.validateBrowseButton();
    	}
    };
    
    ExcelImportTemplateNodeListener.prototype.buildBrowseButton = function(container) {
        var that = this;
        var browseButtonConfig = {
                tag:"button",
                text: D3.LanguageManager.getLabel("label.excelimport.browse"),
                css:"DefaultButton",
                callbacks:{
                    onclick:this._browseButton_onClick
                },
                context:this
        };
        this._browseButton = D3.UI.createElement(browseButtonConfig);
        this._browseButton.setAttribute('style', 'cursor:default !important');
        this._browseButton.id = "browseButton";
        
        this._fileForm = document.createElement("form");
        this._fileForm.action = "upload";
        this._fileForm.method = "post";
        this._fileForm.id = "fileForm";
        this._fileForm.enctype = "multipart/form-data";
        
        this._fileUpload = document.createElement("input");
        this._fileUpload.type = "file";
        this._fileUpload.accept = ".xls, .xlsx, .xlsm";
        this._fileUpload.id = "fileUpload";
        this._fileUpload.style.display = "none";
        $(this._fileUpload).on("change", {context: this}, this.prepareUpload);
        
        this._fileForm.appendChild(this._fileUpload);
        container.appendChild(this._fileForm);
        container.appendChild(this._browseButton);
    };
    
    ExcelImportTemplateNodeListener.prototype.prepareUpload = function(event) {
        var context = event.data.context;
        context.fileToBeUploaded = event.target.files[0];
        context.fileSet = true;
        $(context._browseButton).text(context.fileToBeUploaded.name);
        if (context.selectedImportTemplate && context.selectedImportTemplate != "") {
        	context.commandBeanProxy.rootNode.setFieldValue("@importTemplate", context.selectedImportTemplate);
        }
        context.validateBrowseButton();
    }
    
    ExcelImportTemplateNodeListener.prototype.buildImportButton = function(container) {
        var importButtonConfig = {
                tag:"button",
                text:D3.LanguageManager.getLabel("button.import"),
                css:"DefaultButton",
                callbacks:{
                    onclick:this._importButton_onClick
                },
                context:this
        };
        this._importButton = D3.UI.createElement(importButtonConfig);
        this._importButton.setAttribute('style', 'cursor:default !important');
        this.enableButton(this._importButton, false);
        container.appendChild(this._importButton);
    };
    
    ExcelImportTemplateNodeListener.prototype.enableButton = function(button, value) {
        if (value) {
            $(button).removeAttr("disabled");
        } else {
            $(button).attr("disabled","disabled");
        }
    };
    
    ExcelImportTemplateNodeListener.prototype.validateBrowseButton = function() {
        this.selectedImportTemplate = this.getImportTemplate();
        if(this.selectedImportTemplate != null && this.selectedImportTemplate != "" && this.fileSet && this._fileUpload.files.length > 0) {
            this.enableButton(this._importButton, true);
        } else {
            this.enableButton(this._importButton, false);
        }
    };
    
    ExcelImportTemplateNodeListener.prototype.onclickImport = function() {
        this.showExcelImportWindow();
    };
    
    ExcelImportTemplateNodeListener.prototype._browseButton_onClick = function(){
        this._fileUpload.click();
    };
    
    ExcelImportTemplateNodeListener.prototype._importButton_onClick = function(){
    	this.selectedImportTemplate = this.getImportTemplate();
        var that = this;
        var page = this.commandBeanProxy.getCommandBeanUrl();
        
        var formData = new FormData();
        formData.append('_invokeCustomFilter', 'importTemplateExcel');
        formData.append('template', this.selectedImportTemplate);
        formData.append('file', this.fileToBeUploaded);
        
        $.ajax({
           url: page,
           data: formData,
           type: "POST",
           contentType: false,
           processData: false
        }).done(function(data, textStatus, jqXHR) {
            var response = JSON.parse(jqXHR.responseText);
            that.tempFilePath = response.tempFile;
            that.uploadCompleted(that.tempFilePath);
        }).fail(function( jqXHR, textStatus, errorThrown) {
            that.outputMessage(false, D3.LanguageManager.getLabel("label.excelimport.errormsg") + errorThrown);
        });
        that.importBoxClose();
    };
    
    ExcelImportTemplateNodeListener.prototype.uploadCompleted = function(tempFilePath) {
        this.commandBeanProxy.addAdditionalFormRequestParams("action", "importTemplateExcel");
        this.commandBeanProxy.addAdditionalFormRequestParams("template", this.selectedImportTemplate);
        this.commandBeanProxy.addAdditionalFormRequestParams("file", tempFilePath);
        this.commandBeanProxy.submitForServerSideProcess();
    };
    
    ExcelImportTemplateNodeListener.prototype.outputMessage = function(success, msg) {
        if(!success) {
            D3.UIManager.popup(D3.LanguageManager.getLabel("label.excelimport.error"), msg);
        }
    };
    
    ExcelImportTemplateNodeListener.prototype.getImportTemplate = function() {
        var importTemplate = $(this._importPopupLineTwoImportTemplateDropDown).find(":selected").attr('value');
        if (importTemplate == null) {
        	importTemplate = this.commandBeanProxy.rootNode.getFieldValue("@importTemplate");
        }
    	return importTemplate;
    };
    
    D3.ExcelImportTemplateNodeListener = ExcelImportTemplateNodeListener;
    D3.CommandBeanProxy.nodeListener = ExcelImportTemplateNodeListener;
    
})(window);