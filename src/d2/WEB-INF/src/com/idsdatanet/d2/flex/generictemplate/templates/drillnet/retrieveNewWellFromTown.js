(function(window){
	
	D3.inherits(RetrieveNewWellFromTownCustomField,D3.AbstractFieldComponent);
	function RetrieveNewWellFromTownCustomField(){
		D3.AbstractFieldComponent.call(this);
		this.currentPage = 0;
		this.clearWellList = true;
	}
	RetrieveNewWellFromTownCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.buildButton();
		}
		return this._fieldEditor;
	}
	RetrieveNewWellFromTownCustomField.prototype.buildButton = function() {
		var rnoftConfig = "<div id='container' class='ui-layout-pane ui-layout-pane-center' align='center'>" +
			"<table cellspacing='0' cellpadding='0' border='0'> "+
			"<tr><td class='HtmlTableFieldHeaderItem' width='500px'>" +
			"<span class='HtmlTableLabel'>Wells:<br>" +
			"<input class='wellName' style='width:434px;margin-top:3px;'> </input>" +
			"<button class='filter'>Filter</button>" +
			"</span></td>" +
			"</tr>"+
			"<tr><td><select class='wells' size='20' style='width: 100%'>" + 
			"</tr>"+
			"</table></div>";
		var table = $(rnoftConfig);
		this._wellName = table.find(".wellName").get(0);
		this._wellName.onkeypress = this.onWellNameEnter.bindAsEventListener(this);
		this._filterB = table.find(".filter").get(0);
		this._filterB.onclick = this.filter.bindAsEventListener(this);
		this._wells = table.find(".wells").get(0);
		this._wells.onchange = this.wellSelected.bindAsEventListener(this);
		this.showPopUp(true);
		this.retrieveWellList();

		this.showPopUp(false);
		return table.get(0);
	}	
	RetrieveNewWellFromTownCustomField.prototype.retrieveWellList = function(){
		var params = {};
		params._invokeCustomFilter = "flexRetrieveWells";
		params.wellName = this._wellName.value;
		params.page = this.currentPage;
		this._node.commandBeanProxy.customInvoke(params,this.wellListLoaded,this);
	}
	RetrieveNewWellFromTownCustomField.prototype.wellListLoaded = function(response) {
		var doc = $(response);
		var wellslist = this._wells;
		if (this.clearWellList) D3.UI.removeAllChildNodes(wellslist);
		else {
			this.clearWellList = true;
			wellslist.remove(wellslist.length-1);
		}
		var self = this;
		doc.find("Well").each(function(index,item){
			var data = $(this);
			var wellUid = data.find("wellUid").text();
			var name = data.find("name").text();
			if (wellUid){
				option = document.createElement("option");
				option.setAttribute("value", wellUid);
				option.appendChild(document.createTextNode(name));
				wellslist.appendChild(option);
			}
		},this);
		if (doc.get(0).firstChild.hasAttribute("hasNext") ){
			option = document.createElement("option");
			option.setAttribute("value", "__MORE__");
			option.style.color = "#999999";
			option.appendChild(document.createTextNode("Load More..."));
			wellslist.appendChild(option);
		}
	}

	RetrieveNewWellFromTownCustomField.prototype.onWellNameEnter = function(event){
		if (event.keyCode==13) {
			this.currentPage = 0;
			this.retrieveWellList();
		}
	}
	RetrieveNewWellFromTownCustomField.prototype.filter = function(){
		this.currentPage = 0;
		this.retrieveWellList();
	}
	RetrieveNewWellFromTownCustomField.prototype.wellSelected = function(){
		if ("__MORE__" == this._wells.value){
			this.currentPage ++;
			this.showPopUp(true);
			this.clearWellList = false;
			this.retrieveWellList();
			this.showPopUp(false);
			return;
		}
		this._node.setFieldValue("@selectedWellUid", this._wells.value);
	}
	RetrieveNewWellFromTownCustomField.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Loading data from town...</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	D3.inherits(RetrieveNewWellFromTownDataNodeListener,D3.EmptyNodeListener);
	
	function RetrieveNewWellFromTownDataNodeListener()
	{

	}
	
	RetrieveNewWellFromTownDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "WWODChooser"){
			return new RetrieveNewWellFromTownCustomField();
		}else{
			return RetrieveNewOperationFromTownDataNodeListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	RetrieveNewWellFromTownDataNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "retrieveNewWellFromTown"){
			this._commandBeanProxy = commandBeanProxy;
			var newRootBtn = document.createElement("button"); 
			newRootBtn.className = "rootButtons";
			newRootBtn.setAttribute("style","background-color:#E3E1B8");
			var labels = document.createTextNode("Retrieve New Well From Town");
			newRootBtn.appendChild(labels);
			newRootBtn.onclick = this.onClickRetrieveNewWellFromTown.bindAsEventListener(this);
			return newRootBtn;
		} else {
			return null;
		}
	}
	
	RetrieveNewWellFromTownDataNodeListener.prototype.onClickRetrieveNewWellFromTown = function(event) {
		this._commandBeanProxy.addAdditionalFormRequestParams("retrieveNewWellFromTown","1");
		this._commandBeanProxy.submitForServerSideProcess();
	}

	D3.CommandBeanProxy.nodeListener = RetrieveNewWellFromTownDataNodeListener;

})(window);