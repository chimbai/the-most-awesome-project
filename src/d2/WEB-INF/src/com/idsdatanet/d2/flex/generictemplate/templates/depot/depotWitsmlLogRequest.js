(function(window){

	D3.inherits(EnableDisableButton,D3.AbstractFieldComponent);
	
	function EnableDisableButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	EnableDisableButton.prototype.getFieldRenderer = function(){
		var isActive = this._node.getFieldValue("isActive");
		var isReprocess = this._node.getFieldValue("isReprocess");
		if (isReprocess=="1"){
			
		}else{
			if (isActive=="1"){
				this._fieldRenderer = D3.UI.createElement({
					tag:"button",
					text:"Disable",
					css:"RootButton RootButtonCancel",
					callbacks:{
						onclick:this.disable
					},
					context:this
				}) ;
			}else{
				this._fieldRenderer = D3.UI.createElement({
					tag:"button",
					text:"Enable",
					css:"RootButton RootButtonConfirm",
					callbacks:{
						onclick:this.enable
					},
					context:this
				}) ;
			}
		}		
		return this._fieldRenderer;
		
	}
	
	EnableDisableButton.prototype.enable = function(){
		var params = {};
		params._invokeCustomFilter = "setActiveOn";
		params.depotWitsmlLogRequestUid = this._node.getFieldValue("depotWitsmlLogRequestUid");
		if(params.depotWitsmlLogRequestUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.activeCompleted, this);
			
		}
	}
	
	EnableDisableButton.prototype.disable = function(){
		var params = {};
		params._invokeCustomFilter = "setActiveOff";
		params.depotWitsmlLogRequestUid = this._node.getFieldValue("depotWitsmlLogRequestUid");
		if(params.depotWitsmlLogRequestUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.activeCompleted, this);
		}
	}

	EnableDisableButton.prototype.activeCompleted = function(response) {
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	EnableDisableButton.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(DisableReprocessButton,D3.AbstractFieldComponent);
	
	function DisableReprocessButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	DisableReprocessButton.prototype.getFieldRenderer = function(){
		var isReprocess = this._node.getFieldValue("isReprocess");
		var username = this._node.getFieldValue("@username");
		if ((isReprocess=="1") && (username=="jwong")) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Disable",
				css:"RootButton RootButtonCancel",
				callbacks:{
					onclick:this.disable
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
		
	}
	
	DisableReprocessButton.prototype.disable = function(){
		var params = {};
		params._invokeCustomFilter = "setReprocessOff";
		params.depotWitsmlLogRequestUid = this._node.getFieldValue("depotWitsmlLogRequestUid");
		if(params.depotWitsmlLogRequestUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.activeCompleted, this);
		}
	}

	DisableReprocessButton.prototype.activeCompleted = function(response) {
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	DisableReprocessButton.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(FilterSelectionField, D3.AbstractFieldComponent);
	
	function FilterSelectionField() {
		D3.AbstractFieldComponent.call(this);
		this.hidefield = document.createElement("div");
		this.input1 = document.createElement("input");
		this.input1.setAttribute("type", "checkbox");
		this.input1.onclick = this.checkbox_onClick.bindAsEventListener(this);
		this.hidefield.appendChild(this.input1);
		this.hidefield.appendChild(document.createTextNode("Active  "));
	}
	
	FilterSelectionField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.hidefield;
			if (this._node.getFieldValue("@activefilter")=="0") {
				this.input1.checked=false;
				this._node.setFieldValue("@activefilter","0");
			} else {
				this.input1.checked=true;
				this._node.setFieldValue("@activefilter","1");
			}
		}
		return this._fieldEditor;
	};
	
	FilterSelectionField.prototype.checkbox_onClick = function(e){
		
		if (this.input1.checked){
			this._node.setFieldValue("@activefilter","1");
		}else{
			this._node.setFieldValue("@activefilter","0");
		}
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@activefilter");
	};
	
	FilterSelectionField.prototype.refreshFieldEditor = function () {};
	
	
	D3.inherits(InFilterSelectionField, D3.AbstractFieldComponent);
	
	function InFilterSelectionField() {
		D3.AbstractFieldComponent.call(this);
		this.hidefield2 = document.createElement("div");

		this.inaInput = document.createElement("input");
		this.inaInput.setAttribute("type", "checkbox");
		this.inaInput.onclick = this.checkbox_onClick.bindAsEventListener(this);
		this.hidefield2.appendChild(this.inaInput);
		this.hidefield2.appendChild(document.createTextNode("InActive"));
	}
	
	InFilterSelectionField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.hidefield2;
			if (this._node.getFieldValue("@inactivefilter")=="1") {
				this.inaInput.checked=true;
				this._node.setFieldValue("@inactivefilter","1");
			}else {
				this.inaInput.checked=false;
				this._node.setFieldValue("@inactivefilter","0");
			}
		}
		return this._fieldEditor;
	};
	
	InFilterSelectionField.prototype.checkbox_onClick = function(e){
		
		if (this.inaInput.checked){
			this._node.setFieldValue("@inactivefilter","1");
		}else{
			this._node.setFieldValue("@inactivefilter","0");
		}
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@inactivefilter");
	};
	
	InFilterSelectionField.prototype.refreshFieldEditor = function () {};
	
	D3.inherits(DepotWitsmlLogRequestDataNodeListener,D3.EmptyNodeListener);

	function DepotWitsmlLogRequestDataNodeListener() { 
	}

	DepotWitsmlLogRequestDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "enabledisable"){
			return new EnableDisableButton();
		} else if(id == "disableReprocess"){
			return new DisableReprocessButton();
		} else if(id == "filterselect"){
			return new FilterSelectionField();
		} else if(id == "infilterselect"){
			return new InFilterSelectionField();	
		} else {
			return null;
		}
	}

	D3.CommandBeanProxy.nodeListener = DepotWitsmlLogRequestDataNodeListener;

})(window);