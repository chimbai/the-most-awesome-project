(function(window){
	
	D3.inherits(ModularFormationDynamicsTesterGraphLinkField, D3.AbstractFieldComponent);
	
	function ModularFormationDynamicsTesterGraphLinkField(graphType, linkLabel) {
		D3.AbstractFieldComponent.call(this);
		this._graphType = graphType;
		this._linkButtonLabel = linkLabel;
	}
	
	ModularFormationDynamicsTesterGraphLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var imageURL = this._node.commandBeanProxy.baseUrl + "graph.html?type="+ this._graphType + "&size=medium&uid=" + escape(this._node.getFieldValue("modularFormationDynamicsTesterUid")) + "&cache=" + new Date().getTime();
			imageURL = D3.CommandBeanProxy.appendNoCacheParameters(imageURL);
			var label = this._linkButtonLabel;
			this._fieldRenderer = D3.UI.createElement({
				tag : "button",
				text : label,
				css : "DefaultButton",
				attributes : {
					title : D3.LanguageManager.getLabel("tooltip.graphLink")
				},
				callbacks:{
					onclick:function(event) {
						var _graphImage = D3.UI.createElement({
							tag:"img",
							attributes:{
								// w x h from flash equivalent
								width:"640",
								height:"480",
								src: imageURL
							},
							context:this
						});
						var _popUpDiv = "<div></div>";
						this._popUpDialog = $(_popUpDiv);
						this._popUpDialog.append(_graphImage);
						this._popUpDialog.dialog({
							height:"auto",
							width:"660", // same as flash equivalent
							resizable:false,
							modal:true,
							title:D3.LanguageManager.getLabel("popup.graph.title")
						});
					}
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
	}
	
	ModularFormationDynamicsTesterGraphLinkField.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(ModularFormationDynamicsTesterNodeListener,D3.EmptyNodeListener);
	
	function ModularFormationDynamicsTesterNodeListener() { }
	
	
	ModularFormationDynamicsTesterNodeListener.prototype.getCustomFieldComponent = function(id){
		if(id == "formation_pressure_graph_link"){
			// language for 2nd param resource needed
			return new ModularFormationDynamicsTesterGraphLinkField("formation_pressure","Formation Pressure Graph");			
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ModularFormationDynamicsTesterNodeListener;
	
})(window);