(function(window) {
	
	D3.inherits(PasswordInputField,D3.TextInputField);
	function PasswordInputField(){
		D3.TextInputField.call(this);
	}
	
	PasswordInputField.prototype.refreshFieldEditor = function() {
		PasswordInputField.uber.refreshFieldEditor.call(this);
		if (this._fieldEditor) {
			this._fieldEditor.type = "password";
		}
	};
	
	PasswordInputField.prototype.refreshFieldRenderer = function() {
		if(this.fieldRenderer) {
			this.setFieldRendererValue("");
		}
	};
	
	D3.inherits(ServerPropertiesNodeListener,D3.EmptyNodeListener);
	
	function ServerPropertiesNodeListener() {
	}
	
	ServerPropertiesNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id=="password_field"){
			return new PasswordInputField();
		}
	},
	D3.CommandBeanProxy.nodeListener = ServerPropertiesNodeListener;
	
})(window);