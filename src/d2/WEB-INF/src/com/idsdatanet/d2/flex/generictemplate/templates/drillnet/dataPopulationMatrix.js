(function(window) {
	function DataPopulationMatrix(moduleContain, headerContain, mainContain){
		this.commandBeanProxy = new D3.CommandBeanProxy();
		this._moduleContain = moduleContain;
		this._headerContain = headerContain;
		this._mainContain = mainContain;
	};
	DataPopulationMatrix.prototype.redirect = function(url){
		this.commandBeanProxy.pageRedirect(url,D3.LanguageManager.getLabel("page.redirect"));
	}
	DataPopulationMatrix.prototype.initData = function(data){
		this._data = data;
	};
	DataPopulationMatrix.prototype.initCurrentDailyUid = function(currentDailyUid){
		this._currentDailyUid = currentDailyUid.currentDailyUid;
	};
	DataPopulationMatrix.prototype.initDays = function(days){
		this._days = days;
		this._colSpan = this._days.length;
	};
	DataPopulationMatrix.prototype.render = function(){
		this.moduleTemplate = "<tbody>";
		this.headerTemplate = "<tbody>";
		this.mainTemplate = "<tbody>";
		this.getHeader();
		this.getQCHeader();
		this.getBody();

		this.moduleTemplate = this.moduleTemplate + "</tbody>";
		this.headerTemplate = this.headerTemplate + "</tbody>";
		this.mainTemplate = this.mainTemplate + "</tbody>";
		var moduleContainer = $(this.moduleTemplate);
		var headerContainer = $(this.headerTemplate);
		var mainContainer = $(this.mainTemplate);
		this._moduleContain.appendChild(moduleContainer.get(0));
		this._headerContain.appendChild(headerContainer.get(0));
		this._mainContain.appendChild(mainContainer.get(0));
		
		
		var daynumlist = headerContainer.find(".daynum");
		this._daynum = {};
		for (i=0; i<daynumlist.length; i++) {
			this._daynum[i] = daynumlist.get(i);
			this._daynum[i].onclick = this.btn_dayBynOnClick.bindAsEventListener(this, this._daynum[i]);
		}
		
		var list2 = mainContainer.find(".cheader");
		this._thm = {};
		for (i=0; i<list2.length; i++) {
			this._thm[i] = list2.get(i);
		}
		
		var list = moduleContainer.find(".cheader");
		this._th = {};
		for (i=0; i<list.length; i++) {
			this._th[i] = list.get(i);
			this._th[i].onclick = this.btn_onClick.bindAsEventListener(this._th[i], this._thm[i]);
		}
		$(this._headerContain).css('width', this._colSpan *30 );
		$(this._mainContain).css('width', this._colSpan *30 );
		
		$('[data-toggle="tooltip"]').tooltip({
	        container: "body"
	    });
	}
	DataPopulationMatrix.prototype.btn_dayBynOnClick = function(item, target){
		var dailyUid = $(target).attr('uid');
		this.commandBeanProxy.submitSummary(dailyUid);
	}
	DataPopulationMatrix.prototype.btn_onClick = function(item, mainItem){
		if ($(this).find("img").get(0).attributes[0].value=="images/up.gif"){
			$(this).find("img").get(0).attributes[0].value="images/down.gif";
		}else{
			$(this).find("img").get(0).attributes[0].value="images/up.gif"
		}
		$(this).nextUntil('tr.cheader').slideToggle(50);
		$(mainItem).nextUntil('tr.cheader').slideToggle(50);
	}
	DataPopulationMatrix.prototype.getHeader = function(){
	    this.headerTemplate = this.headerTemplate + "<tr>";
	 	D3.forEach(this._days,function(reportDaily){
	 		var reportNumber = reportDaily.reportNumber;
	 		var dailyUid = reportDaily.dailyUid;
	 		this.headerTemplate = this.headerTemplate + "<td uid='"+dailyUid+"' " + (this._currentDailyUid==dailyUid?"class='header-current-day":"class='header") + " daynum'" +
	 				" style='width: 30px;' title='go to day "+reportNumber + "'>" + reportNumber + "</td>";
	 	},this);
	 	this.headerTemplate = this.headerTemplate + "</tr>";
	}
	DataPopulationMatrix.prototype.getQCHeader = function(){
		 this.headerTemplate = this.headerTemplate + "<tr>";
		 	D3.forEach(this._days,function(reportDaily){
		 		var qcFlag = reportDaily.qcFlag;
		 		if (qcFlag== '1') this.headerTemplate = this.headerTemplate + "<td class='rqc'>RQC";
		 		else if (qcFlag== '2') this.headerTemplate = this.headerTemplate + "<td class='tqc'>TQC";
		 		else this.headerTemplate = this.headerTemplate + "<td class='nqc'>-";
		 		this.headerTemplate = this.headerTemplate + "</td>";
		 	},this);
		 	this.headerTemplate = this.headerTemplate + "</tr>";
	}
	DataPopulationMatrix.prototype.getBody = function(){
		var self = this;
	 	for (var key in this._data) {
	 		if (key!=""){
	 			this.moduleTemplate = this.moduleTemplate + "<tr class='cheader'>" +
	 			"<td valign='middle'><strong>" + key + "</strong> <img src='images/up.gif'></td>" +
	 			"</tr>";
	 			var wid = this._colSpan * 31;
	 			this.mainTemplate = this.mainTemplate + "<tr class='cheader'>";
	 			for (i=0; i<this._colSpan; i++) {
	 				 this.mainTemplate = this.mainTemplate +
	 				"<td>&nbsp;</td>";
	 			}
	 		   this.mainTemplate = this.mainTemplate +  "</tr>";
	 			D3.forEach(this._data[key],function(data){
	 				var moduleName = data.moduleName;
	 				var url = data.url;
	 				var dailyData = data.data;
	 				this.moduleTemplate = this.moduleTemplate + "<tr>" +
		 			"<td>" +  
		 			"<span class='fieldContainer'>" +
		 			"<div class='fieldRenderer' data-toggle='tooltip' data-placement='top' title='" + moduleName + "'>" + self.getModuleNameLabel(moduleName) + "</div>" +
		 			"</td></span>" +
		 			"</tr>";
	 				this.mainTemplate = this.mainTemplate + "<tr>";
	 				for (var day in this._days) {
	 					var dailyUid = this._days[day].dailyUid;
	 					var reportNumber = this._days[day].reportNumber;
	 					var result = dailyData.find(function(obj) { return obj.dailyUid === dailyUid });
	 					var redirectURL = url + (url.indexOf("?")>0?"&":"?") + "gotoday=" + dailyUid;
	 					this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.redirect('"+redirectURL+"')\" " +
	 										" style='width: 30px;' ";
	 					if (result != undefined){
	 						if(result.isIncompleteState == undefined){
	 							this.mainTemplate = this.mainTemplate + (this._currentDailyUid==dailyUid?"class='green-current-day'":"class='green'") +
 								"title='" + moduleName + " on day " + reportNumber + "'>&nbsp;</td>";
	 						}else if(result.isIncompleteState == "true"){
	 							this.mainTemplate = this.mainTemplate + (this._currentDailyUid==dailyUid?"class='cyan-current-day'":"class='cyan'") +
 								"title='" + moduleName + " on day " + reportNumber + "'>&nbsp;</td>";
	 						}else{
	 							this.mainTemplate = this.mainTemplate + (this._currentDailyUid==dailyUid?"class='green-current-day'":"class='green'") +
 								"title='" + moduleName + " on day " + reportNumber + "'>&nbsp;</td>";
	 						}
	 					}else{
	 						this.mainTemplate = this.mainTemplate + (this._currentDailyUid==dailyUid?"class='red-current-day'":"class='red'") +
	 								"title='" + moduleName + " has no data on day " + reportNumber + "'>&nbsp;</td>";
	 					}
	 				};
	 				this.mainTemplate = this.mainTemplate + "</tr>";
	 			},this);
	 		};
	 	};
	}
	
	DataPopulationMatrix.prototype.getModuleNameLabel = function(label) {
		if (label.length > 28) {
			return label.substring(0, 25) + "...";
		}
		return label;
	};
	
	// polyfill
	if (!Array.prototype.find) {
	  Object.defineProperty(Array.prototype, 'find', {
	    value: function(predicate) {
	     // 1. Let O be ? ToObject(this value).
	      if (this == null) {
	        throw new TypeError('"this" is null or not defined');
	      }

	      var o = Object(this);

	      // 2. Let len be ? ToLength(? Get(O, "length")).
	      var len = o.length >>> 0;

	      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
	      if (typeof predicate !== 'function') {
	        throw new TypeError('predicate must be a function');
	      }

	      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
	      var thisArg = arguments[1];

	      // 5. Let k be 0.
	      var k = 0;

	      // 6. Repeat, while k < len
	      while (k < len) {
	        // a. Let Pk be ! ToString(k).
	        // b. Let kValue be ? Get(O, Pk).
	        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
	        // d. If testResult is true, return kValue.
	        var kValue = o[k];
	        if (predicate.call(thisArg, kValue, k, o)) {
	          return kValue;
	        }
	        // e. Increase k by 1.
	        k++;
	      }

	      // 7. Return undefined.
	      return undefined;
	    },
	    configurable: true,
	    writable: true
	  });
	}
	
	window.D3.DataPopulationMatrix = DataPopulationMatrix;
})(window);