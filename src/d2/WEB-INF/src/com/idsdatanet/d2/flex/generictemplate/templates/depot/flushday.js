(function(window){
	
	D3.inherits(FlushDayConfirmButton, D3.AbstractFieldComponent);
	
	function FlushDayConfirmButton() {
		D3.AbstractFieldComponent.call(this);
	}
	
	FlushDayConfirmButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel("label.flushDayConfirmButton"),
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("tooltip.flushDayConfirmButton")
				},
				callbacks:{
					onclick:this.onClickBtn
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
	}
	
	FlushDayConfirmButton.prototype.onClickBtn = function() {
		var dayDate = this._node.getFieldValue("@daydate");
		if (dayDate && dayDate != "") {
			this._node.commandBeanProxy.submitForServerSideProcess(this._node, "@daydate", false);
		} else {
			alert(D3.LanguageManager.getLabel("alert.flushDayConfirmButton"));
		}
	}

	FlushDayConfirmButton.prototype.refreshFieldRenderer = function (){
	}
	
	D3.inherits(FlushDayDataNodeListener,D3.EmptyNodeListener);
	
	function FlushDayDataNodeListener() { }
	
	
	FlushDayDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "confirmButton"){
			return new FlushDayConfirmButton();			
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = FlushDayDataNodeListener;
	
})(window);