(function(window){
	
	D3.inherits(CostDailyMain, window.CostGrid);
	CostDailyMain.DEL = "del";
	CostDailyMain.SAV = "sav";
	
	function CostDailyMain(){
		this.costDailyList = [];
		this.modifiedRecords = [];
		this.deletedRecords = [];
		this.baseUrl = "";
		this.height = "";
		this.rootBox = "";
		this.currentAction = "";
		this.primaryCurrency = "";
		this.parentAfe = "";
		this.screenLayout = "";
		this.jqGridMain = "";
		this.option = null;
		this.gridCostDetail = null;
		this.afeGrid = null;
		this.currentUser = null;
		this.currentOps = null;
		this.currentDay = null;
		this.isLoading = false;
		this.isLocking = false;
		this.allowLocking = false;
		this.sumCost = {
			afesum:0.0, 
			cumcost:0.0, 
			daycost:0.0, 
			balance:0.0
		};	
		this.commandBeanProxy = new D3.CommandBeanProxy();
	}
	
	CostDailyMain.prototype.init = function (options){
		$.LoadingOverlaySetup({
			 image       : "",
			 text        : "Loading ......"	,
		});
		
		this.commandBeanModel = new D3.CommandBeanModel(options.data.template);
		this.commandBeanProxy.commandBeanModel = this.commandBeanModel;
		this.commandBeanProxy.initiate(options.data)
		this.node = this.commandBeanProxy.rootNode;
		this.option = options;
		this.option.width = this.option.width - 23;
		this.baseUrl =  options.baseUrl;
		this.baseUrl = this.baseUrl.replace("newcostnet3.html", "webservice/costnetservice.html?method=");
		this.baseUrl = this.baseUrl.replace("costdailysheet.html", "webservice/costnetservice.html?method=");
		this.height = options.height;
		var template = this.getLayout();
		var container = $(template);
		this.jqGridMain = $(template).get(0);

		this.dialog = $(this.jqGridMain).find("#dialog").get(0);
		this.cvsDialog = $(this.jqGridMain).find("#cvsDialog").get(0);
		this.loadingDialog = $(this.jqGridMain).find("#loadingDialog").get(0);
		
		// === Functional Section ===
		this.groupCat = $(this.jqGridMain).find("#groupCat").get(0);
		this.freezeCheckBox = $(this.jqGridMain).find("#freezeCheckBox").get(0);
		
		this.groupCat.onchange = this.groupingChanged.bindAsEventListener(this);
		this.freezeCheckBox.onchange = this.freezeChanged.bindAsEventListener(this);

		// === Summary Section ===
		this.data1 = $(this.jqGridMain).find("#data1").get(0);
		this.data2 = $(this.jqGridMain).find("#data2").get(0);
		this.data3 = $(this.jqGridMain).find("#data3").get(0);
		this.data4 = $(this.jqGridMain).find("#data4").get(0);
		
		// === CRUD Control ===
		this.btn_confirmButton = $(this.jqGridMain).find("#confirmButton").get(0);
		this.btn_cancelButton = $(this.jqGridMain).find("#cancelButton").get(0);
		this.btn_getAccountCode = $(this.jqGridMain).find("#btn_getAccountCode").get(0);
		this.btn_copyFromDay = $(this.jqGridMain).find("#btn_copyFromDay").get(0);
		this.btn_copyFromAFE = $(this.jqGridMain).find("#btn_copyFromAFE").get(0);
		this.btn_copyFromPrice = $(this.jqGridMain).find("#btn_copyFromPrice").get(0);
		this.btn_copyFromSO = $(this.jqGridMain).find("#btn_copyFromSO").get(0);
		this.btn_addNewRecord = $(this.jqGridMain).find("#btn_addNewRecord").get(0);
		this.btn_lockRecord = $(this.jqGridMain).find("#btn_lockRecord").get(0);
		this.btn_unlockRecord = $(this.jqGridMain).find("#btn_unlockRecord").get(0);
		this.btn_deleteRecord = $(this.jqGridMain).find("#btn_deleteRecord").get(0);
		this.btn_exportRecord = $(this.jqGridMain).find("#btn_exportRecord").get(0);
		this.btn_exportRawRecord = $(this.jqGridMain).find("#btn_exportRawRecord").get(0);
		this.btn_duplicatedRecord = $(this.jqGridMain).find("#btn_duplicatedRecord").get(0);
		this.btn_copyRecord = $(this.jqGridMain).find("#btn_copyRecord").get(0);
		this.btn_pasteRecord = $(this.jqGridMain).find("#btn_pasteRecord").get(0);
		this.btn_testRecord = $(this.jqGridMain).find("#btn_testRecord").get(0);
		this.btn_import = $(this.jqGridMain).find("#btn_import").get(0);
		
		
		this.addSelectedBtn = $(this.jqGridMain).find("#addSelectedBtn").get(0);
		this.selectAllBtn = $(this.jqGridMain).find("#selectAllBtn").get(0);
		this.unselectAllBtn = $(this.jqGridMain).find("#unselectAllBtn").get(0);
		this.closeDialogBtn = $(this.jqGridMain).find("#closeDialogBtn").get(0);
		this.searchDialogTxt = $(this.jqGridMain).find("#searchDialogTxt").get(0);
		this.clearSearchDialogBtn = $(this.jqGridMain).find("#clearSearchDialogBtn").get(0);
		
		// Getting CVS text
		this.confirmCVSDialogBtn = $(this.jqGridMain).find("#confirmCVSDialogBtn").get(0);
		this.closeCVSDialogBtn = $(this.jqGridMain).find("#closeCVSDialogBtn").get(0);
		this.cvsText = $(this.jqGridMain).find("#cvsText").get(0);
		
		this.btn_confirmButton.onclick = this.confirmClicked.bindAsEventListener(this);
		this.btn_cancelButton.onclick = this.cancelClicked.bindAsEventListener(this);
		this.btn_getAccountCode.onclick = this.getCostListData.bindAsEventListener(this);
		var self = this;
		this.btn_copyFromDay.onclick= function(){
			self.getCostPopup("DAY");
		};
		this.btn_copyFromAFE.onclick= function(){
			self.getCostPopup("AFE");
		};
		this.btn_copyFromPrice.onclick= function(){
			self.getCostPopup("PRICE");
		};
		this.btn_copyFromSO.onclick= function(){
			self.getCostPopup("SO");
		};
//		this.btn_copyFromDay.onclick = this.copyFromYesterdayData.bindAsEventListener(this);
//		this.btn_copyFromAFE.onclick = this.copyFromAFEData.bindAsEventListener(this);
		
		
		this.btn_addNewRecord.onclick = this.addNewRecord.bindAsEventListener(this);
		this.btn_lockRecord.onclick = this.lockRecords.bindAsEventListener(this);
		this.btn_unlockRecord.onclick = this.unlockRecords.bindAsEventListener(this);
		this.btn_deleteRecord.onclick = this.deleteRecords.bindAsEventListener(this);
		this.btn_exportRecord.onclick = this.exportRecords.bindAsEventListener(this);
		this.btn_exportRawRecord.onclick = this.exportRawRecords.bindAsEventListener(this);
		this.btn_duplicatedRecord.onclick = this.btnDuplicatedClicked.bindAsEventListener(this);
		this.btn_copyRecord.onclick = this.btnCopyRecordClicked.bindAsEventListener(this);
		this.btn_pasteRecord.onclick = this.btnPasteRecordClicked.bindAsEventListener(this);
		this.btn_testRecord.onclick = this.btnTestClicked.bindAsEventListener(this);
		this.btn_import.onclick = this.btn_importClicked.bindAsEventListener(this);
		
		this.addSelectedBtn.onclick = this.addSelected.bindAsEventListener(this);
		this.selectAllBtn.onclick = this.selectAllBtnClick.bindAsEventListener(this);
		this.unselectAllBtn.onclick = this.unselectAllBtnClick.bindAsEventListener(this);
		this.closeDialogBtn.onclick = this.closeDialog.bindAsEventListener(this);
		this.clearSearchDialogBtn.onclick = this.clearSearchDialogBtnClicked.bindAsEventListener(this);
		this.searchDialogTxt.onkeyup = this.searchDialogBtnClicked.bindAsEventListener(this);
		
		// CVS function
		this.confirmCVSDialogBtn.onclick = this.confirmImport.bindAsEventListener(this);
		this.closeCVSDialogBtn.onclick = this.closeCSVDialog.bindAsEventListener(this);
		
		// === OTHERS ===
		this.rootBox = $(this.jqGridMain).find("#rootBox").get(0);
		this.codeTree = $(this.jqGridMain).find("#codeTree").get(0);
		this.gridCostDetail = $(this.jqGridMain).find("#grid_costafedetail").get(0);
		
		this.hide(this.rootBox);
		this.groupCat.removeAttribute("disabled");
		this.disableItems(false);
		
//		this.xxx = $(this.jqGridMain).find(".ra");
//	    $(this.xxx).checkboxradio({
//	        icon: false
//	      });
	    
		if (localStorage.getItem("costDaily")!=null){
			$(this.btn_copyRecord).addClass("hide"); 
			$(this.btn_pasteRecord).removeClass("hide"); 
		}else{
			$(this.btn_copyRecord).removeClass("hide");
		};
		D3.UI.removeAllChildNodes(this.codeTree);
		$( this.dialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	      //  height: 400,
	        width: 800,
	      });
		
		$( this.cvsDialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	       // height: auto,
	        width: 500,
		 });
		
		$( this.loadingDialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	        height: 80,
	        width: 500, 
		 });
		
		// Main calling
		console.log("Start Load lookup ");
		this.getUsersession();
	//	$.LoadingOverlay("show");	
		var me = this;
		window.addEventListener("resize", function(){me.onWindowResize();});
	}

	CostDailyMain.prototype.onWindowResize = function(){
		this.afeGrid.jqGrid("setGridWidth", this.jqGridMain.offsetWidth);
		this.autoResizeGrid();
	}
	
	CostDailyMain.prototype.getLoad = function(){
		if (this.afeGrid==null) {
			this.getProgressStatus(true, false, "Loading..");
			this.getLookupList();
		}
	}
	
	CostDailyMain.prototype.getCostListData = function(){
	//	$(this.codeTree).jstree("open_all");
		$(this.dialog).dialog( "open" );
	}
	
	CostDailyMain.prototype.copyFromAFEData = function(){
		this.getCostPopup("AFE");
		//this.getCostPopup("DAY");
	}
	
	CostDailyMain.prototype.getAvailableAFEList = function(type){
		var page = this.baseUrl + "get_afe_listing";
		var self = this;
		this.costAfeDetailsList = [];
		this.costAfeDetailsGroup = [];
		$.ajax({
			url:page,
			dataType: "xml",  
			success: function (result) {			
				$(result).find('CostAfeMaster').each(function(){
					var headerText;
					var model = {};
					var costAfeMasterUid = $(this).get(0).getElementsByTagName("costAfeMasterUid")[0].textContent;
					var afeNumber = $(this).get(0).getElementsByTagName("afeNumber")[0].textContent;
					var afeType = $(this).get(0).getElementsByTagName("afeType")[0].textContent;
					var option = document.createElement("option");
					option.value = costAfeMasterUid;
					option.appendChild(document.createTextNode(afeNumber));
					if (type=="AFE"){
						if (afeType!="PRICELIST" && afeType!="SO") self.diaSelect.appendChild(option);
					}else if (type=="PRICE"){
						if (afeType=="PRICELIST") self.diaSelect.appendChild(option);
					}else if (type=="SO"){
						if (afeType=="SO") self.diaSelect.appendChild(option);
					}else{
						self.diaSelect.appendChild(option);
					}
				});
				
				var list = result.documentElement.getElementsByTagName("CostAfeDetail");
				for(var i=0; i<list.length; i++){
					var rec = new CostAfeDetail();
					for(var j in list[i].children){
						var prop = list[i].children[j].tagName;
						if(rec.hasOwnProperty(prop)){
							if(list[i].children[j].hasChildNodes()){
								rec[prop] = list[i].children[j].childNodes[0].nodeValue;
							}
						}
					}
				//	rec.accountCode= rec.accountCode + " :: " + rec.shortDescription;  
					rec.companyName=self.vendorList[rec.lookupCompanyUid];
					rec.currencyRate=self.currencyRate[rec.currency];
					rec.run();
					self.costAfeDetailsList.push(rec);
				}
			}
		}).done(function(data, textStatus, jqXHR) {
			if (self.diaSelect.length==0){
				if (type=="AFE"){
					alert("No AFE Found!");
				}else if (type=="SO"){
						alert("No Service Order Found!");	
				}else{
					alert("No Price List Found!");
				}
				//$.LoadingOverlay("hide");
				self.getProgressStatus(false, false, "Loading..");
			}else{
				self.loadCostPopup(type);
			}	
		});
	}
	
	CostDailyMain.prototype.getAvailableDayList = function(){  
		var page = this.baseUrl + "get_available_daily_list";
		var self = this;

		$.ajax({
			url:page,
			dataType: "xml",  
			success: function (result) {			
				$(result).find('Daily').each(function(){
					var headerText;
					var model = {};
					var dailyUid = $(this).get(0).getElementsByTagName("dailyUid")[0].textContent;
					var dayNumber = $(this).get(0).getElementsByTagName("dayNumber")[0].textContent;
					var option = document.createElement("option");
					option.value = dailyUid;
					option.appendChild(document.createTextNode(dayNumber));
					self.diaSelect.appendChild(option);
				});
			}
		}).done(function(data, textStatus, jqXHR) {
			if (self.diaSelect.length==0){
				
				alert("No previous day record.");
				//$.LoadingOverlay("hide");
				self.getProgressStatus(false, false, "Loading..");
			}else{
				self.loaddayPopup("DAY");
			}	
		});
	}
	
	CostDailyMain.prototype.getCostPopup = function(type){  
		//$.LoadingOverlay("show");
		this.getProgressStatus(true, false, "Loading..");
		var label = "";
		if (type=="AFE") label = "AFE";
		else if (type=="PRICE") label = "Price List";
		else if (type=="SO") label = "SO";
		else label = "Day";
		var divTemplate = "<div id='dialog' title='Account Code' style='width: 470px;'>" +
			"<div style='height: 30px;text-align: center;'>" +
				"<button id='addSelBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Add Selected</button>" +
				"<button id='selectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Select All</button>" +
				"<button id='unselectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Unselect All</button>" +
				"<button id='closeDlogBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Close</button>" +
			"</div>" +
			"<div style='display: flex; position: relative;'>" +
				"<input id='searchDgTxt' type='text' class='fieldEditor textInput'>" +
				"<button id='clearSearchDgBtn' style='width: 30%; height:22px' class='DefaultButton'>Clear</button>" +
			"</div>" +
			"<div style='display: flex; position: relative;align-items: center;'>" +
				label + ": <select name='groupData' id='groupData' class='fieldEditor comboBox' style='width:90%'>" +
				
				"</select>" +
			"</div>" +
			"<div style='display: block; overflow: auto; height: " + this.option.popHeight + "px;''>" +
				"<div id='codeTree'></div>" +
			"</div>" +
		"</div>" ;
	
		this.dialogBox = $(divTemplate).get(0);

		this.diaSelect = $(this.dialogBox).find("#groupData").get(0);
		var btadd = $(this.dialogBox).find("#addSelBtn").get(0);
		var btselall = $(this.dialogBox).find("#selectAllBtn").get(0);
		var btuselall = $(this.dialogBox).find("#unselectAllBtn").get(0);
		var btclose = $(this.dialogBox).find("#closeDlogBtn").get(0);
		this.diaCodeTree = $(this.dialogBox).find("#codeTree").get(0);
		
		var searchDgTxt = $(this.dialogBox).find("#searchDgTxt").get(0);
		var clearSearchDgBtn = $(this.dialogBox).find("#clearSearchDgBtn").get(0);
		
		var self = this;
		this.diaSelect.onchange  = function(){
		//	$.LoadingOverlay("show");
			self.getProgressStatus(true, false, "Loading..");
			if (type=="AFE") self.loadCostPopup(type);
			if (type=="PRICE") self.loadCostPopup(type);
			if (type=="SO") self.loadCostPopup(type);
			else self.loaddayPopup(type);
		};
		
		btadd.onclick = function(){
			self.addingToScreen(type);
		};
		
		searchDgTxt.onkeyup = function(){
			$(self.diaCodeTree).jstree(true).search(searchDgTxt.value);
		};
		
		clearSearchDgBtn.onclick = function(){
			searchDgTxt.value = "";
			$(self.diaCodeTree).jstree("clear_search");
		};
		
		btselall.onclick = function(){
			$(self.diaCodeTree).jstree("check_all");
		};
		
		btuselall.onclick = function(){
			$(self.diaCodeTree).jstree().deselect_all(true);
		};
		
		btclose.onclick = function(){
			$(self.dialogBox).dialog('close');
		};
		
		if (type=="AFE") {
			this.getAvailableAFEList(type);
		} else if (type=="PRICE") {
			this.getAvailableAFEList(type);
		} else if (type=="SO") {
			this.getAvailableAFEList(type);
		} else {
			this.getAvailableDayList();
		}
		
	}
	
	CostDailyMain.prototype.addingToScreen = function(type){ 
		if ($(this.diaCodeTree).jstree("get_selected").length==0){
			alert("Please select one");
		}else{
			var selectedList = $(this.diaCodeTree).jstree("get_selected");
			var selectedCode = [];
			var count = 0;
			for(var i = 0; i < selectedList.length; i++){
				var code = selectedList[i];
				if (code.includes("c:")){
					
				}else{
					count = count +1;
					selectedCode.push(code);
				}
			}
			for(var i = 0; i < selectedCode.length; i++){
				var selUid = selectedCode[i];
				if ((type=="AFE") || (type=="PRICE") || (type=="SO")){ 
					var csd = this.costAfeDetailsList.find(x => x.costAfeDetailUid === selUid);  
					var rec = new CostDailysheet();
					for (var key in rec){
						 if (rec.hasOwnProperty(key)) {
							 if (csd.hasOwnProperty(key)) {
								 rec[key] = csd[key];
							 }
						 }
					}
					
					if (this.planRefMaps[rec.planReference]==undefined){
						rec.phaseCode = "";
					}else{
						rec.planReferenceName = this.planRefMaps[rec.planReference].displayName;
						rec.phaseCode = this.planRefMaps[rec.planReference].phaseCode;
						rec.phaseName = this.phsCodeList[rec.phaseCode];
					}	
					rec.lookupCompanyName = csd.companyName;
					rec.afeItemDescription = rec.itemDescription;
					rec.itemDescription = rec.longDescription;
					rec.quantity = 1.0;
					rec.itemTotal = 0.0;
					rec.itemTotalInPrimaryCurrency = 0.0;
					rec.action = CostDailyMain.SAV;
					rec.isnew = true;
					rec.costDailysheetUid = "";
									
					var d = new Date();
					var n = d.getTime();
					var parameters =
					{
					    rowID : "new" + n,
					    initdata : rec,
					    position :"last",
					};
					this.afeGrid.jqGrid('addRow',parameters);
					this.show(this.rootBox);
					this.groupCat.setAttribute("disabled", true);
					this.disableItems(true);
					$(this.btn_lockRecord).addClass("hide");
					this.autoResizeGrid();
				}else if (type=="DAY"){
					var rec = this.costDayDataList.find(x => x.costDailysheetUid === selUid);
					
					var nrec = new CostDailysheet();
					for (var key in nrec){
						 if (rec.hasOwnProperty(key)) {
							 nrec[key] = rec[key];
						 }
					}
					
			//		nrec.quantity = 0.0;
			//		nrec.itemTotal = 0.0;
			//		nrec.itemTotalInPrimaryCurrency = 0.0;
					nrec.action = CostDailyMain.SAV;
					nrec.isnew = true;
					nrec.costDailysheetUid = "";
									
					var d = new Date();
					var n = d.getTime();
					var parameters =
					{
					    rowID : "new" + n,
					    initdata : nrec,
					    position :"last",
					};
					this.afeGrid.jqGrid('addRow',parameters);
					this.show(this.rootBox);
					this.groupCat.setAttribute("disabled", true);
					this.disableItems(true);
					$(this.btn_lockRecord).addClass("hide");
					this.autoResizeGrid();
				};
			};
			
		//	$.LoadingOverlay("hide");
			this.getProgressStatus(false, false, "Loading..");
			$( this.dialogBox ).dialog( "close");
		}
	}
	
	CostDailyMain.prototype.loaddayPopup = function(type){ 
		this.costDayDataList = [];
		var page = this.baseUrl + "get_yesterday_cost_list";
		page = page + "&dailyUid=" + this.diaSelect.value;
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",  
			success: function (result) {			
				var list = result.documentElement.getElementsByTagName("CostDailysheet");
				for(var i=0; i<list.length; i++){
					var rec = new CostDailysheet();
					for(var j in list[i].children){
						var prop = list[i].children[j].tagName;
						if(rec.hasOwnProperty(prop)){
							if(list[i].children[j].hasChildNodes()){
								rec[prop] = list[i].children[j].childNodes[0].nodeValue;
							}
						}
					}
			//		rec.quantity = 0.0;
			//		rec.itemTotalInPrimaryCurrency = 0.0;
			//		rec.accountCode= rec.accountCode + " :: " + rec.afeShortDescription;  
					rec.lookupCompanyName=self.vendorList[rec.lookupCompanyUid];
					rec.currencyRate=self.currencyRate[rec.currency];
					rec.className = self.clsCodeList[rec.classCode];
					rec.phaseName = self.phsCodeList[rec.phaseCode];
					rec.taskName = self.opsCodeList[rec.taskCode];
					rec.rootCauseName = self.rcCodeList[rec.rootCauseCode];
					rec.planReference = "";
					rec.planReferenceName = "";
					rec.run();
					
					self.costDayDataList.push(rec);
				}
			}
		}).done(function(data, textStatus, jqXHR) {
			self.loadCostPopup(type);
		});
		
	}
	
	// Copy From AFE & Day Section
	CostDailyMain.prototype.loadCostPopup = function(type){ 
		var datatree = [];
		if (type=="AFE") {
			var groupList = [];
			var groupList2 = [];
			var cmUid = this.diaSelect.value;
			for(var i=0; i<this.costAfeDetailsList.length; i++){ 
				var rec =  this.costAfeDetailsList[i];
				if (rec.costAfeMasterUid==cmUid){
					if (!groupList.includes(rec.category)){
						groupList.push(rec.category);
						var gro = {
							"id" : "c:" + rec.category,
							"parent" : "#",
							"text" :  (rec.category=="-"?"Uncategorized":rec.category) ,
						};
						datatree.push(gro);
					};
					var grptxt = rec.category + (rec.afeGroupName==""?"-":rec.afeGroupName);
					if (!groupList2.includes(grptxt)){
						groupList2.push(grptxt);
						var gro = {
							"id" : "c:" + grptxt,
							"parent" : "c:" + rec.category,
							"text" :  (rec.afeGroupName==""?"Uncategorized":rec.afeGroupName) ,
						};
						datatree.push(gro);
					};
					var datao = {
							"id" : rec.costAfeDetailUid,
							"parent" : "c:" + grptxt,
							"text" : rec.accountCode + " " + (rec.itemDescription==""?"":"(" + rec.itemDescription + ")" ) +" :: "+ (rec.itemUnit==undefined?"":rec.itemUnit),
						};
					datatree.push(datao);
				}
			};
		}else if (type=="PRICE") {
			var groupList = [];
			var groupList2 = [];
			var cmUid = this.diaSelect.value;
			for(var i=0; i<this.costAfeDetailsList.length; i++){ 
				var rec =  this.costAfeDetailsList[i];
				if (rec.costAfeMasterUid==cmUid){
					var grptxt = (rec.afeGroupName==""?"-":rec.afeGroupName);
					if (!groupList.includes(grptxt)){
						groupList.push(grptxt);
						var gro = {
							"id" : "c:" + grptxt,
							"parent" : "#",
							"text" :  (rec.afeGroupName==""?"Uncategorized":rec.afeGroupName) ,
						};
						datatree.push(gro);
					};
//					var grptxt = rec.category + (rec.afeGroupName==""?"-":rec.afeGroupName);
//					if (!groupList2.includes(grptxt)){
//						groupList2.push(grptxt);
//						var gro = {
//							"id" : "c:" + grptxt,
//							"parent" : "c:" + rec.category,
//							"text" :  (rec.afeGroupName==""?"Uncategorized":rec.afeGroupName) ,
//						};
//						datatree.push(gro);
//					};
					var datao = {
							"id" : rec.costAfeDetailUid,
							"parent" : "c:" + grptxt,
							"text" : rec.accountCode + " " + (rec.itemDescription==""?"":"(" + rec.itemDescription + ")" ) +" :: "+ (rec.itemUnit==undefined?"":rec.itemUnit),
						};
					datatree.push(datao);
				}
			};
		}else if (type=="SO") {
			var groupList = [];
			var groupList2 = [];
			var cmUid = this.diaSelect.value;
			for(var i=0; i<this.costAfeDetailsList.length; i++){ 
				var rec =  this.costAfeDetailsList[i];
				if (rec.costAfeMasterUid==cmUid){
					var grptxt = (rec.afeGroupName==""?"-":rec.afeGroupName);
					if (!groupList.includes(grptxt)){
						groupList.push(grptxt);
						var gro = {
							"id" : "c:" + grptxt,
							"parent" : "#",
							"text" :  (rec.afeGroupName==""?"Uncategorized":rec.afeGroupName) ,
						};
						datatree.push(gro);
					};

					var datao = {
							"id" : rec.costAfeDetailUid,
							"parent" : "c:" + grptxt,
							"text" : rec.accountCode + " " + (rec.itemDescription==""?"":"(" + rec.itemDescription + ")" ) +" :: "+ (rec.itemUnit==undefined?"":rec.itemUnit),
						};
					datatree.push(datao);
				}
			};
		}else if (type=="DAY") {
			var groupList = [];
			var groupList2 = [];
			for(var i=0; i<this.costDayDataList.length; i++){ 
				var rec =  this.costDayDataList[i];

				if (!groupList.includes(rec.category)){
					groupList.push(rec.category);
					var gro = {
						"id" : "c:" + rec.category,
						"parent" : "#",
						"text" :  (rec.category=="-"?"Uncategorized":rec.category) ,
					};
					datatree.push(gro);
				};
				var grptxt = rec.category + (rec.afeGroupName==""?"-":rec.afeGroupName);
				if (!groupList2.includes(grptxt)){
					groupList2.push(grptxt);
					var gro = {
						"id" : "c:" + grptxt,
						"parent" : "c:" + rec.category,
						"text" :  (rec.afeGroupName==""?"Uncategorized":rec.afeGroupName) ,
					};
					datatree.push(gro);
				};
				var datao = {
						"id" : rec.costDailysheetUid,
						"parent" : "c:" + grptxt,
						"text" : rec.accountCode  + (rec.afeItemDescription==""?"":" :: " + rec.afeItemDescription)  + " " + (rec.itemDescription==""?"":"(" + rec.itemDescription + ")") + " :: "+ (rec.itemUnit==undefined?"":rec.itemUnit),
					};
				datatree.push(datao);
			};		
		};
		
		this.loadCostPopupStart(datatree, type);
	}
	
	CostDailyMain.prototype.loadCostPopupStart = function(data, type){ 
		if ($(this.diaCodeTree).hasClass("jstree")){
			$(this.diaCodeTree).jstree().settings.core.data = eval(data);
			$(this.diaCodeTree).jstree("refresh");
		} else {
			$(this.diaCodeTree).jstree({ 
				"core": {
			        "data": eval(data),
			        "themes":{
			        	"icons":false
			        }
			    },
			    "search": {
			    	'show_only_matches' : true,
			    	'show_only_matches_children' : true,
			    	'search_leaves_only' : true
			    },
			    "checkbox": {
			    	"keep_selected_style": false,
			    //	"three_state" : false,
			    },
			    "sort" : function(a, b) {
						a1 = this.get_node(a);
				        b1 = this.get_node(b);
				        if (a1.parent=="#"){
				        	return (a1.id > b1.id) ? 1 : -1;
				        }else{
					        if (a1.icon == b1.icon){
					            return (a1.text > b1.text) ? 1 : -1;
					        } else {
					        	return (a1.icon > b1.icon) ? 1 : -1;
					        }
				        }
				},
				"plugins": ["sort", "checkbox","search"]
				}) .bind("loaded.jstree", function (event, data) {
					//$(this).jstree("open_all");
		    }) .bind("refresh.jstree", function (e, data) {
		    	    // $(this).jstree("open_all");
		    	});
		};
		
	//	$.LoadingOverlay("hide");
		this.getProgressStatus(false, false, "Loading..");
		var title = "Account Code";
		if (type=="AFE") title = "Copy from AFE";
		if (type=="PRICE") title = "Copy from Price List";
		if (type=="SO") title = "Copy from Service Order";
		if (type=="DAY") title = "Copy from Day";
		$(this.dialogBox).dialog({
	//        resizable: false,
	        width:800,
	//        height:auto,
	        modal: true,
	        draggable:false,
	        title: title,	        
	        open: function(event, ui) { 
	            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
	        },
	    });
	}
	
	CostDailyMain.prototype.copyFromYesterdayData = function(){
	//	
		this.getAvailableDailyList();
	}
	
	CostDailyMain.prototype.getAvailableDailyList = function(){
		var page = this.baseUrl + "get_available_daily_list";
		var self = this;
		this._combo = this.createElement("select",null,"fieldEditor comboBox");
		$.ajax({
			url:page,
			dataType: "xml",  
			success: function (result) {			
				$(result).find('Daily').each(function(){
					var headerText;
					var model = {};
					var dailyUid = $(this).get(0).getElementsByTagName("dailyUid")[0].textContent;
					var dayNumber = $(this).get(0).getElementsByTagName("dayNumber")[0].textContent;
					var option = document.createElement("option");
					option.value = dailyUid;
					option.appendChild(document.createTextNode(dayNumber));
					self._combo.appendChild(option);
				});
				
			}
		}).done(function(data, textStatus, jqXHR) {
				self.continueFromPreviousRunPopup();
		});
	}
	
	CostDailyMain.prototype.continueFromPreviousRunPopup = function(){
		
		this._pnlEditor = this.createElement("div");
		var datumHBox = this.createElement("div",null,"centerdiv");	
		datumHBox.appendChild(this.getMainContain());
		this._pnlEditor.appendChild(datumHBox);
		this._pnlEditor.self = this;
			
		var btnConfirm = {
				text:D3.LanguageManager.getLabel("button.confirm"),
				"class":"PopupConfirmButton",
				click:function(){
					$(this).dialog('close');
					$(this).get(0).self.btnConfirm_onClick();
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				"class":"PopupCancelButton",
				click:function(){
				//	$(this).get(0).self.btnCancel_onClick(node);
	                $(this).dialog('close');
				}
		};
		var btnClose = {
				text:D3.LanguageManager.getLabel("button.close"),
				"class":"RootButton",
				click:function(){
	                $(this).dialog('close');
				}
		};
		$(this._pnlEditor).dialog({
		        resizable: false,
		        width:300,
		        height:110,
		        modal: true,
		        draggable:false,
		        title: "Copy from Day",	        
		        open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        },
		    });
		if (this._combo.length > 0){
			$(this._pnlEditor).dialog('option','buttons',[btnConfirm,btnCancel]);
		}else{
			$(this._pnlEditor).dialog('option','buttons',[btnClose]);
		}
	}
	
	CostDailyMain.prototype.getMainContain = function(){
		this._bhaNumber = [];
		var main = this.createElement("div",null,"VBox");
		if (this._combo.length > 0){
			main.appendChild(this.createElement("label","Copy from Day: ","popupText"));
			main.appendChild(this._combo);
		}else{
			main.appendChild(this.createElement("label","No have Previous Day","popupText"));
		}
		return main;
	}
	
	CostDailyMain.prototype.btnConfirm_onClick = function(){  
		//$.LoadingOverlay("show");
		this.getProgressStatus(false, false, "Loading..");
		var page = this.baseUrl + "get_yesterday_cost_list";
		page = page + "&dailyUid=" + this._combo.value;
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",  
			success: function (result) {			
				var list = result.documentElement.getElementsByTagName("CostDailysheet");
				for(var i=0; i<list.length; i++){
					var rec = new CostDailysheet();
					for(var j in list[i].children){
						var prop = list[i].children[j].tagName;
						if(rec.hasOwnProperty(prop)){
							if(list[i].children[j].hasChildNodes()){
								rec[prop] = list[i].children[j].childNodes[0].nodeValue;
								if(prop == "recurringItem"){
									//rec[prop] = (list[i].children[j].childNodes[0].nodeValue ? "t" : "f");
									//console.log("rec[prop]", rec[prop]);
								}
							}
						}
					}
					rec.quantity = 0.0;
					rec.itemTotalInPrimaryCurrency = 0.0;
					rec.accountCode= rec.accountCode + " :: " + rec.afeShortDescription;
					rec.lookupCompanyName=self.vendorList[rec.lookupCompanyUid];
					rec.currencyRate=self.currencyRate[rec.currency];
					rec.className = self.clsCodeList[rec.classCode];
					rec.phaseName = self.phsCodeList[rec.phaseCode];
					rec.taskName = self.opsCodeList[rec.taskCode];
					rec.rootCauseName = self.rcCodeList[rec.rootCauseCode];
					rec.run();

					rec.action = CostDailyMain.SAV;
					rec.isnew = true;
					var d = new Date();
					var n = d.getTime();
					rec.costDailysheetUid = "new" + n;
					var parameters =
					{
					    rowID : "new" + n,
					    initdata : rec,
					    position :"last",
					};
					self.afeGrid.jqGrid('addRow',parameters);
				}
				self.show(self.rootBox);
				self.groupCat.setAttribute("disabled", true);
				self.disableItems(true);
				self.btn_lockRecord.addClass("hide");
				self.autoResizeGrid();
			}
		}).done(function(data, textStatus, jqXHR) {
			//$.LoadingOverlay("hide");
			this.getProgressStatus(false, false, "Loading..");
		});
	}
	
	CostDailyMain.prototype.searchDialogBtnClicked = function(){
		$(this.codeTree).jstree('search', this.searchDialogTxt.value);
	}
	
	CostDailyMain.prototype.btn_importClicked = function(){
		$( this.cvsDialog ).dialog("open");
	}
	
	CostDailyMain.prototype.clearSearchDialogBtnClicked = function(){
		this.searchDialogTxt.value = "";
		$(this.codeTree).jstree("clear_search");
	}
	
	CostDailyMain.prototype.btnDuplicatedClicked = function(){

		var selRowIds = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		if (selRowIds.length==0) alert("Please select an item to be Duplicated");
		else{
			var copiedData = [];
			for(var i = 0; i < selRowIds.length; i++){	
			    rowData = this.afeGrid.jqGrid("getLocalRow", selRowIds[i]);
			    copiedData.push(rowData);  
			}
			
			for(var j = 0; j < copiedData.length; j++){	
				var rows = copiedData[j];
				var rec = new CostDailysheet();
				for (var key in rec){
					 if (rec.hasOwnProperty(key)) {
						 if (rows.hasOwnProperty(key)) {
							 rec[key] = rows[key];
						 }
					 }
				}
				
				rec.action = CostDailyMain.SAV;
				rec.isnew = true;
				var d = new Date();
				var n = d.getTime();
				var x = Math.floor((Math.random() * 100) + 1);
				rec.costDailysheetUid = "new" + n + x;
				var parameters =
					{
					    rowID : "new" + n + x,
					    initdata : rec,
					    position :"last",
					};
				this.afeGrid.jqGrid('addRow',parameters);
				
			}

			this.show(this.rootBox);
			this.groupCat.setAttribute("disabled", true);
			this.disableItems(true);
			$(this.btn_lockRecord).addClass("hide");
			this.autoResizeGrid();
		}
	}
	
	CostDailyMain.prototype.btnCopyRecordClicked = function(){
		var selRowIds = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		if (selRowIds.length==0) alert("Please select an item to be copied");
		else{
			var copiedData = [];
			for (i = 0, n = selRowIds.length; i < n; i++) {
			    rowData = this.afeGrid.jqGrid("getLocalRow", selRowIds[i]);
			    copiedData.push(rowData);
			}
			
		//	sessionStorage.setItem("costDaily", copiedData);
			localStorage.setItem("costDaily", JSON.stringify(copiedData));
			alert("Selected item(s) added to clipboard");
			$(this.btn_copyRecord).addClass("hide");
			$(this.btn_pasteRecord).removeClass("hide");
		}
	}
	
	CostDailyMain.prototype.btnTestClicked = function(){ 
		this.getProgressStatus(true, false, "xx");
	}
	
	CostDailyMain.prototype.btnPasteRecordClicked = function(){  
		if (localStorage.getItem("costDaily")!=null){
			var cpData = JSON.parse(localStorage.getItem("costDaily"));
			for(var i = 0; i < cpData.length; i++){	
				var cp = cpData[i];
				
				cp.action = CostDailyMain.SAV;
				cp.isnew = true;
				var d = new Date();
				var n = d.getTime();
				cp.costDailysheetUid = "new" + n;
				var parameters =
				{
				    rowID : "new" + n,
				    initdata : cp,
				    position :"last",
				};
				this.afeGrid.jqGrid('addRow',parameters);
			}
			this.show(this.rootBox);
			this.groupCat.setAttribute("disabled", true);
			this.disableItems(true);
			$(this.btn_lockRecord).addClass("hide");
			this.autoResizeGrid();
		//	alert(localStorage.getItem("costDaily"));
			localStorage.removeItem("costDaily");
			$(this.btn_copyRecord).removeClass("hide");
			$(this.btn_pasteRecord).addClass("hide");
		}else{
			alert("Nothing");
		}
	}
	
	CostDailyMain.prototype.getLayout = function(){

		var layoutTemplate = "" +
				"<div id='jqGrid_main'>" +
					"<div id='filter' class='ui-layout-north'>" +
				//	"<div style='height : 32px'>" +
					"<div>" +
					"<div id='rootBox' class='RootButtonContainer' style='text-align: center;'>" +
						"<button class='RootButtonConfirm' type='button' class='btn' id='confirmButton'>Confirm</button>" +
						"<button class='RootButtonCancel' type='button' class='btn' id='cancelButton'>Cancel</button>" +
					"</div>" +
					"</div>" +
					"<div style='display: flex;'>" +
						"<div style='width: 70%;'>" +
							"<select name='groupCat' id='groupCat' class='fieldEditor comboBox'>" +
										  "<option value='Ungroup'>Ungroup</option>" +
										  "<option value='Category'>Category</option>" +
										  "<option value='AccCode'>Account Code</option>" +
										  "<option value='AccGroup'>AFE Grouping</option>" +
										  "<option value='Vendor'>Vendor</option>" +
										"</select>" +
						"</div>" +
						"<div class='hide' id='freezeContainer' style='width: 50%;'>" +
							"<input type='checkbox' id='freezeCheckBox' value='1'>Freeze Account Code" +
						"</div>" +
					//	"<div id='summaryBox' class='container' style='width: 800px;background-color: white;margin-right: 10px;'>" +
							"<div class='boxes' style='color: black;font-weight: 700;padding-top: 2px;' id='data1'>AFE : 0.00</div>" + 
							"<div class='boxes' style='color: black;font-weight: 700;padding-top: 2px;' id='data4'>Day Total : 0.00</div>" + 
						    "<div class='boxes' style='color: black;font-weight: 700;padding-top: 2px;' id='data2'>Cum Cost : 0.00</div>" + 
						    "<div class='boxes' style='color: black;font-weight: 700;padding-top: 2px; padding-right: 20px;' id='data3'>Balance : 0.00</div>" +
					//	"</div>" +
					"</div >" +
				"</div>" +
				"<div class='ui-layout-center' style='overflow: hidden;'> " +
					"<table id='grid_costafedetail'></table>" +
			    	"<div id='jqGridPager'></div>" +
				"</div>" +
				"<div id='filter' class='ui-layout-south'>" +
					"<table style='width:100%; border:0px; border-collapse:collapse'><tr><td>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_getAccountCode'>Get Account Code</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_copyFromDay'>Copy from Day</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_copyFromAFE'>Copy from AFE</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_copyFromPrice'>Copy from Price List</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_copyFromSO'>Copy from SO</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_addNewRecord'>Add</button>" +
						//"<button class='DefaultButton' style='margin: 3px;' id='btn_deleteRecord'>Delete</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_import'>Import</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_exportXls'>Export to XLS</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_exportPdf'>Export to PDF</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_exportRecord'>Export Data</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_exportRawRecord'>Export Raw Data</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_duplicatedRecord'>Duplicate Selected</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_copyRecord'>Copy Selected</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_pasteRecord'>Paste As New</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_testRecord'>Test</button>" +
					"</td><td style='text-align:right'>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_unlockRecord'>Unlock Data</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_lockRecord'>Lock Data</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_deleteRecord'>Delete</button>" +
					"</td></tr></table>" +
				"</div>" +
				
				"<div id='dialog' title='Account Code' style='width: 470px;'>" +
					"<div style='height: 30px;text-align: center;'>" +
						"<button id='addSelectedBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Add Selected</button>" +
						"<button id='selectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Select All</button>" +
						"<button id='unselectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Unselect All</button>" +
						"<button id='closeDialogBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Close</button>" +
					"</div>" +
					"<div style='height: 30px;display: flex; position: relative;'>" +
						"<input id='searchDialogTxt' type='text' class='fieldEditor textInput'>" +
						"<button id='clearSearchDialogBtn' style='width: 30%; height:22px' class='DefaultButton'>Clear</button>" +
					"</div>" +
					"<div style='display: block; overflow: auto; height: " + this.option.popHeight + "px;''>" +
						"<div id='codeTree'></div>" +
					"</div>" +
			//		"<button id='closeDialogBtn' style='width: 100%;' class='DefaultButton'>Close</button>" +
				"</div>" +
				
//				"<div id='dialogyes' title='Account Code' style='width: 470px;'>" +
//					"<div style='height: 30px;display: flex'>" +
//						"<button id='addSelectedBtn' style='width: 70%;height: 25px;' class='DefaultButton'>Add Selected</button>" +
//						"<button id='closeDialogBtn' style='width: 30%;' class='DefaultButton'>Close</button>" +
//					"</div>" +
//					"<div style='height: 30px;display: flex; position: relative;'>" +
//						"<input id='searchDialogTxt' type='text' class='fieldEditor textInput'>" +
//						"<button id='clearSearchDialogBtn' style='width: 30%; height:22px' class='DefaultButton'>Clear</button>" +
//					"</div>" +
//					"<div style='display: block; overflow: auto; height: " + this.option.popHeight + "px;''>" +
//						"<div id='codeTree'></div>" +
//					"</div>" +
//			//		"<button id='closeDialogBtn' style='width: 100%;' class='DefaultButton'>Close</button>" +
//				"</div>" +
				
				// CVS Import
				"<div id='cvsDialog' title='Import' style='display: block; width: auto; min-height: 104px; max-height: none; height: auto;' class='ui-dialog-content ui-widget-content'>" +

				"<div>Paste your data here:</div>" +
				"<div><textarea id='cvsText' style='width: 100%; resize: none;' rows='10'></textarea></div>" +
				"<div><a href='public/templates/costnet/cost_daily.xls'>You may download the template file here</a></div>" +

				"<div style='text-align: right;'>" +
				"<button id='confirmCVSDialogBtn' class='DefaultButton' style='width: 60px;'>Ok</button>" +
				"<button id='closeCVSDialogBtn' class='DefaultButton' style='width: 60px; margin-left: 6px;'>Cancel</button>" +
				"</div>" +

				"</div>" +
				
				// Loading bar
				"<div id='loadingDialog' title='Loading'>" +
				"<div id='progressbar" + this.option.counter + "'><div class='progress-label'>Loading...</div></div>" +
				"</div>" +
				
				
			"</div>";
		return layoutTemplate;
	}
	CostDailyMain.prototype.checkAccountCode = function(pcode, code){
	//	for(var i = 0; i < this.accountCodeMaps.length; i++){ 
		for (var prop in this.accountCodeMaps) {
			var ac = this.accountCodeMaps[prop];
			if ((ac.parentAccountCode==pcode) && (ac.accountCode==code)) return ac;
		}

		
		return "";
	}
	
	CostDailyMain.prototype.addSelected = function(){
		if ($(this.codeTree).jstree("get_selected").length==0){
			alert("Please select one");
		}else{
			var selectedList = $(this.codeTree).jstree("get_selected");
			var selectedCode = [];
			var count = 0;
			for(var i = 0; i < selectedList.length; i++){
				var code = selectedList[i];
				if (code.includes("c:")){
					
				}else{
					count = count +1;
					selectedCode.push(code);
					//this.addNewRecordLast(code);
				}
			}
			
			for(var i = 0; i < selectedCode.length; i++){
				var xx = selectedCode[i];
				this.addNewRecordLast(xx);
			}
			
			$(this.codeTree).jstree().deselect_all(true);
			$( this.dialog ).dialog( "close");
		}
		
	}
	
	CostDailyMain.prototype.selectAllBtnClick = function(){ 
		$(this.codeTree).jstree("check_all");
	}
	CostDailyMain.prototype.unselectAllBtnClick = function(){
		$(this.codeTree).jstree().deselect_all(true);
	}
	
	CostDailyMain.prototype.confirmImport = function(){
		var sourceText = this.cvsText.value ;
		this.processTemplateImport(sourceText);
		this.cvsText.value = "";
		$( this.cvsDialog ).dialog("close");
	}
	
	CostDailyMain.prototype.processTemplateImport = function(sourceText){
		var binding_properties = {};
		var binding_values = [];
		var importedData = [];
		var splitByLine = sourceText.split("\n");
		if (splitByLine.length >= 2){
			for(var i = 0; i < splitByLine.length; i++){
				if (i==0){
					binding_properties = splitByLine[i].split("\t");
				}else{
					var b_value = splitByLine[i].split("\t");
					binding_values.push(b_value);
				}
			}
		}
		if (!jQuery.isEmptyObject(binding_properties)){
			for(var i = 0; i < binding_values.length; i++){
				var b_values = binding_values[i];
				if (b_values.length==binding_properties.length){
					var rec = new CostDailysheet();
					rec.action = CostDailyMain.SAV;
					rec.isnew = true;
					for(var j = 0; j < binding_properties.length; j++){
						var prop = binding_properties[j];
						if(rec.hasOwnProperty(prop)){
							rec[prop] = b_values[j];
						}
					}
					var cac = this.checkAccountCode(rec.parentAccountCode, rec.accountCode);
					if (cac!=""){
						rec.accountCode = cac.accountName;
						rec.shortDescription = cac.shortDescription;
						rec.category = cac.category;
						rec.subcategory = cac.subcategory;
						rec.isTangible = cac.isTangible;
					}
					rec.itemDescription = rec.itemDescription;
					rec.afeItemDescription = rec.afeItemDescription;
					rec.lookupCompanyName = rec.lookupCompanyUid;
					rec.lookupCompanyUid = this.vendorList2[rec.lookupCompanyName];
					rec.quantity = this.unformatString(rec.quantity);
					rec.itemCost = this.unformatString(rec.itemCost);
					if (this.currencyRate[rec.currency]!=undefined){
						rec.currencyRate=this.currencyRate[rec.currency];
					}
					var pref = this.planRefMaps2.find(x => x.displayName === rec.planReferenceName);
					if (pref!=undefined){
						rec.planReference = pref.id;
					}
					rec.classCode = rec.className;
					rec.phaseCode = rec.phaseName;
					rec.taskCode = rec.taskName;
					rec.rootCauseCode = rec.rootCauseName;
					
					importedData.push(rec);
				}else{
					console.log("check data ", i , " NG");
				}
			}
			if (importedData.length>0){
				for(var i = 0; i < importedData.length; i++){
					var impData = importedData[i];
					impData.runBeforeSave();
					impData.accountCode = (impData.parentAccountCode==""?"-":impData.parentAccountCode) + " :: " + impData.accountCode;
					if (impData.isTangible== false) impData.isTangible= "No";
					if (impData.isTangible== true) impData.isTangible= "Yes";
					var d = new Date();
					var n = d.getTime();
					var parameters =
					{
					    rowID : "new" + n,
					    initdata : impData,
					    position :"last",
					};
					this.afeGrid.jqGrid('addRow',parameters);
				}
				this.show(this.rootBox);
				this.groupCat.setAttribute("disabled", true);
				this.disableItems(true);
				$(this.btn_lockRecord).addClass("hide");
				this.autoResizeGrid();
			}
		}
	}
	
	CostDailyMain.prototype.checkAccountCode = function(pcode, code){
	//	for(var i = 0; i < this.accountCodeMaps.length; i++){ 
		for (var prop in this.accountCodeMaps) {
			var ac = this.accountCodeMaps[prop];
			if ((ac.parentAccountCode==pcode) && (ac.accountCode==code)) return ac;
		}
		return "";
	}
	
	CostDailyMain.prototype.validationData = function(){
		var check = true;
		var msg = "";
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			if (rec.action==CostDailyMain.SAV){
				if ($("#" +rowIds[i] + "_accountCode").val()=="") {
					check = false;
					msg = "Account Code Field is Required";
					continue;
				}
			}
		}
		if (check==false) alert(msg);
		return check;
	}

	
	CostDailyMain.prototype.addNewRecordLast = function(codeId){
		var rec =  new CostDailysheet();
		rec.quantity = 1.0;
		rec.isnew = true;
		var cac = this.accountCodeMaps[codeId];
		rec.action = CostDailyMain.SAV;
		
		rec.parentAccountCode = cac.parentAccountCode;
		rec.accountCode = cac.accountName;
		rec.shortDescription = cac.shortDescription;
		rec.category = cac.category;
		rec.subcategory = cac.subcategory;
		rec.isTangible = cac.isTangible;
		
		var d = new Date();
		var n = d.getTime();
		
		var parameters =
		{
		    rowID : "new" + n,
		    initdata : rec,
		    position :"last",
		};
		this.afeGrid.jqGrid('addRow',parameters);
	//	this.afeGrid.addRowData("new" + n ,rec, 'last');
	//	this.afeGrid.editRow("new" + n);
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		$(this.btn_lockRecord).addClass("hide");
		this.autoResizeGrid();
	}
	
	CostDailyMain.prototype.confirmClicked = function(){ 
		if (this.validationData()){
			var rowIds = this.afeGrid.jqGrid("getDataIDs");
			var delrec = 0;
			for(var i = 0; i < rowIds.length; i++){
				var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
				this.afeGrid.jqGrid("saveRow", rowIds[i], false);
				if (rec.action==CostDailyMain.DEL) delrec ++;		
			}
			
			if (delrec==0) {
				this.saveCostDaily();
			}else{
				var wantDelete  = confirm("Are you sure you want to delete the selected record?");
				if (wantDelete) this.saveCostDaily();
			}
		}
	}
	
	CostDailyMain.prototype.saveCostDaily = function(){
		this.getProgressStatus(true, 0, "Saving...");
		var dataxml = this.getCostDailyDataXML();

		// Trying to Save through Webservice - 
		var params = {};
		params.xmldata = dataxml;
		
		var self = this;
		var url = this.baseUrl + "update_daily_cost_record";

		$.post(url, params, function(response) {
			self.hide(self.rootBox);
			self.groupCat.removeAttribute("disabled");
			self.disableItems(false);
			self.autoResizeGrid();
			self.afeDoneSave(response);
			self.getProgressStatus(false, 0, "Saving...");
		}, "xml");

	}
	
	CostDailyMain.prototype.afeDoneSave = function(response) {
	//	if (this.option.isNew==true) location.reload(); 
		this.loadCostDailyData();
	}
	
	CostDailyMain.prototype.getCostDailyDataXML = function(){  
		var xml = "<root>";
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			if (data.constructor.name!="CostDailysheet"){
				var cds = new CostDailysheet();
				var newRec = Object.assign(cds, data);
				data = newRec;
			}
			if (this.currencyRate[data.currency]!=undefined){
				data.currencyRate=this.currencyRate[data.currency];
			}
			data.runBeforeSave();	
			if (data.action==CostDailyMain.DEL){
				xml += "<CostDailysheet costDailysheetUid='"+ data.costDailysheetUid +"' deleted='true'/> ";
			}else if (data.action==CostDailyMain.SAV){
			    if (data.costDailysheetUid.includes("new")){
			    	xml += "<CostDailysheet costDailysheetUid='' ";
			    }  else {
			    	xml += "<CostDailysheet costDailysheetUid='"+ data.costDailysheetUid +"' ";
			    }
			    xml += "parentAccountCode='"+ this.getHtmlEncode(data.parentAccountCode) +"' ";
				xml += "accountCode='"+ this.getHtmlEncode(data.accountCode) +"' ";
				xml += "afeItemDescription='"+ this.getHtmlEncode(data.afeItemDescription) +"' ";
				xml += "itemDescription='"+ this.getHtmlEncode(data.itemDescription) +"' ";
				xml += "recurringItem='"+ data.recurringItem +"' ";
				xml += "isAdjusted='"+ data.isAdjusted +"' ";
				xml += "quantity='"+ data.quantity +"' ";
				xml += "itemUnit='"+ data.itemUnit +"' ";
				xml += "currency='"+ data.currency +"' ";
				xml += "category='"+ this.getHtmlEncode(data.category) +"' ";
				xml += "afeGroupName='"+ this.getHtmlEncode(data.afeGroupName) +"' ";
				xml += "planReference='"+ data.planReference +"' ";
				xml += "classCode='"+ this.getHtmlEncode(data.classCode) +"' ";
				xml += "phaseCode='"+ this.getHtmlEncode(data.phaseCode) +"' ";
				xml += "holeSize='' ";
				xml += "taskCode='"+ data.taskCode +"' ";
				xml += "referenceNumber='' ";
				xml += "rootCauseCode='"+ data.rootCauseCode +"' ";
				xml += "lookupCompanyUid='"+ data.lookupCompanyUid +"' ";
				xml += "holeSectionUid='' ";
				xml += "isTangible='"+ data.isTangible +"' ";
				xml += "itemCost='"+ data.itemCost +"' ";
				xml += "itemTotal='"+ data.itemTotal +"' "; 
				xml += "pstCurrency='0' ";
				xml += "gstCurrency='0' ";
				xml += "ticketNumber='' ";
				xml += "chargeScope='' ";
					
				xml += "/>";
			}
		};

		xml += "</root>";
		return xml;
	}
	
	CostDailyMain.prototype.cancelClicked = function(){
		var rowIds = this.afeGrid.jqGrid("getDataIDs");

		for(var i = 0; i < rowIds.length; i++){
			var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			rec.action = "";
			this.afeGrid.jqGrid("restoreRow", rowIds[i]);
		}
		this.afeGrid.jqGrid('resetSelection');
		
		this.currentAction="";
		this.hide(this.rootBox);
		this.groupCat.removeAttribute("disabled");
		$(this.btn_lockRecord).removeClass("hide");
		this.disableItems(false);
		this.loadCostDailyData();
		this.autoResizeGrid();
	}
	
	CostDailyMain.prototype.onDataFieldChange = function(e){
	//	if (this.isLocking) return; 
		var pr = e.target.attributes.getNamedItem('rowid').ownerElement.parentElement.parentElement; 
		$(pr).removeClass("DeleteRecordBackground");
		var rid = e.target.attributes.getNamedItem('rowid').value;
		console.log("onDataFieldChange", e.keyCode + "  " + rid);
		var field = e.currentTarget.getAttribute("name");
		var rec = this.afeGrid.jqGrid('getLocalRow', rid);

		// === Lookup Field Setting == 
		if (field=="accountCode") {
			if (e.currentTarget.value==""){
				rec.parentAccountCode = "";
				rec.category = "";
				rec.subcategory = "";
				rec.isTangible = "";
			}else{
				rec.parentAccountCode = this.accountCodeMaps[e.currentTarget.value].parentAccountCode;
				rec.category = this.accountCodeMaps[e.currentTarget.value].category;
				rec.subcategory = this.accountCodeMaps[e.currentTarget.value].subcategory;
				rec.isTangible = this.accountCodeMaps[e.currentTarget.value].isTangible;	
			}
		}
		if (field=="lookupCompanyName") {
			rec.lookupCompanyUid = e.currentTarget.value;
			if (rec.lookupCompanyUid == "") {
				rec.lookupCompanyName = "";
				rec.action = CostDailyMain.SAV;
				console.log("updated");
			}else{
				rec.lookupCompanyName = this.vendorList[rec.lookupCompanyUid];
			}
		}
		if (field=="planReferenceName") {
			rec.planReference = e.currentTarget.value;  
			if (this.planRefMaps[e.currentTarget.value]==undefined){
				rec.phaseCode = "";
			}else{
				rec.phaseCode = this.planRefMaps[e.currentTarget.value].phaseCode;
			}	
		}
		if (field=="className") {
			rec.classCode = e.currentTarget.value;
		}
		if (field=="phaseName") {
			rec.phaseCode = e.currentTarget.value;
		}
		if (field=="taskName") {
			rec.taskCode = e.currentTarget.value;
		}
		if (field=="rootCauseName") {
			rec.rootCauseCode = e.currentTarget.value;
		}
		if (field=="currency") {
			rec.currencyRate = this.currencyRate[e.currentTarget.value];
		}
		if(rec[field] != e.currentTarget.value){
			rec.action = CostDailyMain.SAV;
			console.log("updated");
		}
	}
	
	CostDailyMain.prototype.onInitLookupFieldSelection = function(name){
		return this.onInitRecurringItemOptions("recurringItem") && this.onInitAccountCodeOptions("accountCode");
	}
	
	CostDailyMain.prototype.onInitRecurringItemOptions = function(name){
		this.afeGrid.jqGrid('setColProp', name, {
			editoptions: {
				value:{"true": "Yes", "false": "No"}
			}
		});
		console.log("done onInitRecurringItemOptions");
		return true;
	}
	
	CostDailyMain.prototype.onInitAccountCodeOptions = function(name){
		$.get({
			url: "getaccountcodeslist.xml",
			dataType: "xml",
			context: this
		}).done(function(data){
			var options = {};
			var list = data.documentElement.getElementsByTagName("CostAccountCodes");
			for(var i=0; i<list.length; i++){
				var accountCode = "";
				accountCode = list[i].getElementsByTagName("accountCode")[0].textContent + " :: " +
				list[i].getElementsByTagName("shortDescription")[0].textContent +
				(list[i].getElementsByTagName("chargeScope")[0].textContent == "" ? "" : " :: " + list[i].getElementsByTagName("chargeScope")[0].textContent);
				options[list[i].getElementsByTagName("costAccountCodesUid")[0].textContent] = accountCode;
			}
			this.afeGrid.jqGrid('setColProp', name, {editoptions: {value:options}});
			console.log("done onInitAccountCodeOptions");
			return false;
		}).fail(function(){
			return false;
		}).always(function(){
			console.log("always onInitAccountCodeOptions");
			return false;
		});
	}

	CostDailyMain.prototype.getUsersession = function(){
		var page = this.baseUrl + "get_session_id";
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var currentU = result.documentElement.getElementsByTagName("currentUser")[0].textContent;
				self.currentUser = currentU;
				var currentOperation = result.documentElement.getElementsByTagName("currentOperation")[0].textContent;
				var currentDay = result.documentElement.getElementsByTagName("currentDay")[0].textContent;
				
				self.currentOps = currentOperation;
				self.currentDay = currentDay;
				
				var allowLock = result.documentElement.getElementsByTagName("allowLock")[0].textContent;
				var isLock = result.documentElement.getElementsByTagName("isLock")[0].textContent;
				if (allowLock=="true") self.allowLocking = true;
				if (isLock=="true") self.isLocking = true;
			}
		}).done(function(data, textStatus, jqXHR) {
			if (self.currentUser=='jwong') {
				$(self.btn_exportRecord).removeClass("hide"); 
				$(self.btn_exportRawRecord).removeClass("hide"); 
//				$(this.btn_deleteSelected).removeClass("hide"); 
				$(self.btn_import).removeClass("hide"); 
//				$(this.btn_exportXls).removeClass("hide"); 
//				$(this.btn_exportPdf).removeClass("hide"); 
//				$(this.btn_test).removeClass("hide"); 
//				$(self.btn_copyRecord).removeClass("hide"); 
//				$(self.btn_pasteRecord).removeClass("hide"); 
				$(self.btn_copyFromDay).removeClass("hide"); 
				$(self.btn_copyFromAFE).removeClass("hide"); 
				$(self.btn_testRecord).removeClass("hide"); 
				$(self.btn_copyRecord).removeClass("hide"); 
			}
			console.log(">> Get Session END ");
		});
	}
	
	CostDailyMain.prototype.getLookupList = function(){
		console.log(">> getLookupList Start ");
		this.lookupList = [];
		var page = this.baseUrl + "get_all_lookups";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				$(result).find('itemUnit').each(function(){
					var headerText;
					var model = {};
					var code = $(this).get(0).getAttribute("code");
					var label = $(this).get(0).getAttribute("label");
					options[code] = label;
				});
				self.lookupList = options;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> getLookupList END ");
			self.getCurrencyList();
		});
	}
	
	CostDailyMain.prototype.getCurrencyList = function(){
		console.log(">> getCurrencyList");
		this.currencyList = [];
		this.currencyRate = [];
		var page = this.baseUrl + "get_currency_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var option2 = {};
				var list = result.documentElement.getElementsByTagName("CostCurrencyLookup");
				for(var i=0; i<list.length; i++){
					var currencySymbol = list[i].getElementsByTagName("currencySymbol")[0].textContent;
					var conversionRate = list[i].getElementsByTagName("conversionRate")[0].textContent;
					var isP = list[i].getElementsByTagName("isPrimaryCurrency")[0].textContent
					if (options[currencySymbol] == null) {
						options[currencySymbol] = currencySymbol;
						option2[currencySymbol] = conversionRate;
						if (isP=="true") self.primaryCurrency = currencySymbol;
					}
				}
				self.currencyList = options;
				self.currencyRate = option2;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> getCurrencyList Done");
			self.getAccountCodeList();
		});
	}
	
	CostDailyMain.prototype.getAccountCodeList = function(){
		console.log(">> GetAccountCodeList");
		this.accountCodeList = [];
		this.accountCodeMaps = [];
		var page = this.baseUrl + "get_account_codes_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var option3 = {};
				var catGroup = [];
				var catGroups = {};
				var catGroupx = {};
				options[""] = "";
				var list = result.documentElement.getElementsByTagName("CostAccountCodes");
				for(var i=0; i<list.length; i++){
					var costAccountCodesUid = list[i].getElementsByTagName("costAccountCodesUid")[0].textContent;
					var parentAccountCode = list[i].getElementsByTagName("parentAccountCode")[0].textContent;
					var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
					var shortDescription = list[i].getElementsByTagName("shortDescription")[0].textContent;
					var category = list[i].getElementsByTagName("category")[0].textContent; 
					var subcategory = list[i].getElementsByTagName("subcategory")[0].textContent;
					var isTangible = list[i].getElementsByTagName("isTangible")[0].textContent; 
					var cac = new CostAccountCodes();
					cac.costAccountCodesUid = costAccountCodesUid;  
					cac.parentAccountCode = parentAccountCode;
					cac.accountCode = accountCode;
					cac.shortDescription = shortDescription;
					cac.category = category;
					cac.subcategory = subcategory;
					cac.isTangible = (isTangible=="false"?false:true);
					cac.run();
					
					options[costAccountCodesUid] = cac.accountName;
					option3[costAccountCodesUid] = cac;
					if (catGroup.indexOf(category) == -1) catGroup.push(category);
					if (catGroups[category]==undefined){
						catGroups[category] = [];
						catGroups[category].push(subcategory);
					}else{
						if (catGroups[category].indexOf(subcategory) == -1) catGroups[category].push(subcategory);
					}
				}
				self.accountCodeList = options;
				self.accountCodeMaps = option3;
				
				var treeSection = "<ul>";
				for (var j=0; j < catGroup.sort().length; j++){
					treeSection = treeSection + "<li  id='c:" + (catGroup[j]==""?"Uncategorized":catGroup[j]) + "' >" + (catGroup[j]==""?"Uncategorized":catGroup[j]);
					treeSection = treeSection + "<ul>";
					var checkCat = catGroup[j];
					
					if ((catGroups[checkCat].length==1) && (catGroups[checkCat][0]=="")){
						for(var i=0; i<list.length; i++){
							var parentAccountCode = list[i].getElementsByTagName("parentAccountCode")[0].textContent;
							var costAccountCodesUid = list[i].getElementsByTagName("costAccountCodesUid")[0].textContent;
							var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
							var shortDescription = list[i].getElementsByTagName("shortDescription")[0].textContent;
							var codeName = (parentAccountCode==""?"-":parentAccountCode) + " :: " + (accountCode==""?"-":accountCode) + " :: " + shortDescription;
							var category = list[i].getElementsByTagName("category")[0].textContent; 
							if (checkCat == category){
								treeSection = treeSection + "<li id='" + costAccountCodesUid + "'>" + codeName + "</li>";
							}
						}
					}else{
						for (var k=0; k < catGroups[checkCat].length; k++){
							var subcat = catGroups[checkCat][k];
							treeSection = treeSection + "<li  id='cs:" + (subcat==""?"-":subcat) + "' >" + (subcat==""?"-":subcat);
							treeSection = treeSection + "<ul>";
							
							for(var i=0; i<list.length; i++){
								var costAccountCodesUid = list[i].getElementsByTagName("costAccountCodesUid")[0].textContent;
								var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
								var shortDescription = list[i].getElementsByTagName("shortDescription")[0].textContent;
								var codeName = accountCode+" :: "+shortDescription;
								var category = list[i].getElementsByTagName("category")[0].textContent; 
								var subcategory = list[i].getElementsByTagName("subcategory")[0].textContent; 
								if ((checkCat == category) && (subcat == subcategory)){
									treeSection = treeSection + "<li id='" + costAccountCodesUid + "'>" + codeName + "</li>";
								}
							}
							treeSection = treeSection + "</ul>";
							treeSection = treeSection + "</li>";
						}
					}
					treeSection = treeSection + "</ul>";
					treeSection = treeSection + "</li>";
				}
				treeSection = treeSection + "</ul>";
				self.codeTree.appendChild($(treeSection).get(0));
				
				$(self.codeTree).jstree({
					"core" : {

				        "themes":{
				            "icons":false
				        }
				    },
					"search": {
						'show_only_matches' : true,
				        'show_only_matches_children' : true,
				        'search_leaves_only' : true
				    },
			        "checkbox": {
			            "keep_selected_style": false,
			          //  "three_state" : false,
			        },
			            "plugins": ["checkbox","search"]
			    })
			    .bind("loaded.jstree", function (event, data) {
			      //  $(this).jstree("open_all");
			    });
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetAccountCodeList Done");
			self.getVendorList();
		});
	}
	
	CostDailyMain.prototype.getVendorList = function(){
		console.log(">> GetVendorList");
		this.vendorList = [];
		this.vendorList2 = [];
		this.vendorListInactive = [];
		var page = this.baseUrl + "get_vendor_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var options2 = {};
				options[""] = "";
				$(result).find('LookupCompany').each(function(){
					var headerText;
					var model = {};
					var lookupCompanyUid = $(this).get(0).getAttribute("lookupCompanyUid");
					var companyName = $(this).get(0).getAttribute("companyName");
					options[lookupCompanyUid] =  companyName;
					options2[companyName] =  lookupCompanyUid;
				});
				self.vendorList = options;
				self.vendorList2 = options2;
				var xx = "";
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetVendorList Done");
			self.getAFEGroup(); 
		});
	}
	
	CostDailyMain.prototype.getAFEGroup = function(){ 
		console.log(">> GetAFEGroupList");
		this.afeGroupList = [];
		this.afeGroupSCList = [];
		
		var page = this.baseUrl + "get_afe_group_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				options[""] = "";
				var optionsSC = {};
				var list = result.documentElement.getElementsByTagName("CostAfeGroup");
				for(var i=0; i<list.length; i++){
					var afeGroupName = list[i].getElementsByTagName("afeGroupName")[0].textContent;
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var description = list[i].getElementsByTagName("description")[0].textContent;
					options[afeGroupName] =  afeGroupName;
					optionsSC[afeGroupName] =  shortCode;
				}
				self.afeGroupList = options;
				self.afeGroupSCList = optionsSC;
				var xx = "";
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetAFEGroupList Done");
			self.getPlanReferenceList(); 
		});
	}
	
	CostDailyMain.prototype.getPlanReferenceList = function(){ 
		console.log(">> GetPlanReference");
		this.planRefList = [];
		this.planRefAList = [];
		this.planRefereList = [];
		
		this.planRefMaps = [];
		this.planRefMaps2 = [];	
		
		var page = this.baseUrl + "get_plan_reference_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var optionsA = {};
				var optionsMaps = {};
				options[""] = "";
				optionsA[""] = "";
				var list = result.documentElement.getElementsByTagName("OperationPlanPhase");
				for(var i=0; i<list.length; i++){
					var id = list[i].getElementsByTagName("id")[0].textContent;
					var sequence = list[i].getElementsByTagName("sequence")[0].textContent;
					var phaseCode = list[i].getElementsByTagName("phaseCode")[0].textContent;
					var phaseName = list[i].getElementsByTagName("phaseName")[0].textContent;
					var use = list[i].getElementsByTagName("use")[0].textContent;
					
					var opp = new OperationPlanPhase();
					opp.id = id;
					opp.sequence = sequence;
					opp.phaseCode = phaseCode;
					opp.phaseName = phaseName;
					opp.use = use;
					opp.setDisplayName();
					
					optionsMaps[id] = opp;
					self.planRefMaps2.push(opp);
					
					options[id] =  opp.displayName;
					if (use=="1") optionsA[id] = opp.displayName;
				}

				self.planRefList = options;
				self.planRefAList = optionsA;
				self.planRefereList = options;
				
				self.planRefMaps = optionsMaps;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetPlanReference Done");
			self.getActivityCodesList(); 
		});
	}
	
	CostDailyMain.prototype.getActivityCodesList = function(){
		console.log(">> Get Activity Code List");
		this.clsCodeList = [];
		this.phsCodeList = [];
		this.opsCodeList = [];
		this.rcCodeList = [];
		var page = this.baseUrl + "get_activity_codes_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				options[""] = "";
				var list = result.documentElement.getElementsByTagName("LookupClassCode");
				for(var i=0; i<list.length; i++){
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var name = list[i].getElementsByTagName("name")[0].textContent;
					options[shortCode] =  name;
				}
				self.clsCodeList = options;
				
				options = {};
				options[""] = "";
				list = result.documentElement.getElementsByTagName("LookupPhaseCode");
				for(var i=0; i<list.length; i++){
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var name = list[i].getElementsByTagName("name")[0].textContent;
					options[shortCode] =  name;
				}
				self.phsCodeList = options;
				
				options = {};
				options[""] = "";
				list = result.documentElement.getElementsByTagName("LookupTaskCode");
				for(var i=0; i<list.length; i++){
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var name = list[i].getElementsByTagName("name")[0].textContent;
					options[shortCode] =  name;
				}
				self.opsCodeList = options;
				
				options = {};
				options[""] = "";
				list = result.documentElement.getElementsByTagName("LookupRootCauseCode");
				for(var i=0; i<list.length; i++){
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var name = list[i].getElementsByTagName("name")[0].textContent;
					options[shortCode] =  name;
				}
				self.rcCodeList = options;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> Get Activity Code List Done");
			self.retrieveColumnSetting(); 
		});
	}
	
	CostDailyMain.prototype.retrieveColumnSetting = function (){
		this.colModel = [];
		this.colNames = [];
		var page = this.baseUrl + "get_daily_cost_columns";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				$(result).find('costDailysheet').find('columns').each(function(){
					var headerText;
					var model = {};
					var dataField = $(this).get(0).getAttribute("dataField");
					var headerText = $(this).get(0).getAttribute("headerText");

					model = self.checkField(dataField, headerText);
					
					self.colNames.push(dataField);
					self.colModel.push(model);
				});
				
				self.colModel.push({
					name: 'costDailysheetUid',
					key: true,
					label: 'costDailysheetUid',
					hidden: true,
					width: 30
				});
				self.colNames.push("costDailysheetUid");
				
				self.colModel.push({
					name: 'lookupCompanyUid',
					label: 'lookupCompanyUid',
					hidden: true,
				});
				self.colNames.push("lookupCompanyUid");
				
				self.colModel.push({
					name: 'planReference',
					label: 'planReference',
					hidden: true,
				});
				self.colNames.push("planReference");
				
				self.colModel.push({
					name: 'parentAccountCode',
					label: 'parentAccountCode',
					hidden: true,
				});
				self.colNames.push("parentAccountCode");

				self.colModel.push({
					name: 'classCode',
					label: 'classCode',
					hidden: true
				});
				self.colNames.push("classCode");
				
				self.colModel.push({
					name: 'phaseCode',
					label: 'phaseCode',
					hidden: true
				});
				self.colNames.push("phaseCode");	
				
				self.colModel.push({
					name: 'taskCode',
					label: 'taskCode',
					hidden: true
				});
				self.colNames.push("taskCode");	
				
				self.colModel.push({
					name: 'rootCauseCode',
					label: 'rootCauseCode',
					hidden: true
				});
				self.colNames.push("rootCauseCode");
				
				self.colModel.push({
					name: 'action',
					label: 'action',
					hidden: true
				});
				self.colNames.push("action");	
			}
		}).done(function(data, textStatus, jqXHR) {
			//self.loadAfeData();
			self.loadCostDailyData();
		});
		
		console.log("CostAfeMaster.retrieveColumnSetting done");
		console.log(this.colModel);
	}
	
	CostDailyMain.prototype.getBasicLookName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;

	    $.each(options.colModel.editoptions.value, function (k, value)
	    {
	        if (cellValue == k)
	        {
	            unformatValue = value;
	        }
	    });

	    return unformatValue;
	}
	
	CostDailyMain.prototype.getCodeName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;
		var n = unformatValue.indexOf("::");
		if (n<=0){
			unformatValue = unformatValue + " :: "+ rowObject["shortDescription"];
		}

	    return unformatValue;
	}
	
	CostDailyMain.prototype.getVendorName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;

	    $.each(options.colModel.editoptions.value, function (k, value)
	    {
	        if (cellValue == k)
	        {
	            unformatValue = value;
	        }
	    });

	    return unformatValue;
	}
	
	CostDailyMain.prototype.loadCostDailyData = function(){
		console.log("Load Cost daily Sheet Start");
		var page = this.baseUrl + "get_cost_dailysheet_list";
		var self = this;
		this.costDailyList = [];

		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var list = result.documentElement.getElementsByTagName("CostDailysheet");
				for(var i=0; i<list.length; i++){
					var rec = new CostDailysheet();
					for(var j in list[i].children){
						var prop = list[i].children[j].tagName;
						if(rec.hasOwnProperty(prop)){
							if(list[i].children[j].hasChildNodes()){
								rec[prop] = list[i].children[j].childNodes[0].nodeValue;
								if(prop == "recurringItem"){
									//rec[prop] = (list[i].children[j].childNodes[0].nodeValue ? "t" : "f");
									//console.log("rec[prop]", rec[prop]);
								}
							}
						}
					}
				//	rec.accountCode= rec.accountCode + " :: " + rec.afeShortDescription;  
					rec.planReferenceName= self.planRefList[rec.planReference];
					rec.lookupCompanyName=self.vendorList[rec.lookupCompanyUid];
					rec.currencyRate=self.currencyRate[rec.currency];
					rec.className = self.clsCodeList[rec.classCode];
					rec.phaseName = self.phsCodeList[rec.phaseCode];
					rec.taskName = self.opsCodeList[rec.taskCode];
					rec.rootCauseName = self.rcCodeList[rec.rootCauseCode];
					rec.run();
					self.costDailyList.push(rec);
				}
				
				var wellCostToDate = result.documentElement.getElementsByTagName("wellCostToDate")[0].textContent;
				var reportDailyDayCost = result.documentElement.getElementsByTagName("reportDailyDayCost")[0].textContent;
			//	var cumCost = parseFloat(wellCostToDate) + parseFloat(reportDailyDayCost);
				var cumCost = parseFloat(wellCostToDate);
				self.sumCost.cumcost = cumCost;
				self.sumCost.daycost = parseFloat(reportDailyDayCost);
				self.data2.textContent = "Cum Cost: " + self.primaryCurrency + " " + self.numberWithCommas(cumCost);
				self.data4.textContent = "Day Total: " + self.primaryCurrency + " " + self.numberWithCommas(reportDailyDayCost);
			}
		}).done(function(data, textStatus, jqXHR) {
			self.loadAFEData();
		});	
		console.log("Load Cost daily Sheet END ");
	}
	
	CostDailyMain.prototype.loadAFEData = function(){
		var page = this.baseUrl + "get_afe_listing";
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var list = result.documentElement.getElementsByTagName("CostAfeMaster");
				var sumtotal = 0.0;
				for(var i=0; i<list.length; i++){
					var atotal =list[i].getElementsByTagName("afeTotal")[0].textContent;
					var afeType =list[i].getElementsByTagName("afeType")[0].textContent;
					if (afeType!="PRICELIST" && afeType!="SO") sumtotal = sumtotal + parseFloat(atotal);
				}	
				self.sumCost.afesum = sumtotal;
				self.data1.textContent = "AFE: " + self.primaryCurrency + " " + self.numberWithCommas(sumtotal);
				var balance = sumtotal - self.sumCost.cumcost;
				self.data3.textContent = "Balance: " + self.primaryCurrency + " " + self.numberWithCommas(balance);
			}
		}).done(function(data, textStatus, jqXHR) {
			//self.createGrid();
			self.editDataEnable();
		});	
	}
	
	CostDailyMain.prototype.editDataEnable = function(){ 
		if (this.isLocking){
			$(this.btn_getAccountCode).addClass("hide"); 
			$(this.btn_copyFromDay).addClass("hide"); 
			$(this.btn_copyFromAFE).addClass("hide"); 
			$(this.btn_copyFromPrice).addClass("hide"); 
			$(this.btn_copyFromSO).addClass("hide"); 
			$(this.btn_addNewRecord).addClass("hide"); 
			$(this.btn_import).addClass("hide"); 
			
			$(this.btn_copyRecord).addClass("hide"); 
			$(this.btn_pasteRecord).addClass("hide"); 
			$(this.btn_deleteRecord).addClass("hide"); 
			$(this.btn_duplicatedRecord).addClass("hide"); 	
			
			if (this.allowLocking){
			//	$(this.btn_lockRecord).addClass("hide"); 	
				$(this.btn_unlockRecord).removeClass("hide"); 	
			}else{

			}

		}else{
			if (this.allowLocking){
				$(this.btn_lockRecord).removeClass("hide"); 
			}else{

			}	
		}
		this.createGrid();
	}
	
	CostDailyMain.prototype.createGrid = function(){
		console.log("Create Grid");
		var pHeight = 400;
		var enable = false;
		if (this.currentUser=="jwong"){enable = true; };
		if (this.afeGrid == null){
			this.afeGrid = $(this.gridCostDetail).jqGrid({
				data: this.costDailyList,
			    datatype: 'local',
			//    height: pHeight - 146 - 60,
			    height: this.height,
	//		    colNames: co1,
			    colModel: this.colModel,
				guiStyle: 'bootstrap4',
				altRows: false, /*if guiStyle != bootstrap style, need to implicitly set altclass prop to style the alternate row */
				
			//	altclass: 'myAltRowClass',
				
				// >> pager setting
				pager: 'jqGridPager',
				recordtext: "{2} records",
				emptyrecords: "Nothing to display",
				rowList: [],
				pgbuttons: false,
				pgtext: null,
				viewrecords: true,
				// << pager setting
				colMenu : enable,
				
				sortable: true,
				headertitles: true,
				rowNum: 500,
				shrinkToFit: true,
				autowidth: true,
				multiselect: true, /* enable leftmost checkbox column*/
				viewrecords: true,
				multiSort: true,
				grouping: true,
				ondblClickRow: this.onDblClickGridRow.bind(this),
				afterEditCell: function (id,name,val,iRow,iCol){
					alert("afteredit");
				},
				beforeSubmit: function(postdata, formid){ 
					alert("beforeSubmit");
				},
				afterSubmitCell: function(serverStatus, aPostData) {
					var response = serverStatus.responseText;
					  alert(response);
				},
				loadComplete: function() {
					// $("tr.jqgrow:odd").addClass('myAltRowClass');
				},
				gridComplete: function(){
					var rows = $(this).getDataIDs(); 
				    for (var i = 0; i < rows.length; i++)
				    {
				        var recurringItem = $(this).getCell(rows[i],"action");
				        if(recurringItem == "Del")
				        {
				        //	 $(this).jqGrid('setRowData',rows[i],false, "DeleteRecordBackground"); 
				        //	$(this).jqGrid('setRowData',rows[i],false, {  color:'white',weightfont:'bold',background:'blue'});            
				        }else{
				    //    	$(this).jqGrid('setRowData',rows[i],false, {  color:'white',weightfont:'bold',background:'red'});   
				        }
				    //    $(this).jqGrid('setRowData',rows[i],false, "DeleteRecordBackground"); 
				    //    $(this).jqGrid('setRowData',rows[i],false, {  color:'white',weightfont:'bold',background:'blue'});
				       // $(this).jqGrid('setRowData',rows[i],false, {  color:'white',weightfont:'bold',background:'blue'});
				    }
				},
				checkValues: function(val, valref,g, customobject, nam) {
					
						alert("Check");
				},
				inlineEditing: {
		            keys: true,                        
		            saveRowValidation: function (options) {
		                var newData = options.newData;
		                alert("inlineEditing");
		                newData.itemCost = String(newData.itemCost).replace(",", "");
		                newData.estimatedDays = String(newData.estimatedDays).replace(",", "");
		                newData.quantity = String(newData.quantity).replace(",", "");
		                return true;
		            }
		        }
			});
			if (this.currentUser=="jwong"){
				this.afeGrid.jqGrid('navGrid','#jqGridPager' ,{del:false,add:false,edit:false,search:true},{},{},{},{multipleSearch:true});
			}else{
				this.afeGrid.jqGrid('navGrid','#jqGridPager' ,{del:false,add:false,edit:false,search:false},{},{},{},{multipleSearch:true});
			}
			this.afeGrid.jqGrid('filterToolbar',
		        {
				  autosearch: true,
				  stringResult: true,
				  searchOnEnter: true,
				  defaultSearch: "cn"          
				});
		} else{
			this.afeGrid.jqGrid('clearGridData');
			this.afeGrid.jqGrid('setGridParam', {data: this.costDailyList});
			this.afeGrid.trigger('reloadGrid');
		}
		//$.LoadingOverlay("hide");
		this.getProgressStatus(false, false, "Loading..");
		// $(this.laodbar).dialog("close");
		this.autoResizeGrid();
	}
	
	CostDailyMain.prototype.autoResizeGrid = function(){
		var n = this.afeGrid.get(0).parentNode;
		while(n){
			if(n.className && n.className.indexOf("ui-jqgrid-bdiv") != -1) break;
			n = n.parentNode;
		}
		if(n){
			if(document.body.scrollHeight > document.body.clientHeight){
				this.afeGrid.jqGrid("setGridHeight", n.offsetHeight - (document.body.scrollHeight - document.body.clientHeight));
			}else{
				var extra = document.body.clientHeight - this.jqGridMain.parentNode.offsetHeight;
				this.afeGrid.jqGrid("setGridHeight", n.offsetHeight + extra);
			}
		}
	}
	
	CostDailyMain.prototype.onDblClickGridRow = function (rowid, iRow, iCol, e){
		if (this.isLocking) return;
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		$(this.btn_lockRecord).addClass("hide"); 
		this.autoResizeGrid();
		console.log("onDblClickGridRow", arguments);

		var oneditfunc = function(){
			console.log(" >> oneditfunc");
		};
		
		
		var successfunc = function(){
			console.log(" >> successfunc", arguments);
		};
		
		var aftersavefunc = function(){
			console.log(" >> aftersavefunc");
			console.log("getLocalRow", this.afeGrid.jqGrid('getLocalRow', rowid));

		};
		
		var errorfunc = function(){
			console.log(" >> errorfunc", arguments);
		};
		
		var afterrestorefunc = function(){
			console.log(" >> afterrestorefunc", arguments);
		};
		
	//	this.afeGrid.jqGrid('editRow', rowid, false, oneditfunc.bind(this), successfunc.bind(this), null, null, aftersavefunc.bind(this), errorfunc.bind(this), afterrestorefunc.bind(this));
		this.afeGrid.jqGrid('setRowData',rowid,false, { });
		this.afeGrid.jqGrid('editRow', rowid, true);
	}
	
	CostDailyMain.prototype.addNewRecord = function(){
		var rec =  new CostDailysheet();
		rec.quantity = 1.0;
		rec.isnew = true;
		
		rec.currency = this.primaryCurrency;
		rec.currencyRate = 1.0;
		
		var d = new Date();
		var n = d.getTime();
		
		var parameters =
		{
		    rowID : "new" + n,
		    initdata : rec,
		    position :"last",
		};
		this.afeGrid.jqGrid('addRow',parameters);
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		$(this.btn_lockRecord).addClass("hide"); 
		this.autoResizeGrid();
	}
	
	CostDailyMain.prototype.exportRecords = function(){ 
		var fileName = this.currentOps + "_Cost_" + this.currentDay;
		this.exportToExcel(fileName);
	}

	CostDailyMain.prototype.exportRawRecords = function(){ 
		var htmls = "<div><table id='tests'>" +
				"<tr>" +
	    			"<td style='font-weight: bold;width:100px'>IsTangible</td>" +
	    			"<td style='font-weight: bold;width:100px'>CostCategory</td>" +
	    			"<td style='font-weight: bold;width:140px'>CostCategoryCode</td>" +
	    			"<td style='font-weight: bold;width:140px'>ParentAccountCode</td>" +
	    			"<td style='font-weight: bold;width:100px'>AccountCode</td>" +
	    			"<td style='font-weight: bold;width:140px'>AccountCodeDesc</td>" +
	    			"<td style='font-weight: bold;width:100px'>Vendor</td>" +
	    			"<td style='font-weight: bold;width:140px'>ItemComment</td>" +
	    			"<td style='font-weight: bold;width:140px'>ItemDescription</td>" +
	    			"<td style='font-weight: bold;width:100px'>AfeGroupName</td>" +
	    			"<td style='font-weight: bold;width:100px'>AfeGroupCode</td>" +
	    			"<td style='font-weight: bold;width:100px'>Qty</td>" +
	    			"<td style='font-weight: bold;width:100px'>ItemUnit</td>" +
	    			"<td style='font-weight: bold;width:100px'>Currency</td>" +
	    			"<td style='font-weight: bold;width:100px'>ItemCost</td>" +
	    			"<td style='font-weight: bold;width:100px'>Total</td>" +
	    			"<td style='font-weight: bold;width:100px'>TotalInPriCur</td>" +
				"</tr> ";
		
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var datas = [];
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			var res = data.accountCode.split(" :: ");
			
			htmls = htmls + "<tr>";
				
				if (data.isTangible=="Yes") htmls = htmls + "<td style='width:100px'>Y</td>";
				else htmls = htmls + "<td style='width:100px'>N</td>";
				
				htmls = htmls + 
				"<td style='width:100px'>" + data.category + "</td>" +
				"<td style='width:140px'>" + data.category.substring(0, 1) + "</td>" +
				"<td style='width:140px'>" + data.parentAccountCode + "</td>" +
				"<td style='width:100px'>" + res[1] + "</td>" +
				"<td style='width:140px'>" + data.afeShortDescription +"</td>" +
				"<td style='width:100px'>" + data.lookupCompanyName + "</td>" +
				"<td style='width:140px'>" + data.afeItemDescription + "</td>" +
				"<td style='width:140px'>" + data.itemDescription + "</td>" +
				"<td style='width:100px'>" + data.afeGroupName + "</td>";
				if (this.afeGroupSCList[data.afeGroupName]!=null) htmls = htmls +  "<td style='width:100px'>" + this.afeGroupSCList[data.afeGroupName] + "</td>";
				else htmls = htmls +  "<td style='width:100px'>  </td>";
				
				htmls = htmls + 
				"<td style='width:100px'>" + Number(data.quantity).toFixed(2) +"</td>" +
				"<td style='width:100px'>" + data.itemUnit + "</td>" +
				"<td style='width:100px'>" + data.currency + "</td>" +
				"<td style='width:100px'>" + Number(data.itemCost).toFixed(2) + "</td>" +
				"<td style='width:100px'>" + Number(data.itemTotal).toFixed(2) + "</td>" +
				"<td style='width:100px'>" + Number(data.itemTotalInPrimaryCurrency).toFixed(2) + "</td>" +
			"</tr> ";
		}
		
		htmls = htmls + "</table></div>";
		
		var container = $(htmls).get(0);
		var con = $(container).find("#tests").get(0);
		
		var fmtStr = '@'; 
		var fmt = '0.00';
		var wb = XLSX.utils.table_to_book(con, {raw : true} ); 
		var range = XLSX.utils.decode_range(wb.Sheets['Sheet1']['!ref']);
		for (var r = range.s.r; r <= range.e.r; r++) {
			  for (var c = range.s.c; c <= range.e.c; c++) {
				  var cellName = XLSX.utils.encode_cell({ c: c, r: r });
			  }
			  var cellName = XLSX.utils.encode_cell({ c: 4, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmtStr;
			  
			  cellName = XLSX.utils.encode_cell({ c: 11, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 14, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 15, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 16, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			
		}
		
		var fileName = this.currentOps + "_RawCost_" + this.currentDay;
		XLSX.writeFile(wb, fileName + ".xlsx");
	}
	
	CostDailyMain.prototype.exportRawxxRecords = function(){ 
		var csv = [];
		var header = ["IsTangible", "CostCategory", "CostCategoryCode", "ParentAccountCode", "AccountCode", "AccountCodeDesc", "Vendor", "ItemComment", "ItemDescription"];
		header.push("AfeGroupName");
		header.push("AfeGroupCode");
		header.push("Qty");header.push("ItemUnit");
		header.push("Currency");header.push("ItemCost");header.push("Total");
		header.push("TotalInPriCur");
		csv.push(header.join(","));
		
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var datas = [];
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			var res = data.accountCode.split(" :: ");
			
			if (data.isTangible=="Yes") datas.push("Y");
			else datas.push("N");
		//	datas.push(data.isTangible);   // "IsTangible"
			datas.push(data.category); // "CostCategory"
			datas.push(data.category); // "CostCategoryCode"
			datas.push(data.parentAccountCode); // "ParentAccountCode"
			datas.push(res[1]); // "AccountCode"
			datas.push(data.afeShortDescription.replace(/,/g, '.') ); // "AccountCodeDesc"
			datas.push(data.lookupCompanyName); //"Vendor"
			datas.push(data.afeItemDescription.replace(/,/g, '.') );  // "ItemDescription"
			datas.push(data.itemDescription.replace(/,/g, '.') );  // "ItemDescription"
			datas.push(data.afeGroupName);  // "AfeGroupName"
			datas.push(this.afeGroupSCList[data.afeGroupName]);  // "AfeGroupCode"
			datas.push(Number(data.quantity).toFixed(2));  // "Qty"
			datas.push(data.itemUnit);  // "ItemUnit"
			datas.push(data.currency);  // "Currency"
			datas.push(Number(data.itemCost).toFixed(2));  // "ItemCost"
			datas.push(Number(data.itemTotal).toFixed(2));  // "Total"
			datas.push(Number(data.itemTotalInPrimaryCurrency).toFixed(2));  // "TotalInPriCur"  
			
			csv.push(datas.join(","));
		}
		var fileName = this.currentOps + "_RawCost_" + this.currentDay;
		this.downloadCSV(csv.join("\n"),  fileName+ ".csv"); 
	}
	
	CostDailyMain.prototype.lockRecords = function(){ 
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		
		if (rowIds.length==0){
			alert("There is no data today.");
		}else{
			var params = {};
			params.isLockDaily = "true";
			this.getProgressStatus(true, false, "Locking...");
			
			var self = this;
			var url = this.baseUrl + "lock_daily_cost_record";
	
			$.post(url, params, function(response) { 
				alert("Data in this screen has been reviewed and locked. Only PIC with Lock/Unlock access can unlock record.");
				setTimeout(function(){ self.doneAndReload(response)},1000);
			}, "xml");
		}
	}
	
	CostDailyMain.prototype.unlockRecords = function(){ 
		var params = {};
		params.isLockDaily = "false";
		this.getProgressStatus(true, false, "UnLocking...");
		
		var self = this;
		var url = this.baseUrl + "lock_daily_cost_record";

		$.post(url, params, function(response) {
			// self.doneAndReload(response);
			setTimeout(function(){ self.doneAndReload(response)},1000);
		}, "xml");
		
	}
	
	CostDailyMain.prototype.doneAndReload = function(response) {
		this.node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	CostDailyMain.prototype.deleteRecords = function(){ 
		var rows = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		this.deletedRecords = [];
		if (rows.length==0) alert("Please select 1 item to delete.");
		else{
			for(var i=0; i<rows.length; i++){
				var rec = rows[i];
				var recData = this.afeGrid.jqGrid('getLocalRow', rows[i]);
				recData.action = CostDailyMain.DEL;
				this.afeGrid.jqGrid('setRowData', rows[i], recData, "DeleteRecordBackground");
				this.deletedRecords.push(rec);   
			}
			this.currentAction = "delete";
			this.show(this.rootBox);
			this.groupCat.setAttribute("disabled", true);
			$(this.btn_lockRecord).addClass("hide"); 
			this.autoResizeGrid();
		}
	}
	
	CostDailyMain.prototype.deleteRecord = function(){
		var rows = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		for(var i=rows.length-1; i>=0; i--){
			console.log("selected Id: ", rows[i]);
			var rec = this.afeGrid.jqGrid('getLocalRow', rows[i]);
			rec.deleted = true;
			this.deletedRecords.push(rec);
			this.afeGrid.jqGrid('delRowData', rows[i]);
		}
		//at this point, grid's data array contains record which are not deleted only
		console.log("grid data provider: ", this.afeGrid.jqGrid('getGridParam', 'data')); 
	}
	
	window.CostDailyMain = CostDailyMain;
	
	
})(window);