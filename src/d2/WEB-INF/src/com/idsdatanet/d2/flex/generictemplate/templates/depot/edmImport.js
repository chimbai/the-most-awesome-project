(function(window) {
	EdmDailyImportNodeListener.ACTION_IMPORT = "import";
	EdmDailyImportNodeListener.ACTION_CANCEL_IMPORT = "cancel_import";
	EdmDailyImportNodeListener.ACTION_GET_JOB_STATUS = "get_job_status";
	
	EdmDailyImportNodeListener.DAY_IMPORT_STRING = "Importing day";
	EdmDailyImportNodeListener.DAY_IMPORT_CANCEL_STRING = "Job cancelled.";
	EdmDailyImportNodeListener.LOADING_STRING = "Loading...";
	EdmDailyImportNodeListener.PLEASE_WAIT_STRING = "Please Wait";
	EdmDailyImportNodeListener.STARTING_IMPORTING_STRING = "Starting Import...";
	EdmDailyImportNodeListener.SYSTEM_MESSAGE_STRING = "System Message";
	
	EdmDailyImportNodeListener.JOB_ERROR_OCCURED = -2;
	EdmDailyImportNodeListener.JOB_FAILED_TO_START = -1;
	EdmDailyImportNodeListener.JOB_ENDED = 0;
	EdmDailyImportNodeListener.JOB_STARTED = 1;
	EdmDailyImportNodeListener.JOB_IN_PROGRESS = 2;
	EdmDailyImportNodeListener.JOB_CANCELLING = 3;
	
	EdmDailyImportNodeListener.DAY_RESULT_FAIL = -1;
	EdmDailyImportNodeListener.DAY_RESULT_NOT_STARTED = 0;
	EdmDailyImportNodeListener.DAY_RESULT_SUCCESS = 1;
	EdmDailyImportNodeListener.DAY_RESULT_USER_CANCELLED = 2;
	EdmDailyImportNodeListener.DAY_RESULT_FAIL_STRING = "Failed";
	EdmDailyImportNodeListener.DAY_RESULT_NOT_STARTED_STRING = "Not Started";
	EdmDailyImportNodeListener.DAY_RESULT_SUCCESS_STRING = "Completed";
	EdmDailyImportNodeListener.DAY_RESULT_USER_CANCELLED_STRING = "Incomplete";
	
	EdmDailyImportNodeListener.DAY_RESULT_HEADER_NAME = "Day";
	EdmDailyImportNodeListener.DAY_RESULT_HEADER_VALUE = "Status";
	
	EdmDailyImportNodeListener.INTERVAL = 1000;
	
	D3.inherits(EdmDailyImportNodeListener, D3.EmptyNodeListener);
	
	function EdmDailyImportNodeListener() {
		this.commandBeanProxy = null;
		this.alert(); // initialise alert box
		this.loadingPopupInit();
	}
	
	EdmDailyImportNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
        if(id == EdmDailyImportNodeListener.ACTION_IMPORT) {
            var importBtn = document.createElement("button");
            importBtn.className = "rootButtons";
            importBtn.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Import")));
            importBtn.onclick = this.onclickImport.bindAsEventListener(this);
            return importBtn;
        }
        return null;
	}
	
	EdmDailyImportNodeListener.prototype.onclickImport = function() {
		var selectedNodes = this.commandBeanProxy.rootNode.collectSelectedChildNodes();
		var dailyNodes = selectedNodes.EdmDailyImport; 
		var dayDateList = {};
		if(dailyNodes.length > 0) {
			var inData = {};
			D3.forEach(dailyNodes, function(node) {
				dayDateList[node.fieldData.dailyUid.value] = node.fieldData.dayDate.value;
			});
			inData.dayDates = dayDateList;
			
			var params = {};
			params._invokeCustomFilter = EdmDailyImportNodeListener.ACTION_IMPORT;
			params.data = JSON.stringify(inData);
			this.postToServer(EdmDailyImportNodeListener.ACTION_IMPORT, params, this.handleImportStartResponse, this);
		} else {
			this.alert(EdmDailyImportNodeListener.SYSTEM_MESSAGE_STRING, "Please select at least one day to import.", false, "Close");
		}
	}
	
	EdmDailyImportNodeListener.prototype.postToServer = function(action, params, callback, context) {
		if(action != EdmDailyImportNodeListener.ACTION_GET_JOB_STATUS && action != EdmDailyImportNodeListener.ACTION_CANCEL_IMPORT) {
			this.loadStarted();
		}
		this.commandBeanProxy.customInvoke(params, callback, context, "json");
	};
	
	EdmDailyImportNodeListener.prototype.handleImportStartResponse = function(response) {
		if(response.success) {
			this.prepareImport();
			this.importDialog.dialog("open");
			this.loadCompleted();
		} else {
			this.handleErrorResponse(response);
		}
	};
	
	EdmDailyImportNodeListener.prototype.prepareImport = function() {
		var dialogDiv = "<div id='importDialog'>" +
			"<div class='progressLabel'><p class='progressLabelText'></p></div>" +
			"<div id='progressBar'></div>" +
			"<div><button id='showResultsButton'>Results</button></div>" +
		"</div>";
		var dialog = $(dialogDiv);
		
		var self = this;
		this.progressTimer;
		this.currentImportingDay = "";
		this.progressBar = dialog.find("#progressBar");
		this.progressLabel = $(dialog.find(".progressLabelText").get(0));
		this.resultsButton = dialog.find("#showResultsButton");
		this.resultsButton.on("click",function(){self.popupWellResults();});
		this.resultsButton.hide();
		this.dialogButtons = [{
			id: "cancelImportBtn",
			text: "Cancel Import",
			disabled: true,
			closeOnEscape: false,
			click: function() {
				self.postCancelImport();
			}
		}];
		
		this.importDialog = dialog.dialog({
			title: "Import Progress",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			buttons: self.dialogButtons,
			modal:true,
			open: function() {
				self.progressTimer = setTimeout(function(){
					self.getImportProgress();
				}, EdmDailyImportNodeListener.INTERVAL);
			}
		});

		this.progressBar.progressbar({
			value: false,
			change: function() {
				self.updateImportText();
			}
		});
		this.progressLabel.text("Retrieving Import Status...");
	};
	
	EdmDailyImportNodeListener.prototype.updateImportText = function() {
		var text = "Importing Day: " + this.currentImportingDay + "\n";
		text += "Data Object: " + ((this.currentImportingDataObject.length > 0) ? this.currentImportingDataObject : "...") + "\n";
		text += "Progress: " + this.progressBar.progressbar("value") + "%";
		this.progressLabel.html(text);
		this.progressLabel.html(this.progressLabel.html().replace(/\n/g,'<br/>'));
	}
	
	EdmDailyImportNodeListener.prototype.closeImport = function() {
		clearTimeout(this.progressTimer);
		if(this.indexDialog && this.indexDialog.hasClass("ui-dialog-content") && this.indexDialog.dialog("isOpen") === true) {
			this.importDialog.dialog("option", "buttons", this.dialogButtons);
			this.importDialog.dialog("close");
			this.progressBar.progressbar("value", false);
			this.progressLabel.text(EdmDailyImportNodeListener.STARTING_IMPORTING_STRING);
		}
		this.closeDialogBoxes();
		this.refreshScreen();
	};
	
	EdmDailyImportNodeListener.prototype.alert = function(title, message, error, buttonLabel) {
		if(title == null) { title = EdmDailyImportNodeListener.SYSTEM_MESSAGE_STRING; }
		if(message == null) { message = ""; }
		if(error == null) { error = false; }
		if(buttonLabel == null) { buttonLabel = "OK" }
		if(!this.alertMessage) {
			var setting = "<div><p class='alertMessage'><img style='float:left;' src='images/warning.png'/>" + message + "</p></div>";
			this.alertMessage = $(setting);
			this.alertMessage.dialog({
				autoOpen: false,
				width: "auto",
				height: "auto",
				modal : true,
				resizable: false,
				title : title,
				buttons : [{
				   	text: buttonLabel,
				   	id: "alertLabel",
				   	click: function() {
				   		$(this).dialog("close");
				    }
				}]
			});
		} else if(title && message){
			var msgText = "";
			if(error) {
				msgText += "<img style='float:left;' src='images/warning.png'/>";
			}
			msgText += message;
			
			var msg = this.alertMessage.find(".alertMessage").get(0);
			$(msg).html(msgText);
			$(msg).html($(msg).html().replace(/\n/g,'<br/>'));
			
			this.alertMessage.dialog("option", "title", title);
			$("#alertLabel").button("option", "label", buttonLabel);
			this.alertMessage.dialog("open");
		}
	};
	
	EdmDailyImportNodeListener.prototype.loadingPopupInit = function() {
		var title = EdmDailyImportNodeListener.PLEASE_WAIT_STRING;
		var message = EdmDailyImportNodeListener.LOADING_STRING;
		if(!this._loadingPopup) {
			var setting = "<div><p class='alertMessage'>" + message + "</p></div>";
			this._loadingPopup = $(setting);
			this._loadingPopup.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 300,
				minHeight: "auto",
				modal : true,
				resizable: false,
				title : title
			});
		}
	};
	
	// Cancel Import
	EdmDailyImportNodeListener.prototype.postCancelImport = function() {
		var params = {};
		params._invokeCustomFilter = EdmDailyImportNodeListener.ACTION_CANCEL_IMPORT;
		this.postToServer(EdmDailyImportNodeListener.ACTION_CANCEL_IMPORT, params, this.handleCancelImportResponse, this);
	};
	
	EdmDailyImportNodeListener.prototype.handleCancelImportResponse = function() {
		var self = this;
		this.importDialog.dialog("option", "buttons", [{
			id: "cancelImportCloseBtn",
			text: "Close",
			disabled: true,
			click: function() {
				self.closeImport();
			}
		}]);
		var msg = "Cancelling Import...\nPlease wait momentarily";
		this.progressLabel.text(msg);
		this.progressLabel.html(self.progressLabel.html().replace(/\n/g,'<br/>'));
		this.progressBar.hide();
	};
	
	EdmDailyImportNodeListener.prototype.popupDayResults = function () {
		this.closeDialogBoxes();
		var table = $("<table class='dayResultsPopUpTable'></table>");
		if(this._currentDayResults && this._currentDayResults.length > 0) {
			var headerTable = $("<table class='dayResultsPopUpHeaderTable dayResultsPopUpTable'></table>");
			var tableHeader = $("<tr class='dayResultsPopUpTableHeaderRow'></tr>");
			var tableNameHeader = $("<td class='dayResultsPopUpTableColumn'>" + EdmDailyImportNodeListener.DAY_RESULT_HEADER_NAME + "</td>");
			tableHeader.append(tableNameHeader);
			var tableStatusHeader = $("<td class='dayResultsPopUpTableColumn'>" + EdmDailyImportNodeListener.DAY_RESULT_HEADER_VALUE + "</td>");
			tableHeader.append(tableStatusHeader);
			headerTable.append(tableHeader);
			
			for(var i=0; i < this._currentDayResults.length; i++) {
				var obj = this._currentDayResults[i];
				var name = obj.name;
				var status = obj.status;
				var statusString;
				if(status == EdmDailyImportNodeListener.DAY_RESULT_FAIL) {
					statusString = EdmDailyImportNodeListener.DAY_RESULT_FAIL_STRING 
				} else if(status == EdmDailyImportNodeListener.DAY_RESULT_NOT_STARTED) {
					statusString = EdmDailyImportNodeListener.DAY_RESULT_NOT_STARTED_STRING
				} else if(status == EdmDailyImportNodeListener.DAY_RESULT_SUCCESS) {
					statusString = EdmDailyImportNodeListener.DAY_RESULT_SUCCESS_STRING
				} else if(status == EdmDailyImportNodeListener.DAY_RESULT_USER_CANCELLED) {
					statusString = EdmDailyImportNodeListener.DAY_RESULT_USER_CANCELLED_STRING
				} else {
					statusString = "UNDEFINED";
				}
				
				var tableRow = $("<tr class='dayResultsPopUpTableRow'></tr>");
				
				var tableNameColumn = $("<td class='dayResultsPopUpTableColumn'>" + name + "</td>");
				tableRow.append(tableNameColumn);
				
				var tableStatusColumn = $("<td class='dayResultsPopUpTableColumn'>" + statusString + "</td>");
				tableRow.append(tableStatusColumn);
				
				table.append(tableRow);
			}
		}
		
		this._wellResultsPopup = $("<div></div>");
		var headerDiv = $("<div class='wellResultsPopUpHeaderDiv'></div>");
		var contentDiv = $("<div class='wellResultsPopUpContentDiv'></div>");
		headerDiv.append(headerTable);
		contentDiv.append(table);
		this._wellResultsPopup.append(headerDiv);
		this._wellResultsPopup.append(contentDiv);
		
		var self = this;
		this._wellResultsPopup.dialog({
			title: "Import Results",
			closeOnEscape: false,
			resizable: false,
			width: "500",
			height: "auto",
			maxHeight: "500",
			buttons: [{
				text: "Close",
				click: function() {
					self.closeImport();
				}
			}],
			modal:true
		});
	}
	
	EdmDailyImportNodeListener.prototype.getImportProgress = function() {
		var val = this.progressBar.progressbar("value") || 0;
		var currentVal = this.postGetImportStatus(val);
	};
	
	// Get Import Status (During Import Process)
	EdmDailyImportNodeListener.prototype.postGetImportStatus = function(val) {
		var inData = {};
		inData.percent = val;
		var params = {};
		params._invokeCustomFilter = EdmDailyImportNodeListener.ACTION_GET_JOB_STATUS;
		params.data = JSON.stringify(inData);
		this.postToServer(EdmDailyImportNodeListener.ACTION_GET_JOB_STATUS, params, this.handleGetImportStatusResponse, this);
	};
	
	EdmDailyImportNodeListener.prototype.handleGetImportStatusResponse = function(response) {
		var self = this;
		var progress = response.data.response.status.progress;
		progress = Math.round(progress);
		var code = response.data.response.status.code;
		var type = response.data.response.status.type;
		this.currentImportingDay = this.parseDayFromImportString(response.data.response.status.message);
		this.currentImportingDataObject = response.data.response.status.dataObjectId;
		this.updateCurrentDayResults(response.data.response.status.dayResults);
		this.progressBar.progressbar("value", progress);
		$("#cancelImportBtn").removeAttr('disabled').removeClass('ui-state-disabled');
		if(EdmDailyImportNodeListener.ACTION_IMPORT == type && (code == EdmDailyImportNodeListener.JOB_IN_PROGRESS || code == EdmDailyImportNodeListener.JOB_CANCELLING) && progress < 100 && progress >= 0) {
			if(code == EdmDailyImportNodeListener.JOB_IN_PROGRESS) {
				this.updateImportText();
			}
			this.progressTimer = setTimeout(function() {
				self.getImportProgress();
			}, EdmDailyImportNodeListener.INTERVAL);
		} else if(EdmDailyImportNodeListener.ACTION_IMPORT == type && code == EdmDailyImportNodeListener.JOB_ENDED) {
			this.popupDayResults();
		} else {
			clearTimeout(this.progressTimer);
			this.closeImport();
		}
	};
	
	EdmDailyImportNodeListener.prototype.handleErrorResponse = function(response) {
		if(response.error)
			console.log(response.error);
		this.closeDialogBoxes();
		this.alert("System Error", "Server responded with failure.", true);
	};
	
	EdmDailyImportNodeListener.prototype.closeDialogBoxes = function() {
		if (this.importDialog && this.importDialog.hasClass("ui-dialog-content") && this.importDialog.dialog("isOpen") === true) {
			this.importDialog.dialog("close");
			this.importDialog.dialog("destroy");
		} else if (this._dayResultsPopup && this._dayResultsPopup.hasClass("ui-dialog-content") && this._dayResultsPopup.dialog("isOpen") === true) {
			this._dayResultsPopup.dialog("close");
			this._dayResultsPopup.dialog("destroy");
		} else if (this._loadingPopup && this._loadingPopup.hasClass("ui-dialog-content") && this._loadingPopup.dialog("isOpen") === true) {
			this._loadingPopup.dialog("close");
			this._loadingPopup.dialog("destroy");
		}
	};
	
	EdmDailyImportNodeListener.prototype.loadStarted = function() {
		this._loadingPopup.dialog("open");
	}
	
	EdmDailyImportNodeListener.prototype.loadCompleted = function() {
		this._loadingPopup.dialog("close");
	}
	
	EdmDailyImportNodeListener.prototype.updateCurrentDayResults = function (dayResults) {
		this._currentDayResults = [];
		for(var day in dayResults) {
			if(dayResults.hasOwnProperty(day)) {
				obj = {};
				obj.name = day;
				obj.status = dayResults[day];
				this._currentDayResults.push(obj);
			}
		}
	}
	
	EdmDailyImportNodeListener.prototype.parseDayFromImportString = function(dayString) {
		var str = "";
		var strArr = dayString.split(EdmDailyImportNodeListener.DAY_IMPORT_STRING);
		if(strArr[1]) {
			str = strArr[1].replace(/^\s+|\s+$/gm,''); // trim white space
		}
		return str;
	};
	
	EdmDailyImportNodeListener.prototype.refreshScreen = function () {
		var url = window.location.href;
		window.location.href = url;
	}
	
	D3.CommandBeanProxy.nodeListener = EdmDailyImportNodeListener;
})(window);