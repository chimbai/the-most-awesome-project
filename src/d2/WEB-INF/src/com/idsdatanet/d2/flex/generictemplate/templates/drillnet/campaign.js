(function(window){
	
	D3.inherits(CampaignOperationList,D3.AbstractFieldComponent);
	function CampaignOperationList(){
		D3.AbstractFieldComponent.call(this);
		this._operationFullList = false;
		this._operationContainer = null;
		this._operationLinks = [];
	}
	
	CampaignOperationList.prototype.getFieldRenderer = function(){
		if (this._renderer == null){
			this._renderer = this.createRenderer();
		}
		return this._renderer;
	}
	
	CampaignOperationList.prototype.createDocumentList = function(fieldName, headerText,links,linkState,linkCreationFunction){
		var conConfig = {
				tag:"div",
		};
		var container = D3.UI.createElement(conConfig);
		
		var headerConfig = {
			tag:"div",
		};
		var header = D3.UI.createElement(headerConfig);
		container.appendChild(header);
		var strDocuments = this._node.getFieldValue(fieldName);
		
		var documents = null;
		if (strDocuments){
			documents = strDocuments.split(",");
		}	
		
		var contConfig = {
				tag:"div",
		}
		var content = D3.UI.createElement(contConfig);
		container.appendChild(content);
		if (documents && documents.length>0){
			for (var i=0;i<documents.length;i++){
				var strParams = documents[i];
				if (!strParams[0] != "&" && !strParams[0] != "?")
					strParams = "&"+strParams;
				strParams = decodeURIComponent(strParams.replace(/\+/g,  " "));
				var params = {};
			    var parts = strParams.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			    	params[key] = value;
			    });

				var documentLinkConfig = {
						tag:"div",
						css:"campaign-operationLink",
						attributes:{},
						callbacks:{},
						context:this
				
				};
				linkCreationFunction.call(this,documentLinkConfig,params);
				var documentLinkConfig = D3.UI.createElement(documentLinkConfig);
				content.appendChild(documentLinkConfig);
				links.push(documentLinkConfig);
			}
		}
		
		return container;
	}
	
	CampaignOperationList.prototype.campaignOperationItemCreation = function(documentLinkConfig,params){
		var operationName = params.operationname;
		
		documentLinkConfig.text = operationName;
		documentLinkConfig.attributes.title = operationName;
		documentLinkConfig.data = params;
		documentLinkConfig.callbacks.onclick = function(event){
			var link = event.target;
			var data = $(link).data();
			var dailyUid = data.dailyuid;
			
			var redirect_url = "reportdaily.html?gotoday=" + escape(dailyUid) ;
			this._node.commandBeanProxy.gotoUrl(redirect_url,D3.LanguageManager.getLabel("label.operationLink.redirect"));
		}
	}
	
	
	CampaignOperationList.prototype.createRenderer = function(){
		var conConfig = {
				tag:"div",
		};
		var container = D3.UI.createElement(conConfig);
		
		var lessonTickets = this._node.getFieldValue("@operationList");
		if (lessonTickets != null){
			this._operationContainer = this.createDocumentList("@operationList","" ,this._operationLinks,this._operationFullList, this.campaignOperationItemCreation);
			container.appendChild(this._operationContainer);
		}
		
		return container;
	}
	
	
	D3.inherits(CampaignListener,D3.EmptyNodeListener);
	
	function CampaignListener()
	{

	}
	
	CampaignListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "operationList"){
			return new CampaignOperationList();
		}
	}


	D3.CommandBeanProxy.nodeListener = CampaignListener;

})(window);