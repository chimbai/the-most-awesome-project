(function(window){
	
	D3.inherits(TimerCustomField, D3.AbstractFieldComponent);
	
	function TimerCustomField() {
		D3.AbstractFieldComponent.call(this);
		this._triggerTypeCollection = ["every", "daily at"];
		this._hourCollection = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" ];
		this._minuteCollection = ["0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"];
		this._secondCollection = ["0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"];
	}
	
	TimerCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			var divConfig = "<div id='triggerTimerContainer' class='triggerTimerContainer'>" +
					"<label>Trigger</label>" +
					"<div id='triggerTimerType'/>" +
					"<div id='triggerTimerHour'/>" +
					"<label>h</label>" +
					"<div id='triggerTimerMinute'/>" +
					"<label>m</label>" +
					"<div id='triggerTimerSecond'/>" +
					"<label>s</label>" +
				"</div>";
			this._triggerTimerContainer = $(divConfig);
			this._triggerTimerType = this._triggerTimerContainer.find("#triggerTimerType");
			this._triggerTimerHour = this._triggerTimerContainer.find("#triggerTimerHour");
			this._triggerTimerMinute = this._triggerTimerContainer.find("#triggerTimerMinute");
			this._triggerTimerSecond = this._triggerTimerContainer.find("#triggerTimerSecond");
			this._init();
			
			this._fieldEditor = this._triggerTimerContainer.get(0);
		}
		return this._fieldEditor;
	}
	
	TimerCustomField.prototype._init = function () {
		var currentTriggerDetails = this._getTriggerDetails(this.getFieldValue());
		this._createDropdown(this._triggerTimerType, this._triggerTypeCollection, this._triggerTypeChangeListener, currentTriggerDetails.triggerType);
		this._createDropdown(this._triggerTimerHour, this._hourCollection, this._onchangeTimer, currentTriggerDetails.hour);
		this._createDropdown(this._triggerTimerMinute, this._minuteCollection, this._onchangeTimer, currentTriggerDetails.minute);
		this._createDropdown(this._triggerTimerSecond, this._secondCollection, this._onchangeTimer, currentTriggerDetails.second);
	}
	
	TimerCustomField.prototype._createDropdown = function(container, list, listener, currentValue) {
		var select = document.createElement("select");
		if(listener != null)
			$(select).on("change", {context: this}, listener);
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var opt = document.createElement("option");
			opt.value = item;
			opt.textContent = item;
			if ((item == currentValue) && item != "")
				opt.selected = "selected";
			select.appendChild(opt);
		}
		container.append(select);
	}
	
	TimerCustomField.prototype._triggerTypeChangeListener = function(e) {
		if (e!= null)
			context = e.data.context;
		else
			context = this;
		context._node.setFieldValue("triggerType", this.value);
	}
	
	TimerCustomField.prototype._onchangeTimer = function(e) {
		var context = null;
		if (e!= null)
			context = e.data.context;
		else
			context = this;
		var value = "01 Jan 1970 " + context._getEditorValue() + " GMT+0";
		value = Date.parse(value);
		var fieldValue = context.getFieldValue();
		context.setFieldValue(value);
	}
	
	TimerCustomField.prototype._getEditorValue = function() {
		var hour = this._getTimeString(this._triggerTimerHour);
		var minute = this._getTimeString(this._triggerTimerMinute);
		var second = this._getTimeString(this._triggerTimerSecond);
		return hour + ":" + minute + ":" + second;
	}
	
	TimerCustomField.prototype._getTimeString = function(container) {
		var number = container.find('select').val();
		if (parseInt(number) < 10)
			number = "0" + number;
		return number;
	}

	TimerCustomField.prototype.refreshFieldRenderer = function () {
		var fieldValue = this.getFieldValue();
		
		if(fieldValue != null && fieldValue != "") {
			var triggerDetails = this._getTriggerDetails(fieldValue);
			if (triggerDetails.triggerType == null || triggerDetails.triggerType == "") {
				triggerDetails.triggerType = "every";
				this._node.setFieldValue("triggerType", triggerDetails.triggerType);
			}
			var triggerString = D3.LanguageManager.getLabel("string.schedulerTemplateTrigger");
			fieldValue = triggerString + " " + triggerDetails.triggerType + " " + triggerDetails.hour + "h " + triggerDetails.minute + "m " + triggerDetails.second + "s";
		}
		this.setFieldRendererValue(fieldValue);
	}
	
	TimerCustomField.prototype._getTriggerDetails = function(fieldValue) {
		var triggerDetails = {};
		if (fieldValue != null && fieldValue != "") {
			triggerDetails.triggerType = this._node.getFieldValue("triggerType");
			var date = new Date(parseInt(fieldValue));
			triggerDetails.hour = date.getUTCHours().toString();
			triggerDetails.minute = date.getUTCMinutes().toString();
			triggerDetails.second = date.getUTCSeconds().toString();
		}
		return triggerDetails;
	}
	
	D3.inherits(DepotSchedulerTemplateDataNodeListener,D3.EmptyNodeListener);
	
	function DepotSchedulerTemplateDataNodeListener() { }
	
	
	DepotSchedulerTemplateDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "schedulerTimer"){
			return new TimerCustomField();			
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = DepotSchedulerTemplateDataNodeListener;
	
})(window);