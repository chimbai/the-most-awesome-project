(function(window){
	
	D3.inherits(AdvancedLessonSearchFilter,D3.AbstractFieldComponent);
	function AdvancedLessonSearchFilter() {
	}
	AdvancedLessonSearchFilter.prototype.dispose = function() {
		AdvancedLessonSearchFilter.uber.dispose.call(this);
		
		this._textInput = null;
		this._button = null;
		this._fieldRenderer = null;
		this._redirectUrl = null;
	}
	
	AdvancedLessonSearchFilter.prototype.refreshFieldRenderer = function (){
		$(this._textInput).val(this.getFieldValue());
	}

	AdvancedLessonSearchFilter.prototype.getFieldRenderer = function() {
		
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({tag:"span",css:"fieldRenderer"});
			if (this._textInput == null) {
				var textConfig = {
						tag:"input",
						css:"fieldEditor textInputField",
						attributes:{},
						callbacks:{
							onkeydown:this.txtInput_onKeyDown,
							onblur:this.dataChangeListener
						},
						context:this
				}
				
				if (this._config) {
					var toolTip = this._config.toolTip;
					if (toolTip) {
						textConfig.attributes.title = toolTip;
					}
				}
				this._textInput = D3.UI.createElement(textConfig);
				
				this._fieldRenderer.appendChild(this._textInput);
				
			}
			
			if (this._button == null) {
				var label = D3.LanguageManager.getLabel("button.search");
				
				var buttonConfig = {
						tag:"button",
						text:label,
						css:"DefaultButton",
						attributes:{
							type:"button",
							title:label
						},
						callbacks:{
							onclick:this.searchButtonClicked
						},
						context:this
				}
				this._button  = D3.UI.createElement(buttonConfig);
				this._fieldRenderer.appendChild(this._button);
			}
		}
		
		return this._fieldRenderer;
	}
	AdvancedLessonSearchFilter.prototype.txtInput_onKeyDown = function(event) {
		if (event.currentTarget == this._textInput) {
			if (event.keyCode == 13) {
				//ENTER key
				this.setFieldValueNow();
				this.searchButtonClicked(event);
			}
		}
	}
	AdvancedLessonSearchFilter.prototype.data = function(value) {
		AdvancedLessonSearchFilter.uber.data.call(this, value);
		this._node.addEventListener(D3.CommandBeanProxyEvent.OPERATION_TYPE_SUBMIT, this.pageRedirect, this);
	}
	
	AdvancedLessonSearchFilter.prototype.pageRedirect = function() {
		var msg = D3.LanguageManager.getLabel("label.loading"); 
		this._node.commandBeanProxy.gotoUrl(this._redirectUrl,msg);
	}
	
	AdvancedLessonSearchFilter.prototype.dataChangeListener = function(event) {
		if (event.target === this._textInput) {
			this.setFieldValueNow();
		}
	}
	AdvancedLessonSearchFilter.prototype.setFieldValueNow = function() {
		this.setFieldValue($(this._textInput).val());
	}
	
	AdvancedLessonSearchFilter.prototype.searchButtonClicked = function(event) {
		if (event.target == this._button || event.target == this._textInput){
			var relate = this._config.relatedFields;
			var advancedLessonFilter = this.setValue("@advancedLessonFilter");
			var onOffShore = this.setValue("@onOffShore");
			var status = this.setValue("@status");
			var rigType = this.setValue("@rigType");
			var wellType = this.setValue("@wellType");
			var country = this.setValue("@country");
			var lessonCategory = this.setValue("@lessonCategory");
			var phaseCode = this.setValue("@phaseCode");	
			var jobtypeCode = this.setValue("@jobtypeCode");
			var taskCode = this.setValue("@taskCode");
			var rootcauseCode = this.setValue("@rootcauseCode");
			var holeSize = this.setValue("@holeSize");
			var companies = this.setValue("@companies");
			var wellborePurpose = this.setValue("@wellborePurpose");
			var wellboreType = this.setValue("@wellboreType");
			var queryParam = "";
			if (advancedLessonFilter!=null && advancedLessonFilter!="")
				queryParam += advancedLessonFilter;
			if (onOffShore!=null && onOffShore!="")
				queryParam += onOffShore;	
			if (status!=null && status!="")
				queryParam += status;
			if (rigType!=null && rigType!="")
				queryParam += rigType;
			if (wellType!=null && wellType!="")
				queryParam += wellType;
			if (country!=null && country!="")
				queryParam += country;
			if (lessonCategory!=null && lessonCategory!="")
				queryParam += lessonCategory;
			if (phaseCode!=null && phaseCode!="")
				queryParam += phaseCode;
			if (jobtypeCode!=null && jobtypeCode!="")
				queryParam += jobtypeCode;
			if (taskCode!=null && taskCode!="")
				queryParam += taskCode;
			if (rootcauseCode!=null &&rootcauseCode!="")
				queryParam += rootcauseCode;
			if (holeSize!=null && holeSize!="")
				queryParam += holeSize;
			if (companies!=null && companies!="")
				queryParam += companies;
			if (wellborePurpose!=null && wellborePurpose!="")
				queryParam += wellborePurpose;
			if (wellboreType!=null && wellboreType!="")
				queryParam += wellboreType;
			//encrypt the queryParam
			var encodedText = btoa(queryParam);
			this._redirectUrl = null;
			
			if (encodedText==null || encodedText==""){
				this._redirectUrl = "advancedlessonsearch.html?tab=advancedlessonsearchresult";
			} else{
				this._redirectUrl = "advancedlessonsearch.html?tab=advancedlessonsearchresult&queryParam="+escape(encodedText);
			}

			//submit node data to server
			this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node, relate);
			this.pageRedirect();
		}
	}
	
	AdvancedLessonSearchFilter.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = null;
		if (data != null && data.length > 0) {
			for (var i=0; i < data.length; i++) {
				if (stringValue == null) {
					if (i == 0 && data[i] && data[i] != "null") {
						if(data.length==1){
							stringValue = fieldName.replace("@","")+",'"+data[i]+"';";
						}
						else{
							stringValue = fieldName.replace("@","")+",'"+data[i]+"'";
						}
					} else {
						stringValue = "";
					}
				} else {

					if(data[i] && data[i]!="null"){
						if(i==data.length-1){
							stringValue +=",'"+data[i]+"';";
						} else {
							stringValue +=",'"+data[i]+"'";
						}					
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue == "";
		}
		
		if (fieldName == "@advancedLessonFilter") {
			var str = this._node.getFieldValue(fieldName);
			if (str == null || str == "") {
				stringValue = "";
			} else {
				stringValue = "advancedLessonFilter,"+str+";";
			}
		}
		return stringValue;
	}
	
	D3.inherits(JobTypeCodeListCustomField,D3.MultiSelectField);
	function JobTypeCodeListCustomField() {
		D3.MultiSelectField.call(this);
	}
	
	JobTypeCodeListCustomField.prototype.dispose = function() {
		JobTypeCodeListCustomField.uber.dispose.call(this);
	}
	
	JobTypeCodeListCustomField.prototype.data = function(data) {
		JobTypeCodeListCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
		
	}
	
	JobTypeCodeListCustomField.prototype.fieldValueChange = function(event) {
		if (event.fieldName == "@wellType") {
			this.load();
		}
	}
	
	JobTypeCodeListCustomField.prototype.refreshFieldEditor = function() {
		JobTypeCodeListCustomField.uber.refreshFieldEditor.call(this);
	}

	JobTypeCodeListCustomField.prototype.getMultiSelectLookupReference = function() {
		var params = {};
		params._invokeCustomFilter = "jobTypeCodeList";
		var wellType = this.setValue("@wellType");
		var lookupRef = new D3.LookupReference();
		if (wellType) {
			params._wellType = wellType;
			var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
			
			var doc = $(response);
			
			doc.find("jobTypeCodeList").each(function(index, element){
				var newItem = new D3.LookupItem();
				var elm = $(this);
				newItem.data = elm.find("code").first().text();
				newItem.label = elm.find("label").first().text();
				lookupRef.addLookupItem(newItem);
			});
		}
		return lookupRef;
	}
	
	JobTypeCodeListCustomField.prototype.load = function() {
		D3.UI.removeAllChildNodes(this.fieldContainer.uiElement);
		this._fieldEditor = null;
		this._lookupRef = this.getMultiSelectLookupReference();
		
		this.fieldContainer.uiElement.appendChild(this.getFieldEditor());
	}
	
	JobTypeCodeListCustomField.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = "";
		if (data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (stringValue == "") {
					if(data[i] && data[i]!="null") {
						stringValue = ""+data[i]+"";
					} else {
						stringValue = "";
					}
				}
				else{
					if(data[i] && data[i]!="null") {
						stringValue +=",'"+data[i]+"";
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue = "";
		}
		return stringValue;
	}
	
	D3.inherits(rigTypeListCustomField,D3.MultiSelectField);
	function rigTypeListCustomField() {
		D3.MultiSelectField.call(this);
	}
	
	rigTypeListCustomField.prototype.dispose = function() {
		rigTypeListCustomField.uber.dispose.call(this);
	}
	
	rigTypeListCustomField.prototype.data = function(data) {
		rigTypeListCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
		
	}
	
	rigTypeListCustomField.prototype.fieldValueChange = function(event) {
		if (event.fieldName == "@onOffShore") {
			this.load();
		}
	}
	rigTypeListCustomField.prototype.load = function() {
		D3.UI.removeAllChildNodes(this.fieldContainer.uiElement);
		this._fieldEditor = null;
		this._lookupRef = this.getMultiSelectLookupReference();
		
		this.fieldContainer.uiElement.appendChild(this.getFieldEditor());
	}
	rigTypeListCustomField.prototype.getMultiSelectLookupReference = function() {
		var params = {};
		params._invokeCustomFilter = "rigTypeList";
		var onOffShore = this.setValue("@onOffShore");
		var lookupRef = new D3.LookupReference();
		if (onOffShore) {
			params._onOffShore = onOffShore;
			var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
			
			var doc = $(response);
			
			doc.find("rigTypeList").each(function(index, element){
				var newItem = new D3.LookupItem();
				var elm = $(this);
				newItem.data = elm.find("code").first().text();
				newItem.label = elm.find("label").first().text();
				lookupRef.addLookupItem(newItem);
			});
		}
		return lookupRef;
	}
	rigTypeListCustomField.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = "";
		if (data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (stringValue == "") {
					if(data[i] && data[i]!="null") {
						stringValue = "'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
				else{
					if(data[i] && data[i]!="null") {
						stringValue +=",'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue = "";
		}
		return stringValue;
	}
	rigTypeListCustomField.prototype.refreshFieldEditor = function() {
		rigTypeListCustomField.uber.refreshFieldEditor.call(this);
	}
	
	D3.inherits(phaseCodeListCustomField,D3.MultiSelectField);
	function phaseCodeListCustomField() {
		D3.MultiSelectField.call(this);
	}
	
	phaseCodeListCustomField.prototype.dispose = function() {
		phaseCodeListCustomField.uber.dispose.call(this);
	}
	
	phaseCodeListCustomField.prototype.data = function(data) {
		phaseCodeListCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
		
	}
	
	phaseCodeListCustomField.prototype.fieldValueChange = function(event) {
		if (event.fieldName == "@wellType") {
			this.load();
		}
	}
	phaseCodeListCustomField.prototype.load = function() {
		D3.UI.removeAllChildNodes(this.fieldContainer.uiElement);
		this._fieldEditor = null;
		this._lookupRef = this.getMultiSelectLookupReference();
		
		this.fieldContainer.uiElement.appendChild(this.getFieldEditor());
	}
	phaseCodeListCustomField.prototype.getMultiSelectLookupReference = function() {
		var params = {};
		params._invokeCustomFilter = "phaseCodeList";
		var wellType = this.setValue("@wellType");
		var lookupRef = new D3.LookupReference();
		if (wellType) {
			params._wellType = wellType;
			var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
			
			var doc = $(response);
			
			doc.find("phaseCodeList").each(function(index, element){
				var newItem = new D3.LookupItem();
				var elm = $(this);
				newItem.data = elm.find("code").first().text();
				newItem.label = elm.find("label").first().text();
				lookupRef.addLookupItem(newItem);
			});
		}
		return lookupRef;
	}
	phaseCodeListCustomField.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = "";
		if (data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (stringValue == "") {
					if(data[i] && data[i]!="null") {
						stringValue = "'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
				else{
					if(data[i] && data[i]!="null") {
						stringValue +=",'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue = "";
		}
		return stringValue;
	}
	phaseCodeListCustomField.prototype.refreshFieldEditor = function() {
		phaseCodeListCustomField.uber.refreshFieldEditor.call(this);
	}
	
	D3.inherits(taskCodeListFromPhaseCodeCustomField,D3.MultiSelectField);
	function taskCodeListFromPhaseCodeCustomField() {
		D3.MultiSelectField.call(this);
	}
	
	taskCodeListFromPhaseCodeCustomField.prototype.dispose = function() {
		phaseCodeListCustomField.uber.dispose.call(this);
	}
	
	taskCodeListFromPhaseCodeCustomField.prototype.data = function(data) {
		phaseCodeListCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
		
	}
	
	taskCodeListFromPhaseCodeCustomField.prototype.fieldValueChange = function(event) {
		if (event.fieldName == "@phaseCode") {
			this.load();
		}
	}
	taskCodeListFromPhaseCodeCustomField.prototype.load = function() {
		D3.UI.removeAllChildNodes(this.fieldContainer.uiElement);
		this._fieldEditor = null;
		this._lookupRef = this.getMultiSelectLookupReference();
		
		this.fieldContainer.uiElement.appendChild(this.getFieldEditor());
	}
	taskCodeListFromPhaseCodeCustomField.prototype.getMultiSelectLookupReference = function() {
		var params = {};
		params._invokeCustomFilter = "taskCodeListFromPhaseCode";
		var phaseCode = this.setValue("@phaseCode");
		var lookupRef = new D3.LookupReference();
		if (phaseCode) {
			params._phaseCode = phaseCode;
			var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
			
			var doc = $(response);
			
			doc.find("taskCodeListFromPhaseCode").each(function(index, element){
				var newItem = new D3.LookupItem();
				var elm = $(this);
				newItem.data = elm.find("code").first().text();
				newItem.label = elm.find("label").first().text();
				lookupRef.addLookupItem(newItem);
			});
		}
		return lookupRef;
	}
	taskCodeListFromPhaseCodeCustomField.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = "";
		if (data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (stringValue == "") {
					if(data[i] && data[i]!="null") {
						stringValue = "'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
				else{
					if(data[i] && data[i]!="null") {
						stringValue +=",'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue = "";
		}
		return stringValue;
	}
	taskCodeListFromPhaseCodeCustomField.prototype.refreshFieldEditor = function() {
		taskCodeListFromPhaseCodeCustomField.uber.refreshFieldEditor.call(this);
	}
	
	D3.inherits(TaskCodeListCustomField,D3.MultiSelectField);
	function TaskCodeListCustomField() {
		D3.MultiSelectField.call(this);
	}
	
	TaskCodeListCustomField.prototype.dispose = function() {
		TaskCodeListCustomField.uber.dispose.call(this);
	}
	
	TaskCodeListCustomField.prototype.data = function(data) {
		TaskCodeListCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
	}
	
	TaskCodeListCustomField.prototype.fieldValueChange = function(event) {
		if (event.fieldName == "@jobtypeCode" || event.fieldName == "@onOffShore" || event.fieldName == "@wellType") {
			this.load();
		}
	}
	
	TaskCodeListCustomField.prototype.refreshFieldEditor = function() {
		TaskCodeListCustomField.uber.refreshFieldEditor.call(this);
	}

	TaskCodeListCustomField.prototype.getMultiSelectLookupReference = function() {
		var params = {};
		params._invokeCustomFilter = "taskCodeList";
		var jobTypeCode = this.setValue("@jobtypeCode");
		if (jobTypeCode) {
			params._jobtypeCode = jobTypeCode;
		}
		var onOffShore = this.setValue("@onOffShore");
		if (onOffShore) {
			params._onOffShore = onOffShore;
		}
		
		var wellType = this.setValue("@wellType");
		if (wellType) {
			params._wellType = wellType;
		}
		
		var lookupRef = new D3.LookupReference();

		if (jobTypeCode || onOffShore || wellType) {
			var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
			
			var doc = $(response);
			
			doc.find("taskCodeList").each(function(index, element){
				var newItem = new D3.LookupItem();
				var elm = $(this);
				newItem.data = elm.find("code").first().text();
				newItem.label = elm.find("label").first().text();
				lookupRef.addLookupItem(newItem);
			});
		}
		return lookupRef;
	}
	
	TaskCodeListCustomField.prototype.load = function() {
		D3.UI.removeAllChildNodes(this.fieldContainer.uiElement);
		this._fieldEditor = null;
		this._lookupRef = this.getMultiSelectLookupReference();
		
		this.fieldContainer.uiElement.appendChild(this.getFieldEditor());
	}
	
	TaskCodeListCustomField.prototype.setValue = function(fieldName) {
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = null;
		if (data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (stringValue == null) {
					if(data[i] && data[i]!="null") {
						stringValue = "'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
				else{
					if(data[i] && data[i]!="null") {
						stringValue +=",'"+data[i]+"'";
					} else {
						stringValue = "";
					}
				}
			}
		} else {
			stringValue = "";
		}
		return stringValue;
	}
	
	D3.inherits(AdvancedLessonSearchListener,D3.EmptyNodeListener);
	function AdvancedLessonSearchListener()
	{

	}
	AdvancedLessonSearchListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "advancedLessonFilter"){
			return new AdvancedLessonSearchFilter();
		}else if(id == "taskCodeList"){
			return new TaskCodeListCustomField();
		}else if(id == "jobTypeCodeList"){
			return new JobTypeCodeListCustomField();
		}else if(id == "rigTypeList"){
			return new rigTypeListCustomField();	
		}else if(id == "phaseCodeList"){
			return new phaseCodeListCustomField();	
		}else if(id == "taskCodeListFromPhaseCode"){
			return new taskCodeListFromPhaseCodeCustomField();	
		}else{
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = AdvancedLessonSearchListener;
})(window);