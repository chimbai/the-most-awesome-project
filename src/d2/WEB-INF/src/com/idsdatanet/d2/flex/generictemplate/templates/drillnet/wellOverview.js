(function(window){
	D3.inherits(DailyLinkButton,D3.HyperLinkField);
	function DailyLinkButton(){
		D3.HyperLinkField.call(this);
	}
	
	DailyLinkButton.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		
		if (!this._fieldRenderer){
			var dayDate = this.getFieldValue(); 
			if (dayDate!=""){
				
				
			
			var d = new Date(parseInt(dayDate)); 
			var linkLabel = $.datepick.formatDate("dd M yyyy", d);
		//  var linkLabel = this._node.getFieldValue("daydate");
		  fieldRenderer = DailyLinkButton.uber.getFieldRenderer.call(this);
		  fieldRenderer.appendChild(document.createTextNode(linkLabel));
		  fieldRenderer.removeAttribute("target");
		  fieldRenderer.removeAttribute("href");
		  $(fieldRenderer).addClass("whitespace");
		  fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
			}
		}else{
	      fieldRenderer = DailyLinkButton.uber.getFieldRenderer.call(this);
		}
		return fieldRenderer; 
	}
	
	DailyLinkButton.prototype.onclick = function(){
		var url = "reportdaily.html?gotoday=" + this._node.getFieldValue("dailyUid");
		this._node.commandBeanProxy.gotoUrl(url,"Loading " + this.getFieldValue());
	}
	
	DailyLinkButton.prototype.refreshFieldRenderer = function (){}
	
	
	D3.inherits(WellOverviewListener,D3.EmptyNodeListener);
	
	function WellOverviewListener()
	{

	}
	WellOverviewListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "dailyLink") {
			return new DailyLinkButton();
		}else{
			return null;
		}
	}

	D3.CommandBeanProxy.nodeListener = WellOverviewListener;
	
	
	
})(window);