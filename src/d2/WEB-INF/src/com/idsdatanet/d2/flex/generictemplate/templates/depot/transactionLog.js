(function(window){

	D3.inherits(ShowLogField,D3.TextInputField);
	
	function ShowLogField(){
		D3.TextInputField.call(this);
	}

	ShowLogField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			this._fieldEditor = ShowLogField.uber.getFieldEditor.call(this);
			this._fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			this._fieldEditor = ShowLogField.uber.getFieldEditor.call(this);
		}			
		return this._fieldEditor;
	}
	
	ShowLogField.prototype.onKeyUp = function(event){
		if (event.keyCode === 13) {
			alert(this.getFieldEditorValue());
			navigator.clipboard.writeText(this.getFieldEditorValue());
		}
	}
	
	D3.inherits(TransactionLogListener,D3.EmptyNodeListener);
	
	function TransactionLogListener() { }
	
	TransactionLogListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "showLog"){
			return new ShowLogField();
		}
	}

	D3.CommandBeanProxy.nodeListener = TransactionLogListener;
	
})(window);