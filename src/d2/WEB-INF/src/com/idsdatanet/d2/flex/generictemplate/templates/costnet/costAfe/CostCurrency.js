(function(window){
	
	function CostCurrency(){
		this._pendingProcess = [];
		this.hasData = false;
		this._columns = [
		     {
		    	 type: "recordAction",
		    	 width:"10px"
		     },
		     {
		    	 type:"field",
		    	 field:"currencyName",
		    	 width:"80px",
		    	 align:"left",
		    	 title:"Currency"
		     },   
		     {
		    	 type:"field",
		    	 field:"currencySymbol",
		    	 width:"80px",
		    	 align:"left",
		    	 title:"Symbol"
		     },    
		     {
		    	 type:"field",
		    	 field:"conversionRate",
		    	 width:"80px",
		    	 align:"left",
		         title:"Conversion Rate"
		     },
		     {
		    	 type:"field",
		    	 field:"isPrimaryCurrency",
		    	 width:"80px",
		    	 align:"left",
		         title:"Is Primary?"
		     }
		    ];
	}
	
	CostCurrency.prototype.init = function(option){
		this.url = option.baseUrl;
		this.masterAfeUid = option.masterAfeUid;
		this.needReload = false;
	}
	
	CostCurrency.prototype.showPopup = function(){
		if (this.masterAfeUid==""){
			alert("No Afe found");
			return;
		}
		if (!this._container){
			this.initiateContainer();
		}
		
		this.loadCurrencyData();
	//	this.hidePopup();
		$(this._container).dialog({title:"Currency",dialogClass: 'no-close' ,
			closeonEscape:false,modal:true, width:"1024", height:"500", resizable:false,closable:false});
	}
	
	CostCurrency.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th class='SimpleGridHeaderCell' style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}
	
	CostCurrency.prototype.initiateContainer = function (){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container' style='overflow-y: hidden;'>" +
			"<div class='StandardButtonContainer'>" +
			    "<button class='RootButton Addnew'>Add New</button>" +
				"<button class='RootButton DeleteSelected'>Delete Selected</button>" +
				"<button class='RootButton Close'>Close</button>" +
			"</div>" +
		 	"<div class='ConfirmCancelButtonContainer hide'>" +
		 		"<button class='RootButton RootButtonConfirm Confirm'>Confirm</button>" +
		 		"<button class='RootButton RootButtonCancel Cancel'>Cancel</button>" +
			"</div>" +
	//		"<div style='overflow-x: hidden;overflow-y: auto;height:280px'>" +
				"<table class='SimpleGridTable reportListing'><thead><tr>"+this.getColumnHeader()+"</tr></thead><tbody class='costItemsList-TBODY'></tbody></table>"+
	//		"</div>" +
			"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			
			this._standardButtonContainer = container.find(".StandardButtonContainer").get(0);
			
			this._confirmCancelButtonContainer = container.find(".ConfirmCancelButtonContainer").get(0);
			
			this._btnConfirm = container.find(".Confirm").get(0);
			this._btnConfirm.onclick = this.btnConfirm_onClick.bindAsEventListener(this);
			this._btnCancel = container.find(".Cancel").get(0);
			this._btnCancel.onclick = this.btnCancel_onClick.bindAsEventListener(this);
			
			this._btnAddnew = container.find(".Addnew").get(0);
			this._btnAddnew.onclick = this.btnAddnew_onClick.bindAsEventListener(this);
			this._btnDeleteSelected = container.find(".DeleteSelected").get(0);
			this._btnDeleteSelected.onclick = this.btnDeleteSelected_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
			
			this._tbody = container.find(".costItemsList-TBODY").get(0);
		}
	}
	
	CostCurrency.prototype.btnClose_onClick = function(){
		if (this.needReload) location.reload(); 
		$(this._container).dialog("close");
	}
	
	CostCurrency.prototype.btnDeleteSelected_onClick = function(){
		var selectedItems = [];
		for (var i=0;i<this._CostItems.length;i++){
			var item = this._CostItems[i];
			if (item.selected)
				selectedItems.push(item);
		}
		if (selectedItems.length>0){
			var deleteSelected = false;
			deleteSelected = confirm("Are you sure you want to delete\nthe selected record?");
			if(deleteSelected) {
				for (var key in selectedItems){
					var record = selectedItems[key].action = "delete";
				}
				this.processRecords();
			}
		}else{
			alert("You have not selected any record.\nPlease select at least one record to delete.");
		}
	}
	
	CostCurrency.prototype.btnAddnew_onClick = function(){
		if (!this._addNewContainer){
			var addNewContainerTemplate = "<div class='addNew-container'>" +
				"<table class='SimpleGridTable'>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>Currency Name</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input type='text' class='currencyName'/></span></td>" + 
				"</tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>Currency Symbol</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input type='text' class='currencySymbol'/>" + 
				"</span></td></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>Conversion Rate</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input type='text' class='conversionRate'/></td>" + 
				"</span></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>Is Primary Currency?</span></td>" +  
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<select class='fieldEditor isPrimaryCurrency'>" +
				"<option value='0'>No</option>" +
				"<option value='1'>Yes</option>" +
				"</select>" +
			
				"</span></td></tr>" +
				"<tr class='SimpleGridDataRow'><td colspan=2 align='center'>" +
				"<span class='fieldRenderer'>" +
				"<div class='ConfirmButtonContainer'>" +
				"<button class='PopupConfirmButton popConfirm'>Confirm</button>" +
				"<button class='PopupCancelButton popCancel'>Cancel</button>" +
				"</div></span></td></tr>" +
				"</table>"+
				"</div>";
			var addNewContainer = $(addNewContainerTemplate);
			this._addNewContainer = addNewContainer.get(0);
			
			this._popIsPrimaryCurrency = addNewContainer.find(".isPrimaryCurrency").get(0);
			this._popConversionRate = addNewContainer.find(".conversionRate").get(0);
			this._popCurrencySymbol = addNewContainer.find(".currencySymbol").get(0);
			this._popCurrencyName = addNewContainer.find(".currencyName").get(0);
			
			this._btnPopConfirm = addNewContainer.find(".popConfirm").get(0);
			this._btnPopConfirm.onclick = this.btnPopConfirm_onClick.bindAsEventListener(this);
			this._btnPopCancel = addNewContainer.find(".popCancel").get(0);
			this._btnPopCancel.onclick = this.btnPopCancel_onClick.bindAsEventListener(this);
		}
		this._popCurrencySymbol.value = "";
		this._popConversionRate.value = 1;
		this._popCurrencyName.value = "";
		
		$(this._addNewContainer).dialog({
			title:"Add New Currency",
			dialogClass: 'no-close' ,
			closeonEscape:false,
			modal:true, 
			width:"400", 
			height:"auto", 
			resizable:false,
			closable:false
		});
	}
	
	CostCurrency.prototype.btnPopConfirm_onClick = function(){
		if (this._popCurrencyName.value=="") {
			alert("Please enter Currency Name");
			return;
		}
		if (this._popCurrencySymbol.value=="") {
			alert("Please enter Currency Symbol");
			return;
		}
		if (this.hasData == false){
			var params = {};
			params.costCurrencyLookupUid = "";
			params.costAfeMasterUid = this.masterAfeUid;
			
			params.currencyName = "USD";
			params.currencySymbol = "$";
			params.isPrimaryCurrency = 1;
			params.conversionRate = 1.0;
			this.submitNewCurrency(params);
		}
		var params = {};
		params.costCurrencyLookupUid = "";
		params.costAfeMasterUid = this.masterAfeUid;
		
		params.currencyName = this._popCurrencyName.value;
		params.currencySymbol = this._popCurrencySymbol.value;
		params.isPrimaryCurrency = this._popIsPrimaryCurrency.value;
		params.conversionRate = this._popConversionRate.value;
		
		this.submitNewCurrency(params);
	}
	
	CostCurrency.prototype.submitNewCurrency = function(params){
		var page = this.url + "update_currency_record";
		var self = this;
		this.needReload = true;
		
		$.post(page, params, function(response) {
			//location.reload(); 
			$(self._addNewContainer).dialog("close");
			self.loadCurrencyData();
		});
	}
	
	CostCurrency.prototype.btnPopCancel_onClick = function(){
		$(this._addNewContainer).dialog("close");
	}
	
	CostCurrency.prototype.btnConfirm_onClick = function(){
		var hasDeleted = false;
		for (var i=0;i<this._CostItems.length;i++){
			var item = this._CostItems[i];
			if (item.action == "delete") hasDeleted = true;
		}
		if (hasDeleted) {
			var deleteSelected = false;
			deleteSelected = confirm("Are you sure you want to delete\nthe selected record?");
			if(deleteSelected) this.processRecords();
		}else this.processRecords();
	}
	
	CostCurrency.prototype.btnCancel_onClick = function(){
		for (var i=0;i<this._CostItems.length;i++){
			var record = this._CostItems[i];
			this.setAction(record,null);
		}
	}
	
	CostCurrency.prototype.processRecords = function(){
		if (!this._processDialog)
			this._processDialog = $("<div>Processing...</div>");
		this._processDialog.dialog({dialogClass: 'no-close' ,closeonEscape:false,closable:false,modal:true,title:"WARNING"});
		
		for (var i=0;i<this._CostItems.length;i++){
			var record = this._CostItems[i];
			this.processRecord(record);
		}
		console.log("Currency Done - for");
		if (this._processDialog)
			this._processDialog.dialog("close");
		this.loadCurrencyData();
	}
	
	CostCurrency.prototype.processRecord = function(record){
		if (record.data.costCurrencyLookupUid==""){
			record.action = "save";
		}
		if (record.action == "delete"){
			this.deleteReport(record.uid);
		}else if (record.action == "save"){
			var params = {};
			params.costCurrencyLookupUid = record.data.costCurrencyLookupUid;
			params.costAfeMasterUid = this.masterAfeUid;
			
			params.currencyName = record.data.currencyName;
			params.currencySymbol = record.data.currencySymbol;
			params.conversionRate = record.data.conversionRate;
			params.isPrimaryCurrency = record.data.isPrimaryCurrency;
				
			if (record.modified.currencyName !== undefined) params.currencyName = record.modified.currencyName;
			if (record.modified.currencySymbol !== undefined) params.currencySymbol = record.modified.currencySymbol;
			if (record.modified.conversionRate !== undefined) params.conversionRate = record.modified.conversionRate;
			if (record.modified.isPrimaryCurrency !== undefined) params.isPrimaryCurrency = record.modified.isPrimaryCurrency;
			this.submitCurrency(params);
		}
	}
	
	CostCurrency.prototype.submitCurrency = function(params){
		var page = this.url + "update_currency_record";
		var self = this;
		this.needReload = true;
		
//		$.post(page, params, function(response) {
//			console.log("Currency Save " + params.currencyName );
//		});
		
		 $.ajax({
	           url: page,
	           data: params,
	           type: "POST",
	        }).done(function(data, textStatus, jqXHR) {
	        	console.log("Currency Save " + params.currencyName );
	        });
		 
	}
	
	CostCurrency.prototype.deleteReport = function(uid){
		var page = this.url + "delete_currency_record";
		var self = this;
		this.needReload = true;
		
		var params = {};
		params.costCurrencyLookupUid = uid;
		
//		$.post(page, params, function(response) {
//			console.log("Currency Delete " + params.currencyName );
//		});
		$.ajax({
           url: page,
           data: params,
           type: "POST",
        }).done(function(data, textStatus, jqXHR) {
        	console.log("Currency Delete " + params.currencyName );
        });
	}
	
	CostCurrency.prototype.loadCurrencyData = function(){
		this.currencyList = [];
		this.currencyRate = [];
		
		var page = this.url + "get_currency_list";
		var self = this;
		var collection = [];
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {

				var list = result.documentElement.getElementsByTagName("CostCurrencyLookup");
				for(var i=0; i<list.length; i++){
					var costCurrencyLookupUid = list[i].getElementsByTagName("costCurrencyLookupUid")[0].textContent;
					if (costCurrencyLookupUid!="") self.hasData = true;
					var currencyName = list[i].getElementsByTagName("currencyName")[0].textContent;
					var currencySymbol = list[i].getElementsByTagName("currencySymbol")[0].textContent;
					var conversionRate = list[i].getElementsByTagName("conversionRate")[0].textContent;
					var isPrimaryCurrency = list[i].getElementsByTagName("isPrimaryCurrency")[0].textContent
					
					var newItem = {};
					
					newItem.costCurrencyLookupUid = costCurrencyLookupUid;
					newItem.currencyName = currencyName;
					newItem.currencySymbol = currencySymbol;
					newItem.conversionRate = conversionRate;
					newItem.isPrimaryCurrency = isPrimaryCurrency;
					
					newItem.selectedIndex = (newItem.isPrimaryCurrency == "true" ? 1 : 0);
					newItem.isPrimaryLabel = (newItem.isPrimaryCurrency == "true" ? "Yes" : "No");
					newItem.isPrimaryCurrency = newItem.isPrimaryLabel;
					var selected = false;
					
					var record = {};
					record.data = newItem;
					record.uid = newItem.costCurrencyLookupUid;
					record.selected = selected;
					record.editMode = false;
					record.action = null;
					record.modified = {};
					collection.push(record);
				}
				self._CostItems = collection;
				
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> getCurrencyList Done");
			D3.UI.removeAllChildNodes(self._tbody);
			self.updateCostItemsList();
			self.updateRootContainer();
		});
	}
	
	CostCurrency.prototype.updateCostItemsList = function(){
		for (var i=0;i<this._CostItems.length;i++){
			var row = this._CostItems[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			tr.ondblclick = this.row_onDoubleClick.bindAsEventListener(this);
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	
	CostCurrency.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
			 var editor = this.getEditor(record,column,columnContainer);
			 if (editor){
				 record.field[column.field]._editor = editor;
				 columnContainer.appendChild(editor);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'/>");
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.check_onclick.bindAsEventListener(this);
			var btnDelete = $("<button class='RecordActionsDelete RecordActionsButton'>");
			btnDelete.data({uid:record.uid});
			btnDelete.get(0).onclick = this.btnDelete_onclick.bindAsEventListener(this);
			var btnCancel = $("<button class='RecordActionsButton RecordActionsCancel hide'></button>");
			btnCancel.data({uid:record.uid});
			btnCancel.get(0).onclick = this.btnRecordCancel_onclick.bindAsEventListener(this);
			$(columnContainer).append(checkbox);
			$(columnContainer).append(btnDelete);
			$(columnContainer).append(btnCancel);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox,"delete":btnDelete,"cancel":btnCancel};
		}
	}
	
	CostCurrency.prototype.checkSelectionEvent = function(event,data){
		data.selected = data.field["_recordAction"]._uiComponents.check.get(0).checked;
	}
	
	CostCurrency.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	
	CostCurrency.prototype.btnDelete_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,"delete");
	}
	
	CostCurrency.prototype.row_onDoubleClick= function(event){
		var target = event.currentTarget;
		var uid = $(target).data().uid;
		var row = this.getRecord(uid);
		if (!row.editMode){
			this.setAction(row,"save");
		}
	}
	
	CostCurrency.prototype.setAction = function(record,action){
		if (action === "delete"){
			record.action = "delete";
		}else if (action === "save"){
			record.action = "save";
			this.setEditMode(record,true);
		}else if (!action){
			record.action = null;
			this.setEditMode(record,false);
			record.modified = {};
		}
		this.updateRecordAction(record);
		this.updateRootContainer();
	}
	
	CostCurrency.prototype.btnRecordCancel_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,null);
	}
	
	CostCurrency.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		var span = document.createElement("span");
		span.className = "fieldRenderer";
		$(span).text(value);
		return span;
	}
	
	CostCurrency.prototype.refreshFieldEditor = function(record,fieldName){
		var value = record.data[fieldName];
		if (record.modified && record.modified.hasOwnProperty("isPrimaryCurrency"))
			value = record.modified[fieldName];
		if (fieldName == "isPrimaryCurrency"){
			$(record.field[fieldName]._editor).val(value=="true"?"1":"0");
		}
	}
	
	CostCurrency.prototype.getEditor = function(record,column,columnContainer){
		var fieldName = column.field;
		if (fieldName == "isPrimaryCurrency"){
			var config = "<select class='fieldEditor combobox hide'>" +
							"<option value='0'>No</option>" +
							"<option value='1'>Yes</option>" +
						"</select>";
			var combo = $(config).get(0);
			$(combo).data({uid:record.uid});
			$(combo).val(record.data["selectedIndex"]);
			combo.onchange = this.isPrimaryCurrency_onChange.bindAsEventListener(this);
			return combo;
		}else if (fieldName == "conversionRate"){
			var config = "<input class='hide' type='text'>";
			var inputBox = $(config).get(0);
			$(inputBox).data({uid:record.uid});
			$(inputBox).val(record.data["conversionRate"]);
			inputBox.onchange = this.conversionRate_onChange.bindAsEventListener(this);
			return inputBox;
		}else if (fieldName == "currencySymbol"){
			var config = "<input class='hide' type='text'>";
			var inputBox = $(config).get(0);
			$(inputBox).data({uid:record.uid});
			$(inputBox).val(record.data["currencySymbol"]);
			inputBox.onchange = this.currencySymbol_onChange.bindAsEventListener(this);
			return inputBox;
		}else if (fieldName == "currencyName"){
			var config = "<input class='hide' type='text'>";
			var inputBox = $(config).get(0);
			$(inputBox).data({uid:record.uid});
			$(inputBox).val(record.data["currencyName"]);
			inputBox.onchange = this.currencyName_onChange.bindAsEventListener(this);
			return inputBox;
		}
	}
	
	CostCurrency.prototype.getRecord = function(uid){
		for (var i=0;i<this._CostItems.length;i++){
			var record = this._CostItems[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	
	CostCurrency.prototype.setEditMode = function(row,editMode){
		row.editMode = editMode;
		var fields = row.field;
		
		for (var key in fields){
			var field = fields[key];
			var hasEditor = field._editor != null;
			if (field._renderer){
				if (editMode && hasEditor)
					$(field._renderer).addClass("hide");
				else
					$(field._renderer).removeClass("hide");
			}
			if (field._editor){
				if (editMode && hasEditor){
					$(field._editor).removeClass("hide");
					this.refreshFieldEditor(row,key);
				}else
					$(field._editor).addClass("hide");
			}
		}
	}
	
	CostCurrency.prototype.updateRecordAction = function(row){
		var uiComponents = row.field["_recordAction"]._uiComponents;
		
		
		if (row.action){
			if (row.action == "delete"){
				$(row.refRow).addClass("DeleteRecordBackground");
				uiComponents["check"].removeAttr("checked");
			}
			uiComponents["check"].addClass("hide");
			uiComponents["delete"].addClass("hide");
			uiComponents["cancel"].removeClass("hide");
			
		}else{
			$(row.refRow).removeClass("DeleteRecordBackground");
			uiComponents["check"].removeClass("hide");
			uiComponents["delete"].removeClass("hide");
			uiComponents["cancel"].addClass("hide");
			
		}
	}
	
	CostCurrency.prototype.updateRootContainer = function(){
		var hasAnythingToSubmit = false;
		for (var i=0;i<this._CostItems.length;i++){
			var record = this._CostItems[i];
			if (record.action)
				hasAnythingToSubmit = true;
		}
		if (hasAnythingToSubmit){
			$(this._standardButtonContainer).addClass("hide");
			$(this._confirmCancelButtonContainer).removeClass("hide");
		}else{
			$(this._standardButtonContainer).removeClass("hide");
			$(this._confirmCancelButtonContainer).addClass("hide");
		}
	}
	
	CostCurrency.prototype.isPrimaryCurrency_onChange = function(event){
		var target =$(event.currentTarget);
		var uid = target.data().uid;
		var record = this.getRecord(uid);
		record.modified.selectedIndex = target.val();
		record.modified.isPrimaryCurrency = target.val()==1?true:false;	 
	}
	
	CostCurrency.prototype.conversionRate_onChange = function(event){
		var target =$(event.currentTarget);
		var uid = target.data().uid;
		var record = this.getRecord(uid);
		record.modified.conversionRate = target.val();	 
	}
	
	CostCurrency.prototype.currencyName_onChange = function(event){
		var target =$(event.currentTarget);
		var uid = target.data().uid;
		var record = this.getRecord(uid);
		record.modified.currencyName = target.val();	 
	}
	
	CostCurrency.prototype.currencySymbol_onChange = function(event){
		var target =$(event.currentTarget);
		var uid = target.data().uid;
		var record = this.getRecord(uid);
		record.modified.currencySymbol = target.val();	 
	}
	
	window.CostCurrency = CostCurrency;
})(window);