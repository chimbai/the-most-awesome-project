/**
 * 
 */

(function(window){
	
	function CostAfeMas(){
		
		this.costAfeMasterUid = null;
		this.afeNumber = null;
		this.afeType = "";
		this.longDescription = null;
		this.workingInterest = null;
		this.parentCostAfeMasterUid = null;
		this.afeTotal = 0.0;
		this.afeUsed = 0.0;
		this.afeBalance = 0.0;
		this.lookupCompanyUid = null;
	}
	
	
	window.CostAfeMas = CostAfeMas;
})(window);