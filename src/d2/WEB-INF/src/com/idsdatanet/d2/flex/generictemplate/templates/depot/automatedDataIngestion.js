(function(window) {
	D3.inherits(ResetButton,D3.AbstractFieldComponent);
	function ResetButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ResetButton.prototype.getFieldRenderer = function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get("debug");
		if ((!this._fieldRenderer) && (c==1)){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Clean off All Transaction",
				css:"DefaultButton",
				callbacks:{
					onclick:this.onClick
				},
				context:this
			}) ;
		}			
		return this._fieldRenderer;
	}
	
	ResetButton.prototype.onClick = function(){
		var code = prompt("Enter Passcode to Clean :", "");
	
		var today = new Date();
		var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
		
		if (code==date){
			this.showPopUp(true);
			var params = {};
			params._invokeCustomFilter = "resetAll";
			this._node.commandBeanProxy.customInvoke(params, this.jobCompleted, this);
		}else{
			alert("WRONG Code");
		}
	}
	
	ResetButton.prototype.jobCompleted = function(response) {
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
		this.showPopUp(false);
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	ResetButton.prototype.refreshFieldRenderer = function() {
	}
	
	ResetButton.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Cleaning</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	
	D3.inherits(ReIndexALLButton,D3.AbstractFieldComponent);
	function ReIndexALLButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReIndexALLButton.prototype.getFieldRenderer = function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get("debug");
		if ((!this._fieldRenderer) && (c==1)){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Reindex ALL",
				css:"DefaultButton",
				callbacks:{
					onclick:this.onClick
				},
				context:this
			}) ;
		}			
		return this._fieldRenderer;
	}
	
	ReIndexALLButton.prototype.onClick = function(){
		var code = prompt("Enter Passcode to Run :", "");
	
		var today = new Date();
		var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
		
		if (code==date){
			this.showPopUp(true);
			var params = {};
			params._invokeCustomFilter = "reindexAll";
			this._node.commandBeanProxy.customInvoke(params, this.jobCompleted, this);
		}else{
			alert("WRONG Code");
		}
	}
	
	ReIndexALLButton.prototype.jobCompleted = function(response) {
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
		this.showPopUp(false);
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	ReIndexALLButton.prototype.refreshFieldRenderer = function() {
	}
	
	ReIndexALLButton.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Indexing</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	
	D3.inherits(FlushContainerButton,D3.AbstractFieldComponent);
	function FlushContainerButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	FlushContainerButton.prototype.getFieldRenderer = function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get("debug");
		if ((!this._fieldRenderer) && (c==1)){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Flush Con",
				css:"DefaultButton",
				callbacks:{
					onclick:this.reClick
				},
				context:this
			}) ;
		}			
		return this._fieldRenderer;
	}
	
	FlushContainerButton.prototype.reClick = function(){
		var params = {};
		params._invokeCustomFilter = "flushCon";
		params.goldboxIndexUid = this._node.getFieldValue("goldboxIndexUid");
		this._node.commandBeanProxy.customInvoke(params, this.flushCompleted, this);
	}
	
	FlushContainerButton.prototype.flushCompleted = function(response) {
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
	}
	
	FlushContainerButton.prototype.refreshFieldRenderer = function() {
	}
	
	D3.inherits(ReIndexButton,D3.AbstractFieldComponent);
	
	function ReIndexButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReIndexButton.prototype.getFieldRenderer = function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get("debug");
		if ((!this._fieldRenderer) && (c==1)){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"ReIndex",
				css:"DefaultButton",
				callbacks:{
					onclick:this.reClick
				},
				context:this
			}) ;
		}			
		return this._fieldRenderer;
	}
	
	ReIndexButton.prototype.reClick = function(){
		this.showPopUp(true);
		var params = {};
		params._invokeCustomFilter = "reIndex";
		params.goldboxIndexUid = this._node.getFieldValue("goldboxIndexUid");
		this._node.commandBeanProxy.customInvoke(params, this.reindexCompleted, this);
		
	}
	
	ReIndexButton.prototype.reindexCompleted = function(response) {
		
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
		this.showPopUp(false);
	}
	
	ReIndexButton.prototype.refreshFieldRenderer = function() {
	}
	
	ReIndexButton.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Indexing</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}

	D3.inherits(RepublishButton,D3.AbstractFieldComponent);
	
	function RepublishButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	RepublishButton.prototype.getFieldRenderer = function() {
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get("debug");
		if ((!this._fieldRenderer) && (c==1)){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Republish",
				css:"DefaultButton",
				callbacks:{
					onclick:this.reClick
				},
				context:this
			}) ;
		}			
		return this._fieldRenderer;
	}
	
	RepublishButton.prototype.reClick = function(){
		this.showPopUp(true);
		var params = {};
		params._invokeCustomFilter = "republish";
		params.goldboxIndexUid = this._node.getFieldValue("goldboxIndexUid");
		this._node.commandBeanProxy.customInvoke(params, this.reindexCompleted, this);
		
	}
	
	RepublishButton.prototype.reindexCompleted = function(response) {
		
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
		this.showPopUp(false);
	}
	
	RepublishButton.prototype.refreshFieldRenderer = function() {
	}
	
	RepublishButton.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Republish</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}

	D3.inherits(ReQATButton,D3.AbstractFieldComponent);
	
	function ReQATButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReQATButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var label = "Chg Status";
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				attributes:{
					title:"Chg Status",
				},
				callbacks:{
					onclick:this.showPopup
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReQATButton.prototype.showPopup = function(event){
		if (!this._container){
			this.initiateContainer();
		}
		
		$(this._container).dialog({title:"Goldbox Index",dialogClass: 'no-close' ,closeonEscape:false,modal:true, width:"200", height:"150", resizable:false,closable:false});
	}
	
	ReQATButton.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container'>" +
				"<select class='goldboxStatus' name='gStatus' id='gStatus'>" +
				  "<option value='PENDING'>PENDING</option>" +
				  "<option value='DONE'>DONE</option>" +
				  "<option value='ERROR'>ERROR</option>" +
				  "<option value='PUBLISHED'>PUBLISHED</option>" +
				"</select><br><br>" +
				"<div class='ConfirmButtonContainer'>" +
				"<button class='PopupConfirmButton popConfirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				"<button class='PopupCancelButton popCancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
				"</div>" +
			"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			this._goldboxStatus = container.find(".goldboxStatus").get(0);
			this._btnPopConfirm = container.find(".popConfirm").get(0);
			this._btnPopConfirm.onclick = this.btnPopConfirm_onClick.bindAsEventListener(this);
			this._btnPopCancel = container.find(".popCancel").get(0);
			this._btnPopCancel.onclick = this.btnPopCancel_onClick.bindAsEventListener(this);
		}
	}
	ReQATButton.prototype.xxxxx = function() {

	}
	
	ReQATButton.prototype.btnPopConfirm_onClick = function(){
	//	alert("Good " + this._goldboxStatus.value);
		$(this._container).dialog('close');
		this.showPopUp(true);
		var params = {};
		params._invokeCustomFilter = "changeStatus";
		params.statusValue = this._goldboxStatus.value;
		params.goldboxIndexUid = this._node.getFieldValue("goldboxIndexUid");
		this._node.commandBeanProxy.customInvoke(params, this.reindexCompleted, this);
		
	}
	
	ReQATButton.prototype.btnPopCancel_onClick = function(){
		$(this._container).dialog('close');
	}
			
//	ReQATButton.prototype.getFieldRenderer = function() {
//		var url_string = window.location.href;
//		var url = new URL(url_string);
//		var c = url.searchParams.get("debug");
//		if ((!this._fieldRenderer) && (c==1)){
//			this._fieldRenderer = D3.UI.createElement({
//				tag:"button",
//				text:"ReQAT",
//				css:"DefaultButton",
///				callbacks:{
//					onclick:this.reClick
//				},
//				context:this
//			}) ;
//		}			
//		return this._fieldRenderer;
//	}
	
	ReQATButton.prototype.reClick = function(){
		this.showPopUp(true);
		var params = {};
		params._invokeCustomFilter = "reqat";
		params.goldboxIndexUid = this._node.getFieldValue("goldboxIndexUid");
		this._node.commandBeanProxy.customInvoke(params, this.reindexCompleted, this);
		
	}
	
	ReQATButton.prototype.reindexCompleted = function(response) {
		var status = "";
		var doc = $(response);
		doc.find("Response").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
		});
		alert(status);
		this.showPopUp(false);
	}
	
	ReQATButton.prototype.refreshFieldRenderer = function() {
	}
	
	ReQATButton.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Changing Goldbox Status</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	
	D3.inherits(statusField, D3.AbstractFieldComponent);
	
	function statusField(){
		D3.AbstractFieldComponent.call(this);
	}
	
	statusField.prototype.getFieldRenderer = function() {
		
		if (this.getFieldValue()=="PENDING") { backcolor='white'; fontcolor='#f7c602'; }
		else if (this.getFieldValue()=="ERROR") { backcolor='white'; fontcolor='#ff6464'; }
		else if (this.getFieldValue()=="DONE") { backcolor='white'; fontcolor='#82FA58'; }
		else if (this.getFieldValue()=="PUBLISHED") { backcolor='white'; fontcolor='#afeeee'; }
		else { backcolor='white'; fontcolor='black'; }
		
		var containerTemplate = ""
			+ "<div style='font-weight: bold; color:"+ fontcolor +"'>" + this.getFieldValue() + "</div>" 

		var container = $(containerTemplate);
		this._fieldRenderer = container.get(0);
		return this._fieldRenderer;
	}
	
	D3.inherits(QATField,D3.AbstractFieldComponent);
	function QATField(){
		D3.AbstractFieldComponent.call(this);
		this._invertFlag = 1;
	}
	QATField.prototype.dispose = function() {
		if(this._node) this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		KpiRangeField.uber.dispose.call(this);
	};
	QATField.prototype.fieldChangeListener = function(event) {
		if(event.fieldName == "isInverted"){
			this._invertFlag = ("1" == this._node.getFieldValue("isInverted"));
		}
		if(event.fieldName == "@kpiTarget" || event.fieldName == "@kpiTolerance" || event.fieldName == "isInverted"){
			this.refreshFieldRenderer();
		}
	};
	QATField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var div = document.createElement("div");
			div.style.width = this._config.width + "px";
			div.style.height = this._config.height + "px";
			this._fieldRenderer = div;
			this._rangeWidth = this._config.width;
			this._tooltips = [this.createTooltip(div, 0)];
			this._range1 = document.createElement("div");
			this._range1.className = "KpiRangeBar";
			div.appendChild(this._range1);
			this._range2 = document.createElement("div");
			this._range2.className = "KpiRangeBar";
			div.appendChild(this._range2);
			
			this._range3 = document.createElement("div");
			this._range3.className = "KpiRangeBar";
			div.appendChild(this._range3);
			
			this._range4 = document.createElement("div");
			this._range4.className = "KpiRangeBar";
			div.appendChild(this._range4);
			
	//		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldChangeListener, this);
		}
		return this._fieldRenderer;
	};
	QATField.prototype.createTooltip = function(parent, index) {
		var arrow = document.createElement("span");
		arrow.className = index % 2 == 0 ? "KpiRangeTooltipArrow" : "KpiRangeTooltipArrowBottom"; 
		var textNode = document.createElement("span");
		//textNode.className = index % 2 == 0 ? "KpiRangeTooltipText" : "KpiRangeTooltipTextBottom";
		textNode.className = "KpiRangeTooltipTextBottom";
		arrow.appendChild(textNode);
		parent.appendChild(arrow);
		return {arrow: arrow, text: textNode};
	};
	QATField.prototype.updateTooltip = function(index, position, label) {
		var target = this._tooltips[index];
		target.arrow.style.left = (position - 6) + "px";
		if (label==0) target.arrow.style.display = "none";
		while(target.text.firstChild) target.text.removeChild(target.text.firstChild);
		var Test = document.createTextNode("Hello\u000aWorld");
		target.text.appendChild(Test);
	};
	QATField.prototype.refreshFieldRenderer = function() {
		var v1Ori = this._node.getFieldValue("@statusPending");
		var v2Ori = this._node.getFieldValue("@statusError");
		var v3Ori = this._node.getFieldValue("@statusDone");
		var v4Ori = this._node.getFieldValue("@statusPublish");
		v1Ori = v1Ori.replace(",", "");
		v2Ori = v2Ori.replace(",", "");
		v3Ori = v3Ori.replace(",", "");
		v4Ori = v4Ori.replace(",", "");
		var v1 = parseFloat(v1Ori);
		var v2 = parseFloat(v2Ori);
		var v3 = parseFloat(v3Ori);
		var v4 = parseFloat(v4Ori);
		if(isNaN(v1) || isNaN(v2) || isNaN(v3) || isNaN(v4)){
			this._fieldRenderer.className = "KpiRangeInvalid";
			return;
		}

		this._fieldRenderer.className = "KpiRange";
		this._fieldRenderer.style.backgroundColor = "White";
		this._fieldRenderer.onmouseover = this.fhover.bindAsEventListener(this);
		var total = v1 + v2 + v3 + v4;
		var p1 = Math.round(v1/total * this._rangeWidth);
		var p2 = Math.round(v2/total * this._rangeWidth);
		var p3 = Math.round(v3/total * this._rangeWidth);
		var p4 = Math.round(v4/total * this._rangeWidth);
		var mid = Math.round(0.5 * this._rangeWidth);
		var index = 0;

		index = 0;

		this._target = this._tooltips[0];
		this._target.arrow.style.left = (mid - 6) + "px";
		var summary = this._node.getFieldValue("@summary");
		if (summary==null) summary="";
		var sumArray = summary.split("||");
		
		var template = "<div>" +
						"Pending : " + v1 + "<br>" +
						"Error : " + v2 + "<br>" +
						"Done : " + v3 + "<br>" +
						"Publish : " + v4 + "<br>" +
						"<hr>" +
						"<table >" ;
		if (sumArray.length > 1)	{
			for (var i=0; i<sumArray.length; i++) {
				var record = sumArray[i];
				var records = record.split("==");
				var tableName = records[0];
				var datas = records[1];
				var data = datas.split(",");
				
				var vv1 = 0;
				var vv2 = 0;
				var vv3 = 0;
				var vv4 = 0;
				for (var o=0; o<data.length; o++) {
					var dats = data[o];
					var da = dats.split(":");
					var st = da[0];
					var va = da[1];
					if (st=="PENDING") vv1 = va;
					if (st=="ERROR") vv2 = va;
					if (st=="DONE") vv3 = va;
					if (st=="PUBLISHED") vv4 = va;
				}
				
				template = template + "<tr><td>" + tableName + "</td> <td style='display: flex;'>" +
				"<div style='background:#f7c602;width:50px;text-align: center;border-radius:50px;margin-left: 5px;'>" + vv1 + "</div>" +
				"<div style='background:#ff6464;width:50px;text-align: center;border-radius:50px;margin-left: 5px;'>" + vv2 + "</div>" +
				"<div style='background:#82FA58;width:50px;text-align: center;border-radius:50px;margin-left: 5px;'>" + vv3 + "</div>" +
				"<div style='background:#afeeee;width:50px;text-align: center;border-radius:50px;margin-left: 5px;'>" + vv4 + "</div>" +
				"</td></trd>";
				
			}			
						
		}				
						
				template = template +"</table>" +
						"</div>";
		
		var container = $(template);
		var contain = container.get(0);
		this._target.text.appendChild(contain);

		this._range1.style.width = p1 + "px";
		this._range1.style.backgroundColor = "#f7c602";
		this._range1.style.height =  "20px";
		this._range1.style.borderRadius = "50px";
		
		this._range2.style.width = p2 + "px";
		this._range2.style.left = p1 + "px";
		this._range2.style.backgroundColor = "#ff6464";
		this._range2.style.height =  "20px";
		this._range2.style.borderRadius = "50px";
		
		this._range3.style.width = p3 + "px";
		this._range3.style.left = (p1 + p2) + "px";
		this._range3.style.backgroundColor = "#82FA58";
		this._range3.style.height =  "20px";
		this._range3.style.borderRadius = "50px";
		
		this._range4.style.width = p4 + "px";
		this._range4.style.left = (p1 + p2 + p3) + "px";
		this._range4.style.backgroundColor = "#afeeee";
		this._range4.style.height =  "20px";
		this._range4.style.borderRadius = "50px";
	};
	
	QATField.prototype.fhover = function(event){
		var xx = window.innerHeight - this._target.arrow.getBoundingClientRect().top;
		if (xx <= 100)
			this._target.text.style.top = "-380px";
		else if (xx <= 250)
			this._target.text.style.top = "-280px";
		else
			this._target.text.style.top = "-80px";
	}
	
	D3.inherits(PasswordInputField,D3.TextInputField);
	function PasswordInputField(){
		D3.TextInputField.call(this);
	}
	
	PasswordInputField.prototype.refreshFieldEditor = function() {
		PasswordInputField.uber.refreshFieldEditor.call(this);
		if (this._fieldEditor) {
			this._fieldEditor.type = "password";
		}
	};
	
	PasswordInputField.prototype.refreshFieldRenderer = function() {
		if(this.fieldRenderer) {
			this.setFieldRendererValue("");
		}
	};
	
	D3.inherits(AutomatedDataIngestionDataNodeListener, D3.EmptyNodeListener);
	
	function AutomatedDataIngestionDataNodeListener() { }
	
	AutomatedDataIngestionDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "flushContainer"){
			return new FlushContainerButton();
		} else if(id == "reIndex"){
			return new ReIndexButton();
		} else if(id == "republish"){
			return new RepublishButton();	
		} else if(id == "reQat"){
			return new ReQATButton();		
		} else if(id == "status"){
			return new statusField();	
		} else if(id == "qatField"){
			return new QATField();		
		} else if (id=="password_field"){
			return new PasswordInputField();	
		} else if(id == "resetGB"){
			return new ResetButton();
		} else if(id == "reindexGB"){
			return new ReIndexALLButton();			
		} else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = AutomatedDataIngestionDataNodeListener;
})(window);