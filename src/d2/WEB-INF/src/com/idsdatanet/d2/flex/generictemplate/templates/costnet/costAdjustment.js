(function(window){

	D3.inherits(CostAdjustmentNodeListener,D3.EmptyNodeListener);
	
	function CostAdjustmentNodeListener() {
	}	

	CostAdjustmentNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "updateData") {
			this._commandBeanProxy = commandBeanProxy;
			
			this._btneUpdateData = document.createElement("button");
			this._btneUpdateData.className = "rootButtons";
			var label = "Update Data";
			var addNew = document.createTextNode(label);
			this._btneUpdateData.appendChild(addNew);
			this._btneUpdateData.onclick = this.updateDatas.bindAsEventListener(this);
			if (commandBeanProxy.rootNode.getFieldValue("@username")=="jwong"){
				return this._btneUpdateData;
			}
			
		}
	}
	
	CostAdjustmentNodeListener.prototype.updateDatas = function(event) {
		this._node = this._commandBeanProxy.rootNode;
		this._node.commandBeanProxy.addAdditionalFormRequestParams("_action", "updateData");
		this._node.commandBeanProxy.submitForServerSideProcess();
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	

	D3.CommandBeanProxy.nodeListener = CostAdjustmentNodeListener;
	
})(window);