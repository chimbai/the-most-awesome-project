(function(window){
	
	D3.inherits(TightHoleSetupLinkField,D3.HyperLinkField);
	
	function TightHoleSetupLinkField(){
		D3.HyperLinkField.call(this);
	}
	
	TightHoleSetupLinkField.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		if (!this._fieldRenderer){
			fieldRenderer = TightHoleSetupLinkField.uber.getFieldRenderer.call(this);
			fieldRenderer.appendChild(document.createTextNode(D3.LanguageManager.getLabel("button.tightHoleSetupLink")));
			fieldRenderer.removeAttribute("target");
			fieldRenderer.removeAttribute("href");
			fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}else{
			fieldRenderer = TightHoleSetupLinkField.uber.getFieldRenderer.call(this);
		}			
		return fieldRenderer;
	}
	TightHoleSetupLinkField.prototype.onclick = function(){
		if (window.parent){
			window.parent.tabsManager.select("tightholesetup");
		}
	}
	TightHoleSetupLinkField.prototype.refreshFieldRenderer = function() {
		
	};
	D3.inherits(OperationNameCustomField,D3.TextInputField);
	
	function OperationNameCustomField(){
		D3.TextInputField.call(this);
	}
	
	OperationNameCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			fieldEditor = OperationNameCustomField.uber.getFieldEditor.call(this);
			fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			fieldEditor = OperationNameCustomField.uber.getFieldEditor.call(this);
		}			
		return fieldEditor;
	}
	OperationNameCustomField.prototype.onKeyUp = function(event){
		this.checkOperationNameExist();
	}
	OperationNameCustomField.prototype.checkOperationNameExist = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	OperationNameCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "checkingOperationNameExist";
		params.name = this.getFieldEditor().value;
		params.isNewRecord = this._node.atts.isNewRecord? "1":"";
		params.parentWellUid = this._node.getFieldValue("wellUid");
		params.parentWellboreUid = this._node.getFieldValue("wellboreUid");
		return params;
	}
	
	OperationNameCustomField.prototype.loaded = function(response) {
		var self = this;
		this.fieldContainer.clearFieldUserMsgContainer();
		var isDuplicate = false;
		$(response).find('item').each(function(){
			if ($(this).attr('name')=='duplicate'){
				if ($(this).attr('value')=='true'){
					isDuplicate = true;
				}
			}
		});
		if (isDuplicate){
			var msg = D3.LanguageManager.getLabel("warning.opertaionNameExist");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);

		}
			
	}
	
	D3.inherits(OperationNodeListener,D3.EmptyNodeListener);
	
	function OperationNodeListener()
	{
		this._btnLockOperation = null;
	}
	
	OperationNodeListener.prototype.getLockOperationLabel = function(){
		return D3.LanguageManager.getLabel("button.lockOperation");
	}
	
	OperationNodeListener.prototype.getUnlockOperationLabel = function(){
		return D3.LanguageManager.getLabel("button.unlockOperation");
	}
	OperationNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "operationNameInWWO") {
			return new OperationNameCustomField();
		} else if (id == "tightHoleSetupLink") {
			return new TightHoleSetupLinkField();
		} else if (id == "defaultDatum") {
			return new D3.DefaultDatumCustomField();
		}else if(id == "DatumReferenceCombobox"){
			return new D3.DatumReferenceCombobox();
		} else if(id== "DatumTypeCombobox"){
			return new D3.DatumTypeCombobox();
		} else{
			return null;
		}
	}
	
	OperationNodeListener.prototype.getLabel = function(node,key) {
		var msg = D3.LanguageManager.getLabel(key);
		msg = msg.replace("{wellName}",this.getWellName(node));
		msg = msg.replace("{wellboreName}",this.getWellboreName(node));
		msg = msg.replace("{operationName}",node.getFieldValue("@wellName"));
		return msg;
	}
	OperationNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		
		 if (id == "toggleLockOperation") {
			if (!commandBeanProxy.rootNode.isAllowedAction("toggleLockOperation", "Operation")) {
				return null;
			}
			
			this._commandBeanProxy = commandBeanProxy;
			
			
			this._btnLockOperation = document.createElement("button");
			this._btnLockOperation.className = "rootButtons";
			var label = this.getLockOperationButtonLabel();
			var addNew = document.createTextNode(label);
			this._btnLockOperation.appendChild(addNew);
			this._btnLockOperation.onclick = this.toggleLockOperation.bindAsEventListener(this);
			
			
			return this._btnLockOperation;
		}
		
	}
	OperationNodeListener.prototype.toggleLockOperation = function(event) {
		this._commandBeanProxy.submitForm("toggleLockOperation");
	}
	
	
	OperationNodeListener.prototype.getLockOperationButtonLabel = function() {
		var operations = this._commandBeanProxy.rootNode.children["Operation"].getItems();
		var label = this.getLockOperationLabel();

		D3.forEach(operations,function(child) {
			if (!child.atts.isNewRecord) {
				if (child.getFieldValue("isLocked") == "1") {
					label = this.getUnlockOperationLabel();
					return;
				}
			}
		},this);
		return label;
	}
	
	OperationNodeListener.prototype.getConfirmationMessage = function(commandBeanProxy) {
		var operations = commandBeanProxy.rootNode.children["Operation"].getItems(); 
		var msg = null;
		D3.forEach(operations,function(node){
			var isLastOperation = node.getFieldValue("@isLastOperation");
			
			if (node.atts.isNewRecord){
				var operationCode = node.getFieldValue("operationCode");
				var parentWellboreUid = node.getFieldValue("@parentWellboreUid");
				
				if (node.getFieldValue("@well.wellName") != null && node.getFieldValue("@wellbore.wellboreName") != null) { // make sure the template has these two fields
					var operationName = node.getFieldValue("@wellName");
					if (operationCode.startWith("DRLLG")) {
						if (parentWellboreUid && parentWellboreUid != "") {
							msg= this.getLabel(node,"message.existingWellNewWellboreOperationCreation");
							return;
						} else {
							msg =  this.getLabel(node,"message.newWellWellboreOperation");
							return;
						}
					} else {
						msg = this.getLabel(node,"message.existingWellWellboreNewOperation");
						return;
					}
				}
			}
			if (node.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE)
			{
				if (isLastOperation == "true" && isLastOperation !=""){
					msg = D3.LanguageManager.getLabel("warning.lastOperation");
					return;
				}
			}
			
		},this);
		
		
		return msg;
	}
		
	
	OperationNodeListener.prototype.getWellName = function(node) {
		var wellName = node.getFieldValue("@well.wellName");
		if (!wellName || wellName == "") {
			return node.getFieldValue("@wellName");
		}
		return wellName;
	}
	
	OperationNodeListener.prototype.getWellboreName = function(node) {
		var wellboreName = node.getFieldValue("@wellbore.wellboreName");
		if (!wellboreName || wellboreName == "") {
			return node.getFieldValue("@wellName");
		}
		return wellboreName;
	}
	
	D3.CommandBeanProxy.nodeListener = OperationNodeListener;
	
})(window);