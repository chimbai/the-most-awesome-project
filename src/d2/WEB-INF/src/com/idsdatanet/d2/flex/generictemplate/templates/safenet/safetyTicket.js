(function(window){

	D3.inherits(SafetyTicketDetailLink, D3.AbstractFieldComponent);
	function SafetyTicketDetailLink() {
		D3.AbstractFieldComponent.call(this);
	}
	SafetyTicketDetailLink.prototype.getFieldRenderer = function() {
		if (!this._button)
		{
			this._button = document.createElement("button");
			var label = D3.LanguageManager.getLabel("button.showDetails");
			this._button.appendChild(document.createTextNode(label));
			this._button.onclick = this.pageRedirect.bindAsEventListener(this);
			this._button.className = "DefaultButton";
		}
		return this._button;
	};
	SafetyTicketDetailLink.prototype.pageRedirect = function(){
		var redirect_url = "safetyticketbycategory.html?gotowellop=" + escape(this._node.getFieldValue("operationUid")) + "&gotoFieldName=safetyTicketUid&gotoFieldUid=" + escape(this._node.getFieldValue("safetyTicketUid")) +"&gotoday=" + escape(this._node.getFieldValue("dailyUid"));
		var redirect_msg = D3.LanguageManager.getLabel("msg.redirect");
		this._node.commandBeanProxy.gotoUrl(redirect_url, redirect_msg);
	}
	
	D3.inherits(LessonTicketDetailNodeListener,D3.EmptyNodeListener);
	
	function LessonTicketDetailNodeListener()
	{
		
	}
	
	LessonTicketDetailNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "safety_ticket_detail_link"){
			return new SafetyTicketDetailLink();
		} else {
			return null;
		}
	}
	D3.CommandBeanProxy.nodeListener = LessonTicketDetailNodeListener;
})(window);