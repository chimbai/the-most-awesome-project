(function(window){
	
	D3.inherits(CostSummary, window.CostGrid);
	
	function CostSummary(){
		this.jqGridMain = "";
		this.option = null;
		this.jqGridTable = null;
		this.afeGrid = null;
		this.afeGrid2 = null;
		this.datalist = [];
	}
	
	CostSummary.prototype.init = function (options){
		this.option = options;
		this.baseUrl =  options.baseUrl;
		this.baseUrl = this.baseUrl.replace("costdailysummary.html", "webservice/costnetserviceother.html?method=");
		this.height = options.height;
		
		var template = this.getLayout();
		this.jqGridMain = $(template).get(0);
		
		this.filter = $(this.jqGridMain).find("#filter").get(0);
		this.filter.onchange = this.filterChanged.bindAsEventListener(this);
		
		this.gridCostSummary = $(this.jqGridMain).find("#grid_costSummary").get(0);
		
		this.getSummary();
	}
	
	CostSummary.prototype.getLayout = function(){
		var layoutTemplate = "" +
			"<div id='jqGrid_main'>" +
				"<div style='display: flex;'>" +
					"<div style='width: 220px;padding-top: 6px;'>" +
							"<select name='filter' id='filter' style='height: 24px;' class='fieldEditor comboBox'>" +
										  "<option value='current'>Current Ops</option>" +
										  "<option value='ass'>Asso Ops</option>" +
										"</select>" +
						"</div>" +
					"</div>" +	

				"<div  style='overflow: hidden;'> " +
					"<table id='grid_costSummary'></table>" +
			    	"<div id='jqGridSummaryPager'></div>" +
				"</div>" +
			"</div>";
		return layoutTemplate;
	}
	
	CostSummary.prototype.filterChanged = function(){
		this.getSummary();
	}
	
	CostSummary.prototype.getSummary = function(){
		var page = this.baseUrl + "get_cost_summary_todate";
		if (this.filter.value=="current") page = page + "&filter=ops";
		if (this.filter.value=="ass") page = page + "&filter=ass";
		var self = this;
		this.datalist  = [];
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var list = result.documentElement.getElementsByTagName("CostGroup");
				for(var i=0; i<list.length; i++){
					var isTangible = list[i].getElementsByTagName("isTangible")[0].textContent;
					var parentAccountCode = list[i].getElementsByTagName("parentAccountCode")[0].textContent;
					var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
					var afeShortDescription = list[i].getElementsByTagName("afeShortDescription")[0].textContent;
					var category = list[i].getElementsByTagName("category")[0].textContent;
					var afeTotal = list[i].getElementsByTagName("afeTotal")[0].textContent;
					var totalDaily = list[i].getElementsByTagName("totalDaily")[0].textContent;
					var cumDaily = list[i].getElementsByTagName("cumDaily")[0].textContent;
					var inAfe = list[i].getElementsByTagName("inAfe")[0].textContent;
					
					var cs = new CostSumm();
					if (isTangible=="false") cs.isTangible = "Intangible";
					else cs.isTangible = cs.isTangible = "Tangible";
					
					if (inAfe=="false") cs.inAfe = "Non AFE";
					else cs.inAfe = "AFE";
					
					cs.parentAccountCode = parentAccountCode;
					cs.accountCode = accountCode;
					cs.afeShortDescription = afeShortDescription;
					cs.category = category;
					cs.afeTotal = Number.parseFloat(afeTotal).toFixed(2);
					cs.totalDaily = Number.parseFloat(totalDaily).toFixed(2);
					cs.cumDaily = Number.parseFloat(cumDaily).toFixed(2);
					
					cs.run();
					self.datalist.push(cs);
				}
				var xx= "";
			}
		}).done(function(data, textStatus, jqXHR) {
			self.createGrid(); 
		});
	}
	
	CostSummary.prototype.createGrid = function(){
		if (this.afeGrid == null){
			this.afeGrid = $("#jqGridTable").jqGrid({
				data: this.datalist,
				datatype: 'local',
				height: this.height,
				altRows: true, 
				altclass: 'myAltRowClass',
				colModel: [
					{ label: 'inAfe', name: 'inAfe', width: 200 },
					{ label: 'isTangible', name: 'isTangible', width: 200 },
	                { label: 'Account Code', name: 'accountName', key: true, width: 200 },
	      //          { label: 'afeShortDescription', name: 'afeShortDescription', width: 150 },
	                { label: 'Category', name: 'category', width: 150 },
	                { label: 'AFE', name: 'afeTotal', width: 150, align: 'right', formatter: 'number', summaryTpl : "<b>{0}</b>",summaryType: "sum"},
	                { label: 'Daily', name: 'totalDaily', width: 150, align: 'right',formatter: 'number', summaryTpl : "<b>{0}</b>",summaryType: "sum"},
	                { label: 'Cum Cost', name: 'cumDaily', width: 150, align: 'right',formatter: 'number', summaryTpl : "<b>{0}</b>",summaryType: "sum"},
	            ],
	          // shrinkToFit: true,
	            autowidth: true,
	            viewrecords: true,
	            rowNum: 100,
	            pager: "#jqGridPager",
	        	grouping: true,
	            groupingView: {
	                groupField: ["inAfe", "isTangible","category"],
	                groupColumnShow: [false,false,false],
	                groupText: [
	                	"<b>{0}</b>",
	                	"<b>{0}</b>",
						"Category: <b>{0}</b>"
					],
	                groupOrder: ["asc"],
	                groupSummary: [true,true,true],
					groupSummaryPos: ['header','header','header'],
	                groupCollapse: true
	            },
	            caption: "Cost Summary",
			}).navGrid('#jqGridPager', { edit: false, add: false, del: false });
		
		} else{
			this.afeGrid.jqGrid('clearGridData');
			this.afeGrid.jqGrid('setGridParam', {data: this.datalist});
			this.afeGrid.trigger('reloadGrid');
		}


	}
	
	window.CostSummary = CostSummary;
	
	
})(window);