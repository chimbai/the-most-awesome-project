(function(window){
	
	D3.inherits(RetrieveNewOperationFromTownCustomField,D3.AbstractFieldComponent);
	function RetrieveNewOperationFromTownCustomField(){
		D3.AbstractFieldComponent.call(this);
	}
	RetrieveNewOperationFromTownCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.buildButton();
		}
		return this._fieldEditor;
	}
	RetrieveNewOperationFromTownCustomField.prototype.buildButton = function() {
		var rnoftConfig = "<div id='container' class='ui-layout-pane ui-layout-pane-center' align='center'>" +
			"<table cellspacing='0' cellpadding='0' border='0'> "+
			"<tr><td class='HtmlTableFieldHeaderItem' width='500px'><span class='HtmlTableLabel'>Well Operations:</span></td>" +
			"<td width='30px'></td>" +
			"<td class='HtmlTableFieldHeaderItem' width='200px'><span class='HtmlTableLabel'>Days:</span></td></tr>"+
			"<tr><td><select class='operations' size='20' style='width: 100%'>" + 
			"</select></td><td width='30px'></td>" +
			"<td><select class='days' size='20' style='width: 100%'>" + 
			"</select></td></tr>"+
			"</table></div>";
		var table = $(rnoftConfig);
		this._operations = table.find(".operations").get(0);
		this._operations.onchange = this.operationSelected.bindAsEventListener(this);
		this.showPopUp(true);
		this.retrieveOperations();
		this._days = table.find(".days").get(0);
		this._days.onchange = this.daySelected.bindAsEventListener(this);
		this.showPopUp(false);
		return table.get(0);
	}	
	RetrieveNewOperationFromTownCustomField.prototype.retrieveOperations = function(){
		var params = {};
		params._invokeCustomFilter = "flexRetrieveOperations";
		
		this._node.commandBeanProxy.customInvoke(params,this.operationsLoaded,this);
	}
	RetrieveNewOperationFromTownCustomField.prototype.operationsLoaded = function(response) {
		var doc = $(response);
		var operationslist = this._operations;
		D3.UI.removeAllChildNodes(operationslist);
		var self = this;
		doc.find("Operation").each(function(index,item){
			var data = $(this);
			var operationUid = data.find("operationUid").text();
			var name = data.find("name").text();
			if (operationUid){
				option = document.createElement("option");
				option.setAttribute("value", operationUid);
				option.appendChild(document.createTextNode(name));
				operationslist.appendChild(option);
			}
		},this);
	}
	RetrieveNewOperationFromTownCustomField.prototype.operationSelected = function(){
		this.showPopUp(true);
		this._node.setFieldValue("@selectedOperationUid", this._operations.value);
		this.retrieveDays();
		this.showPopUp(false);
	}
	RetrieveNewOperationFromTownCustomField.prototype.daySelected = function(){
		this._node.setFieldValue("@selectedDailyUid", this._days.value);
	}
	RetrieveNewOperationFromTownCustomField.prototype.retrieveDays = function(){
		var params = {};
		params._invokeCustomFilter = "flexRetrieveDays";
		params.operationUid = this._operations.value;
		this._node.commandBeanProxy.customInvoke(params,this.daysLoaded,this);
	}
	RetrieveNewOperationFromTownCustomField.prototype.daysLoaded = function(response) {
		var doc = $(response);
		var dayslist = this._days;
		D3.UI.removeAllChildNodes(dayslist);
		var self = this;
		option = document.createElement("option");
		option.setAttribute("value", "");
		option.appendChild(document.createTextNode("..."));
		dayslist.appendChild(option);
		doc.find("Day").each(function(index,item){
			var data = $(this);
			var dailyUid = data.find("dailyUid").text();
			var label = data.find("label").text();
			if (dailyUid){
				option = document.createElement("option");
				option.setAttribute("value", dailyUid);
				option.appendChild(document.createTextNode(label));
				dayslist.appendChild(option);
			}
		},this);

	}
	
	RetrieveNewOperationFromTownCustomField.prototype.showPopUp = function(enable) {
		if (enable){
			var _popUpDiv = "<div>Loading data from town...</div>";
			this._popUpDialog = $(_popUpDiv);
			$(this._popUpDialog).dialog({
				title:"Please wait",
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"350", 
				height:"75", 
				resizable:false,
				closable:false,
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}else{
			$(this._popUpDialog).dialog('close');
		}
	}
	D3.inherits(RetrieveNewOperationFromTownDataNodeListener,D3.EmptyNodeListener);
	
	function RetrieveNewOperationFromTownDataNodeListener()
	{

	}
	
	RetrieveNewOperationFromTownDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "WWODChooser"){
			return new RetrieveNewOperationFromTownCustomField();
		}else{
			return RetrieveNewOperationFromTownDataNodeListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	RetrieveNewOperationFromTownDataNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "retrieveNewOperationFromTown"){
			this._commandBeanProxy = commandBeanProxy;
			var newRootBtn = document.createElement("button"); 
			newRootBtn.className = "rootButtons";
			newRootBtn.setAttribute("style","background-color:#E3E1B8");
			var labels = document.createTextNode("Retrieve New Operation From Town");
			newRootBtn.appendChild(labels);
			newRootBtn.onclick = this.onClickRetrieveNewOperationFromTown.bindAsEventListener(this);
			return newRootBtn;
		} else {
			return null;
		}
	}
	
	RetrieveNewOperationFromTownDataNodeListener.prototype.onClickRetrieveNewOperationFromTown = function(event) {
		this._commandBeanProxy.addAdditionalFormRequestParams("retrieveNewWellFromTown","1");
		this._commandBeanProxy.submitForServerSideProcess();
	}

	D3.CommandBeanProxy.nodeListener = RetrieveNewOperationFromTownDataNodeListener;

})(window);