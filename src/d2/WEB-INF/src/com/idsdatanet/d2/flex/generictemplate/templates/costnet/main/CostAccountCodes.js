/**
 * CostAccountCodes Table store fields.
 */

(function(window){
	
	function CostAccountCodes(){
		
		this.costAccountCodesUid = null;
		this.parentAccountCode = "";
		this.accountCode = "";
		this.shortDescription = null;
		this.accountName = null;
		this.category = null;
		this.subcategory = null;
		this.isTangible = false;
		this.operationCode = null;
	}
	
	CostAccountCodes.prototype.run = function(){
		if (this.isTangible==false) this.isTangible= "No";
		if (this.isTangible==true) this.isTangible= "Yes";
		if (this.category == "") this.category = "-";
		if (this.subcategory == "") this.subcategory = "-";
		this.accountName = (this.parentAccountCode==""?"-":this.parentAccountCode) + " :: " + (this.accountCode==""?"-":this.accountCode) + " :: " + this.shortDescription;
	}
	
	window.CostAccountCodes = CostAccountCodes;
})(window);