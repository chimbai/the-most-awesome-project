(function(window){

	D3.inherits(CostItemsSelectionPopup,D3.AbstractFieldComponent);
	function CostItemsSelectionPopup(node) {
		D3.AbstractFieldComponent.call(this);
		this._node = node;
		this._columns = [
		    		     {
		    		    	 type: "recordAction",
		    		    	 width:"10px"
		    		     },
		    		     {
		    		    	 type:"field",
		    		    	 field:"category",
		    		    	 width:"80px",
		    		    	 align:"left",
		    		    	 title:"Category"
		    		     },   
		    		     {
		    		    	 type:"field",
		    		    	 field:"accountCode",
		    		    	 width:"80px",
		    		    	 align:"left",
		    		    	 title:"Account Code"
		    		     },    
		    		     {
		    		    	 type:"field",
		    		    	 field:"shortDescription",
		    		    	 width:"300px",
		    		    	 align:"left",
		    		         title:"Short Description"
		    		     }
		    		    ];
		this.cachedSelectedOperationList = [];
		this.cachedSelectedWorkOrder = [];
		this.showMsg(D3.LanguageManager.getLabel("label.pleasewait"), D3.LanguageManager.getLabel("label.loading"), false);
		this.showPopup();
	}
	
	CostItemsSelectionPopup.prototype.showPopup = function(){
		if (!this._container){
			this.initiateContainer();
		}
		this.hidePopup();
		$(this._container).dialog({title:"Account Code",dialogClass: 'no-close' ,
			closeonEscape:false,modal:true, width:"1024", height:"500", resizable:false,closable:false});
	}
	CostItemsSelectionPopup.prototype.showMsg = function(title, msg, showCloseButton) {
		if(this.$popup && this.$popup.dialog("isOpen")){
			this.$popup.dialog("destroy");
		}
		if(! this.popupContentDiv){
			this.popupContentDiv = document.createElement("div");
			this.popupContentDiv.style.padding = "15px 10px";	
		}
		while(this.popupContentDiv.hasChildNodes()) this.popupContentDiv.removeChild(this.popupContentDiv.lastChild);
		this.popupContentDiv.appendChild(document.createTextNode(msg));
		this.$popup = $(this.popupContentDiv).dialog({title: title, minHeight: 10, resizable: false, closeOnEscape: false, modal:true,
			 open: function(event, ui) { 
		        	$(".ui-dialog-titlebar-close", ui.dialog).hide();
		        }
		});
		this.popupStartTime = new Date();
	};
	CostItemsSelectionPopup.prototype.hidePopup = function()
	{
		this.$popup.dialog("destroy");
	}
	CostItemsSelectionPopup.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container' style='overflow-y: hidden;'>" +
//					"<div><table width='100%'>" +
//					"<tr><td width='60%'>" +
//					"<div class='multiselect dailySelector'>" +
//					"<div class='multiselect-Title floatItem-Left' style='margin-top: 5px;'>"+D3.LanguageManager.getLabel("pop.item.wells")+"</div>"+
//					"<div class='floatItem-Left'><input type='text' style='width:330px' class='opsSearch'/>" +
//					"<button class='DefaultButton btnopsSearch' style='margin: 3px;'>"+D3.LanguageManager.getLabel("button.search")+"</button>"+
//					"<div class='multiselect-ButtonContainer'>" +
//					"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='fieldEditor operationList'>" +
//					"</ul>" +
//					"</div></div></div></td>"+
//					"<td width='40%'>" +
//					"<div class='multiselect dailySelector'>" +
//					"<div class='multiselect-Title floatItem-Left' style='margin-top: 5px;'>"+D3.LanguageManager.getLabel("pop.item.workorder")+"</div>"+
//					"<div class='floatItem-Left'><input type='text' style='width:180px' class='woSearch'/>" +
//					"<button class='DefaultButton btnwoSearch' style='margin: 3px;'>"+D3.LanguageManager.getLabel("button.search")+"</button>"+
//					"<div class='multiselect-ButtonContainer'>" +
//					"<ul style='list-style-type: none; margin: 0px; padding: 0px;width:250px' class='fieldEditor woList'>" +
//					"</ul>" +
//					"</div></div></div></td>"+
//					"</table></div>"+
			
					// New Table design
					"<div><table width='100%'>" +
					"<tr><td width='60%'>" +
					"<tr><td width='60%'>" +
					"<input type='checkbox' id='previousDay' class='previousDay'>" +
					"<label for='latestReportCB' class='textBold'>Previous Day</label>  " +
					"<input type='checkbox' id='afe' class='afe'>" +
					"<label for='latestReportCB' class='textBold'>AFE</label>  " +
					"<input type='checkbox' id='accountCode' class='accountCode'>" +
					"<label for='latestReportCB' class='textBold'>Cost Account Code</label><br>" +
					"</td>" +
					"</table></div>"+
					
					"<div class='StandardButtonContainer'>" +
					    "<button class='RootButton SelectAll'>"+D3.LanguageManager.getLabel("button.selectAll")+"</button>" +
						"<button class='RootButton UnselectAll'>"+D3.LanguageManager.getLabel("button.unselectAll")+"</button>" +
						"<button class='RootButton Close'>"+D3.LanguageManager.getLabel("button.close")+"</button>" +
						"<button class='RootButton Add2List'>Add Selected >></button>" +
					"</div><div style='overflow-x: hidden;overflow-y: auto;height:280px'>" +
	 				"<table class='SimpleGridTable reportListing'><thead><tr>"+this.getColumnHeader()+"</tr></thead><tbody class='costItemsList-TBODY'></tbody></table>"+
				"</div></div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			
			this._btnSelectAll = container.find(".SelectAll").get(0);
			this._btnSelectAll.onclick = this.btnSelectAll_onClick.bindAsEventListener(this);
			this._btnUnselectAll = container.find(".UnselectAll").get(0);
			this._btnUnselectAll.onclick = this.btnUnselectAll_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
			this._btnAdd2List = container.find(".Add2List").get(0);
			this._btnAdd2List.onclick = this.btnAdd2List_onClick.bindAsEventListener(this);
			
			
			this._tbody = container.find(".costItemsList-TBODY").get(0);
			this.loadData();
		}
	}
	CostItemsSelectionPopup.prototype.btnOpsSearch_onClick = function(){
		this.loadOperations();
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.btnWOSearch_onClick = function(){
		this.loadWorkOrders();
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.btnClose_onClick = function(){
		$(this._container).dialog("close");
	}
	CostItemsSelectionPopup.prototype.btnAdd2List_onClick = function(){
	//	$(this._container).dialog("close");
		var count = 0;
		for (var key in this._CostItems) {
			if (this._CostItems[key].selected) count ++;
		}
		
		if (count>50){
			alert("Too Much data selected ");
		} else if (count>0){
			for (var key in this._CostItems) {
				if (this._CostItems[key].selected) {
					var children = this._node.addNewChildNodeOfClass("CostDailysheet");
					if (children._items.length > 0){
						var newChild = children._items[0];
						var aCode = this._CostItems[key].data["accountCode"] + "::" + this._CostItems[key].data["shortDescription"];
						newChild.setFieldValue("@accountCode", aCode);
						newChild.setFieldValue("accountCode", this._CostItems[key].data["accountCode"]);
						newChild.setFieldValue("afeShortDescription", this._CostItems[key].data["shortDescription"]);
						newChild.setFieldValue("category", this._CostItems[key].data["category"]);
						
						newChild.fireRefreshNodeEvent();
					}
				}
			}
			

		}else {
			alert("Please select at least 1 ");
		}

	}
	CostItemsSelectionPopup.prototype.btnSelectAll_onClick = function(){
		for (var key in this._CostItems) {
			if (!this._CostItems[key].selected){
				var item = this._CostItems[key];
		//		this.checkSelection(item, true);
				var uiComponents = item.field["_recordAction"]._uiComponents;
				uiComponents["check"].get(0).checked = true;
			}
			this._CostItems[key].selected=true;
		}
	}
	CostItemsSelectionPopup.prototype.btnUnselectAll_onClick = function(){
		for (var key in this._CostItems) {
			if (this._CostItems[key].selected){
				var item = this._CostItems[key];
	//			this.checkSelection(item, false);
				var uiComponents = item.field["_recordAction"]._uiComponents;
				uiComponents["check"].get(0).checked = false;
			}
			this._CostItems[key].selected=false;
		}
	}
	CostItemsSelectionPopup.prototype.loadOperations = function(){
		this._lookupItems = this.getMultiSelectLookupReference();

		var container = this._operationList;
		D3.UI.removeAllChildNodes(container);
		this._checkboxes = [];
		D3.forEach(this._lookupItems.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				if (this.cachedSelectedOperationList.indexOf(item.data)==-1) cb.checked=false;
				else cb.checked=true;
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this,item.data);
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		this.loadWorkOrders();
	}
	CostItemsSelectionPopup.prototype.loadWorkOrders = function(){
		var operationUids = this.getSelectedOperation();
		var params = {};
		params._invokeCustomFilter = "getAvailableWorkOrder";
		params.costVendorInvoiceUid = this._node.primaryKeyValue;
		params.lookupCompanyUid = this._node.getFieldValue("lookupCompanyUid");
		params.operationUids = operationUids;
		params.workOrderFilter = this._woSearch.value;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		var doc = $(response);
		var currentList = null;
		
		doc.find("WorkOrder").each(function(index, item){
			var newItem = new D3.LookupItem();
			//var item = $(this);
			newItem.data = $(item).get(0).getAttribute("costWorkOrderUid");
			newItem.label = $(item).get(0).getAttribute("workOrderNumber");
			lookupReference.addLookupItem(newItem);
		});

		var container = this._woList;
		D3.UI.removeAllChildNodes(container);
		this._woCheckboxes = [];
		D3.forEach(lookupReference.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				if (this.cachedSelectedWorkOrder.indexOf(item.data)==-1) cb.checked=false;
				else cb.checked=true;
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._woCheckboxes[item.data] = cb;
				this._woCheckboxes[item.data].onclick = this.btnWoCheckbox_onClick.bindAsEventListener(this,item.data);
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
	}
	CostItemsSelectionPopup.prototype.getSelectedOperation = function(){
		var operationUids = "";
		var allOperationsUids = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked) {
				operationUids += (operationUids==""?"":",") + "'"+key+"'";
			}
			allOperationsUids += (allOperationsUids==""?"":",") + "'"+key+"'";
		}
		if (operationUids=="") operationUids = allOperationsUids;
		return operationUids;
	}
	CostItemsSelectionPopup.prototype.getSelectedWorkOrder = function(){
		var workOrderUids = "";
		for (var key in this._woCheckboxes) {
			if (this._woCheckboxes[key].checked) {
				workOrderUids += (workOrderUids==""?"":",") + "'"+key+"'";
			}
		}
		return workOrderUids;
	}
	CostItemsSelectionPopup.prototype.getMultiSelectLookupReference = function(){
		var params = {};
		
		params._invokeCustomFilter = "getAvailableOperations";
		params.costVendorInvoiceUid = this._node.primaryKeyValue;
		params.lookupCompanyUid = this._node.getFieldValue("lookupCompanyUid");
	//	params.operationNameLike = this._opsSearch.value;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		var doc = $(response);
		var currentList = null;
		
		doc.find("Operation").each(function(index, item){
			var newItem = new D3.LookupItem();
			//var item = $(this);
			newItem.data = $(item).get(0).getAttribute("operationUid");
			newItem.label = $(item).get(0).getAttribute("operationName");
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	CostItemsSelectionPopup.prototype.btnOperation_onClick = function(e, data){
		if (this._checkboxes[data].checked){
			this.cachedSelectedOperationList.push(data);
		}else{
			var index = this.cachedSelectedOperationList.indexOf(data);
			if (index >-1) this.cachedSelectedOperationList.splice(index,1);
		}
		this.loadWorkOrders();
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.btnWoCheckbox_onClick = function(e, data){
		if (this._woCheckboxes[data].checked){
			this.cachedSelectedWorkOrder.push(data);
		}else{
			var index = this.cachedSelectedWorkOrder.indexOf(data);
			if (index >-1) this.cachedSelectedWorkOrder.splice(index,1);
		}
		this.loadData();
	}
	CostItemsSelectionPopup.prototype.loadData = function(){
		var operationUids = this.getSelectedOperation();
		var workOrderUids = this.getSelectedWorkOrder();
		var params = {};
	//	params._invokeCustomFilter = "getAvailableCostItems";
		params._invokeCustomFilter = "getCostAccountCodesList";
		params.costVendorInvoiceUid = this._node.primaryKeyValue;
		params.lookupCompanyUid = this._node.getFieldValue("lookupCompanyUid");
		params.operationUids = operationUids;
		params.workOrderUids = workOrderUids;
	//	params.workOrderFilter = this._woSearch.value;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var doc = $(response);
		var self = this;
		var collection = [];
		doc.find("Code").each(function(index,item){
			var newItem = {};
			var data = $(this);
			
			newItem.accountCode = data.find("accountCode").text();
			newItem.shortDescription = data.find("shortDescription").text();
			newItem.category = data.find("category").text();
			var selected = false;
//			var childGroup = self._node.children;
//			if (childGroup!=null){
//				if (childGroup.hasOwnProperty("CostVendorInvoiceDetail")) {
//					for (var key in childGroup.CostVendorInvoiceDetail._items)
//					{
//						var childNode = childGroup.CostVendorInvoiceDetail._items[key];
//						var costDailysheetUid = childNode.getFieldValue("costDailysheetUid"); 
//						if (newItem.costDailysheetUid==costDailysheetUid) {
//							selected = true;
//							break;
//						}
//					};
//				}
//			}
			
			var record = {};
			record.data = newItem;
			record.uid = newItem.accountCode;
			record.selected = selected;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		D3.UI.removeAllChildNodes(this._tbody);
		this._CostItems = collection;
		this.updateCostItemsList();
	}
	CostItemsSelectionPopup.prototype.updateCostItemsList = function(){
		for (var i=0;i<this._CostItems.length;i++){
			var row = this._CostItems[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	CostItemsSelectionPopup.prototype.generateDataColumn = function(elemTR,record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'/>");
			if (record.selected) checkbox.get(0).checked = true;
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.checkSelectionEvent.bindAsEventListener(this,record);
			$(columnContainer).append(checkbox);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox};
		}
	}
	CostItemsSelectionPopup.prototype.checkSelectionEvent = function(event,data){
		//this.checkSelection(data, null);
		data.selected = data.field["_recordAction"]._uiComponents.check.get(0).checked;
	}
	CostItemsSelectionPopup.prototype.checkSelection = function(data,checked){
		this._node.setEditMode(true); 
		var nodeExist = false;
		if (checked!=null) data.selected = checked;
		else data.selected = !data.selected;
		var childGroup = this._node.children;
		if (childGroup!=null){
			if (childGroup.hasOwnProperty("CostVendorInvoiceDetail")) {
				for (var key in childGroup.CostVendorInvoiceDetail._items)
				{
					var childNode = childGroup.CostVendorInvoiceDetail._items[key];
					var costDailysheetUid = childNode.getFieldValue("costDailysheetUid"); 
					if (data.data["costDailysheetUid"]==costDailysheetUid) {
						if (!data.selected) {
							var costVendorInvoiceDetailUid = childNode.primaryKeyValue; //jwong
							var params = {}; 
							params._invokeCustomFilter = "markDetailAsDeleted";
							params.costVendorInvoiceDetailUid = costVendorInvoiceDetailUid;
							params.markDeleted = true;
							var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
							childNode.removeNode();
						} else {
							nodeExist = true;
						}
						break;
					}
				};
			}
		}
		
		if (data.selected && !nodeExist) {
			var children = this._node.addNewChildNodeOfClass("CostVendorInvoiceDetail");
			if (children._items.length > 0){
				var newChild = children._items[0];
				newChild.setFieldValue("costDailysheetUid", data.data["costDailysheetUid"]);
				newChild.setFieldValue("@afeNumber", data.data["afeNumber"]);
				newChild.setFieldValue("@workOrderNumber", data.data["workOrderNumber"]);
				newChild.setFieldValue("@ticketNumber", data.data["ticketNumber"]);
				newChild.setFieldValue("@dayDate", data.data["dayDate"]);
				newChild.setFieldValue("@accountCode", data.data["accountCode"]);
				newChild.setFieldValue("@itemDescription", data.data["itemDescription"]);
				newChild.setFieldValue("@itemTotal", data.data["lineTotal"]);
				newChild.setFieldValue("@chargeScope", data.data["chargeScope"]);
				newChild.setFieldValue("@cost", data.data["itemTotal"]);
				newChild.setFieldValue("invoiceTotal", data.data["invoiceTotal"]==null?data.data["itemTotal__raw"]:data.data["invoiceTotal"]);
				newChild.fireRefreshNodeEvent();
			}
		}
	}
	
	CostItemsSelectionPopup.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th class='SimpleGridHeaderCell' style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}

	CostItemsSelectionPopup.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	CostItemsSelectionPopup.prototype.getRecord = function(uid){
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	CostItemsSelectionPopup.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		var span = document.createElement("span");
		span.className = "fieldRenderer";
		$(span).text(value);
		return span;
	}
	
	D3.inherits(CostDailysheetNodeListener,D3.EmptyNodeListener);
	
	function CostDailysheetNodeListener() {
	}
	
	CostDailysheetNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
		if (id == "getCost") {
			var getCostBtn = document.createElement("button"); 
			getCostBtn.className = "rootButtons";
			getCostBtn.appendChild(document.createTextNode("Get Cost"));
			getCostBtn.onclick = this.onclickGetCost.bindAsEventListener(this);
			//this.startStatusCheck();
			return getCostBtn;
		}
	};

	CostDailysheetNodeListener.prototype.onclickGetCost = function() {
		
	};
	
	CostDailysheetNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "get_cost") {
			btn = $("<button class='DefaultButton' style='margin:3px'>Get Cost</button>");
			return btn.get(0);
		}
	}
	
	CostDailysheetNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		this._node = node;
		if (id == "get_cost") {
		//	this.continueFromPreviousRunPopup(node, id, button);
			return new CostItemsSelectionPopup(node);
		}
	};
	
	CostDailysheetNodeListener.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	D3.CommandBeanProxy.nodeListener = CostDailysheetNodeListener;
	
})(window);