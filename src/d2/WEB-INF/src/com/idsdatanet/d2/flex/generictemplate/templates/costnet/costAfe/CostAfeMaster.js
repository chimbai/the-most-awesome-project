(function(window){
	
	D3.inherits(CostAfeMaster,window.CostGrid);
	CostAfeMaster.DEL = "del";
	CostAfeMaster.SAV = "sav";
	
	function CostAfeMaster(){
		this.costAfeMaster = [];
		this.costAfeDetails = [];
		//this.accountCodeList ={};
		this.modifiedRecords = [];
		this.deletedRecords = [];
		this.baseUrl = "";
		this.height = "";
		this.rootBox = "";
		this.afeNoBox = "";
		this.currentAction = "";
		this.primaryCurrency = "";	
		this.selectedAfe = "";
		this.afeType = "";
		this.parentAfe = "";
		this.screenLayout = "";
		this.jqGridMain = "";
		this.option = null;
		this.gridCostDetail = null;
		this.hasSup = false;
		this.afeGrid = null;
		this.showWorkingInterest = true;
		this.afeMaster = null;
		this.xlsImport = "costnet/cost_afe_masters.xls";
		this.priceixlsImport = "costnet/cost_price_list.xls";
		this.orderixlsImport = "costnet/cost_service_order.xls";
		this.needRefresh = false;
		this.isLocking = false;
		this.allowLocking = false;
		this.version = "ver. 20210302";
	}
	
	CostAfeMaster.prototype.init = function (options){
		this.option = options;
		this.option.width = this.option.width - 37;
		this.baseUrl =  options.baseUrl;
		this.height = options.height;
		var template = this.getLayout();
		var container = $(template);
		this.jqGridMain = $(template).get(0);
		
		this.dialog = $(this.jqGridMain).find("#dialog").get(0);
	//	this.cvsDialog = $(this.jqGridMain).find("#cvsDialog").get(0);
		this.loadingDialog = $(this.jqGridMain).find("#loadingDialog").get(0);
		
		this.progressbar = $(this.jqGridMain).find("#progressbar" + this.option.counter).get(0);
		this.progressLabel = $(this.jqGridMain).find(".progress-label").get(0);
		
		// === Main 3 Fields ===
		this.afeNoBox = $(this.jqGridMain).find("#afeNo").get(0);
		this.afeComment = $(this.jqGridMain).find("#afeComment").get(0);
		this.afeinterest = $(this.jqGridMain).find("#afeinterest").get(0);
		
		this.afeNoBox.onchange = this.fieldOnChange.bindAsEventListener(this);
		this.afeComment.onchange = this.fieldOnChange.bindAsEventListener(this);
		this.afeinterest.onchange = this.fieldOnChange.bindAsEventListener(this);
		
		// == Span ==
		this.afeNoSpan = $(this.jqGridMain).find("#afeNoSpan" + this.option.counter).get(0);
	//	this.afeNoSpan.ondblclick = this.fieldOnChange.bindAsEventListener(this);
		this.afeCommentSpan = $(this.jqGridMain).find("#afeCommentSpan" + this.option.counter).get(0);
	//	this.afeCommentSpan.ondblclick = this.fieldOnChange.bindAsEventListener(this);
		
		this.wi1 = $(this.jqGridMain).find("#wi1").get(0);
		
		// === Functional Section ===
		this.groupCat = $(this.jqGridMain).find("#groupCat").get(0);
		this.freezeCheckBox = $(this.jqGridMain).find("#freezeCheckBox").get(0);
	//	this.afeGroup = $(this.jqGridMain).find("#afeGroup").get(0);
		this.testNo = $(this.jqGridMain).find("#testNo").get(0);
		this.data1 = $(this.jqGridMain).find("#data1").get(0);
		this.data2 = $(this.jqGridMain).find("#data2").get(0);
		this.data3 = $(this.jqGridMain).find("#data3").get(0);
		this.vendorname= $(this.jqGridMain).find("#vendorname").get(0);

		this.groupCat.onchange = this.groupingChanged.bindAsEventListener(this);
		this.freezeCheckBox.onchange = this.freezeChanged.bindAsEventListener(this);
	//	this.afeGroup.onchange = this.afeGroupChanged.bindAsEventListener(this);
		
	//	$(this.groupCat).select2();
		// === CRUD Control ===
		this.btn_saveCostAfeMaster = $(this.jqGridMain).find("#saveCostAfeMaster").get(0);
		this.btn_cancelCostAfeMaster = $(this.jqGridMain).find("#cancelCostAfeMaster").get(0);
		this.btn_getAccountCode = $(this.jqGridMain).find("#btn_getAccountCode").get(0);
		this.btn_addNewRecord = $(this.jqGridMain).find("#btn_addNewRecord").get(0);
		this.btn_deleteRecord = $(this.jqGridMain).find("#btn_deleteRecord").get(0);
		this.btn_deleteSelected = $(this.jqGridMain).find("#btn_deleteSelected").get(0);
		this.btn_import = $(this.jqGridMain).find("#btn_import").get(0);
		this.btn_exportXls = $(this.jqGridMain).find("#btn_exportXls").get(0);
		this.btn_exportRaw = $(this.jqGridMain).find("#btn_exportRaw").get(0);
		this.btn_exportPdf = $(this.jqGridMain).find("#btn_exportPdf").get(0);
		this.btn_duplicatedRecord = $(this.jqGridMain).find("#btn_duplicatedRecord").get(0);
		this.btn_test = $(this.jqGridMain).find("#btn_test").get(0);
		this.btn_reload = $(this.jqGridMain).find("#btn_reload").get(0);
		
		
		this.btn_deteleAfe = $(this.jqGridMain).find("#btn_deteleAfe").get(0);
		this.btn_currency = $(this.jqGridMain).find("#btn_currency").get(0);
		this.addSelectedBtn = $(this.jqGridMain).find("#addSelectedBtn").get(0);
		this.selectAllBtn = $(this.jqGridMain).find("#selectAllBtn").get(0);
		this.unselectAllBtn = $(this.jqGridMain).find("#unselectAllBtn").get(0);
		this.closeDialogBtn = $(this.jqGridMain).find("#closeDialogBtn").get(0);
		this.searchDialogTxt = $(this.jqGridMain).find("#searchDialogTxt").get(0);
		this.clearSearchDialogBtn = $(this.jqGridMain).find("#clearSearchDialogBtn").get(0);
		
//		this.confirmCVSDialogBtn = $(this.jqGridMain).find("#confirmCVSDialogBtn").get(0);
//		this.closeCVSDialogBtn = $(this.jqGridMain).find("#closeCVSDialogBtn").get(0);
//		this.cvsText = $(this.jqGridMain).find("#cvsText").get(0);
		
		this.btn_saveCostAfeMaster.onclick = this.confirmClicked.bindAsEventListener(this);
		this.btn_cancelCostAfeMaster.onclick = this.cancelClicked.bindAsEventListener(this);
		this.btn_getAccountCode.onclick = this.getCostListData.bindAsEventListener(this);
		this.btn_addNewRecord.onclick = this.addNewRecord.bindAsEventListener(this);
		this.btn_deleteRecord.onclick = this.deleteRecords.bindAsEventListener(this);
		this.btn_import.onclick = this.btn_importClicked.bindAsEventListener(this);
		this.btn_exportXls.onclick = this.btn_exportXlsClicked.bindAsEventListener(this);
		this.btn_exportRaw.onclick = this.btn_exportRawClicked.bindAsEventListener(this);
		this.btn_exportPdf.onclick = this.btn_exportPdfClicked.bindAsEventListener(this);
		this.btn_duplicatedRecord.onclick = this.btnDuplicatedClicked.bindAsEventListener(this);
		this.btn_test.onclick = this.btn_testtClicked.bindAsEventListener(this);
		this.btn_reload.onclick = this.btn_reloadClicked.bindAsEventListener(this);
		
		this.btn_deteleAfe.onclick = this.deleteAfeConfirm.bindAsEventListener(this);
		this.btn_currency.onclick = this.btnCurrencyClicked.bindAsEventListener(this);
		this.addSelectedBtn.onclick = this.addSelected.bindAsEventListener(this);
		this.selectAllBtn.onclick = this.selectAllBtnClick.bindAsEventListener(this);
		this.unselectAllBtn.onclick = this.unselectAllBtnClick.bindAsEventListener(this);
		this.closeDialogBtn.onclick = this.closeDialog.bindAsEventListener(this);
		this.clearSearchDialogBtn.onclick = this.clearSearchDialogBtnClicked.bindAsEventListener(this);
		this.searchDialogTxt.onkeyup = this.searchDialogBtnClicked.bindAsEventListener(this);
		this.btn_deleteSelected.onclick = this.deleteSelected.bindAsEventListener(this);
//		this.confirmCVSDialogBtn.onclick = this.confirmImport.bindAsEventListener(this);
//		this.closeCVSDialogBtn.onclick = this.closeCSVDialog.bindAsEventListener(this);
		
		// === OTHERS ===
		this.rootBox = $(this.jqGridMain).find("#rootBox").get(0);
		this.codeTree = $(this.jqGridMain).find("#codeTree" + this.option.counter).get(0);
		this.gridCostDetail = $(this.jqGridMain).find("#grid_costafedetail" + this.option.counter).get(0);
		
		this.hide(this.rootBox);
		this.groupCat.removeAttribute("disabled");
		if (this.option.currentUser=='jwong') {
			$(this.btn_deleteSelected).removeClass("hide"); 
			$(this.btn_import).removeClass("hide"); 
			$(this.btn_exportXls).removeClass("hide"); 
			$(this.btn_exportPdf).removeClass("hide"); 
			$(this.btn_test).removeClass("hide");
			$(this.btn_reload).removeClass("hide");
			$(this.btn_duplicatedRecord).removeClass("hide");
			$(this.btn_currency).removeClass("hide");
		//	$(this.btn_deteleAfe).removeClass("hide");
		}
		
		D3.UI.removeAllChildNodes(this.codeTree);
		$( this.dialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	  //      height: this.option.height.popHeight,
	        width: 800,
	      });
		
//		$( this.cvsDialog ).dialog({
//			dialogClass: 'no-close' ,
//			closeonEscape:false,
//	        autoOpen: false,
//	        closeText: "",
//	        modal: true,
//	       // height: auto,
//	        width: 500,
//		 });
		
		$( this.loadingDialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	        height: 80,
	        width: 500, 
		 });
		
		// Main calling
		console.log("Start Load lookup "  + this.option.counter);
		//this.getLookupList();
		var me = this;
		window.addEventListener("resize", function(){me.onWindowResize();});
	}
	
	CostAfeMaster.prototype.onTabActivated = function(){
		if(this.resizeDirtyFlag){
			delete this.resizeDirtyFlag;
			this.onWindowResize();
		}
	}
	
	CostAfeMaster.prototype.onWindowResize = function(){
		if(this.afeGrid){
			if(this.jqGridMain && this.jqGridMain.offsetWidth){
				if(this.lastWindowResize && this.lastWindowResize.h == window.innerHeight && this.lastWindowResize.w == window.innerWidth) return;
				this.lastWindowResize = {w: window.innerWidth, h: window.innerHeight};
				this.afeGrid.jqGrid("setGridWidth", this.jqGridMain.offsetWidth);
				this.autoResizeGrid();
			}else{
				this.resizeDirtyFlag = true;
			}
		}
	}
	
	CostAfeMaster.prototype.btnCurrencyClicked = function(){
		this.option.currencyClick();
	}
	
	CostAfeMaster.prototype.autoResizeGrid = function(){
		var n = this.jqGridMain.parentNode; 
		while(n){
			if(n.id == "tabs") break;
			n = n.parentNode;
		}
		if(n){
			var m = this.afeGrid.get(0).parentNode;
			while(m){
				if(m.className && m.className.indexOf("ui-jqgrid-bdiv") != -1) break;
				m = m.parentNode;
			}
			if(m){
				if(n.scrollHeight > n.clientHeight){
					this.afeGrid.jqGrid("setGridHeight", m.offsetHeight - (n.scrollHeight - n.clientHeight));
				}else{
					var extra = n.clientHeight - this.jqGridMain.parentNode.offsetHeight - this.jqGridMain.parentNode.offsetTop;
					this.afeGrid.jqGrid("setGridHeight", m.offsetHeight + extra);
				}
			}
		}
	}
	
	CostAfeMaster.prototype.btn_importClicked = function(){
		var xlsFile = this.xlsImport;
		if (this.afeType=="PRICELIST") xlsFile = this.priceixlsImport;
		if (this.afeType=="SO") xlsFile = this.orderixlsImport;
		var csvtemplate = "<div><div id='cvsDialog' title='Import' style='display: block; width: auto; min-height: 104px; max-height: none; height: auto;' class='ui-dialog-content ui-widget-content'>" +
			"<div>Paste your data here:</div>" +
			"<div><textarea id='cvsText' style='width: 100%; resize: none;' rows='10'></textarea></div>" +
			"<div><a href='public/templates/" + xlsFile + "'>You may download the template file here</a></div>" +

			"<div style='text-align: right;'>" +
			"<button id='confirmCVSDialogBtn' class='DefaultButton' style='width: 60px;'>Ok</button>" +
			"<button id='closeCVSDialogBtn' class='DefaultButton' style='width: 60px; margin-left: 6px;'>Cancel</button>" +
			"</div>" +

			"</div></div>";
		var csvmain = $(csvtemplate).get(0);
		this.cvsDialog = $(csvmain).find("#cvsDialog").get(0);
		this.confirmCVSDialogBtn = $(csvmain).find("#confirmCVSDialogBtn").get(0);
		this.closeCVSDialogBtn = $(csvmain).find("#closeCVSDialogBtn").get(0);
		this.cvsText = $(csvmain).find("#cvsText").get(0);
		
		this.confirmCVSDialogBtn.onclick = this.confirmImport.bindAsEventListener(this);
		this.closeCVSDialogBtn.onclick = this.closeCSVDialog.bindAsEventListener(this);
		
		$( this.cvsDialog ).dialog({
			dialogClass: 'no-close' ,
			closeonEscape:false,
	        autoOpen: false,
	        closeText: "",
	        modal: true,
	       // height: auto,
	        width: 500,
		 });
		$( this.cvsDialog ).dialog("open");
	}
	
	CostAfeMaster.prototype.enableEditFields = function(value){
//		var fedit = $(this.afeNoSpan).find(".fieldEditor").get(0);
//		var frender = $(this.afeNoSpan).find(".fieldRenderer").get(0);
//			
//		if (value==true){
//			fedit.style.display = "";
//			frender.style.display = "none";
//		}else{
//			fedit.style.display = "none";
//			frender.style.display = "";
//		}
//		
//		var fedit1 = $(this.afeCommentSpan).find(".fieldEditor").get(0);
//		var frender1 = $(this.afeCommentSpan).find(".fieldRenderer").get(0);
//			
//		if (value==true){
//			fedit1.style.display = "";
//			frender1.style.display = "none";
//		}else{
//			fedit1.style.display = "none";
//			frender1.style.display = "";
//		}
	}
	
	CostAfeMaster.prototype.setAfeNo = function(value){
		if (value!=null){
			var fedit = $(this.afeNoSpan).find(".fieldEditor").get(0);
			var frender = $(this.afeNoSpan).find(".fieldRenderer").get(0);
			fedit.value= value;
			frender.textContent= value;
		}
	}
	
	CostAfeMaster.prototype.setAfeComment = function(value){
		if (value!=null){
			var fedit = $(this.afeCommentSpan).find(".fieldEditor").get(0);
			var frender = $(this.afeCommentSpan).find(".fieldRenderer").get(0);
			fedit.value= value;
			frender.textContent= value;
		}
	}
	
	CostAfeMaster.prototype.btn_testtClicked = function(){
		$.LoadingOverlay("show");
	}
	
	CostAfeMaster.prototype.btn_reloadClicked = function(){
		
		this.createGrid();
	}
	
	CostAfeMaster.prototype.btn_exportRawClickedxxx = function(){
	    var csv = 'Name,Title\n';
//	    data.forEach(function(row) {
//	            csv += row.join(',');
//	            csv += "\n";
//	    });
	 
	    console.log(csv);
	    var hiddenElement = document.createElement('a');
	    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
	    hiddenElement.target = '_blank';
	    hiddenElement.download = 'people.csv';
	    hiddenElement.click();
	}
	
	CostAfeMaster.prototype.btn_exportXlsClicked = function(){ 
		this.exportToExcel("AFE_" + this.afeMaster.afeNumber);
	}
	
	CostAfeMaster.prototype.btn_exportRawClicked = function(){ 
		var htmls = "<div><table id='tests'>" +
				"<tr>" +
					"<td style='font-weight: bold;width:100px'>Seq</td>" +
	    			"<td style='font-weight: bold;width:100px'>IsTangible</td>" +
	    			"<td style='font-weight: bold;width:100px'>CostCategory</td>" +
	    			"<td style='font-weight: bold;width:140px'>CostCategoryCode</td>" +
	    			"<td style='font-weight: bold;width:140px'>ParentAccountCode</td>" +
	    			"<td style='font-weight: bold;width:100px'>AccountCode</td>" +
	    			"<td style='font-weight: bold;width:140px'>AccountCodeDesc</td>" +
	    			"<td style='font-weight: bold;width:100px'>Vendor</td>" +
	    			"<td style='font-weight: bold;width:140px'>ItemDescription</td>" +
	    			"<td style='font-weight: bold;width:100px'>AfeGroupName</td>" +
	    			"<td style='font-weight: bold;width:100px'>AfeGroupCode</td>" +
	    			"<td style='font-weight: bold;width:100px'>Qty</td>" +
	    			"<td style='font-weight: bold;width:100px'>ItemUnit</td>" +
	    			"<td style='font-weight: bold;width:100px'>QtyPerDay</td>" +
	    			"<td style='font-weight: bold;width:100px'>Currency</td>" +
	    			"<td style='font-weight: bold;width:100px'>ItemCost</td>" +
	    			"<td style='font-weight: bold;width:100px'>Total</td>" +
	    			"<td style='font-weight: bold;width:100px'>TotalInPriCur</td>" +
				"</tr> ";
		
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var datas = [];
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			var res = data.accountCode.split(" :: ");
			
			htmls = htmls + "<tr>" +
				"<td style='width:100px'>" + data.sequence + "</td>";
				
				if (data.isTangible=="Yes") htmls = htmls + "<td style='width:100px'>Y</td>";
				else htmls = htmls + "<td style='width:100px'>N</td>";
				
				var replaced = this.getHtmlEncode(data.shortDescription) ;
				replaced = replaced.replace("/", "&#47;");
						
				htmls = htmls + 
				"<td style='width:100px'>" + data.category + "</td>" +
				"<td style='width:140px'>" + data.category.substring(0, 1) + "</td>" +
				"<td style='width:140px'>" + data.parentAccountCode + "</td>" +
				"<td style='width:100px'>" + res[1] + "</td>" +
				"<td style='width:140px'>" + replaced +"</td>" +
				"<td style='width:100px'>" + data.companyName + "</td>" +
				"<td style='width:140px'>" + data.itemDescription + "</td>" +
				"<td style='width:100px'>" + data.afeGroupName + "</td>";
				if (this.afeGroupSCList[data.afeGroupName]!=null) htmls = htmls +  "<td style='width:100px'>" + this.afeGroupSCList[data.afeGroupName] + "</td>";
				else htmls = htmls +  "<td style='width:100px'>  </td>";
				
				htmls = htmls + 
				"<td style='width:100px'>" + Number(data.quantity).toFixed(2) +"</td>" +
				"<td style='width:100px'>" + data.itemUnit + "</td>" +
				"<td style='width:100px'>" + Number(data.estimatedDays).toFixed(2) + "</td>" +
				"<td style='width:100px'>" + data.currency + "</td>" +
				"<td style='width:100px'>" + Number(data.itemCost).toFixed(2) + "</td>" +
				"<td style='width:100px'>" + Number(data.itemTotal).toFixed(2) + "</td>" +
				"<td style='width:100px'>" + Number(data.itemTotalInPrimaryCurrency).toFixed(2) + "</td>" +
			"</tr> ";
		}
		
		htmls = htmls + "</table></div>";
		
		var container = $(htmls).get(0);
		var con = $(container).find("#tests").get(0);
		
		var fmtStr = '@'; 
		var fmt = '0.00';

        var wb = XLSX.utils.table_to_book(con, {raw : true} ); 
		var range = XLSX.utils.decode_range(wb.Sheets['Sheet1']['!ref']);
		for (var r = range.s.r; r <= range.e.r; r++) {
			  for (var c = range.s.c; c <= range.e.c; c++) {
				  var cellName = XLSX.utils.encode_cell({ c: c, r: r });
			  }
			  var cellName = XLSX.utils.encode_cell({ c: 5, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmtStr;
			  
			  cellName = XLSX.utils.encode_cell({ c: 6, r: r });

			  wb.Sheets['Sheet1'][cellName].t = "s";
			  wb.Sheets['Sheet1'][cellName].z = fmtStr;
			  XLSX.utils.format_cell( wb.Sheets['Sheet1'][cellName]);
			  
			  cellName = XLSX.utils.encode_cell({ c: 11, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 13, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 15, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 16, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
			  
			  cellName = XLSX.utils.encode_cell({ c: 17, r: r });
			  wb.Sheets['Sheet1'][cellName].z = fmt;
		}
		XLSX.writeFile(wb, "AFE_(" + this.afeMaster.afeNumber + ")_RawCost.xls");
	}
	
	CostAfeMaster.prototype.btn_exportRawxxClicked = function(){ 
		var csv = [];
		var header = ["Seq", "IsTangible", "CostCategory", "CostCategoryCode", "ParentAccountCode", "AccountCode", "AccountCodeDesc", "Vendor", "ItemDescription"];
		header.push("AfeGroupName");
		header.push("AfeGroupCode");
		header.push("Qty");header.push("ItemUnit");header.push("QtyPerDay");
		header.push("Currency");header.push("ItemCost");header.push("Total");
		header.push("TotalInPriCur");
		csv.push(header.join(","));
		
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var datas = [];
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			var res = data.accountCode.split(" :: ");
			
			datas.push(data.sequence); // "Seq"
			if (data.isTangible=="Yes") datas.push("Y");
			else datas.push("N");
		//	datas.push(data.isTangible);   // "IsTangible"
			datas.push(data.category.replace(/,/g, '.')); // "CostCategory"
			datas.push(data.category.substring(0, 1)); // "CostCategoryCode"
			datas.push(data.parentAccountCode); // "ParentAccountCode"
			datas.push(res[1]); // "AccountCode"
			datas.push(data.shortDescription.replace(/,/g, '.') ); // "AccountCodeDesc"
			datas.push(data.companyName); //"Vendor"
			datas.push(data.itemDescription.replace(/,/g, '.') );  // "ItemDescription"
			datas.push(data.afeGroupName);  // "AfeGroupName"
			datas.push(this.afeGroupSCList[data.afeGroupName]);  // "AfeGroupCode"
			datas.push(Number(data.quantity).toFixed(2));  // "Qty"
			datas.push(data.itemUnit);  // "ItemUnit"
			datas.push(Number(data.estimatedDays).toFixed(2));  // "QtyPerDay"
			datas.push(data.currency);  // "Currency"
			datas.push(Number(data.itemCost).toFixed(2));  // "ItemCost"
			datas.push(Number(data.itemTotal).toFixed(2));  // "Total"
			datas.push(Number(data.itemTotalInPrimaryCurrency).toFixed(2));  // "TotalInPriCur"  
			
			csv.push(datas.join(","));
		}
		this.downloadCSV(csv.join("\n"),  "AFE_(" + this.afeMaster.afeNumber + ")_RawCost.csv"); 
	}
	
	CostAfeMaster.prototype.btn_exportPdfClicked = function(){ 
		this.exportToPdf("costAfeMaster", "Cost Afe Master");
	}
	
	CostAfeMaster.prototype.btnDuplicatedClicked = function(){

		var selRowIds = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		if (selRowIds.length==0) alert("Please select an item to be Duplicated");
		else{
			var copiedData = [];
			for(var i = 0; i < selRowIds.length; i++){	
			    rowData = this.afeGrid.jqGrid("getLocalRow", selRowIds[i]);
			    copiedData.push(rowData);  
			}
			
			for(var i = 0; i < copiedData.length; i++){	
				var rows = copiedData[i];
				
				var rec = new CostAfeDetail();
				for (var key in rec){
					 if (rec.hasOwnProperty(key)) {
						 if (rows.hasOwnProperty(key)) {
							 rec[key] = rows[key];
						 }
					 }
				}
				
				rec.action = CostAfeMaster.SAV;
				rec.isnew = true;
				var d = new Date();
				var n = d.getTime();
				var x = Math.floor((Math.random() * 100) + 1);
				rec.costAfeDetailUid = "new" + n + x;
				var parameters =
					{
					    rowID : "new" + n + x,
					    initdata : rec,
					    position :"last",
					};
				this.afeGrid.jqGrid('addRow',parameters);
			}

			this.show(this.rootBox);
			this.groupCat.setAttribute("disabled", true);
			this.disableItems(true);
			this.autoResizeGrid();
		}
	}
	
	CostAfeMaster.prototype.onProgress = function(){
		$( this.loadingDialog ).dialog("open");
		$( this.progressbar ).progressbar({
	        value: false,
	        change: function() {
	        	var progressLabel = $(this).find(".progress-label").get(0)
	        	progressLabel.textContent = $(this).progressbar("value") + "%";
	          },
	          complete: function() {
	        	  var progressLabel = $(this).find(".progress-label").get(0)
		          progressLabel.textContent =  "Complete!";
	            }
	      }, this);
	}
	CostAfeMaster.prototype.setProgress = function(value){
		$( this.progressbar ).progressbar( "value", value );
		//	this.progressLabel.textContent = "xxx";
	}
	
	CostAfeMaster.prototype.btn_testRecordClick = function(){
		alert("X");
		$(".ui-search-toolbar").hide();
	//	this.afeGrid.jqGrid('navGrid','#jqGridPager' + this.option.counter,{del:false,add:false,edit:false},{},{},{},{multipleSearch:false});
	//	this.afeGrid.jqGrid('navGrid','#jqGridPager' + this.option.counter,{},{},{},{},{});
	//	this.afeGrid.jqGrid('filterToolbar');
	//	this.afeGrid.trigger('reloadGrid');
	}
	
	CostAfeMaster.prototype.getLoadNewAFE = function(afe){
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		this.autoResizeGrid();
		this.getLoadAFE(afe); 
	}
	
	CostAfeMaster.prototype.getLoadAFE = function(afe, afeType){
		this.selectedAfe = afe;
		this.afeType = afeType;
	//	if (!afe.includes("afe")){
		if (this.afeGrid==null) this.getLookupList();
	//	}
	}
	
	CostAfeMaster.prototype.searchDialogBtnClicked = function(){
	//	$(this.codeTree).jstree('search', this.searchDialogTxt.value);
		$(this.codeTree).jstree(true).search(this.searchDialogTxt.value);
	}
	
	CostAfeMaster.prototype.clearSearchDialogBtnClicked = function(){
		this.searchDialogTxt.value = "";
		$(this.codeTree).jstree("clear_search");
	}
	
	CostAfeMaster.prototype.getCostListData = function(){
	//	$(this.codeTree).jstree("open_all");
		$(this.dialog).dialog( "open" );
	}
	
	CostAfeMaster.prototype.getLayout = function(){
		var layoutTemplate = "" +
				"<div id='jqGrid_main'>" +
					"<div id='filter' class='ui-layout-north'>" +
					"<div style=''>" +
					"<div id='rootBox' class='RootButtonContainer' style='text-align: center;'>" +
						"<button class='RootButtonConfirm' type='button' class='btn' id='saveCostAfeMaster'>Confirm</button>" +
						"<button class='RootButtonCancel' type='button' class='btn' id='cancelCostAfeMaster'>Cancel</button>" +
					"</div>" +
					"</div>" +
					"<div style='display: flex;'>" +
//						"<div style='width: 30%;'>" +
//							"AFE :	<select name='afeGroup' id='afeGroup'>" +
//				
//									"</select>" +
//						"</div>" +
						"<div style='width: 220px;padding-top: 6px;'>" + 
							"<select name='groupCat' id='groupCat' style='height: 24px;' class='fieldEditor comboBox'>" +
										  "<option value='Ungroup'>Ungroup</option>" +
										  "<option value='Category'>Category</option>" +
										  "<option value='AccCode'>Account Code</option>" +
										  "<option value='AccGroup'>AFE Grouping</option>" +
										  "<option value='PlanPhase'>Plan Phase</option>" +
										  "<option value='Vendor'>Vendor</option>" +  
										"</select>" +
						"</div>" +
						"<div class='hide' id='freezeContainer' style='width: 50%;'>" +
							"<input type='checkbox' id='freezeCheckBox' value='1'>Freeze Account Code" +
						"</div>" +
	//					"<div id='summaryBox' class='container' style='width: 300px;background-color: white;margin-right: 10px;'>" +
	//						"<div class='boxes' style='color: blue;font-weight: 900;padding-top: 10px;width: 150px;' id='data1'>AFE Total: 0.00</div>" + 
	//					"</div>" +
	//				"</div >" +
				"<div>" +
					"<table width='100%'>" +
						"<tr>" +
							"<td width='30px'><span id='testNo' class='HtmlTableLabel' class='fieldContainer' style='display: inline-block;color: black;font-weight: 700;padding-top: 12px; margin: auto;font-size: 12px !important;' >AFE No.:</span><td>" +
							"<td width='250px'><span id='afeNoSpan" + this.option.counter +  "' class='fieldContainer' style='display: inline-block;color: black;font-weight: 700;padding-top: 10px; margin: auto;font-size: 12px !important;' >" +
							"<div class='fieldRenderer' > </div>" +
							"<input id='afeNo' type='text' class='fieldEditor textInput required' maxlength='40' style='display: none;'>" +
							"</span></td>" +
							"<td width='30px'><span class='HtmlTableLabel' class='fieldContainer' style='display: inline-block;color: black;font-weight: 700;padding-top: 12px; margin: auto;font-size: 12px !important;' >Comment:</span></td>" +
							"<td width='400px'><span id='afeCommentSpan" + this.option.counter +  "' class='fieldContainer' style='display: inline-block;color: black;font-weight: 700;padding-top: 10px; margin: auto;font-size: 12px !important;' >" +
							"<div class='fieldRenderer' > </div>" +
							" <input id='afeComment' type='text' class='fieldEditor textInput'  style='display: none;'>" +
							"</span></td>" +
							"<td width='300px' id='vendorname' class='hide'  style='display: inline-block;color: black;font-weight: 700;padding-top: 12px; margin: auto;font-size: 12px !important;' > Vendor: </td>" +

							"<td width='300px' id='wi1' class='hide'>  Working Interest <input id='afeinterest' type='number' class='fieldEditor textInput' style='width:100px'> </td>" +
							"<td width='300px'><button class='DefaultButton hide' style='margin: 3px; width: 250px;height: 25px' id='btn_deteleAfe'>Delete this AFE</button></td>" +
							"<td width='100px'><button class='DefaultButton hide' style='margin: 3px; width: 100px;height: 25px' id='btn_currency'>Currency</button></td>" +
							"<td style='text-align:right;'>" +
								"<div style='display: flex;'>" +
									"<div class='boxes hide' style='color: black;font-weight: 700;padding-top: 10px;width: 200px; margin: auto;font-size: 12px !important;' id='data1'>AFE Total: 0.00</div>" + 
									"<div class='boxes hide' style='color: black;font-weight: 700;padding-top: 10px;width: 200px; margin: auto;font-size: 12px !important;' id='data2'>AFE Used: 0.00</div>" + 
									"<div class='boxes hide' style='color: black;font-weight: 700;padding-top: 10px;width: 200px; margin: auto;font-size: 12px !important;' id='data3'>AFE Balance: 0.00</div>" + 
								"</div>"+							
							"</td>" +
						"</tr>" +
					"</table>" +
					"</div >" +
					"</div >" +
				"</div>" +
				"<div class='ui-layout-center' style='overflow: hidden;'> " +
					"<table id='grid_costafedetail" + this.option.counter +  "'></table>" +
			    	"<div id='jqGridPager" + this.option.counter +  "'></div>" +
				"</div>" +
				"<div id='filter' class='ui-layout-south'>" +
					"<table style='width:100%; border:0px; border-collapse:collapse'><tr><td style='vertical-align:top'>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_getAccountCode'>Get Account Code</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_addNewRecord'>Add</button>" +
					//	"<button class='DefaultButton' style='margin: 3px;' id='btn_deleteRecord'>Delete</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_deleteSelected'>Delete Selected</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_import'>Import</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_exportXls'>Export Data</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_exportRaw'>Export Raw Data</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_exportPdf'>Export to PDF</button>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_duplicatedRecord'>Duplicate Selected</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_reload'>Reload</button>" +
						"<button class='DefaultButton hide' style='margin: 3px;' id='btn_test'>test</button>" +
					"</td><td style='text-align:right'>" +
						"<button class='DefaultButton' style='margin: 3px;' id='btn_deleteRecord'>Delete</button>" +
						"<div>" + this.version + "</div>" +
					"</td></tr></table>" +	
				"</div>" +
				
				"<div id='dialog' title='Account Code' style='width: 470px;'>" +
					"<div style='height: 30px;text-align: center;'>" +
						"<button id='addSelectedBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Add Selected</button>" +
						"<button id='selectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Select All</button>" +
						"<button id='unselectAllBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Unselect All</button>" +
						"<button id='closeDialogBtn' style='width: 100px;height: 25px;' class='DefaultButton'>Close</button>" +
					"</div>" +
					"<div style='height: 30px;display: flex; position: relative;'>" +
						"<input id='searchDialogTxt' type='text' class='fieldEditor textInput'>" +
						"<button id='clearSearchDialogBtn' style='width: 30%; height:22px' class='DefaultButton'>Clear</button>" +
					"</div>" +
					"<div style='display: block; overflow: auto; height: " + this.option.popHeight + "px;''>" +
						"<div id='codeTree" + this.option.counter +  "'></div>" +
					"</div>" +
			//		"<div style='height: 30px;'>" +
			//		"<button id='closeDialogBtn' style='width: 100%;' class='DefaultButton'>Close</button>" +
				"</div>" +
				
				
//				"<div id='cvsDialog' title='Import' style='display: block; width: auto; min-height: 104px; max-height: none; height: auto;' class='ui-dialog-content ui-widget-content'>" +
//
//				"<div>Paste your data here:</div>" +
//				"<div><textarea id='cvsText' style='width: 100%; resize: none;' rows='10'></textarea></div>" +
//				"<div><a href='public/templates/" + this.xlsImport + "'>You may download the template file here</a></div>" +
//
//				"<div style='text-align: right;'>" +
//				"<button id='confirmCVSDialogBtn' class='DefaultButton' style='width: 60px;'>Ok</button>" +
//				"<button id='closeCVSDialogBtn' class='DefaultButton' style='width: 60px; margin-left: 6px;'>Cancel</button>" +
//				"</div>" +
//
//				"</div>" +
				
				"<div id='loadingDialog' title='Loading'>" +
				"<div id='progressbar" + this.option.counter + "'><div class='progress-label'>Loading...</div></div>" +
				"</div>" +
				
				
				
				"</div>" +
			"</div>";
		return layoutTemplate;
	}

	CostAfeMaster.prototype.afeGroupChanged = function(){

		this.selectedAfe = camid;

		this.loadAfeData();
	}
	
	CostAfeMaster.prototype.confirmImport = function(){ 		
		this.getProgressStatus(true, false, "Importing ..");
		var sourceText = this.cvsText.value ;
		this.processTemplateImport(sourceText);
		this.cvsText.value = "";
		$( this.cvsDialog ).dialog("close");
		this.getProgressStatus(false, false, "Importing ..");
		
	}
	
	CostAfeMaster.prototype.processTemplateImport = function(sourceText){
		var binding_properties = {};
		var binding_values = [];
		var importedData = [];
		var splitByLine = sourceText.split("\n");
		if (splitByLine.length >= 2){
			for(var i = 0; i < splitByLine.length; i++){
				if (i==0){
					binding_properties = splitByLine[i].split("\t");
				}else{
					if (splitByLine[i]!=""){
						var b_value = splitByLine[i].split("\t");
						binding_values.push(b_value);
					}
				}
			}
		}
		if (!jQuery.isEmptyObject(binding_properties)){
			for(var i = 0; i < binding_values.length; i++){
				var b_values = binding_values[i];
				if (b_values.length==binding_properties.length){
					var rec = new CostAfeDetail();
					rec.action = CostAfeMaster.SAV;
					rec.isnew = true;
					for(var j = 0; j < binding_properties.length; j++){
						var prop = binding_properties[j];
						if(rec.hasOwnProperty(prop)){
							rec[prop] = b_values[j];
						}
					}
					var cac = this.checkAccountCode(rec.parentAccountCode, rec.accountCode);
					if (cac!=""){  
						rec.parentAccountCode = cac.parentAccountCode;
						rec.accountCode = cac.accountName;
						rec.shortDescription = cac.shortDescription;
						rec.category = cac.category;
						rec.subcategory = cac.subcategory;
						rec.isTangible = cac.isTangible;
					}
					if (rec.quantity=="") {
						if (this.afeMaster.afeType=="PRICELIST") rec.quantity = 0.0; 
						else rec.quantity = 1.0;
					}
					rec.quantity = this.unformatString(rec.quantity);
					rec.itemCost = this.unformatString(rec.itemCost);
					rec.companyName=rec.lookupCompanyUid;  
					if (this.currencyRate[rec.currency]!=undefined){
						rec.currencyRate=this.currencyRate[rec.currency];
					}
					var pref = this.planRefMaps2.find(x => x.displayName === rec.planReferenceName);
					if (pref!=undefined){
						rec.planReference = pref.id;
					}
					
					importedData.push(rec);
				}else{
					console.log("check data ", i , " NG");
				}
			}
			if (importedData.length>0){
				for(var i = 0; i < importedData.length; i++){
					var impData = importedData[i];
					impData.runBeforeSave();
					impData.accountCode = (impData.parentAccountCode==""?"-":impData.parentAccountCode) + " :: " + impData.accountCode;
					if (impData.isTangible== false) impData.isTangible= "No";
					if (impData.isTangible== true) impData.isTangible= "Yes";
					var d = new Date();
					var n = d.getTime();
					var parameters =
					{
					    rowID : "new" + n,
					    initdata : impData,
					    position :"last",
					};
					this.afeGrid.jqGrid('addRow',parameters);
				}
				this.show(this.rootBox);
				this.groupCat.setAttribute("disabled", true);
				this.disableItems(true);
				this.autoResizeGrid();
			}
		}
	}
	
	CostAfeMaster.prototype.checkAccountCode = function(pcode, code){
	//	for(var i = 0; i < this.accountCodeMaps.length; i++){ 
		for (var prop in this.accountCodeMaps) {
			var ac = this.accountCodeMaps[prop];
			if ((ac.parentAccountCode==pcode) && (ac.accountCode==code)) return ac;
		}

		
		return "";
	}
	
	CostAfeMaster.prototype.addSelected = function(){
		if ($(this.codeTree).jstree("get_selected").length==0){
			alert("Please select one");
		}else{
			var selectedList = $(this.codeTree).jstree("get_selected");
			var selectedCode = [];
			var count = 0;
			for(var i = 0; i < selectedList.length; i++){
				var code = selectedList[i];
				if (code.includes("c:")){
					
				}else{
					count = count +1;
					selectedCode.push(code);
					//this.addNewRecordLast(code);
				}
			}
			
			for(var i = 0; i < selectedCode.length; i++){
				var xx = selectedCode[i];
				this.addNewRecordLast(xx);
			}
			
			$(this.codeTree).jstree().deselect_all(true);
			$(this.dialog).dialog( "close" );
		}
		
	}
	
	CostAfeMaster.prototype.selectAllBtnClick = function(){ 
		$(this.codeTree).jstree("check_all");
	}
	CostAfeMaster.prototype.unselectAllBtnClick = function(){
		$(this.codeTree).jstree().deselect_all(true);
	}
	
	CostAfeMaster.prototype.addNewRecordLast = function(codeId){
		var rec =  new CostAfeDetail();
		rec.quantity = 1.0;
		rec.action = CostAfeMaster.SAV;
		rec.isnew = true;
		rec.currency = this.primaryCurrency;
		
		var cac = this.accountCodeMaps[codeId];
		rec.parentAccountCode = cac.parentAccountCode;
		rec.accountCode = cac.accountName;
		rec.shortDescription = cac.shortDescription;
		rec.category = cac.category;
		rec.subcategory = cac.subcategory;
		rec.isTangible = cac.isTangible;
		
		rec.currency = this.primaryCurrency;
		
		var d = new Date();
		var n = d.getTime();
		
		var parameters =
		{
		    rowID : "new" + n,
		    initdata : rec,
		    position :"last",
		};
		this.afeGrid.jqGrid('addRow',parameters);
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		this.autoResizeGrid();
	}
	
	CostAfeMaster.prototype.validateCheck = function(){
		if (this.afeNoBox.value=="") {
			alert("AFE No. must be entered.")
			return false;
		}
			
		return true;
	}
	
	CostAfeMaster.prototype.confirmClicked = function(){
//		if (this.currentAction=="delete"){
//			this.deleteCostAfeDetails();
		
		if (this.currentAction=="deleteAfe"){
			this.deleteCostAfeMaster();
		}else{
			if (this.validationData()){
				var rowIds = this.afeGrid.jqGrid("getDataIDs");
				var delrec = 0;
				var validateCheck = true;
				for(var i = 0; i < rowIds.length; i++){
					var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
					this.afeGrid.jqGrid("saveRow", rowIds[i], false);
					if (rec.action==CostAfeMaster.DEL) delrec ++;		
				}
				
				if (delrec==0) {
					this.saveCostAfeMaster();
				}else{
					var wantDelete  = confirm("Are you sure you want to delete the selected record?");
					if (wantDelete) this.saveCostAfeMaster();
				}
				
				
			}
		}
	}
	
	CostAfeMaster.prototype.validationData = function(){
		var check = true;
		var msg = "";
		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		var temp = "";	
		var accCode = []
		var dup = false;
		var save = false;
		for(var i = 0; i < rowIds.length; i++){
			var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			if(this.accountCodeMaps[$("#" +rowIds[i] + "_accountCode").val()]!=undefined){
				temp = this.accountCodeMaps[$("#" +rowIds[i] + "_accountCode").val()].accountName;
			}else{
				temp = rec.accountCode;
			}
			if(accCode.includes(temp)){
				dup = true;
			}else{
				accCode.push(temp);
			}
			temp = "";
			if (rec.action==CostAfeMaster.SAV){
				save= true;
				if ($("#" +rowIds[i] + "_accountCode").val()=="") {
					check = false;
					msg = "Account Code Field is Required";
					continue;
				}else if ($("#" +rowIds[i] + "_currency").val()=="") {
					check = false;
					msg = "Currency Field is Required";
					continue;
				}
			}
		}
		if (this.afeType=="SO" && dup && save ){
			check = false;
			msg = "There is duplicate account code";
		}
		
		if (check==false) alert(msg);
		return check;
	}
	
	CostAfeMaster.prototype.deleteAfeConfirm = function(){
		var canDelete = true;
		if (this.option.parentAfe==this.selectedAfe){
			if (this.hasSup) canDelete= false;
		}
		if (canDelete){
			var wantDelete  = confirm("Are you sure you want to delete this AFE?");
			if (wantDelete){
				this.currentAction = "deleteAfe";
				this.show(this.rootBox);
				this.groupCat.setAttribute("disabled", true);
				this.autoResizeGrid();
			}
		}else alert("Please delete Supplemantary AFE before delete the Master AFE.")
	}
	
	CostAfeMaster.prototype.deleteAfe = function(){
		
	}
	
	CostAfeMaster.prototype.deleteCostAfeMaster = function(){
		var params = {};
		params.costAfeMasterUid = this.selectedAfe;
		
		var url = this.baseUrl + "delete_afe_master_record";
		$.post(url, params, function(response) {
			location.reload(); 
		});
	}
	
	CostAfeMaster.prototype.deleteCostAfeDetails = function(){
		for(var i = 0; i < this.deletedRecords.length; i++){
			var id = this.deletedRecords[i];
			var params = {};
			params.costAfeDetailUid = id;
			
			var self = this;
			var url = this.baseUrl + "delete_afe_detail_record";
			this.afeGrid.jqGrid('delRowData', id);
			$.post(url, params, function(response) {
				
			});
		}
		this.currentAction = "";
		this.hide(this.rootBox);
		this.groupCat.removeAttribute("disabled");
		this.autoResizeGrid();
	}
	
	CostAfeMaster.prototype.saveCostAfeMaster = function(){  
		this.getProgressStatus(true, 0, "Saving...");
		var dataxml = this.getCostAfeDetailXML();
	
		// Trying to Save through Webservice - 
		var params = {};
		params.xmldata = dataxml;
		
		var self = this;
		var url = this.baseUrl + "update_costafe_record";

		$.post(url, params, function(response) {
			self.hide(self.rootBox);
			self.groupCat.removeAttribute("disabled");
			self.disableItems(false);
			self.autoResizeGrid();
			self.afeDoneSave(response);
			self.getProgressStatus(false, 0, "Saving...");
		}, "xml");

	}
	
	CostAfeMaster.prototype.afeDoneSave = function(response) {
		if (this.option.isNew==true) location.reload(); 
		this.loadAfeData();
	}
	
	CostAfeMaster.prototype.getCostAfeDetailXML = function(){
		var xml = "<root>";

		xml += "<CostAfeMaster uid='"+this.selectedAfe+"'>";
		xml += "<costAfeMasterUid>"+ this.selectedAfe +"</costAfeMasterUid>";
		if (this.option.parentAfe!=this.selectedAfe){
			xml += "<parentCostAfeMasterUid>"+this.option.parentAfe+"</parentCostAfeMasterUid>"; 
		}
		xml += "<afeNumber>" + this.getHtmlEncode(this.afeNoBox.value) + "</afeNumber>";
		xml += "<longDescription>" + this.getHtmlEncode(this.afeComment.value) + "</longDescription>";
		xml += "<workingInterest>"+ this.afeinterest.value +"</workingInterest>";


		var rowIds = this.afeGrid.jqGrid("getDataIDs");
		for(var i = 0; i < rowIds.length; i++){
			var data = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			if (data.constructor.name!="CostAfeDetail"){
				var cds = new CostAfeDetail();
				var newRec = Object.assign(cds, data);
				data = newRec;
			}
			if (this.currencyRate[data.currency]!=undefined){
				data.currencyRate=this.currencyRate[data.currency];
			}
			data.runBeforeSave();	
			
			if (data.action==CostAfeMaster.DEL){
				xml += "<costAfeDetail uid='"+ data.costAfeDetailUid +"' deleted='true'/> ";
			}else if (data.action==CostAfeMaster.SAV){

				xml += "<costAfeDetail uid='"+ data.costAfeDetailUid +"'>";
				xml += "<costAfeDetailUid>"+ data.costAfeDetailUid +"</costAfeDetailUid>";
				xml += "<costAfeMasterUid>"+ data.costAfeMasterUid +"</costAfeMasterUid>"; 
				xml += "<sequence>"+ data.sequence +"</sequence>";
				xml += "<isTangible>"+ data.isTangible +"</isTangible>";
				xml += "<category>"+ this.getHtmlEncode(data.category) +"</category>";
				xml += "<parentAccountCode>"+ this.getHtmlEncode(data.parentAccountCode) +"</parentAccountCode>";
				xml += "<accountCode>"+ this.getHtmlEncode(data.accountCode) +"</accountCode>";
				xml += "<lookupCompanyUid>"+ this.getHtmlEncode(data.lookupCompanyUid) + "</lookupCompanyUid>";
				xml += "<itemDescription>"+ this.getHtmlEncode(data.itemDescription) +"</itemDescription>";
				xml += "<longDescription>"+ this.getHtmlEncode(data.longDescription) +"</longDescription>";
				xml += "<afeGroupName>"+ this.getHtmlEncode(data.afeGroupName) +"</afeGroupName>";
				xml += "<planReference>"+ this.getHtmlEncode(data.planReference) +"</planReference>";
				xml += "<recurringItem>"+data.recurringItem+"</recurringItem>";
				xml += "<quantity>"+data.quantity+"</quantity>";
				xml += "<itemUnit>"+data.itemUnit+"</itemUnit>";
				xml += "<estimatedDays>"+data.estimatedDays+"</estimatedDays>";
				xml += "<currency>"+data.currency+"</currency>";
				xml += "<itemCost>"+data.itemCost+"</itemCost>";
				xml += "<itemTotal>"+data.itemTotalInPrimaryCurrency+"</itemTotal>";
	
				xml += "</costAfeDetail>";
			}
		}

		xml += "</CostAfeMaster>";
		xml += "</root>";
		
		return xml;
	}
	
	CostAfeMaster.prototype.cancelClicked = function(){ 
		if (this.selectedAfe.indexOf("afe") == 0) {
			location.reload(); 
		//	window.location.reload();
		}
		if (this.needRefresh){
			location.reload(); 
		}
		var rowIds = this.afeGrid.jqGrid("getDataIDs");

		for(var i = 0; i < rowIds.length; i++){
			var rec = this.afeGrid.jqGrid('getLocalRow',rowIds[i]);
			rec.action = "";
			if (rowIds[i].includes("new")){
				this.afeGrid.jqGrid('delRowData', rowIds[i]);
			} else {
				this.afeGrid.jqGrid("restoreRow", rowIds[i]);
			}
			
		}
		this.afeGrid.jqGrid('resetSelection');
		this.currentAction="";
		this.hide(this.rootBox);
		this.groupCat.removeAttribute("disabled");
		this.disableItems(false);
		this.autoResizeGrid();
	}
	
	CostAfeMaster.prototype.onDataFieldChange = function(e){
		var pr = e.target.attributes.getNamedItem('rowid').ownerElement.parentElement.parentElement; 
		$(pr).removeClass("DeleteRecordBackground");
		var rid = e.target.attributes.getNamedItem('rowid').value;
		console.log("onDataFieldChange", e.keyCode + "  " + rid);
		var field = e.currentTarget.getAttribute("name");
		var rec = this.afeGrid.jqGrid('getLocalRow', rid);
		
		if (field=="sequence" && e.type=="change") {
			if (isNaN(e.currentTarget.value)){
				rec.sequence = "0";
				e.currentTarget.value = "0";
				alert("Enter Number only")
			}
		}
		
		// == Lookup Update ==
		if (field=="accountCode") {
			rec.parentAccountCode = this.accountCodeMaps[e.currentTarget.value].parentAccountCode;
			rec.category = this.accountCodeMaps[e.currentTarget.value].category;
			rec.subcategory = this.accountCodeMaps[e.currentTarget.value].subcategory;
			rec.isTangible = this.accountCodeMaps[e.currentTarget.value].isTangible;
		}
		if (field=="planReferenceName") {
			rec.planReference = e.currentTarget.value;  
		//	if (this.planRefMaps[e.currentTarget.value]==undefined){
		//		rec.phaseCode = "";
		//	}else{
		//		rec.phaseCode = this.planRefMaps[e.currentTarget.value].phaseCode;
		//	}	
		}
		if (field=="companyName") {
			rec.lookupCompanyUid = e.currentTarget.value;
		}
		if (field=="currency") {
			rec.currencyRate = this.currencyRate[e.currentTarget.value];
		}
		
		if(rec[field] != e.currentTarget.value){
			rec.action = CostAfeMaster.SAV;
			console.log("updated");
		}
	}
	
	CostAfeMaster.prototype.onInitLookupFieldSelection = function(name){
		return null;
	}
	
	CostAfeMaster.prototype.getLookupList = function(){
		//this.onProgress();
		//this.setProgress(0);
		this.getProgressStatus(true, 0, "Loading..");
		console.log(">> getLookupList Start "+ this.option.counter);
		this.lookupList = [];
		var page = this.baseUrl + "get_all_lookups";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				$(result).find('itemUnit').each(function(){
					var headerText;
					var model = {};
					var code = $(this).get(0).getAttribute("code");
					var label = $(this).get(0).getAttribute("label");
					options[code] = label;
				});
				self.lookupList = options;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> getLookupList END " + self.option.counter);
			self.getCurrencyList();
		});
	}
	
	CostAfeMaster.prototype.getCurrencyList = function(){
		console.log(">> getCurrencyList");
		//this.setProgress(10);
		this.getProgressStatus(true, 10, "Load Currency...");
		this.currencyList = [];
		this.currencyRate = [];
		var page = this.baseUrl + "get_currency_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var option2 = {};
				options[""] = "";
				var list = result.documentElement.getElementsByTagName("CostCurrencyLookup");
				for(var i=0; i<list.length; i++){
					var currencySymbol = list[i].getElementsByTagName("currencySymbol")[0].textContent;
					var conversionRate = list[i].getElementsByTagName("conversionRate")[0].textContent;
					var isP = list[i].getElementsByTagName("isPrimaryCurrency")[0].textContent
					if (options[currencySymbol] == null) {
						options[currencySymbol] = currencySymbol;
						option2[currencySymbol] = conversionRate;
						if (isP=="true") self.primaryCurrency = currencySymbol;
					}
				}
				self.currencyList = options;
				self.currencyRate = option2;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> getCurrencyList Done");
			self.getAccountCodeList();
		});
	}
	
	CostAfeMaster.prototype.getAccountCodeList = function(){
		console.log(">> GetAccountCodeList");
		//this.setProgress(20);
		this.getProgressStatus(true, 20, "Load Account Code");
		this.accountCodeList = [];
		this.accountCodeMaps = [];
		var page = this.baseUrl + "get_account_codes_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var option2 = [];
				var option3 = {};
				var catGroup = [];
				options[""] = "";
				var list = result.documentElement.getElementsByTagName("CostAccountCodes");
				for(var i=0; i<list.length; i++){
					var costAccountCodesUid = list[i].getElementsByTagName("costAccountCodesUid")[0].textContent;
					var parentAccountCode = list[i].getElementsByTagName("parentAccountCode")[0].textContent;
					var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
					var shortDescription = list[i].getElementsByTagName("shortDescription")[0].textContent;
					var category = list[i].getElementsByTagName("category")[0].textContent; 
					var subcategory = list[i].getElementsByTagName("subcategory")[0].textContent; 
					var isTangible = list[i].getElementsByTagName("isTangible")[0].textContent; 

					var cac = new CostAccountCodes();
					cac.costAccountCodesUid = costAccountCodesUid;  
					cac.parentAccountCode = parentAccountCode;
					cac.accountCode = accountCode;
					cac.shortDescription = shortDescription;
					cac.category = category;
					cac.subcategory = subcategory;
					cac.isTangible = (isTangible=="false"?false:true);
					cac.run();
					
					options[costAccountCodesUid] = cac.accountName;
					option3[costAccountCodesUid] = cac;
					option2.push(cac);
					if (catGroup.indexOf(category) == -1) catGroup.push(category);
				}
				self.accountCodeList = options;
				self.accountCodeMaps = option3;
				var treeSection = "<ul>";
				for (var j=0; j < catGroup.sort().length; j++){
					treeSection = treeSection + "<li id='c:" + (catGroup[j]==""?"Uncategorized":catGroup[j]) + "' >" + (catGroup[j]==""?"Uncategorized":catGroup[j]);
					treeSection = treeSection + "<ul>";
					var checkCat = catGroup[j];
					
					for(var i=0; i<list.length; i++){
						var costAccountCodesUid = list[i].getElementsByTagName("costAccountCodesUid")[0].textContent;
						var parentAccountCode = list[i].getElementsByTagName("parentAccountCode")[0].textContent;  
						var accountCode = list[i].getElementsByTagName("accountCode")[0].textContent;
						var shortDescription = list[i].getElementsByTagName("shortDescription")[0].textContent;
						var codeName = (parentAccountCode==""?"-":parentAccountCode) + " :: " + (accountCode==""?"-":accountCode) + " :: " + shortDescription;
						var category = list[i].getElementsByTagName("category")[0].textContent; 
						if (checkCat == category){
							treeSection = treeSection + "<li id='" + costAccountCodesUid + "'>" + codeName + "</li>";
						}
					}
					
					treeSection = treeSection + "</ul>";
					treeSection = treeSection + "</li>";
				}
				treeSection = treeSection + "</ul>";
				self.codeTree.appendChild($(treeSection).get(0));
				
				$(self.codeTree).jstree({
					"core" : {

				        "themes":{
				            "icons":false
				        }
				    },
					"search": {
						'show_only_matches' : true,
				        'show_only_matches_children' : true,
				        'search_leaves_only' : true
				    },
			        "checkbox": {
			            "keep_selected_style": false,
			           // "three_state" : false,
			        },
			            "plugins": ["checkbox","search"]
			    })
			    .bind("loaded.jstree", function (event, data) {
			        $(this).jstree("open_all");
			    });
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetAccountCodeList Done");
			self.getVendorList();
		});
	}
	
	CostAfeMaster.prototype.getVendorList = function(){
		console.log(">> GetVendorList");
		//this.setProgress(40);
		this.getProgressStatus(true, 40, "Load Vendor...");
		this.vendorList = [];
		this.vendorListInactive = [];
		var page = this.baseUrl + "get_vendor_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				options[""] = "";
				$(result).find('LookupCompany').each(function(){
					var headerText;
					var model = {};
					var lookupCompanyUid = $(this).get(0).getAttribute("lookupCompanyUid");
					var companyName = $(this).get(0).getAttribute("companyName");
					options[lookupCompanyUid] =  companyName;
				});
				self.vendorList = options;
				var xx = "";
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetVendorList Done");
			//self.createGrid('grid_costafedetail');
			//self.loadAfeData();
			self.getAFEGroup(); 
		});
	}
	
	CostAfeMaster.prototype.getAFEGroup = function(){ 
		console.log(">> GetAFEGroupList");
		//this.setProgress(40);
		this.getProgressStatus(true, 50, "Load AFE Group...");
		this.afeGroupList = [];
		this.afeGroupSCList = [];
		
		var page = this.baseUrl + "get_afe_group_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				options[""] = "";
				var optionsSC = {};
				var list = result.documentElement.getElementsByTagName("CostAfeGroup");
				for(var i=0; i<list.length; i++){
					var afeGroupName = list[i].getElementsByTagName("afeGroupName")[0].textContent;
					var shortCode = list[i].getElementsByTagName("shortCode")[0].textContent;
					var description = list[i].getElementsByTagName("description")[0].textContent;
					options[afeGroupName] =  afeGroupName;
					optionsSC[afeGroupName] =  shortCode;
				}
				self.afeGroupList = options;
				self.afeGroupSCList = optionsSC;
				var xx = "";
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetAFEGroupList Done");
			self.getPlanReferenceList(); 
		});
	}
	
	CostAfeMaster.prototype.getPlanReferenceList = function(){ 
		console.log(">> GetPlanReference");
		this.planRefList = [];
		this.planRefAList = [];
		this.planRefereList = [];
		this.planRefMaps = [];
		this.planRefMaps2 = [];
		
		var page = this.baseUrl + "get_plan_reference_list";
		var self = this;
		
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var options = {};
				var optionsA = {};
				var optionsMaps = {};
				options[""] = "";
				optionsA[""] = "";
				var list = result.documentElement.getElementsByTagName("OperationPlanPhase");
				for(var i=0; i<list.length; i++){
					var id = list[i].getElementsByTagName("id")[0].textContent;
					var sequence = list[i].getElementsByTagName("sequence")[0].textContent;
					var phaseCode = list[i].getElementsByTagName("phaseCode")[0].textContent;
					var phaseName = list[i].getElementsByTagName("phaseName")[0].textContent;
					var use = list[i].getElementsByTagName("use")[0].textContent;
					
					var opp = new OperationPlanPhase();
					opp.id = id;
					opp.sequence = sequence;
					opp.phaseCode = phaseCode;
					opp.phaseName = phaseName;
					opp.use = use;
					opp.setDisplayName();
					
					optionsMaps[id] = opp;
					self.planRefMaps2.push(opp);
					
					options[id] =  opp.displayName;
					if (use=="1") optionsA[id] =  opp.displayName;
				}

				self.planRefList = options;
				self.planRefAList = optionsA;
				self.planRefereList = options;
				
				self.planRefMaps = optionsMaps;
			}
		}).done(function(data, textStatus, jqXHR) {
			console.log(">> GetPlanReference Done");
			self.retrieveColumnSetting(); 
		});
	}
	
	CostAfeMaster.prototype.retrieveColumnSetting = function (){
		//this.setProgress(60);
		this.getProgressStatus(true, 60, "Load Setting ...");
		this.colModel = [];
		this.colNames = [];
		var page = this.baseUrl + "get_daily_cost_columns";
		var self = this;

		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var istrue = $(result).find('costAfeMaster')[0].getAttribute('showWorkingInterest');
				var xlsFile = $(result).find('costAfeMaster')[0].getAttribute('importXls');
				var pxlsFile = $(result).find('costAfeMaster')[0].getAttribute('priceimportXls');
				var oxlsFile = $(result).find('costAfeMaster')[0].getAttribute('orderimportXls');
				if (xlsFile!=null) self.xlsImport = xlsFile;
				if (pxlsFile!=null) self.priceixlsImport = pxlsFile; 
				if (oxlsFile!=null) self.orderixlsImport = oxlsFile; 
				
				self.showWorkingInterest = (istrue=='true');
				if (istrue!='true') {
					$(self.wi1).addClass("hide");
				}
				$(result).find('costAfeMaster').find('columns').each(function(){
					var headerText;
					var model = {};
					var dataField = $(this).get(0).getAttribute("dataField");
					var headerText = $(this).get(0).getAttribute("headerText");
					var hideForPrice = $(this).get(0).getAttribute("hideForPrice");
					var hideForOrder = $(this).get(0).getAttribute("hideForOrder");
					var hideForAFE = $(this).get(0).getAttribute("hideForAFE");
					if (self.afeType=="PRICELIST"){
						if (hideForPrice!="yes"){
							model = self.checkField(dataField, headerText);
							
							self.colNames.push(dataField);
							self.colModel.push(model);	
						}
					}else if(self.afeType=="SO"){
						if(hideForOrder!="yes"){
							model = self.checkField(dataField, headerText);
							
							self.colNames.push(dataField);
							self.colModel.push(model);	
						}
					}else if(self.afeType=="MASTER" || self.afeType=="SUPPLEMENTARY"){
						if(hideForAFE!="yes"){
							model = self.checkField(dataField, headerText);
							
							self.colNames.push(dataField);
							self.colModel.push(model);	
						}
					}else {
						model = self.checkField(dataField, headerText);
						
						self.colNames.push(dataField);
						self.colModel.push(model);	
					}
					
				});
				
				self.colModel.push({
					name: 'costAfeDetailUid',
					key: true,
					label: 'costAfeDetailUid',
					hidden: true
				});
				self.colNames.push("costAfeMasterUid");
				
				self.colModel.push({
					name: 'lookupCompanyUid',
					label: 'lookupCompanyUid',
					hidden: true,
				});
				self.colNames.push("lookupCompanyUid");
				
				self.colModel.push({
					name: 'parentAccountCode',
					label: 'parentAccountCode',
					hidden: true,
				});				
				self.colNames.push("parentAccountCode");
				
				self.colModel.push({
					name: 'planReference',
					label: 'planReference',
					hidden: true,
				});				
				self.colNames.push("planReference");
				
				self.colModel.push({
					name: 'action',
					label: 'action',
					hidden: true
				});
				self.colNames.push("action");
				
			}
		}).done(function(data, textStatus, jqXHR) {
			self.loadAfeData();
		});
		
		console.log("CostAfeMaster.retrieveColumnSetting done");
		console.log(this.colModel);
	}
	
	CostAfeMaster.prototype.validateData = function(value, column) {
		alert("xx");
        if (value < 0)
            return [false, "Please enter a positive value"];
        else
            return [true, ""];
    }
	
	CostAfeMaster.prototype.getBasicLookName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;

	    $.each(options.colModel.editoptions.value, function (k, value)
	    {
	        if (cellValue == k)
	        {
	            unformatValue = value;
	        }
	    });

	    return unformatValue;
	}
	
	CostAfeMaster.prototype.getCodeName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;
		var n = unformatValue.indexOf("::");
		if (n<=0){
			unformatValue = unformatValue + " :: "+ rowObject["shortDescription"];
		}

	    return unformatValue;
	}
	
	CostAfeMaster.prototype.getVendorName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;

	    $.each(options.colModel.editoptions.value, function (k, value)
	    {
	        if (cellValue == k)
	        {
	            unformatValue = value;
	        }
	    });

	    return unformatValue;
	}
	
	CostAfeMaster.prototype.loadAfeData = function(){
		//this.setProgress(70);
		this.getProgressStatus(true, 70, "Load AFE Data...");
		console.log("loadAfeData Start "  + this.option.counter);
		var page = this.baseUrl + "get_afe_listing";
		page = page + "&costAfeMasterUid=" + this.selectedAfe;
		var self = this;
		this.costAfeDetails = [];
		this.costAfeDetailsSec = [];
		this.costAfeDetailsList = [];
		this.costAfeMaster = [];
//		D3.UI.removeAllChildNodes(this.afeGroup);
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var afeMaster = result.documentElement.getElementsByTagName("CostAfeMaster");
				var afeSection = "";
				for(var i=0; i<afeMaster.length; i++){
					var recMas = new CostAfeMas();
					for(var j in afeMaster[i].children){
						var prop = afeMaster[i].children[j].tagName;
						if(recMas.hasOwnProperty(prop)){
							if(afeMaster[i].children[j].hasChildNodes()){
								recMas[prop] = afeMaster[i].children[j].childNodes[0].nodeValue;
							}
						}
					}
					self.costAfeMaster.push(recMas);
				}

				var camid = self.selectedAfe;
				var rAFE = new CostAfeMas();
				for(var i=0; i<self.costAfeMaster.length; i++){
					if (camid==self.costAfeMaster[i].costAfeMasterUid) rAFE = self.costAfeMaster[i];
				}
				if (self.costAfeMaster.length >= 2) self.hasSup = true;
				var masterAfeUid = rAFE.costAfeMasterUid;
				if (rAFE.afeNumber!=null){
					if (rAFE.afeNumber.length < 15) self.btn_deteleAfe.textContent = "Delete " + rAFE.afeNumber; 
					else {
						var labeltext = rAFE.afeNumber; 
						labeltext = labeltext.substring(0, 15);
						self.btn_deteleAfe.textContent = "Delete " + labeltext + " ..."; 
					} 
				}
				self.afeMaster = rAFE;
				self.afeNoBox.value= rAFE.afeNumber;
				self.afeComment.value= rAFE.longDescription;
				self.afeinterest.value= rAFE.workingInterest;
				self.data1.textContent = "AFE total: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeTotal);
				self.data2.textContent = "AFE used: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeUsed);
				self.data3.textContent = "AFE balance: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeBalance);
				if(self.afeType=="SO"){
				 self.data1.textContent = "Total: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeTotal);
				 self.data2.textContent = "Used: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeUsed);
				 self.data3.textContent = "Balance: " + self.primaryCurrency + " " + self.numberWithCommas(rAFE.afeBalance);
				 self.testNo.textContent = "SO:";
				 vendorName=self.vendorList[self.afeMaster.lookupCompanyUid];
					 $(self.vendorname).removeClass("hide"); 
				 if(document.getElementById("groupCat").options[4]){
			    	document.getElementById("groupCat").options[4].remove();
			     }
					self.vendorname.textContent ="Vendor: "+vendorName;
				}
				
				if (self.afeType!="PRICELIST") $(self.data1).removeClass("hide"); 
				if (self.afeType!="PRICELIST") $(self.data2).removeClass("hide"); 
				if (self.afeType!="PRICELIST") $(self.data3).removeClass("hide"); 
				if (self.afeType!="SO") $(self.data2).addClass("hide"); 
				if (self.afeType!="SO") $(self.data3).addClass("hide"); 

				//self.setProgress(80);
				
				var list = result.documentElement.getElementsByTagName("CostAfeDetail");
				for(var i=0; i<list.length; i++){
					var rec = new CostAfeDetail();
					for(var j in list[i].children){
						var prop = list[i].children[j].tagName;
						if(rec.hasOwnProperty(prop)){
							if(list[i].children[j].hasChildNodes()){
								rec[prop] = list[i].children[j].childNodes[0].nodeValue;
							}
						}
					}
				//	rec.accountCode = rec.accountCode + " :: " + rec.shortDescription;  
					rec.planReferenceName= self.planRefList[rec.planReference];
					rec.companyName=self.vendorList[rec.lookupCompanyUid];
					rec.currencyRate=self.currencyRate[rec.currency];
					rec.run();
					if (rec.costAfeMasterUid == masterAfeUid) self.costAfeDetails.push(rec);
					else self.costAfeDetailsSec.push(rec);
					self.costAfeDetailsList.push(rec);
				}
				
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
	            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
	        }
		}).done(function(data, textStatus, jqXHR) {
			self.editDataEnable();
		}).fail(function() {
        	alert("fail");
        });
		
		console.log("loadAfeData END " + this.option.counter);
	}
	
	CostAfeMaster.prototype.editDataEnable = function(){ 
		if (this.isLocking){

			
			if (this.allowLocking){

			}else{

			}

		}else{
			if (this.allowLocking){
		
			}else{

			}	
		}
		this.createGrid();
	}
	
	CostAfeMaster.prototype.createGrid = function(){
		console.log("Start Create Grid");
		//this.setProgress(90);
		this.getProgressStatus(true, 90, "Load Grid...");
		this.setAfeNo(this.afeMaster.afeNumber);
		this.setAfeComment(this.afeMaster.longDescription);
		var label = "";
		if (this.afeMaster.parentCostAfeMasterUid=="SUPPLEMENTARY"){
			label = this.afeMaster.afeNumber; // + " (Supplementary)";
		} else {
			label = this.afeMaster.afeNumber + " (Master)";
		}
		if (this.option.tabli!=undefined){
			$(this.option.tabli).find(".ui-tabs-anchor").get(0).textContent = label;
		}
	//	this.option.tabli.textContent = this.afeMaster.afeNumber;
		
		if (this.afeGrid == null){
			
			var enable = false;
			if (this.option.currentUser=="jwong"){enable = true; };
			this.afeGrid = $(this.gridCostDetail).jqGrid({
				data: this.costAfeDetails,
			    datatype: 'local',

			    height: this.height,
			    colModel: this.colModel,
				guiStyle: 'bootstrap4',
				altRows: false, 
				
			//	altclass: 'myAltRowClass',
				// >> pager setting
				pager: 'jqGridPager' + this.option.counter,
				recordtext: "{2} records",
				emptyrecords: "Nothing to display",
				rowList: [],
				pgbuttons: false,
				pgtext: null,
				viewrecords: true,
				// << pager setting
				colMenu : enable,
				
				sortable: true,
				headertitles: true,
				rowNum: 500,
				shrinkToFit: true,
				autowidth: true,
				multiselect: true, /* enable leftmost checkbox column*/
				
				multiSort: true,
				grouping: true,
				ondblClickRow: this.onDblClickGridRow.bind(this),
				afterEditCell: function (id,name,val,iRow,iCol){
					alert("afteredit");
				},
				beforeSubmit: function(postdata, formid){ 
					alert("beforeSubmit");
				},
				afterSubmitCell: function(serverStatus, aPostData) {
					var response = serverStatus.responseText;
					  alert(response);
				},
				loadComplete: function() {
					// $("tr.jqgrow:odd").addClass('myAltRowClass');
				},
				checkValues: function(val, valref,g, customobject, nam) {
					
						alert("Check");
				},
				afterInsertRow: function (rowid, aData, rowElem) {
					$("#" + rowid + "").css("background", "yellow");
				},
				inlineEditing: {
		            keys: true,                        
		            saveRowValidation: function (options) {
		                var newData = options.newData;
		                newData.itemCost = String(newData.itemCost).replace(",", "");
		                newData.estimatedDays = String(newData.estimatedDays).replace(",", "");
		                newData.quantity = String(newData.quantity).replace(",", "");
		                return true;
		            }
		        }
			});		
			if (this.option.currentUser=="jwong"){
				this.afeGrid.jqGrid('navGrid','#jqGridPager' + this.option.counter,{del:false,add:false,edit:false,search:true},{},{},{},{multipleSearch:true});
			}else{
				this.afeGrid.jqGrid('navGrid','#jqGridPager' + this.option.counter,{del:false,add:false,edit:false,search:false},{},{},{},{multipleSearch:true});
			}
			this.afeGrid.jqGrid('filterToolbar',
		        {
				  autosearch: true,
				  stringResult: true,
				  searchOnEnter: true,
				  defaultSearch: "cn"          
				});
		} else{
			this.afeGrid.jqGrid('clearGridData');
			this.afeGrid.jqGrid('setGridParam', {data: this.costAfeDetails});
			this.afeGrid.trigger('reloadGrid');
		}
	//	$( this.loadingDialog ).dialog("close");
		this.getProgressStatus(false, 0, "Loading..");
		this.autoResizeGrid();
	}
	
	CostAfeMaster.prototype.onDblClickGridRow = function (rowid, iRow, iCol, e){
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		this.autoResizeGrid();
		console.log("onDblClickGridRow", arguments);

		var oneditfunc = function(){
			console.log(" >> oneditfunc");
		};
		
		
		var successfunc = function(){
			console.log(" >> successfunc", arguments);
		};
		
		var aftersavefunc = function(){
			console.log(" >> aftersavefunc");
			console.log("getLocalRow", this.afeGrid.jqGrid('getLocalRow', rowid));

		};
		
		var errorfunc = function(){
			console.log(" >> errorfunc", arguments);
		};
		
		var afterrestorefunc = function(){
			console.log(" >> afterrestorefunc", arguments);
		};
		
		var beforeEditRow = function(){
			console.log(" >> beforeEditRow", arguments);
			return this.onInitLookupFieldSelection();
		};
		
		this.afeGrid.jqGrid('editRow', rowid, false, oneditfunc.bind(this), successfunc.bind(this), null, null, aftersavefunc.bind(this), errorfunc.bind(this), afterrestorefunc.bind(this), beforeEditRow.bind(this));
	}
	
	CostAfeMaster.prototype.addNewRecord = function(){
		var rec =  new CostAfeDetail();
		rec.quantity = 1.0;
		rec.isnew = true;
		rec.currency = this.primaryCurrency;
		
		var d = new Date();
		var n = d.getTime();
		
		var parameters =
		{
		    rowID : "new" + n,
		    initdata : rec,
		    position :"last",
		};
		this.afeGrid.jqGrid('addRow',parameters);
		this.show(this.rootBox);
		this.groupCat.setAttribute("disabled", true);
		this.disableItems(true);
		this.autoResizeGrid();
	}
	
	CostAfeMaster.prototype.deleteRecords = function(){ 
		var rows = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		if (rows.length==0) alert("Please select 1 item to delete.");
		else{
//			var wantDelete  = confirm("Are you sure you want to delete selected line item(s)?");
//			if (wantDelete){
				for(var i=0; i<rows.length; i++){
					var rec = rows[i];
					var recData = this.afeGrid.jqGrid('getLocalRow', rows[i]);
					recData.action = CostAfeMaster.DEL;
					this.afeGrid.jqGrid('setRowData', rows[i], recData, "DeleteRecordBackground");
				}
				this.currentAction = "delete";
				this.show(this.rootBox);
				this.groupCat.setAttribute("disabled", true);
				this.autoResizeGrid();
//			}
		}
	}
	
	CostAfeMaster.prototype.deleteRecord = function(){
		var rows = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		for(var i=rows.length-1; i>=0; i--){
			console.log("selected Id: ", rows[i]);
			var rec = this.afeGrid.jqGrid('getLocalRow', rows[i]);
			rec.deleted = true;
			this.deletedRecords.push(rec);
			this.afeGrid.jqGrid('delRowData', rows[i]);
		}
		console.log("grid data provider: ", this.afeGrid.jqGrid('getGridParam', 'data')); 
	}
	
	CostAfeMaster.prototype.deleteSelected = function(){

		var rows = this.afeGrid.jqGrid('getGridParam', 'selarrrow');
		if (rows.length>0){
			for(var i=rows.length-1; i>=0; i--){
				this.afeGrid.jqGrid('delRowData', rows[i]);
			}
		}else{
			alert("Please Select Row to delete!");
		}
	}
	
	window.CostAfeMaster = CostAfeMaster;
	
	
})(window);