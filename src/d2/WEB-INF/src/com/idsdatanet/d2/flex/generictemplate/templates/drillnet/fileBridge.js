(function(window) {
									    
	D3.inherits(FileUpload,D3.NodeFilesAttachment);
	
	function FileUpload() {
		D3.NodeFilesAttachment.call(this);
	}

	FileUpload.prototype.createFieldEditor = function() {
		var editorDiv = document.createElement("div");
		var filesUl = document.createElement("ul");
		filesUl.className = "fieldEditor nodeFilesAttachment";
		editorDiv.style.width = "300px"
		this._editorFilesUl = filesUl;
		editorDiv.appendChild(filesUl);
		
		var buttonsDiv = document.createElement("div");
		buttonsDiv.style.paddingTop = "4px";
		if (this._node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)) {
			var addBtn =  document.createElement("button");
			addBtn.className = "DefaultButton";
			addBtn.appendChild(document.createTextNode("Browse"));
			this._editorAddBtn = addBtn;
			var fileInput = document.createElement("input");
			fileInput.style.opacity = 0;
			fileInput.style.position = "absolute";
			fileInput.style.top = 0;
			fileInput.style.right = 0;
			fileInput.style.fontSize = "20px";
			fileInput.style.cursor = "pointer";
			fileInput.setAttribute("type","file");
			fileInput.setAttribute("accept",".pdf,.xlsx,.xls,.html,.xml,.csv");
			fileInput.setAttribute("name","files[]");
			var addSpan = document.createElement("span");
			addSpan.style.position = "relative";
			addSpan.style.overflow = "hidden";
			addSpan.style.display = "inline-block";
			addSpan.style.verticalAlign = "bottom";
			addSpan.appendChild(addBtn);
			addSpan.appendChild(fileInput);
			this._editorFileInput = fileInput;
			var me = this;
			this._fileupload = $(editorDiv).fileupload( {
				url: this._node.commandBeanProxy.baseUrl + "webservice/filemanagerservice.html" + "?mainAction=uploadTempFile",
				dataType: 'json',
				dropZone: editorDiv,
				pasteZone: null,
				fileInput: fileInput,
				sequentialUploads: true,
				autoUpload: false,
				maxChunkSize: 5242880,
				add: function(e, data) {
					me.onAddFiles(data);
				},
				send: function(e, data) {
					me.fileUploadStarted(data);
				},
				fail: function(e, data ){
					me.fileUploadFailed(data);
				},
				done: function(e, data){
					me.fileUploadDone(data);
				},
				chunksend: function(e,data){
					if (data._myProperties.tempFile) {
						data.data.append("tempFile", data._myProperties.tempFile);
						data.data.append("relativePath", data._myProperties.relativePath);
					}
				},
				chunkdone: function(e,data) {
					data._myProperties.uploadRetry = 0;
					data._myProperties.tempFile = data.result.tempFile;
					data._myProperties.relativePath = data.result.relativePath;
				},
				progress: function (e, data) {
					D3.FileUpload.progress(data);
				}
			});
			this.copyFilesIntoFileUpload();
			buttonsDiv.appendChild(addSpan);
		}
		if (this._node.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_DELETE)) {
			var removeBtn =  document.createElement("button");
			removeBtn.className = "DefaultButton";
			removeBtn.appendChild(document.createTextNode("Remove"));
			this._editorRemoveBtn = removeBtn;
			this._editorRemoveBtn.disabled = true;
			this._editorRemoveBtn.hidden = true;
			var self = this;
			removeBtn.onclick = function(){
				self.removeSelected();
			};
			buttonsDiv.appendChild(removeBtn);
		}
		editorDiv.appendChild(buttonsDiv);
		return editorDiv;
	}
	
	FileUpload.prototype.onAddFiles = function(data){
		if(!data._myProperties) data._myProperties = {};
		if(!this._newFileNo) this._newFileNo = 0;
		data._myProperties.fileId = "new_" + (++this._newFileNo); 
		if(!this._filesData._newFiles) this._filesData._newFiles = new Array();
		this._filesData._newFiles.push(data);
		this._node.setFieldValue("fileName",this._filesData._newFiles[0].originalFiles[0].name);
		this._editorAddBtn.disabled = true;
		this._editorAddBtn.hidden = true;
		this._editorRemoveBtn.disabled = false;
		this._editorRemoveBtn.hidden = false;
		this._editorFileInput.disabled = true;
		this._editorFileInput.hidden = true;
		if(!this._copyingNewFiles) this._refreshFieldEditor();
		//$('.fieldEditor.nodeFilesAttachment input').hide(); // Hide checkbox
	};
	
	FileUpload.prototype.collectFile = function(node, result){
		if(node.tagName == "INPUT" && node.type == "checkbox" && node.checked){
			result.push({fileId: node.getAttribute("data-fileId"), newFile: node.getAttribute("data-newFile")});
			return;
		}
		var children = node.children;
		if(children){
			for(var i=0; i < children.length; ++i){
				children[i].checked = true;
				this.collectFile(children[i], result);
			}
		}
	};
	
	FileUpload.prototype.removeSelected = function(){
		var list = new Array();
		this.collectFile(this._editorFilesUl, list);
		if(list.length == 0){
			alert("Please select one or more files to remove.");
			return;
		}
		for(var i=0; i < list.length; ++i){
			var item = list[i];
			if(item.newFile == "1"){
				var files = this._filesData._newFiles;
				for(var j=0; j < files.length; ++j){
					var f = files[j];
					if(f._myProperties.fileId == item.fileId){
						files.splice(j,1);
						break;
					}
				}
			}else{
				var loadedFiles = this._filesData._loadedFiles;
				for(var j=0; j < loadedFiles.length; ++j){
					var f = loadedFiles[j];
					if(f.fileId == item.fileId){
						f.deleted = true;
						break;
					}
				}
			}
		}
		this._node.setFieldValue("fileName","");
		this._editorAddBtn.disabled = false;
		this._editorAddBtn.hidden = false;
		this._editorRemoveBtn.disabled = true;
		this._editorRemoveBtn.hidden = true;
		this._editorFileInput.disabled = false;
		this._editorFileInput.hidden = false;
		this._refreshFieldEditor();
	};
	
	D3.inherits(ReparseBtn,D3.AbstractFieldComponent);
	
	function ReparseBtn(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReparseBtn.prototype.getFieldRenderer = function(){
		this._fieldRenderer = D3.UI.createElement({
			tag:"button",
			text:"Reparse",
			css:"DefaultButton",
			callbacks:{
				onclick:this.reparse
			},
			context:this
		}) ;
		var status = this._node.getFieldValue("status");
		if ((status=="Success") || (status=="Fail")) this._fieldRenderer.hidden = false;
		else this._fieldRenderer.hidden = true;
		return this._fieldRenderer;
	}
	
	ReparseBtn.prototype.reparse = function(){
		var params = {};
		params._invokeCustomFilter = "reparse";
		params.fileBridgeFilesUid = this._node.getFieldValue("fileBridgeFilesUid");
		if(params.fileBridgeFilesUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.completed, this);	
		}
	}
	
	ReparseBtn.prototype.completed = function(response) {
		alert("Not Working at the moment~");
	}
	
	ReparseBtn.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(ReprocessBtn,D3.AbstractFieldComponent);
	
	function ReprocessBtn(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReprocessBtn.prototype.getFieldRenderer = function(){
		this._fieldRenderer = D3.UI.createElement({
			tag:"button",
			text:"Reprocess",
			css:"DefaultButton",
			callbacks:{
				onclick:this.reprocess
			},
			context:this
		}) ;
		var status = this._node.getFieldValue("status");
		if ((status=="Success")) this._fieldRenderer.hidden = false;
		else this._fieldRenderer.hidden = true;
		return this._fieldRenderer;
	}
	
	ReprocessBtn.prototype.reprocess = function(){
		var params = {};
		params._invokeCustomFilter = "reprocess";
		params.fileBridgeFilesUid = this._node.getFieldValue("fileBridgeFilesUid");
		if(params.fileBridgeFilesUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.completed, this);	
		}
	}
	
	ReprocessBtn.prototype.completed = function(response) {
		alert("done");
	}
	
	ReprocessBtn.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(StatusTxt,D3.AbstractFieldComponent);
	
	function StatusTxt(){
		D3.AbstractFieldComponent.call(this);
		this.tic = "";
	}

	StatusTxt.prototype.getFieldRenderer = function(){
		var barsize;
		var fontcolor;

		if (this.getFieldValue()=="Success") { barsize=100; fontcolor='green'; }
		else if (this.getFieldValue()=="Fail") { barsize=75; fontcolor='red'; }
		else if (this.getFieldValue()=="Processing") { barsize=50; fontcolor='orange'; }
		else if (this.getFieldValue()=="Pending") { barsize=25; fontcolor='black'; }
		else { barsize='0%'; fontcolor='black'; }
		
		var containerTemplate = "<div class='statusContainer'>"
			+ "<h3 id='statusLabel' style='background: white; color:" + fontcolor + "'>" + this.getFieldValue() + "</h3>" 
			+ "<div class='progress-container'><progress value='" + barsize + "' max='100'  style='width:100%'/></div>"  +
		+ "</div>";
		var container = $(containerTemplate);
		this._fieldRenderer = container.get(0);
		return this._fieldRenderer;
	}
	
	StatusTxt.prototype.refreshFieldRenderer = function(){
		var barsize;
		var fontcolor;
		
		if(typeof value === 'object') return;
		value = this.getFieldValue();

		if (value=="Success") { barsize=100; fontcolor='green'; }
		else if (value=="Fail") { barsize=75; fontcolor='red'; }
		else if (value=="Processing") { barsize=50; fontcolor='orange'; }
		else if (value=="Pending") { barsize=25; fontcolor='black'; }
		else { barsize='0%'; fontcolor='black'; }
		
		var containerTemplate = "<div class='statusContainer'>"
			+ "<h3 style='background: white; color:" + fontcolor + "'>" + value + "</h3>"
			+ "<div class='progress-container'><progress value='" + barsize + "' max='100' style='width:100%'/></div>"  
		+ "</div>";
		var container = $(containerTemplate);
		D3.UI.removeAllChildNodes(this._fieldRenderer);
		this._fieldRenderer.appendChild(container.get(0));
	}
	
	D3.inherits(RemarkTxt,D3.AbstractFieldComponent);
	
	function RemarkTxt(){
		D3.AbstractFieldComponent.call(this);
		this.tic = "";
	}

	RemarkTxt.prototype.getFieldRenderer = function(){
		if (!this._fieldRenderer) {
			this._fieldRenderer = this.createDefaultSimpleTextRenderer();
		}
		var self = this;
		var status = this._node.getFieldValue("@status");
		this.tic = (status=="Success" || status=="Fail") ? "" : setInterval(function(){self.checkStatus();}, 10000);

		return this._fieldRenderer;
	}
	
	RemarkTxt.prototype.checkStatus = function(){
		var params = {};
		params._invokeCustomFilter = "statusRemark";
		params.fileBridgeFilesUid = this._node != null ? this._node.getFieldValue("fileBridgeFilesUid") : "";
		if(params.fileBridgeFilesUid != "") {
			this._node.commandBeanProxy.customInvoke(params, this.completed, this);	
		}
	}
	
	RemarkTxt.prototype.completed = function(response) {
		var r = $(response).find('parserKey');
		var txt = r[0].textContent;		
		this._node.setFieldValue("parserKey", txt);
		
		r = $(response).find('status');
		var txt1 = r[0].textContent;		
		this._node.setFieldValue("@status", txt1);
		if (txt1=="Success" || txt1=="Fail") {
			clearInterval(this.tic);
		}
		
		r = $(response).find('remark');
		var txt2 = r[0].textContent;
		this._node.setFieldValue("remark", txt2);
		this._node.fireRefreshNodeEvent();
	}
	
	D3.inherits(FileBridgeDataNodeListener,D3.EmptyNodeListener);

	function FileBridgeDataNodeListener() { 
	}

	FileBridgeDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "nFiles"){
			return new FileUpload();
		} else if(id == "reparse"){
			return new ReparseBtn();
		} else if(id == "reprocess"){
			return new ReprocessBtn();
		} else if(id == "status"){
			return new StatusTxt();
		} else if(id == "remark"){
			return new RemarkTxt();
		} else {
			return null;
		}
	}

	D3.CommandBeanProxy.nodeListener = FileBridgeDataNodeListener;
	
})(window);