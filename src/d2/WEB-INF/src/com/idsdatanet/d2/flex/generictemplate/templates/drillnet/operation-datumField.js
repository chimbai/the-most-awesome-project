(function(window){
	
	D3.inherits(DefaultDatumCustomField,D3.AbstractFieldComponent);
	
	function DefaultDatumCustomField(){
		D3.AbstractFieldComponent.call(this);
		this._renderer = null;
		this._rendererText = null;
		this._btnChange = null;
		this._mainEditor = null;
		this._entryEditor = null;
		this._slantEditor = null;
		this._editor = null;
		this._aboveEditor = null;
		this._dynaAttrNames = null;
		this._btnGoToOpsDatum = null;
		this._checkBoxIsGLReference = null;
		this._labelCheckBoxIsGLReference = null;
		this._btnToggleCreateNew = null;
		this._comboBox = null;
	};
	
	
	DefaultDatumCustomField.prototype.dispose = function(){
		if (this._node){
			this._node.removeEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE,this.nodeFieldStateChangeListener,this);
			//this._node.commandBeanProxy.removeEventListener(D3.CommandBeanProxyEvent.REDRAW_ROOT_BUTTONS,this.editModeChangeListener,this);
		}
		DefaultDatumCustomField.uber.dispose.call(this);
		
		this._renderer = null;
		this._rendererText = null;
		this._btnChange = null;
		this._mainEditor = null;
		this._entryEditor = null;
		this._slantEditor = null;
		this._editor = null;
		this._aboveEditor = null;
		this._dynaAttrNames = null;
		this._btnGoToOpsDatum = null;
		this._checkBoxIsGLReference = null;
		this._labelCheckBoxIsGLReference = null;
		this._btnToggleCreateNew = null;
		this._comboBox = null;
		// to be continue
	};

	DefaultDatumCustomField.prototype.getFieldRenderer = function(){
		if (this._renderer)
			return this._renderer;
			
		this._renderer = this.createElement("span",null,"fieldRenderer");
		
		var rendererTextAttributes = {};
		var toolTip = this.getFieldAttr("toolTip");
		if (toolTip)
			rendererTextAttributes.toolTip = toolTip;
		
		this._rendererText = this.createElement("span",null,"datumFieldRendererText",rendererTextAttributes);
		this._renderer.appendChild(this._rendererText);
		
		if (this._node.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_EDIT, "Operation")) {
			var bean = this._config.beanForAcl;
			if((!bean || (bean && this._node.commandBeanProxy.aclManager.isBeanActionAllowed(bean, D3.CommandBeanDataNode.STANDARD_ACTION_READ)))){
				this._btnChange = this.createElement("a",D3.LanguageManager.getLabel("button.changeDatum"),"hyperlinkField",null,{onclick:this.btnChange_onClick});
				this._renderer.appendChild(this._btnChange);
			}
		}
		
		return this._renderer;
	};
	
	DefaultDatumCustomField.prototype.showCreateNewOnly = function(){
		if (this._config){
			var showCreateNewOnly = this._config.showCreateNewOnly;
			return showCreateNewOnly != null && showCreateNewOnly == "true";
		}
		return false;
	};
	
	DefaultDatumCustomField.prototype.getFieldValidator = function(){
		if(this._validator != null) return this._validator;
		
		if(this._validationRules == null) return null;
		
		this._validator = D3.ValidatorManager.getValidator(this._validationRules, true,this._node.commandBeanProxy);
		if(this._validator == null) return null;
		
		this._validator.source = this;
		this._validator.triggerEvent = "";

		return this._validator;			
	}
	
	DefaultDatumCustomField.prototype.getFieldEditor = function(){
		if (this._mainEditor)
			return this._mainEditor;
		
		this._mainEditor = this.createElement("div",null,"VBox");
		this._entryEditor = this.createElement("div",null,"HBox");
		this._slantEditor = this.createElement("div",null,"HBox");
		this._editor = this.createElement("div",null,"HBox");
		this._aboveEditor = this.createElement("div",null,"HBox");
		
		var attrComboBox = {};
		
		var toolTip = this.getFieldAttr("toolTip");
		if (toolTip)
			attrComboBox.toolTip = toolTip;
		
		this._comboBox = this.createElement("select",null,"fieldEditor comboBox required",attrComboBox,{onchange:this.dataChangeListener});
		$(this._comboBox).addClass("floatLeft");
		this._editor.appendChild(this._comboBox);
		
		this._dynaAttrNames = this.getFieldAttr("createNew").split(",");
		
		var tokenizedLabel = null;
		var createNewLabel = this.getFieldAttr("createNewLabel");
		if (createNewLabel != null) {
			tokenizedLabel = createNewLabel.split(",");
		}
		
		this._createNewField = {};
		this._createNewLabel = {};
		
		for (var i= 0; i < this._dynaAttrNames.length; i++) {
			var dynaAttrName = this._dynaAttrNames[i];
			var dynLabel = D3.LanguageManager.getLabel("label."+dynaAttrName);
			this._createNewField[dynaAttrName] = this.createNewField(dynaAttrName);

			if (dynaAttrName == "@offsetMsl") {
				if (this._node && this._node.getFieldValue(this._onOffShoreField) == "ON") {
					this._offsetMslLabel = {};
					this._offsetMslLabel[0] = D3.LanguageManager.getLabel("label.@offsetMsl");
					this._offsetMslLabel[1] = D3.LanguageManager.getLabel("label.@offsetMsl2");
					var datumReferencePoint = this._node.getFieldValue("@datumReferencePoint");
					if (this._offsetMslLabel != null && datumReferencePoint != "MSL") {							
						dynLabel = this._offsetMslLabel[1];
					}
				}
				
			}
			this._createNewLabel[dynaAttrName] = this.createNewLabel(dynLabel);
			if (dynaAttrName == "@entryType") {
				this._entryEditor.appendChild(this._createNewLabel[dynaAttrName]);
			} else if (dynaAttrName == "@slantAngle") {
				this._slantEditor.appendChild(this._createNewLabel[dynaAttrName]);
			} else {
				this._editor.appendChild(this._createNewLabel[dynaAttrName]);
			}
			this.hide(this._createNewLabel[dynaAttrName]);
				
			if (dynaAttrName == "@entryType") {
				this._entryEditor.appendChild(this._createNewField[dynaAttrName].uiElement);
			} else if (dynaAttrName == "@slantAngle") {
				this._slantEditor.appendChild(this._createNewField[dynaAttrName].uiElement);
			} else {
				this._editor.appendChild(this._createNewField[dynaAttrName].uiElement);
			}
			this.hide(this._createNewField[dynaAttrName].uiElement);
		}
		
		
		if (!this.showCreateNewOnly()) {
			var bean = this._config.beanForAcl;
			if (!bean || (bean && this._node.commandBeanProxy.aclManager.isBeanActionAllowed(bean, D3.CommandBeanDataNode.STANDARD_ACTION_ADD))) {
				this._btnToggleCreateNew = this.createElement("button",null,"DefaultButton floatLeft",null,{onclick:this.btnToggleCreateNew_onClick});
				this.setButtonStyle(DefaultDatumCustomField.BUTTON_STYLE_ADD_NEW);
				this._editor.appendChild(this._btnToggleCreateNew);
			}
		}
		var bean = this._config.beanForAcl;
		if (!bean || (bean && (this._node.commandBeanProxy.aclManager.isBeanActionAllowed(bean, D3.CommandBeanDataNode.STANDARD_ACTION_EDIT) || this._node.commandBeanProxy.aclManager.isBeanActionAllowed(bean, D3.CommandBeanDataNode.STANDARD_ACTION_DELETE)))) {	
			this._btnGoToOpsDatum = this.createElement("button",D3.LanguageManager.getLabel("button.editOpsDatum"),"DefaultButton floatLeft",{toolTip:D3.LanguageManager.getLabel("button.editOpsDatum.toolTip")},{onclick:this.btnGoToOpsDatum_onClick});
			this._editor.appendChild(this._btnGoToOpsDatum);
		}	
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.nodeFieldStateChangeListener,this);
		
		var onOffshore = this._node.getFieldValue(this._onOffShoreField);
		if (onOffshore == "ON"){
			
			this._checkBoxIsGLReference = this.createElement("input",null,"CheckboxField",{type:"checkbox",id:"isGLReference"},{onclick:this.checkBoxIsGLReference_onClick});
			if (this._node.getFieldValue("@isGlReference")=="true") this._checkBoxIsGLReference.checked = true;
			else this._checkBoxIsGLReference.checked = false;
			this._labelCheckBoxIsGLReference = this.createElement("label",D3.LanguageManager.getLabel("checkbox.checkBoxIsGLReference"),"CheckboxField fieldRenderer floatLeft",{"for":"isGLReference"});			
			this._labelCheckBoxIsGLReference.style.width = "140px";
			this._aboveEditor.appendChild(this._checkBoxIsGLReference);
			this._aboveEditor.appendChild(this._labelCheckBoxIsGLReference);
			this.hide(this._aboveEditor);
		}
		
		if (this.showCreateNewOnly()) {
			this.toggleCreateNew(true);
		}
		
		if (this._entryEditor.childNodes.length > 0) this._aboveEditor.appendChild(this._entryEditor);
		if (this._slantEditor.childNodes.length > 0) this._aboveEditor.appendChild(this._slantEditor);
		if (this._aboveEditor.childNodes.length > 0) this._mainEditor.appendChild(this._aboveEditor);
		this._mainEditor.appendChild(this._editor);
		return this._mainEditor;
	};
	
	DefaultDatumCustomField.prototype.checkBoxIsGLReference_onClick = function(event){
		var newWidgetField = null;
		var key = null;
		var dynaAttrName = "";
		if (this._checkBoxIsGLReference.checked) {
			this._node.setFieldValue("@isGlReference", "true");
			// reset the value if GLReference is ticked under checkbox

			for (key in this._dynaAttrNames) {
				dynaAttrName =this._dynaAttrNames[key]; 
				if (dynaAttrName != "@offsetMsl") {
					newWidgetField = this._createNewField[dynaAttrName];
					this.hide(newWidgetField);
					this.hide(this._createNewLabel[dynaAttrName]);
					if (newWidgetField.fieldComponent)
						newWidgetField.fieldComponent.enableValidation(false);
				}
				this._node.setFieldValue("@datumType", "--");
				if (dynaAttrName == "@reportingDatumOffset") {
					this._node.setFieldValue("@reportingDatumOffset", "0");
				} else if (dynaAttrName == "@datumReferencePoint") {
					this._node.setFieldValue("@datumReferencePoint", "GL");
				} else if (dynaAttrName == "@entryType") {
					this._node.setFieldValue("@entryType", "0");
				} else {
					this._node.setFieldValue(dynaAttrName, "");
				}
				this._createNewField[dynaAttrName].fieldComponent.refreshFieldEditor();
			}

		} else {
			this._node.setFieldValue("@isGlReference", "false");

			for (key in this._dynaAttrNames) {
				dynaAttrName =this._dynaAttrNames[key];
				this._node.cancelModifiedFieldValue(dynaAttrName);
				if (dynaAttrName == "@offsetMsl") {
					var datumReferencePoint = this._node.getFieldValue("@datumReferencePoint");
					if (datumReferencePoint == "" || datumReferencePoint == "MSL") {
						this.hide(this._createNewField["@offsetMsl"].uiElement);
						this.hide(this._createNewLabel["@offsetMsl"]);
						this._createNewField["@offsetMsl"].fieldComponent.refreshFieldEditor();
					}
				} else if (dynaAttrName == "@slantAngle") {
					var entryType = this._node.getFieldValue("@entryType");
					if (entryType == "0") {
						this.hide(this._createNewField["@slantAngle"].uiElement);
						this.hide(this._createNewLabel["@slantAngle"]);
						this._createNewField["@slantAngle"].fieldComponent.refreshFieldEditor();
					}
				} else {
					newWidgetField = this._createNewField[dynaAttrName];
					this.show(newWidgetField.uiElement);
					this.show(this._createNewLabel[dynaAttrName]);
					if (newWidgetField.fieldComponent){
						newWidgetField.fieldComponent.enableValidation(true);
						newWidgetField.fieldComponent.refreshFieldEditor();
					}
				}
			}
		}
		
	};
	DefaultDatumCustomField.prototype.btnToggleCreateNew_onClick = function(){
		this.toggleCreateNew(true);
	};
	DefaultDatumCustomField.prototype.btnGoToOpsDatum_onClick = function() {
		this._node.commandBeanProxy.gotoUrl("opsdatum.html", "Loading...");
	};

	DefaultDatumCustomField.prototype.setButtonStyle = function(buttonStyle) {
		var toolTip = null;
	
		switch (buttonStyle) {
			case DefaultDatumCustomField.BUTTON_STYLE_ADD_NEW:
				
					toolTip = this.getFieldAttr("buttonToolTip");
					if (!toolTip)toolTip = D3.LanguageManager.getLabel("button.datumToggleCreateNew.default.toolTip");
					this.updateElement(this._btnToggleCreateNew,D3.LanguageManager.getLabel("button.datumToggleCreateNew.default"),{toolTip:toolTip});
					break;
				
			case DefaultDatumCustomField.BUTTON_STYLE_CANCEL:
				
					toolTip = this.getFieldAttr("buttonToolTip");
					if (!toolTip)toolTip = D3.LanguageManager.getLabel("button.datumToggleCreateNew.cancel.toolTip");
					this.updateElement(this._btnToggleCreateNew,D3.LanguageManager.getLabel("button.datumToggleCreateNew.cancel"),{toolTip:toolTip});
					break;
				
				
		}
	};
	DefaultDatumCustomField.prototype.toggleCreateNew = function(refreshEditor) {
		var dynaAttrName = null;
		var newWidgetField = null;
		var key = null;
		if (this._isCreateNew) {
			for (key in this._dynaAttrNames) {
				dynaAttrName =this._dynaAttrNames[key];
				newWidgetField = this._createNewField[dynaAttrName];
				this.hide(newWidgetField.uiElement);
				this.hide(this._createNewLabel[dynaAttrName]);
				newWidgetField.fieldComponent.enableValidation(false);
				this._node.cancelModifiedFieldValue(dynaAttrName);
			}
			this.show(this._comboBox);
			this.show(this._btnGoToOpsDatum);
			this.hide(this._aboveEditor);
			if (refreshEditor) this.refreshFieldEditor();
			this.enableValidation(true);
			this.setButtonStyle(DefaultDatumCustomField.BUTTON_STYLE_ADD_NEW);
			this.updateWarningLabel(this.getDatumType(this._comboBox[this._comboBox.selectedIndex].innerHTML));
		} else {
			var datumReferencePoint = this._node.getFieldValue("@datumReferencePoint");
			var isGlReference = this._node.getFieldValue("@isGlReference");
			var entryType = this._node.getFieldValue("@entryType");
			var offSet = this._node.getFieldValue("@entryType");
			var is_show;
			if (isGlReference == "true") {
				this._checkBoxIsGLReference.selected = true;
				this._node.setFieldValue("@isGlReference", "true");
			}

			for (key in this._dynaAttrNames) {
				dynaAttrName =this._dynaAttrNames[key];
				newWidgetField = this._createNewField[dynaAttrName];
				is_show = false;

				if (isGlReference == "true") {
					if (dynaAttrName == "@offsetMsl") {
						is_show = true;
					} else {
						is_show = false;
					}
					this._node.setFieldValue("@datumType", "--");
					if (dynaAttrName == "@reportingDatumOffset") {
						this._node.setFieldValue("@reportingDatumOffset", "0");
					} else if (dynaAttrName == "@datumReferencePoint") {
						this._node.setFieldValue("@datumReferencePoint", "GL");
					} else if (dynaAttrName == "@entryType") {
						this._node.setFieldValue("@entryType", "0");
					} else if (dynaAttrName == "@slantAngle" && entryType == "0") {
						is_show = false;
					}

				} else {
					if (dynaAttrName == "@slantAngle" && entryType == "0") {
						is_show = false;
					} else if (dynaAttrName == "@slantAngle" && entryType == "1") {
						is_show = true;
					} else {
						if (dynaAttrName != "@offsetMsl" || (datumReferencePoint != "" && datumReferencePoint != "MSL")) {
							is_show = true;
						}
					}
				}

				if (is_show) {
					this.show(newWidgetField.uiElement);
					this.show(this._createNewLabel[dynaAttrName]);
					this.show(this._aboveEditor);
				}
				if (this._isSetDateOnNewWidget) {
					if (refreshEditor) newWidgetField.fieldComponent.refreshFieldEditor();
				} else {
					newWidgetField.data(this._node);
					if (dynaAttrName == "@reportingDatumOffset" || dynaAttrName == "@offsetMsl" || dynaAttrName == "@slantAngle"){
						$(newWidgetField._fieldComponent.getFieldEditor()).addClass("datumTextField");
					}
					$(newWidgetField.fieldComponent.getFieldEditor()).addClass("autoWidth");
				}
				newWidgetField.fieldComponent.enableValidation(is_show);
			}
			this._isSetDateOnNewWidget = true;
			this.hide(this._comboBox);
			this.hide(this._btnGoToOpsDatum);
			this.enableValidation(false);
			this._node.cancelModifiedFieldValue(this._fieldName);
			this.setButtonStyle(DefaultDatumCustomField.BUTTON_STYLE_CANCEL);
			this.updateWarningLabel(this._node.getFieldValue("@datumType"));
		}
		if (!this.showCreateNewOnly()) {
			this._isCreateNew = !this._isCreateNew;
		}
		if (this._editorPanel) {
			$(this._editorPanel).dialog('show');
		}
		
	};
	
	DefaultDatumCustomField.prototype.nodeFieldStateChangeListener = function(event) {
		if (event.currentTarget == this._node) {
			if (event.fieldName == "@datumReferencePoint") {
				var datumReferencePoint = this._node.getFieldValue("@datumReferencePoint");
				if (datumReferencePoint != "" && datumReferencePoint != "MSL") {
					if (this._offsetMslLabel != null) {
						
						if (datumReferencePoint != "MSL") {
							$(this._createNewLabel["@offsetMsl"]).text(this._offsetMslLabel[1]);
						} else {
							$(this._createNewLabel["@offsetMsl"]).text(this._offsetMslLabel[0]);
						}
					}
					this.show(this._createNewField["@offsetMsl"].uiElement);
					this.show(this._createNewLabel["@offsetMsl"]);
					this._createNewField["@offsetMsl"].fieldComponent.enableValidation(true);
				} else {
					this.hide(this._createNewField["@offsetMsl"].uiElement);
					this.hide(this._createNewLabel["@offsetMsl"]);
					this._node.setFieldValue("@offsetMsl", "");
					this._createNewField["@offsetMsl"].fieldComponent.refreshFieldEditor();
					this._createNewField["@offsetMsl"].fieldComponent.enableValidation(false);
				}
			} else if (event.fieldName == "@entryType") {
				var entryType = this._node.getFieldValue("@entryType");
				if (entryType == "1") {
					this.show(this._createNewField["@slantAngle"].uiElement);
					this.show(this._createNewLabel["@slantAngle"]);
					this._createNewField["@slantAngle"].fieldComponent.enableValidation(true);
				} else {
					this.hide(this._createNewField["@slantAngle"].uiElement);
					this.hide(this._createNewLabel["@slantAngle"]);
					this._node.setFieldValue("@slantAngle", "");
					this._createNewField["@slantAngle"].fieldComponent.refreshFieldEditor();
					this._createNewField["@slantAngle"].fieldComponent.enableValidation(false);
				}
			} else if (event.fieldName == "@datumType") {
				this.updateWarningLabel(this._node.getFieldValue("@datumType"));
			}
			if (this._dynaAttrNames.indexOf(event.fieldName) != -1) {
				this._node.commandBeanProxy.rootNode.atts.forceReloadLookupReference = true;
			}
		}
	};
	DefaultDatumCustomField.prototype.dataChangeListener = function(event){
		if (event.currentTarget == this._comboBox) {
			var rawValue = null;
			if (this._comboBox.selectedIndex !=-1) {
				var selected = this._comboBox[this._comboBox.selectedIndex];
			
				rawValue = selected.value;
				this.updateWarningLabel(this.getDatumType(selected.innerHTML));
			}
			this.setFieldValue(rawValue);
			this.fieldContainer.editorUpdatedFieldValue();
		}
	};
	DefaultDatumCustomField.prototype.setEditModeInUse = function(value) {
		DefaultDatumCustomField.uber.setEditModeInUse.call(this,value);
		if (!value) {
			if (this._isCreateNew) {
				this.toggleCreateNew(false);
			}
		}
	};
	DefaultDatumCustomField.prototype.show = function(value) {
		if (!value) return;
		if (value instanceof D3.Field)
			D3.UI.show(value.uiElement);
		else
			D3.UI.show(value);
	};

	DefaultDatumCustomField.prototype.hide = function(value) {
		if (!value) return;
		if (value instanceof D3.Field)
			D3.UI.hide(value.uiElement);
		else
			D3.UI.hide(value);
	};

	DefaultDatumCustomField.prototype.updateWarningLabel = function(datumType) {
		if (this._warningLabel != null) {
			if (datumType != null && datumType != "") {
				var msg = D3.LanguageManager.getLabel("warning.changeDatum");
				msg = msg.replace("{datumType}",datumType);
				this.updateElement(this._warningLabel,msg);
				this.show(this._warningLabel);
			} else {
				this.hide(this._warningLabel);
			}
		}
	};

	DefaultDatumCustomField.prototype.getDatumType = function(datumDisplayName) {
		if (datumDisplayName != null) {
			var parts = datumDisplayName.split(" "); // assume the display name will always be in the format of "100 m RT MSL"
			if (parts != null && parts.length >= 2) {
				return parts[parts.length - 2];
			}
		}
		return null;
	};

	DefaultDatumCustomField.prototype.afterPropertiesSet = function() {
		DefaultDatumCustomField.uber.afterPropertiesSet.call(this);
		if (this.fieldContainer) {
			this.fieldContainer.triggerEditOnMouseClick = false;
		}
		this._onOffShoreField = this.getFieldAttr("onOffShoreField");
		if (this._onOffShoreField == "") {
			this._onOffShoreField = "@well.onOffShore";
		}
		this._countryField = this.getFieldAttr("countryField");
		if (this._countryField == "") {
			this._countryField = "@well.country";
		}
	};
	DefaultDatumCustomField.prototype.createOption = function(data, label, removeLineBreak, isSelected) {
		if (removeLineBreak) {
			label = label.replace(/(\r\n|\n|\r)/gm," ");
		}
		if (this._textImportantLength && this._textImportantLength > 0) {
			if (label.length > this._textImportantLength) {
				if (this._textImportantPositionRight) {
					label = "..." + label.substring((label.length - this._textImportantLength), label.length);
				} else {
					label = label.substring(0, this._textImportantLength) + "...";
				}
			}
		}
		var option = document.createElement("option");
		option.value = data;
		option.selected = isSelected;
		option.appendChild(document.createTextNode(label)); // workaround for IE's odd bug
		return option;
	};
	
	DefaultDatumCustomField.prototype.refreshFieldEditor = function(){
		var dynaAttrName = null;
		var key = null;
		if (this._isCreateNew) {
			for (key in this._dynaAttrNames) {
				dynaAttrName =this._dynaAttrNames[key];
				var newWidgetField = this._createNewField[dynaAttrName];
				newWidgetField.fieldComponent.refreshFieldEditor();
			}
		}

		var showNewWidgetField = false;
		var fields = ["@reportingDatumOffset", "@offsetMsl"];
		for (key in fields) {
			dynaAttrName = fields[key];
			var createNewFieldValue = this._node.getFieldValue(dynaAttrName);
			if (createNewFieldValue != null && createNewFieldValue != "") {
				showNewWidgetField = true;
			}
		}
		if (showNewWidgetField) {
			if (!this._isCreateNew) {
				this.toggleCreateNew(false);
			}
		}

		var rawValue = this.getFieldValue();

		var lookupRef = this._node.getLookup(this._fieldName);

		var selectedItem= null;
		D3.UI.removeAllChildNodes(this._comboBox);
		var removeLineBreakWhenEdit = (this._config.lookupRemoveLineBreakWhenEdit && this._config.lookupRemoveLineBreakWhenEdit === "true") ? true:false;
		lookupRef.forEachActiveLookup(this, function(lookupItem) {
			if (!selectedItem && lookupItem.data == rawValue) {
				selectedItem = lookupItem;
			}
			this._comboBox.appendChild(this.createOption(lookupItem.data, lookupItem.label, removeLineBreakWhenEdit, (lookupItem.data == rawValue)));
		});

		if (selectedItem != null) {
			this.updateWarningLabel(this.getDatumType(selectedItem.label));
			return;
		}else if (!selectedItem && rawValue) {
			var label = "";
			if (rawValue != "") {
				label = rawValue + " (Lookup N/A)";
			}
			
			
			this._comboBox.appendChild(this.createOption(rawValue, label, removeLineBreakWhenEdit, true));
		}
	};

	DefaultDatumCustomField.prototype.createNewField = function(fieldName,container){
		var config = {name:fieldName};
		if (fieldName == "@datumReferencePoint") {
			config.editPolicy = "on";
			config.customField = "DatumReferenceCombobox";
			if (this._node.atts.editMode) config.onChange = "redrawLayout";
			config.onOffShoreField = this.getFieldAttr("onOffShoreField");
			config.countryField = String(this._config.countryField);
			
		} else if (fieldName == "@reportingDatumOffset" || fieldName == "@offsetMsl" || fieldName == "@slantAngle") {
			config.editPolicy = "on";
			config.width=80;
		} else if(fieldName == "@datumType"){
			config.editPolicy = "on";
			config.customField = "DatumTypeCombobox";
		} else{
			config.editPolicy = "on";
		}
		var field = new D3.Field(config,container);
		$(field.uiElement).addClass("floatLeft autoWidth");
		
		return field;
	};
	
	DefaultDatumCustomField.prototype.createNewLabel = function(text){
		return this.createElement("span",text,"fieldRenderer floatLeft autoWidth");
	};
	
	DefaultDatumCustomField.prototype.setRendererText = function(text){
		$(this._rendererText).html(text);
	};
	
	
	
	DefaultDatumCustomField.prototype.refreshFieldRenderer = function(){
		var lookupRef = this._node.getLookup(this._fieldName);
		var fieldValue = this.getFieldValue();
		var selectedLookupItem = lookupRef.getLookupItem(fieldValue);
		if (selectedLookupItem){
			this.setRendererText(selectedLookupItem.label);
		}else{
			this.setRendererText(fieldValue + " <font color=\"#FF0000\">(Lookup N/A)</font>");
		}
			
	};
	
	DefaultDatumCustomField.prototype.btnChange_onClick = function(){
		// to do:changeDatumEventHandler
		if (!this._pnlEditor){
			this._pnlEditor = this.createElement("div");
			this._pnlEditor.setAttribute("style", "min-width:200px");
			this._warningLabel = this.createElement("span",null,"popupDatumWarning");
			this._pnlEditor.appendChild(this._warningLabel);
			this.hide(this._warningLabel);
			var datumHBox = this.createElement("div",null,"HBox");
			datumHBox.appendChild(this.getFieldEditor());
			this._pnlEditor.appendChild(datumHBox);
			this._pnlEditor.self = this;
		}
		this.refreshFieldEditor();
		
		var btnConfirm = {
				text:D3.LanguageManager.getLabel("button.popupDatumConfirm"),
				"class":"PopupConfirmButton",
				click:function(){
					$(this).get(0).self.btnConfirm_onClick();
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.popupDatumCancel"),
				"class":"PopupCancelButton",
				click:function(){
					$(this).get(0).self.btnCancel_onClick();
	                $(this).dialog('close');
				}
		};
		
		$(this._pnlEditor).dialog({
		        resizable: false,
		        modal: true,
		        width:"auto",
		        title: D3.LanguageManager.getLabel("popupDatum.title"),	        
		        buttons: [btnConfirm,btnCancel],
		        open: function(event, ui) { 
		            //hide close button.
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        },
		        
		    });
	};
	DefaultDatumCustomField.prototype.btnConfirm_onClick = function(){
		var result = true;
		for (var key in this._dynaAttrNames){
			var dynaAttrName = this._dynaAttrNames[key];
			var newWidgetField = this._createNewField[dynaAttrName];
			if (D3.UI.isVisible(newWidgetField.uiElement)){
				if (!this._node.validateField(dynaAttrName)){
					result = false;
				}
			}
		}
		if (this._comboBox.value == ""){
			result = false;
		}
		if (result){
			var params = this.getParams();
			this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
		}else{
			alert(D3.LanguageManager.getLabel("warning.failToValidate"));
		}
			
	}
	DefaultDatumCustomField.prototype.loaded = function(){
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	DefaultDatumCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "changeDatum";
		params.operationUid = this._node.getFieldValue("operationUid");
		params.defaultDatumUid = this._node.getFieldValue("defaultDatumUid");
		params.reportingDatumOffset = this._node.getFieldValue("@reportingDatumOffset");
		params.datumType = this._node.getFieldValue("@datumType");
		params.datumReferencePoint = this._node.getFieldValue("@datumReferencePoint");
		params.offsetMsl = this._node.getFieldValue("@offsetMsl");
		params.entryType = this._node.getFieldValue("@entryType");
		params.slantAngle = this._node.getFieldValue("@slantAngle");
		params.isGlReference = this._node.getFieldValue("@isGlReference");
		return params;
	}
	
	
	
	DefaultDatumCustomField.prototype.btnCancel_onClick = function(){
		this._node.cancelAllActions();
	}
	DefaultDatumCustomField.prototype.getFieldAttr = function(name){
		if (this._config){
			return this._config[name];
		}
		return null;
	};
	
	DefaultDatumCustomField.prototype.updateElement = function(element,text,css,attributes){
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
	};
		
	DefaultDatumCustomField.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	DefaultDatumCustomField.BUTTON_STYLE_ADD_NEW = 0;
	DefaultDatumCustomField.BUTTON_STYLE_CANCEL = 1;
	
	D3.DefaultDatumCustomField = DefaultDatumCustomField;
	
})(window);