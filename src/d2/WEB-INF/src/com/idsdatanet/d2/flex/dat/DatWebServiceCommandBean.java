package com.idsdatanet.d2.flex.dat;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.servlet.ServletContext;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.validation.BindException;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.AbstractDatumFormatter;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;
import com.idsdatanet.d2.costnet.CostNetConstants;
import com.idsdatanet.d2.drillnet.mudVolume.MudVolumeUtil;

public class DatWebServiceCommandBean extends AbstractGenericWebServiceCommandBean implements ServletContextAware {
	
	public static final String PARAM_INVOCATION_METHOD = "method";
	public static final String METHOD_GET_ACTIVITY_LIST = "get_activity_list";
	public static final String METHOD_GET_COST_ANALYSIS_LIST = "get_cost_list";
	public static final String METHOD_GET_DVD_LIST = "get_dvd_list";
	public static final String METHOD_GET_CANQUERY_OPTIONS = "getCanQueryOptions";
	public static final String METHOD_GET_CANQUERY_RESULT = "getCanQueryResult";
	public static final String METHOD_PROCESS_FILE_OUTPUT = "processFileOutput";
	public static final String METHOD_GET_SETTINGS = "get_dat_settings";
	public static final String METHOD_SAVE_PDF = "save_pdf";
	
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
	private ServletContext servletContext = null;
	private String operationUid = "";
	private Double activityDurationTotal = 0.0;
	private Map<String, String> datumNameMap = null; 
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String method = request.getParameter(PARAM_INVOCATION_METHOD);
		if (method==null) return;
		String filename = null;
		if (method.indexOf("?")>=0) { // for save
			if (method.indexOf("=")>method.indexOf("?")) {
				filename = method.substring(method.indexOf("=") + 1);
			}
			method = method.substring(0, method.indexOf("?"));
		}
		if (METHOD_GET_ACTIVITY_LIST.equals(method)) {
			this.getActivityList(request, response);
		} else if (METHOD_GET_COST_ANALYSIS_LIST.equals(method)) {
			this.getCostAnalysisList(request, response);
		} else if (METHOD_GET_DVD_LIST.equals(method)) {
			this.getDvdList(request, response);
		} else if (METHOD_GET_CANQUERY_OPTIONS.equals(method)) {
			this.getCanQueryOptions(request, response);
		} else if (METHOD_GET_CANQUERY_RESULT.equals(method)) {
			if (request.getParameter("hql")!=null) {
				this.getCanQueryResult(request, response);
			} else if (request.getParameter("xmlHql")!=null) {
				this.getCanQueryResultSpecial(request, response);
			}
		} else if (METHOD_PROCESS_FILE_OUTPUT.equals(method)) {
			this.processFileOutput(request, response);
		} else if (METHOD_GET_SETTINGS.equals(method)) {
			this.getDatSettings(request, response);
		} else if (METHOD_SAVE_PDF.equals(method)) {
			this.savePDF(request, response, filename);
		} 
	}
	
	private Locale getLocale(HttpServletRequest request) throws Exception {
		return UserSession.getInstance(request).getUserLocale();
	}
	
	private void getDvdList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		
		String[] operationUids = request.getParameterValues("operationUids");
		if (operationUids != null) {
			for (int i = 0; i < operationUids.length; i++) {
				Double p10 = 0.0;
				Double p50 = 0.0;
				Double p90 = 0.0;
				Double budget = 0.0;
				String operationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUids[i]);
				operationUomContext.setOperationUid(operationUids[i]);
				CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "daysSpentPriorToSpud", operationUomContext);
				CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(request), OperationPlanPhase.class, "depthMdMsl", operationUomContext);
				
				if (operationPlanMasterUid!=null) {
					String queryString = "FROM OperationPlanPhase WHERE (isDeleted=false or isDeleted is null) and operationPlanMasterUid=:operationPlanMasterUid order by sequence";
					List<OperationPlanPhase> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationPlanMasterUid", operationPlanMasterUid, queryProperties);
					
					for (OperationPlanPhase phase: list) {
						Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getCachedOperation(phase.getOperationUid());
						
						writer.startElement("OperationPlanTask");
						writer.addElement("operationName", operation != null ? operation.getOperationName() : "Unknown Operation");
						writer.addElement("sequence", (phase.getSequence()==null?"0":phase.getSequence().toString()));
						Double duration = 0.0;
						Double depth = 0.0;
						if (phase.getP10Duration()!=null) {
							durationConverter.setBaseValue(phase.getP10Duration());
							duration = durationConverter.getConvertedValue();
							p10 += duration;
							writer.addElement("p10Duration", p10.toString());
						}
						if (phase.getP50Duration()!=null) {
							durationConverter.setBaseValue(phase.getP50Duration());
							duration = durationConverter.getConvertedValue();
							p50 += duration;
							writer.addElement("p50Duration", p50.toString());
						}
						if (phase.getP90Duration()!=null) {
							durationConverter.setBaseValue(phase.getP90Duration());
							duration = durationConverter.getConvertedValue();
							p90 += duration;
							writer.addElement("p90Duration", p90.toString());
						}
						if (phase.getDepthMdMsl()!=null) {
							depthConverter.setBaseValue(phase.getDepthMdMsl());
							depthConverter.addDatumOffset();
							depth = depthConverter.getBasevalue();
						}						
						writer.addElement("depthMdMsl", (phase.getDepthMdMsl()==null?"0":depth.toString()));
						if (phase.getPhaseBudget()!=null) budget += phase.getPhaseBudget();
						writer.addElement("taskBudget", budget.toString());
						writer.endElement();
					}
				}
			}
		}		
		
		writer.close();
	}
	
	private Map<String, LookupItem> getLookupList(HttpServletRequest request, String uri) {
		Map<String, LookupItem> result = new HashMap<String, LookupItem>(); 
		try {
			result = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
		} catch (Exception e) {
			System.out.println("lookup not found : " + uri);
		}
		return result;
	}
	
	private void getActivityList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		datumNameMap = new HashMap<String, String>();
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		HQLQuery query = new HQLQuery("SELECT d.operationUid, a, d, o from Activity a, Daily d, Operation o " +
				"where (a.isDeleted = false or a.isDeleted is null) " +
				"and (d.isDeleted = false or d.isDeleted is null) " +
				"and (o.isDeleted = false or o.isDeleted is null) " +
				"and a.dailyUid = d.dailyUid and a.operationUid=o.operationUid " +
				"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
				"and (a.isSimop=false or a.isSimop is null) " +
				"and (a.isOffline=false or a.isOffline is null) ");
		DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
		
		String[] operationUids = request.getParameterValues("operationUids");
		if (operationUids != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < operationUids.length; i++) {
				query.queryString += operator + "o.operationUid = :operationUid_" + i;
				query.addParam("operationUid_" + i, operationUids[i]);
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] classCodes = request.getParameterValues("classCodes");
		if (classCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < classCodes.length; i++) {
				String[] classCode = classCodes[i].split("::");
				if (StringUtils.isNotBlank(classCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_c" + i + " and a.classCode = :classCode_" + i + ")";
					query.addParam("operationCode_c" + i, classCode[0]);
					query.addParam("classCode_" + i, classCode[1]);
				} else {
					query.queryString += operator + " (a.classCode = :classCode_" + i + ")";
					query.addParam("classCode_" + i, classCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] phaseCodes = request.getParameterValues("phaseCodes");
		if (phaseCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < phaseCodes.length; i++) {
				String[] phaseCode = phaseCodes[i].split("::");
				if (StringUtils.isNotBlank(phaseCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_p" + i + " and a.phaseCode = :phaseCode_" + i + ")";
					query.addParam("operationCode_p" + i, phaseCode[0]);
					query.addParam("phaseCode_" + i, phaseCode[1]);
				} else {
					query.queryString += operator + " (a.phaseCode = :phaseCode_" + i + ")";
					query.addParam("phaseCode_" + i, phaseCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] taskCodes = request.getParameterValues("taskCodes");
		if (taskCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < taskCodes.length; i++) {
				String[] taskCode = taskCodes[i].split("::");
				if (StringUtils.isNotBlank(taskCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_t" + i + " and a.taskCode = :taskCode_" + i + ")";
					query.addParam("operationCode_t" + i, taskCode[0]);
					query.addParam("taskCode_" + i, taskCode[1]);
				} else {
					query.queryString += operator + " (a.taskCode = :taskCode_" + i + ")";
					query.addParam("taskCode_" + i, taskCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] rootCauseCodes = request.getParameterValues("rootCauseCodes");
		if (rootCauseCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < rootCauseCodes.length; i++) {
				String[] rootCauseCode = rootCauseCodes[i].split("::");
				if (StringUtils.isNotBlank(rootCauseCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_r" + i + " and a.rootCauseCode = :rootCauseCode_" + i + ")";
					query.addParam("operationCode_r" + i, rootCauseCode[0]);
					query.addParam("rootCauseCode_" + i, rootCauseCode[1]);
				} else {
					query.queryString += operator + " (a.rootCauseCode = :rootCauseCode_" + i + ")";
					query.addParam("rootCauseCode_" + i, rootCauseCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] userCodes = request.getParameterValues("userCodes");
		if (userCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < userCodes.length; i++) {
				String[] userCode = userCodes[i].split("::");
				if (StringUtils.isNotBlank(userCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_r" + i + " and a.userCode = :userCode_" + i + ")";
					query.addParam("operationCode_r" + i, userCode[0]);
					query.addParam("userCode_" + i, userCode[1]);
				} else {
					query.queryString += operator + " (a.userCode = :userCode_" + i + ")";
					query.addParam("userCode_" + i, userCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] jobTypeCodes = request.getParameterValues("jobTypeCodes");
		if (jobTypeCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < jobTypeCodes.length; i++) {
				String[] jobTypeCode = jobTypeCodes[i].split("::");
				if (StringUtils.isNotBlank(jobTypeCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_r" + i + " and a.jobTypeCode = :jobTypeCode_" + i + ")";
					query.addParam("operationCode_r" + i, jobTypeCode[0]);
					query.addParam("jobTypeCode_" + i, jobTypeCode[1]);
				} else {
					query.queryString += operator + " (a.jobTypeCode = :jobTypeCode_" + i + ")";
					query.addParam("jobTypeCode_" + i, jobTypeCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		query.queryString += " order by d.dayDate, a.dayPlus, a.startDatetime";
		
		Map<String, LookupItem>companyList = getLookupList(request, "db://LookupCompany?key=lookupCompanyUid&value=companyName");			
		Map<String, LookupItem>rigList = getLookupList(request, "db://RigInformation?key=rigInformationUid&value=rigName");
		Map<String, LookupItem>holeSizeList = getLookupList(request, "xml://activity.holeSize?key=code&amp;value=label");
		Map<String, LookupItem>sectionsList = getLookupList(request, "xml://Activity.sections?key=code&amp;value=label");
		Map<String, LookupItem>incidentLevelList = getLookupList(request, "xml://Activity.incidentLevel?key=code&amp;value=label");
		
		Map<String, Double>durationList = new HashMap<String, Double>();
		Map<String, ReportDaily>reportDailyMap = new HashMap<String, ReportDaily>();
		
		
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query.queryString, query.paramNames, query.paramValues, queryProperties);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		writer.addElement("NumberOfRecords", String.valueOf(list.size()));
		
		String value = null;
		for (Object[] obj: list) {
			SimpleAttributes atts = new SimpleAttributes();
			String operationUid = (String) obj[0];
			
			operationUomContext.setOperationUid(operationUid);
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "depthMdMsl", operationUomContext);
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
			CustomFieldUom holeSizeConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "holeSize", operationUomContext);
			
			//String strSql = null;
			Activity activity = (Activity) obj[1];
			Daily daily = (Daily) obj[2];
			Operation operation = (Operation) obj[3];
			
			atts.addAttribute("activityUid", activity.getActivityUid());
			atts.addAttribute("dailyUid", activity.getDailyUid());
			
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), operationUid, null, true);
			if (operationName==null) operationName = "Unknown Operation";
			atts.addAttribute("operationName", operationName);
			atts.addAttribute("operationCode", operation.getOperationCode());
			
			ReportDaily reportDaily = null;
			if (reportDailyMap.containsKey(daily.getDailyUid())) {
				reportDaily = reportDailyMap.get(daily.getDailyUid());
			} else {
				reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(activity.getOperationUid()), daily.getDailyUid(), false);
				if (reportDaily==null) {
					reportDaily = new ReportDaily();
					reportDaily.setReportNumber("-");
				}
				reportDailyMap.put(daily.getDailyUid(), reportDaily);
			}
			Date todayDate = daily.getDayDate();
			String reportNumber = reportDaily.getReportNumber();
			Integer dayPlus = activity.getDayPlus();
			if (dayPlus==null) dayPlus = 0;
			
			atts.addAttribute("dayNumber", WellNameUtil.getPaddedStr(reportNumber));
			atts.addAttribute("dayNumber__text", (dayPlus>0?reportNumber+"+":reportNumber));
			atts.addAttribute("dayDate__raw", String.valueOf(todayDate.getTime() + dayPlus));
			atts.addAttribute("dayDate", dateFormat.format(todayDate));
			
			atts.addAttribute("startDatetime", timeFormat.format(activity.getStartDatetime()));
			String endTime = timeFormat.format(activity.getEndDatetime());
			if (endTime.startsWith("00:00") || endTime.startsWith("23:59")) endTime = "24:00";
			atts.addAttribute("endDatetime", endTime);
			
			Double duration = activity.getActivityDuration();
			if (duration == null) duration = 0.0;
			durationConverter.setBaseValue(duration);
			atts.addAttribute("activityDuration", String.valueOf(durationConverter.getConvertedValue()));
			value = durationConverter.formatOutputPrecision();
			atts.addAttribute("activityDuration__text", value);
			atts.addAttribute("activityDuration_uomSymbol", durationConverter.getUomSymbol());
			
			atts.addAttribute("incidentNumber", activity.getIncidentNumber()!=null?WellNameUtil.getPaddedStr(activity.getIncidentNumber().toString()):"");
			atts.addAttribute("incidentNumber__text", activity.getIncidentNumber()!=null?activity.getIncidentNumber().toString():"");
			String incidentLevel = (activity.getIncidentLevel()!=null?activity.getIncidentLevel().toString():"");
			LookupItem lookupItem = incidentLevelList.get(incidentLevel);
			if (lookupItem!=null) incidentLevel = lookupItem.getValue().toString();
			atts.addAttribute("incidentLevel", incidentLevel);

			String sections = (activity.getSections()!=null?activity.getSections():"");
			lookupItem = sectionsList.get(sections);
			if (lookupItem!=null) sections = (String) lookupItem.getValue();
			atts.addAttribute("sections", activity.getSections()!=null?activity.getSections():"");
			
			String hoursInDay = value;
			
			Double totalDuration = 0.0;
			if (durationList.containsKey(activity.getDailyUid())) {
				totalDuration = durationList.get(activity.getDailyUid()); 
			} else {
				List<Double> ls = null;
				String strSql = "SELECT sum(activityDuration) FROM Activity " +
						"WHERE (isDeleted=false OR isDeleted is null) " +
						"and (isSimop=false or isSimop is null) " +
						"and (isOffline=false or isOffline is null) " +
						"AND dailyUid=:dailyUid " +
						"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) ";
				ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", activity.getDailyUid(), queryProperties);
				if (ls.size()>0) totalDuration = ls.get(0);
				if (totalDuration==null) totalDuration = 0.0;
				durationList.put(activity.getDailyUid(), totalDuration);
			}
			
			durationConverter.setBaseValue(totalDuration);
			value = durationConverter.formatOutputPrecision();
			Double percentage = 0.0;
			if (totalDuration > 0.0) percentage = duration / totalDuration * 100;
			hoursInDay += " / " + value + " = " + formatter.format(percentage);
			atts.addAttribute("hoursInDay", hoursInDay);
			atts.addAttribute("percentage", String.valueOf(percentage));
			
			Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(daily.getDailyUid());
			if (dayCost==null)
			{
				dayCost = 0.0;
				if (reportDaily!=null && reportDaily.getDaycost() != null) dayCost += reportDaily.getDaycost();
			}
			if (reportDaily!=null && reportDaily.getDaycompletioncost() != null) dayCost += reportDaily.getDaycompletioncost();
			
			atts.addAttribute("dayCost", String.valueOf(dayCost));
			costConverter.setBaseValue(dayCost);
			atts.addAttribute("dayCost__text", costConverter.formatOutputPrecision());
			atts.addAttribute("dayCost_uomSymbol", costConverter.getUomSymbol());
			Double costImpact = Math.round(dayCost * percentage) / 100.0;
			atts.addAttribute("costImpact", String.valueOf(costImpact));
			costConverter.setBaseValue(costImpact);
			atts.addAttribute("costImpact__text", costConverter.formatOutputPrecision());
			atts.addAttribute("costImpact_uomSymbol", costConverter.getUomSymbol());
			
			if (StringUtils.isNotBlank(activity.getClassCode())) {
				atts.addAttribute("classCode", activity.getClassCode());
			} else {
				atts.addAttribute("classCode", " ");
			}
			
			if (StringUtils.isNotBlank(activity.getPhaseCode())) {
				atts.addAttribute("phaseCode", activity.getPhaseCode());
			} else {
				atts.addAttribute("phaseCode", " ");
			}
			
			if (StringUtils.isNotBlank(activity.getTaskCode())) {
				atts.addAttribute("taskCode", activity.getTaskCode());
			} else {
				atts.addAttribute("taskCode", " ");
			}
			
			if (StringUtils.isNotBlank(activity.getRootCauseCode())) {
				atts.addAttribute("rootCauseCode", activity.getRootCauseCode());
			} else {
				if ("TP".equals(activity.getClassCode()) || "TU".equals(activity.getClassCode()))
				atts.addAttribute("rootCauseCode", " ");
			}
			
			atts.addAttribute("rigStatus", activity.getRigStatus());			
			atts.addAttribute("userCode", activity.getUserCode());
			atts.addAttribute("jobTypeCode", activity.getJobTypeCode());
			atts.addAttribute("activityDescription", activity.getActivityDescription());
			atts.addAttribute("additionalRemarks", activity.getAdditionalRemarks());
			
			atts.addAttribute("depthMdMsl", activity.getDepthMdMsl() == null ? "0" : activity.getDepthMdMsl().toString());
			depthConverter.setBaseValue(activity.getDepthMdMsl() == null?0.0:activity.getDepthMdMsl()); 
			value = depthConverter.formatOutputPrecision();
			atts.addAttribute("depthMdMsl__text", value);
			atts.addAttribute("depthMdMsl_uomSymbol", depthConverter.getUomSymbol());
			
			String companyUid = activity.getLookupCompanyUid();
			if (StringUtils.isNotBlank(companyUid)) {
				if (companyList.containsKey(companyUid)) {
					LookupItem selectedLookup = companyList.get(companyUid);
					atts.addAttribute("company", (String) selectedLookup.getValue());
				} else {
					atts.addAttribute("company", companyUid);
				}
			} else {
				if (StringUtils.isNotBlank(activity.getRootCauseCode())) {
					atts.addAttribute("company", "Unassigned");
				}
			}
			
			String rigInformationUid = reportDaily.getRigInformationUid();
			if (StringUtils.isBlank(rigInformationUid)) {
				rigInformationUid = operation.getRigInformationUid();
			}
			if (StringUtils.isNotBlank(rigInformationUid)) {
				if (rigList.containsKey(rigInformationUid)) {
					LookupItem selectedLookup = rigList.get(rigInformationUid);
					rigInformationUid = (String) selectedLookup.getValue();
				}
			}
			atts.addAttribute("rigName", rigInformationUid);
			
			String holeSize = "";
			if (holeSizeList != null && activity.getHoleSize()!=null) {
				holeSizeConverter.setBaseValue(activity.getHoleSize()==null?0.0:activity.getHoleSize());
				holeSize = holeSizeConverter.formatOutputPrecision();
				lookupItem = holeSizeList.get(holeSize);
				if (lookupItem != null) {
					holeSize = lookupItem.getValue().toString();
				}
				else
					holeSize = "";
			}
			atts.addAttribute("holeSize", holeSize);
			atts.addAttribute("datumName", getDatumDisplayName(operationUomContext, UserSession.getInstance(request), operationUomContext.getDatumUid()));
			
			writer.addElement("Activity", "", atts);
		}
		writer.close();
	}
	
	private void getCostAnalysisList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		
		HQLQuery query = new HQLQuery("select d.operationUid, a.dailyUid, d.dayDate, a.classCode, a.phaseCode, a.taskCode, a.rootCauseCode, sum(a.activityDuration) as duration, o.operationCode " +
				"from Activity a, Daily d, Operation o " +
				"where (a.isDeleted = false or a.isDeleted is null) " +
				"and (d.isDeleted = false or d.isDeleted is null) " +
				"and (o.isDeleted = false or o.isDeleted is null) " +
				"and d.dailyUid=a.dailyUid " +
				"and d.operationUid=o.operationUid " +
				"and (a.isSimop=false or a.isSimop is null) " +
				"and (a.isOffline=false or a.isOffline is null) " +
				"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) ");
		
		String[] operationUids = request.getParameterValues("operationUids");
		if (operationUids != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < operationUids.length; i++) {
				query.queryString += operator + "d.operationUid = :operationUid_" + i;
				query.addParam("operationUid_" + i, operationUids[i]);
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] classCodes = request.getParameterValues("classCodes");
		if (classCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < classCodes.length; i++) {
				String[] classCode = classCodes[i].split("::");
				if (StringUtils.isNotBlank(classCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_c" + i + " and a.classCode = :classCode_" + i + ")";
					query.addParam("operationCode_c" + i, classCode[0]);
					query.addParam("classCode_" + i, classCode[1]);
				} else {
					query.queryString += operator + " (a.classCode = :classCode_" + i + ")";
					query.addParam("classCode_" + i, classCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] phaseCodes = request.getParameterValues("phaseCodes");
		if (phaseCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < phaseCodes.length; i++) {
				String[] phaseCode = phaseCodes[i].split("::");
				if (StringUtils.isNotBlank(phaseCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_p" + i + " and a.phaseCode = :phaseCode_" + i + ")";
					query.addParam("operationCode_p" + i, phaseCode[0]);
					query.addParam("phaseCode_" + i, phaseCode[1]);
				} else { 
					query.queryString += operator + " (a.phaseCode = :phaseCode_" + i + ")";
					query.addParam("phaseCode_" + i, phaseCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] taskCodes = request.getParameterValues("taskCodes");
		if (taskCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < taskCodes.length; i++) {
				String[] taskCode = taskCodes[i].split("::");
				if (StringUtils.isNotBlank(taskCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_t" + i + " and a.taskCode = :taskCode_" + i + ")";
					query.addParam("operationCode_t" + i, taskCode[0]);
					query.addParam("taskCode_" + i, taskCode[1]);
				} else {
					query.queryString += operator + " (a.taskCode = :taskCode_" + i + ")";
					query.addParam("taskCode_" + i, taskCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		String[] rootCauseCodes = request.getParameterValues("rootCauseCodes");
		if (rootCauseCodes != null) {
			String operator = "";
			query.queryString += " and (";
			for (int i = 0; i < rootCauseCodes.length; i++) {
				String[] rootCauseCode = rootCauseCodes[i].split("::");
				if (StringUtils.isNotBlank(rootCauseCode[0])) {
					query.queryString += operator + " (o.operationCode=:operationCode_r" + i + " and a.rootCauseCode = :rootCauseCode_" + i + ")";
					query.addParam("operationCode_r" + i, rootCauseCode[0]);
					query.addParam("rootCauseCode_" + i, rootCauseCode[1]);
				} else {
					query.queryString += operator + " (a.rootCauseCode = :rootCauseCode_" + i + ")";
					query.addParam("rootCauseCode_" + i, rootCauseCode[1]);
				}
				operator = " or ";
			}
			query.queryString += ")";
		}
		
		
		query.queryString += " group by d.operationUid, o.operationCode, a.dailyUid, d.dayDate, a.classCode, a.phaseCode, a.taskCode, a.rootCauseCode ";
		query.queryString += " order by d.operationUid, d.dayDate";
		
		Map<String, LookupItem>rigList = LookupManager.getConfiguredInstance().getLookup("db://RigInformation?key=rigInformationUid&value=rigName", new UserSelectionSnapshot(UserSession.getInstance(request)), null);
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query.queryString, query.paramNames, query.paramValues, qp);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		Map<String, Double> durationList = new HashMap<String, Double>();
		Map<String, ReportDaily> reportDailyMap = new HashMap<String, ReportDaily>();
		
		for (Object[] obj: list) {
			String operationUid = (String)obj[0];
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), operationUid, null, true);
			if (operationName==null) operationName = "Unknown Operation";
			
			operationUomContext.setOperationUid(operationUid);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			
			String dailyUid = (String)obj[1];
			Date dayDate = (Date)obj[2];
			String classCode = (String)obj[3];
			String phaseCode = (String)obj[4];
			String taskCode = (String)obj[5];
			String rootCauseCode = (String)obj[6];
			Double duration = (Double)obj[7];
			if (duration==null) duration = 0.0;
			String operationCode = (String)obj[8];
			
			ReportDaily reportDaily = null;
			if (reportDailyMap.containsKey(dailyUid)) {
				reportDaily = reportDailyMap.get(dailyUid);
			} else {
				reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
				if (reportDaily==null) {
					reportDaily = new ReportDaily();
					reportDaily.setReportNumber("-");
				}
				reportDailyMap.put(dailyUid, reportDaily);
			}
			String reportNumber = reportDaily.getReportNumber();
			String rigInformationUid = reportDaily.getRigInformationUid();
			if (StringUtils.isBlank(rigInformationUid)) {
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				rigInformationUid = operation.getRigInformationUid();
			}

			Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(dailyUid);
			if (dayCost==null) {
				dayCost = 0.0;
				if (reportDaily!=null && reportDaily.getDaycost() != null) dayCost += reportDaily.getDaycost();
			}
			if (reportDaily!=null && reportDaily.getDaycompletioncost() != null) dayCost += reportDaily.getDaycompletioncost();
			
			writer.startElement("costAnalysis");
			writer.addElement("operationUid", operationUid);
			writer.addElement("operationName", operationName);
			writer.addElement("operationCode", operationCode);
			writer.addElement("dailyUid", dailyUid);
			writer.addElement("dayNumber", WellNameUtil.getPaddedStr(reportNumber));
			writer.addElement("dayNumber__text", reportNumber);
			writer.addElement("dayDate__raw", String.valueOf(dayDate.getTime()));
			writer.addElement("dayDate", dateFormat.format(dayDate));
			writer.addElement("classCode", classCode);
			writer.addElement("phaseCode", phaseCode);
			writer.addElement("taskCode", taskCode);
			writer.addElement("rootCauseCode", rootCauseCode);
			durationConverter.setBaseValue(duration);
			writer.addElement("duration", String.valueOf(durationConverter.getConvertedValue()));
			writer.addElement("duration__text", durationConverter.formatOutputPrecision());
			writer.addElement("duration_uomSymbol", durationConverter.getUomSymbol());

			Double dayDuration = 0.0;
			if (durationList.containsKey(dailyUid)) {
				dayDuration = durationList.get(dailyUid);
			} else {
				String strSql = "select sum(activityDuration) from Activity " +
						"where (isDeleted = false or isDeleted is null) " +
						"and (isSimop=false or isSimop is null) " +
						"and (isOffline=false or isOffline is null) " +
						"and dailyUid=:dailyUid " +
						"and (carriedForwardActivityUid='' or carriedForwardActivityUid is null)";
				List<Double> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
				if (rs.size()>0) {
					if (rs.get(0)!=null) dayDuration = rs.get(0);
				}
				durationList.put(dailyUid, dayDuration);
			}
			
			Double costImpact = 0.0;
			if (dayDuration > 0.0) {
				costImpact = dayCost * duration / dayDuration;
			}
			costConverter.setBaseValue(costImpact);
			writer.addElement("costImpact", String.valueOf(costConverter.getConvertedValue()));
			writer.addElement("costImpact__text", costConverter.formatOutputPrecision());
			writer.addElement("costImpact_uomSymbol", costConverter.getUomSymbol());
			
			if (StringUtils.isNotBlank(rigInformationUid)) {
				if (rigList.containsKey(rigInformationUid)) {
					LookupItem selectedLookup = rigList.get(rigInformationUid);
					rigInformationUid = selectedLookup.getValue().toString();
				}
			}
			writer.addElement("rigName", rigInformationUid);
			
			writer.endElement();
		}
		writer.close();
	}
	
	private class HQLQuery {
		
		public String queryString = "";
		public List<String> paramNames = new ArrayList<String>();
		public List<Object> paramValues = new ArrayList<Object>();
		
		public HQLQuery(String queryString) {
			this.queryString = queryString;
		}
		
		public void addParam(String name, Object value){
			this.paramNames.add(name);
			this.paramValues.add(value);
		}
	}
	
	private static double null2Zero(Double value){
		if(value == null) return 0;
		return value;
	}

	public static List convertArrayToList(Object[] array)
	{
		List result = new ArrayList();
		for (Object obj : array)
		{
			result.add(obj);
		}
		return result;
	}
	
	public static Object[] convertStringToObject(String[] input, Boolean isDouble)
	{
	    if (input == null)
	    {
	        return null;
	    }
	    Object[] output = new Object[input.length];
	    for (int i = 0; i < input.length; i++)
	    {
	    	if (isDouble)
	    		output[i]= Double.parseDouble(input[i].toString());
	    	else
	    		output[i] = input[i];
	    }
	    return output;
	}
	
	private void getCanQueryResult(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			this.activityDurationTotal = 0.0;
			datumNameMap = new HashMap<String, String>();
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			String hql = request.getParameter("hql");
			String properties = request.getParameter("properties");
			String[] lookups = request.getParameterValues("lookups");
		
			String[] paramsFields = new String[] {};
			Object[] paramsValues = new Object[] {};
			
			String[] hqlvalues = request.getParameterValues("values");
			String[] hqlparams = request.getParameterValues("paramFields");
			
			ArrayList aV = new ArrayList();
			ArrayList aP = new ArrayList();
			
			if (hqlparams!=null){
				for (String paramField:hqlparams) {						
					if (hqlvalues!=null){
						aV = new ArrayList();
						for (String valueField:hqlvalues) {	
							Integer count = valueField.indexOf(":");
							Boolean isDouble = false;
							
							String[] dataType = StringUtils.split(valueField.substring(0,count),"|");
							if (!"".equals(dataType)){
								if (dataType[0]!=null){
									if ("number".equals(dataType[0].toString())){
										isDouble = true;
									}
								}
							}
												
							valueField = valueField.substring(count+1);																
							String[] splitField = StringUtils.split(StringUtils.trim(valueField), ":");
							if (!"".equals(splitField)) {							
								List a = convertArrayToList(convertStringToObject(splitField, isDouble)); // need to set based on datatype
								aV.add(a);					
							}					
						}										
					}
					aP.add(paramField);
				}
			}
			
			if (aP.size()>0){
				paramsFields = (String[]) aP.toArray(new String [aP.size()]);
				paramsValues = aV.toArray();
			}
			
			
			if (properties==null) properties = "";
			UserSession session = UserSession.getInstance(request);
			String groupUid = session.getCurrentGroupUid();
			
			String[] extraFields = request.getParameterValues("extras");
			Map<String, Map> extraFieldCollection = new HashMap<String, Map>();
			Map<String, String> extraFieldFunction = new HashMap<String, String>();
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			Map<String, String> fieldUom = new HashMap<String, String>();
			
			if (extraFields!=null) {
				for (String extra:extraFields) {
					String[] attr = extra.split(";");
					if (attr.length==3) {
						Map<String, String> extraFieldMap = new HashMap<String, String>();
						String fieldName = attr[0];
						String functionName = attr[1];
						String params = attr[2];
						String[] param = params.split(",");
						for (String field : param) {
							fieldValues.put(field, null);
							extraFieldMap.put(field, field);
						}
						extraFieldFunction.put(fieldName, functionName);
						extraFieldCollection.put(fieldName, extraFieldMap);
					}
				}
			}
			
			CustomFieldUom thisConverter;
			Integer start =  hql.indexOf(" ");
			String value, classname, property, offsetValue, offsetValue_noref;
			Double offsetValue_raw;
			Boolean startFromZero = false;
			Integer end =  hql.toUpperCase().indexOf("FROM ");
			if (end==0) {
				startFromZero = true;
			} else {
				while (hql.toUpperCase().indexOf(" FROM ", end + 1) > 0) {
					end = hql.toUpperCase().indexOf(" FROM ", end + 1);
				}
			}
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql.replace("|", ","), paramsFields, paramsValues, queryProperties);
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			DecimalFormat intFormatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			intFormatter.setMinimumFractionDigits(0);
			intFormatter.setMaximumFractionDigits(0);
			writer.startElement("root");
			if (!startFromZero) {	//select field1, field2, field3 from table1 where...
				String strFields = hql.substring(start,end);
				String[] fields = strFields.split(","); 
				String operationUid = "";
				for (int i=0;i<rs.size(); i++) {
					writer.startElement("datarow");
					
					for (Map.Entry<String, Object> a : fieldValues.entrySet()) {
						fieldValues.put(a.getKey(), null);
					}
					
					if (fields.length>1) { //if more than 1 field
						Object[] obj = (Object[]) rs.get(i);
						Integer index = 0;
						for(String fieldname: fields){
							offsetValue = null;
							offsetValue_noref = null;
							offsetValue_raw = null;
							Integer strIdx = 0;
							fieldname = fieldname.trim();
							while(fieldname.indexOf(" ")>0) {
								strIdx = fieldname.indexOf(" ");
								fieldname = fieldname.substring(strIdx + 1);
								fieldname = fieldname.trim();
							}
							if (fieldname.indexOf(".")>=0) fieldname = fieldname.substring(fieldname.indexOf(".") + 1);
							fieldname = fieldname.trim();
							if (fieldValues.containsKey(fieldname)) {
								fieldValues.put(fieldname, obj[index]);
							}
							value = (obj[index]!=null?obj[index].toString():"");
							if ("operationUid".equals(fieldname)) {
								operationUid = value;
								operationUomContext.setOperationUid(operationUid);
								Datum datum = DatumManager.getDatum(operationUomContext.getDatumUid(), this.getLocale(request), session.getCurrentGroupUid());
								if (datum!=null) {
									writer.addElement("datumName", getDatumDisplayName(operationUomContext, session, datum.getOpsDatumUid()));
									writer.addElement("datumReferencePoint", datum.getDatumReferencePoint());
								}
								
							}
							String lookupValue = value;
							if ("operationUid".equals(fieldname)) {
								value = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, value, null, true);
							} else if (!"".equals(value)) {
								if (properties.indexOf(fieldname+":")>=0) {
									Integer startProp = properties.indexOf(fieldname+":") + fieldname.length() + 1;
									Integer endProp = properties.indexOf(",",startProp);
									property = "";
									if (endProp>0) {
										property = properties.substring(startProp, endProp);
									} else {
										property = properties.substring(startProp);
									}
									if (property.indexOf(".")>0) {
										writer.addElement(fieldname+"_raw", value);
										if (fieldValues.containsKey(fieldname+"_raw")) {
											fieldValues.put(fieldname+"_raw", value);
										}
										classname = property.substring(0, property.indexOf("."));
										property = property.substring(property.indexOf(".") + 1);
										thisConverter = new CustomFieldUom(this.getLocale(request), Class.forName("com.idsdatanet.d2.core.model."+classname), property, operationUomContext);
										CustomFieldUom thisConverterNoLocale = new CustomFieldUom((Locale) null, Class.forName("com.idsdatanet.d2.core.model."+classname), property, operationUomContext);
										try {
											String originalValue = value;
											thisConverter.setBaseValue(Double.parseDouble(originalValue));
											value = thisConverter.formatOutputPrecision();
											if (fieldValues.containsKey(fieldname)) {
												fieldValues.put(fieldname, thisConverter.getConvertedValue());
											}
											
											if (thisConverter.getUOMMapping().isDatumConversion()) {
												thisConverter.setBaseValueFromUserValue(thisConverter.getConvertedValue());
												offsetValue = thisConverter.formatOutputPrecision();
												fieldUom.put(fieldname + "_offset", thisConverter.getUomSymbol());
												writer.addElement(fieldname+"_offset_uomSymbol", thisConverter.getUomSymbol());
												
												offsetValue_noref = thisConverter.formatOutputPrecision();
												fieldUom.put(fieldname + "_offset_noref", thisConverter.getUomSymbol());
												writer.addElement(fieldname+"_offset_noref_uomSymbol", thisConverter.getUomSymbol());
												
												offsetValue_raw = thisConverter.getConvertedValue();
												if (fieldValues.containsKey(fieldname + "_offset")) {
													fieldValues.put(fieldname + "_offset", offsetValue_raw);
												}
												writer.addElement(fieldname+"_uomSymbol", thisConverter.getUomSymbol());
												fieldUom.put(fieldname, thisConverter.getUomSymbol());
											} else {
												fieldUom.put(fieldname, thisConverter.getUomSymbol());
												writer.addElement(fieldname+"_uomSymbol", thisConverter.getUomSymbol());
											}
											
											thisConverterNoLocale.setBaseValue(Double.parseDouble(originalValue));
											lookupValue = thisConverterNoLocale.formatOutputPrecision();
											lookupValue = lookupValue.trim();
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("integer".equals(property)){
										try  {
											Long l = Math.round(Double.parseDouble(value));
											value = intFormatter.format(l);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
										
									} else if ("number".equals(property)){
										try {
											Long l = Math.round(Double.parseDouble(value) * 100);
											Double d = (Double.parseDouble(l.toString()) / 100); 
											value = formatter.format(d);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("month".equals(property)){
										try {
											writer.addElement(fieldname+"_raw", value);
											String[] monthName = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}; 
											value = monthName[Integer.parseInt(value) - 1];
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("date".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
										if (!"".equals(value)) value = df.format(obj[index]);
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd");
										if (!"".equals(value)) value = df.format(obj[index]);
									} else if ("time".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("HH:mm");
										if (!"".equals(value)) value = df.format(obj[index]);
										if (value.startsWith("23:59")) value = "24:00";
									} else if ("datetime".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
										if (!"".equals(value)) value = df.format(obj[index]);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
										if (!"".equals(value)) value = df.format(obj[index]);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
									}
								}
							}
							if (lookups!=null) {
								for (String lookupField:lookups) {
									if (lookupField.indexOf(fieldname+":")>=0) {
										Integer startProp = lookupField.indexOf(fieldname+":") + fieldname.length() + 1;
										String lookupUri = lookupField.substring(startProp);
										Map<String, LookupItem> lookupList = new HashMap<String, LookupItem>();
										if (!"".equals(lookupUri)) {
											String[] splitLookup = StringUtils.split(StringUtils.deleteWhitespace(lookupUri), "|");
											for (String lookupURI : splitLookup) {
												try {
													if (!"".equals(splitLookup)) {
														Map<String, LookupItem> splitLookupList = LookupManager.getConfiguredInstance().getLookup(lookupURI, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
														for (Map.Entry<String, LookupItem> item : splitLookupList.entrySet()) {
															lookupList.put(item.getKey(), item.getValue());
														}
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
											//lookupList = LookupManager.getConfiguredInstance().getLookup(lookupUri, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
										}
										if (lookupList!=null) {
											String[] multiSelect = lookupValue.split("\t");
											String concatString = "";
											for (String s: multiSelect) {
												if (lookupList.get(s)!=null) {
													LookupItem item = lookupList.get(s);
													if (!"".equals(item.getValue().toString())) {
														if (!"".equals(value)) concatString += "\r";
														concatString += item.getValue().toString();
													}
												}
											}
											if (!"".equals(concatString)) value = concatString;
										}
										break;
									}
								}
							}				
							writer.addElement(fieldname, value);
							if (offsetValue!=null) writer.addElement(fieldname + "_offset", offsetValue);
							if (offsetValue_noref!=null) writer.addElement(fieldname + "_offset_noref", offsetValue_noref);
							if (offsetValue_raw!=null) writer.addElement(fieldname + "_offset_raw", offsetValue_raw.toString());
							index++;
						}
					} else {	//if only 1 field
						Object obj2 = (Object) rs.get(i);
						for(String fieldname: fields){
							offsetValue = null;
							offsetValue_noref = null;
							offsetValue_raw = null;
							Integer strIdx = 0;
							fieldname = fieldname.trim();
							while(fieldname.indexOf(" ")>0) {
								strIdx = fieldname.indexOf(" ");
								fieldname = fieldname.substring(strIdx + 1);
								fieldname = fieldname.trim();
							}
							if (fieldname.indexOf(".")>=0) fieldname = fieldname.substring(fieldname.indexOf(".") + 1);
							fieldname = fieldname.trim();
							value = obj2.toString();
							String lookupValue = value;
							if ("operationUid".equals(fieldname)) {
								value = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, value, null, true);
							} else if (!"".equals(value)) {
								if (properties.indexOf(fieldname+":")>=0) {
									Integer startProp = properties.indexOf(fieldname+":") + fieldname.length() + 1;
									Integer endProp = properties.indexOf(",",startProp);
									property = "";
									if (endProp>0) {
										property = properties.substring(startProp, endProp);
									} else {
										property = properties.substring(startProp);
									}
									if (property.indexOf(".")>0) {
										writer.addElement(fieldname+"_raw", value);
										classname = property.substring(0, property.indexOf("."));
										property = property.substring(property.indexOf(".") + 1);
										thisConverter = new CustomFieldUom(this.getLocale(request), Class.forName("com.idsdatanet.d2.core.model."+classname), property);
										CustomFieldUom thisConverterNoLocale = new CustomFieldUom((Locale) null, Class.forName("com.idsdatanet.d2.core.model."+classname), property);
										try {
											//String uom = thisConverter.getUomSymbol();
											
											String originalValue = value;
											thisConverter.setBaseValue(Double.parseDouble(originalValue));
											//value = thisConverter.getFormattedValue();
											value = thisConverter.formatOutputPrecision();
											
											if (thisConverter.getUOMMapping().isDatumConversion()) {
												thisConverter.setBaseValueFromUserValue(thisConverter.getConvertedValue());
												//offsetValue = thisConverter.getFormattedValue() + datumReferencePoint;
												offsetValue = thisConverter.formatOutputPrecision();
												fieldUom.put(fieldname + "_offset", thisConverter.getUomSymbol());
												
												//offsetValue_noref = thisConverter.getFormattedValue();
												offsetValue_noref = thisConverter.formatOutputPrecision();
												fieldUom.put(fieldname + "_offset_noref", thisConverter.getUomSymbol());
												
												offsetValue_raw = thisConverter.getConvertedValue();
												//add datum label 
												//value = value + datumLabel;
												fieldUom.put(fieldname, thisConverter.getUomSymbol());
											} else {
												fieldUom.put(fieldname, thisConverter.getUomSymbol());
											}
											
											/*if ("CurrencyMeasure".equals(thisConverter.getUOMMapping().getUnitTypeID())) {
												value = uom + " " + value.substring(0,value.indexOf(uom)).trim();
											}*/
											thisConverterNoLocale.setBaseValue(Double.parseDouble(originalValue));
											lookupValue = thisConverterNoLocale.getFormattedValue();
											lookupValue = thisConverterNoLocale.formatOutputPrecision();
											//lookupValue = lookupValue.replaceFirst(uom, "");
											lookupValue = lookupValue.trim();
										} catch (Exception ex) {
											//value = thisConverter.getFormattedValue(0.0);
										}
									} else if ("integer".equals(property)){
										try {
											Long l = Math.round(Double.parseDouble(value));
											value = intFormatter.format(l);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("number".equals(property)){
										try {
											Long l = Math.round(Double.parseDouble(value) * 100);
											Double d = (Double.parseDouble(l.toString()) / 100); 
											value = formatter.format(d);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("month".equals(property)){
										try {
											writer.addElement(fieldname+"_raw", value);
											String[] monthName = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}; 
											value = monthName[Integer.parseInt(value) - 1];
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									} else if ("date".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
										if (!"".equals(value)) value = df.format(obj2);
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd");
										if (!"".equals(value)) value = df.format(obj2);
									} else if ("time".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("HH:mm");
										if (!"".equals(value)) value = df.format(obj2);
										if (value.startsWith("23:59")) value = "24:00";
									} else if ("datetime".equals(property)){
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
										if (!"".equals(value)) value = df.format(obj2);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
										if (!"".equals(value)) value = df.format(obj2);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
									}
								}
							}
							if (lookups!=null) {
								for (String lookupField:lookups) {
									if (lookupField.indexOf(fieldname+":")>=0) {
										Integer startProp = lookupField.indexOf(fieldname+":") + fieldname.length() + 1;
										String lookupUri = lookupField.substring(startProp);
										Map<String, LookupItem> lookupList = null;
										if (!"".equals(lookupUri)) {
											lookupList = LookupManager.getConfiguredInstance().getLookup(lookupUri, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
										}
										if (lookupList!=null) {
											String[] multiSelect = lookupValue.split("\t");
											String concatString = "";
											for (String s: multiSelect) {
												if (lookupList.get(s)!=null) {
													LookupItem item = lookupList.get(s);
													if (!"".equals(item.getValue().toString())) {
														if (!"".equals(value)) concatString += "\r";
														concatString += item.getValue().toString();
													}
												}
											}
											if (!"".equals(concatString)) value = concatString;
										}
										break;
									}
								}
							}
							writer.addElement(fieldname, value);
							if (offsetValue!=null) writer.addElement(fieldname + "_offset", offsetValue);
							if (offsetValue_noref!=null) writer.addElement(fieldname + "_offset_noref", offsetValue_noref);
							if (offsetValue_raw!=null) writer.addElement(fieldname + "_offset_raw", offsetValue_raw.toString());
							
						}
					}
					if (extraFieldCollection.size()>0) {
						for (Map.Entry<String, Map> extra : extraFieldCollection.entrySet()) {
							String fieldname = extra.getKey();
							value = "";
							if (extraFieldFunction.containsKey(extra.getKey())) {
								String[] values = this.callExtraFieldFunction(request, operationUomContext, extraFieldFunction.get(extra.getKey()), fieldValues);
								value = values[0];
								if (StringUtils.isNotBlank(values[1])) {
									fieldUom.put(fieldname, values[1]);
									writer.addElement(fieldname+"_uomSymbol", values[1]);
								}
								if (values.length>2) {
									if (StringUtils.isNotBlank(values[2])) {
										writer.addElement(fieldname+"_raw", values[2]);
									}
								}
							}
							writer.addElement(fieldname, value);
						}
					}
					writer.endElement();
				}
			} else {	// direct from object
				for (Object obj: rs) {
					writer.startElement("datarow");
					Map map = BeanUtils.describe(obj);
					for (Object entry: map.entrySet()) {
						Map.Entry<String,String> field = (Map.Entry)entry;
						String fieldname = field.getKey();
						fieldname = fieldname.trim();
						value = field.getValue();
						String lookupValue = value;
						if ("null".equals(value)) value = "";
						if (!"".equals(value)) {
							if (properties.indexOf(fieldname+":")>=0) {
								Integer startProp = properties.indexOf(fieldname+":") + fieldname.length() + 1;
								Integer endProp = properties.indexOf(",",startProp);
								property = "";
								if (endProp>0) {
									property = properties.substring(startProp, endProp);
								} else {
									property = properties.substring(startProp);
								}
								if (property.indexOf(".")>0) {
									writer.addElement(fieldname+"_raw", value);
									classname = property.substring(0, property.indexOf("."));
									property = property.substring(property.indexOf(".") + 1);
									thisConverter = new CustomFieldUom(this.getLocale(request), Class.forName("com.idsdatanet.d2.core.model."+classname), property);
									CustomFieldUom thisConverterNoLocale = new CustomFieldUom((Locale) null, Class.forName("com.idsdatanet.d2.core.model."+classname), property);
									try {
										//String uom = thisConverter.getUomSymbol();
										thisConverter.setBaseValue(Double.parseDouble(value));
										//value = thisConverter.getFormattedValue();
										value = thisConverter.formatOutputPrecision();
										fieldUom.put(fieldname, thisConverter.getUomSymbol());
										/*if ("CurrencyMeasure".equals(thisConverter.getUOMMapping().getUnitTypeID())) {
											value = uom + " " + value.substring(0,value.indexOf(uom)).trim();
										}*/
										thisConverterNoLocale.setBaseValue(Double.parseDouble(value));
										//lookupValue = thisConverterNoLocale.getFormattedValue();
										lookupValue = thisConverterNoLocale.formatOutputPrecision();
										//lookupValue = lookupValue.replaceFirst(uom, "");
										lookupValue = lookupValue.trim();
									} catch (Exception ex) {
										//value = thisConverter.getFormattedValue(0.0);
									}
								} else if ("integer".equals(property)){
									try {
										Long l = Math.round(Double.parseDouble(value));
										value = intFormatter.format(l);
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								} else if ("number".equals(property)){
									try {
										Long l = Math.round(Double.parseDouble(value) * 100);
										Double d = (Double.parseDouble(l.toString()) / 100); 
										value = formatter.format(d);
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								} else if ("month".equals(property)){
									try {
										writer.addElement(fieldname+"_raw", value);
										String[] monthName = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}; 
										value = monthName[Integer.parseInt(value) - 1];
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								} else if ("date".equals(property)){
									if (!"".equals(value)) {
										Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(value);
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
										value = df.format(date);
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd");
										value = df.format(date);
									}
								} else if ("time".equals(property)){
									if (!"".equals(value)) {
										Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(value);
										SimpleDateFormat df = new SimpleDateFormat("HH:mm");
										value = df.format(date);
										if (value.startsWith("23:59")) value = "24:00";
									}
								} else if ("datetime".equals(property)){
									if (!"".equals(value)) {
										Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(value);
										SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
										value = df.format(date);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
										writer.addElement(fieldname+"_label", value);
										df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
										value = df.format(date);
										if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
									}
								}
							}
						}
						if (lookups!=null) {
							for (String lookupField:lookups) {
								if (lookupField.indexOf(fieldname+":")>=0) {
									Integer startProp = lookupField.indexOf(fieldname+":") + fieldname.length() + 1;
									String lookupUri = lookupField.substring(startProp);
									Map<String, LookupItem> lookupList = null;
									if (!"".equals(lookupUri)) {
										lookupList = LookupManager.getConfiguredInstance().getLookup(lookupUri, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
									}
									if (lookupList!=null) {
										String[] multiSelect = lookupValue.split("\t");
										String concatString = "";
										for (String s: multiSelect) {
											if (lookupList.get(s)!=null) {
												LookupItem item = lookupList.get(s);
												if (!"".equals(item.getValue().toString())) {
													if (!"".equals(value)) concatString += "\r";
													concatString += item.getValue().toString();
												}
											}
										}
										if (!"".equals(concatString)) value = concatString;
									}
									break;
								}
							}
						}
						writer.addElement(fieldname, value);
					}
					writer.endElement();
				}
				
			}
			
			for (Map.Entry<String, String> entry : fieldUom.entrySet()) {
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("fieldName", entry.getKey());
				attr.addAttribute("uomSymbol", entry.getValue());
				writer.addElement("UnitSymbol", "", attr);
			}
			operationUomContext.setNullDatumBehaviour(1);
			operationUomContext.setNullUomTemplateBehaviour(1);
			writer.close();
		}catch(Exception e){
			e.printStackTrace();
			SimpleXmlResponse.send(response, false, "error", "CAN QUERY FAILED - " + e.getMessage());
		}
	}
	
	private void getCanQueryResultSpecial(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			UserSession session = UserSession.getInstance(request);
			String groupUid = session.getCurrentGroupUid(); 
			
			CustomFieldUom thisConverter;
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			DecimalFormat intFormatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			intFormatter.setMinimumFractionDigits(0);
			intFormatter.setMaximumFractionDigits(0);
			
			SimpleXMLElement xml = SimpleXMLElement.loadXMLString(request.getParameter("xmlHql"));
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for(Iterator i = xml.getChild().iterator(); i.hasNext(); ){
				SimpleXMLElement hql = (SimpleXMLElement) i.next();
				try {
					String property = hql.getAttribute("property");
					String datatype = hql.getAttribute("datatype");
					datatype = datatype.toLowerCase();
					List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql.getText());
					if (rs.size()>0) {
						Object a = rs.get(0);
						String value = a.toString();
						if (property.indexOf(".")>0) {
							Map<String, Object> attributes = new HashMap<String, Object>();
							String classname = property.substring(0, property.indexOf("."));
							property = property.substring(property.indexOf(".") + 1);
							thisConverter = new CustomFieldUom(this.getLocale(request), Class.forName("com.idsdatanet.d2.core.model."+classname), property);
							try {
								thisConverter.setBaseValue(Double.parseDouble(value));
								value = thisConverter.getFormattedValue();
								//Double convertedValue = thisConverter.getConvertedValue();
								attributes.put("convertedValue", thisConverter.getConvertedValue());
								attributes.put("uomSymbol", thisConverter.getUomSymbol());
							} catch (Exception ex) {
								value = thisConverter.getFormattedValue(0.0);
							}
							writer.addElement(hql.getTagName(), value, attributes);
						} else {
							if ("number".equals(datatype)) {
								try {
									Long l = Math.round(Double.parseDouble(value) * 100);
									Double d = (Double.parseDouble(l.toString()) / 100); 
									value = formatter.format(d);
								} catch (Exception ex) {
									ex.printStackTrace();
								}
							} else if ("integer".equals(datatype)){
								try {
									Long l = Math.round(Double.parseDouble(value));
									value = intFormatter.format(l);
								} catch (Exception ex) {
									ex.printStackTrace();
								}
							} else if ("month".equals(datatype)){
								try {
									writer.addElement(hql.getTagName()+"_raw", value);
									String[] monthName = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}; 
									value = monthName[Integer.parseInt(value) - 1];
								} catch (Exception ex) {
									ex.printStackTrace();
								}
							} else if ("date".equals(datatype)){
								try {
									SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
									value = df.format(a);
									writer.addElement(hql.getTagName()+"_label", value);
									df = new SimpleDateFormat("yyyy-MM-dd");
									value = df.format(a);
								} catch (Exception ex) {
									value = "";
								}
								writer.addElement(hql.getTagName(), value);
							} else if ("time".equals(datatype)){
								try {
									SimpleDateFormat df = new SimpleDateFormat("HH:mm");
									value = df.format(a);
									if (value.startsWith("23:59")) value = "24:00";
								} catch (Exception ex) {
									value = "";
								}
								writer.addElement(hql.getTagName(), value);
							} else if ("datetime".equals(datatype)){
								try {
									SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
									value = df.format(a);
									if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
									writer.addElement(hql.getTagName()+"_label", value);
									df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
									value = df.format(a);
									if (value.endsWith("23:59")) value.replaceAll("23:59", "24:00");
								} catch (Exception ex) {
									value = "";
								}
								writer.addElement(hql.getTagName(), value);
							} else {
								if ("operationUid".equals(hql.getTagName())) {
									value = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, value, null, true);
								}
								writer.addElement(hql.getTagName(), value);
							}
						}
					}
				} catch (Exception ex) {
					writer.addElement(hql.getTagName(), "");
				}
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			SimpleXmlResponse.send(response, false, "error", "CAN QUERY FAILED - " + e.getMessage());
		}
	}
	
	private void savePDF(HttpServletRequest request, HttpServletResponse response, String name) throws Exception {
		int i = 0;
		int k = 0;
		int maxLength = request.getContentLength();
		byte[] bytes = new byte[maxLength];
		//String method = request.getParameter("method");
		//String name = request.getParameter("name");
		ServletInputStream si = request.getInputStream();
		while (true)
		{
			k = si.read(bytes,i,maxLength);
			i += k;
			if (k <= 0)
				break;
		}
		if (bytes != null)
		{
			ServletOutputStream stream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			response.setHeader("Content-Disposition",METHOD_SAVE_PDF + ";filename=" + name);
			stream.write(bytes);
			stream.flush();
			stream.close();
		}
		else
		{
			response.setContentType("text");
			response.getWriter().write("bytes is null");
		}
	}
	
	private void processFileOutput(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			String contentType = request.getParameter("contentType");
			String data = request.getParameter("data");
			String isBase64 = request.getParameter("isBase64");
			String filename = request.getParameter("filename");
			String asAttachment = request.getParameter("attachment");
			String compressed = request.getParameter("compressed");
			if (isBase64==null) isBase64 = "false";
			if (compressed==null) compressed = "false";
			if (data==null) data = "";
			if (contentType==null) contentType = "text/plain";
			if (filename==null) filename = "output";
			if (asAttachment==null) asAttachment = "1";
			
			response.setContentType(contentType);
			response.setCharacterEncoding("utf-8");
			if (asAttachment.equals("1")) response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
			if ("true".equals(isBase64)) {
				ServletOutputStream out = response.getOutputStream();
				Base64 bc = new Base64();
				byte[] b = bc.decode(data.getBytes());
				
				if ("true".equals(compressed)) {
					// Create the decompressor and give it the data to compress
					Inflater decompressor = new Inflater();
					decompressor.setInput(b);
					
					// Create an expandable byte array to hold the decompressed data
				    ByteArrayOutputStream bos = new ByteArrayOutputStream(b.length);

				    // Decompress the data
				    byte[] buf = new byte[1024];
				    while (!decompressor.finished()) {
				        try {
				            int count = decompressor.inflate(buf);
				            bos.write(buf, 0, count);
				        } catch (DataFormatException e) {
				        }
				    }
				    try {
				        bos.close();
				    } catch (IOException e) {
				    }
					b = bos.toByteArray();
				}
				out.write(b, 0, b.length);
				out.close();
			} else {
				response.getWriter().write(data);
			}
			
		} catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
	    }
	}
	
	public void getDatSettings(HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.readXMLFile(response, "WEB-INF/config/dat_settings.xml");
	}
	
	public void getCanQueryOptions(HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.readXMLFile(response, "WEB-INF/config/canquery.xml");
	}
	
	private void readXMLFile(HttpServletResponse response, String filePath) throws Exception {
		filePath = this.servletContext.getRealPath("/") + filePath;
		try{
			FileInputStream fstream = new FileInputStream(filePath);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			response.setContentType("text/xml");
			while ((strLine = br.readLine()) != null)   {
				response.getWriter().write(strLine+"\n");
			}
			in.close();
		} catch (Exception e){//Catch exception if any
			SimpleXmlResponse.send(response, false, "error", "Load Settings: " + e.getMessage());
	    }
	}
	
	private String[] callExtraFieldFunction(HttpServletRequest request, OperationUomContext operationUomContext, String functionName, Map<String, Object> fieldValues) {
		String[] value = {"",""};
		if (StringUtils.isBlank(functionName)) return value;
		
		if ("getRigInformationUid_unwantedEvent".equals(functionName)) {
			return this.getRigInformationUid_unwantedEvent(request, (String) fieldValues.get("operationUid"), (Date) fieldValues.get("eventDatetime")); //OK 
		} else if ("getAverageROP_bit".equals(functionName)) {
			return this.getAverageROP_bit(request, operationUomContext, (String) fieldValues.get("bharunUid")); //OK
		} else if ("getRigInformationUidByDaily".equals(functionName)) {
			return this.getRigInformationUidByDaily(request, (String) fieldValues.get("dailyUid")); //OK
		} else if ("getReportNumberByDaily".equals(functionName)) {
			return this.getReportNumberByDaily(request, (String) fieldValues.get("dailyUid")); //OK
		} else if ("getDrillingMetricByDay".equals(functionName)) {
			return this.getMetricByDay(request, operationUomContext, (Double) fieldValues.get("diffDepth_offset"), Double.parseDouble(fieldValues.get("duration_raw").toString())); //OK
		} else if ("getAltMetricByDay".equals(functionName)) {
			return this.getMetricByDay(request, operationUomContext, (Double) fieldValues.get("maxDepth"), Double.parseDouble(fieldValues.get("duration_raw").toString())); //OK
		} else if ("getCurrentDayCost".equals(functionName)) {
			return this.getCurrentDayCost(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("reportType")); //ok
		} else if ("getCummulativeCost".equals(functionName)) {
			return getCummulativeCost(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("reportType")); //ok 
		} else if ("getCummulativeTangibleCost".equals(functionName)) {
			return getCummulativeTangibleCost(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("reportType"), (String) fieldValues.get("operationUid")); //ok 
		} else if ("getTotalDailyCost".equals(functionName)) {
			return getTotalDailyCost(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("reportType"), (String) fieldValues.get("operationUid")); //OK
		} else if ("getTotalCumulativeCost".equals(functionName)) {
			return getTotalCumulativeCost(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("reportType"), (String) fieldValues.get("operationUid")); //OK
		} else if ("getLastHoleSizeFromActivity".equals(functionName)) {
			return getLastHoleSizeFromActivity(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid")); //OK
		}else if ("getCumCostForOperation".equals(functionName)) {
			return getCumCostForOperation(request, operationUomContext, (String) fieldValues.get("operationUid")); //OK
		}else if ("getAfeCost".equals(functionName)) {
			return getAfeCost(request, operationUomContext, (String) fieldValues.get("operationUid")); //OK
		} else if ("getNptPercent".equals(functionName)) {
			return getNptPercent(request, (String) fieldValues.get("operationUid")); //OK 
		} else if ("getActivityDurationRunningTotal".equals(functionName)) {
			return this.getActivityDurationRunningTotal(request, operationUomContext, (String) fieldValues.get("operationUid"), (Double) fieldValues.get("activityDuration")); //OK
		} else if ("getFormationForDepth".equals(functionName)) {
			return this.getFormationForDepth(request, operationUomContext, (String) fieldValues.get("operationUid"), (Double) fieldValues.get("topMdMsl_offset"), (Double) fieldValues.get("bottomMdMsl_offset")); //OK
		}else if ("getCostPerMeasuredDepth".equals(functionName)) {
			return this.getCostPerMeasuredDepth(request, (String) fieldValues.get("operationUid"),  (Date) fieldValues.get("startDateF"), (Date) fieldValues.get("startDateT"));
		}else if ("getDepthPerDay".equals(functionName)) {
			return this.getDepthPerDay(request, (String) fieldValues.get("operationUid"), (Date) fieldValues.get("startDateF"), (Date) fieldValues.get("startDateT"));
		}else if ("getDrillingContractorNPT".equals(functionName)) {
			return this.getDrillingContractorNPT(request, (String) fieldValues.get("operationUid"), (Date) fieldValues.get("startDateF"), (Date) fieldValues.get("startDateT"));
		}else if ("getNptPercentWithDateRange".equals(functionName)) {
			return this.getNptPercentWithDateRange(request, (String) fieldValues.get("operationUid"), (Date) fieldValues.get("startDateF"), (Date) fieldValues.get("startDateT"));
		} else if ("getPaddedReportNumber".equals(functionName)) {
			return this.getPaddedStr(request, (String) fieldValues.get("reportNumber_label"));
		}else if ("getDurationBasedOnUserCodeCMIC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "CMIC");
		}else if ("getDurationBasedOnUserCodeCMPC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "CMPC");
		}else if ("getDurationBasedOnUserCodeCMSC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "CMSC");
		}else if ("getDurationBasedOnUserCodeEVAL".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "EVAL");
		}else if ("getDurationBasedOnUserCodeIBH".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "IBH");
		}else if ("getDurationBasedOnUserCodeITDWT".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "ITDWT");
		}else if ("getDurationBasedOnUserCodeLOT".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "LOT");
		}else if ("getDurationBasedOnUserCodeNDB".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "NDB");
		}else if ("getDurationBasedOnUserCodeNUB".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "NUB");
		}else if ("getDurationBasedOnUserCodePACMP".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "PACMP");
		}else if ("getDurationBasedOnUserCodePAOTH".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "PAOTH");
		}else if ("getDurationBasedOnUserCodePTDWT".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "PTDWT");
		}else if ("getDurationBasedOnUserCodeRRIC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "RRIC");
		}else if ("getDurationBasedOnUserCodeRRPC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "RRPC");
		}else if ("getDurationBasedOnUserCodeRRSC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "RRSC");
		}else if ("getDurationBasedOnUserCodeRC".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "RC");
		}else if ("getDurationBasedOnUserCodeSCWT".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "SCWT");
		}else if ("getDurationBasedOnUserCodeTOLDP".equals(functionName)) {
			return this.getDurationBasedOnUserCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "TOLDP");
		}else if ("getPhaseTotalByCodePS".equals(functionName)) {
			return this.getPhaseTotalByCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "PS");
		}else if ("getPhaseTotalByCodeEP".equals(functionName)) {
			return this.getPhaseTotalByCode(request,operationUomContext, (String) fieldValues.get("operationUid"), "EP");
		}else if ("getCasingCost".equals(functionName)) {
			return this.getCasingCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getCasingCementCost".equals(functionName)) {
			return this.getCasingCementCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getHolesCost".equals(functionName)) {
			return this.getHolesCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getPhMudCost".equals(functionName)) {
			return this.getPhMudCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getCompletionCost".equals(functionName)) {
			return this.getCompletionCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getShMudCost".equals(functionName)) {
			return this.getShMudCost(request,operationUomContext, (String) fieldValues.get("operationUid"));		
		}else if ("getTotalWellCost".equals(functionName)) {
			return this.getTotalWellCost(request,operationUomContext, (String) fieldValues.get("operationUid"));
		}else if ("getActivityDurationByPhaseSC".equals(functionName)) {
			return this.getActivityDurationByPhase(request,operationUomContext, (String) fieldValues.get("operationUid"),"SC");
		}else if ("getActivityDurationByPhaseSH".equals(functionName)) {
			return this.getActivityDurationByPhase(request,operationUomContext, (String) fieldValues.get("operationUid"),"SH");	
		}else if ("getSpudToTdDuration".equals(functionName)) {
			return this.getSpudToTdDuration(request,operationUomContext, (Date) fieldValues.get("spudDate"),(Date) fieldValues.get("tdDateTdTime"));
		}else if ("getSpudToRigUpDuration".equals(functionName)) {
			return this.getSpudToTdDuration(request,operationUomContext, (Date) fieldValues.get("spudDate"),(Date) fieldValues.get("rigreleaseDateRigreleaseTime"));
		}else if ("getSpudToTdNoCsg".equals(functionName)) {
			return this.getSpudToTdNoCsg(request,operationUomContext,(Date) fieldValues.get("spudDate"),(Date) fieldValues.get("tdDateTdTime"), (String) fieldValues.get("operationUid"), "SC"  );
		}else if ("getTotalEvalBeforeTD".equals(functionName)) {
			return this.getTotalEvalBeforeTD(request,operationUomContext,(Date) fieldValues.get("spudDate"),(Date) fieldValues.get("tdDateTdTime"), (String) fieldValues.get("operationUid"));
		}else if ("getTotalEvalAfterTD".equals(functionName)) {
			return this.getTotalEvalAfterTD(request,operationUomContext,(Date) fieldValues.get("tdDateTdTime"), (String) fieldValues.get("operationUid"));
		}else if ("getNptOperation".equals(functionName)) {
			return this.getNptDuration(request,operationUomContext,(String) fieldValues.get("operationUid"),"TP");
		}else if ("getNptNoOperation".equals(functionName)) {
			return this.getNptDuration(request,operationUomContext,(String) fieldValues.get("operationUid"),"TU");
		}else if ("getRigMoveTime".equals(functionName)) {
			return this.getRigMoveTime(request,operationUomContext,(Date) fieldValues.get("rigOnHireDate"),(Date) fieldValues.get("onlocDateOnlocTime"));
		}else if ("getRigUpTime".equals(functionName)) {
			return this.getRigUpTime(request,operationUomContext,(Date) fieldValues.get("onlocDateOnlocTime"),(Date) fieldValues.get("spudDate"));
		}else if ("getD10kIADC".equals(functionName)) {
			return this.get10kftIADCTOB(request,operationUomContext,(String) fieldValues.get("operationUid"),"IADC");
		}else if ("getD10kTOB".equals(functionName)) {
			return this.get10kftIADCTOB(request,operationUomContext,(String) fieldValues.get("operationUid"),"TOB");
		}else if ("gotLTI".equals(functionName)) {
			return this.gotLTI(request,operationUomContext,(String) fieldValues.get("operationUid"));
		} else if ("getAfeVariance".equals(functionName)) {
			return this.getAfeVariance(request,operationUomContext,(String) fieldValues.get("operationUid"));
		} else if ("getPOBSummaryByRigMonthRecordedDays".equals(functionName)) {
			return this.getPOBSummaryByRigMonthRecordedDays(request, (String) fieldValues.get("operationUid"), (Integer) fieldValues.get("reportYear"), (Integer) fieldValues.get("reportMonth"));
		}else if ("getPOBSummaryByRigMonthAverageRecordedDays".equals(functionName)) {
			return this.getPOBSummaryByRigMonthAverageRecordedDays(request, (String) fieldValues.get("operationUid"), (Integer) fieldValues.get("reportYear"), (Integer) fieldValues.get("reportMonth"), Double.parseDouble(fieldValues.get("sumPax").toString()));
		} else if ("getPOBPersonnelByWellAndCompanyRecordedDays".equals(functionName)) {
			return this.getPOBPersonnelByWellAndCompanyRecordedDays(request, (String) fieldValues.get("operationUid"), (String) fieldValues.get("companyName"));
		}else if ("getPOBPersonnelByWellAndCompanyAverageRecordedDays".equals(functionName)) {
			return this.getPOBPersonnelByWellAndCompanyAverageRecordedDays(request, (String) fieldValues.get("operationUid"), Double.parseDouble(fieldValues.get("sumPax").toString()), (String) fieldValues.get("companyName"));
		}else if ("getPOBSummaryByCompanyMonthRecordedDays".equals(functionName)) {
			return this.getPOBSummaryByCompanyMonthRecordedDays(request, (String) fieldValues.get("operationUid"), (Integer) fieldValues.get("reportYear"), (Integer) fieldValues.get("reportMonth"), (String) fieldValues.get("companyName"));
		}else if ("getPOBSummaryByCompanyMonthAverageRecordedDays".equals(functionName)) {
			return this.getPOBSummaryByCompanyMonthAverageRecordedDays(request, (String) fieldValues.get("operationUid"), (Integer) fieldValues.get("reportYear"), (Integer) fieldValues.get("reportMonth"), Double.parseDouble(fieldValues.get("sumPax").toString()), (String) fieldValues.get("companyName"));
		}else if ("getTotalMudVolumesPerDay_vol".equals(functionName)) {
			return this.getTotalMudVolumesPerDay(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), (String) fieldValues.get("mudVolumesUid"), "vol", null);
		}else if ("getTotalMudVolumesPerDay_loss_surface".equals(functionName)) {
			return this.getTotalMudVolumesPerDay(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), (String) fieldValues.get("mudVolumesUid"), "loss", "loss_surface");
		}else if ("getTotalMudVolumesPerDay_loss_dumped".equals(functionName)) {
			return this.getTotalMudVolumesPerDay(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), (String) fieldValues.get("mudVolumesUid"), "loss", "loss_dumped");
		}else if ("getTotalMudVolumesPerDay_loss_dh".equals(functionName)) {
			return this.getTotalMudVolumesPerDay(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), (String) fieldValues.get("mudVolumesUid"), "loss", "loss_dh");
		}else if ("getCummulativeMudVolumes_loss_surface".equals(functionName)) {
			return this.getCummulativeMudVolumes(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), "loss", "loss_surface");
		}else if ("getCummulativeMudVolumes_loss_dumped".equals(functionName)) {
			return this.getCummulativeMudVolumes(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), "loss", "loss_dumped");
		}else if ("getCummulativeMudVolumes_loss_dh".equals(functionName)) {
			return this.getCummulativeMudVolumes(request, operationUomContext, (String) fieldValues.get("dailyUid"), (String) fieldValues.get("operationUid"), "loss", "loss_dh");
		} else if ("getDvdCumP50Duration".equals(functionName)) {
			return this.getDvdCumP50Duration(request, operationUomContext, (String) fieldValues.get("operationPlanPhaseUid"));
		} else if (functionName.indexOf("getWellPhaseCost[",0)==0) {
			return this.getWellPhaseCost(request, operationUomContext, (String) fieldValues.get("operationUid"), functionName);
		}
		return value;
	}
	
	//Query POB - Summary by Company/Month
	private String[] getPOBSummaryByCompanyMonthRecordedDays(HttpServletRequest request, String operationUid, Integer reportYear, Integer reportMonth, String companyName) {
		String recordedDays = getRecordedDays(operationUid, reportYear, reportMonth, companyName);
		return new String[] {recordedDays, ""};
	}
	
	private String[] getPOBSummaryByCompanyMonthAverageRecordedDays(HttpServletRequest request, String operationUid, Integer reportYear, Integer reportMonth, Double sumPax, String companyName) {
		double average = 0.00;
		String averagePax = "";
		DecimalFormat formatter;
		
		try {
			formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);	
			String averageRecordedDays = getRecordedDays(operationUid, reportYear, reportMonth, companyName);
			
			if (!"".equals(averageRecordedDays) && sumPax != null && !"0".equals(averageRecordedDays)){
				average = Double.parseDouble(sumPax.toString()) / Double.parseDouble(averageRecordedDays);
				average = Math.round(average * 100) / 100.0;
				averagePax = formatter.format(average);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return new String[] {averagePax, ""};
	}
	
	//Query POB - POB Summary by Rig/Month
	private String[] getPOBPersonnelByWellAndCompanyRecordedDays(HttpServletRequest request, String operationUid, String companyName) {
		String recordedDays = getRecordedDays(operationUid, null, null, companyName);
		return new String[] {recordedDays, ""};
	}
	
	private String[] getPOBPersonnelByWellAndCompanyAverageRecordedDays(HttpServletRequest request, String operationUid, Double sumPax, String companyName) {
		double average = 0.00;
		String averagePax = "";
		DecimalFormat formatter;
		
		try {
			formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			String averageRecordedDays = getRecordedDays(operationUid, null, null, companyName);
			
			if (!"".equals(averageRecordedDays) && sumPax != null && !"0".equals(averageRecordedDays)){
				average = Double.parseDouble(sumPax.toString()) / Double.parseDouble(averageRecordedDays);
				average = Math.round(average * 100) / 100.0;
				averagePax = formatter.format(average);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {averagePax, ""};
	}
	
	//Query POB - Personnel By Well and Company
	private String[] getPOBSummaryByRigMonthRecordedDays(HttpServletRequest request, String operationUid, Integer reportYear, Integer reportMonth) {
		String recordedDays = getRecordedDays(operationUid, reportYear, reportMonth, null);
		return new String[] {recordedDays, ""};
	}
	
	private String[] getPOBSummaryByRigMonthAverageRecordedDays(HttpServletRequest request, String operationUid, Integer reportYear, Integer reportMonth, Double sumPax) {
		double average = 0.00;
		String averagePax = "";
		DecimalFormat formatter;
		
		try {
			formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);		
			String averageRecordedDays = getRecordedDays(operationUid, reportYear, reportMonth, null);

			if (!"".equals(averageRecordedDays) && sumPax != null && !"0".equals(averageRecordedDays)){
				average = Double.parseDouble(sumPax.toString()) / Double.parseDouble(averageRecordedDays);
				average = Math.round(average * 100) / 100.0;
				averagePax = formatter.format(average);
			}					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new String[] {averagePax, ""};
	}
	
	public String getRecordedDays(String operationUid, Integer reportYear, Integer reportMonth, String companyName){
		String recordedDays = "";
		String strSql = "";
		try {
			if (companyName != null){
				if (reportYear != null && reportMonth != null){
					strSql = "SELECT COUNT(DISTINCT rd.reportDatetime) FROM PersonnelOnSite p, ReportDaily rd, LookupCompany c WHERE (p.isDeleted=false or p.isDeleted is null) AND (rd.isDeleted=false or rd.isDeleted is null) AND (c.isDeleted=false or c.isDeleted is null) AND rd.reportType!='DGR' AND rd.dailyUid=p.dailyUid AND c.lookupCompanyUid=p.crewCompany AND p.operationUid=:operationUid AND c.companyName=:companyName AND year(rd.reportDatetime)=:reportYear AND month(rd.reportDatetime)=:reportMonth";
					List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "reportYear", "reportMonth", "companyName"}, new Object[] {operationUid, reportYear, reportMonth, companyName });
					if (rs.size()>0) {
						recordedDays = rs.get(0).toString() ;
					}
				}
				else 
				{
					strSql = "SELECT COUNT(DISTINCT rd.reportDatetime) FROM PersonnelOnSite p, Daily d, LookupCompany c, Operation o, ReportDaily rd WHERE (p.isDeleted=false or p.isDeleted is null) AND (d.isDeleted=false or d.isDeleted is null) AND (o.isDeleted=false or o.isDeleted is null) AND (rd.isDeleted=false or rd.isDeleted is null) AND p.dailyUid=d.dailyUid AND d.dailyUid=rd.dailyUid AND c.lookupCompanyUid=p.crewCompany AND p.operationUid=o.operationUid AND p.operationUid=:operationUid AND c.companyName=:companyName";
					List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "companyName"}, new Object[] {operationUid, companyName });
					if (rs.size()>0) {
						recordedDays = rs.get(0).toString() ;
					}
				}
			}
			else
			{
				strSql = "SELECT COUNT(DISTINCT rd.reportDatetime) FROM PersonnelOnSite p, ReportDaily rd WHERE (p.isDeleted=false or p.isDeleted is null) AND (rd.isDeleted=false or rd.isDeleted is null) AND rd.reportType!='DGR' AND p.dailyUid=rd.dailyUid and p.operationUid=:operationUid and year(rd.reportDatetime)=:reportYear and month(rd.reportDatetime)=:reportMonth";
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "reportYear", "reportMonth"}, new Object[] {operationUid, reportYear, reportMonth });
				if (rs.size()>0) {
					recordedDays = rs.get(0).toString() ;
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return recordedDays;
	}
	
	private String[] gotLTI(HttpServletRequest request,
			OperationUomContext operationUomContext, String opsid) {
		String output ="0";
		
		try{
		String hsequery = "SELECT COUNT(hseIncidentUid) FROM HseIncident WHERE (isDeleted=false or isDeleted is null) AND operationUid=:opsid AND incidentCategory IN ('Lost Time Incident','Medical Treatment Incident')";
		List hse = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hsequery, new String[]{"opsid"}, new Object[]{opsid});
		if (!hse.isEmpty()){
			String value = hse.get(0).toString() ;
			if ("0".equals(value)){
				output = "N";
			}else{
				output = "Y";
			}
		}		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,""} ;	
		
	}

	private String[] get10kftIADCTOB(HttpServletRequest request,
			OperationUomContext operationUomContext, String opsid, String type) {
		String output = "0.0";
		try {	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);	
			String qsiadctob = "SELECT br.operationUid ,SUM(bds.progress),SUM(bds.IADCDuration),SUM(bds.duration)FROM Bharun br, BharunDailySummary bds " +
								"WHERE (br.isDeleted=false or br.isDeleted is null) " +
								"AND (bds.isDeleted=false or bds.isDeleted is null) " +
								"AND bds.bharunUid = br.bharunUid " +
								"AND br.operationUid = :opsid " +
								"GROUP BY br.operationUid";
			List <Object[]> iadctobvalue = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(qsiadctob, new String[]{"opsid"},new Object[]{opsid},qp);
			
			double footage = 0.0;
			double IADCdur = 0.0;
			double onBtmDur = 0.0;
			double IADCp10kvalue = 0.0;
			double TOBp10kvalue = 0.0;
			for (Object[] iadctobobj:iadctobvalue){
				if (iadctobobj[1] != null){
				footage =  footage + (Double) iadctobobj[1]/0.30480000000000003340608;
				}
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), BharunDailySummary.class, "IADCDuration", operationUomContext);
				if (iadctobobj[2] != null){			
				thisConverter.setBaseValue((Double) iadctobobj[2]);
				IADCdur = IADCdur + thisConverter.getConvertedValue();
				}
				thisConverter = new CustomFieldUom(this.getLocale(request), BharunDailySummary.class, "duration",operationUomContext);
				if (iadctobobj[3] != null){
				thisConverter.setBaseValue((Double) iadctobobj[3]);
				onBtmDur = onBtmDur + thisConverter.getConvertedValue();
				
				}
			}
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			if (type == "IADC"){
				IADCp10kvalue = IADCp10kvalue + (1/((footage/IADCdur)*24)*10000);
				output = formatter.format(IADCp10kvalue);
			}else if (type =="TOB"){
				TOBp10kvalue = TOBp10kvalue + (1/((footage/onBtmDur)*24)*10000);
				output = formatter.format(TOBp10kvalue);
			}
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,""} ;		
				
		
		
		
	}

	private String[] getRigUpTime(HttpServletRequest request,
			OperationUomContext operationUomContext, Date onlocDateOnlocTime, Date spudDate) {
		double riguptime = 0.0;
		String output = "0.0";
		String UOM = "";
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
			riguptime =  CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(onlocDateOnlocTime,spudDate);
			UOM = thisConverter.getUomSymbol();
			thisConverter.setBaseValue(riguptime);
			
			output = thisConverter.formatOutputPrecision(); ;
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM} ;
	}

	private String[] getRigMoveTime(HttpServletRequest request,
			OperationUomContext operationUomContext, Date rigOnHireDate, Date onlocDateOnlocTime) {
		double rigmovetime = 0.0;
		String output = "0.0";
		String UOM = "";
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
			rigmovetime =  CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(rigOnHireDate,onlocDateOnlocTime);
			UOM = thisConverter.getUomSymbol();
			thisConverter.setBaseValue(rigmovetime);
			
			output = thisConverter.formatOutputPrecision(); ;
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM} ;
	}

	private String[] getCompletionCost(HttpServletRequest request,
			OperationUomContext operationUomContext, String opsid) {
		String totalHoleCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('PC','PC1','PC2','PC3','C','CTB')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalHoleCost = thisConverter.formatOutputPrecision();
			}else{
				String phasetotalquery2 = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
						"WHERE (isDeleted=false or isDeleted is null) " +	
						"AND operationUid = :opsid AND phase_code IN ('PA','ABN')";	

				 List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery2,new String[]{"opsid"}, new Object[]{opsid},qp);
				 if (results.get(0) != null){					 
					thisConverter.setBaseValue(Double.parseDouble(results.get(0).toString()));
					totalHoleCost = thisConverter.formatOutputPrecision();
				 }
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return new String[] {totalHoleCost,UOM} ;	
	}

	private String[] getNptDuration(HttpServletRequest request,
			OperationUomContext operationUomContext, String opsid,
			String classcode) {
		 String output = "0.0";
			try{
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
				String totalEvalBeforeTDQuery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " +
												"WHERE (a.isDeleted=false or a.isDeleted is null) " +
												"AND (d.isDeleted=false or d.isDeleted is null) " +
												"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
												"AND (a.isSimop=false or a.isSimop is null) " +
												"AND (a.isOffline=false or a.isOffline is null) " +
												"AND a.operationUid =:opsid " + 
												"AND a.internalClassCode =:classcode " +
												"AND a.dailyUid = d.dailyUid ";
				List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","classcode"}, new Object[]{opsid,classcode},qp);
				if (totalEvalBeforeTD.get(0) != null){
					
					 thisConverter.setBaseValue(Double.parseDouble(totalEvalBeforeTD.get(0).toString()));
					 output = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e) {
				
				e.printStackTrace();
			}
			return new String[]{output,""};
	}

	private String[] getTotalEvalAfterTD(HttpServletRequest request,
			OperationUomContext operationUomContext, Date tddatetime,
			String opsid) {
		 String output = "0.0";
			String UOM = "";
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycurve", operationUomContext);
			UOM = thisConverter.getUomSymbol(); 
			String totalEvalBeforeTDQuery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " +
											"WHERE (a.isDeleted=false or a.isDeleted is null) " +
											"AND (d.isDeleted=false or d.isDeleted is null) " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
											"AND (a.isSimop=false or a.isSimop is null) " +
											"AND (a.isOffline=false or a.isOffline is null) " +
											"AND a.operationUid =:opsid " + 
											"AND a.phaseCode IN ('EP','EI') " +
											"AND a.dailyUid = d.dailyUid " +
											"AND d.dayDate >=:tddatetime";
			List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","tddatetime"}, new Object[]{opsid,tddatetime},qp);
			if (totalEvalBeforeTD.get(0) != null){
				
				 thisConverter.setBaseValue(Double.parseDouble(totalEvalBeforeTD.get(0).toString()));
				 output = thisConverter.formatOutputPrecision();
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM};
		
	}
	

	private String[] getTotalEvalBeforeTD(HttpServletRequest request,
			OperationUomContext operationUomContext, Date spuddate, Date tddatetime,
			String opsid) {
		 String output = "0.0";
			String UOM = "";
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycurve", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String totalEvalBeforeTDQuery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a, Daily d " +
											"WHERE (a.isDeleted=false or a.isDeleted is null) " +
											"AND (d.isDeleted=false or d.isDeleted is null) " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
											"AND (a.isSimop=false or a.isSimop is null) " +
											"AND (a.isOffline=false or a.isOffline is null) " +
											"AND a.operationUid =:opsid " + 
											"AND a.phaseCode IN ('EP','EI') " +
											"AND a.dailyUid = d.dailyUid " +
											"AND d.dayDate >=:spuddate " +
											"AND d.dayDate <=:tddatetime";
			List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","spuddate","tddatetime"}, new Object[]{opsid,spuddate,tddatetime},qp);
			if (totalEvalBeforeTD.get(0) != null){
				
				 thisConverter.setBaseValue(Double.parseDouble(totalEvalBeforeTD.get(0).toString()));
				 output = thisConverter.formatOutputPrecision();
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM};
		
	}

	private String[] getSpudToTdNoCsg(HttpServletRequest request,
			OperationUomContext operationUomContext, Date spudDate, Date tdDateTime,
			String opsid, String code) {
		String output = "0.0";
		double spudtotd = 0.0;
		double casingduration = 0.0;
		double spudtotdnocsg = 0.0;
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycurve", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			spudtotd = CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(spudDate,tdDateTime);
			
			String strSql = "SELECT SUM(coalesce(activityDuration,0.0)) FROM Activity " +
			"WHERE (isDeleted = false or isDeleted is null) " +
			"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
			"AND (isSimop=false or isSimop is null) " +
			"AND operationUid = :opsid " +
			"AND phaseCode = :code " +
			"AND (isOffline=false or isOffline is null)";

			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","code"}, new Object[]{opsid,code},qp);
			if (result.get(0) != null){
				casingduration = (Double) result.get(0);
			
			}

			spudtotdnocsg =  spudtotd - casingduration;
			
			thisConverter.setBaseValue(spudtotdnocsg);
			output = thisConverter.formatOutputPrecision();
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM} ;
	}

	private String[] getSpudToTdDuration(HttpServletRequest request,OperationUomContext operationUomContext, Date spudDate,	Date tdDateTime) {
		double spudToTdDuration = 0.0;
		String output = "0.0";
		String UOM = "";
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycurve", operationUomContext);
			spudToTdDuration =  CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(spudDate,tdDateTime);
			UOM = thisConverter.getUomSymbol();
			thisConverter.setBaseValue(spudToTdDuration);
			
			output = thisConverter.formatOutputPrecision(); ;
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return new String[]{output,UOM} ;
	}

	private String[] getActivityDurationByPhase(HttpServletRequest request, OperationUomContext operationUomContext, String opsid, String code) {
		String actDuration="0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycurve", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String strSql = "SELECT SUM(coalesce(activityDuration,0.0)) FROM Activity " +
							"WHERE (isDeleted = false or isDeleted is null) " +
							"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
							"AND (isSimop=false or isSimop is null) " +
							"AND operationUid = :opsid " +
							"AND phaseCode = :code " +
							"AND (isOffline=false or isOffline is null)";
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","code"}, new Object[]{opsid,code},qp);
			if (result.get(0) != null){
				
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				actDuration = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {actDuration,UOM} ;
	}

	private String[] getTotalWellCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalWellCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS well_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid ";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalWellCost = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalWellCost,UOM} ;
	}
	
	private String[] getPhMudCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalHoleCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('PH','PH1','PH2','PH3','SC','EP','PC')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalHoleCost = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalHoleCost,UOM} ;
	}
	private String[] getShMudCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalHoleCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('SH','SH1','SH2','SH3')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalHoleCost = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalHoleCost,UOM} ;
	}
	private String[] getHolesCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalHoleCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('PH','PH1','PH2','PH3')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalHoleCost = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalHoleCost,UOM} ;
	}

	private String[] getCasingCementCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalCasingCementCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT  SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('SC','IC','PC','PC1','PC2','PC3')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalCasingCementCost = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalCasingCementCost,UOM} ;
	}

	private String[] getCasingCost(HttpServletRequest request,OperationUomContext operationUomContext, String opsid) {
		String totalCasingCost="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code IN ('SC','IC')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				totalCasingCost = thisConverter.formatOutputPrecision();
				
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {totalCasingCost,UOM} ;
	}

	private String[] getPhaseTotalByCode(HttpServletRequest request,OperationUomContext operationUomContext, String opsid, String phasecode) {
		String avalue="0.0";
		String UOM = "";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			UOM = thisConverter.getUomSymbol();
			String phasetotalquery = "SELECT SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid AND phase_code = :code";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid","code"}, new Object[]{opsid,phasecode},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				avalue = thisConverter.formatOutputPrecision();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return new String[] {avalue,UOM} ;
	}

	private String[] getDurationBasedOnUserCode(HttpServletRequest request,	OperationUomContext operationUomContext, String opsid,String codes) {
		String strConvertedValue = "0.0";
		
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "activityDuration", operationUomContext);
			
			
			String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d " +
							"WHERE (a.isDeleted = false or a.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND a.dailyUid = d.dailyUid " +
 							"AND a.operationUid = :opsid " +
							"AND a.userCode = :code " +
							"AND (a.isOffline=false or a.isOffline is null)";
							
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","code"}, new Object[]{opsid,codes},qp );
		
			if (result.get(0) != null ){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				strConvertedValue = thisConverter.formatOutputPrecision();				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
			
		return new String[]{strConvertedValue,""};
	}
	private String[] getActivityDurationRunningTotal(HttpServletRequest request, OperationUomContext operationUomContext, String operationUid, Double activityDuration)  {
		if (operationUid==null) return new String[]{this.activityDurationTotal.toString(),"", "0.0"};
		
		if("".equals(this.operationUid)){
			this.operationUid = operationUid;
		}else{
			if(!operationUid.equals(this.operationUid)){
				this.operationUid = operationUid;
				this.activityDurationTotal = 0.0;
			}
		}
		
		try {
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			if(activityDuration != null) this.activityDurationTotal = this.activityDurationTotal + activityDuration;
			return new String[]{durationConverter.formatOutputPrecision(this.activityDurationTotal),durationConverter.getUomSymbol(), "0.0"};
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[]{this.activityDurationTotal.toString(), "", "0.0"};
	}
	
	private String[] getMetricByDay(HttpServletRequest request, OperationUomContext operationUomContext, Double depth, Double duration)  {
		if (depth==null) return new String[]{"","", "0.0"};
		if (duration==null) return new String[]{"","", "0.0"};
		
		try {
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "depthMdMsl", operationUomContext);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			String uomSymbol = depthConverter.getUomSymbol() + "/" + durationConverter.getUomSymbol();
			durationConverter.setBaseValue(duration);
			Double value = (depth / durationConverter.getConvertedValue());
			value = Math.round(value * 100) / 100.0;
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getLocale(request));
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			return new String[]{formatter.format(value),uomSymbol, value.toString()};
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[]{"","", "0.0"};
	}
	
	private String[] getFormationForDepth(HttpServletRequest request, OperationUomContext operationUomContext, String operationUid, Double topMdMsl, Double bottomMdMsl) {
		
		String formationName = "";
		String lastFormation = "";
		try {
			Operation operation = null;
			String wellboreUid = "";
			if (ApplicationUtils.getConfiguredInstance().getCachedAllOperations().containsKey(operationUid)) {
				operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
				wellboreUid = operation.getWellboreUid();
			}
			
			QueryProperties qp = new QueryProperties();
			qp.setDatumConversionEnabled(false);
			qp.setUomConversionEnabled(false);
			
			String strSql = "SELECT formationName FROM Formation " +
					"where (isDeleted=false or isDeleted is null) " +
					"and wellboreUid=:wellboreUid " +
					"and sampleTopMdMsl<=:topMdMsl " +
					"order by sampleTopMdMsl DESC";
			List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"wellboreUid", "topMdMsl"}, new Object[] {wellboreUid, topMdMsl}, qp);
			if (rs.size()>0) {
				if (StringUtils.isNotBlank(rs.get(0))) {
					formationName = rs.get(0);
					lastFormation = rs.get(0);
				}
			}
			strSql = "SELECT formationName FROM Formation " +
					"where (isDeleted=false or isDeleted is null) " +
					"and wellboreUid=:wellboreUid " +
					"and sampleTopMdMsl>=:topMdMsl " +
					"and sampleTopMdMsl<=:bottomMdMsl " +
					"order by sampleTopMdMsl";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"wellboreUid", "topMdMsl", "bottomMdMsl"}, new Object[] {wellboreUid, topMdMsl, bottomMdMsl}, qp);			
			for (String formation : rs) {
				if (StringUtils.isBlank(formation)) continue;
				if (lastFormation.equals(formation)) continue;
				formationName += ("".equals(formationName)?"":"\r") + formation;
				lastFormation = formation;
			}
			strSql = "SELECT formationName FROM Formation " +
				"where (isDeleted=false or isDeleted is null) " +
				"and wellboreUid=:wellboreUid " +
				"and sampleTopMdMsl<=:bottomMdMsl " +
				"order by sampleTopMdMsl DESC";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"wellboreUid", "bottomMdMsl"}, new Object[] {wellboreUid, bottomMdMsl}, qp);			
			if (rs.size()>0) {
				if (StringUtils.isNotBlank(rs.get(0)) && !lastFormation.equals(rs.get(0))) {
					formationName += ("".equals(formationName)?"":"\r") + rs.get(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {formationName,""};
	}
	
	private String[] getRigInformationUid_unwantedEvent(HttpServletRequest request, String operationUid, Date reportDate) {
		String rigInformationUid = "";
		if(reportDate != null) {
			reportDate =DateUtils.truncate(reportDate, Calendar.DAY_OF_MONTH);
		}
		try {
			String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid and dayDate=:dayDate";
			List<Daily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "dayDate"}, new Object[] {operationUid, reportDate});
			if (rs.size()>0) {
				Daily d = rs.get(0) ;
				ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(operationUid), d.getDailyUid(), false);
				if (rd!=null) {
					if (rd.getRigInformationUid()!=null) rigInformationUid = rd.getRigInformationUid();
				}
			}
			if ("".equals(rigInformationUid)) {
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
				if (operation.getRigInformationUid()!=null) rigInformationUid = operation.getRigInformationUid();
			}
			
			if (!"".equals(rigInformationUid)) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
				if (rig!=null) {
					rigInformationUid = rig.getRigName();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {rigInformationUid, ""};
	}
	
	private String[] getRigInformationUidByDaily(HttpServletRequest request, String dailyUid) {
		String rigInformationUid = "";
		try {
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if (daily!=null) {
				if (daily.getIsDeleted()==null) daily.setIsDeleted(false);
				if (!daily.getIsDeleted()) {
					String operationUid = daily.getOperationUid();
					ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
					if (rd!=null) {
						rigInformationUid = rd.getRigInformationUid();
						if (rigInformationUid!=null) {
							RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
							if (rig!=null) {
								rigInformationUid = rig.getRigName();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {rigInformationUid, ""};
	}
	
	private String[] getReportNumberByDaily(HttpServletRequest request, String dailyUid) {
		String reportNumber = "";
		try {
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if (daily!=null) {
				if (daily.getIsDeleted()==null) daily.setIsDeleted(false);
				if (!daily.getIsDeleted()) {
					String operationUid = daily.getOperationUid();
					ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
					if (rd!=null) {
						reportNumber = rd.getReportNumber();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {reportNumber, ""};
	}
	
	private String[] getAverageROP_bit(HttpServletRequest request, OperationUomContext operationUomContext, String bharunUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double avgrop = 0.00;
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), BharunDailySummary.class, "rop", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			String strSql = "SELECT SUM(coalesce(duration,0.0)), SUM(coalesce(progress,0.0)) FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :bharunUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid, qp);
			if (lstResult.size()>0) {
				Object[] thisResult = (Object[]) lstResult.get(0);
				if (thisResult[0] != null && thisResult[1] != null) {
					Double totalProgress = 0.00;
					totalProgress = Double.parseDouble(thisResult[1].toString());
					Double totalDuration = 0.00;
					totalDuration = Double.parseDouble(thisResult[0].toString());
					if (totalDuration!=0) avgrop = totalProgress / totalDuration;
				}
			}
			thisConverter.setBaseValue(avgrop);
			strConvertedValue = thisConverter.formatOutputPrecision();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new String[] {strConvertedValue, uomSymbol, avgrop.toString()};
	}
	
	private String[] getCurrentDayCost(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String reportType) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double dayCost = 0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			Double daycostFromCostNet = 0.0;
			daycostFromCostNet = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(dailyUid);
			if (daycostFromCostNet!=null) {
				strConvertedValue = thisConverter.formatOutputPrecision(daycostFromCostNet);
				return new String[] {strConvertedValue, uomSymbol, daycostFromCostNet.toString()};
			}
			
			String strSql = "select daycost from ReportDaily where (isDeleted=false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid";
			List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"dailyUid", "reportType"}, new Object[] {dailyUid, reportType});
			if (result.size()>0) {
				dayCost = (Double) result.get(0);
				if (dayCost==null) dayCost = 0.0;
				strConvertedValue = thisConverter.formatOutputPrecision(dayCost);
				return new String[] {strConvertedValue, uomSymbol, dayCost.toString()};
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, dayCost.toString()};
	}

	private String[] getCummulativeCost(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String reportType) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumcost = 0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			cumcost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(dailyUid, reportType);
			if (cumcost==null) cumcost = 0.0;
			thisConverter.setBaseValue(cumcost);
			strConvertedValue = thisConverter.formatOutputPrecision(cumcost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, cumcost.toString()};
	}
	
	private String[] getTotalDailyCost(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String reportType, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double totalcost = 0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), UserSession.getInstance(request).getCachedAllAccessibleOperations().get(operationUid), dailyUid, false);
			
			if (reportDaily.getDaycost() != null) totalcost = totalcost + reportDaily.getDaycost();
			if (reportDaily.getDaycompletioncost() != null)totalcost =  totalcost + reportDaily.getDaycompletioncost();
			if (reportDaily.getDaytangiblecost() != null)totalcost =  totalcost + reportDaily.getDaytangiblecost();
			if (reportDaily.getOtherCost() != null)totalcost =  totalcost + reportDaily.getOtherCost();
			
			thisConverter.setBaseValue(totalcost);
			strConvertedValue = thisConverter.formatOutputPrecision(totalcost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, totalcost.toString()};
	}
	
	
	private String[] getLastHoleSizeFromActivity(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String operationUid) {
		Double lastHoleSize=null;
		String strConvertedValue = "";
		
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Activity.class, "holeSize", operationUomContext);
			Map<String, LookupItem> lookup = LookupManager.getConfiguredInstance().getLookup("xml://activity.holeSize?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)), null);
			
			//get hole size from last activity of the day
			String strSql = "select coalesce(holeSize,0.0) "+
					"FROM Activity " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"and (isSimop=false or isSimop is null) and (isOffline=false or isOffline is null) " +
					"and (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
					"AND holeSize is not null  "+
					"AND operationUid=:operationUid and dailyUid=:dailyUid order by endDatetime desc";

			List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"dailyUid", "operationUid"}, new Object[] {dailyUid, operationUid});
			if (result.size()>0) {
				Object a = (Object) result.get(0);
				if (a != null) lastHoleSize = Double.parseDouble(a.toString());
			}
			
			if(lastHoleSize != null){
				thisConverter.setBaseValue(lastHoleSize);
				strConvertedValue = thisConverter.formatOutputPrecision(lastHoleSize);
				if (lookup.containsKey(strConvertedValue)) {
					strConvertedValue = (String) lookup.get(strConvertedValue).getValue();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new String[] {strConvertedValue, "", nullToEmptyString(lastHoleSize)};
	}
	
	private String nullToEmptyString(Double str) {
		if (str == null) {
			return "";
		}
		
		return str.toString();
	}
	
	private String[] getTotalCumulativeCost(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String reportType, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumtotalcost = 0.0;
		String strSql="";
		
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "cumtotalcost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			
			//cumCost
			Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(dailyUid, reportType);
			
			Double cumcompletioncost = 0.0;
			Double cumtangiblecost = 0.0;
			Double cumothercost = 0.0;
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if (daily != null){
				Date todayDate = daily.getDayDate();
				String[] paramsFields = {"todayDate", "operationUid", "reportType"};
				Object[] paramsValues = new Object[3]; 
				paramsValues[0] = todayDate; 
				paramsValues[1] = operationUid;
				paramsValues[2] = reportType;
				
				//cumcompletioncost
				strSql = "select sum(daycompletioncost) as cumcompletioncost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				Object a = (Object) lstResult.get(0);
				if (a != null) cumcompletioncost = Double.parseDouble(a.toString());
				
				//calculate cum. tangible cost
				strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = (Object) lstResult.get(0);
				if (a != null) cumtangiblecost = Double.parseDouble(a.toString());
				
				//other cost
				strSql = "select sum(otherCost) as cumothercost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = (Object) lstResult.get(0);
				if (a != null) cumothercost = Double.parseDouble(a.toString());
			}
			
			//calc Total Cumulative Cost
			if ((cumCost!=null) || (cumcompletioncost !=null) || (cumtangiblecost !=null) || (cumothercost !=null)) {
				cumtotalcost = 0.0;
				if (cumCost!=null) cumtotalcost += cumCost;
				if (cumcompletioncost!=null) cumtotalcost += cumcompletioncost;
				if (cumtangiblecost!=null) cumtotalcost += cumtangiblecost;
				if (cumothercost!=null) cumtotalcost += cumothercost;
			}
			
			thisConverter.setBaseValue(cumtotalcost);
			strConvertedValue = thisConverter.formatOutputPrecision(cumtotalcost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, cumtotalcost.toString()};
	}
	
	private String[] getCummulativeTangibleCost(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String reportType, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumtangiblecost = 0.0;
		String strSql="";
		
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "cumtangiblecost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			
			//calculate
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if (daily != null){
				Date todayDate = daily.getDayDate();
				
				String[] paramsFields = {"todayDate", "operationUid", "reportType"};
				Object[] paramsValues = new Object[3]; 
				paramsValues[0] = todayDate; 
				paramsValues[1] = operationUid;
				paramsValues[2] = reportType;
				
				strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				Object a = (Object) lstResult.get(0);
				if (a != null) cumtangiblecost = Double.parseDouble(a.toString());
			}
			
			if (cumtangiblecost==null) cumtangiblecost = 0.0;
			thisConverter.setBaseValue(cumtangiblecost);
			strConvertedValue = thisConverter.formatOutputPrecision(cumtangiblecost);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, cumtangiblecost.toString()};
	}
	
	private String[] getCumCostForOperation(HttpServletRequest request, OperationUomContext operationUomContext, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumcost =0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			cumcost = CommonUtil.getConfiguredInstance().calculateOperationCost(operationUid);
			if (cumcost==null) cumcost = 0.0;
			thisConverter.setBaseValue(cumcost);
			strConvertedValue = thisConverter.formatOutputPrecision(cumcost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, cumcost.toString()};
	}
	
	private String[] getAfeCost(HttpServletRequest request, OperationUomContext operationUomContext, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double afeTotal =0.0;
		
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			
			List<Object[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.MASTER_AFE, operationUid});
			if (afeList.size()>0) {
				for (Object[] rec : afeList) {
					CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
					Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operationUid);
					if (thisAfeTotal!=null) {
						if (afeTotal==null) afeTotal = 0.0;
						afeTotal += thisAfeTotal;
					}
				}
			}else
			{
				List<Double> oplist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select afe FROM Operation where (isDeleted=false or isDeleted is null) and operationUid=:operationUid", new String[] {"operationUid"}, new Object[] {operationUid});
				if (oplist.size()>0) {
					if (oplist.get(0)!= null)
						afeTotal = oplist.get(0);
				}
				if (afeTotal==null) afeTotal = 0.0;
			}
			thisConverter.setBaseValue(afeTotal);
			strConvertedValue = thisConverter.formatOutputPrecision(afeTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, afeTotal.toString()};
	}
	
	private String[] getNptPercent(HttpServletRequest request, String operationUid) {
		String NptPct = "0.00";
		Double nptPercent = 0.00;
		try {
			String strSql = "select sum(coalesce(act.activityDuration,0.0)) as duration from Activity act, ReportDaily rd where (act.isDeleted = false or act.isDeleted is null) ";
			strSql += "and (act.isSimop=false or act.isSimop is null) and (act.isOffline=false or act.isOffline is null) and act.operationUid=:operationUid ";
			strSql += "and (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) ";
			strSql += "and (act.dailyUid=rd.dailyUid) ";
			strSql += "and (rd.isDeleted = false or rd.isDeleted is null)";
			
			List<Double> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			if (rs.size()>0)
			{
				Double totalDuration = 0.00;
				Double totalNptDuration = 0.00;
				
				if (rs.get(0) != null || rs.get(0) > 0) {
					totalDuration = rs.get(0);
					String strSql2 = "select sum(coalesce(act.activityDuration,0.0)) as duration from Activity act, ReportDaily rd where (act.isDeleted = false or act.isDeleted is null) ";
					strSql2 += "and (act.isSimop=false or act.isSimop is null) and (act.isOffline=false or act.isOffline is null) and act.operationUid=:operationUid ";
					strSql2 += "and (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) ";
					strSql2 += "and (act.dailyUid=rd.dailyUid) ";
					strSql2 += "and (rd.isDeleted = false or rd.isDeleted is null) ";
					strSql2 += "and act.internalClassCode IN ('TP','TU') ";			   
					List<Double> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", operationUid);
					
					if (rs2.size()>0)
					{	
						if (rs2.get(0) != null) totalNptDuration = rs2.get(0);
					}
					nptPercent = (totalNptDuration / totalDuration) * 100;
				}
				DecimalFormat formatter = new DecimalFormat("#0.00");
				NptPct = CommonUtils.roundUpFormat(formatter, nptPercent).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {NptPct, "", nptPercent.toString()};
	}	
	
	private String[] getNptPercentWithDateRange(HttpServletRequest request, String operationUid, Date startDateF, Date startDateT) {
		String NptPct = "0.00";
		Double nptPercent = 0.00;
		Double totalNptDuration = 0.00;
		Double totalDuration = 0.00;
		try {
			String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) as duration " +
					"FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted = false or d.isDeleted is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"and (a.dailyUid=d.dailyUid) " +
					"AND d.operationUid=:operationUid " +
					"AND d.dayDate BETWEEN '"+startDateF+"' AND '"+startDateT+"'";
			
			List<Double> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			if (rs.size()>0) totalDuration = rs.get(0);
			if (totalDuration==null) totalDuration = 0.0;
				
			
			strSql = "select sum(coalesce(a.activityDuration,0.0)) as duration " +
					"from Activity a, Daily d " +
					"where (a.isDeleted = false or a.isDeleted is null) " +
					"and (d.isDeleted = false or d.isDeleted is null) " +
					"and (a.isSimop=false or a.isSimop is null) " +
					"and (a.isOffline=false or a.isOffline is null) " +
					"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"and a.operationUid=:operationUid " +
					"and (a.dailyUid=d.dailyUid) " +
					"and a.internalClassCode IN ('TP','TU') " +
					"AND d.dayDate BETWEEN '"+startDateF+"' AND '"+startDateT+"'";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			if (rs.size()>0) totalNptDuration = rs.get(0);
			if (totalNptDuration==null) totalNptDuration = 0.0;
			
			if (totalDuration>0.0) {
				nptPercent = (totalNptDuration / totalDuration) * 100;
				DecimalFormat formatter = new DecimalFormat("#0.00");
				NptPct = CommonUtils.roundUpFormat(formatter, nptPercent).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {NptPct, "%", nptPercent.toString()};
	}	
	
	private String[] getCostPerMeasuredDepth(HttpServletRequest request, String operationUid, Date startDateF, Date startDateT) {
				
		String strConvertedValue = "";
		String uomSymbol = "";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			String afeUomSymbol="";
			String depthUomSymbol="";
			String lastdayUid="";
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Operation.class, "afe",operationUomContext);
			afeUomSymbol = thisConverter.getUomSymbol();
			Double afeCost = 0.0;
			String strSql = "select coalesce(afe,0.0) from Operation where (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
			List<Double> lsResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			if (lsResult.size()>0) {
				afeCost = lsResult.get(0);	
				if(afeCost==0.0)
				{
					return new String[] {strConvertedValue, uomSymbol, "0.0"};
				}
			}
			
			CustomFieldUom thisConverterDepth = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "depthMdMsl",operationUomContext);
			depthUomSymbol = thisConverterDepth.getUomSymbol();
			
			if(startDateF!=null && startDateT!=null)
			{
				String strSqlDailyUid = "select dailyUid from Daily where (isDeleted=false or isDeleted is null) ";
				strSqlDailyUid += "and dayDate between '"+startDateF+"' and '"+startDateT+"' and operationUid = :operationUid order by dayDate desc";
				List<String> lsResultDailyUid = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDailyUid, "operationUid", operationUid);
				if (lsResultDailyUid.size()>0) {
					lastdayUid = lsResultDailyUid.get(0);
				}
				
			}else{
				lastdayUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(operationUid);
			}

			String strSqlDepth = "select coalesce(rd.depthMdMsl,0.0) from ReportDaily rd, Daily d where rd.dailyUid=d.dailyUid and (d.isDeleted=false or d.isDeleted is null) ";
			strSqlDepth += "and (rd.isDeleted=false or rd.isDeleted is null) ";
			strSqlDepth += "and d.dayDate between '"+startDateF+"' and '"+startDateT+"'";
			strSqlDepth += "and rd.dailyUid=:lastdayUid";			
			List<Double> lsResultDepth = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDepth, "lastdayUid", lastdayUid);
			Double maxDepth = 0.0;
			if (lsResultDepth.size()>0) {
				maxDepth = lsResultDepth.get(0);
			}else{
				return new String[] {strConvertedValue, uomSymbol, "0.0"};
			}
			
			Double avgCostDepth= afeCost/maxDepth;
			strConvertedValue = thisConverter.formatOutputPrecision(avgCostDepth);
			uomSymbol = afeUomSymbol+"/"+depthUomSymbol;
			return new String[] {strConvertedValue, uomSymbol, avgCostDepth.toString()};
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol , "0.0"};
	}
	
	private String[] getDepthPerDay(HttpServletRequest request, String operationUid, Date startDateF, Date startDateT) {
		
		String strConvertedValue = "";
		String uomSymbol = "";
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			String depthUomSymbol="";
			Double totalDuration = 0.0;
			String strSql = "select d.operationUid, sum(coalesce(a.activityDuration,0.0)) as duration " +
					"from Activity a, Daily d " +
					"where a.dailyUid=d.dailyUid " +
					"and (d.isDeleted = false or d.isDeleted is null) " +
					"and (a.isDeleted = false or a.isDeleted is null) " +
					"and (a.isSimop=false or a.isSimop is null) " +
					"and (a.isOffline=false or a.isOffline is null) " +
					"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"and d.operationUid=:operationUid " +
					"and (a.phaseCode='D') " +
					"and (a.taskCode='DAP' OR a.taskCode='DAR' OR a.taskCode='DARS') " +
					"and (a.internalClassCode!='TP') " +
					"and d.dayDate between '"+startDateF+"' and '"+startDateT+"' " +
					"group by d.operationUid ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
			if (list.size()>0) {
				totalDuration = (Double) list.get(0)[1];
			} else {
				return new String[] {"","","0.0"};
			}
			if (totalDuration==null) return new String[] {"","","0.0"};
			CustomFieldUom thisConverterDuration = new CustomFieldUom(this.getLocale(request), Operation.class, "daysSpentPriorToSpud",operationUomContext);
			thisConverterDuration.setBaseValue(totalDuration);
			totalDuration = thisConverterDuration.getConvertedValue();
			
			if (totalDuration > 0.0) {
				CustomFieldUom thisConverterDepth = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "depthMdMsl",operationUomContext);
				depthUomSymbol = thisConverterDepth.getUomSymbol();
				String strSqlDepth = "select d.operationUid, min(coalesce(a.depthMdMsl,0.0)), max(coalesce(a.depthMdMsl,0.0)) " +
						"from Daily d, Activity a " +
						"where (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"and d.operationUid=:operationUid " +
						"and d.dayDate between '"+startDateF+"' and '"+startDateT+"' " +
						"group by d.operationUid ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDepth, "operationUid", operationUid, qp);
				Double minDepth = 0.0;
				Double maxDepth = 0.0;
				if (list.size()>0) {
					minDepth = (Double) list.get(0)[1];
					maxDepth = (Double) list.get(0)[2];
				}
				if (minDepth==null) minDepth = 0.0;
				if (maxDepth==null) maxDepth = 0.0;
				Double progress = maxDepth - minDepth;
				thisConverterDepth.setBaseValue(progress);
				progress = thisConverterDepth.getConvertedValue();
				Double avgDepthPerDay= progress/totalDuration;
				strConvertedValue = thisConverterDepth.formatOutputPrecision(avgDepthPerDay);
				uomSymbol = depthUomSymbol+"/day";
				return new String[] {strConvertedValue, uomSymbol, avgDepthPerDay.toString()};
			} else {
				return new String[] {"", "", "0.0"};
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, "0.0"};
	}

	private String[] getAfeVariance (HttpServletRequest request, OperationUomContext operationUomContext, String operationUid) {
		String strConvertedValue = "";
		String uomSymbol = "%";
		try {
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			String lastDailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(operationUid, reportType);
			
			//String costAfeMasterUid = "";
			Double afeTotal = 0.0;
			String queryString = "SELECT o.costAfeMasterUid FROM CostAfeMaster c, OperationAfe o " +
				"where (c.isDeleted=false or c.isDeleted is null) " +
				"and (o.isDeleted=false or o.isDeleted is null) " +
				"and o.operationUid=:operationUid " +
				"and c.costAfeMasterUid=o.costAfeMasterUid ";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
			if (list.size()>0) {
				for (String costAfeMasterUid : list) {
					Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMasterUid, operationUid);
					if (thisAfeTotal!=null) afeTotal += thisAfeTotal;
				}
			} else {
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				if (operation!=null) {
					if (operation.getAfe()!=null) afeTotal += operation.getAfe();
					if (operation.getSecondaryafeAmt()!=null) afeTotal += operation.getSecondaryafeAmt();
				}
			}
			Double actualTotal = CommonUtil.getConfiguredInstance().calculateCummulativeCost(lastDailyUid, reportType);
			if (actualTotal==null) actualTotal = 0.0;
			
			if (afeTotal>0.0 && actualTotal > 0.0) {
				Double afeVarience = (afeTotal - actualTotal) / actualTotal * 100.0; 
				DecimalFormat formatter = new DecimalFormat("#0.00");
				strConvertedValue = CommonUtils.roundUpFormat(formatter, afeVarience).toString();
			} else {
				return new String[] {"","","0.0"};
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, "0.0"};
	}
	
	private String[] getDrillingContractorNPT(HttpServletRequest request, String operationUid, Date startDateF, Date startDateT) {		
		String strConvertedValue = "";
		String uomSymbol = "%";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String contractorUid = "";
			
			String strSql = "Select ri.rigManager from Operation o, RigInformation ri where o.rigInformationUid = ri.rigInformationUid and (ri.isDeleted=false or ri.isDeleted is null) and o.operationUid=:operationUid";
			List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
			if (rs.size()>0) contractorUid = rs.get(0);
			if (StringUtils.isBlank(contractorUid)) return new String[] {"","","0.0"};
			
			// total NPT hours
			Double totalNptDuration = 0.00;
			strSql = "select sum(coalesce(a.activityDuration,0.0)) as duration from Activity a, Daily d " +
					"where (a.isDeleted = false or a.isDeleted is null) " +
					"and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid=a.dailyUid " +
					"and (a.isSimop=false or a.isSimop is null) " +
					"and (a.isOffline=false or a.isOffline is null) " +
					"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"and d.operationUid=:operationUid " +
					"and d.dayDate between '"+startDateF+"' and '"+startDateT+"' " +
					"and a.internalClassCode IN ('TP','TU') ";			   
			List<Double> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
			if(list.size()>0) totalNptDuration = list.get(0);
			if (totalNptDuration==null) return new String[] {"","","0.0"};
			
			//total contractor hours
			Double contractorNPT=0.0;
			strSql = "select sum(coalesce(a.activityDuration,0.0)) as duration from Activity a, Daily d " +
					"where (a.isDeleted = false or a.isDeleted is null) " +
					"and (d.isDeleted = false or d.isDeleted is null) " +
					"and a.dailyUid=d.dailyUid " +
					"and (a.isSimop=false or a.isSimop is null) " +
					"and (a.isOffline=false or a.isOffline is null) " +
					"and d.operationUid=:operationUid " +
					"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"and d.dayDate between '"+startDateF+"' and '"+startDateT+"' " +
					"and a.internalClassCode IN ('TP','TU') " +
					"and a.lookupCompanyUid=:lookupCompanyUid ";
			String[] paramNames = {"operationUid","lookupCompanyUid"};
			String[] paramValues = {operationUid,contractorUid};
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames,paramValues, qp);
			if (list.size()>0) contractorNPT = list.get(0);
			if (contractorNPT==null) contractorNPT = 0.0; 
						
			Double contractorNptHrsPct = 0.0;
			if(totalNptDuration>0.0) {
				contractorNptHrsPct = (contractorNPT/totalNptDuration)*100;	
			} else {
				return new String[] {"","","0.0"};
			}
			DecimalFormat formatter = new DecimalFormat("#0.00");
			strConvertedValue = CommonUtils.roundUpFormat(formatter, contractorNptHrsPct).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, "0.0"};
	}
	
	private String[] getPaddedStr(HttpServletRequest request, String value) {
		value = WellNameUtil.getPaddedStr(value);
		return new String[] {value, ""};
	}
	
	private String getDatumDisplayName(OperationUomContext operationUomContext, UserSession session, String opsDatumUid) throws Exception {
		if (datumNameMap.containsKey(opsDatumUid)) return datumNameMap.get(opsDatumUid);
		String displayName = "";
		String separator = "";
		AbstractDatumFormatter datum_formatter = AbstractDatumFormatter.getConfiguredInstance();
		if(datum_formatter != null){
			String[] separators =  datum_formatter.getDatumLabelSeparators(session.getCurrentGroupUid());
			if (separators.length>0) separator = separators[0];
		}
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String queryString = "FROM OpsDatum WHERE (isDeleted=false or isDeleted is NULL) AND opsDatumUid=:opsDatumUid";
		List<OpsDatum> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "opsDatumUid", opsDatumUid, qp);
		if (list.size()>0) {
			OpsDatum opsDatum = list.get(0);
			
			CustomFieldUom formatter = new CustomFieldUom(session.getUserLocale(), OpsDatum.class, "reportingDatumOffset", operationUomContext);
			formatter.setShowUomSymbol(false);
			formatter.setBaseValue(null2Zero(opsDatum.getReportingDatumOffset()));
			if (opsDatum.getDatumReferencePoint()==null) opsDatum.setDatumReferencePoint("MSL");
			displayName = formatter.getFormattedValue() + separator + formatter.getUomSymbol() + separator + opsDatum.getDatumCode() + separator + opsDatum.getDatumReferencePoint();
			formatter.dispose();
			
			datumNameMap.put(opsDatumUid, displayName);
		}
		
		return displayName;
	}
	
	/**
	 * Common Method to calculate total volume for each mud volume record (based on type and label )
	 * @param request
	 * @param operationUomContext
	 * @param operationUid
	 * @param dailyUid
	 * @param mudVolumesUid
	 * @param type
	 * @param label 
	 * @throws Exception
	 */
	private String[] getTotalMudVolumesPerDay(HttpServletRequest request, OperationUomContext operationUomContext, String dailyUid, String operationUid, String mudVolumesUid, String type, String label) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double totalVolume = 0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), MudVolumeDetails.class, "volume", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			totalVolume =MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, mudVolumesUid, type, label);
			if (totalVolume==null) totalVolume = 0.0;
			thisConverter.setBaseValue(totalVolume);
			strConvertedValue = thisConverter.formatOutputPrecision();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, totalVolume.toString()};
	}
	
	/**
	 * Common Method to calculate cummulative volume to date (based on type and label )
	 * @param request
	 * @param operationUomContext
	 * @param operationUid
	 * @param dailyUid
	 * @param type
	 * @param label 
	 * @throws Exception
	 */
	private String[] getCummulativeMudVolumes(HttpServletRequest request, OperationUomContext operationUomContext,String dailyUid, String operationUid, String type, String label) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumVolume = 0.0;
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), MudVolumeDetails.class, "volume", operationUomContext);
			uomSymbol = thisConverter.getUomSymbol();
			cumVolume = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, type, label);
			if (cumVolume==null) cumVolume = 0.0;
			thisConverter.setBaseValue(cumVolume);
			strConvertedValue = thisConverter.formatOutputPrecision();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {strConvertedValue, uomSymbol, cumVolume.toString()};
	}
	
	private String[] getDvdCumP50Duration(HttpServletRequest request,
			OperationUomContext operationUomContext, String operationPlanPhaseUid) {
		String strConvertedValue = "";
		String uomSymbol = "";
		Double cumVolume = 0.0;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		try {
			if (StringUtils.isNotBlank(operationPlanPhaseUid)) {
				OperationPlanPhase opp = (OperationPlanPhase) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanPhase.class, operationPlanPhaseUid);
				if (opp!=null) {
					if (StringUtils.isNotBlank(opp.getOperationPlanMasterUid())) {
						OperationPlanMaster opm = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, opp.getOperationPlanMasterUid());
						if (opm!=null) {
							Integer seq = opp.getSequence();
							String gwpvalue = "";
							String altgwpvalue = null;
							String sqlCondition = "";
							Map<String,String> excludedPhaseCode = new HashMap<String,String>();
							
							if ("CS".equals(opm.getCostPhaseType())) {
								gwpvalue = GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
								altgwpvalue = GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
							} else if ("".equals(opm.getCostPhaseType())) {
								gwpvalue = GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
								altgwpvalue = GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
							}
							if (StringUtils.isNotBlank(gwpvalue)) {
								String strCondtion = "";
								for (String item:gwpvalue.toString().split(";")) {
									String phase = item.toString();
									if (StringUtils.isNotBlank(phase)) {
										strCondtion = strCondtion + "'" + phase + "',";
										excludedPhaseCode.put(item,item);
									}
								}
								strCondtion = StringUtils.substring(strCondtion, 0, -1);
								sqlCondition = "and phaseCode NOT in (" + strCondtion + ")";
							}
							
							if (excludedPhaseCode.containsKey(opp.getPhaseCode())) {
								String strCondtion = "";
								if (StringUtils.isNotBlank(altgwpvalue)) {
									for (String item:altgwpvalue.toString().split(";")) {
										String phase = item.toString();
										if (StringUtils.isNotBlank(phase)) {
											strCondtion = strCondtion + "'" + phase + "',";
											excludedPhaseCode.put(item,item);
										}
									}
								}
								strCondtion = StringUtils.substring(strCondtion, 0, -1);
								sqlCondition = "and phaseCode NOT in (" + strCondtion + ")";
							}							
							
							String queryString = "SELECT sum(p50Duration) FROM OperationPlanPhase WHERE (isDeleted=false or isDeleted is null) and operationPlanMasterUid=:opmUid and sequence<=:seq " + sqlCondition;
							List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"opmUid","seq"}, new Object[]{opp.getOperationPlanMasterUid(),seq}, qp);
							if (result.size()>0 && result.get(0)!=null) {
								cumVolume = result.get(0);
								CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), OperationPlanPhase.class, "p50Duration", operationUomContext);
								uomSymbol = thisConverter.getUomSymbol();
								thisConverter.setBaseValue(cumVolume);
								strConvertedValue = thisConverter.formatOutputPrecision();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[]{strConvertedValue, uomSymbol, cumVolume.toString()};
	}
	
	private String[] getWellPhaseCost(HttpServletRequest request, OperationUomContext operationUomContext, String operationUid, String functionName) {
		
		String strConvertedValue = "";
		String uomSymbol = "";
		Double totalCost = 0.0;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		try {
			int startIndex = functionName.indexOf("[");
			if (startIndex>0) {
				String phases = functionName.substring(startIndex+1, functionName.length() - 1);
				String[] phase = phases.split(",");
				Map<String, Double> dailyDurationList = new HashMap<String, Double>();
				
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost", operationUomContext);
				String queryString = "select d.operationUid, d.dailyUid, sum(a.activityDuration) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"GROUP BY d.operationUid, d.dailyUid ";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec:list) {
					if (rec[1]!=null && rec[2]!=null) {
						dailyDurationList.put((String) rec[1], (Double) rec[2]);
					}
				}
				
				queryString = "select d.operationUid, d.dailyUid, sum(a.activityDuration) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND d.operationUid=:operationUid " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND a.phaseCode in ('" + StringUtils.join(phase, "','") + "') " +
						"GROUP BY d.operationUid, d.dailyUid ";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
				for (Object[] rec : list) {
					if (rec[1]!=null && rec[2]!=null) {
						String dailyUid = (String) rec[1];
						Double duration = (Double) rec[2];
						if (duration>0.0 && dailyDurationList.containsKey(dailyUid)) {
							Double dayDuration = dailyDurationList.get(dailyUid);
							if (dayDuration>0.0) {
								Double cost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(dailyUid);
								if (cost==null) {
									String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
									queryString = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND reportType=:reportType and dailyUid=:dailyUid";
									List<ReportDaily> rdList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"reportType","dailyUid"}, new Object[] {reportType, dailyUid}, qp);
									if (rdList.size()>0) {
										ReportDaily reportDaily = rdList.get(0);
										if (reportDaily!=null) {
											cost = reportDaily.getDaycost();
										}
									}
								}
								if (cost!=null) {
									totalCost += cost * duration / dayDuration;
								}
							}
						}
					}
				}
				
				
				if (totalCost>0.0) {
					uomSymbol = thisConverter.getUomSymbol();
					thisConverter.setBaseValue(totalCost);
					strConvertedValue = thisConverter.formatOutputPrecision();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[]{strConvertedValue, uomSymbol, totalCost.toString()};
	}
	
}