(function(window){
	D3.inherits(DumpAndRecreteField,D3.AbstractFieldComponent);
	DumpAndRecreteField.URL = "webservice/activitycode.html";
	
	function DumpAndRecreteField() {
		D3.AbstractFieldComponent.call(this);	
	}
	
	
	DumpAndRecreteField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var label = D3.LanguageManager.getLabel("button.label");
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				callbacks:{
					onclick:this.showPopup
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	DumpAndRecreteField.prototype.showPopup = function(event){
		var confirmContainerTemplate = "<div class='RootButtonContainer'>" +
		"<span class='fieldRenderer'>" +
		"<label class='popMsg'></label>" +
		"</span><br>" +
		"<span class='fieldRenderer'>" +
		"<button class='RootButton popOk'>"+D3.LanguageManager.getLabel("button.ok")+"</button>" +
		"<button class='RootButton popConfirm'>"+D3.LanguageManager.getLabel("button.yes")+"</button>" +
		"<button class='RootButton popCancel'>"+D3.LanguageManager.getLabel("button.no")+"</button>" +
		"</span>" +
		"</div>";
		var confirmContainer = $(confirmContainerTemplate);
		this._confirmContainer = confirmContainer.get(0);
		
		this._popMsg = confirmContainer.find(".popMsg").get(0);
		this._popMsg.textContent = this.getWarningMsg();
		this._btnPopOk = confirmContainer.find(".popOk").get(0);
		this._btnPopOk.onclick = this.btnPopOk_onClick.bindAsEventListener(this);
		$(this._btnPopOk).addClass("hide");
		this._btnPopConfirm = confirmContainer.find(".popConfirm").get(0);
		this._btnPopConfirm.onclick = this.btnPopConfirm_onClick.bindAsEventListener(this);
		this._btnPopCancel = confirmContainer.find(".popCancel").get(0);
		this._btnPopCancel.onclick = this.btnPopCancel_onClick.bindAsEventListener(this);
		
		$(this._confirmContainer).dialog({
			title:D3.LanguageManager.getLabel("confirm.title"),
			dialogClass: 'no-close' ,
			closeonEscape:false,
			modal:true, 
			width:"600", 
			height:"100", 
			resizable:false,
			closable:false,
			open: function(event, ui) { 
	            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
	        }
			});
	}
	
	DumpAndRecreteField.prototype.getWarningMsg = function(){
		var tablename = this._node.getFieldValue("tablename");
		var msg = D3.LanguageManager.getLabel("confirm.warning");
		return msg.replace("$lookupTableName", tablename); 			
	}
	DumpAndRecreteField.prototype.btnPopOk_onClick = function(){
		$(this._confirmContainer).dialog("close");
	}
	DumpAndRecreteField.prototype.btnPopConfirm_onClick = function(){
		this.dumpAndRecrete();
	}
	DumpAndRecreteField.prototype.dumpAndRecrete = function() {
		var params = {};
		params.method = "dump_and_create";
		params.lookupName = this._node.getFieldValue("tablename");
		
		var self = this;
		var url = this._node.commandBeanProxy.baseUrl + DumpAndRecreteField.URL;
		$.post(url, params, function(response) {
			self.dumpAndRecreteLoaded(response);
		}, "xml");
	}
	DumpAndRecreteField.prototype.dumpAndRecreteLoaded = function(response) {
		var self = this;
		responseAns = $(response).find("status").get(0).textContent;
		$(this._btnPopOk).removeClass("hide");
		$(this._btnPopConfirm).addClass("hide");
		$(this._btnPopCancel).addClass("hide");
		if (responseAns=="Good"){
			this._popMsg.textContent = D3.LanguageManager.getLabel("status.done");
		}else{
			this._popMsg.textContent = D3.LanguageManager.getLabel("status.duplicated");
		}
	};

	DumpAndRecreteField.prototype.btnPopCancel_onClick = function(){
		$(this._confirmContainer).dialog("close");
	}
	DumpAndRecreteField.prototype.getCompletedMsg = function(){
		var tablename = this._node.getFieldValue("tablename");
		var msg = D3.LanguageManager.getLabel("status.success");
		return msg.replace("$lookupTableName", tablename); 			
	}
	DumpAndRecreteField.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(ActivityCodeListener,D3.EmptyNodeListener);
	
	function ActivityCodeListener() {
	}

	ActivityCodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id=="dumpAndRecrete"){
			return new DumpAndRecreteField();
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ActivityCodeListener;
	
})(window);