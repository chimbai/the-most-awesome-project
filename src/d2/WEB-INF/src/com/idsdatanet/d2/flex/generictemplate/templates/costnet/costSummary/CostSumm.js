/**
 * 
 */

(function(window){
	
	function CostSumm(){
		
		this.isTangible = "";
		this.parentAccountCode = "";
		this.accountCode = "";
		this.afeShortDescription = "";
		this.accountName = "";
		this.category = "";
		this.inAfe = "";
		this.afeTotal = 0.0;
		this.totalDaily = 0.0;
		this.cumDaily = 0.0;
	
	}

	CostSumm.prototype.run = function(){
		if (this.category == "") this.category = "-";
		this.accountName = this.parentAccountCode + " :: " + this.accountCode + " :: " + this.afeShortDescription;
	}
	
	window.CostSumm = CostSumm;
})(window);