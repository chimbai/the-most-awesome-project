(function(window) {
	
	D3.inherits(PasswordInputField,D3.TextInputField);
	function PasswordInputField(){
		D3.TextInputField.call(this);
		this.setFieldValue("");
	}
	
	PasswordInputField.prototype.data = function(data) {
		PasswordInputField.uber.data.call(this, data);
	}
	
	PasswordInputField.prototype.refreshFieldEditor = function() {
		this.setFieldValue("");
		if (this._fieldEditor) {
			this._fieldEditor.type = "password";
			this._fieldEditor.value="";
			this._fieldEditor.title = "Must contain at least \n - one number \n - one uppercase letter \n - one lowercase letter \n - one special character \n - 8 or more characters \n - no space";
		}
	}
	
	PasswordInputField.prototype.refreshFieldRenderer = function() {
		this.setFieldValue("");
		if(this.fieldRenderer)
			this.setFieldRendererValue("");
		
	}
	
	D3.inherits(ButtonField, D3.AbstractFieldComponent);
	function ButtonField() {
		D3.AbstractFieldComponent.call(this);

	}
	ButtonField.prototype.dispose = function() {
		this._fieldRenderer = null;
		this._fieldEditor = null;
		ButtonField.uber.dispose.call(this);
	};
	
	ButtonField.prototype.afterPropertiesSet = function() {
		ButtonField.uber.afterPropertiesSet.call(this);
		
	};
	
	ButtonField.prototype.getFieldRenderer = function() {
		if (!this._container)
		{
			var divConfig = "<div>" +
								"<table>" +
									"<tr>" +
										"<td>" +
											"<input class='signature' type='file' accept='.jpg,.jpeg,.gif,.png'>" +
												"<button class='DefaultButton buttonSignature' />" +
											"</input>" +
										"</td>" +
										"<td style='padding-top:4px'>" +
											"<button class='DefaultButton buttonRemove hide'>"+D3.LanguageManager.getLabel("button.remove")+"</button>" +
										"</td>" +
									"</tr>" +
								"</table>" +
								"<div class='pnlProgress hide'>" +
									"<div class='bar' style='width: 0%; height:20px; background:red; text-align:center; font-weight:bold; color:white'/>" +
								"</div>" +
								"<div class='dialogContainer hide'>" +
									"<p>"+D3.LanguageManager.getLabel("dialog.message")+"</p>" +
								"</div>"+
							"</div>" ;
							
			var container = $(divConfig);
			this._container = container.get(0);

			this._btnSignature = container.find(".buttonSignature");
			this._signature = container.find(".signature");
			this._pnlProgress = container.find(".pnlProgress").get(0);
			var that = this;
			this._btnSignature.click(function() {
				$(that._signature).click();
			});
			
			this._btnRemove = container.find(".buttonRemove");
			this._dialogContainer = container.find(".dialogContainer");
			this._btnRemove.click(function(){
				var btnYes = {
						text:D3.LanguageManager.getLabel("button.yes"),
						click:function(){
							that.btnRemove_onclick();
							$(this).remove();
						}
				};
				var btnNo = {
						text:D3.LanguageManager.getLabel("button.no"),
						click:function(){
							$(this).dialog("close");
						}
				};
				var buttons = [btnYes, btnNo];
				$(that._dialogContainer).removeClass('hide');
				$(that._dialogContainer).dialog({
					modal: true,
					resizable: false,
					title: D3.LanguageManager.getLabel("dialog.alertTitle"),
					buttons: buttons,
					close: function () {
			            $(this).remove();
			        }
				});
			});
			
			this._progressBar = container.find(".bar").get(0);
		}
		return this._container;
	};
	
	ButtonField.prototype.refreshFieldRenderer = function(){
		this.signatureButtonRenderer();
		this.updateLayout();

	};
	
	ButtonField.prototype.updateLayout = function(){
		var hasSignature = this._node.getFieldValue("@hasSignature");
		if (hasSignature!="1"){
			var labelField = this._config.labelField;
			var fields = labelField.split(",");
			var label;
			var i = 0;
			for (; i < fields.length; i++){
				var value = this._node.getFieldValue(fields[i]);
				if (value){
					if (label)
						label+=" "+value;
					else
						label = value;
				}
			}
			this._btnSignature.text(label);
			this._btnRemove.addClass("hide");
		}else{
			this._btnSignature.text(D3.LanguageManager.getLabel("button.signature"));
			this._btnRemove.removeClass("hide");
			this._btnRemove.text(D3.LanguageManager.getLabel("button.remove"));
		}
	}
	
	ButtonField.prototype.signatureButtonRenderer = function(){
		this._signature.data("userUid",this._node.getFieldValue("userUid"));
		this._signature.data("size",this._config.limitUploadSize);
		this._signature.data("buttonField",this);
		this._signature.fileupload({
			url: this._node.commandBeanProxy.getCommandBeanUrl(),
			add: function (e, data) {
				var that = $(this);
				var field =that.data("buttonField");
				that.data("fileName",data.files[0].name);
				var size = (that.data("size")/1000);
				if(size && data.files[0].size/1000>size){
					var getLabel = "message.maxFileSizeLimit";
					var replacedString = "$(fileSizeLimit)";
					var msg = field.getMessage(getLabel, replacedString, size);
					alert(msg);
				}else{
					data.formData = {
						_invokeCustomFilter : "uploadSignature",
						userUid: $(this).data("userUid")
					}
					data.submit();
				}
				field.refreshFieldRenderer();
			},
			start: function (e,data){
				var that = $(this);
				var field = that.data("buttonField");
				var getLabel = "dialog.uploadTitle";
				var replacedString = "$(fileName)";
				var msg = field.getMessage(getLabel, replacedString, that.data("fileName"));
				$(field._pnlProgress).removeClass("hide");
				$(field._pnlProgress).dialog({
					modal:false,
					width:400,
					height:70,
					title: msg
				});
			},
			progress: function(e, data){
				var p = parseInt(data.loaded / data.total * 100);
				var field = $(this).data("buttonField");
				$(field._progressBar).css(
		            'width',
		            p + '%'
		        );
				$(field._progressBar).text(p+"%");
			},
			done: function(e, data){
				var that = $(this);
				var field =that.data("buttonField");
				field._node.setFieldValue("@hasSignature","1");
				$(field._progressBar).text("Processing...");
				$(field._pnlProgress).dialog('close');
				field.updateLayout();
				that.fileupload('destroy');
				field.refreshFieldRenderer();
			},
			fail: function(e, data) {
				var that = $(this);
				var field =that.data("buttonField");
				$(field._pnlProgress).dialog('close');
				that.fileupload('destroy');
				var getLabel = "message.failedUpload";
				var replacedString = "$(fileName)";
				var msg = field.getMessage(getLabel, replacedString, data.files[0].name);
				alert(msg);
				field.refreshFieldRenderer();
			}
		});
		
	}
	
	ButtonField.prototype.btnRemove_onclick = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.signatureRemoved,this);
	}
	
	ButtonField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "removeSignature";
		params.userUid = this._node.getFieldValue("userUid");
		return params;
	}
	
	ButtonField.prototype.signatureRemoved = function() {
		this._node.setFieldValue("@hasSignature","");
		this.updateLayout();
	}
	
	ButtonField.prototype.getMessage = function(getLabel, replacedString, replacedValue){
		var msg = D3.LanguageManager.getLabel(getLabel);
		return msg.replace(replacedString, replacedValue); 			
	}
	
	D3.inherits(IdsBridgeTokenButtonField, D3.AbstractFieldComponent);
	function IdsBridgeTokenButtonField() {
		D3.AbstractFieldComponent.call(this);
		this.ids_bridge_api_key = null;
		this.current_action = IdsBridgeTokenButtonField.REQUEST_API_KEY;
	}
	IdsBridgeTokenButtonField.REQUEST_API_KEY = "requestAPIKey";
	IdsBridgeTokenButtonField.REVOKE_API_KEY = "revokeAPIKey";
	
	IdsBridgeTokenButtonField.prototype.dispose = function() {
		this._fieldRenderer = null;
		this._fieldEditor = null;
		IdsBridgeTokenButtonField.uber.dispose.call(this);
	};
	
	IdsBridgeTokenButtonField.prototype.afterPropertiesSet = function() {
		IdsBridgeTokenButtonField.uber.afterPropertiesSet.call(this);
	};
	
	IdsBridgeTokenButtonField.prototype.getFieldRenderer = function() {
		if (!this._container)
		{
			var self = this;
			this._container = $("<div></div>");
			
			this.idsbridge_token_label = $("<label></label>");
			this.idsbridge_token_label.css("padding", "0 7px");
			this.idsbridge_token_label.css("cursor", "pointer");
			this.idsbridge_token_label.click(function(event) {
				event.stopPropagation();
				D3.UIManager.popup("API Key", self.ids_bridge_api_key);
			});
			this._container.append(this.idsbridge_token_label);

			this.idsbridge_token_btn = $("<button class='DefaultButton buttonIdsBridgeToken'>" + D3.LanguageManager.getLabel("button.idsbridgetoken") + "</button>");
			this.idsbridge_token_btn.click(function(event) {
				event.stopPropagation();
				var message = self.isAPIKeyNotEmpty()? "Revoking API Key...":"Requesting API Key...";
				self.setStatusMessage(message);
				var params = self.getParams();
				self._node.commandBeanProxy.customInvoke(params, self.idsbridgeSubmitted, self, "json");
			});
			this._container.append(this.idsbridge_token_btn);
		}
		return this._container.get(0);
	};
	
	IdsBridgeTokenButtonField.prototype.idsbridgeSubmitted = function(response) {
		var data = response.SimpleXmlResponse.data.root;
		var success = data.success.text;
		if (success == "true") {
			if (this.current_action == IdsBridgeTokenButtonField.REQUEST_API_KEY) {
				var api_key = data.idsbridge_api_key.text;
				this._node.setFieldValue("idsBridgeApiKey", api_key);
			} else {
				this._node.setFieldValue("idsBridgeApiKey", null);
			}
			this.current_action = null;
		} else {
			D3.UIManager.popup("Error", data.error.text);
		}
		this.refreshFieldRenderer();
		this.statusDialog.dialog("close");
	};
	
	IdsBridgeTokenButtonField.prototype.isAPIKeyNotEmpty = function() {
		return (this.ids_bridge_api_key != null && this.ids_bridge_api_key != "");
	};
	
	IdsBridgeTokenButtonField.prototype.refreshFieldRenderer = function() {
		this.ids_bridge_api_key = this._node.getFieldValue("idsBridgeApiKey");
		var btnLabel = "Request API Key";
		this.idsbridge_token_label.empty();
		if (this.isAPIKeyNotEmpty()) {
			btnLabel = "Revoke API Key";
			this.idsbridge_token_label.append("<i class='fa fa-eye'></i>")
		}
		this.idsbridge_token_btn.empty();
		this.idsbridge_token_btn.append(document.createTextNode(btnLabel));
	};
	
	IdsBridgeTokenButtonField.prototype.getParams = function() {
		var params ={};
		this.current_action = this.isAPIKeyNotEmpty()? IdsBridgeTokenButtonField.REVOKE_API_KEY:IdsBridgeTokenButtonField.REQUEST_API_KEY;
		params._invokeCustomFilter = this.current_action;
		params.userUid = this._node.getFieldValue("userUid");
		return params;
	};
	
	IdsBridgeTokenButtonField.prototype.prepareDialog = function() {
		if (this.dialogContent != null) return;
		var dialog = $("<div id='statusDialog'></div>");
		this.dialogContent = $("<div class='dialogContent' style='white-space:pre;'></div>");
		dialog.append(this.dialogContent);
		var self = this;
		this.statusDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
	
	IdsBridgeTokenButtonField.prototype.setStatusMessage = function(message) {
		this.prepareDialog();
		this.dialogContent.empty();
		this.dialogContent.append(message);
		this.statusDialog.dialog("open");
	};

	D3.inherits(ResetTOTPButton, D3.AbstractFieldComponent);
	function ResetTOTPButton() {
		D3.AbstractFieldComponent.call(this);
	}

	ResetTOTPButton.prototype.getFieldRenderer = function() {
		if(!this._renderer){
			var btn = document.createElement("button");
			btn.className = "DefaultButton";
			btn.appendChild(document.createTextNode(D3.LanguageManager.getLabel("button.resetTOTP")));
			var self = this;
			btn.onclick = function(){
				self.setFieldValue("1");
				self._node.setFieldValue("totpResetDatetime", (new Date()).getTime());
				if(!self._node.atts.editMode) self._node.setEditMode(true);
			}
			this._renderer = btn;
		}
		return this._renderer;
	}
				
	D3.inherits(UserNodeListener,D3.EmptyNodeListener);
	
	function UserNodeListener()
	{
		
	}
	
	UserNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id=="password_field"){
			return new PasswordInputField();
		}else if (id=="signatureField"){
			return new ButtonField();
		} else if (id == "idsbridge_token_button") {
			return new IdsBridgeTokenButtonField();
		}else if(id == "ResetTOTPButton"){
			return new ResetTOTPButton();
		}
	},
	D3.CommandBeanProxy.nodeListener = UserNodeListener;
	
})(window);