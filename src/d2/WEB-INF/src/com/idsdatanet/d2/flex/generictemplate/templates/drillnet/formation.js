(function(window){
	D3.inherits(WITSMLUpdateImport, D3.AbstractFieldComponent);

	function WITSMLUpdateImport(node, buttonDeclaration, context) {
		if(!node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
        	return null;
        }
        var type = "";
		this.WITSMLImportButton = new D3.WITSMLImportButton(node.commandBeanProxy, buttonDeclaration, node);
        this._linkButton = D3.UI.createElement({
            tag:"button",
            text:buttonDeclaration.label,
            css:"DefaultButton",
            attributes:{
                title:D3.LanguageManager.getLabel("Get updates from WITSML server")
            },
            
            callbacks:{
                onclick:function(event) {
                	var formations = node.commandBeanProxy.rootNode.children["Formation"];
            		var nodes = formations._items.slice(0, formations._items.length);
            		for(var i=0; i<=nodes.length; i++){
            			var node = nodes[i];
            			var importParameters = node.getFieldValue("importParameters")
            			var formationUid = node.getFieldValue("formationUid");
            			if(importParameters != ""){
            				this.WITSMLImportButton.doUpdateWitsml (formationUid, importParameters);
            			}
            			
            		}
                }
            },
            context:context
        });
	    return this._linkButton;
	}
	
	WITSMLUpdateImport.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(DrillerFormationNodeListener,D3.EmptyNodeListener);
	
	function DrillerFormationNodeListener() {}
	
	DrillerFormationNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "witsml-import-formation") {
			if(!parentNode.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
	        	return null;
	        }
			var config = {
					tag:"button",
					text:buttonDeclaration.label,
					css:"DefaultButton",
					context:this
			};
			this.btnSnapshot = D3.UI.createElement(config);
			return this.btnSnapshot;
		} else {
			return null;
		}
	};
	
	DrillerFormationNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "witsml-import-formation") {
			var formations = node.commandBeanProxy.rootNode.children["Formation"];
    		var nodes = formations._items.slice(0, formations._items.length);
    		var buttonDeclaration = {
					tag:"button",
					text:"WITSML",
					css:"DefaultButton",
					context:this
			};
    		for(var i=0; i<=nodes.length; i++){
    			var node = nodes[i];
    			var importParameters = node.getFieldValue("importParameters")
    			var formationUid = node.getFieldValue("formationUid");
    			if(importParameters != ""){
    				var WITSMLUpdateImport = new D3.WITSMLImportButton(node.commandBeanProxy, buttonDeclaration, node);
    				WITSMLUpdateImport.doUpdateWitsml (formationUid, importParameters);
    			}
    			
    		}
			
		}
	}

	DrillerFormationNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if (commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_NEW" || commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_UPDATE") {
			D3.UIManager.addCalllater(this.archive, this, [commandBeanProxy, commandBeanProxy.formSubmissionReason]);
		}
	};
	
	DrillerFormationNodeListener.prototype.archive = function(commandBeanProxy, formSubmissionReason){
		var formations = commandBeanProxy.rootNode.children["Formation"];
		var nodes = formations._items.slice(0, formations._items.length);
		var editObjPrimaryKeys = [];
		for (var i = 0; i < nodes.length; i++){
			if(nodes[i].atts.editMode){
				editObjPrimaryKeys.push(nodes[i].primaryKeyValue);
			}
		}
		for (var i = 0; i < nodes.length; i++)
		{
			if(!nodes[i].atts.editMode ){
				if ((formSubmissionReason == "WITSML_IMPORT_NEW" && editObjPrimaryKeys.length > 0 && editObjPrimaryKeys.indexOf(nodes[i].primaryKeyValue) != -1) || formSubmissionReason == "WITSML_IMPORT_UPDATE")
					formations.archive(nodes[i]);
			}
		}
		if (formations._items.length == 0) formations.restore();
	};
	
	D3.CommandBeanProxy.nodeListener = DrillerFormationNodeListener;
})(window);