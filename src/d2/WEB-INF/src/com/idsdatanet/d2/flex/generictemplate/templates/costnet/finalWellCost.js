(function(window){

	
	D3.inherits(FinalWellCostNodeListener,D3.EmptyNodeListener);
	
	function FinalWellCostNodeListener() {
		this.baseUrl = "";
		this.wellName="";
	}
	
	FinalWellCostNodeListener.prototype.btn_exportData = function(){ 
		var fileName = "Final Well Cost";
		this.getWellName();
	}
	
	FinalWellCostNodeListener.prototype.exportToExcel = function(fileName){
		var dataType = 'application/vnd.ms-excel';
    	var tableSelect = document.getElementsByClassName('SimpleGridTable');
		 /* Declaring array variable */
    	var rows =[];
    	
		var htmls = "<table id='tests' >" ;
		for(var i=0,row; row = tableSelect[0].rows[i];i++){
			if (i==0){
				 htmls= htmls="<thead><tr>";
				 htmls= htmls+"<td style='width:100px'>"+row.cells[0].innerText+"</td>"+
						"<td style='width:150px'>"+row.cells[1].innerText+"</td>"+
						"<td style='width:400px'>"+row.cells[2].innerText+"</td>"+
						"<td style='width:150px'>"+row.cells[3].innerText+"</td>"+
						"<td style='width:150px'>"+row.cells[4].innerText+"</td>"+
						"<td style='width:150px'>"+row.cells[5].innerText+"</td>";
						"</tr><br> "
				 htmls="</thead>";
			}
			
			htmls = htmls + "<tbody><tr>";
			
			htmls = htmls + "<td style='width:100px'>"+row.cells[0].innerText+"</td>"+
							"<td style='width:150px'>"+row.cells[1].innerText+"</td>"+
							"<td style='width:400px'>"+row.cells[2].innerText+"</td>"+
							"<td style='width:150px'>"+row.cells[3].innerText+"</td>"+
							"<td style='width:150px'>"+row.cells[4].innerText+"</td>"+
							"<td style='width:150px'>"+row.cells[5].innerText+"</td>";
							"</tr><br> ";
			
		}
		htmls = htmls + "</tbody></table>";
		
		var uri = 'data:application/vnd.ms-excel;base64,'
	          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
	          , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))); }
	          , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }); };
	          table=htmls;
	          var ctx = { worksheet: 'Final Well Cost' || 'Final Well Cost', table: htmls };
	        var filename=fileName+'.xls'; 
	        var link = document.createElement("a");
			link.setAttribute("href", uri + base64(format(template, ctx)));
			link.setAttribute("download", filename);
			document.body.appendChild(link);
			link.click();  
	       
	}
	FinalWellCostNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "exportData") {
			this._commandBeanProxy = commandBeanProxy;
			
			this._btnExportData = document.createElement("button");
			this._btnExportData.className = "rootButtons";
			this._btnExportData.id="btn-export";
			var label = "Export";
			var addNew = document.createTextNode(label);
			this._btnExportData.appendChild(addNew);
			this._btnExportData.onclick = this.btn_exportData.bindAsEventListener(this);
			this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace("defintivecosts.html","webservice/costnetservice.html?method=");
			var page = this.baseUrl + "get_account_codes_list";
			var self = this;
			return this._btnExportData;
		}
		
		
	}
	
	FinalWellCostNodeListener.prototype.getWellName = function(){
		if(this._commandBeanProxy!=undefined)this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace(this.baseUrl,"webservice/costnetservice.html?method=");

		var page = this.baseUrl + "export_final_cost_well_record";
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var wellName = result.documentElement.getElementsByTagName("wellName")[0].textContent;
				self.wellName = wellName;
			}
		}).done(function(data, textStatus, jqXHR) {
			fileName="FWCR "+self.wellName;
			self.exportToExcel(fileName);
		});
		
	}
	

	D3.CommandBeanProxy.nodeListener = FinalWellCostNodeListener;
	
})(window);
