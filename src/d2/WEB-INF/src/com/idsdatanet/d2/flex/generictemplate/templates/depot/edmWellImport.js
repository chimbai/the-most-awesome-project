(function(window){

	D3.inherits(ServerPropertiesComboBox,D3.ComboBoxField);

	function ServerPropertiesComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
	}

	ServerPropertiesComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		ServerPropertiesComboBox.uber.dispose.call(this);
	}

	ServerPropertiesComboBox.prototype.afterDataChanged = function(event) {
		this._onChanged(event);
	}

	ServerPropertiesComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireServerChangedEvent(this._node.uid);
	}

	D3.inherits(EdmPolicyComboBox,D3.ComboBoxField);

	function EdmPolicyComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
	}
	
	EdmPolicyComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		EdmPolicyComboBox.uber.dispose.call(this);
	}

	EdmPolicyComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}

	EdmPolicyComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrievePolicyList();
		}		
	}

	EdmPolicyComboBox.prototype.clearList = function() {
		this._policyListLookup = null;
		this._node.setFieldValue("edmPolicyUid","");
		this._node.setFieldValue("edmPolicyName","");
		this.refreshFieldEditor();
	};

	EdmPolicyComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("edmPolicyName");
		this.setFieldRendererValue(renderedText);
	}

	EdmPolicyComboBox.prototype.retrievePolicyList = function() {
		var params = {};
		params._invokeCustomFilter = "get_policy_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		if(params.importExportServerPropertiesUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.policyLoadCompleted, this);
		}
	}

	EdmPolicyComboBox.prototype.getLookupRef = function() {
		return this._policyListLookup;
	};

	EdmPolicyComboBox.prototype.policyLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.policy") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('policy');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});
				this._policyListLookup = lookupReference;
				this._node.setFieldValue("edmPolicyUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.policy"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	EdmPolicyComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	EdmPolicyComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._policyListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("edmPolicyUid", lookupItem.data);
		this._node.setFieldValue("edmPolicyName", lookupItem.label);
		this._onChanged(event);
	}

	EdmPolicyComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.firePolicyChangedEvent(this._node.uid);
	}
	
	D3.inherits(EdmProjectComboBox,D3.ComboBoxField);

	function EdmProjectComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
	}

	EdmProjectComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		EdmProjectComboBox.uber.dispose.call(this);
	}

	EdmProjectComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}

	EdmProjectComboBox.prototype.onPolicyChanged = function(event) {
		this._refreshField(event);
	}

	EdmProjectComboBox.prototype.clearList = function() {
		this._projectListLookup = null;
		this._node.setFieldValue("edmProjectUid","");
		this._node.setFieldValue("edmProjectName","");
		this.refreshFieldEditor();
	};

	EdmProjectComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveProjectList();
		}
	}

	EdmProjectComboBox.prototype.getLookupRef = function() {
		return this._projectListLookup;
	};

	EdmProjectComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("edmProjectName");
		this.setFieldRendererValue(renderedText);
	}

	EdmProjectComboBox.prototype.retrieveProjectList = function() {
		var params = {};
		params._invokeCustomFilter = "get_project_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		params.policyUid = this._node.getFieldValue("edmPolicyUid");
		if(params.policyUid != "" && params.importExportServerPropertiesUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.projectLoadCompleted, this);
		}
	}

	EdmProjectComboBox.prototype.projectLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.project") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('project');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item) {
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});

				this._projectListLookup = lookupReference;
				this._node.setFieldValue("edmProjectUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.project"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	EdmProjectComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._projectListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("edmProjectUid", lookupItem.data);
		this._node.setFieldValue("edmProjectName", lookupItem.label);
		this._onChanged(event);
	}

	EdmProjectComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireProjectChangedEvent(this._node.uid);
	}

	EdmProjectComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}
	
	
	D3.inherits(EdmSiteComboBox,D3.ComboBoxField);

	function EdmSiteComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
	}

	EdmSiteComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		EdmSiteComboBox.uber.dispose.call(this);
	}

	EdmSiteComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}

	EdmSiteComboBox.prototype.onPolicyChanged = function(event) {
		this._refreshField(event);
	}
	
	EdmSiteComboBox.prototype.onProjectChanged = function(event) {
		this._refreshField(event);
	}

	EdmSiteComboBox.prototype.clearList = function() {
		this._siteListLookup = null;
		this._node.setFieldValue("edmSiteUid","");
		this._node.setFieldValue("edmSiteName","");
		this.refreshFieldEditor();
	};

	EdmSiteComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveSiteList();
		}
	}

	EdmSiteComboBox.prototype.getLookupRef = function() {
		return this._siteListLookup;
	};

	EdmSiteComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("edmSiteName");
		this.setFieldRendererValue(renderedText);
	}

	EdmSiteComboBox.prototype.retrieveSiteList = function() {
		var params = {};
		params._invokeCustomFilter = "get_site_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		params.projectUid = this._node.getFieldValue("edmProjectUid");
		if(params.projectUid != "" && params.importExportServerPropertiesUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.siteLoadCompleted, this);
		}
	}

	EdmSiteComboBox.prototype.siteLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.site") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('site');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item) {
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});

				this._siteListLookup = lookupReference;
				this._node.setFieldValue("edmSiteUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.site"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	EdmSiteComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._siteListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("edmSiteUid", lookupItem.data);
		this._node.setFieldValue("edmSiteName", lookupItem.label);
		this._onChanged(event);
	}

	EdmSiteComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireSiteChangedEvent(this._node.uid);
	}

	EdmSiteComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}
	
	D3.inherits(DepotWellComboBox,D3.ComboBoxField);

	function DepotWellComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SITE_CHANGED, this.onSiteChanged, this);
	}

	DepotWellComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SITE_CHANGED, this.onSiteChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		DepotWellComboBox.uber.dispose.call(this);
	}

	DepotWellComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellComboBox.prototype.onPolicyChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellComboBox.prototype.onProjectChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellComboBox.prototype.onSiteChanged = function(event) {
		this._refreshField(event);
	}

	DepotWellComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveWellList();
		}		
	}

	DepotWellComboBox.prototype.clearList = function() {
		this._wellListLookup = null;
		this._node.setFieldValue("depotWellUid","");
		this.refreshFieldEditor();
	};

	DepotWellComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotWellName");
		this.setFieldRendererValue(renderedText);
	}

	DepotWellComboBox.prototype.retrieveWellList = function() {
		var params = {};
		params._invokeCustomFilter = "get_well_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		params.edmSiteUid = this._node.getFieldValue("edmSiteUid");
		if(params.importExportServerPropertiesUid != "") {
			if (params.edmSiteUid != ""){
				this._schedulerEventDispatcher.fireLoadStartedEvent();
				this._node.commandBeanProxy.customInvoke(params, this.wellLoadCompleted, this);
			}
		}
	}

	DepotWellComboBox.prototype.getLookupRef = function() {
		return this._wellListLookup;
	};

	DepotWellComboBox.prototype.wellLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.well") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('well');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});
				this._wellListLookup = lookupReference;
				this._node.setFieldValue("depotWellUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.well"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	DepotWellComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	DepotWellComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._wellListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("depotWellUid", lookupItem.data);
		this._node.setFieldValue("depotWellName", lookupItem.label);
		this._onChanged(event);
	}

	DepotWellComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireWellChangedEvent(this._node.uid);
	}

	D3.inherits(DepotWellboreComboBox,D3.ComboBoxField);

	function DepotWellboreComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SITE_CHANGED, this.onSiteChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
	}

	DepotWellboreComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_POLICY_CHANGED, this.onPolicyChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_PROJECT_CHANGED, this.onProjectChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SITE_CHANGED, this.onSiteChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_WELL_CHANGED, this.onWellChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		DepotWellboreComboBox.uber.dispose.call(this);
	}

	DepotWellboreComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellboreComboBox.prototype.onPolicyChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellboreComboBox.prototype.onProjectChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellboreComboBox.prototype.onSiteChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotWellboreComboBox.prototype.onWellChanged = function(event) {
		this._refreshField(event);
	}

	DepotWellboreComboBox.prototype.clearList = function() {
		this._wellboreListLookup = null;
		this._node.setFieldValue("depotWellboreUid","");
		this.refreshFieldEditor();
	};

	DepotWellboreComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveWellboreList();
		}
	}

	DepotWellboreComboBox.prototype.getLookupRef = function() {
		return this._wellboreListLookup;
	};

	DepotWellboreComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotWellboreName");
		this.setFieldRendererValue(renderedText);
	}

	DepotWellboreComboBox.prototype.retrieveWellboreList = function() {
		var params = {};
		params._invokeCustomFilter = "get_wellbore_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		params.depotWellUid = this._node.getFieldValue("depotWellUid");
		if(params.depotWellUid != "" && params.schedulerTemplateUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.wellboreLoadCompleted, this);
		}
	}

	DepotWellboreComboBox.prototype.wellboreLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.wellbore") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('wellbore');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item) {
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});

				this._wellboreListLookup = lookupReference;
				this._node.setFieldValue("depotWellboreUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.wellbore"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	DepotWellboreComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._wellboreListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("depotWellboreUid", lookupItem.data);
		this._node.setFieldValue("depotWellboreName", lookupItem.label);
		this._onChanged(event);
	}

	DepotWellboreComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireWellboreChangedEvent(this._node.uid);
	}

	DepotWellboreComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}
	
	// Contractor Combobox
	D3.inherits(EdmContractorComboBox, D3.ComboBoxField);
	function EdmContractorComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
	}
	
	EdmContractorComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_SERVER_CHANGED, this.onServerChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		EdmContractorComboBox.uber.dispose.call(this);
	}

	EdmContractorComboBox.prototype.onServerChanged = function(event) {
		this._refreshField(event);
	}

	EdmContractorComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveContractorList();
		}		
	}

	EdmContractorComboBox.prototype.clearList = function() {
		this._contractorListLookup = null;
		this._node.setFieldValue("edmContractorUid","");
		this._node.setFieldValue("edmContractorName","");
		this.refreshFieldEditor();
	};

	EdmContractorComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("edmContractorName");
		this.setFieldRendererValue(renderedText);
	}

	EdmContractorComboBox.prototype.retrieveContractorList = function() {
		var params = {};
		params._invokeCustomFilter = "get_contractor_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		if(params.importExportServerPropertiesUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.contractorLoadCompleted, this);
		}
	}

	EdmContractorComboBox.prototype.getLookupRef = function() {
		return this._contractorListLookup;
	};

	EdmContractorComboBox.prototype.contractorLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.contractor") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('depotValue');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});
				this._contractorListLookup = lookupReference;
				this._node.setFieldValue("edmContractorUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.contractor"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	EdmContractorComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	EdmContractorComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._contractorListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("edmContractorUid", lookupItem.data);
		this._node.setFieldValue("edmContractorName", lookupItem.label);
		this._onChanged(event);
	}

	EdmContractorComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireContractorChangedEvent(this._node.uid);
	}
	
	// Rig Combobox
	D3.inherits(EdmRigComboBox, D3.ComboBoxField);
	function EdmRigComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_CONTRACTOR_CHANGED, this.onContractorChanged, this);
	}
	
	EdmRigComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_CONTRACTOR_CHANGED, this.onContractorChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		EdmRigComboBox.uber.dispose.call(this);
	}

	EdmRigComboBox.prototype.onContractorChanged = function(event) {
		this._refreshField(event);
	}

	EdmRigComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveRigList();
		}		
	}

	EdmRigComboBox.prototype.clearList = function() {
		this._rigListLookup = null;
		this._node.setFieldValue("edmRigUid","");
		this._node.setFieldValue("edmRigName","");
		this.refreshFieldEditor();
	};

	EdmRigComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("edmRigName");
		this.setFieldRendererValue(renderedText);
	}

	EdmRigComboBox.prototype.retrieveRigList = function() {
		var params = {};
		params._invokeCustomFilter = "get_rig_list";
		params.importExportServerPropertiesUid = this._node.getFieldValue("importExportServerPropertiesUid");
		params.edmContractorUid = this._node.getFieldValue("edmContractorUid");
		if(params.edmContractorUid != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.rigLoadCompleted, this);
		}
	}

	EdmRigComboBox.prototype.getLookupRef = function() {
		return this._rigListLookup;
	};

	EdmRigComboBox.prototype.rigLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.rig") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find('depotValue');
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});
				this._rigListLookup = lookupReference;
				this._node.setFieldValue("edmRigUid","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.rig"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	EdmRigComboBox.prototype._hasError = function(response) {
		var e = $(response).find('error');
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	EdmRigComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._rigListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("edmRigUid", lookupItem.data);
		this._node.setFieldValue("edmRigName", lookupItem.label);
	}
	
	// Depot Well Operation Mapping Datanode Listener
	D3.inherits(DepotWellImportNodeListener, D3.EmptyNodeListener);
	function DepotWellImportNodeListener() { 
		if (!this.dispatcher) {
			this._dispatcher = new D3.SchedulerEventDispatcher();
		}
	}

	DepotWellImportNodeListener.prototype.getCustomFieldComponent = function(id, node){
		var commandBean = node.commandBeanProxy;
		this._dispatcher._commandBean = commandBean;

		if(id == "serverProperties"){
			return new ServerPropertiesComboBox(this._dispatcher);
		} else if(id == "edmPolicy"){
			return new EdmPolicyComboBox(this._dispatcher);	
		} else if(id == "edmProject"){
			return new EdmProjectComboBox(this._dispatcher);	
		} else if(id == "edmSite"){
			return new EdmSiteComboBox(this._dispatcher);	
		} else if(id == "depotWell"){
			return new DepotWellComboBox(this._dispatcher);	
		} else if(id == "depotWellbore"){
			return new DepotWellboreComboBox(this._dispatcher);
		} else if(id == "edmContractor") {
			return new EdmContractorComboBox(this._dispatcher);
		} else if(id == "edmRig") {
			return new EdmRigComboBox(this._dispatcher);
		} else {
			return null;
		}
	}

	DepotWellImportNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		node.setFieldValue("schedulerStatus", "stop");
	}

	D3.CommandBeanProxy.nodeListener = DepotWellImportNodeListener;

})(window);