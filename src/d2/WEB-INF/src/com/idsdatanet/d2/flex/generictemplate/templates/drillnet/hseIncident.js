(function(window){
	
	D3.inherits(HseIncidentNodeListener,D3.EmptyNodeListener);
	
	function HseIncidentNodeListener()
	{

	}

	HseIncidentNodeListener.prototype.customButtonTriggered = function(node, id, button)
	{
		if (id == "add-hse-related" ) {
			if (window.parent){
				window.parent.tabsManager.select("hserelatedoperationsetup");
			}
		}
	
	}
	
	HseIncidentNodeListener.prototype.handleAddNewNode = function(parentNode, childClass){
		if (childClass == "LaggingIndicator" || childClass == "LeadingIndicator" || childClass == "OtherIndicator"){
			parentNode.commandBeanProxy.addAdditionalFormRequestParams("target_class",childClass);
			parentNode.commandBeanProxy.submitForServerSideProcess(null, null, true);
			return true;
		}else {
			return false;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = HseIncidentNodeListener;
	
})(window);