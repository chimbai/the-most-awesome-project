(function(window){
	
	D3.inherits(CombinedDDRSelectionField,D3.CheckboxField);
	CombinedDDRSelectionField.FIELD_COMBINE_DDR = "@includeCombinedDdr";
	function CombinedDDRSelectionField(){
		D3.CheckboxField.call(this);
	}
	
	CombinedDDRSelectionField.prototype.config = function(config) {
		if (!this._config){
			this._fieldName = CombinedDDRSelectionField.FIELD_COMBINE_DDR;
			config.label = D3.LanguageManager.getLabel("label.combinedReport");
		}
		CombinedDDRSelectionField.uber.config.call(this,config);
		
		
	};
	CombinedDDRSelectionField.prototype.setFieldValue = function(value) {
		if (value==="1")
			value="true";
		else if(value ==="0")
			value="false";
		CombinedDDRSelectionField.uber.setFieldValue.call(this,value);
	}
	
	CombinedDDRSelectionField.prototype.getFieldValue = function() {
		
		var value = CombinedDDRSelectionField.uber.getFieldValue.call(this);
		if (value == "true") return "1";
		if (value == "false") return "0";
		return value;
	}
	
	D3.inherits(DailySelectorField,D3.MultiSelectField);
	DailySelectorField.FIELD_SELECTED_DAYS = "@reportSelectedDays";
	function DailySelectorField(){
		D3.MultiSelectField.call(this);
	}
	DailySelectorField.prototype.getSelectedValue = function(){
		var str = this._node.getFieldValue(DailySelectorField.FIELD_SELECTED_DAYS);
		if (str){
			return str.split(",");
		}
	}
	DailySelectorField.prototype.setSelectedValueToField = function(selected){
		var value = null;
		if (selected){
			for(var i=0; i<selected.length;i++){
				var item = selected[i];
				if (!value)
					value = item;
				else
					value += "," + item;
			}
		}
		this._node.setFieldValue(DailySelectorField.FIELD_SELECTED_DAYS, value);
	}
	DailySelectorField.prototype.config = function(config) {
		if (!this._config){
			config.title = "Selected Days: ";
			config.titleOrientation = "horizontal";
		}
		DailySelectorField.uber.config.call(this,config);
		
		
	};
	
	DailySelectorField.prototype.getFieldEditor = function(){
		
		if (!this._fieldEditor){
			var fieldEditor = DailySelectorField.uber.getFieldEditor.call(this);
			$(fieldEditor).addClass("dailySelector");
		}
		return DailySelectorField.uber.getFieldEditor.call(this);
	}
	
	DailySelectorField.prototype.getMultiSelectLookupReference = function(){
		
		var params = {};
		
		params._invokeCustomFilter = "flexDdrReportLoadDayList";
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		var doc = $(response);
		
		var currentSelected = this._node.getFieldValue(DailySelectorField.FIELD_SELECTED_DAYS);
		var currentList = null;
		
		doc.find("Daily").each(function(item,index){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("dailyUid").text();
			newItem.label = item.find("dayNumber").text() + " ("+ item.find("dayDate").text()+")";
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	
	
	D3.inherits(EmptyField,D3.AbstractFieldComponent);
	
	function EmptyField(){
		D3.AbstractFieldComponent.call(this);
	}
	
	D3.inherits(DDRReportListener,D3.DefaultReportListener);
	
	function DDRReportListener()
	{
		D3.DefaultReportListener.call(this);
	}
	DDRReportListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "ddr_report_chooser"){
			var showAllDays = this._commandBeanProxy.rootNode.getFieldValue("@showAllDays") == "1"? true : false;
			if (!showAllDays){
				return new DailySelectorField();
			}else{
				return new EmptyField();
			}
		}else if (id=="ddr_combined_ddr_option"){
			return new CombinedDDRSelectionField();
		}else{
			return DDRReportListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	DDRReportListener.prototype.commandBeanLoadCompleted = function(event) {
		DDRReportListener.uber.commandBeanLoadCompleted.call(this,event);
		this._showAllDays = this._commandBeanProxy.rootNode.getFieldValue("@showAllDays") == "1"? true : false;
	}

	
	DDRReportListener.prototype.generateReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report","1");
		this._commandBeanProxy.submitForServerSideProcess(null, null, this._showAllDays);
		
		this._report_job_started = true;
	}
	
	DDRReportListener.prototype.generateAndSendReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_and_sent","1");
		this._commandBeanProxy.submitForServerSideProcess(null, null, this._showAllDays);
		
		this._report_job_started2 = true;
	}
	
	
	
	DDRReportListener.prototype.generateAndDownloadReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report_and_download","1");
		this._commandBeanProxy.submitForServerSideProcess(null, null, this._showAllDays);
		
		this._report_job_started3 = true;
	}
	
	
	D3.DDRReportListener = DDRReportListener;
	D3.CommandBeanProxy.nodeListener = DDRReportListener;
})(window);