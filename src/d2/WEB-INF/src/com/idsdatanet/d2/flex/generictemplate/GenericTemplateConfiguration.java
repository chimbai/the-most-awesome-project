package com.idsdatanet.d2.flex.generictemplate;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.i18n.MultiLangLabelService;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.flex.FlexStartupConfigManager;
import com.idsdatanet.d2.core.web.mvc.flex.FlexTemplateLoader;
import com.idsdatanet.d2.core.web.mvc.flex.GenericTemplateStartupConfig;

public class GenericTemplateConfiguration extends AbstractFlashComponentConfiguration {
	private FlexTemplateLoader flexTemplateLoader = null;
	
	public GenericTemplateConfiguration() throws Exception {
		this.flexTemplateLoader = new FlexTemplateLoader();
	}
	
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		GenericTemplateStartupConfig config = null;
		String configId = request.getParameter("configId");
		if(StringUtils.isNotBlank(configId)){
			FlexStartupConfigManager manager = userSession.getFlexStartupConfigForCommandBean();
			config = manager.getConfig(configId, GenericTemplateStartupConfig.class);
		}else if(StringUtils.isNotBlank(request.getParameter(GenericTemplateStartupConfig.STARTUP_CONFIG))){
			config = GenericTemplateStartupConfig.fromJSONString(request.getParameter(GenericTemplateStartupConfig.STARTUP_CONFIG));
		}
		if(config == null) throw new Exception("Startup config not found for: " + configId);
		return "commandBeanUrl=" + urlEncode(CommonUtils.urlPathConcat(userSession.getClientBaseUrl(), config.getCommandBeanUrl())) + "&config=" + urlEncode(this.getTemplate(config)) + "&baseUrl=" + urlEncode(userSession.getClientBaseUrl()) + "&gotoFieldName=" + urlEncode(config.getGotoFieldName()) + "&gotoFieldUid=" + urlEncode(config.getGotoFieldUid()) + "&debug=" + urlEncode(config.getDebugFlag()) + "&serverInstanceId=" + urlEncode(ApplicationUtils.getConfiguredInstance().getServerInstanceId()) + "&pageNo=" + urlEncode(config.getPageNo()) 
			+ "&gwpDblClickEdit=" + urlEncode(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_DOUBLE_CLICK_TO_EDIT)) 
			+ "&gwpAutoScroll=" + urlEncode(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_FLEX_ENABLE_AUTO_SCROLL))
			+ "&gwpDragScroll=" + urlEncode(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_FLEX_ENABLE_DRAG_SCROLL))
			+ "&gwpFirstDayOfWeek=" + urlEncode(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_DEFAULT_FIRST_DAY_OF_WEEK_IN_CALENDAR))
			+ "&gwpDefaultTimeInterval=" + urlEncode(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_DEFAULT_TIME_INTERVAL))
			+ (userSession.isAutomationTestingMode() ? "&testingPlugin=TestingPlugin.swf" : "");
	}
	
	private String getTemplate(GenericTemplateStartupConfig config) throws Exception {
		String userLanguage = config.getLanguage();
		boolean isMultiLangEnabled = StringUtils.isNotBlank(userLanguage);
		//trigger it to force collect if template content is changed            
        Boolean forceCollect = this.flexTemplateLoader.isTemplateUpdated(config.getTemplateFile());
		String flexTemplate = this.flexTemplateLoader.loadTemplateFile(config.getTemplateFile(), false);
		if (isMultiLangEnabled) {
			return MultiLangLabelService.processContent(new Locale(userLanguage), config.getVariant(), config.getTableList(), flexTemplate,forceCollect,config.getTemplateFile());
		} else {
			return flexTemplate;
		}
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "GenericTemplateRenderer";
	}
	
	@Override
	public String getAdditionalHtmlHeaderContent(String targetSubModule, UserSession userSession, HttpServletRequest request) throws Exception {
		return "<script language=\"JavaScript\" type=\"text/javascript\">" +
		"function reloadParentHtmlPage(clearQueryString,params){parent.d2ScreenManager.reloadParentHtmlPage(clearQueryString,params);}" +
		"function submitSummary(dailyUid){parent.d2ScreenManager.submitSummary(dailyUid);}" +
		"function setBlockUI(block){" +
		"  if(block){"+
		"    parent.d2ScreenManager.toggleMainBlockUI();" +
		"  }else{" +
		"    parent.d2ScreenManager.toggleMainUnblockUI();" +
		"  }" +
		"}" +
		"function gotoUrl(url,msg){parent.d2ScreenManager.gotoToUrl(url,msg);}" +
		"function npdAjaxSubmit(id, service, versionKind) {" +
		"  parent.d2ScreenManager.npdAjaxSubmit(id,service,versionKind);" +
		"}" +
		"function gotoTightHoleSetupTab(){" +
		"  parent.d2ScreenManager.gotoTightHoleSetupTab();" + 
		"}" +
		"function gotoActivityMatrixTab(){" +
		"  parent.d2ScreenManager.gotoActivityMatrixTab();" + 
		"}" +
		"function gotoActivityCodeDisplayTab(){" +
		"  parent.d2ScreenManager.gotoActivityCodeDisplayTab();" + 
		"}" +
		"function gotoGasGraphTab(){" +
		"  parent.d2ScreenManager.gotoGasGraphTab();" + 
		"}" +
		"function gotoHseRelatedOperationSetupTab(){" +
		"  parent.d2ScreenManager.gotoHseRelatedOperationSetupTab();" + 
		"}" +
		"function gotoPersonnelOnSiteSummaryTab(){" +
		"  parent.d2ScreenManager.gotoPersonnelOnSiteSummaryTab();" + 
		"}" +
		"function gotoPobMasterListTab(){" +
		"  parent.d2ScreenManager.gotoPobMasterListTab();" + 
		"}" +
		"function gotoQcStatusTab(){" +
		"  parent.d2ScreenManager.gotoQcStatusTab();" + 
		"}" +
		"function gotoLessonsearchresultTab(){" +
		"  parent.d2ScreenManager.gotoLessonsearchresultTab();" +
		"}" +
		"function gotoAdvancedlessonsearchresultTab(){" +
		"  parent.d2ScreenManager.gotoAdvancedlessonsearchresultTab();" +
		"}" +
		"function getFlexApp() {" +
		"	if(navigator.appName.indexOf(\"Microsoft\") != -1){" +
		"		return window[\"GenericTemplateRenderer\"];" +
		"	}else{" +
		"		return document[\"GenericTemplateRenderer\"];" +
		"	}" +
		"}" +
		"function callbackEvent(eventId,eventData){" +
		"	getFlexApp().callbackEvent(eventId,eventData);" +
		"}" +
		"function launchFrame(url){" +
		"	window.parent.launchFileManagerFrame(url,window );" +
		"}"
		
		+ (userSession.isAutomationTestingMode() ? this.loadTestingScript() : "") +
		
		"</script>";
	}
	
	private String loadTestingScript() throws Exception {
		InputStream in = this.getClass().getResourceAsStream("TestingScript.js");
		if(in == null) return "";
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[2048];
		int total = 0;
		while((total = in.read(buffer)) != -1){
			out.write(buffer, 0, total);
		}
		in.close();

		return new String(out.toByteArray(), "utf-8");
	}
}
