(function(window){
	
	function CostDailysheet(){
		
		this.costDailysheetUid = "";
		
		this.sequence = "";
		
		this.isTangible = "No";
		this.category = "";		
		this.subcategory = "";	
		this.parentAccountCode = "";
		this.accountCode = "";
		this.afeShortDescription = "";
		this.afeItemDescription = "";
		this.afeGroupName = "";
		this.planReference = "";
		this.planReferenceName = "";
		this.lookupCompanyUid = "";
		this.lookupCompanyName = "";
		this.afeItemDescription = "";
		this.itemDescription = "";
		this.shortDescription = "";
		this.longDescription = "";
		
		this.classCode = "";
		this.className = "";
		this.phaseCode = "";
		this.phaseName = "";
		this.taskCode = "";
		this.taskName = "";
		this.rootCauseCode = "";
		this.rootCauseName = "";
		
		this.isAdjusted = "";
		this.recurringItem = "No";
		this.referenceNumber = "";
		
		this.quantity = 0.0;
		this.itemUnit = "";
		this.currency = "";
		this.currencyRate = 1.0;
		this.itemCost = 0.0;
		this.itemTotal = 0.0;
		this.itemTotalInPrimaryCurrency = 0.0;
		
		
		this.ticketNumber = "";
		this.chargeScope = "";
		
		this.action = "";
	}
	
	CostDailysheet.prototype.run = function(){
		if (this.isTangible== "false") this.isTangible= "No";
		if (this.isTangible== "true") this.isTangible= "Yes";
		if (this.recurringItem== "false") this.recurringItem= "No";
		if (this.recurringItem== "true") this.recurringItem= "Yes";
		
		if (this.category == "") this.category = "-";
		if (this.subcategory == "") this.subcategory = "-";
		this.itemTotalInPrimaryCurrency = this.itemTotal;
		this.itemTotal = this.itemCost * this.quantity;
		
		if (this.accountCode=="undefined") this.accountCode = "";
		if (this.afeShortDescription=="undefined") this.afeShortDescription = "";
		
		this.accountCode = (this.parentAccountCode==""?"-":this.parentAccountCode) + " :: " + (this.accountCode==""?"-":this.accountCode) + " :: " + this.afeShortDescription;
	}
	
	CostDailysheet.prototype.runBeforeSave = function(){
		this.isTangible = (this.isTangible=="Yes"?true:false);
		this.recurringItem = (this.recurringItem=="Yes"?true:false);
		if (this.currencyRate==undefined) this.currencyRate = 1.0;
		this.itemTotal = (this.itemCost * this.quantity) / this.currencyRate;
		
		var res = this.accountCode.split(" :: ");
		this.accountCode = res[1] + " :: " + res[2];
	}
	
	window.CostDailysheet = CostDailysheet;
})(window);