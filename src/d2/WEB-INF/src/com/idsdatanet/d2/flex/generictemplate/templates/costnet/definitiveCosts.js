(function(window){

	
	D3.inherits(DefinitiveCostNodeListener,D3.EmptyNodeListener);
	
	function DefinitiveCostNodeListener() {
		this.baseUrl = "";
		this.wellName="";
	}
	
	DefinitiveCostNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
	
		 if (id == "lockData") {	
			this._commandBeanProxy = commandBeanProxy;	
			this._btnLockData = document.createElement("button");
			this._btnLockData.className = "rootButtons";
			setTimeout(() => {  
					this.getLockStatus();	
			}, 100);
//			if(!this.isLocking){
//				var label = "Lock Data";
//				var addNew = document.createTextNode(label);
//				this._btnLockData.appendChild(addNew);
//				this._btnLockData.onclick = this.lockData.bindAsEventListener(this);
//				
//			}
//			else{
				var label="Lock Data";
				var addNew = document.createTextNode(label);
				this._btnLockData.appendChild(addNew);
				this._btnLockData.onclick = this.lockData.bindAsEventListener(this);
//			}
		
				return this._btnLockData;

			
					
		}
		 if (id == "exportData") {
				this._commandBeanProxy = commandBeanProxy;
				
				this._btnExportData = document.createElement("button");
				this._btnExportData.className = "rootButtons";
				this._btnExportData.id="btn-export";
				var label = "Export";
				var addNew = document.createTextNode(label);
				this._btnExportData.appendChild(addNew);
				this._btnExportData.onclick = this.btn_exportData.bindAsEventListener(this);
				return this._btnExportData;
		 }
	}
	
		 DefinitiveCostNodeListener.prototype.btn_exportData = function(){ 
				this.getWellName();
			}
			
		 DefinitiveCostNodeListener.prototype.exportToExcel = function(fileName){
				var dataType = 'application/vnd.ms-excel';
		    	var tableSelect = document.getElementsByClassName('SimpleGridTable');
				 /* Declaring array variable */
		    	var rows =[];

				var htmls = "<table id='tests' >" ;

				for(var i=0,row; row = tableSelect[0].rows[i];i++){
					if (i==0){
						 htmls= htmls="<thead><tr>";
						 htmls= htmls+"<td style='width:400px'>"+row.cells[1].innerText+"</td>"+
								"<td style='width:400px'>"+row.cells[2].innerText+"</td>"+

								"</tr><br> "
						 htmls="</thead>";
					}
					
					htmls = htmls + "<tbody><tr>";
					
					htmls = htmls + "<td style='width:400px'>"+row.cells[1].innerText+"</td>"+
									"<td style='width:400px'>"+row.cells[2].innerText+"</td>"+

									"</tr><br> ";
					
				}
				htmls = htmls + "</tbody></table>";
			
				var uri = 'data:application/vnd.ms-excel;base64,'
			          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			          , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))); }
			          , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }); };
			          table=htmls;
			          var ctx = { worksheet: 'Definitive Costs' || 'Definitive Costs', table: htmls };
			        var filename=fileName+'.xls'; 
			        var link = document.createElement("a");
					link.setAttribute("href", uri + base64(format(template, ctx)));
					link.setAttribute("download", filename);
					document.body.appendChild(link);
					link.click();  
			       
			}
		 DefinitiveCostNodeListener.prototype.getWellName = function(){
				if(this._commandBeanProxy!=undefined)this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace(this.baseUrl,"webservice/costnetservice.html?method=");
				this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace("definitivecosts.html","webservice/costnetservice.html?method=");

				var page = this.baseUrl + "export_final_cost_well_record";
				var self = this;
				$.ajax({
					url:page,
					dataType: "xml",
					success: function (result) {
						var wellName = result.documentElement.getElementsByTagName("wellName")[0].textContent;
						self.wellName = wellName;
					}
				}).done(function(data, textStatus, jqXHR) {
					fileName="DC "+self.wellName;
					self.exportToExcel(fileName);
				});
				
			}		
	
		 DefinitiveCostNodeListener.prototype.getLockStatus = function(){
		if(this._commandBeanProxy!=undefined)this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace("definitivecosts.html","webservice/costnetservice.html?method=");
		var page = this.baseUrl + "get_lock_status";
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var allowLock = result.documentElement.getElementsByTagName("allowLock")[0].textContent;
				var isLock = result.documentElement.getElementsByTagName("isLock")[0].textContent;
				if (allowLock=="true") self.allowLocking = true;
				if (isLock=="true") self.isLocking = true;
			}
		}).done(function(data, textStatus, jqXHR) {
			self.changeLockLabel(self.isLocking,self.allowLocking);			
		});
		
	}
	DefinitiveCostNodeListener.prototype.changeLockLabel = function(lockStatus,allowLocking){
		
		if(lockStatus){
			this._btnLockData.childNodes[0].textContent="Unlock Data";
			this._commandBeanProxy.commandBeanModel.showAddNewButton=false;
			document.getElementsByClassName("RootButton")[0].style="display:none;";
			document.getElementsByClassName("RootButton")[1].style="display:none;";
			document.getElementsByClassName("RootButton")[2].style="display:none;";	
			document.getElementsByClassName("DefaultButton")[0].style="display:none";
			document.getElementsByClassName("DefaultButton")[2].style="display:none";
			document.getElementsByClassName("RecordActionsHeader")[0].style="display:none";
			items=document.getElementsByClassName("RecordActionsContainer");
			for (doc in items){
				items[doc].style="display:none";
			}

			
			
		}else{	
			this._btnLockData.childNodes[0].textContent="Lock Data";	
			items=document.getElementsByClassName("RecordActionsContainer");

		}
		if(!allowLocking){
			this._btnLockData.style="display:none;";
		}
		
	}
	DefinitiveCostNodeListener.prototype.doneAndReload = function(response) {
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	DefinitiveCostNodeListener.prototype.lockData = function(event) {
		this._node = this._commandBeanProxy.rootNode;
		if(!this.isLocking){
			
		}
		if (this._node.children.DefinitiveCost._items.length==0){
			alert("There is no data today.");
		}
		else {
			this.baseUrl = this._commandBeanProxy.commandBeanUrl.replace("definitivecosts.html","webservice/costnetservice.html?method=");

			if (this.isLocking){
				this.isLocking=false;
				var params = {};
				params.isLock = "false";
//				this.getProgressStatus(true, false, "UnLocking...");
		
				var self = this;

				var url = this.baseUrl + "lock_definitive_cost_record";
				$.post(url, params, function(response) {
					setTimeout(function(){ 
						self.doneAndReload(response)},1000);
				}, "xml");
			}
			else {
				this.isLocking=true;
				var params = {};
				params.isLock = "true";
//				this.getProgressStatus(true, false, "Locking...");
				
				var self = this;
				var url = this.baseUrl + "lock_definitive_cost_record";
		
				$.post(url, params, function(response) { 
					alert("Data in this screen has been reviewed and locked. Only PIC with Lock/Unlock access can unlock record.");
					setTimeout(function(){ 					
self.doneAndReload(response)},1000);
				}, "xml");
			}
		}		
	}
	
	
	
	D3.CommandBeanProxy.nodeListener = DefinitiveCostNodeListener;
	
})(window);