(function(window){
	
	function CostAfeTabs(){
		this.tabs = null;
		this.tabx = null;
		//this.tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";
		this.tabTemplate = "<li><a href='#{href}' style='color: blue;''>#{label}</a></li>";
		this.tabCounter = 0;
		
		this.option = null;
		this.masterUid = "";
		this.costAfeMasterList = [];
		this.h = null;
		this.currentUser = "";
	}
	
	CostAfeTabs.prototype.init = function (option){
		this.option = option;
//		 var str = "http://localhost:8080/d2/newcostnet3.html";
//		  var res = str.substr(0, str.indexOf("newcostnet3.html"));
		var str = this.option.baseUrl;
		if (str.indexOf("newcostnet2.html")>0){
			this.option.baseUrl = this.option.baseUrl.substr(0, str.indexOf("newcostnet2.html"));
		}
		if (str.indexOf("costafemaster.html")>0){
			this.option.baseUrl = this.option.baseUrl.substr(0, str.indexOf("costafemaster.html"));
		}
		this.option.baseUrl = this.option.baseUrl + "webservice/costnetservice.html?method=";
	//	this.option.baseUrl = this.option.baseUrl.replace("newcostnet2.html", "webservice/costnetservice.html?method=");
	//	this.option.baseUrl = this.option.baseUrl.replace("costafemaster.html", "webservice/costnetservice.html?method=");
		
		// === Set Height Control == 
		var height = {
			tabHeight: 	this.option.height,
			secHeight:  this.option.height - 210,
			popHeight:  this.option.height - 300,
		};
		this.h = height;
		// +++++++++++++++++++++++++++++++++++++++++++++  this.h.tabHeight
		var $tabs = $("#tabs");
		$tabs.css('height', "100%").css("box-sizing","border-box").css("padding","0");
		$tabs.css('overflow', "hidden");
		this.tabx = $tabs;
		var self = this;
		this.tabs = $tabs.tabs({
			activate: function(event, ui){
				var afeUid = ui.newPanel.attr("id");
				if (afeUid != undefined){
					if (afeUid=="addnew") {
						self.addNewAFE();
					}else{
						var afetype = self.costAfeMasterList.find(x => x.costAfeMasterUid === afeUid).afeType;
						self.tabsdata[afeUid].getLoadAFE(afeUid, afetype);
						self.tabsdata[afeUid].onTabActivated();
					}
				}
			}
		});
		
		this.tabContent = $( "#tab_content" );
		$("#add_tab").click(this.addTabClicked.bind(this));
		$("#btn_currency").click(this.btnCurrencyClick.bind(this));
		$("#btn_test1").click(this.btn_test1Click.bind(this));

		this.tabs.on( "click", "span.ui-icon-close", function() {
	        var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
	        $( "#" + panelId ).remove();
	        tabs.tabs( "refresh" );
	      });
	//	this.loadAfeData();
		this.getUsersession();
	}
	
	CostAfeTabs.prototype.addNewAFE = function(){
		var afeUid =  $("#tabs .ui-tabs-panel:visible").attr("aria-labelledby");
		$( "#" + afeUid ).remove();
		$( "#addnew" ).remove();
		$("#tabs .ui-state-focus").remove();
		 $("#tabs .ui-tabs-panel:visible").remove();
		this.tabs.tabs( "refresh" );
		this.addTabClicked();
	}
	
	CostAfeTabs.prototype.btn_test1Click = function(){
		this.tabs.tabs( "refresh" );
	}
	
	CostAfeTabs.prototype.btnCurrencyClick = function (){
		var cc = new CostCurrency();  
		cc.init({
			baseUrl : this.option.baseUrl,
			masterAfeUid : this.masterUid,
		});
		cc.showPopup();
	}
	
	CostAfeTabs.prototype.resetTab = function (){
	//	alert("Reset");
	//	this.tabs.tabs('destroy').tabs();
	//	$('#tabs').tabs('remove', 2);
	}
	
	CostAfeTabs.prototype.selTabClicked = function (){
		location.reload(); 
	}
	
	CostAfeTabs.prototype.actTabClicked = function (){
		var xx = $("#tabs .ui-tabs-panel:visible").attr("id");
		alert(xx);
	}
	
	CostAfeTabs.prototype.addTabClicked = function (){
		var d = new Date();
		var n = d.getTime();
		
	//	var label = "(Supplementary)";
		var label = "";
		if (this.masterUid=="") label = "(Master)";
		
		var id = "afe" + n;
		var li = $( this.tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) );
		var tabContentHtml = this.tabContent.val() || "Tab " + this.tabCounter + " content.";
			
		this.tabs.find( ".ui-tabs-nav" ).append( li );
		this.tabs.append( "<div id='" + id + "'></div>" );
		this.tabs.tabs( "refresh" );
		
		var costMas = new CostAfeMaster();
		
		costMas.init({
			baseUrl : this.option.baseUrl,
			height : this.h.secHeight,
			width :  this.option.width,
			popHeight : this.h.popHeight,
			counter : this.tabCounter,
			parentAfe : this.masterUid,
			currentUser : this.currentUser,
			lang : this.option.lang,
			isNew: true
		});
		this.tabsdata[id] = costMas;
		$("#" + id).append(costMas.jqGridMain);
		this.tabCounter ++;
		
		this.tabs.tabs( "option", "active", this.tabCounter - 1);
		var tabId = $("#tabs .ui-tabs-panel:visible").attr("id");
		this.tabsdata[tabId].getLoadNewAFE(tabId);
		$("#add_tab").addClass("hide");
		$("#btn_currency").addClass("hide");
	}
	
	CostAfeTabs.prototype.addTabClicked2 = function (){
	      var label =  "Tab " + this.tabCounter;
	      var id = "tabs-" + this.tabCounter;
	      var li = $( this.tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) );
	      var tabContentHtml = this.tabContent.val() || "Tab " + this.tabCounter + " content.";
	      
	      this.tabs.find( ".ui-tabs-nav" ).append( li );
	      this.tabs.append( "<div id='" + id + "'>" + tabContentHtml + "</div>" );
	      this.tabs.tabs( "refresh" );
	      this.tabCounter++;
	}

	CostAfeTabs.prototype.getUsersession = function(){
		var page = this.option.baseUrl + "get_session_id";
		var self = this;
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var currentU = result.documentElement.getElementsByTagName("currentUser")[0].textContent;
				self.currentUser = currentU;
			}
		}).done(function(data, textStatus, jqXHR) {
			self.loadAfeData();
		});
	}
	
	CostAfeTabs.prototype.loadAfeData = function(){
		console.log("loadAfeData");
		var page = this.option.baseUrl + "get_afe_listing";
		page = page + "&costAfeMasterUid=" + this.selectedAfe;
		var self = this;
		this.costAfeMasterList = [];
		$.ajax({
			url:page,
			dataType: "xml",
			success: function (result) {
				var afeMaster = result.documentElement.getElementsByTagName("CostAfeMaster");
				var afeSection = "";
				for(var i=0; i<afeMaster.length; i++){
					var recMas = new CostAfeMas();
					for(var j in afeMaster[i].children){
						var prop = afeMaster[i].children[j].tagName;
						if(recMas.hasOwnProperty(prop)){
							if(afeMaster[i].children[j].hasChildNodes()){
								recMas[prop] = afeMaster[i].children[j].childNodes[0].nodeValue;
							}
						}
					}
					self.costAfeMasterList.push(recMas);
				}
				console.log(self.costAfeMasterList);
			}
		}).done(function(data, textStatus, jqXHR) {
			 self.tabsUpdate();
		});
		
		console.log("loadAfeData done");
	}
	
	CostAfeTabs.prototype.createTabHeader = function(id, label){
		var li = document.createElement("li");
		var a = document.createElement("a");
		a.style.visibility = "hidden";
		a.style.position = "absolute";
		a.href = "#" + id;
		var s = document.createElement("span");
		s.style.color = "blue";
		s.className = "ui-tabs-anchor";
		s.appendChild(document.createTextNode(label));
		li.appendChild(a);
		li.appendChild(s);
		s.onclick = function(){$(a).trigger("click")};
		return li;
	}
	
	CostAfeTabs.prototype.tabsUpdate = function(){
		this.tabsdata = {};
		for(var i=0; i< this.costAfeMasterList.length; i++){
			var cMas = this.costAfeMasterList[i];

			var label = "";
			if (cMas.parentCostAfeMasterUid=="SUPPLEMENTARY"){
				// label = (cMas.afeNumber==null?"":cMas.afeNumber) + " (Supplementary)";
				label = (cMas.afeNumber==null?"":cMas.afeNumber);
			} else {
				label = (cMas.afeNumber==null?"":cMas.afeNumber) + " (Master)";
			}
			
			var id = cMas.costAfeMasterUid;
			if (cMas.parentCostAfeMasterUid!="SUPPLEMENTARY"){
				this.masterUid = id;
			}
			var li = this.createTabHeader(id, label);
			//var tabContentHtml = this.tabContent.val() || "Tab " + this.tabCounter + " content.";
			
			if (cMas.afeType=="PRICELIST"){
				//$(li).css('background', "#00FFB5");
				$(li).addClass("pricelist");  
			}
			if (cMas.afeType=="SO"){
				//$(li).css('background', "#00FFB5");
				$(li).addClass("so");  
			}
			this.tabs.find( ".ui-tabs-nav" ).append( li );
			this.tabs.append( "<div id='" + id + "'></div>" );
			this.tabs.tabs( "refresh" );
			
			
			var costMas = new CostAfeMaster(); 
			
			costMas.init({
				baseUrl : this.option.baseUrl,
				height : this.h.secHeight,
				width :  this.option.width,
				popHeight : this.h.popHeight,
				counter : this.tabCounter,
				parentAfe : this.masterUid,
				currentUser : this.currentUser,
				tabli : li,
				lang : this.option.lang,
				currencyClick: this.btnCurrencyClick.bindAsEventListener(this)
			});
			this.tabsdata[id] = costMas;
			$("#" + id).append(costMas.jqGridMain);
			
			this.tabCounter ++;
		}
		if (this.costAfeMasterList.length>=1){
			this.tabs.tabs( "option", "active", 0);
		}else{
			//$("#add_tab")[0].textContent = "Add New Master AFE";
		}
		
		if (this.costAfeMasterList.length>=1){
//			var label = "+";
//			
//			var id = "addnew";
//			var li = $( this.tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) );
//			var tabContentHtml = this.tabContent.val() || "Tab " + this.tabCounter + " content.";
//		 
//			this.tabs.find( ".ui-tabs-nav" ).append( li );
//			this.tabs.append( "<div id='" + id + "'></div>" );
//			this.tabs.tabs( "refresh" );
		}else{
//			this.addTabClicked();
			this.tabs.find( ".ui-tabs-nav" ).append( "No AFE has been created.");
			alert("No AFE has been created.");
		}
	}
	
	window.CostAfeTabs = CostAfeTabs;
	
	
})(window);