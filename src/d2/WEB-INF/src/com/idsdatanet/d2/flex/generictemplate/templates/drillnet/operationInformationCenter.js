(function(window){
	
	D3.inherits(NbbHyperlink,D3.HyperLinkField);
	
	function NbbHyperlink(){
		D3.HyperLinkField.call(this);
		this._placeHolder = "{nbbName}";
	}
	
	NbbHyperlink.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		if (!this._fieldRenderer){
			fieldRenderer = NbbHyperlink.uber.getFieldRenderer.call(this);
			fieldRenderer.removeAttribute("target");
			fieldRenderer.removeAttribute("href");
			if (this.getFieldValue() && this._config.hyperlink){
				fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
			}
		}else{
			fieldRenderer = NbbHyperlink.uber.getFieldRenderer.call(this);
		}			
		return fieldRenderer;
	}
	
	NbbHyperlink.prototype.onclick = function(){
		var url = this._config.hyperlink;
		url = url.replace(this._placeHolder,this.getFieldValue());
		this._node.commandBeanProxy.gotoUrl(url,"Redirecting to " + this.getFieldValue());
	}
	
	
	
	D3.inherits(OICOperationNameFilter,D3.AbstractFieldComponent);
	
	function OICOperationNameFilter(){
		this._renderer = null;
		this._textInput = null;
		this._btnRefresh = null;
		
		this._comboBoxOperationType = null;
		this._comboBoxCountry = null;
		this._comboBoxDate = null;
	}
	
	OICOperationNameFilter.prototype.refreshFieldRenderer = function(){
		$(this._textInput).val(this.getFieldValue());
	}
	
	OICOperationNameFilter.prototype.getFieldRenderer = function(){
		if(this._renderer == null){
			this._renderer = D3.UI.createElement({tag:"span",css:"fieldRenderer"});
			
			if (this._textInput ==null) {
				var textConfig = {
						tag:"input",
						css:"fieldEditor textInputField",
						attributes:{},
						callbacks:{
							onkeydown:this.txtInput_onKeyDown,
							onblur:this.dataChangeListener
						},
						context:this
				}
				
				if (this._config) {
					var toolTip = this._config.toolTip;
					if (toolTip) {
						textConfig.attributes.title = toolTip;
					}
				}
				this._textInput = D3.UI.createElement(textConfig);
				
				
				this._renderer.appendChild(this._textInput);
			}
			
			if (this._comboBoxOperationType ==null) {
				this._createOperationType_OICFilterDropdown(this._renderer,"operationType");
			}
			
			if (this._comboBoxCountry ==null) {
				this._createCountry_OICFilterDropdown(this._renderer,"country");
			}
			
			if (this._comboBoxDate ==null) {
				this._createDate_OICFilterDropdown(this._renderer);
			}
			
			if (this._button ==null) {
				var btnConfig = {
						tag:"button",
						text:D3.LanguageManager.getLabel("button.operationNameFilter.refresh"),
						css:"DefaultButton",
						callbacks:{
							onclick:this.btnRefresh_onClick
						},
						context:this
				}
				this._btnRefresh = D3.UI.createElement(btnConfig);
				this._renderer.appendChild(this._btnRefresh);
			}
		}
		
		return this._renderer;
	}
	
	OICOperationNameFilter.prototype.loadOICLookupFilter = function(type){
		var params = {};
		
		params._invokeCustomFilter = "loadOICLookupFilter";
		params._action = type;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();

		$(response).find('item').each(function(){
			var newItem = new D3.LookupItem();
			newItem.data = $(this).attr('key');
			newItem.label = $(this).attr('value');
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	
	OICOperationNameFilter.prototype.setSelectedValue = function(comboBox, selectedValue) {
		for (var i=0; i < comboBox.length; i++) {
			if (comboBox.options[i].value == selectedValue) {
				comboBox.selectedIndex = i;
				break;
			}
		}
	}
	
	OICOperationNameFilter.prototype._createOperationType_OICFilterDropdown = function(container, type) {
		this._operationType = this.loadOICLookupFilter(type);
		
		var rendererTextAttributes = {};
		rendererTextAttributes.size = 1;
		
		this._comboBoxOperationType = this.createElement("select",null,"fieldEditor comboBox",rendererTextAttributes);
		var emptyOpt = document.createElement("option");
		emptyOpt.value = "";
		emptyOpt.textContent = "";
		this._comboBoxOperationType.appendChild(emptyOpt);
		
		
		this._operationType.forEachActiveLookup(this, function(item) {
			if (item && !item.firstBlankItem) {
				var option = document.createElement("option");
				option.value = item.data;
				option.appendChild(document.createTextNode(item.label));
				this._comboBoxOperationType.appendChild(option);
			}
		});

		this.setSelectedValue(this._comboBoxOperationType, this._node.getFieldValue("@operationTypeFilter"));
		
		container.append(" " + D3.LanguageManager.getLabel("label.filter.operationType") + " ");
		container.append(this._comboBoxOperationType);
	}
	
	OICOperationNameFilter.prototype._createCountry_OICFilterDropdown = function(container, type) {
		this._country = this.loadOICLookupFilter(type);
		
		var rendererTextAttributes = {};
		rendererTextAttributes.size = 1;
		
		this._comboBoxCountry = this.createElement("select",null,"fieldEditor comboBox",rendererTextAttributes);
		var emptyOpt = document.createElement("option");
		emptyOpt.value = "";
		emptyOpt.textContent = "";
		this._comboBoxCountry.appendChild(emptyOpt);
		
		
		this._country.forEachActiveLookup(this, function(item) {
			if (item && !item.firstBlankItem) {
				var option = document.createElement("option");
				option.value = item.data;
				option.appendChild(document.createTextNode(item.label));
				this._comboBoxCountry.appendChild(option);
			}
		});

		this.setSelectedValue(this._comboBoxCountry, this._node.getFieldValue("@countryFilter"));
		
		container.append(" " + D3.LanguageManager.getLabel("label.filter.country") + " ");
		container.append(this._comboBoxCountry);
	}
	
	OICOperationNameFilter.prototype._createDate_OICFilterDropdown = function(container) {
		var dateRange = [2,6,12];
		this._date = dateRange;
		
		var rendererTextAttributes = {};
		rendererTextAttributes.size = 1;
		
		this._comboBoxDate = this.createElement("select",null,"fieldEditor comboBox",rendererTextAttributes);
		var emptyOpt = document.createElement("option");
		emptyOpt.value = "";
		emptyOpt.textContent = "";
		this._comboBoxDate.appendChild(emptyOpt);
		
		for (var i=0; i < this._date.length; i++) {
				var option = document.createElement("option");
				option.value = this._date[i];
				option.appendChild(document.createTextNode(D3.LanguageManager.getLabel("label.filter.date."+this._date[i])));
				this._comboBoxDate.appendChild(option);
		}

		this.setSelectedValue(this._comboBoxDate, this._node.getFieldValue("@dateFilter"));
		
		container.append(" " + D3.LanguageManager.getLabel("label.filter.date") + " ");
		container.append(this._comboBoxDate);
	}
	
	OICOperationNameFilter.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	OICOperationNameFilter.prototype.txtInput_onKeyDown = function(event){
		if (event.currentTarget == this._textInput) {
			if (event.keyCode == 13) {
				this.searchText();
			}
		}
	}
	
	OICOperationNameFilter.prototype.searchText = function(){
		this.setFieldValueNow();
		var searchText = this._node.getFieldValue(this._fieldName);
		
		var operationType = this._comboBoxOperationType.value;
		if (operationType==null || operationType==""){
			var operationType = " ";
		}
		
		var country = this._comboBoxCountry.value;
		if (country==null || country==""){
			country = " ";
		}
		
		var date = this._comboBoxDate.value;
		if (date==null || date==""){
			date = " ";
		}
		
		var all = searchText + "::" + operationType + "::" + country + "::" + date;
		this._node.setFieldValue("@OICFilter", all);
		this._node.setFieldValue("@operationTypeFilter", this._comboBoxOperationType.value);
		this._node.setFieldValue("@countryFilter", this._comboBoxCountry.value);
		this._node.setFieldValue("@dateFilter", this._comboBoxDate.value);
		
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@OICFilter");
	}
	OICOperationNameFilter.prototype.dataChangeListener = function(event){
		if (event.target === this._textInput)
		this.setFieldValueNow();
	}
	OICOperationNameFilter.prototype.btnRefresh_onClick = function(event){
		this.searchText();
	}
	
	OICOperationNameFilter.prototype.setFieldValueNow = function() {
		this.setFieldValue($(this._textInput).val());
	}
	
	D3.inherits(OICOtherDocumentsList,D3.AbstractFieldComponent);
	OICOtherDocumentsList.FILE_MANAGER_DOWNLOAD_URL = "abaccess/webservice/filemanagerservice/";
	OICOtherDocumentsList.REPORT_DOWNLOAD_URL = "abaccess/download/";
	function OICOtherDocumentsList(){
		D3.AbstractFieldComponent.call(this);
		OICOtherDocumentsList.TOOLTIP_VIEW_MORE_REPORTS = D3.LanguageManager.getLabel("tooltip.documentList.viewMoreReports");
		OICOtherDocumentsList.SHOW_MORE = D3.LanguageManager.getLabel("button.showMore");
		OICOtherDocumentsList.SHOW_LESS = D3.LanguageManager.getLabel("button.showLess");
		OICOtherDocumentsList.LABEL_LESSON_TICKET_LIST = D3.LanguageManager.getLabel("label.documentList.lessonTicketList");
		OICOtherDocumentsList.LABEL_FILE_MANAGER_LIST = D3.LanguageManager.getLabel("label.documentList.fileManagerList");
		OICOtherDocumentsList.LABEL_CASING_CEMENT_LIST = D3.LanguageManager.getLabel("label.documentList.casingCementList");
		OICOtherDocumentsList.LABEL_DAY= D3.LanguageManager.getLabel("label.documentList.day");
		OICOtherDocumentsList.LABEL_ON= D3.LanguageManager.getLabel("label.documentList.on");
		
		this._fileFullList = false;
		this._fileContainer = null;
		this._fileLinks = [];
		this._lessonTicketFullList = false;
		this._lessonTicketContainer = null;
		this._lessonTicketLinks = [];
		this._casingCementFullList = false;
		this._casingCementContainer = null;
		this._casingCementLinks = [];
	}
	

	OICOtherDocumentsList.prototype.getFieldRenderer = function(){
		if (this._renderer == null){
			this._renderer = this.createRenderer();
		}
		return this._renderer;
	}
	
	OICOtherDocumentsList.prototype.createDocumentList = function(fieldName, headerText,links,linkState,linkCreationFunction){
		var conConfig = {
				tag:"div",
				css:"otherDocument-Listing"
		};
		var container = D3.UI.createElement(conConfig);
		
		
		var headerConfig = {
			tag:"div",
			css:"otherDocument-header",
		};
		var header = D3.UI.createElement(headerConfig);
		container.appendChild(header);
		var strDocuments = this._node.getFieldValue(fieldName);
		
		var documents = null;
		if (strDocuments){
			documents = strDocuments.split(",");
		}
		
		
		var more = false;
		if (documents && documents.length>3){
			more = true;
		}
		
		var title = headerText;
		header.appendChild(document.createTextNode(title));
		if (more){
			var moreConfig = {
					tag:"button",
					css:"otherDocument-moreReports DefaultButton",
					text:OICOtherDocumentsList.SHOW_MORE,
					data:{
						links:links,
						linkState:linkState,
						defaultShow:3
					},
					callbacks:{
						onclick:function(event){
							var button = event.target;
							var data = $(button).data();
							data.linkState = !data.linkState;
							if (data.linkState){
								$(button).text(OICLatestReportList.SHOW_LESS);
							}else{
								$(button).text(OICLatestReportList.SHOW_MORE);
							}
							for (var i=data.defaultShow;i<data.links.length;i++){
								if (data.linkState){
									$(data.links[i]).removeClass("hide");
								}else{
									$(data.links[i]).addClass("hide");
								}
									
							}
						}
					}
			};
			var btnMore = D3.UI.createElement(moreConfig);
			header.appendChild(btnMore);
		}
		
		var contConfig = {
				tag:"div",
				css:"otherDocument-content"
		}
		var content = D3.UI.createElement(contConfig);
		container.appendChild(content);
		if (documents && documents.length>0){
			for (var i=0;i<documents.length;i++){
				var strParams = documents[i];
				if (!strParams[0] != "&" && !strParams[0] != "?")
					strParams = "&"+strParams;
				strParams = decodeURIComponent(strParams.replace(/\+/g,  " "));
				var params = {};
			    var parts = strParams.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			    	params[key] = value;
			    });

				var documentLinkConfig = {
						tag:"div",
						css:"otherDocument-documentLink"+(i>=3?" hide":""),
						attributes:{},
						callbacks:{},
						context:this
				
				};
				linkCreationFunction.call(this,documentLinkConfig,params);
				var documentLinkConfig = D3.UI.createElement(documentLinkConfig);
				content.appendChild(documentLinkConfig);
				links.push(documentLinkConfig);
			}
		}
		
		return container;
		
	}
	
	OICOtherDocumentsList.prototype.lessonTicketItemCreation = function(documentLinkConfig,params){
		var dayNum = params.daynum;
		var dailyUid = params.dailyuid;
		var dayDate = params.daydate;
		var lessonTicketUid = params.lessonticketuid;
		var lessonTitle = params.lessontitle;
		documentLinkConfig.text = dayDate + " - " + lessonTitle;
		documentLinkConfig.css +=" LessonTicketLink";
		documentLinkConfig.attributes.title = OICOtherDocumentsList.LABEL_DAY+" " + dayNum + "(" + dayDate + ") - " + lessonTitle;
		documentLinkConfig.data = params;
		documentLinkConfig.callbacks.onclick = function(event){
			var link = event.target;
			var data = $(link).data();
			var dailyUid = data.dailyuid
			var lessonTicketUid =  data.lessonticketuid;
			var lessonTicketScreen = this._config.lessonTicketPage;
			
			if (!lessonTicketScreen) {
				lessonTicketScreen = "lessonticketdetail.html";
			}
			var redirect_url = lessonTicketScreen +"?gotoday=" + escape(dailyUid) + "&gotoFieldName=lessonTicketUid&gotoFieldUid=" + escape(lessonTicketUid);
			this._node.commandBeanProxy.gotoUrl(redirect_url,D3.LanguageManager.getLabel("label.redirecting"));
		}
	}
	
	OICOtherDocumentsList.prototype.fileManagerItemCreation = function(documentLinkConfig,params){
		documentLinkConfig.text = params.filename;
		documentLinkConfig.css +=" FileManagerFileLink";
		documentLinkConfig.attributes.title = params.filename+" "+OICOtherDocumentsList.LABEL_ON+" " + params.date;
		documentLinkConfig.data = params;
		documentLinkConfig.callbacks.onclick = function(event){
			var link = event.target;
			var data = $(link).data();
			this._node.commandBeanProxy.openDownloadWindow(OICOtherDocumentsList.FILE_MANAGER_DOWNLOAD_URL + encodeURIComponent(data.filename) + "?mainAction=download&fileId=" + encodeURIComponent(data.fileuid) + "&showName=false");
		}
	}

	OICOtherDocumentsList.prototype.casingCementItemCreation = function(documentLinkConfig,params){
		
		var reportName = params.reportname;
		
		
		var date = new Date(strReportName);
		
		if (!isNaN(date.getTime())) {
			var localDate = this.shiftTimeIntoLocalTimezone(date);
			strReportName = $.datepick.formatDate("d M yyyy", localDate) + params.asterisk;
		}
		documentLinkConfig.text = reportName;
		documentLinkConfig.css +=" ReportLink"+(params.asterisk? " asterisk":"");
		documentLinkConfig.attributes.title = reportName;
		documentLinkConfig.data = params;
		documentLinkConfig.callbacks.onclick = function(event){
			var link = event.target;
			var data = $(link).data();
			this._node.commandBeanProxy.openDownloadWindow(OICOtherDocumentsList.REPORT_DOWNLOAD_URL + encodeURIComponent(data.fileName) + "?mainAction=download&fileId=" + encodeURIComponent(data.reportfileuid) + "&showName=false");
		}
	}
	
	OICOtherDocumentsList.prototype.createRenderer = function(){
		var conConfig = {
				tag:"div",
				css:"otherDocument-Container"
		};
		var container = D3.UI.createElement(conConfig);
		
		var lessonTickets = this._node.getFieldValue("@lessonTicketList");
		if (lessonTickets != null){
			this._lessonTicketContainer = this.createDocumentList("@lessonTicketList",OICOtherDocumentsList.LABEL_LESSON_TICKET_LIST ,this._lessonTicketLinks,this._lessonTicketFullList, this.lessonTicketItemCreation);
			container.appendChild(this._lessonTicketContainer);
		}

		var files = this._node.getFieldValue("@fileManagerList");
		if (files != null){
			this._fileContainer = this.createDocumentList("@fileManagerList",OICOtherDocumentsList.LABEL_FILE_MANAGER_LIST ,this._fileLinks,this._fileFullList, this.fileManagerItemCreation);
			container.appendChild(this._fileContainer);
		}
		
		var casingCements = this._node.getFieldValue("@casingCementingList");
		if (casingCements != null){
			this._casingCementContainer = this.createDocumentList("@casingCementingList",OICOtherDocumentsList.LABEL_CASING_CEMENT_LIST ,this._casingCementLinks,this._casingCementFullList, this.casingCementItemCreation);
			container.appendChild(this._casingCementContainer);
		}
		
		return container;
	}
	
	
	
	D3.inherits(OICLatestReportList,D3.AbstractFieldComponent);
	
	function OICLatestReportList(){
		D3.AbstractFieldComponent.call(this);
		OICLatestReportList.TOOLTIP_VIEW_MORE_REPORTS = D3.LanguageManager.getLabel("tooltip.reportList.viewMoreReports");
		OICLatestReportList.SHOW_MORE = D3.LanguageManager.getLabel("button.showMore");
		OICLatestReportList.SHOW_LESS = D3.LanguageManager.getLabel("button.showLess");
		OICLatestReportList.LABEL_DAILY_REPORTS = D3.LanguageManager.getLabel("label.reportList.dailyReports");
		OICLatestReportList.LABEL_DDR = D3.LanguageManager.getLabel("label.reportList.ddr");
		OICLatestReportList.LABEL_DGR = D3.LanguageManager.getLabel("label.reportList.dgr");
		OICLatestReportList.NO_DAILY_REPORTS = D3.LanguageManager.getLabel("label.reportList.noDailyReports");
		this._ddrFullList = false;
		this._ddrContainer = null;
		this._ddrLinks = [];
		this._dgrFullList = false;
		this._dgrContainer = null;
		this._dgrLinks = [];
	}
	
	OICLatestReportList.prototype.getReportHeaderTitle = function(simpleClassName){
		var reports = this._node.children[simpleClassName];
		if (reports && reports.getLength()>0){
			var node = reports.getItemAt(0);
			var value = node.getFieldValue("reportType");
			var lookupRef = node.getLookup("reportType");
			if (lookupRef){
				var lookupItem = lookupRef.getLookupItem(value);
				if (lookupItem)
					return lookupItem.label;
			}
				
		}
		return null;
	}
	OICLatestReportList.prototype.getFieldRenderer = function(){
		if (this._renderer == null){
			this._renderer = this.createRenderer();
		}
		return this._renderer;
	}
	
	OICLatestReportList.prototype.createReportList = function(simpleClassName, reportType,links,linkState, showNoReportMessage){
		var conConfig = {
				tag:"div",
				css:"reportList-Listing"
		};
		var container = D3.UI.createElement(conConfig);
		
		
		var headerConfig = {
			tag:"div",
			css:"reportList-header",
		};
		var header = D3.UI.createElement(headerConfig);
		container.appendChild(header);
		
		var reports = this._node.children[simpleClassName];
		var more = false;
		if (reports && reports.getLength()>1){
			more = true;
		}
		
		var title =this.getReportHeaderTitle(simpleClassName);
		if (!title)
			title = OICLatestReportList.LABEL_DAILY_REPORTS; 
		header.appendChild(document.createTextNode(title+"s"));
		if (more){
			var moreConfig = {
					tag:"button",
					css:"reportList-moreReports DefaultButton",
					text:OICLatestReportList.SHOW_MORE,
					data:{
						links:links,
						linkState:linkState
					},
					callbacks:{
						onclick:function(event){
							var button = event.target;
							var data = $(button).data();
							data.linkState = !data.linkState;
							if (data.linkState){
								$(button).text(OICLatestReportList.SHOW_LESS);
							}else{
								$(button).text(OICLatestReportList.SHOW_MORE);
							}
							for (var i=1;i<data.links.length;i++){
								if (data.linkState){
									$(data.links[i]).removeClass("hide");
								}else{
									$(data.links[i]).addClass("hide");
								}
									
							}
						}
					}
			};
			var btnMore = D3.UI.createElement(moreConfig);
			header.appendChild(btnMore);
		}
		
		var contConfig = {
				tag:"div",
				css:"reportList-content"
		}
		var content = D3.UI.createElement(contConfig);
		container.appendChild(content);
		if (reports && reports.getLength()>0){
			for (var i=0;i<reports.getLength();i++){
				var node = reports.getItemAt(i);
				
				var date = new Date(parseInt(node.getFieldValue("reportDate")));
				
				var reportDate = null;
				if (!isNaN(date.getTime())) {
					var localDate = this.shiftTimeIntoLocalTimezone(date);
					reportDate = $.datepick.formatDate("d M yyyy", localDate);
				} else {
					reportDate = node.getFieldValue("reportDate");
				}
				
				var toolTip = node.getFieldValue("reportDisplayName");
				
				var reportLinkConfig = {
						tag:"div",
						css:"reportList-reportLink"+(i!=0?" hide":""),
						text:reportDate,
						attributes:{
							
							title:toolTip
						},
						data:node,
						callbacks:{
							onclick:function(event){
								var button = event.target;
								var data = $(button).data();
								var url = "abaccess/download/" + 
								encodeURIComponent(D3.Common.replaceInvalidCharInFileName(data.getFieldValue("reportDisplayName"))) + 
								"." + 
								data.getFieldValue("fileExtension") + 
								"?handlerId=ddr&reportId=" + 
								encodeURIComponent(data.getFieldValue("reportFilesUid"));
								data.commandBeanProxy.openDownloadWindow(url);
							}
						},
						context:this
				
				};
				var reportLink = D3.UI.createElement(reportLinkConfig);
				content.appendChild(reportLink);
				links.push(reportLink);
			}
		}else{
			if (showNoReportMessage){
				content.appendChild(document.createTextNode(OICLatestReportList.NO_DAILY_REPORTS));
			}
		}
		
		return container;
		
	}
	
	OICLatestReportList.prototype.createRenderer = function(){
		var conConfig = {
				tag:"div",
				css:"reportList-Container"
		};
		var container = D3.UI.createElement(conConfig);
		
		this._ddrContainer = this.createReportList("DDRReportList",OICLatestReportList.LABEL_DDR ,this._ddrLinks,this._ddrFullList, true);
		container.appendChild(this._ddrContainer);
		var dgrs = this._node.children["DGRReportList"];
		if (dgrs && dgrs.getLength()>0){
			this._dgrContainer = this.createReportList("DGRReportList",OICLatestReportList.LABEL_DGR ,this._dgrLinks,this._dgrFullList);
			container.appendChild(this._dgrContainer);
		}

		return container;
	}
	
	OICLatestReportList.prototype.createDocumentList = function(strDocuments){
		var conConfig = {
				tag:"div",
				css:"otherDocument-Listing"
		};
		var container = D3.UI.createElement(conConfig);
		
		
		var headerConfig = {
			tag:"div",
			css:"otherDocument-header",
		};
		var header = D3.UI.createElement(headerConfig);
		container.appendChild(header);
		
		var documents = null;
		if (strDocuments){
			documents = strDocuments.split(",");
		}
		
		var title = "Other Reports";
		header.appendChild(document.createTextNode(title));

		var contConfig = {
				tag:"div",
				css:"otherDocument-content"
		}
		var content = D3.UI.createElement(contConfig);
		container.appendChild(content);
		if (documents && documents.length>0){
			for (var i=0;i<documents.length;i++){
				var strParams = documents[i];
				if (!strParams[0] != "&" && !strParams[0] != "?")
					strParams = "&"+strParams;
				strParams = decodeURIComponent(strParams.replace(/\+/g,  " "));
				var params = {};
			    var parts = strParams.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			    	params[key] = value;
			    });	
				var reportDate = params.reportDate;
				var toolTip = params.reportname + " (" + params.reportDate + ")";
				
				var reportLinkConfig = {
						tag:"div",
						css:"reportList-reportLink",
						text:toolTip,
						attributes:{
							title:toolTip
						},
						callbacks:{
							onclick:function(event){
								var button = event.target;
								var data = $(button).data();
								var url = "abaccess/download/" + 
								encodeURIComponent(D3.Common.replaceInvalidCharInFileName(toolTip)) + 
								"." + 
								params.filename + 
								"?handlerId=ddr&reportId=" + 
								encodeURIComponent(params.reportfileuid);
								this._node.commandBeanProxy.openDownloadWindow(url);
							}
						},
						context:this
				
				};
				var reportLink = D3.UI.createElement(reportLinkConfig);
				content.appendChild(reportLink);
				//links.push(reportLink);
			}
		}
		
		return container;
		
	}
	D3.OICLatestReportList = OICLatestReportList;
	D3.inherits(OICLatestReportListForDgr,D3.OICLatestReportList);
	
	function OICLatestReportListForDgr(){
		D3.OICLatestReportList.call(this);
	}
	OICLatestReportListForDgr.prototype.createRenderer = function(){
		var conConfig = {
				tag:"div",
				css:"reportList-Container"
		};
		var container = D3.UI.createElement(conConfig);
		
		var dgrs = this._node.children["DGRReportList"];
		this._dgrContainer = this.createReportList("DGRReportList",OICLatestReportList.LABEL_DGR ,this._dgrLinks,this._dgrFullList);
		container.appendChild(this._dgrContainer);
		
		var oths = this._node.getFieldValue("@otherReportList");
		if (oths){
			var documents  = oths.split(",");
			this._othContainer = this.createDocumentList(oths);
			container.appendChild(this._othContainer);
		}
		return container;
	}
	
	D3.inherits(OperationLinkButton,D3.HyperLinkField);
	
	function OperationLinkButton(){
		D3.HyperLinkField.call(this);
	}
	
	OperationLinkButton.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		
		if (!this._fieldRenderer){
		  var linkLabel = this._node.getFieldValue("@completeOperationName");
		  fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		  fieldRenderer.appendChild(document.createTextNode(linkLabel));
		  fieldRenderer.removeAttribute("target");
		  fieldRenderer.removeAttribute("href");
		  $(fieldRenderer).addClass("whitespace");
		  fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}else{
	      fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		}
		return fieldRenderer; 
	}
	
	OperationLinkButton.prototype.onclick = function(){
		var url = this._node.getFieldValue("@operationPage") + "?gotowellop=" + this._node.getFieldValue("operationUid");
		this._node.commandBeanProxy.gotoUrl(url,"Loading " + this.getFieldValue());
	}
	
	OperationLinkButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(OICSnapshot,D3.AbstractFieldComponent);
	
	function OICSnapshot(btnSnapshot,listener)
	{
		this.listener = listener;
		this.btnSnapshot = btnSnapshot;
		var that = this;
		$(this.btnSnapshot).on("snapshotButtonClicked",function(event,data) {
			that.snapshotButtonClicked(event,data);
		});
	}
	
	OICSnapshot.prototype.snapshotButtonClicked = function(event,data){
		var snapshotType = data.data;

		if (snapshotType == OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH) {
			this._accordion.toggle("dvdGraph");
			$(this.btnSnapshot).text(OperationInformationCenterListener.SNAPSHOT_SHOW_DETAILS);
			this.listener.snapshotState = OperationInformationCenterListener.SNAPSHOT_SHOW_DETAILS;
		} else if (snapshotType == OperationInformationCenterListener.SNAPSHOT_SHOW_DETAILS) {
			this._accordion.toggle("wellDetail");
			$(this.btnSnapshot).text(OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH);
			this.listener.snapshotState = OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH;
		}
	}
	
	
	OICSnapshot.prototype.getFieldRenderer = function() {
		if (this._renderer == null) {
			this._renderer = this.createSnapshotUIComponent();
		}

		return this._renderer;
	}
	OICSnapshot.prototype.headerActivity_onClick = function(){
		var url = "activity.html?gotoday=" + this._node.getFieldValue("@lastActivityDaily");
		this._node.commandBeanProxy.gotoUrl(url, "Loading");
	}
	
	OICSnapshot.prototype.createSimpleDetail = function(container,headerText,fieldContent){
		var sectionConfig = {
			tag:"div",
			css:"wellDetail-section"
		}
		var section = D3.UI.createElement(sectionConfig);
		container.appendChild(section);
		var headerConfig = {
			tag:"a",
			text:headerText,
			css:"wellDetail-sectionHeader",
		};
		var header = D3.UI.createElement(headerConfig);
		section.appendChild(header);
		
		var contentConfig = {
			tag:"div",
			css:"wellDetail-sectionContent",
			text:this._node.getFieldValue(fieldContent)
		};
		var content = D3.UI.createElement(contentConfig);
		section.appendChild(content);
		
	}
	OICSnapshot.prototype.createActivityDetail = function(container){
		var sectionConfig = {
			tag:"div",
			css:"wellDetail-section"
		}
		var section = D3.UI.createElement(sectionConfig);
		container.appendChild(section);
		
		var headerConfig = {
			tag:"a",
			text:D3.LanguageManager.getLabel("label.snapshot.header.activity"),
			css:"wellDetail-sectionHeader link",
			callbacks:{
				onclick:this.headerActivity_onClick
			},
			context:this
		};
		var header = D3.UI.createElement(headerConfig);
		section.appendChild(header);
		
		var contentConfig = {
			tag:"div",
			css:"wellDetail-sectionContent",
			context:this
		};
		var content = D3.UI.createElement(contentConfig);
		section.appendChild(content);
		
		
		var actDateConfig = {
			tag:"div",				
			css:"wellDetail-subHeader",
			text : this.getLastActivityDateTime()
		};
		var actDate = D3.UI.createElement(actDateConfig);
		content.appendChild(actDate);
		
		
		var actDescConfig = {
			tag:"div",
			css:"wellDetail-description",
			text:	this._node.getFieldValue("@lastActivity"),
			
		}

		var actClass = this._node.getFieldValue("@lastActivityClass");

		if (["T","TP"].indexOf(actClass)!=-1) {
			actDescConfig.css +=" trouble";
		}
		
		var actDateDesc = D3.UI.createElement(actDescConfig);
		content.appendChild(actDateDesc);
		
	}
	
	OICSnapshot.prototype.getLastActivityDescription = function(){
		return this._node.getFieldValue("@lastActivity");
	}
	OICSnapshot.prototype.formatTime = function(date){
		var hour = date.getHours();
		var minute = date.getMinutes();
		var sec = date.getSeconds();
		var hourString = hour;
		var minuteString = minute;
		if (hour == 23 && minute == 59 && date.getUTCSeconds() == 59) {
			hourString = "24";
			minuteString = "00";
		} else {
			if (hour <= 0) {
				hourString = "00";
			} else if (hour < 10) {
				hourString = "0" + hour;
			}
			if (minute < 10) {
				minuteString = "0" + minute;
			}
		}
		
		return hourString + ":" + minuteString;
	}
	
	OICSnapshot.prototype.shiftTimeIntoLocalTimezone = function(date) {
		return new Date(date.getTime() + (date.getTimezoneOffset() * 60000)); 
	};
	
	OICSnapshot.prototype.getLastActivityDateTime = function(){
		var activityDate = new Date(parseInt(this._node.getFieldValue("@lastActivityDate")));
		
		var txtDateTime = null;
		if (!isNaN(activityDate.getTime())) {
			var localDate = this.shiftTimeIntoLocalTimezone(activityDate);
			txtDateTime = $.datepick.formatDate("d M yyyy", localDate);
		} else {
			txtDateTime = this._node.getFieldValue("@lastActivityDate");
		}

		txtDateTime += "  " + this._node.getFieldValue("@lastActivityTime");
		return txtDateTime;
	}
	OICSnapshot.prototype.headerHSE_onClick = function(){
		var url = "hseincident.html?gotoday=" + this._node.getFieldValue("@lastHseDaily");
		this._node.commandBeanProxy.gotoUrl(url, "Loading");
	}
	OICSnapshot.prototype.getLastHSEDatetTime = function(){
		var hseDateTime = new Date(parseInt(this._node.getFieldValue("@lastHseDatetime")));
		
		var txtDateTime = null;
		if (!isNaN(hseDateTime.getTime())) {
			var localDate = this.shiftTimeIntoLocalTimezone(hseDateTime);
			txtDateTime = $.datepick.formatDate("d M yyyy", localDate)+" "+this.formatTime(localDate);
		} else {
			txtDateTime = this._node.getFieldValue("@lastHseDatetime");
		}
		return txtDateTime;
	}
	
	OICSnapshot.prototype.createHSE = function(container){
		var sectionConfig = {
			tag:"div",
			css:"wellDetail-section"
		}
		var section = D3.UI.createElement(sectionConfig);
		container.appendChild(section);
		
		var headerConfig = {
			tag:"a",
			text:D3.LanguageManager.getLabel("label.snapshot.header.hse"),
			css:"wellDetail-sectionHeader link",
			callbacks:{
				onclick:this.headerHSE_onClick
			},
			context:this
		};
		var header = D3.UI.createElement(headerConfig);
		section.appendChild(header);
		
		var contentConfig = {
			tag:"div",
			css:"wellDetail-sectionContent",
			context:this
		};
		var content = D3.UI.createElement(contentConfig);
		section.appendChild(content);
		
		
		var hseDateConfig = {
			tag:"div",				
			css:"wellDetail-subHeader",
			text : this.getLastHSEDatetTime()
		};
		var hseDate = D3.UI.createElement(hseDateConfig);
		content.appendChild(hseDate);
		
		
		var hseDescConfig = {
			tag:"div",
			css:"wellDetail-description",
			text:	this._node.getFieldValue("@lastHse"),
			
		}

		
		var hseDateDesc = D3.UI.createElement(hseDescConfig);
		content.appendChild(hseDateDesc);
		
	}
	OICSnapshot.prototype.createSnapshotUIComponent = function() {
		var _graphParameter = this._node.getFieldValue("@graphParameter");
		var _graphHeight = this._node.getFieldValue("@graphHeight");
		var _graphWidth = this._node.getFieldValue("@graphWidth");

		// well details
		this._divWellDetail = document.createElement("div");
		var lblWellDetail = D3.LanguageManager.getLabel("label.wellDetail");
		if (this._node.getFieldValue("@lastActivityDate") != null) {
			this.createActivityDetail(this._divWellDetail);
		}

		if (this._node.getFieldValue("@24HoursSummary") != null) {
			this.createSimpleDetail(this._divWellDetail,D3.LanguageManager.getLabel("label.snapshot.header.24HoursSummary"),"@24HoursSummary");
		}

		if (this._node.getFieldValue("@prev24HoursSummary") != null) {
			this.createSimpleDetail(this._divWellDetail,D3.LanguageManager.getLabel("label.snapshot.header.prev24HoursSummary"),"@prev24HoursSummary");
		}

		if (this._node.getFieldValue("@reportPlanSummary") != null) {
			this.createSimpleDetail(this._divWellDetail,D3.LanguageManager.getLabel("label.snapshot.header.reportPlanSummary"),"@reportPlanSummary");
		}

		if (this._node.getFieldValue("@lastHseDatetime") != null) {
			this.createHSE(this._divWellDetail);
		}
		var config = {css:{container:"accordionSnapshot-Container",header:"accordionSnapshot-Header",content:"accordionSnapshot-Content"}};
		if (_graphParameter){
			/*config.width = _graphWidth;
			config.height = _graphHeight;*/
		}
		this._accordion = new D3.Accordion(config);
		
		this._accordion.addChild("wellDetail",lblWellDetail,this._divWellDetail);
		// dvd graph
		if (_graphParameter) {
			this._dvdGraphUrl = this._node.commandBeanProxy.baseUrl + "graph.html?" + _graphParameter + "&operation_uid=" + this._node.getFieldValue("operationUid");
			
			var graphImageConfig = {
					tag:"img",
					attributes:{
						src: this._dvdGraphUrl,
						title:D3.LanguageManager.getLabel("img.snapshot.showDVDGraph")
					},
					
					callbacks:{
						onclick: function(event){
							var url = "dvdgraph.html?operation_uid=" + this._node.getFieldValue("operationUid");
							this._node.commandBeanProxy.gotoUrl(url,"Loading DVD Graph");
						}
					},
					context:this
			}
			
			this._dvdGraphImage = D3.UI.createElement(graphImageConfig);
			var lblGraph = D3.LanguageManager.getLabel("label.dvdGraph");
			this._divDVDGraph = document.createElement("div");
			this._divDVDGraph.appendChild(this._dvdGraphImage);

			
			this._accordion.addChild("dvdGraph",lblGraph,this._divDVDGraph);

			
		} 
		D3.UIManager.addCalllater(function(){
			this._accordion.toggle("wellDetail");
		},this);
		
		return this._accordion.uiElement;
	}
	
	D3.OICSnapshot = OICSnapshot;
	
	D3.inherits(OICGeoDetails,D3.AbstractFieldComponent);
	
	function OICGeoDetails(btnSnapshot,listener)
	{
		this.listener = listener;
		this.btnSnapshot = btnSnapshot;

	}
	
	OICGeoDetails.prototype.getFieldRenderer = function() {
		if (this._renderer == null) {
			this._renderer = document.createElement("div");
			if (this._node.getFieldValue("@dailyEndSummary") != null) {
				this.createSimpleDetail(this._renderer,D3.LanguageManager.getLabel("label.snapshot.header.dailyEndSummary"),"@dailyEndSummary");
			}
			if (this._node.getFieldValue("@predictedLoggingDateTime") != null) {
				var predictedLoggingDateTime = this._node.getFieldValue("@predictedLoggingDateTime");
				var date = new Date(parseInt(predictedLoggingDateTime));
				var localDate = new Date(date.getTime() + (date.getTimezoneOffset() * 60000));
				var hour = localDate.getHours();
				var minute = localDate.getMinutes();
				var sec = localDate.getSeconds();
				var hourString = hour;
				var minuteString = minute;
				if (hour == 23 && minute == 59 && localDate.getUTCSeconds() == 59) {
					hourString = "24";
					minuteString = "00";
				} else {
					if (hour <= 0) {
						hourString = "00";
					} else if (hour < 10) {
						hourString = "0" + hour;
					}
					if (minute < 10) {
						minuteString = "0" + minute;
					}
				}
				if (!isNaN(date.getTime())) {
					predictedLoggingDateTime = $.datepick.formatDate("dd M yyyy", localDate)+" "+hourString + ":" + minuteString;
					this._node.setFieldValue("@predictedLoggingDateTime", predictedLoggingDateTime);
				}
				this.createSimpleDetail(this._renderer,D3.LanguageManager.getLabel("label.snapshot.header.predictedLoggingDateTime"),"@predictedLoggingDateTime");
			}
			if (this._node.getFieldValue("@24HoursSummary") != null) {
				this.createSimpleDetail(this._renderer,D3.LanguageManager.getLabel("label.snapshot.header.24HoursSummary"),"@24HoursSummary");
			}
			if (this._node.getFieldValue("@geolOps") != null) {
				this.createSimpleDetail(this._renderer,D3.LanguageManager.getLabel("label.snapshot.header.geolOps"),"@geolOps");
			}
			if (this._node.getFieldValue("@last24hrsincident") != null) {
				this.createSimpleDetail(this._renderer,D3.LanguageManager.getLabel("label.snapshot.header.last24hrsincident"),"@last24hrsincident");
			}
		}

		return this._renderer;
	}
	OICSnapshot.prototype.formatTime = function(date){
		var hour = date.getHours();
		var minute = date.getMinutes();
		var sec = date.getSeconds();
		var hourString = hour;
		var minuteString = minute;
		if (hour == 23 && minute == 59 && date.getUTCSeconds() == 59) {
			hourString = "24";
			minuteString = "00";
		} else {
			if (hour <= 0) {
				hourString = "00";
			} else if (hour < 10) {
				hourString = "0" + hour;
			}
			if (minute < 10) {
				minuteString = "0" + minute;
			}
		}
		
		return hourString + ":" + minuteString;
	}
	OICGeoDetails.prototype.createSimpleDetail = function(container,headerText,fieldContent){
		var sectionConfig = {
			tag:"div",
			css:"wellDetail-section"
		}
		var section = D3.UI.createElement(sectionConfig);
		container.appendChild(section);
		var headerConfig = {
			tag:"a",
			text:headerText,
			css:"wellDetail-sectionHeader",
		};
		var header = D3.UI.createElement(headerConfig);
		section.appendChild(header);
		
		var contentConfig = {
			tag:"div",
			css:"wellDetail-sectionContent",
			text:this._node.getFieldValue(fieldContent)
		};
		var content = D3.UI.createElement(contentConfig);
		section.appendChild(content);
		
	}
	OICGeoDetails.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(OperationInformationCenterListener,D3.EmptyNodeListener);
	
	function OperationInformationCenterListener()
	{
		this.btnSnapshot = null;
		this.snapshotState = null;
		
	}
	
	
	
	OperationInformationCenterListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "operationLink") {
			return new OperationLinkButton();
		} else if (id == "snapshot") {
			return new OICSnapshot(this.btnSnapshot,this);
		} else if(id == "latestReportList"){
			return new OICLatestReportList();
		} else if(id == "latestReportListForDgr"){
			return new OICLatestReportListForDgr();
		} else if(id == "operationNameFilter"){
			return new OICOperationNameFilter();
		} else if(id == "nbbHyperlink") {
			return new NbbHyperlink();
		} else if(id == "otherDocumentsList"){
			return new OICOtherDocumentsList();
		}else if(id == "geoDetails"){
			return new OICGeoDetails(this.snapshotButton,this);
		}else{
			return null;
		}
	}
	OperationInformationCenterListener.prototype.getCustomButtonForHeaderColumn = function(node,id){
		if (id == "showGraphOrDetails"){
			OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH = D3.LanguageManager.getLabel("button.showGraph");
			OperationInformationCenterListener.SNAPSHOT_SHOW_DETAILS = D3.LanguageManager.getLabel("button.showDetail");
			var config = {
					tag:"button",
					text:OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH,
					css:"DefaultButton",
					callbacks:{
						onclick:this.btnSnapshot_onClick
					},
					context:this
			};
		
			this.btnSnapshot = D3.UI.createElement(config);
			this.snapshotState = OperationInformationCenterListener.SNAPSHOT_SHOW_GRAPH;
			return this.btnSnapshot;
		}
		return null;
	}
	OperationInformationCenterListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		
	}
	OperationInformationCenterListener.prototype.btnSnapshot_onClick = function(event){
		var button = event.target;
		$(button).trigger("snapshotButtonClicked",{data:this.snapshotState});
	}
	D3.CommandBeanProxy.nodeListener = OperationInformationCenterListener;
	
	
	
})(window);