(function(window){
	
	D3.inherits(CasingLinkField,D3.HyperLinkField);
	
		
	function CasingLinkField(id){
		
		this.currentId = id;		
		D3.HyperLinkField.call(this);
	}
	
	CasingLinkField.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		var showLink = false;
		
		//check need to show link or not
		if ((this.currentId == "previousCasingLink" || this.currentId == "previousTaperedCasingLink" ) && this._node.getFieldValue("previousCasingSectionUid") != ""){
			showLink = true;
		}
		
		if ((this.currentId == "casingLink" || this.currentId == "taperCasingLink" || this.currentId == "taperedCasingLink") && this._node.getFieldValue("casingSectionUid")){
			showLink = true;	
		}
			
		
		if (showLink){
			if (!this._fieldRenderer){
				fieldRenderer = CasingLinkField.uber.getFieldRenderer.call(this);
				fieldRenderer.appendChild(document.createTextNode(D3.LanguageManager.getLabel("label.casingLink")));
				fieldRenderer.removeAttribute("target");
				fieldRenderer.removeAttribute("href");
				
				fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
				this._fieldRenderer = fieldRenderer;

			}else{
				fieldRenderer = CasingLinkField.uber.getFieldRenderer.call(this);
			}	
			
		}
		
		return fieldRenderer;
	}
	
	CasingLinkField.prototype.onclick = function(){
		var url = "";
		
		if (this.currentId == "taperedCasingLink"){
			url = "taperedcasing.html?gotoFieldName=casingSectionUid&gotoFieldUid=" + escape(this._node.getFieldValue("casingSectionUid")) + "&gotowellop=" + escape(this._node.getFieldValue("@casingSection_operationUid"));
		}else if (this.currentId == "previousTaperedCasingLink"){
			url = "taperedcasing.html?gotoFieldName=casingSectionUid&gotoFieldUid=" + escape(this._node.getFieldValue("previousCasingSectionUid")) + "&gotowellop=" + escape(this._node.getFieldValue("@previousCasingSection_operationUid"));
		}else if (this.currentId == "previousCasingLink"){
			url = "casingsection.html?gotoFieldName=casingSectionUid&gotoFieldUid=" + escape(this._node.getFieldValue("previousCasingSectionUid")) + "&gotowellop=" + escape(this._node.getFieldValue("@previousCasingSection_operationUid"));
		}else{
			url = "casingsection.html?gotoFieldName=casingSectionUid&gotoFieldUid=" + escape(this._node.getFieldValue("casingSectionUid")) + "&gotowellop=" + escape(this._node.getFieldValue("@casingSection_operationUid"));
		}
		
		this._node.commandBeanProxy.gotoUrl(url,"Redirecting to ");
	}
	
	
	CasingLinkField.prototype.refreshFieldRenderer = function() {
		
	};
	
	
	D3.inherits(CementJobNodeListener,D3.EmptyNodeListener);
	
	function CementJobNodeListener()
	{
		this.CasingLinkField = null;
	}
	
	
	CementJobNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "casingLink"){
			if (node.getFieldValue(this.casingSectionUid) != "") {
				return new CasingLinkField(id);
			}else {
				return null;
			}				
		}else if(id=="previousCasingLink") {
			if (node.getFieldValue(this.previousCasingSectionUid) != "") {
				return new CasingLinkField(id);
			}else {
				return null;
			}
		}else if(id=="taperedCasingLink") {
			if (node.getFieldValue(this.casingSectionUid) != "") {
				return new CasingLinkField(id);
			}else {
				return null;
			}
		}else if(id=="previousTaperedCasingLink") {
			if (node.getFieldValue(this.previousCasingSectionUid) != "") {
				return new CasingLinkField(id);
			}else {
				return null;
			}
		}else {
			return null;
		}
	}
	
	
	D3.CommandBeanProxy.nodeListener = CementJobNodeListener;
	
})(window);