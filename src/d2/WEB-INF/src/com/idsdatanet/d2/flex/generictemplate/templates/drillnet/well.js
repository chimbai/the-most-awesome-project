(function(window){
	D3.inherits(ComboBoxWithWarningMessageInEditModeField,D3.ComboBoxField)
	
	function ComboBoxWithWarningMessageInEditModeField(cascade){
		D3.ComboBoxField.call(this);
	}
	
	ComboBoxWithWarningMessageInEditModeField.prototype.afterFieldEditorAppended = function() {
		 
		D3.UIManager.addCalllater(function(){
			this.fieldContainer.clearFieldUserMsgContainer();
			var msg = D3.LanguageManager.getLabel("warning.gmtOffset");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);
		},this);

		
	}
	D3.inherits(WellNameCustomField,D3.TextInputField);
	
	function WellNameCustomField(){
		D3.TextInputField.call(this);
	}
	
	WellNameCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			fieldEditor = WellNameCustomField.uber.getFieldEditor.call(this);
			fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			fieldEditor = WellNameCustomField.uber.getFieldEditor.call(this);
		}			
		return fieldEditor;
	}
	WellNameCustomField.prototype.onKeyUp = function(event){
		this.checkWellNameExist();
	}
	WellNameCustomField.prototype.checkWellNameExist = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	WellNameCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "checkingWellNameExist";
		params.name = this.getFieldEditor().value;
		params.isNewRecord = this._node.atts.isNewRecord? "1":"";
		
		return params;
	}
	
	WellNameCustomField.prototype.loaded = function(response) {
		var self = this;
		this.fieldContainer.clearFieldUserMsgContainer();
		var isDuplicate = false;
		$(response).find('item').each(function(){
			if ($(this).attr('name')=='duplicate'){
				if ($(this).attr('value')=='true'){
					isDuplicate = true;
				}
			}
		});
		if (isDuplicate){
			var msg = D3.LanguageManager.getLabel("warning.wellNameExist");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);

		}
			
	}
	
	D3.inherits(WellNodeListener,D3.EmptyNodeListener);
	
	function WellNodeListener()
	{
		
	}
	WellNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "wellNameInWWO") {
			return new WellNameCustomField();
		} else if (id == "gmtWithWarningMessageInEditMode") {
			return new ComboBoxWithWarningMessageInEditModeField(false);
		}else {
			return null;
		}
	}
	
	WellNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration) {
		this._commandBeanProxy = commandBeanProxy;
		if (id == "customAddNewWell") {
			if (!commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "Well")) {
				return null;
			}
			var customAddNewWell = document.createElement("button");
			customAddNewWell.className = "rootButtons";
			var label = D3.LanguageManager.getLabel("button.addNewWell");
			var addNew = document.createTextNode(label);
			customAddNewWell.appendChild(addNew);
			customAddNewWell.onclick = this.onClickCustomAddNewWell.bindAsEventListener(this);
			
			return customAddNewWell;
		} else if (id == "edmImportWell") {
			if (commandBeanProxy.rootNode.getFieldValue("@edmSyncEnabled") == "1") {
				var btn = document.createElement("button");
				btn.className = "rootButtons";
				var label = D3.LanguageManager.getLabel("button.edmImportWell");
				btn.appendChild(document.createTextNode(label));
				btn.onclick = this.onClickEdmImportWell.bindAsEventListener(this);
				return btn;
			}
			return null;
		}
	}
	
	WellNodeListener.prototype.onClickEdmImportWell = function(event) {
		this._commandBeanProxy.pageRedirect("edmwellimport.html", "Redirecting to EDM Well Import screen...");
	};
		
	WellNodeListener.prototype.onClickCustomAddNewWell = function(event) { //for well 
		var wells = this._commandBeanProxy.rootNode.children["Well"];
		wells.archive();

		var newNodes = this._commandBeanProxy.rootNode.addNewChildNodeOfClass("Well");
		if (newNodes && newNodes.getLength()>0) {
			var newNode = newNodes.getItemAt(0);
			newNode.setFieldValue("@copyFromOperationUid", "_currentSelectedOperationUid"); // set a marker here and let the backend figures out the current operationUid in session
			newNode.commandBeanProxy.submitForServerSideProcess(newNode, "@copyFromOperationUid");
		}
	}
		
		
	D3.CommandBeanProxy.nodeListener = WellNodeListener;
	
})(window);