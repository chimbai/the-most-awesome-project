package com.idsdatanet.d2.flex.dat;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.Lwd;
import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.model.LwdTool;
import com.idsdatanet.d2.core.model.LwdWitness;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigIssue;
import com.idsdatanet.d2.core.model.RigIssueLog;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WirelineLog;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.model.WirelineTool;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.flex.dat.customObject.DVDCostTimeBreakDownObject;

public class DatCustomHtmlToFlexObject {

	private String _callFunction = null;
	private Map<String, Object> _queryFilters = new HashMap<String, Object>();
	public HttpServletRequest _httpRequest = null;
	
	public void setCallFunction(String callFunctionName) { 
		this._callFunction = callFunctionName; 
	}
	
	public String getCallFunction() { 
		return this._callFunction; 
	}
	
	public void setFilter(String filters) throws Exception {
		try {
			this._queryFilters = new HashMap<String, Object>();
			SimpleXMLElement xml = SimpleXMLElement.loadXMLString(filters);
			for(Iterator i = xml.getChild().iterator(); i.hasNext(); ) {
				SimpleXMLElement thisFilter = (SimpleXMLElement) i.next();
				String filterName = thisFilter.getAttribute("name");
				if (StringUtils.isNotBlank(filterName)) {
					String tagName = thisFilter.getTagName();
					if ("DateRange".equals(tagName)) {
						Map<String, String> dateRangeFilter = new HashMap<String, String>();
						dateRangeFilter.put("min", thisFilter.getAttribute("startDate"));
						dateRangeFilter.put("max", thisFilter.getAttribute("endDate"));
						this._queryFilters.put(filterName, dateRangeFilter);
					} else if ("Slider".equals(tagName)) {
						Map<String, String> minMaxFilter = new HashMap<String, String>();
						minMaxFilter.put("min", thisFilter.getAttribute("min"));
						minMaxFilter.put("max", thisFilter.getAttribute("max"));
						this._queryFilters.put(filterName, minMaxFilter);
					} else if ("List".equals(tagName)) {
						Map<String, String> listFilter = new HashMap<String, String>();
						for(Iterator j = thisFilter.getChild().iterator(); j.hasNext(); ) {
							SimpleXMLElement listItem = (SimpleXMLElement) j.next();
							String value = listItem.getAttribute("value");
							if (StringUtils.isNotBlank(value)) {
								listFilter.put(value, value);
							}
						}
						this._queryFilters.put(filterName, listFilter);
					} else if ("Text".equals(tagName)) {
						this._queryFilters.put(filterName, thisFilter.getAttribute("value"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Object getFilter(String filterName) {
		if (this._queryFilters.containsKey(filterName)) return this._queryFilters.get(filterName);
		return null;
	}
	//TODO
	public String getHtmlResult() throws Exception {
		String xml = "<html/>";
		if ("QueryForTestingOnly".equals(this._callFunction)) {
			xml = this.testMyHtmlFunctions();
		} else if ("QueryForLWDWirelineForOSL".equals(this._callFunction)) {
			xml = this.queryLwdWirelineOSL();
		} else if ("QueryForLWDSummaryForOSL".equals(this._callFunction)) {
			xml = this.queryLwdSummaryOSL();
		} else if ("QueryForDayDepthAnalysisRushmoreForOSL".equals(this._callFunction)) {
			xml = this.queryDayDepthAnalysisRushmoreSL();
		} else if ("QueryForWirelineWitnessForOSL".equals(this._callFunction)) {
			xml = this.queryWirelineWitnessOSL();
		} else if ("QueryForLWDWitnessForOSL".equals(this._callFunction)) {  
			xml = this.queryLwdWitnessOSL();
		} else if ("QueryForActivityCodesBreakdownByOperationForOSL".equals(this._callFunction)) {  
			xml = this.queryActivityCodesBreakdownByOperationOSL();
		} else if ("QueryBitByHoleSectionOSL".equals(this._callFunction)) {  
			xml = this.queryBitByHoleSectionOSL();
		} else if ("QueryDayDepthCostTimeBreakdownOSL".equals(this._callFunction)) {  
			xml = this.queryDayDepthCostTimeBreakdownOSL();		
		} else if ("QueryWellsEndOfMonthWellSummaryForSantos".equals(this._callFunction)) {  
			xml = this.queryForEndOfMonthWellSummaryForSantos();
		} else if ("QueryForActivityCodesDailyActualTimeBreakdownForOSL".equals(this._callFunction)) {  
			xml = this.queryForActivityCodesDailyActualTimeBreakdownForOSL();
		} else if ("QueryForRigIssueForSantos".equals(this._callFunction)) {  
			xml = this.queryForRigIssueForSantos();
		} else if ("QueryRigMoveForSantos".equals(this._callFunction)) {  
			xml = this.queryForRigMoveForSantos();
		}
		return xml;
	}
	
	public Boolean checkIssueProgressFilter(String rigIssueUid, String searchText) throws Exception {
		String sql = "FROM RigIssueLog WHERE rigIssueUid =:rigIssueUid AND (isDeleted IS NULL OR isDeleted = FALSE) " +
					(searchText.length()>0?" AND comment LIKE '%" + searchText +"%' ":"") +
					" ORDER BY logDateTime ";
		List<RigIssueLog> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigIssueUid", rigIssueUid);		
		
		for(Object objResult: items){
			RigIssueLog thisRigIssueLog = (RigIssueLog) objResult;
			if (thisRigIssueLog.getComment()!=null){
				return true;
			}
		}
		if ("".equals(searchText)){
			return true;
		}
		return false;
	}
	
	public String combineIssueProgress(String rigIssueUid) throws Exception {
		//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat df_3 = new SimpleDateFormat("dd MMM yyyy");
		
		String sql = "FROM RigIssueLog WHERE rigIssueUid =:rigIssueUid  AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY logDateTime ";
		List<RigIssueLog> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigIssueUid", rigIssueUid);		
		
		String issueProgress = "";
		for(Object objResult: items){
			RigIssueLog thisRigIssueLog = (RigIssueLog) objResult;
			if (thisRigIssueLog.getLogDateTime()!=null){
				String logDateTime = "";
				Date dateLogDateTime = thisRigIssueLog.getLogDateTime(); 	
				if (dateLogDateTime!=null){
					logDateTime = df_3.format(dateLogDateTime);
				}
				issueProgress += logDateTime + ":" + thisRigIssueLog.getComment() + "\n" ;
			}
		}
		return issueProgress;
	}
	
	private String getOperationUidWithRigDate(String rigInformationUid, Date dayDate) throws Exception {
		String queryString = "SELECT d.operationUid FROM ReportDaily rd, Daily d, Operation o, Wellbore wb, Well w " +
				"WHERE (w.isDeleted=false or w.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (o.isDeleted=false or o.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND (rd.isDeleted=false or rd.isDeleted is null) " +
				"AND rd.dailyUid=d.dailyUid " +
				"AND d.operationUid=o.operationUid " +
				"AND o.wellboreUid=wb.wellboreUid " +
				"AND wb.wellUid=w.wellUid " +
				"AND d.dayDate=:dayDate " +
				"AND (rd.rigInformationUid=:rigInformationUid or o.rigInformationUid=:rigInformationUid) " +
				"AND rd.reportType!='DGR' " +
				"GROUP BY d.operationUid";
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dayDate","rigInformationUid"}, new Object[]{dayDate, rigInformationUid});
		for (String rec : list) {
			if (StringUtils.isNotBlank(rec)) return rec;
		}
		return "";
	}
	
	private String queryForRigIssueForSantos() throws Exception {
		//Main Container
		Map<String, Object> OperationList = new LinkedHashMap<String, Object>();
		
		//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat df_3 = new SimpleDateFormat("dd MMM yyyy");
		
		String selectedImportance = "";
		Map<String, String> importanceFilters = (Map<String, String>) this.getFilter("importance");
		if (importanceFilters != null) {
			for (Map.Entry<String, String> entry:importanceFilters.entrySet()) {
				selectedImportance += (StringUtils.isNotBlank(selectedImportance)?",":"") + "'" + entry.getValue() + "'";
			}
		}
		
		String selectedResponsibility = nullToEmptyString(this.getFilter("responsibility"));
		String selectedIssueProgress = nullToEmptyString(this.getFilter("issueProgress"));
		String selectedActionRecommended = nullToEmptyString(this.getFilter("actionRecommended"));
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 

		writer.startElement("root");
		
		Map<String, String> operations = (Map<String, String>) this.getFilter("operationUid");
		String operationUids = "";
		for (Map.Entry<String, String> entry : operations.entrySet()) {
			operationUids += (StringUtils.isNotBlank(operationUids)?"','":"") + entry.getValue();
		}
			
		String sql = "SELECT r.operationUid, r.rigInformationUid, r.dateRaised, r.importance, r.equipment, r.issueDescription, r.actionRecommended, r.responsibility, r.estimatedDateCompletion, r.issueProgress, r.status, r.rigIssueUid " +
			"FROM RigIssue r, RigInformation rig " +
			"WHERE (r.isDeleted=false or r.isDeleted is null) " +
			"AND (rig.isDeleted=false or rig.isDeleted is null) " +
			"AND r.rigInformationUid=rig.rigInformationUid " +
			//"AND r.operationUid in ('"+operationUids+"') " +
			"AND r.rigInformationUid in (select rigInformationUid FROM Operation where (isDeleted=false or isDeleted is null) AND operationUid in ('" + operationUids + "')) " +
			(StringUtils.isNotBlank(selectedImportance)?" AND r.importance IN ("+selectedImportance+") ":"") +
			(StringUtils.isNotBlank(selectedResponsibility)?" AND r.responsibility LIKE '%" + selectedResponsibility +"%' ":"") +
			(StringUtils.isNotBlank(selectedActionRecommended)?" AND r.actionRecommended LIKE '%" + selectedActionRecommended +"%' ":"") +
			" order by rig.rigName, r.dateRaised" ;
		List<RigIssue> rigIssueLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		
		for(Object objRigIssue : rigIssueLists){
			Object[] objRigIssues = (Object[]) objRigIssue;		

			
			String rigIssueUid = nullToEmptyString(objRigIssues[11]);
			if (checkIssueProgressFilter(rigIssueUid, selectedIssueProgress) )
			{			
				String operationUid = nullToEmptyString(objRigIssues[0]);
				String rigInformationUid = nullToEmptyString(objRigIssues[1]);
				Date dateRaise = (Date)objRigIssues[2]; 	
				String importance = nullToEmptyString(objRigIssues[3]);
				String equipment = nullToEmptyString(objRigIssues[4]);
				String issueDescription = nullToEmptyString(objRigIssues[5]);
				String actionRecommended = nullToEmptyString(objRigIssues[6]);
				String responsibility = nullToEmptyString(objRigIssues[7]);
				Date dateEstimatedDateCompletion = (Date)objRigIssues[8]; 	
				String status = nullToEmptyString(objRigIssues[10]);

				String dateRaised = "";
				if (dateRaise!=null) dateRaised = df_3.format(dateRaise);
				String estimatedDateCompletion = "";
				if (dateEstimatedDateCompletion!=null) estimatedDateCompletion = df_3.format(dateEstimatedDateCompletion);
				
				try {
					String lookup="xml://yes_no_true_false?key=code&value=label"; 												
					Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
					
					if (list.containsKey(status)) {
						LookupItem selectedLookup = list.get(status);	
						status = (String) selectedLookup.getValue();			
					} 
				} catch (Exception e) {
					e.getMessage();
				}
				
				
				//Data Container
				Map<String, Object> rigIssueListDetail = new HashMap<String, Object>();
				rigIssueListDetail.put("operationUid",nullToEmptyString(operationUid));
				rigIssueListDetail.put("rigInformationUid",getRigName(rigInformationUid));
				operationUid = getOperationUidWithRigDate(rigInformationUid, dateRaise); //find operation
				rigIssueListDetail.put("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
				rigIssueListDetail.put("dateRaised",nullToEmptyString(dateRaised));
				rigIssueListDetail.put("importance",nullToEmptyString(importance));
				rigIssueListDetail.put("equipment",nullToEmptyString(equipment));
				rigIssueListDetail.put("issueDescription",nullToEmptyString(issueDescription));
				rigIssueListDetail.put("actionRecommended",nullToEmptyString(actionRecommended));
				rigIssueListDetail.put("responsibility",nullToEmptyString(responsibility));
				rigIssueListDetail.put("estimatedDateCompletion",nullToEmptyString(estimatedDateCompletion));
				rigIssueListDetail.put("issueProgress",nullToEmptyString(combineIssueProgress(rigIssueUid)));
				rigIssueListDetail.put("status",nullToEmptyString(status));
				
				OperationList.put(rigIssueUid, rigIssueListDetail);
			}	
		}								
		
		SimpleAttributes attr_header_6 = new SimpleAttributes();
		attr_header_6.addAttribute("style", "font-weight:bold;background-color:#90EE90;");
		attr_header_6.addAttribute("width","6");
		
		SimpleAttributes attr_header_10 = new SimpleAttributes();
		attr_header_10.addAttribute("style", "font-weight:bold;background-color:#90EE90;");
		attr_header_10.addAttribute("width","10");
		
		SimpleAttributes attr_header_7 = new SimpleAttributes();
		attr_header_7.addAttribute("style", "font-weight:bold;background-color:#90EE90;");
		attr_header_7.addAttribute("width","7");
		
		SimpleAttributes attr_header_13 = new SimpleAttributes();
		attr_header_13.addAttribute("style", "font-weight:bold;background-color:#90EE90;");
		attr_header_13.addAttribute("width","13");
		
		SimpleAttributes attr_detail_6 = new SimpleAttributes();
		attr_detail_6.addAttribute("width","6");
		
		SimpleAttributes attr_detail_10 = new SimpleAttributes();
		attr_detail_10.addAttribute("width","10");
		
		SimpleAttributes attr_detail_7 = new SimpleAttributes();
		attr_detail_7.addAttribute("width","7");
		
		SimpleAttributes attr_detail_13 = new SimpleAttributes();
		attr_detail_13.addAttribute("width","13");
		
		SimpleAttributes attr = new SimpleAttributes();
		attr.addAttribute("style","borderStyle: solid;borderColor:black;");
		attr.addAttribute("border","1");
		attr.addAttribute("width","100%");
		writer.startElement("table", attr);
		
		writer.startElement("tr");
		writer.addElement("td","Rig", attr_header_6);
		writer.addElement("td","Wellname", attr_header_10);
		writer.addElement("td","Date Raised", attr_header_7);
		writer.addElement("td","Importance", attr_header_7);
		writer.addElement("td","Equipment", attr_header_10);
		writer.addElement("td","Issue", attr_header_13);
		writer.addElement("td","Recommended Action", attr_header_13);
		writer.addElement("td","Task assigned to", attr_header_10);
		writer.addElement("td","Estimated Date of completion", attr_header_7);
		writer.addElement("td","Description", attr_header_10);
		writer.addElement("td","Completed Flag", attr_header_7);
		writer.endElement();		
		
		for (Map.Entry<String, Object> entry : OperationList.entrySet()) {
			Map<String, Object> rigIssueDetail = (Map<String, Object>) entry.getValue();
			writer.startElement("tr");
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("rigInformationUid")),attr_detail_6);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("operationName")),attr_detail_10);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("dateRaised")),attr_detail_7);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("importance")),attr_detail_7);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("equipment")),attr_detail_10);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("issueDescription")),attr_detail_13);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("actionRecommended")),attr_detail_13);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("responsibility")),attr_detail_10);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("estimatedDateCompletion")),attr_detail_7);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("issueProgress")), attr_detail_10);
			writer.addElement("td", nullToEmptyString(rigIssueDetail.get("status")),attr_detail_7);
			writer.endElement();
		}
		writer.endElement();
		
		writer.close();
		return bytes.toString("utf-8");
	}
	
	private Double getRigUpTime(OperationUomContext operationUomContext, Date onlocDateOnlocTime, Date spudDate) {
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			Double riguptime =  CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(onlocDateOnlocTime,spudDate);
			thisConverter.setBaseValue(riguptime);
			return thisConverter.getConvertedValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getRigMoveTime(OperationUomContext operationUomContext, Date rigOnHireDate, Date onlocDateOnlocTime) {
		try {
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			Double rigmovetime =  CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(rigOnHireDate,onlocDateOnlocTime);
			thisConverter.setBaseValue(rigmovetime);
			return thisConverter.getConvertedValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null ;
	}
	
	private Double getRigMoveToRigMoveTime(OperationUomContext operationUomContext, Date rigOnHireDate, Date rigOffHireDate) {
		try {
			Double rigMoveTime = convertTo12Or24Day(CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(rigOnHireDate,rigOffHireDate), operationUomContext);
			return rigMoveTime;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null ;
	}
	
	private Double getRopSpudToTd(OperationUomContext operationUomContext, Date spudDate, Date tdDateTdTime) {
		try {
			Double rigMoveTime =  convertTo12Or24Day(CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(spudDate,tdDateTdTime), operationUomContext);		
			return rigMoveTime;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getTotalActivityDuration(OperationUomContext operationUomContext, String opsid) {
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			String phasetotalquery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " + 
										"WHERE (a.isDeleted=false or a.isDeleted is null) " +
										"AND (d.isDeleted=false or d.isDeleted is null) " +
										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isSimop=false or a.isSimop is null) " +
										"AND (a.isOffline=false or a.isOffline is null) " +
										"AND a.operationUid =:opsid " + 
										"AND a.dailyUid = d.dailyUid ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				 thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));	 
				 return thisConverter.getConvertedValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getProductionCasingTime(OperationUomContext operationUomContext, String opsid) {
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			String phasetotalquery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " + 
										"WHERE (a.isDeleted=false or a.isDeleted is null) " +
										"AND (d.isDeleted=false or d.isDeleted is null) " +
										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isSimop=false or a.isSimop is null) " +
										"AND (a.isOffline=false or a.isOffline is null) " +
										"AND a.operationUid =:opsid " + 
										"AND a.phaseCode LIKE 'PC%'" +
										"AND a.dailyUid = d.dailyUid ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				 Double value = convertTo12Or24Day(Double.parseDouble(result.get(0).toString()), operationUomContext);
				 return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getProductionTime(OperationUomContext operationUomContext, String opsid, String classCode) {
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			String phasetotalquery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " + 
										"WHERE (a.isDeleted=false or a.isDeleted is null) " +
										"AND (d.isDeleted=false or d.isDeleted is null) " +
										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isSimop=false or a.isSimop is null) " +
										"AND (a.isOffline=false or a.isOffline is null) " +
										"AND a.operationUid =:opsid " + 
										"AND a.internalClassCode =:classcode " +
										"AND a.dailyUid = d.dailyUid ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid", "classcode"}, new Object[]{opsid,classCode},qp);
			if (result.get(0) != null){
				 thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));	 
				 Double value = thisConverter.getConvertedValue();
				 return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private String getCsgShoeDateTime(OperationUomContext operationUomContext, String opsid) {
		String csgShoeDateTime="";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			
			String phasetotalquery = "SELECT min(act.startDatetime) as shoeTime, min(d.dayDate) as shoeDate FROM Activity act, Daily d WHERE (d.isDeleted=false OR d.isDeleted is null) AND (act.isDeleted=false OR act.isDeleted is null) AND act.operationUid=:opsid AND act.phaseCode='PH' AND act.taskCode='D' AND act.depthMdMsl = (SELECT min(ac.depthMdMsl) as maxdepth FROM Activity ac WHERE (ac.isDeleted=false OR ac.isDeleted is null) AND ac.operationUid=:opsid AND ac.phaseCode='PH' AND ac.taskCode='D' GROUP BY ac.operationUid) AND act.dailyUid = d.dailyUid GROUP BY d.dayDate order by min(d.dayDate) ASC ";
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.size()>0) {
				SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm:ss");		
				Date shoeTime = (Date) result.get(0)[0];		
				Date shoeDate = (Date) result.get(0)[1];			
				csgShoeDateTime = dfDate.format(shoeDate) + " " + dfTime.format(shoeTime);
			}
			
			}catch (Exception e){
				e.printStackTrace();
			}
			return csgShoeDateTime;
	}
	
	private Double getCsgShoeDepth(OperationUomContext operationUomContext, String opsid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);	
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			String phasetotalquery = "SELECT cj.operationUid, cj.tdmdMsl FROM CementJob cj, CasingSection cs WHERE (cj.isDeleted=false OR cj.isDeleted is null) AND (cs.isDeleted=false or cs.isDeleted is null) AND cs.operationUid = cj.operationUid AND cs.casingSectionUid = cj.casingSectionUid AND cj.operationUid = :opsid AND cs.sectionName='surface'";
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.size()>0){	
				return (Double) result.get(0)[1];
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getOperationCost(OperationUomContext operationUomContext, String opsid, String taskCode) {
		Double cumCost=null;
		Double cumClassTotal = 0.00;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);

			String strSql = "select rd.dailyUid, act.phaseCode, act.classCode, act.taskCode, sum(coalesce(act.activityDuration,0.0)) as duration from Activity act, ReportDaily rd where (act.isDeleted = false or act.isDeleted is null) ";
			strSql += "and (act.isSimop=false or act.isSimop is null) and (act.isOffline=false or act.isOffline is null) and act.operationUid=:opsid AND act.taskCode=:taskcode ";
			strSql += "and (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) ";
			strSql += "and (act.dailyUid=rd.dailyUid) ";
			strSql += "and (rd.isDeleted = false or rd.isDeleted is null) GROUP BY rd.dailyUid, act.phaseCode, act.classCode, act.taskCode";
			
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","taskcode"}, new Object[]{opsid,taskCode},qp);
			if (result.size()>0) {
					
				for (Object[] rec : result) {
					strSql = "select sum(coalesce(act.activityDuration,0.0)) as duration from Activity act where (act.isDeleted = false or act.isDeleted is null) ";
					strSql += "and (act.isSimop=false or act.isSimop is null) and (act.isOffline=false or act.isOffline is null) ";
					strSql += "and (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) ";
					strSql += "and act.dailyUid=:dailyUid AND act.phaseCode=:phaseCode AND act.classCode=:classCode ";
					Double classDuration = 0.00;
					List resultDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"dailyUid","phaseCode","classCode"}, new Object[]{rec[0].toString(),rec[1].toString(),rec[2].toString()},qp);
					if (resultDuration.get(0) != null){
						classDuration = Double.parseDouble(resultDuration.get(0).toString());			
					}
					
					String phasetotalquery = "SELECT SUM(quantity * itemCost) AS well_total FROM CostDailysheet " +
					 "WHERE (isDeleted=false or isDeleted is null) " +	
					 "AND dailyUid=:dailyUid AND phaseCode=:phaseCode AND classCode=:classCode AND hourlyCalc ='1' ";	

					List resultCost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"dailyUid","phaseCode","classCode"}, new Object[]{rec[0].toString(),rec[1].toString(),rec[2].toString()},qp);
					if (resultCost.get(0) != null){
						Double classTotal = Double.parseDouble(resultCost.get(0).toString());
						cumClassTotal += classTotal * Double.parseDouble(rec[4].toString()) / classDuration;					
					}			
				}
							
				thisConverter.setBaseValue(cumClassTotal);
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return cumCost;
	}
	
	private Double getActualCost(OperationUomContext operationUomContext, String opsid) {
		Double totalWellCost=null;

		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);
			String phasetotalquery = "SELECT  SUM(costAmount) AS actual_cost FROM OperationActualCost " +
									 "WHERE (isDeleted=false or isDeleted is null) " +	
									 "AND operationUid = :opsid ";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				totalWellCost = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return totalWellCost;
	}
	

	
	private Double getTotalWellCost(OperationUomContext operationUomContext, String opsid) {
		Double totalWellCost=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);
			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) AS well_total FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid ";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				totalWellCost = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return totalWellCost;
	}
	
	private Double getSpudToTdCost(OperationUomContext operationUomContext, String opsid) {
		Double totalWellCost=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);

			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) AS well_total FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid " +
									 "and c.phaseCode IN ('SC','SH','PH','PH1','PH2','PH3')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){					
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				totalWellCost = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return totalWellCost;
	}
	
	private Double getProductionHoleCost(OperationUomContext operationUomContext, String opsid) {
		Double totalWellCost=null;

		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);

			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) AS well_total FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid " +
									 "and c.phaseCode IN ('PH','PH1','PH2','PH3')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){		
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				totalWellCost = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return totalWellCost;
	}
	
	
	private Double getPhaseTotalByCode(OperationUomContext operationUomContext, String opsid, String phasecode) {
		Double avalue=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "daycost", operationUomContext);
			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) AS well_total FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid " +
									 "AND c.phaseCode = :code";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid","code"}, new Object[]{opsid,phasecode},qp);
			if (result.get(0) != null){					
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				avalue = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return avalue ;
	}
	
	private Double getAfeSpudToTd(OperationUomContext operationUomContext, String opsid){
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(opsid);
			String queryString = "select sum(coalesce(p.p50Duration, 0.0)) FROM OperationPlanMaster m, OperationPlanPhase p " +
					"WHERE (m.isDeleted=false or m.isDeleted is null) " +
					"AND (p.isDeleted=false or p.isDeleted is null) " +
					"AND m.operationPlanMasterUid=p.operationPlanMasterUid " + 
					"AND m.operationPlanMasterUid=:dvdPlanUid " +
					"AND p.phaseCode in ('SH', 'SC', 'PH') " +
					"ORDER BY sequence";
			List<Double> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dvdPlanUid", dvdPlanUid, qp);
			if (list.size()>0 && list.get(0)!=null) {
				return list.get(0);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}
	
	private Double getActivityDurationByPhase(OperationUomContext operationUomContext, String opsid, String code) {
		Double actDuration=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
	
			String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d " +
							"WHERE (d.isDeleted = false or d.isDeleted is null) " +
							"AND (a.isDeleted = false or a.isDeleted is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND d.operationUid = :opsid " +
							"AND d.dailyUid=a.dailyUid " +
							"AND a.phaseCode = :code " +
							"AND (a.isOffline=false or a.isOffline is null)";
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","code"}, new Object[]{opsid,code},qp);
			if (result.get(0) != null){		
				Double value = convertTo12Or24Day(Double.parseDouble(result.get(0).toString()),operationUomContext);
				actDuration = value;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return actDuration ;
	}
	
	private Double getTotalEvalBeforeTD(OperationUomContext operationUomContext, Date spuddate, Date tddatetime, String opsid) {
		//String output = "0.0";
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String totalEvalBeforeTDQuery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a, Daily d " +
											"WHERE (a.isDeleted=false or a.isDeleted is null) " +
											"AND (d.isDeleted=false or d.isDeleted is null) " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
											"AND (a.isSimop=false or a.isSimop is null) " +
											"AND (a.isOffline=false or a.isOffline is null) " +
											"AND a.operationUid =:opsid " + 
											"AND a.phaseCode IN ('EP','EI') " +
											"AND a.dailyUid = d.dailyUid " +
											"AND a.startDatetime >=:spuddate " +
											"AND a.endDatetime <=:tddatetime";
			List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","spuddate","tddatetime"}, new Object[]{opsid,spuddate,tddatetime},qp);
			if (totalEvalBeforeTD.get(0) != null){			
				 return convertTo12Or24Day(Double.parseDouble(totalEvalBeforeTD.get(0).toString()),operationUomContext);
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return null;
		
	}
	
	private Double getTotalEvalAfterTD(OperationUomContext operationUomContext, Date tddatetime, String opsid) {
		Double output = null;
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String totalEvalBeforeTDQuery = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration FROM Activity a,Daily d " +
											"WHERE (a.isDeleted=false or a.isDeleted is null) " +
											"AND (d.isDeleted=false or d.isDeleted is null) " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
											"AND (a.isSimop=false or a.isSimop is null) " +
											"AND (a.isOffline=false or a.isOffline is null) " +
											"AND a.operationUid =:opsid " + 
											"AND a.phaseCode IN ('EP','EI') " +
											"AND a.dailyUid = d.dailyUid " +
											"AND a.startDatetime >=:tddatetime";
			List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","tddatetime"}, new Object[]{opsid,tddatetime},qp);
			if (totalEvalBeforeTD.get(0) != null){
				 Double value = convertTo12Or24Day(Double.parseDouble(totalEvalBeforeTD.get(0).toString()),operationUomContext);
				 output = value;
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return output;
		
	}
	
	private Double getTimeBetweenDuration(OperationUomContext operationUomContext, Date endTime, Date startTime) {
		try {
			return  convertTo12Or24Day(CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(endTime,startTime), operationUomContext);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null ;
	}
	
	private Double getCompletionCost(OperationUomContext operationUomContext, String opsid) {
		Double totalHoleCost=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);

			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +	
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid AND c.phaseCode IN ('PC','PC1','PC2','PC3','C','CTB')";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
						
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				Double value = thisConverter.getConvertedValue();
				totalHoleCost = value;
				}else{
					String phasetotalquery2 = "SELECT SUM(c.quantity * c.itemCost) FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +	
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid AND c.phaseCode IN ('PA','ABN')";	

					 List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery2,new String[]{"opsid"}, new Object[]{opsid},qp);
					 if (results.get(0) != null){					 
						thisConverter.setBaseValue(Double.parseDouble(results.get(0).toString()));
						Double value = thisConverter.getConvertedValue();
						totalHoleCost = value;
					 }
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		return totalHoleCost;	
		}

	private Double getBulkStockUsedItem(OperationUomContext operationUomContext, String opsid, String stockType) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			String phasetotalquery = "SELECT  SUM(r.amtUsed) FROM RigStock r, Daily d " +
									 "WHERE (r.isDeleted=false or r.isDeleted is null) " +	
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND r.dailyUid=d.dailyUid " +	
									 "AND d.operationUid = :opsid " +
									 "AND (r.stockBrand=:stockType OR r.stockCode in (select lookupRigStockUid FROM LookupRigStock WHERE stockBrand=:stockType)) ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid","stockType"}, new Object[]{opsid,stockType},qp);
			if (result.get(0) != null){
				return (Double) result.get(0);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getShMudCost(OperationUomContext operationUomContext, String opsid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);
			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +	
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid AND c.phaseCode IN ('SH','SH1','SH2','SH3') " +
									 "AND c.accountCode ='10307' and c.afeShortDescription='Drilling Fluid' AND c.afeItemDescription='Daily Drilling Mud Cost' ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));			
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getPhMudCost(OperationUomContext operationUomContext, String opsid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "afe", operationUomContext);

			String phasetotalquery = "SELECT  SUM(c.quantity * c.itemCost) FROM CostDailysheet c, Daily d " +
									 "WHERE (c.isDeleted=false or c.isDeleted is null) " +	
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND d.dailyUid=c.dailyUid " +	
									 "AND d.operationUid = :opsid AND c.phaseCode IN ('PH','PH1','PH2','PH3','SC','EP','PC') " +
									 "AND c.accountCode ='10307' and c.afeShortDescription='Drilling Fluid' AND c.afeItemDescription='Daily Drilling Mud Cost' ";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null ;
	}
	
	private String getPobManHour(OperationUomContext operationUomContext, String opsid) {
		String totalManHour="0.0";
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			

			String phasetotalquery = "SELECT  SUM(p.pax) FROM PersonnelOnSite p, Daily d " +
									 "WHERE (p.isDeleted=false or p.isDeleted is null) " +
									 "AND (d.isDeleted=false or d.isDeleted is null) " +
									 "AND p.dailyUid=d.dailyUid " +
									 "AND d.operationUid = :opsid " +
									 "AND p.pax IS NOT NULL ";	
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"opsid"}, new Object[]{opsid},qp);
			if (result.get(0) != null){		
				totalManHour = result.get(0).toString();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return totalManHour ;
	}
	
	private String gotLTI(OperationUomContext operationUomContext, String opsid) {
		String output ="0";
		
		try{
		String hsequery = "SELECT COUNT(hseIncidentUid) FROM HseIncident WHERE (isDeleted=false or isDeleted is null) AND operationUid=:opsid AND incidentCategory IN ('Lost Time Incident','Medical Treatment Incident')";
		List hse = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hsequery, new String[]{"opsid"}, new Object[]{opsid});
		if (!hse.isEmpty()){
			String value = hse.get(0).toString() ;
			if ("0".equals(value)){
				output = "N";
			}else{
				output = "Y";
			}
		}		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return output;	
		
	}
	
	private Double getDurationBasedOnUserCode(OperationUomContext operationUomContext, String opsid,String codes) {
		try{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			
			
			String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d " +
							"WHERE (a.isDeleted = false or a.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND a.dailyUid = d.dailyUid " +
 							"AND a.operationUid = :opsid " +
							"AND a.userCode = :code " +
							"AND (a.isOffline=false or a.isOffline is null)";
							
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"opsid","code"}, new Object[]{opsid,codes},qp );
		
			if (result.get(0) != null ){
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));	
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getProgress(OperationUomContext operationUomContext, String bitrunUid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			String phasetotalquery = "SELECT SUM(bds.progress) as progress FROM BharunDailySummary bds, Bitrun br, Bharun bha WHERE (br.isDeleted=false OR br.isDeleted is null) AND (bds.isDeleted=false or bds.isDeleted is null) AND (bha.isDeleted=false or bha.isDeleted is null) AND br.bitrunUid=:bitrunUid AND br.bharunUid=bds.bharunUid AND bds.bharunUid=bha.bharunUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"bitrunUid"}, new Object[]{bitrunUid},qp);
			if (result.get(0) != null){	
				return (Double) result.get(0);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private Double getTimeOnBtm(OperationUomContext operationUomContext, String bitrunUid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), BharunDailySummary.class, "duration",operationUomContext);
			String phasetotalquery = "SELECT SUM(bds.duration) as duration FROM BharunDailySummary bds, Bitrun br, Bharun bha WHERE (br.isDeleted=false OR br.isDeleted is null) AND (bds.isDeleted=false or bds.isDeleted is null) AND (bha.isDeleted=false or bha.isDeleted is null) AND br.bitrunUid=:bitrunUid AND br.bharunUid=bds.bharunUid AND bds.bharunUid=bha.bharunUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"bitrunUid"}, new Object[]{bitrunUid},qp);
			if (result.get(0) != null){					
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	private Double getIADCDuration(OperationUomContext operationUomContext, String bitrunUid) {
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), BharunDailySummary.class, "IADCDuration",operationUomContext);
			String phasetotalquery = "SELECT SUM(bds.IADCDuration) as duration FROM BharunDailySummary bds, Bitrun br, Bharun bha WHERE (br.isDeleted=false OR br.isDeleted is null) AND (bds.isDeleted=false or bds.isDeleted is null) AND (bha.isDeleted=false or bha.isDeleted is null) AND br.bitrunUid=:bitrunUid AND br.bharunUid=bds.bharunUid AND bds.bharunUid=bha.bharunUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,new String[]{"bitrunUid"}, new Object[]{bitrunUid},qp);
			
			if (result.get(0) != null){					
				thisConverter.setBaseValue(Double.parseDouble(result.get(0).toString()));
				return thisConverter.getConvertedValue();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String roundDown(int decimalPlace, double r){
	    String value = "0.0";
	    BigDecimal bd = new BigDecimal(r);
	    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_DOWN);
	    r = bd.doubleValue();
	    value = Double.toString(r);
	    return value;
	}
	
	private Double getAverageConnectionTime(OperationUomContext operationUomContext, String operationUid, Double feet) {
		Double duration=null;
		try{	
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);			
			String bitQuery = "SELECT br.operationUid, br.bitrunUid, br.bitrunNumber, br.bitType, br.bitDiameter, br.make, br.model, br.iadcCode, br.depthInMdMsl, br.depthOutMdMsl, br.condFinalReason, br.comment, br.serialNumber, br.dailyUidIn, br.dailyUidOut, br.condFinalInner, br.condFinalOuter, br.condFinalDull, br.condFinalLocation, br.condFinalBearing, br.condFinalGauge, br.condFinalOther FROM Bitrun br, Bharun bha, BharunDailySummary bs WHERE (bs.isDeleted=false or bs.isDeleted is null) AND (br.isDeleted=false or br.isDeleted is null) AND (bha.isDeleted=false or bha.isDeleted is null) AND br.bharunUid=bha.bharunUid AND bha.bharunUid=bs.bharunUid AND br.operationUid=:operationUid GROUP BY br.bitrunUid";
			List <Object[]> BitrunDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitQuery, new String[]{"operationUid"}, new Object[]{nullToEmptyString(operationUid)},qp);
			
			Double cumConnectionTime = 0.0;
			Double cumNumberConnection = 0.0;
			
			for (Object[] BitrunDetailobj:BitrunDetail){
				if (BitrunDetailobj[0]!=null){
					Double progress = getProgress(operationUomContext,nullToEmptyString(BitrunDetailobj[1])); // * feet;
					if (progress!=null) {
						progress = progress * feet;
						if (progress >= 500){
							Double IADCDuration = getIADCDuration(operationUomContext,nullToEmptyString(BitrunDetailobj[1]));
							Double timeOnBtm = getTimeOnBtm(operationUomContext,nullToEmptyString(BitrunDetailobj[1]));
							if (IADCDuration==null) IADCDuration = 0.0;
							if (timeOnBtm==null) timeOnBtm = 0.0;
							Double connectionTime = IADCDuration - timeOnBtm;
												
							Double numberConnection = progress / 31.5; //in feet			
							numberConnection = Math.round(numberConnection * 100) / 100.0;
										
							Double connectionTimeInMinute = 0.0;
							if (numberConnection != 0.0){
								connectionTimeInMinute = connectionTime / numberConnection * 60;	
								connectionTimeInMinute = Math.round(connectionTimeInMinute * 100) / 100.0;
							}					
							
							cumConnectionTime += connectionTime;
							cumNumberConnection += numberConnection;				
						}	
					}						
				}
			}
			
			Double averageConnectionTime = 0.00;
			if (cumNumberConnection != 0.0){
				averageConnectionTime = cumConnectionTime / cumNumberConnection * 60;
				duration = averageConnectionTime;
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return duration;
	}
	
	public String getRigName (String rigInformationUid){
		String rigName = "";
		try{	
			String query = "SELECT rigName FROM RigInformation WHERE (isDeleted=false OR isDeleted is null) AND rigInformationUid=:rigInformationUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query,new String[]{"rigInformationUid"}, new Object[]{rigInformationUid});
			if (!result.isEmpty()){					
				rigName = result.get(0).toString();
				}
			
			}catch (Exception e){
				e.printStackTrace();
			}
		return rigName;
	}
	
	public String getPurposeType(String code){
		String purposeType = "";
		try{	
			String query = "SELECT name FROM LookupPurposeType WHERE (isDeleted=false OR isDeleted is null) AND code=:code";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query,new String[]{"code"}, new Object[]{code});
			if (!result.isEmpty()){					
				purposeType = result.get(0).toString();
				}
			
			}catch (Exception e){
				e.printStackTrace();
			}
		
		return purposeType;
	}
	
	public String getCompanyname(String lookupCompanyUid){
		String cname= "";
		try{	
			String query = "SELECT companyName FROM LookupCompany WHERE (isDeleted=false OR isDeleted is null) AND lookupCompanyUid=:lookupCompanyUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query,new String[]{"lookupCompanyUid"}, new Object[]{lookupCompanyUid});
			if (!result.isEmpty()){					
				cname = result.get(0).toString();
				}
			
			}catch (Exception e){
				e.printStackTrace();
			}
		
		return cname;
	}
	
	public Date getDayDate(String dailyUid){
		try{	
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if (daily!=null){
				return daily.getDayDate();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String queryForEndOfMonthWellSummaryForSantos()throws Exception{
		
		List<String> operations = new ArrayList<String>();		
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}	
		
		//Declarations
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);			
		SimpleAttributes tableAttr = new SimpleAttributes();
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		String filter ="";
		Integer count = 0;
		for (String operationUid : operations) {
			if (count == 0){
				filter = "('" + operationUid;
			}
			else {
				filter= filter + "','" + operationUid ;
			}
			count ++;
		}
		filter += "') ";
		
		String opquery = "SELECT o.operationUid,w.wellName,o.spudDate " +
			"FROM Well w, Wellbore wb, Operation o " +
			"WHERE (o.isDeleted=false or o.isDeleted is null) " +
			"AND (wb.isDeleted=false or wb.isDeleted is null) " +
			"AND (w.isDeleted=false or w.isDeleted is null) " +
			"AND w.wellUid=wb.wellUid " +
			"AND wb.wellboreUid=o.wellboreUid " +
			"AND o.operationUid IN " + filter +
			"ORDER BY o.spudDate, w.wellName";
		
		List <Object[]> opsDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().find(opquery,qp);
		
		if (opsDetail.size()>0){
			operations.clear();
			for (Object[] opsDetailobj:opsDetail){	
				if (opsDetailobj[0]!=null){
					operations.add(opsDetailobj[0].toString());
				}
			}
		}
		
		//Main Container
		Map<String, Object> OperationList = new HashMap<String, Object>();
		
		DecimalFormat formatter; 
		formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);	
		
		DecimalFormat formatter_0; 
		formatter_0 = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter_0.setMinimumFractionDigits(0);
		formatter_0.setMaximumFractionDigits(0);	
		
		/*
		DecimalFormat formatter_1; 
		formatter_1 = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter_1.setMinimumFractionDigits(1);
		formatter_1.setMaximumFractionDigits(1);
		*/
		
		DecimalFormat formatter_6; 
		formatter_6 = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter_6.setMinimumFractionDigits(6);
		formatter_6.setMaximumFractionDigits(6);
		
		Double feet = 3.280839895013123;
		//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat df_1 = new SimpleDateFormat("dd MMM yyyy HH:mm");
		SimpleDateFormat df_2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df_3 = new SimpleDateFormat("dd MMM yyyy");
		
		writer.startElement("root");
		Integer seq = 1;
		//Data Extraction - Begin
		for (String operationUid : operations) {
			//Data Container
			Map<String, Object> EndOfMonthListDetail = new HashMap<String, Object>();
			
			//Declaration
			String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);	
			operationUomContext.setOperationUid(operationUid);
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
							
			String key = ((seq < 10)? "0"+ seq.toString():seq.toString()) + "_" + operationUid;
			seq++;

			OperationList.put(key, EndOfMonthListDetail);	
			EndOfMonthListDetail.put("operationName",nullToEmptyString(fullWellName));
			EndOfMonthListDetail.put("operationUid",nullToEmptyString(operationUid));
			
			//EndOfMonth Breakdown 
			String query = "SELECT o.operationUid,w.wellName,w.state,o.projectNo,o.rigInformationUid,w.purposeType,wb.wellboreFinalPurpose,w.technologyType,w.block, o.rigMoveDistance,o.spudDate,o.tdDateTdTime,o.rigOffHireDate,w.licenseNumber,w.basin, o.afe,o.amountSpentPriorToSpud,o.daysSpentPriorToSpud,o.rigOnHireDate,o.onlocDateOnlocTime, " +
				"(SELECT MAX(cs.casingOd) FROM CasingSection cs WHERE (cs.isDeleted=false or cs.isDeleted is null) AND cs.operationUid = o.operationUid AND cs.type = 'SC') as scSize, " +
				"(SELECT MAX(pcs.casingOd) FROM CasingSection pcs WHERE (pcs.isDeleted=false or pcs.isDeleted is null) AND pcs.operationUid = o.operationUid AND pcs.type = 'PC') as pcSize, " +
				"(SELECT MAX(rd.depthMdMsl) as TD FROM Activity rd WHERE (rd.isDeleted=false OR rd.isDeleted is null) AND rd.operationUid = o.operationUid	) as TD, " +
				"(SELECT MAX(lot.estimatedMudWeight) FROM LeakOffTest lot WHERE (lot.isDeleted=false OR lot.isDeleted is null) AND lot.operationUid = o.operationUid) as lotEmw, " +
				"(SELECT MAX(rdcsg.lastCsgshoeMdMsl) as lastCsgShoeDepth FROM ReportDaily rdcsg WHERE (rdcsg.isDeleted=false OR rdcsg.isDeleted is null) AND rdcsg.operationUid = o.operationUid) as lastCsgShoeDepth, "  +
				"(SELECT SUM(tep.activityDuration) as totalEvalPhase FROM Activity tep, Daily d WHERE  (tep.isDeleted=false OR tep.isDeleted is null) AND (d.isDeleted=false OR d.isDeleted is null) AND (tep.carriedForwardActivityUid='' or tep.carriedForwardActivityUid is null) AND (tep.isSimop=false or tep.isSimop is null) AND (tep.isOffline=false or tep.isOffline is null) AND tep.operationUid = o.operationUid AND tep.dailyUid = d.dailyUid AND tep.phaseCode IN ('EP','EI') ), " +
				"o.rigOffHireDate " +
				"FROM Well w, Wellbore wb, Operation o " +
				"WHERE (o.isDeleted=false or o.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (w.isDeleted=false or w.isDeleted is null) " +
				"AND w.wellUid=wb.wellUid " +
				"AND wb.wellboreUid=o.wellboreUid " +
				"AND o.operationUid=:operationUid " +
				"ORDER BY o.spudDate, o.operationUid ";
				
			List <Object[]> EndOfMonthDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
			for (Object[] EndOfMonthDetailobj:EndOfMonthDetail){											
				if (EndOfMonthDetailobj[10]!=null){			
					Date spudDate = (Date)EndOfMonthDetailobj[10]; 		
					EndOfMonthListDetail.put("spudDate", df_1.format(spudDate));
					EndOfMonthListDetail.put("year", spudDate.toString().substring(0,4));
				}
				
				if (EndOfMonthDetailobj[2]!=null){
					String lookup="xml://well.state?key=code&value=label"; 												
					String stateName = nullToEmptyString(EndOfMonthDetailobj[2]);
					try {
						Map<String,LookupItem> stateList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
						if (stateList.containsKey(stateName)) {
							LookupItem selectedLookup = stateList.get(stateName);	
							stateName = (String) selectedLookup.getValue();			
						} 
					} catch (Exception e) {
						e.getMessage();
					}
					EndOfMonthListDetail.put("state", stateName);
				}
				
				if (EndOfMonthDetailobj[4]!=null){
					String rigName ="";
					rigName = getRigName(nullToEmptyString(EndOfMonthDetailobj[4]));
					EndOfMonthListDetail.put("rigInformationUid", rigName);		
				}
				
				if (EndOfMonthDetailobj[3]!=null){
					EndOfMonthListDetail.put("afebasis", EndOfMonthDetailobj[3].toString());
				}
				
				if (EndOfMonthDetailobj[1]!=null){
					EndOfMonthListDetail.put("wellName", EndOfMonthDetailobj[1].toString());
				}
				
				Double actualWellCost = this.getActualCost(operationUomContext, operation.getOperationUid());
				if (actualWellCost!=null) {
					actualWellCost = Math.round(actualWellCost * 100) / 100.0;
					EndOfMonthListDetail.put("actualWellCost", formatter.format(actualWellCost));
				} else {
					EndOfMonthListDetail.put("actualWellCost", "");
				}
				
				Double totalWellCost = this.getTotalWellCost(operationUomContext, operation.getOperationUid());
				if (totalWellCost!=null) {
					totalWellCost = Math.round(totalWellCost * 100) / 100.0;
					EndOfMonthListDetail.put("totalWellCost", formatter.format(totalWellCost));
				} else {
					EndOfMonthListDetail.put("totalWellCost", "");
				}
				
				/*if (EndOfMonthDetailobj[15]!=null){
					Double afe = 0.00;
					afe = Double.parseDouble(EndOfMonthDetailobj[15].toString());
					afe = Math.round(afe * 100) / 100.0;
					EndOfMonthListDetail.put("afe", formatter_0.format(afe) );
				}*/
				
				//TODO: get afe
				String gwpvalue = "";
				String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
				String queryString = "FROM OperationPlanMaster m " +
						"WHERE (m.isDeleted=false or m.isDeleted is null) " +
						"AND m.operationPlanMasterUid=:dvdPlanUid";
				List<OperationPlanMaster> listDvd = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dvdPlanUid", dvdPlanUid, qp);
				for (OperationPlanMaster dvd : listDvd) {
					String costType = dvd.getCostPhaseType();
					if (StringUtils.isNotBlank(costType)) {
						if ("CS".equalsIgnoreCase(costType)) {
							gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForCaseAndSuspend");
						}else if ("PA".equalsIgnoreCase(costType)) {
							gwpvalue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "phasesExcludedForPlugAndAbandon");
						}
					}
				}
				queryString ="select sum(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) FROM CostAfeDetail d, CostAfeMaster m " +
						" WHERE (m.isDeleted=false or m.isDeleted is null) " +
						" AND (d.isDeleted=false or d.isDeleted is null) " +
						" AND d.costAfeMasterUid=m.costAfeMasterUid " +
						(StringUtils.isNotBlank(gwpvalue)?" AND d.phaseCode not in ('" + StringUtils.join(gwpvalue.split(";"), "','")+ "') ":"") + 
						" AND m.operationUid=:operationUid ";
				List<Double> listAfe = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
				Double afeTotal = 0.0;
				if (listAfe.size()>0 && listAfe.get(0)!=null) {
					afeTotal = listAfe.get(0);
				}
				EndOfMonthListDetail.put("afe", formatter.format(afeTotal) );
				
				Double maxdepth = 0.00;
				if (EndOfMonthDetailobj[22]!=null){			
					maxdepth = Double.parseDouble(EndOfMonthDetailobj[22].toString()) * feet;
					EndOfMonthListDetail.put("tmd", formatter.format(maxdepth));
					Double costPerFeet = 0.00;
					if (actualWellCost!=null && actualWellCost > 0 && maxdepth > 0){
						costPerFeet = actualWellCost/maxdepth ;
					}
					else if (totalWellCost !=null && totalWellCost > 0 && maxdepth > 0) {
						costPerFeet = totalWellCost/maxdepth ;
					}
					
					costPerFeet = Math.round(costPerFeet * 100) / 100.0;		
					EndOfMonthListDetail.put("totalWellCostPerFeet", formatter.format(costPerFeet));
				}
				
				if (EndOfMonthDetailobj[18]!=null){
					
					Date rigOnHireDate = (Date) EndOfMonthDetailobj[18]; 
					
					if (EndOfMonthDetailobj[26]!=null){
						Date rigOffHireDate = (Date) EndOfMonthDetailobj[26]; 
						Double rigMoveTORigMove = getRigMoveToRigMoveTime(operationUomContext, rigOnHireDate, rigOffHireDate);
						rigMoveTORigMove = Math.round(rigMoveTORigMove * 100) / 100.0;
						EndOfMonthListDetail.put("rigMoveTORigMove", formatter.format(rigMoveTORigMove));
					}
					
					if (EndOfMonthDetailobj[19] != null) {
						Date onlocDateOnlocTime = (Date) EndOfMonthDetailobj[19]; 
						Double rigMoveTime= getRigMoveTime(operationUomContext, rigOnHireDate, onlocDateOnlocTime);
						rigMoveTime = Math.round(rigMoveTime * 100) / 100.0;
						EndOfMonthListDetail.put("rigMoveTime", formatter.format(rigMoveTime));
					}			
				}
				
				//Double spudToTdCost = 0.00;
				Double spudToTdCostPerDepth = null;
				Double spudToTdCost = getSpudToTdCost(operationUomContext, operation.getOperationUid());	
				if (maxdepth > 0 && spudToTdCost!=null){
					spudToTdCostPerDepth = spudToTdCost/maxdepth;
				}
				
				if (spudToTdCostPerDepth!=null) {
					spudToTdCostPerDepth = Double.parseDouble(roundDown(0,spudToTdCostPerDepth));	
					EndOfMonthListDetail.put("spudToTdCostPerDepth", formatter.format(spudToTdCostPerDepth));
				} else {
					EndOfMonthListDetail.put("spudToTdCostPerDepth", "");
				}
				
				Double productionHoleTime = 0.00;
				Date tdDateTdTime = (Date)EndOfMonthDetailobj[11]; 
				Date spudDate = (Date)EndOfMonthDetailobj[10]; 
				if (tdDateTdTime!=null & spudDate != null){
					EndOfMonthListDetail.put("tdDateTdTime", df_1.format(tdDateTdTime));
					Double spudToTd = getRopSpudToTd(operationUomContext, spudDate, tdDateTdTime);
					if (spudToTd!=null) {
						Double spudToTdPerDepth = spudToTd / maxdepth * 10000;
						spudToTdPerDepth = Math.round(spudToTdPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("spudToTdPerDepth", formatter.format(spudToTdPerDepth));
						
						if (spudToTd > 0.0){
							Double ropSpudToTd = maxdepth / spudToTd;
							ropSpudToTd = Double.parseDouble(roundDown(0,ropSpudToTd));
							EndOfMonthListDetail.put("ropSpudToTd", formatter.format(ropSpudToTd));
						}			
						
						spudToTd = Math.round(spudToTd * 100) / 100.0;
						EndOfMonthListDetail.put("spudToTd", formatter.format(spudToTd));
					}
					
					Date rigreleaseDateRigreleaseTime = (Date)EndOfMonthDetailobj[12]; 
					if (rigreleaseDateRigreleaseTime!=null){
						Double spudToRigUpDuration = getTimeBetweenDuration(operationUomContext, spudDate, rigreleaseDateRigreleaseTime);
						if (spudToRigUpDuration!=null) {
							EndOfMonthListDetail.put("spudToRigUpDuration", formatter.format(spudToRigUpDuration));
						}
						EndOfMonthListDetail.put("rigreleaseDateRigreleaseTime", df_1.format(rigreleaseDateRigreleaseTime));
					}
					
					Double productionCasingTime = getProductionCasingTime(operationUomContext, operation.getOperationUid());
					if (productionCasingTime!=null) {
						productionCasingTime = Math.round(productionCasingTime * 100) / 100.0;
						EndOfMonthListDetail.put("productionCasingTime", formatter.format(productionCasingTime));
					}
					
					productionCasingTime = getTimeBetweenDuration(operationUomContext, spudDate, rigreleaseDateRigreleaseTime);
					Double totalEvaluationPhase = 0.00;
					if (EndOfMonthDetailobj[25] != null) {
						totalEvaluationPhase = convertTo12Or24Day(Double.parseDouble(EndOfMonthDetailobj[25].toString()), operationUomContext);
						Double spudToRigRelease = productionCasingTime - totalEvaluationPhase;
						spudToRigRelease = Math.round(spudToRigRelease * 100) / 100.0;
						EndOfMonthListDetail.put("spudToRigRelease", formatter.format(spudToRigRelease));
					}	 
											
					if (EndOfMonthDetailobj[19] != null) {
						Date onlocDateOnlocTime = (Date)EndOfMonthDetailobj[19]; 
						Double rigUpTime= getRigUpTime(operationUomContext, onlocDateOnlocTime, spudDate);
						if (rigUpTime!=null) {
							rigUpTime = Math.round(rigUpTime * 100) / 100.0;
							EndOfMonthListDetail.put("rigUpTime", formatter.format(rigUpTime));
						}
					}	
					
					Double evaluationPhaseBeforeTd = getTotalEvalBeforeTD(operationUomContext, spudDate, tdDateTdTime, operation.getOperationUid());
					if (evaluationPhaseBeforeTd!=null) {
						evaluationPhaseBeforeTd = Math.round(evaluationPhaseBeforeTd * 100) / 100.0;
						EndOfMonthListDetail.put("evaluationPhaseBeforeTd", formatter.format(evaluationPhaseBeforeTd));
					}
					
					if (EndOfMonthDetailobj[11]!=null){
						Double evaluationPhaseAfterTd = getTotalEvalAfterTD(operationUomContext, tdDateTdTime, operation.getOperationUid());
						if (evaluationPhaseAfterTd!=null) {
							evaluationPhaseAfterTd = Math.round(evaluationPhaseAfterTd * 100) / 100.0;
							EndOfMonthListDetail.put("evaluationPhaseAfterTd", formatter.format(evaluationPhaseAfterTd));
						} else {
							EndOfMonthListDetail.put("evaluationPhaseAfterTd", "");
						}
						
					}
					SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					if (!"".equals(getCsgShoeDateTime(operationUomContext,operation.getOperationUid()))){
						Date csgShoeDateTime = dfs.parse(getCsgShoeDateTime(operationUomContext,operation.getOperationUid()));
						productionHoleTime = getTimeBetweenDuration(operationUomContext, csgShoeDateTime, tdDateTdTime);
						if (productionHoleTime!=null) {
							productionHoleTime = Math.round(productionHoleTime * 100) / 100.0;
							EndOfMonthListDetail.put("productionHoleTime", formatter.format(productionHoleTime));
						}
					}				
				}
				Double afeSpudToTd = getAfeSpudToTd(operationUomContext,operation.getOperationUid());
				if (afeSpudToTd==null) afeSpudToTd = 0.0;
				afeSpudToTd = Math.round(convertTo12Or24Day(afeSpudToTd, operationUomContext) * 100) / 100.0;
				EndOfMonthListDetail.put("afeSpudToTd", formatter.format(afeSpudToTd));
				
				
				if (EndOfMonthDetailobj[25] != null) {		
					Double totalEvaluationPhase = convertTo12Or24Day( Double.parseDouble(EndOfMonthDetailobj[25].toString()), operationUomContext);
					totalEvaluationPhase = Math.round(totalEvaluationPhase * 100) / 100.0;
					EndOfMonthListDetail.put("totalEvaluationPhase", formatter.format(totalEvaluationPhase));
				}
				
				Double csgShoeDepth = getCsgShoeDepth(operationUomContext,operation.getOperationUid());// * feet;
				if (csgShoeDepth!=null) {
					csgShoeDepth = csgShoeDepth * feet;
					csgShoeDepth = Math.round(csgShoeDepth * 100) / 100.0;
					EndOfMonthListDetail.put("csgShoeDepth", formatter.format(csgShoeDepth));
				}else csgShoeDepth = 0.0;

				if (!"".equals(getCsgShoeDateTime(operationUomContext,operation.getOperationUid()))){
					Date csgShoeDateTime = df_2.parse(getCsgShoeDateTime(operationUomContext,operation.getOperationUid())); 
					EndOfMonthListDetail.put("csgShoeDateTime", df_1.format(csgShoeDateTime));
				}		
				
				Double nptOperational = getProductionTime(operationUomContext, operation.getOperationUid(), "TP");
				if (nptOperational!=null) {
					nptOperational = Math.round(nptOperational * 100) / 100.0;
					EndOfMonthListDetail.put("nptOperational", formatter.format(nptOperational));
				}
				
				Double nptNonOperational = getProductionTime(operationUomContext, operation.getOperationUid(), "TU");
				if (nptNonOperational!=null) {
					nptNonOperational = Math.round(nptNonOperational * 100) / 100.0;
					EndOfMonthListDetail.put("nptNonOperational", formatter.format(nptNonOperational));
				}
				
				Double totalDuration = getTotalActivityDuration(operationUomContext, operation.getOperationUid());
				if (totalDuration==null) totalDuration = 0.0;
				Double nptOfTotalTime = 0.00;
				if (totalDuration> 0.0){
					if (nptOperational==null) nptOperational = 0.0;
					if (nptNonOperational==null) nptNonOperational = 0.0;
					nptOfTotalTime = (nptOperational + nptNonOperational) * 100/ totalDuration;
					nptOfTotalTime = Math.round(nptOfTotalTime * 100) / 100.0;
					EndOfMonthListDetail.put("nptOfTotalTime", formatter.format(nptOfTotalTime));
				}
				
				Double totalNonProgramTime = getProductionTime(operationUomContext, operation.getOperationUid(), "P");
				if (totalNonProgramTime!=null && totalDuration>0.0) {
					totalNonProgramTime = 100.00 - ((totalNonProgramTime/totalDuration) * 100);
					totalNonProgramTime = Math.round(totalNonProgramTime * 100) / 100.0;
					EndOfMonthListDetail.put("totalNonProgramTime", formatter.format(totalNonProgramTime));
				}
				
				Double manhours = Double.parseDouble(getPobManHour(operationUomContext, operation.getOperationUid()));
				manhours = Math.round(manhours * 100) / 100.0;
				EndOfMonthListDetail.put("manhours", formatter.format(manhours));
				
				Double waitWeatherCost = getOperationCost(operationUomContext, operation.getOperationUid(), "WOW");
				if (waitWeatherCost!=null) {
					EndOfMonthListDetail.put("waitWeatherCost", formatter.format(waitWeatherCost) );
				}
				
				Double rigMoveDistance = (Double) EndOfMonthDetailobj[9];
				if (rigMoveDistance!=null){
					rigMoveDistance = rigMoveDistance/1000.0;			
					EndOfMonthListDetail.put("rigMoveDistance", formatter.format(rigMoveDistance));
				}
				
				Double prespudTime = 0.00;
				prespudTime = getActivityDurationByPhase(operationUomContext,operation.getOperationUid(),"PS");
				if (prespudTime!=null) {
					prespudTime = Math.round(prespudTime * 100) / 100.0;
					EndOfMonthListDetail.put("prespudTime", formatter.format(prespudTime));
				}
				
				Double holeDuration = getActivityDurationByPhase(operationUomContext,operation.getOperationUid(),"SH");
				if (holeDuration!=null) {
					holeDuration = Math.round(holeDuration * 100) / 100.0;
					EndOfMonthListDetail.put("holeDuration", formatter.format(holeDuration));
				}
				
				Double surfaceHoleRop = 0.00;
				if (holeDuration!=null && holeDuration > 0){
					surfaceHoleRop = csgShoeDepth / holeDuration;		
					EndOfMonthListDetail.put("surfaceHoleRop", formatter.format(surfaceHoleRop));
				}	
				
				Double productionHoleRop = 0.00;
				if (productionHoleTime > 0){
					productionHoleRop = (maxdepth - csgShoeDepth) / productionHoleTime;			
					EndOfMonthListDetail.put("productionHoleRop", formatter.format(productionHoleRop));
				}
				
				Double surfaceCasingTime = getActivityDurationByPhase(operationUomContext,operation.getOperationUid(),"SC");
				if (surfaceCasingTime!=null) {
					surfaceCasingTime = Math.round(surfaceCasingTime * 100) / 100.0;
					EndOfMonthListDetail.put("surfaceCasingTime", formatter.format(surfaceCasingTime));
				}
				
				Double preSpudCost = getPhaseTotalByCode(operationUomContext,operation.getOperationUid(),"PS");
				if (preSpudCost!=null) {
					preSpudCost = Math.round(preSpudCost * 100) / 100.0;
					EndOfMonthListDetail.put("preSpudCost", formatter.format(preSpudCost));
					Double preSpudCostPerDepth = 0.00;
					if (preSpudCost >= 0 && maxdepth !=null && maxdepth !=0){
						preSpudCostPerDepth = preSpudCost / maxdepth;
						preSpudCostPerDepth = Math.round(preSpudCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("preSpudCostPerDepth", formatter.format(preSpudCostPerDepth));
					}
				}
				
				Double evaluationPhaseCost = getPhaseTotalByCode(operationUomContext,operation.getOperationUid(),"EP");
				if (evaluationPhaseCost!=null) {
					evaluationPhaseCost = Math.round(evaluationPhaseCost * 100) / 100.0;
					EndOfMonthListDetail.put("evaluationPhaseCost",formatter.format(evaluationPhaseCost));
					Double evaluationPhaseCostPerDepth = 0.00;
					if (evaluationPhaseCost >= 0 && maxdepth !=null && maxdepth !=0){
						evaluationPhaseCostPerDepth = evaluationPhaseCost / maxdepth;
						evaluationPhaseCostPerDepth = Math.round(evaluationPhaseCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("evaluationPhaseCostPerDepth", formatter.format(evaluationPhaseCostPerDepth));
					}
				}
				
				Double surfaceHoleCost = getPhaseTotalByCode(operationUomContext,operation.getOperationUid(),"SH");
				if (surfaceHoleCost!=null) {
					surfaceHoleCost = Math.round(surfaceHoleCost * 100) / 100.0;
					EndOfMonthListDetail.put("surfaceHoleCost", formatter.format(surfaceHoleCost));
					Double surfaceHoleCostPerDepth = 0.00;
					if (surfaceHoleCost >= 0 && maxdepth !=null && maxdepth !=0){
						surfaceHoleCostPerDepth = surfaceHoleCost / maxdepth;
						surfaceHoleCostPerDepth = Math.round(surfaceHoleCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("surfaceHoleCostPerDepth", formatter.format(surfaceHoleCostPerDepth));
					}
				}
				
				Double surfaceCasingCost = getPhaseTotalByCode(operationUomContext,operation.getOperationUid(),"SC");
				if (surfaceCasingCost!=null) {
					surfaceCasingCost = Math.round(surfaceCasingCost * 100) / 100.0;
					EndOfMonthListDetail.put("surfaceCasingCost", formatter.format(surfaceCasingCost));
					Double surfaceCasingCostPerDepth = 0.00;
					if (surfaceCasingCost >= 0 && maxdepth !=null && maxdepth !=0){
						surfaceCasingCostPerDepth = surfaceCasingCost / maxdepth;
						surfaceCasingCostPerDepth = Math.round(surfaceCasingCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("surfaceCasingCostPerDepth", formatter.format(surfaceCasingCostPerDepth));
					}
				}
				
				Double productionHoleCost = getProductionHoleCost(operationUomContext,operation.getOperationUid());
				if (productionHoleCost!=null) {
					productionHoleCost = Math.round(productionHoleCost * 100) / 100.0;
					EndOfMonthListDetail.put("productionHoleCost", formatter.format(productionHoleCost));
					Double productionHoleCostPerDepth = 0.00;
					if (productionHoleCost >= 0 && maxdepth !=null && maxdepth !=0){
						productionHoleCostPerDepth = productionHoleCost / maxdepth;
						productionHoleCostPerDepth = Math.round(productionHoleCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("productionHoleCostPerDepth", formatter.format(productionHoleCostPerDepth));
					}
				}
				
				Double completionCost = getCompletionCost(operationUomContext,operation.getOperationUid());
				if (completionCost!=null) {
					completionCost = Math.round(completionCost * 100) / 100.0;
					EndOfMonthListDetail.put("completionCost", formatter.format(completionCost));
					Double completionCostPerDepth = 0.00;
					if (completionCost >= 0 && maxdepth !=null && maxdepth !=0){
						completionCostPerDepth = completionCost / maxdepth;
						completionCostPerDepth = Math.round(completionCostPerDepth * 100) / 100.0;
						EndOfMonthListDetail.put("completionCostPerDepth", formatter.format(completionCostPerDepth));
					}
				}
				
				Double shMudCost = getShMudCost(operationUomContext,operation.getOperationUid());
				if (shMudCost!=null) {
					shMudCost = Math.round(shMudCost * 100) / 100.0;
					EndOfMonthListDetail.put("shMudCost", formatter.format(shMudCost));
				}
				
				Double phMudCost = getPhMudCost(operationUomContext,operation.getOperationUid());
				if (phMudCost!=null) {
					phMudCost = Math.round(phMudCost * 100) / 100.0; 
					EndOfMonthListDetail.put("phMudCost", formatter.format(phMudCost));
				}
				
				if (EndOfMonthDetailobj[5]!=null){
					String purposeType ="";
					purposeType = getPurposeType(nullToEmptyString(EndOfMonthDetailobj[5]));
					EndOfMonthListDetail.put("purposeType", purposeType);	
				}
				
				if (EndOfMonthDetailobj[20]!=null){	
					String scSize ="";
					try {
						String lookup="xml://casingsection.casing_size?key=code&value=label"; 												
						Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
						
						if (list.containsKey(nullToEmptyString(formatter_6.format(Double.parseDouble(EndOfMonthDetailobj[20].toString()))))) {
							LookupItem selectedLookup = list.get(nullToEmptyString(formatter_6.format(Double.parseDouble(EndOfMonthDetailobj[20].toString()))));	
							scSize = selectedLookup.getValue().toString();			
						} 
					} catch (Exception e) {
						e.getMessage();
					}
					
					EndOfMonthListDetail.put("scSize", scSize);	
				}
				
				if (EndOfMonthDetailobj[21]!=null){
					String pcSize ="";
					try {
						String lookup="xml://casingsection.casing_size?key=code&value=label"; 												
						Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
						
						if (list.containsKey(nullToEmptyString(formatter_6.format(Double.parseDouble(EndOfMonthDetailobj[21].toString()))))) {
							LookupItem selectedLookup = list.get(nullToEmptyString(formatter_6.format(Double.parseDouble(EndOfMonthDetailobj[21].toString()))));	
							pcSize = selectedLookup.getValue().toString();			
						} 
					} catch (Exception e) {
						e.getMessage();
					}
					
					EndOfMonthListDetail.put("pcSize", pcSize);	
				}
				
				if (EndOfMonthDetailobj[23]!=null){
					CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LeakOffTest.class, "estimatedMudWeight",operationUomContext);
					thisConverter.setBaseValue(Double.parseDouble(EndOfMonthDetailobj[23].toString()));
					Double lotEmw = thisConverter.getConvertedValue();
					lotEmw = Math.round(lotEmw * 100) / 100.0;
					EndOfMonthListDetail.put("lotEmw", formatter.format(lotEmw));	
				}
				
				EndOfMonthListDetail.put("LtiMti", gotLTI(operationUomContext,operation.getOperationUid()));	
				Double rigFuel = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Rig Fuel");
				if (rigFuel!=null) EndOfMonthListDetail.put("rigFuel", formatter_0.format(rigFuel));	
				
				Double campFuel = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Rig Camp Fuel");
				if (campFuel!=null) EndOfMonthListDetail.put("campFuel", formatter_0.format(campFuel));	
				
				Double drillWater = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Drill Water");
				if (drillWater!=null) EndOfMonthListDetail.put("drillWater", formatter_0.format(drillWater));	
				
				Double cementingWater = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Cementing Water");
				if (cementingWater!=null) EndOfMonthListDetail.put("cementingWater", formatter_0.format(cementingWater));	
				
				Double campPotableWater = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Camp Potable Water");
				if (campPotableWater!=null) EndOfMonthListDetail.put("campPotableWater", formatter_0.format(campPotableWater));	
				
				Double rigsitePotableWater = getBulkStockUsedItem(operationUomContext,operation.getOperationUid(),"Rigsite Potable Water");
				if (rigsitePotableWater!=null) EndOfMonthListDetail.put("rigsitePotableWater", formatter_0.format(rigsitePotableWater));	
				
				Double CMIC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"CMIC");
				if (CMIC!=null) EndOfMonthListDetail.put("CMIC", formatter.format(CMIC));
				
				Double CMPC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"CMPC");
				if (CMPC!=null) EndOfMonthListDetail.put("CMPC", formatter.format(CMPC));
				
				Double CMSC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"CMSC");
				if (CMSC!=null) EndOfMonthListDetail.put("CMSC", formatter.format(CMSC));
				
				Double EVAL = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"EVAL");
				if (EVAL!=null) EndOfMonthListDetail.put("EVAL", formatter.format(EVAL));
				
				Double IBH = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"IBH");
				if (IBH!=null) EndOfMonthListDetail.put("IBH", formatter.format(IBH));
				
				Double ITDWT = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"ITDWT");
				if (ITDWT!=null) EndOfMonthListDetail.put("ITDWT", formatter.format(ITDWT));
				
				Double LOT = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"LOT");
				if (LOT!=null) EndOfMonthListDetail.put("LOT", formatter.format(LOT));
				
				Double NDB = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"NDB");
				if (NDB!=null) EndOfMonthListDetail.put("NDB", formatter.format(NDB));
				
				Double NUB = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"NUB");
				if (NUB!=null) EndOfMonthListDetail.put("NUB", formatter.format(NUB));
				
				Double PACMP = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"PACMP");
				if (PACMP!=null) EndOfMonthListDetail.put("PACMP", formatter.format(PACMP));
				
				Double PAOTH = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"PAOTH");
				if (PAOTH!=null) EndOfMonthListDetail.put("PAOTH", formatter.format(PAOTH));
				
				Double PTDWT = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"PTDWT");
				if (PTDWT!=null) EndOfMonthListDetail.put("PTDWT", formatter.format(PTDWT));
				
				Double RRIC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"RRIC");
				if (RRIC!=null) EndOfMonthListDetail.put("RRIC", formatter.format(RRIC));
				
				Double RRPC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"RRPC");
				if (RRPC!=null) EndOfMonthListDetail.put("RRPC", formatter.format(RRPC));
				
				Double RRSC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"RRSC");
				if (RRSC!=null) EndOfMonthListDetail.put("RRSC", formatter.format(RRSC));
				
				Double RC = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"RC");
				if (RC!=null) EndOfMonthListDetail.put("RC", formatter.format(RC));
				
				Double SCWT = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"SCWT");
				if (SCWT!=null) EndOfMonthListDetail.put("SCWT", formatter.format(SCWT));
				
				Double TOLDP = getDurationBasedOnUserCode(operationUomContext,operation.getOperationUid(),"TOLDP");
				if (TOLDP!=null) EndOfMonthListDetail.put("TOLDP", formatter.format(TOLDP));
				
				Double avgConnectionTime = getAverageConnectionTime(operationUomContext, operationUid, feet);
				if (avgConnectionTime!=null) EndOfMonthListDetail.put("avgConnectionTime", formatter.format(avgConnectionTime));
						
				OperationList.put(key, EndOfMonthListDetail);	
			}					
		}	
		
		//Data Extraction - End
		SimpleAttributes attr_time_related = new SimpleAttributes();
		attr_time_related.addAttribute("style", "background-color:#4f81bd;font-weight:bold;");
		attr_time_related.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_data = new SimpleAttributes();
		attr_data.addAttribute("style", "background-color:#ffff00;font-weight:bold;");
		attr_data.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_$ = new SimpleAttributes();
		attr_$.addAttribute("style", "background-color:#c6eece;font-weight:bold;");
		attr_$.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_$_ft = new SimpleAttributes();
		attr_$_ft.addAttribute("style", "background-color:#9bbb59;font-weight:bold;");
		attr_$_ft.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_ROP_ft_day_etc = new SimpleAttributes();
		attr_ROP_ft_day_etc.addAttribute("style", "background-color:#c0504d;font-weight:bold;");
		attr_ROP_ft_day_etc.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_safety = new SimpleAttributes();
		attr_safety.addAttribute("style", "background-color:#c00000;font-weight:bold;");
		attr_safety.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_santos_input = new SimpleAttributes();
		attr_santos_input.addAttribute("style", "background-color:#00b0f0;font-weight:bold;");
		attr_santos_input.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_calculated_data = new SimpleAttributes();
		attr_calculated_data.addAttribute("style", "background-color:#a5a5a5;");
		attr_calculated_data.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_water_fuel = new SimpleAttributes();
		attr_water_fuel.addAttribute("style", "background-color:#948b54;font-weight:bold;");
		attr_water_fuel.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_default = new SimpleAttributes();
		attr_default.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes attr_default_legend = new SimpleAttributes();
		attr_default_legend.addAttribute("nowrap", "nowrap");
		attr_default_legend.addAttribute("colspan", "3");
		
		SimpleAttributes attr_default_legend_header = new SimpleAttributes();
		attr_default_legend_header.addAttribute("nowrap", "nowrap");
		attr_default_legend_header.addAttribute("colspan", "4");
		attr_default_legend_header.addAttribute("style", "font-weight:bold;");
		
		SimpleAttributes attr_default_bit = new SimpleAttributes();
		attr_default_bit.addAttribute("nowrap", "nowrap");
		attr_default_bit.addAttribute("style", "font-weight:bold;");
			
		//Draw HTML table
		tableAttr.addAttribute("style","borderStyle: solid;borderColor:black;");
		tableAttr.addAttribute("border","1");
		tableAttr.addAttribute("width","100%");
		tableAttr.addAttribute("nowrap", "nowrap");
		writer.startElement("table", tableAttr);
		
		writer.startElement("tr");
		writer.addElement("td", "Spud Date Spud Time", attr_time_related);
		writer.addElement("td", "Year", attr_time_related);
		writer.addElement("td", "State", attr_data);
		writer.addElement("td", "Rig", attr_data);
		writer.addElement("td", "Project #", attr_data);
		writer.addElement("td", "Well Name", attr_data);
		writer.addElement("td", "Actual Well Cost ($)", attr_$);
		writer.addElement("td", "Daily Cost Tracking Final Well Cost ($)", attr_$);
		writer.addElement("td", "AFE Cost ($)", attr_$);
		writer.addElement("td", "Total Well Cost ($/ft)", attr_$_ft);
		writer.addElement("td", "Rig Move to Rig Move (days)", attr_time_related);
		writer.addElement("td", "Spud to TD $/ft (IDS FFC)",  attr_$_ft);
		writer.addElement("td", "ROP Spud to TD (ft/day)", attr_ROP_ft_day_etc);
		writer.addElement("td", "Spud to TD (days)", attr_time_related);
		writer.addElement("td", "AFE: Spud to TD (days)", attr_time_related);
		writer.addElement("td", "Spud to TD (days/10,000ft)", attr_ROP_ft_day_etc);
		writer.addElement("td", "Casing Shoe Depth", attr_time_related);
		writer.addElement("td", "Casing Shoe Date/Time", attr_time_related);
		writer.addElement("td", "Footage Drilled OR TMD", attr_time_related);
		writer.addElement("td", "TD Date/Time", attr_time_related);
		writer.addElement("td", "Non Productive Time : Operational (hrs)", attr_time_related);
		writer.addElement("td", "Non Productive Time : Non operational (hrs)", attr_time_related);
		writer.addElement("td", "NPT % of Total Time", attr_time_related);
		writer.addElement("td", "Total Non Programmed Time as % of Total Time", attr_time_related);
		writer.addElement("td", "Weather Related Incurred Costs (WOW) ($)", attr_$);
		writer.addElement("td", "Average Connection Time in Minutes", attr_time_related);
		writer.addElement("td", "Rig Move Distance (km)", attr_time_related);
		writer.addElement("td", "Rig Move Time (hrs)", attr_time_related);
		writer.addElement("td", "Rig Up Time (hrs)", attr_time_related);
		writer.addElement("td", "Prespud Time (days)", attr_time_related);
		writer.addElement("td", "Surface Hole (days)", attr_time_related);
		writer.addElement("td", "Surface + Int.Casing Time (days)", attr_time_related);
		writer.addElement("td", "Production Hole Time (no eval time) (days)", attr_time_related);
		writer.addElement("td", "Evaluation Phase Before TD (days)", attr_time_related);
		writer.addElement("td", "Evaluation Phase After TD (days)", attr_time_related);
		writer.addElement("td", "TD Date/Time", attr_time_related);
		writer.addElement("td", "Total Evaluation Phase (days)", attr_time_related);
		writer.addElement("td", "Production Casing Time (days)", attr_time_related);
		writer.addElement("td", "Spud to Rig Release (days)", attr_time_related);
		writer.addElement("td", "Spud to Rig Release (no eval time) (days)", attr_time_related);
		writer.addElement("td", "Release Date", attr_time_related);
		writer.addElement("td", "Prespud Costs ($)", attr_$);
		writer.addElement("td", "Surface Hole Costs ($)", attr_$);
		writer.addElement("td", "Surface Casing Costs ($)", attr_$);
		writer.addElement("td", "Production Hole Costs ($)", attr_$);
		writer.addElement("td", "Evaluation Phase Costs ($)", attr_$);
		writer.addElement("td", "Production C&S, C&Comp. or P&A Cost ($)", attr_$);
		writer.addElement("td", "Surface Hole Mud- Total Cost ($)", attr_$);
		writer.addElement("td", "Production Hole Mud - Total Cost ($)", attr_$);
		writer.addElement("td", "Prespud Cost ($/ft)", attr_$_ft);
		writer.addElement("td", "Surface Hole Costs ($/ft)", attr_$_ft);
		writer.addElement("td", "Surface Casing Costs ($/ft)", attr_$_ft);
		writer.addElement("td", "Production Hole Costs ($/ft)", attr_$_ft);
		writer.addElement("td", "Evaluation Phase Costs ($/ft)", attr_$_ft);
		writer.addElement("td", "Production C&S, C&Comp. or P&A Cost ($/ft)", attr_$_ft);
		writer.addElement("td", "Well Type", attr_data);
		writer.addElement("td", "Surface Casing Size", attr_data);
		writer.addElement("td", "Production Casing Size", attr_data);
		writer.addElement("td", "Leak Off Test/EMW", attr_data);
		writer.addElement("td", "MTI/LTI", attr_safety);
		writer.addElement("td", "Manhours", attr_safety);
		writer.addElement("td", "Surface Hole ROP (ft/day) Total Time", attr_ROP_ft_day_etc);
		writer.addElement("td", "Production Hole ROP (ft/day) Total Time", attr_ROP_ft_day_etc);
		writer.addElement("td", "Rig Fuel (ltr)", attr_water_fuel);
		writer.addElement("td", "Camp Fuel (ltr)", attr_water_fuel);
		writer.addElement("td", "Drill Water", attr_water_fuel);
		writer.addElement("td", "Cementing Water", attr_water_fuel);
		writer.addElement("td", "Camp Potable Water (ltr)", attr_water_fuel);
		writer.addElement("td", "Rigsite Potable Water (ltr)", attr_water_fuel);
		writer.addElement("td", "Cooper Basin Road Directory Map Index Reference ie: 75.3G", attr_santos_input);
		writer.addElement("td", "Map Index Reference Map #", attr_santos_input);
		writer.addElement("td", "Map Index Reference ie: 8G", attr_santos_input);
		writer.addElement("td", "\"Like for Like\" filter", attr_santos_input);
		writer.addElement("td", "CEMENT INTERMEDIATE CASING", attr_time_related);
		writer.addElement("td", "CEMENT PRODUCTION CASING", attr_time_related);
		writer.addElement("td", "CEMENT SURFACE CASING", attr_time_related);
		writer.addElement("td", "EVALUATION", attr_time_related);
		writer.addElement("td", "INTERMEDIATE HOLE BOP HANDLING", attr_time_related);
		writer.addElement("td", "INTERMEDIATE HOLE TD WIPER TRIP", attr_time_related);
		writer.addElement("td", "LEAK OFF TEST", attr_time_related);
		writer.addElement("td", "NIPPLE DOWN BOPs", attr_time_related);
		writer.addElement("td", "NIPPLE UP BOPs", attr_time_related);
		writer.addElement("td", "PLUG & ABANDON CEMENT PLUGS", attr_time_related);
		writer.addElement("td", "PLUG & ABANDON OTHER", attr_time_related);
		writer.addElement("td", "PRODUCTION HOLE TD WIPER TRIP", attr_time_related);
		writer.addElement("td", "RIG UP & RUN INTERMEDIATE CASING", attr_time_related);
		writer.addElement("td", "RIG UP & RUN PRODUCTION CASING", attr_time_related);
		writer.addElement("td", "RIG UP & RUN SURFACED CASING", attr_time_related);
		writer.addElement("td", "SURFACE CASING WIPER TRIP", attr_time_related);
		writer.addElement("td", "TRIP OUT LAYING DOWN PIPE", attr_time_related);
		writer.endElement();	
		
		Map<String, Object> opList = OperationList;
		Map<String, Object> sortedMap = new TreeMap(opList);
		
		//Table Result
		//Retrieve all Data Containers 1 into Array
		List<Object[]> mapList = new ArrayList();
		for (Map.Entry<String, Object>item : sortedMap.entrySet()) {
			mapList.add(new Object[] {item.getKey(), item.getValue()});
		}

		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			//Draw Table Header Row 1
			writer.startElement("tr");
			Object[] obj_array = (Object[]) i.next();
			Map<String, Object> EndOfMonthDetail = (Map<String, Object>)obj_array[1];
			
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudDate")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("year")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("state")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigInformationUid")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("afebasis")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("wellName")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("actualWellCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("totalWellCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("afe")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("totalWellCostPerFeet")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigMoveTORigMove")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudToTdCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("ropSpudToTd")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudToTd")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("afeSpudToTd")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudToTdPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("csgShoeDepth")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("csgShoeDateTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("tmd")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("tdDateTdTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("nptOperational")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("nptNonOperational")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("nptOfTotalTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("totalNonProgramTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("waitWeatherCost")), attr_default);		
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("avgConnectionTime")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigMoveDistance")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigMoveTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigUpTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("prespudTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("holeDuration")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceCasingTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("productionHoleTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("evaluationPhaseBeforeTd")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("evaluationPhaseAfterTd")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("tdDateTdTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("totalEvaluationPhase")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("productionCasingTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudToRigUpDuration")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("spudToRigRelease")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigreleaseDateRigreleaseTime")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("preSpudCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceHoleCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceCasingCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("productionHoleCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("evaluationPhaseCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("completionCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("shMudCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("phMudCost")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("preSpudCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceHoleCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceCasingCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("productionHoleCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("evaluationPhaseCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("completionCostPerDepth")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("purposeType")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("scSize")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("pcSize")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("lotEmw")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("LtiMti")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("manhours")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("surfaceHoleRop")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("productionHoleRop")), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigFuel")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("campFuel")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("drillWater")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("cementingWater")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("campPotableWater")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("rigsitePotableWater")),attr_default);
			writer.addElement("td", nullToEmptyString(""), attr_default);
			writer.addElement("td", nullToEmptyString(""), attr_default);
			writer.addElement("td", nullToEmptyString(""), attr_default);
			writer.addElement("td", nullToEmptyString(""), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("CMIC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("CMPC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("CMSC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("EVAL")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("IBH")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("ITDWT")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("LOT")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("NDB")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("NUB")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("PACMP")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("PAOTH")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("PTDWT")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("RRIC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("RRPC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("RRSC")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("SCWT")), attr_default);
			writer.addElement("td", nullToEmptyString(EndOfMonthDetail.get("TOLDP")), attr_default);
			writer.endElement();
		}
		//start legend
		SimpleAttributes attr_default_x = new SimpleAttributes();
		attr_default_x.addAttribute("nowrap", "nowrap");
		attr_default_x.addAttribute("colspan", "4");
		attr_default_x.addAttribute("style","borderStyle: solid;borderColor:white;");
		attr_default_x.addAttribute("border","0");
		writer.startElement("tr");
		writer.addElement("td","", attr_default_x);
		writer.endElement();	
		
		writer.startElement("tr");
		writer.addElement("td","Legend", attr_default_legend_header);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_time_related);
		writer.addElement("td","Time Related cells", attr_default_legend);
		writer.endElement();
	
		writer.startElement("tr");
		writer.addElement("td","", attr_data);
		writer.addElement("td","Data", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_$);
		writer.addElement("td","$", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_$_ft);
		writer.addElement("td","$/ft", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_ROP_ft_day_etc);
		writer.addElement("td","ROP ft/day etc", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_safety);
		writer.addElement("td","Safety", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_santos_input);
		writer.addElement("td","Santos Input only", attr_default_legend);
		writer.endElement();
		
		writer.startElement("tr");
		writer.addElement("td","", attr_calculated_data);
		writer.addElement("td","Calculated Data", attr_default_legend);
		writer.endElement();
		
		
		writer.startElement("tr");
		writer.addElement("td","", attr_water_fuel);
		writer.addElement("td","Water/Fuel", attr_default_legend);
		writer.endElement();
		//end legend
		
		for (String operationUid : operations) {
			writer.startElement("tr");
			writer.addElement("td","", attr_default);
			writer.endElement();
			
			writer.startElement("tr");
			writer.addElement("td","Average Connection Times Calculation", attr_default_legend_header);
			writer.endElement();
			
			writer.startElement("tr");
			writer.addElement("td","From IDS Bit Query", attr_default_legend_header);
			writer.endElement();
			
			writer.startElement("tr");
			writer.addElement("td","Bit #", attr_default_bit);
			writer.addElement("td","Type", attr_default_bit);
			writer.addElement("td","Well-name", attr_default_bit);
			writer.addElement("td","Spud Date", attr_default_bit);
			writer.addElement("td","Rig", attr_default_bit);
			writer.addElement("td","Bit Size", attr_default_bit);
			writer.addElement("td","Make", attr_default_bit);
			writer.addElement("td","Model", attr_default_bit);
			writer.addElement("td","IADC #", attr_default_bit);
			writer.addElement("td","Depth In", attr_default_bit);
			writer.addElement("td","Depth Out", attr_default_bit);
			writer.addElement("td","Ftg.", attr_default_bit); 
			writer.addElement("td","Reason Pulled", attr_default_bit);
			writer.addElement("td","Average ROP", attr_default_bit);
			writer.addElement("td","On-Btm-Hrs", attr_default_bit);
			writer.addElement("td","Total IADC", attr_default_bit);
			writer.addElement("td","Connection Time, Hrs", attr_default_bit);
			writer.addElement("td","Number of connections", attr_default_bit);
			writer.addElement("td","Connection Time in Minutes", attr_default_bit);
			writer.addElement("td","Comment", attr_default_bit);
			writer.addElement("td","Serial #", attr_default_bit);
			writer.addElement("td","Date In", attr_default_bit);
			writer.addElement("td","Date Out", attr_default_bit);
			writer.addElement("td","I", attr_default_bit);
			writer.addElement("td","O1", attr_default_bit);
			writer.addElement("td","D", attr_default_bit);
			writer.addElement("td","L", attr_default_bit);
			writer.addElement("td","B", attr_default_bit);
			writer.addElement("td","G", attr_default_bit);
			writer.addElement("td","O2", attr_default_bit);
			writer.endElement();
			
			String bitQuery = "SELECT br.operationUid, br.bitrunUid, br.bitrunNumber, br.bitType, br.bitDiameter, br.make, br.model, br.iadcCode, br.depthInMdMsl, br.depthOutMdMsl, " +
					"br.condFinalReason, br.comment, br.serialNumber, br.dailyUidIn, br.dailyUidOut, br.condFinalInner, br.condFinalOuter, br.condFinalDull, br.condFinalLocation, br.condFinalBearing, " +
					"br.condFinalGauge, br.condFinalOther " +
					"FROM Bitrun br, Bharun bha, BharunDailySummary bs " +
					"WHERE (bs.isDeleted=false or bs.isDeleted is null) " +
					"AND (br.isDeleted=false or br.isDeleted is null) " +
					"AND (bha.isDeleted=false or bha.isDeleted is null) " +
					"AND br.bharunUid=bha.bharunUid " +
					"AND bha.bharunUid=bs.bharunUid " +
					"AND br.operationUid=:operationUid GROUP BY br.bitrunUid";
			List <Object[]> BitrunDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitQuery, new String[]{"operationUid"}, new Object[]{nullToEmptyString(operationUid)},qp);
			
			Double cumConnectionTime = 0.0;
			Double cumNumberConnection = 0.0;
			
			for (Object[] BitrunDetailobj:BitrunDetail){
				if (BitrunDetailobj[0]!=null){			
					Double progress = 0.0;		
					progress = getProgress(operationUomContext,nullToEmptyString(BitrunDetailobj[1])); // * feet;
					if (progress!=null) {
						progress = progress * feet;
					
						if (progress >= 500){
							writer.startElement("tr");
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[2]), attr_default);
							
							String bitType ="";
							try {
								String lookup="xml://bitrun.bit_type?key=code&value=label"; 												
								Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
								
								if (list.containsKey(nullToEmptyString(BitrunDetailobj[3]))) {
									LookupItem selectedLookup = list.get(nullToEmptyString(BitrunDetailobj[3]));	
									bitType = selectedLookup.getValue().toString();			
								} 
							} catch (Exception e) {
								e.getMessage();
							}
							
							writer.addElement("td",nullToEmptyString(bitType), attr_default);
							
							String query = "SELECT o.operationUid, w.wellName, o.rigInformationUid, o.spudDate " +
							"FROM Well w, Wellbore wb, Operation o " +
							"WHERE (o.isDeleted=false or o.isDeleted is null) " +
							"AND (wb.isDeleted=false or wb.isDeleted is null) " +
							"AND (w.isDeleted=false or w.isDeleted is null) " +
							"AND w.wellUid=wb.wellUid " +
							"AND wb.wellboreUid=o.wellboreUid " +
							"AND o.operationUid=:operationUid " +
							"ORDER BY o.spudDate";
						
							List <Object[]> wellData  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"operationUid"}, new Object[]{nullToEmptyString(operationUid)},qp);
							String date = "";
							String wellname = "";
							String rigName = "";
							
							if (wellData.size()>0){
								for (Object[] wellDataDetail:wellData){											
									if (wellDataDetail[1]!=null){			
										wellname = nullToEmptyString(wellDataDetail[1].toString());
									}
									if (wellDataDetail[3]!=null){
										Date spudDate = (Date) wellDataDetail[3]; 		
										date = df_1.format(spudDate);
									}
									if (wellDataDetail[2]!=null){										
										rigName = getRigName(nullToEmptyString(wellDataDetail[2].toString()));
									}		
								}		
							}				
						
							
							writer.addElement("td",nullToEmptyString(wellname), attr_default);
							writer.addElement("td",nullToEmptyString(date), attr_default);
							writer.addElement("td",nullToEmptyString(rigName), attr_default);	
						
							String bitSize ="";
							try {
								String lookup="xml://bitsize?key=code&value=label"; 												
								Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
								
								if (list.containsKey(nullToEmptyString(formatter_6.format(Double.parseDouble(BitrunDetailobj[4].toString()))))) {
									LookupItem selectedLookup = list.get(nullToEmptyString(formatter_6.format(Double.parseDouble(BitrunDetailobj[4].toString()))));	
									bitSize = selectedLookup.getValue().toString();			
								}	
							} catch (Exception e) {
								e.getMessage();
							}
										
							writer.addElement("td",nullToEmptyString(bitSize), attr_default);
							
							String cname ="";
							cname = getCompanyname(nullToEmptyString(BitrunDetailobj[5]));
							writer.addElement("td",nullToEmptyString(cname), attr_default);					
							
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[6]), attr_default);//model
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[7]), attr_default);//iadc
							
							Double depthIn = 0.0;
							if (BitrunDetailobj[8] != null){
								depthIn = Double.parseDouble(BitrunDetailobj[8].toString()) * feet;
								depthIn = Math.round(depthIn * 100) / 100.0;
							}
							writer.addElement("td",nullToEmptyString(formatter.format(depthIn)), attr_default);//depth in
							
							Double depthOut = 0.0;
							if (BitrunDetailobj[9] != null){
								depthOut = Double.parseDouble(BitrunDetailobj[9].toString()) * feet;
								depthOut = Math.round(depthOut * 100) / 100.0;
							}
							writer.addElement("td",nullToEmptyString(formatter.format(depthOut)), attr_default);//depth out
										
							progress = Math.round(progress * 100) / 100.0;
							writer.addElement("td",nullToEmptyString(formatter.format(progress)));//ftg
							
							String R ="";
							try {
								String lookup="xml://bitrun.cond_final_reason?key=code&value=label"; 												
								Map<String,LookupItem> list = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
								
								if (list.containsKey(nullToEmptyString(BitrunDetailobj[10]))) {
									LookupItem selectedLookup = list.get(nullToEmptyString(BitrunDetailobj[10]));	
									R = selectedLookup.getValue().toString();			
								}
							} catch (Exception e) {
								e.getMessage();
							}
							
							writer.addElement("td",nullToEmptyString(R), attr_default);
							
							Double timeOnBtm = getTimeOnBtm(operationUomContext,nullToEmptyString(BitrunDetailobj[1]));
							if (timeOnBtm!=null && timeOnBtm>0.0) {
								Double rop = 0.0;		
								rop = progress / timeOnBtm;
								rop = Math.round(rop * 100) / 100.0;
								writer.addElement("td",nullToEmptyString(formatter.format(rop)), attr_default);//average rop			
								timeOnBtm = Math.round(timeOnBtm * 100) / 100.0;
								writer.addElement("td",nullToEmptyString(formatter.format(timeOnBtm)), attr_default);//time on bottom
							}							
							
							Double IADCDuration = getIADCDuration(operationUomContext,nullToEmptyString(BitrunDetailobj[1]));
							if (IADCDuration!=null) {
								IADCDuration = Math.round(IADCDuration * 100) / 100.0;
								writer.addElement("td",nullToEmptyString(formatter.format(IADCDuration)), attr_default);//total iadc
							}
							
							Double connectionTime = 0.0;
							if (IADCDuration==null)IADCDuration=0.0;
							if (timeOnBtm==null)timeOnBtm=0.0;
							connectionTime = IADCDuration - timeOnBtm;
							connectionTime = Math.round(connectionTime * 100) / 100.0;
							writer.addElement("td",nullToEmptyString(formatter.format(connectionTime)), attr_default);//connection time hrs
							
							Double numberConnection = 0.0;
							numberConnection = progress / 31.5; //in feet			
							numberConnection = Math.round(numberConnection * 100) / 100.0;;
							writer.addElement("td",nullToEmptyString(formatter.format(numberConnection)), attr_default);//number of connection			
							
							Double connectionTimeInMinute = 0.0;
							if (numberConnection != 0.0){
								connectionTimeInMinute = connectionTime / numberConnection * 60;	
								connectionTimeInMinute = Math.round(connectionTimeInMinute * 100) / 100.0;
							}					
							writer.addElement("td",nullToEmptyString(formatter.format(connectionTimeInMinute)), attr_default);//connection time in mminutes
								
							cumConnectionTime += connectionTime;
							cumNumberConnection += numberConnection;		
							
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[11]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[12]), attr_default);
							
							Date dateIn = getDayDate(nullToEmptyString(BitrunDetailobj[13]));
							if (dateIn!=null) {
								writer.addElement("td",df_3.format(dateIn), attr_default);
							} else {
								writer.addElement("td","", attr_default);
							}
							
							Date dateOut = getDayDate(nullToEmptyString(BitrunDetailobj[14]));
							if (dateOut!=null) {
								writer.addElement("td",df_3.format(dateOut), attr_default);
							} else {
								writer.addElement("td","", attr_default);
							}
							
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[15]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[16]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[17]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[18]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[19]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[20]), attr_default);
							writer.addElement("td",nullToEmptyString(BitrunDetailobj[21]), attr_default);
							
							writer.endElement();
						}
					}
				}
			}
			
			Double averageConnectionTime = 0.00;
			if (cumNumberConnection != 0.0){
				averageConnectionTime = cumConnectionTime / cumNumberConnection * 60;			
			}
			
			writer.startElement("tr");
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td", nullToEmptyString(formatter.format(cumConnectionTime)), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(formatter.format(cumNumberConnection)), attr_calculated_data);
			writer.addElement("td", nullToEmptyString(formatter.format(averageConnectionTime)), attr_calculated_data);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.addElement("td","", attr_default_bit);
			writer.endElement();
		}
		writer.endElement();
		
	writer.close();
	return bytes.toString("utf-8");	
	}

	private HttpServletRequest getCurrentHttpRequest(){
		return this._httpRequest;
	}
	
	private UserSession getCurrentUserSession() throws Exception {
		return UserSession.getInstance(this._httpRequest);
	}
	
	private String nullToEmptyString(Object obj) throws Exception {
		return CommonUtils.null2EmptyString(obj);
	}
	
	private Double nullToZero(Double value) {
		if (value==null) return 0.0;
		return value;
	}
	
	private String emptyToDash(String value) {
		if (StringUtils.isBlank(value)) return "-";
		return value;
	}
	
	private class TypeComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Map<String, Object> in1 = (Map<String, Object>) o1[1];
				Map<String, Object> in2 = (Map<String, Object>) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.get("type").toString());
				String f2 = WellNameUtil.getPaddedStr(in2.get("type").toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	private class dayComparator implements Comparator<String>{
		public int compare(String o1, String o2) {
			
			try{
				o1 = WellNameUtil.getPaddedStr(o1);
				o2 = WellNameUtil.getPaddedStr(o2);
				
				if (o1 == null || o2 == null) return 0;
				return o1.compareTo(o2);
				
			} catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private class operationComparator implements Comparator<String>{
		private UserSession userSession;
		public operationComparator(UserSession userSession)
		{
			this.userSession = userSession;
		}
		public int compare(String o1, String o2) {
			
			try{
				o1 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), o1, null, true));
				o2 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), o2, null, true));
				
				if (o1 == null || o2 == null) return 0;
				return o1.compareTo(o2);
				
			} catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private String getClassName(String classCode) {
		String className = classCode;
		try {
			List<LookupClassCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupClassCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", classCode);
			if (list.size()>0) {
				LookupClassCode code = list.get(0);
				className = code.getName() + " (" + classCode + ")";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return className;
		
	}
	
	private String getPhaseAltCode(String shortCode, String operationCode) throws Exception {
		String phaseCode = nullToEmptyString(shortCode);
		if (phaseCode!="") {
			String strSql = "SELECT shortCode, alternateCode FROM LookupPhaseCode WHERE (isDeleted=false OR isDeleted is null) AND " +	
							"shortCode=:shortCode AND (operationCode=:operationCode or operationCode='' or operationCode is null)";
			String[] paramsFields = {"shortCode", "operationCode"};
			Object[] paramsValues = {shortCode, operationCode};
			
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (result.size()>0) {
				phaseCode = nullToEmptyString(result.get(0)[1]);
				if (phaseCode=="") phaseCode = shortCode;
			}
		}
		return phaseCode;
	}
	
	private String getRootCauseName(String rootCauseCode) throws Exception {
		String rootCauseName = rootCauseCode;
		List<LookupRootCauseCode> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupRootCauseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", rootCauseCode);
		if (result.size()>0) {
			LookupRootCauseCode code = result.get(0);
			rootCauseName = code.getName();
		}
		return rootCauseName;
	}
	
	private Map<String, String> getDailyMap(String operationUid) throws Exception {
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		
		Map<String, String> map = null;
		String strSql = "SELECT dailyUid,dayDate FROM Daily WHERE (isDeleted is null OR isDeleted = false) AND operationUid=:thisOperationUid";
		List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"thisOperationUid"}, new Object[]{operationUid});
		if(rs != null) {
			map = new HashMap<String, String>(rs.size());
			for (Object[] objs : rs) {
				if (objs[0] != null && objs[1] != null) {
					String key = (String) objs[0];
					map.put(key, (String) df.format(objs[1]));
				}
			}
		}
		return map;
	}
	
	private Double convertTo12Or24Day(Double duration, OperationUomContext operationUomContext) throws Exception{
		try {
			CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			if (duration!=null && duration >= 0){
				durationConverter.setBaseValue(nullToZero(duration));
				return durationConverter.getConvertedValue();		
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.00;
	}
	
	private Double activityDurationByClassCode(String dailyUid, String classCode) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String activitySql = "SELECT sum(activityDuration) FROM Activity where dailyUid=:dailyUid " +
						"AND (isDeleted is null or isDeleted=false) AND (isOffline=0 OR isOffline IS NULL) " +
						"AND (isSimop=0 OR isSimop IS NULL) AND dayPlus=0 AND internalClassCode=:classCode";
		String[] paramsFields = {"dailyUid", "classCode"};
		Object[] paramsValues = {dailyUid, classCode};
		
		List activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activitySql, paramsFields, paramsValues, qp);
		
		Double totalDuration = 0.0;
		if(activityList.get(0) != null) {
			totalDuration = Double.parseDouble(activityList.get(0).toString());
		}
		
		return totalDuration;
	}
	
	private String testMyHtmlFunctions() throws Exception { //TODO: THIS FUNCTION FOR TESTING PURPOSE ONLY
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 

		writer.startElement("root");
		
		Map<String, String> operations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:operations.entrySet()) {
			String operationUid = entry.getValue();
			
			SimpleAttributes attr = new SimpleAttributes();
			attr.addAttribute("border", "1");
			writer.startElement("table", attr);
	
			writer.startElement("tr");
			writer.addElement("td","kwong here");
			writer.endElement();
			
			attr = new SimpleAttributes();
			attr.addAttribute("style", "font-weight:bold;");
			writer.startElement("tr");
			writer.addElement("td","kwong here", attr);
			writer.endElement();
			
			attr = new SimpleAttributes();
			attr.addAttribute("style", "text-decoration:underline;color:blue");
			writer.startElement("tr");
			writer.addElement("td","kwong here", attr);
			writer.endElement();
			
			writer.startElement("tr");
			writer.addElement("td","operaitonUid:");
			writer.addElement("td",operationUid);
			writer.endElement();
			
			writer.startElement("tr");
			writer.addElement("td","operaitonName:");
			writer.addElement("td",CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
			writer.endElement();
			
			writer.endElement();
		}
		writer.endAllElements();
		writer.close();
		return bytes.toString("utf-8");
	}
	
	
	public String queryLwdWirelineOSL() throws Exception {
		List<String> operations = new ArrayList<String>(); 
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}

		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		writer.startElement("root");
		
		DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
		
		QueryProperties qp = new QueryProperties();
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		qp.setUomConversionEnabled(false);
		
		String lookup = "xml://bitsize?key=code&amp;value=label";
		Map<String, LookupItem> holeSizeList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale());
		Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
		for (String operationUid : operations) {
			operationUomContext.setOperationUid(operationUid);
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
			SimpleAttributes attr = new SimpleAttributes();
			
			//Data Container
			Map<String, Object> LwdWirelineList = new HashMap<String, Object>();
			
			//Wireline 
			String wirelinequery = "SELECT DISTINCT ws.operationUid,ws,wr FROM WirelineSuite ws, WirelineRun wr " +
							  "WHERE (ws.isDeleted=false OR ws.isDeleted is null) " +
							  "AND (wr.isDeleted=false OR wr.isDeleted is null) " +
							  "AND ws.wirelineSuiteUid =wr.wirelineSuiteUid " +
							  "AND ws.operationUid =:operationUid " +
							  "GROUP BY ws.operationUid, ws.wirelineSuiteUid " +
							  "ORDER BY wr.rigupStartdateRigupStarttime";
			List <Object[]> wirelinelist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelinequery, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
			for (Object[] wirelineobj:wirelinelist){
				
				WirelineSuite ws= (WirelineSuite) wirelineobj[1];
				WirelineRun wr= (WirelineRun) wirelineobj[2];
				
				Map<String, Object> LwdWireline = new HashMap<String, Object>();
			
				//Attributes Start
				LwdWireline.put("suiteNumber",nullToEmptyString(ws.getSuiteNumber()));
				LwdWireline.put("runNumber",nullToEmptyString(wr.getRunNumber()));
				
				if (wr.getRigupStartdateRigupStarttime() != null) {
					LwdWireline.put("Date",nullToEmptyString(df.format((Date) wr.getRigupStartdateRigupStarttime())));
					
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime((Date) wr.getRigupStartdateRigupStarttime());
					LwdWireline.put("DateInTime", thisCalendar.getTimeInMillis());
					LwdWireline.put("type", "WIRELINE" + thisCalendar.getTimeInMillis());
				} else {
					LwdWireline.put("type", "WIRELINE");
				}

				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineSuite.class, "depthMdMsl",operationUomContext);
				thisConverter.setBaseValue(nullToZero(ws.getDepthMdMsl()));
				LwdWireline.put("TopDepth",thisConverter.formatOutputPrecision());
			
				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineSuite.class, "shoeDepthMdMsl",operationUomContext);
				thisConverter.setBaseValue(nullToZero(ws.getShoeDepthMdMsl()));
				LwdWireline.put("BottomDepth", thisConverter.formatOutputPrecision());

				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineRun.class, "temperature",operationUomContext);
				thisConverter.setBaseValue(nullToZero(wr.getTemperature()));
				LwdWireline.put("temperature", thisConverter.formatOutputPrecision());
				
				LwdWireline.put("holeSize",nullToEmptyString(wr.getHoleSize()));
				
				LwdWireline.put("comments",nullToEmptyString(wr.getComments()));

				//Concatenate the toolCode
				List<WirelineTool> wirelineToolLists;
				String[] wirelineToolparamsFields = {"operationUid","wirelineRunUid"};
				Object[] wirelineToolparamsValues = {operationUid,wr.getWirelineRunUid()};
										
				String wirelineToolQuery = "SELECT wt.wirelineToolUid,wt.toolCode from WirelineRun wr, WirelineTool wt WHERE " +
						"wt.operationUid=:operationUid AND wt.wirelineRunUid=:wirelineRunUid AND wr.wirelineRunUid=wt.wirelineRunUid " +
						"AND (wt.isDeleted = false or wt.isDeleted is null) AND (wr.isDeleted = false OR wr.isDeleted is null)";
				wirelineToolLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineToolQuery, wirelineToolparamsFields, wirelineToolparamsValues, qp);				
				String toolCodeList=null;
				if(wirelineToolLists != null && !wirelineToolLists.isEmpty()){
					for(Object objResultwirelineTool: wirelineToolLists){
						Object[] objRstwirelineTool = (Object[]) objResultwirelineTool;
						if (toolCodeList==null)
						{
							toolCodeList= nullToEmptyString(objRstwirelineTool[1]);
						}
						else{
							toolCodeList=toolCodeList+","+ nullToEmptyString(objRstwirelineTool[1]);
						}
					}
				}
				LwdWireline.put("toolCodes",toolCodeList);
				
				//Get Label from lookup.xml
				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineSuite.class, "holeSize",operationUomContext);
				String holeSize = "";
				if (holeSizeList != null && nullToZero((Double) wr.getHoleSize())!=0) {
					thisConverter.setBaseValue(nullToZero((Double) wr.getHoleSize())==0?0.0:nullToZero((Double) wr.getHoleSize()));
					holeSize = thisConverter.formatOutputPrecision();
					LookupItem lookupItem = holeSizeList.get(holeSize);
					if (lookupItem != null) {
						holeSize = nullToEmptyString(lookupItem.getValue());
					}
					else
						holeSize = "";
				}
				LwdWireline.put("holeSize", holeSize);
				LwdWirelineList.put(ws.getWirelineSuiteUid(), LwdWireline);
			}	
			
			//LWD			
			String lwdquery = "SELECT DISTINCT ls.operationUid,ls,lr,lt.toolType,br.bitDiameter,d.dayDate FROM LwdSuite ls, Lwd lr, LwdTool lt, Bitrun br, Daily d " +
							  "WHERE (ls.isDeleted=false OR ls.isDeleted is null) " +
							  "AND (lr.isDeleted=false OR lr.isDeleted is null) " +
							  "AND (lt.isDeleted=false OR lt.isDeleted is null) " +
							  "AND (br.isDeleted=false OR br.isDeleted is null) " +
							  "AND ls.lwdSuiteUid =lr.lwdSuiteUid " +
							  "AND ls.lwdSuiteUid =lt.lwdSuiteUid " +
							  "AND lr.bharunUid =br.bharunUid " +
							  "AND ls.inDailyUid =d.dailyUid " +
							  "AND ls.operationUid =:operationUid " +
							  "GROUP BY ls.operationUid, ls.lwdSuiteUid " +
							  "ORDER BY d.dayDate";
			
			List <Object[]> lwdlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdquery, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
			for (Object[] lwdobj:lwdlist){
				//TODO
				LwdSuite ls= (LwdSuite) lwdobj[1];
				Lwd lr= (Lwd) lwdobj[2];

				Map<String, Object> LwdWireline = new HashMap<String, Object>();
				
				//Attributes Start
				LwdWireline.put("suiteNumber","");
				LwdWireline.put("runNumber",nullToEmptyString(ls.getSuiteNum()));
				
				if (lwdobj[5] != null) {
					LwdWireline.put("Date",nullToEmptyString(df.format((Date) lwdobj[5])));
					
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime((Date) lwdobj[5]);
					LwdWireline.put("DateInTime", thisCalendar.getTimeInMillis());
					LwdWireline.put("type", "LWD" + thisCalendar.getTimeInMillis());
				} else {
					LwdWireline.put("type", "LWD");
				}

				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdSuite.class, "depthInMdMsl",operationUomContext);
				thisConverter.setBaseValue(nullToZero(ls.getDepthInMdMsl()));
				LwdWireline.put("TopDepth",thisConverter.formatOutputPrecision());

				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdSuite.class, "depthOutMdMsl",operationUomContext);
				thisConverter.setBaseValue(nullToZero(ls.getDepthOutMdMsl()));
				LwdWireline.put("BottomDepth", thisConverter.formatOutputPrecision());
				
				LwdWireline.put("temperature", "N/A");
				LwdWireline.put("comments",nullToEmptyString(lr.getComments()));

				//Concatenate the toolCode
				List<String> toolCodeList = new ArrayList<String>();
				for (Object[] rec:lwdlist) {
					String toolType = nullToEmptyString(rec[3]);
					if (!toolCodeList.contains(toolType)) toolCodeList.add(toolType);
				}

				String toolType = StringUtils.join(toolCodeList, ",");
				LwdWireline.put("toolCodes",toolType);
				
				//Get Label from lookup.xml
				thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "bitDiameter",operationUomContext);
				String holeSize = "";
				if (holeSizeList != null && nullToZero((Double) lwdobj[4])!=0) {
					thisConverter.setBaseValue(nullToZero((Double) lwdobj[4])==0?0.0:nullToZero((Double) lwdobj[4]));
					holeSize = thisConverter.formatOutputPrecision();
					LookupItem lookupItem = holeSizeList.get(holeSize);
					if (lookupItem != null) {
						holeSize = nullToEmptyString(lookupItem.getValue());
					}
					else
						holeSize = "";
				}
				LwdWireline.put("holeSize", holeSize);
				LwdWirelineList.put(ls.getLwdSuiteUid(), LwdWireline);
			}	
			
			// Sort the Data Container by Date
			List<Object[]> mapList = new ArrayList();
			for (Map.Entry<String, Object>item : LwdWirelineList.entrySet()) {
				mapList.add(new Object[] {item.getKey(), item.getValue()});
			}
			Collections.sort(mapList, new TypeComparator());
			
			//Draw html table
			attr.addAttribute("border","1");
			writer.startElement("table", attr);
			//Draw Table Header Row 1
			String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
			writer.startElement("tr");
			SimpleAttributes attr2 = new SimpleAttributes();
			attr2.addAttribute("colspan", "9");
			attr2.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
			writer.addElement("td", "Well : " + fullWellName, attr2);
			writer.endElement();
			//Draw Table Header Row 2
			writer.startElement("tr");
			SimpleAttributes attr5TdHeader = new SimpleAttributes();
			attr5TdHeader.addAttribute("width","5");
			attr5TdHeader.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
			writer.addElement("td","Suite", attr5TdHeader);
			writer.addElement("td","Run", attr5TdHeader);
			
			SimpleAttributes attr10TdHeader = new SimpleAttributes();
			attr10TdHeader.addAttribute("width","10");
			attr10TdHeader.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
			writer.addElement("td","Date", attr10TdHeader);
			thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdSuite.class, "depthInMdMsl",operationUomContext);
			writer.addElement("td","Top Depth ("+thisConverter.getUomSymbol()+")", attr10TdHeader);
			writer.addElement("td","Bottom Depth ("+thisConverter.getUomSymbol()+")", attr10TdHeader);
			writer.addElement("td","Hole Size", attr10TdHeader);
			writer.addElement("td","Tool String", attr10TdHeader);
			thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineRun.class, "temperature",operationUomContext);
			writer.addElement("td","Max Temp. ("+thisConverter.getUomSymbol()+")", attr10TdHeader);
			
			SimpleAttributes attr30TdHeader = new SimpleAttributes();
			attr30TdHeader.addAttribute("width", "30");
			attr30TdHeader.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
			writer.addElement("td","Comments", attr30TdHeader);
			writer.endElement();
			
			//Table Result 				
			for(Object[] obj_array:mapList){
				writer.startElement("tr");
				Map<String, Object> LwdWirelineDetail = (Map<String, Object>)obj_array[1];
				attr = new SimpleAttributes();
				
				SimpleAttributes attr5Td = new SimpleAttributes();
				attr5Td.addAttribute("width","5");
				attr5Td.addAttribute("style", "text-align:right;");
				
				SimpleAttributes attr10Td = new SimpleAttributes();
				attr10Td.addAttribute("width","10");
				attr10Td.addAttribute("style", "text-align:right;");
				
				SimpleAttributes attr30td = new SimpleAttributes();
				attr30td.addAttribute("width", "30");
				
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("suiteNumber")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("runNumber")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("Date")), attr10Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("TopDepth")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("BottomDepth")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("holeSize")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("toolCodes")), attr10Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("temperature")), attr5Td);
				writer.addElement("td", nullToEmptyString(LwdWirelineDetail.get("comments")), attr30td);
				writer.endElement();
			}
			writer.endElement();
		}
		writer.close();
		return bytes.toString("utf-8");
	}
	
	public String queryLwdSummaryOSL() throws Exception {
		
		String uri = "db://LwdToolLookup?key=lwdToolLookupUid&value=toolDescr&order=toolDescr";
		Map<String, LookupItem> lwdToolLookup = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
		
		//operation filter
		List<String> operations = new ArrayList<String>(); 
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		writer.startElement("root");
		
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
		for (String operationUid : operations) {
			
			Map<String, String> dailyMap = getDailyMap(operationUid);
			operationUomContext.setOperationUid(operationUid);
			SimpleAttributes attr = new SimpleAttributes();
			
			String depthInUomSymbol = null;
			String depthOutUomSymbol = null;
			String disToBitUomSymbol = null;
			
			String sql = "SELECT ls.operationUid, ls FROM LwdSuite ls WHERE (ls.isDeleted is null or ls.isDeleted = false) AND ls.operationUid=:thisOperationUid ORDER BY suiteNum";
			List<Object[]> thisLwdSuiteList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"thisOperationUid"}, new Object[]{operationUid},queryProperties);
			int max=0;
			//Grab all record and put in LwdSuiteListMap
			Map<String, Object> LwdSuiteLists = new LinkedHashMap<String, Object>();
			for (Object[] rec : thisLwdSuiteList) {
				
				Double depthInMdMsl = 0.0;
				Double depthOutMdMsl = 0.0;
				
				if (rec[0]==null || rec[1]==null) continue;
				
				CustomFieldUom depthInConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdSuite.class, "depthInMdMsl", operationUomContext);
				CustomFieldUom depthOutConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdSuite.class, "depthOutMdMsl", operationUomContext);
				
				LwdSuite lwdSuite = (LwdSuite) rec[1];
				
				if(lwdSuite.getDepthInMdMsl() != null) depthInMdMsl = lwdSuite.getDepthInMdMsl();
				if(lwdSuite.getDepthOutMdMsl() != null) depthOutMdMsl = lwdSuite.getDepthOutMdMsl();
				depthInConverter.setBaseValue(depthInMdMsl);
				depthOutConverter.setBaseValue(depthOutMdMsl);
				
				depthInUomSymbol = depthInConverter.getUomSymbol();
				depthOutUomSymbol = depthOutConverter.getUomSymbol();
				
				Map<String, Object> LwdSuiteList = new LinkedHashMap<String, Object>();
				LwdSuiteList.put("LwdRun", lwdSuite.getSuiteNum());
				LwdSuiteList.put("LwdDateIn", dailyMap.containsKey(lwdSuite.getInDailyUid())?dailyMap.get(lwdSuite.getInDailyUid()):"");
				LwdSuiteList.put("LwdDateOut", dailyMap.containsKey(lwdSuite.getOutDailyUid())?dailyMap.get(lwdSuite.getOutDailyUid()):"");
				LwdSuiteList.put("LwdLogStartDepth", depthInConverter.formatOutputPrecision());
				LwdSuiteList.put("LwdLogEndDepth", depthOutConverter.formatOutputPrecision());
				LwdSuiteList.put("LwdWitness", lwdSuite.getWitness01());
				LwdSuiteList.put("LwdCompany", lwdSuite.getCompanyUid());
				LwdSuiteList.put("LwdEngineer1", lwdSuite.getLwdEngineer01());
				LwdSuiteList.put("LwdEngineer2", lwdSuite.getLwdEngineer02());
				LwdSuiteList.put("LwdEngineer3", lwdSuite.getLwdEngineer03());
				LwdSuiteList.put("LwdObjectives", lwdSuite.getObjectives());
				
				sql = "SELECT l.lwdUid, l.comments FROM Lwd l WHERE (l.isDeleted is null or l.isDeleted = false) AND l.lwdSuiteUid=:thisLwdSuiteUid";
				List thisLwdList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"thisLwdSuiteUid"}, new Object[]{lwdSuite.getLwdSuiteUid()},queryProperties);
				Map<String, Object> LwdLists = new LinkedHashMap<String, Object>();
				for (Object result : thisLwdList) {
					Object[] rec2 = (Object[]) result;
					Map<String, Object> LwdList = new LinkedHashMap<String, Object>();
					LwdList.put("comment", rec2[1]);
					LwdLists.put(nullToEmptyString(rec2[0]), LwdList);
				}
				
				sql = "SELECT lt.lwdToolUid, lt.toolNumber, lt.toolDesc, lt.toolType, lt.distanceFromBit FROM LwdTool lt WHERE (lt.isDeleted is null or lt.isDeleted = false) AND lt.lwdSuiteUid=:thisLwdSuiteUid ORDER BY toolNumber";
				List thisLwdToolList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"thisLwdSuiteUid"}, new Object[]{lwdSuite.getLwdSuiteUid()},queryProperties);
				Map<String, Object> LwdToolLists = new LinkedHashMap<String, Object>();
				for (Object result : thisLwdToolList) {
					Object[] rec2 = (Object[]) result;
					
					CustomFieldUom distanceFromBitConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdTool.class, "distanceFromBit", operationUomContext);
					
					Double distanceFromBit = rec2[4]!=null?Double.parseDouble(nullToEmptyString(rec2[4])):0.0;
					disToBitUomSymbol = distanceFromBitConverter.getUomSymbol();
					
					distanceFromBitConverter.setBaseValue(distanceFromBit);
					Map<String, Object> LwdToolList = new LinkedHashMap<String, Object>();
					LwdToolList.put("toolNumber", rec2[1]);
					LwdToolList.put("toolDesc", rec2[2]);
					LwdToolList.put("toolType", rec2[3]);
					LwdToolList.put("toolDistFromBit", distanceFromBitConverter.formatOutputPrecision());
					LwdToolLists.put(nullToEmptyString(rec2[0]), LwdToolList);
				}
				max=thisLwdToolList.size()>max?thisLwdToolList.size():max;
				LwdSuiteList.put("LwdSummary", LwdLists);
				LwdSuiteList.put("LwdTool", LwdToolLists);
				
				LwdSuiteLists.put(lwdSuite.getLwdSuiteUid(), LwdSuiteList);
			}
			
			//Make the Tool Number column to expand when tool number increase for selected operation
			if (max > 0) {
				attr = new SimpleAttributes();
				attr.addAttribute("border", "1");
				writer.startElement("table", attr);
				
				writer.startElement("tr");
				int numCol = 12;
				if (max > 0) numCol = numCol + (max * 3);
				
				attr = new SimpleAttributes();
				attr.addAttribute("colspan", String.valueOf(numCol));
				attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
				writer.addElement("td", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true), attr);
				writer.endElement();
				
				attr = new SimpleAttributes();
				if (max > 0) attr.addAttribute("rowspan", "2");
				attr.addAttribute("nowrap", "nowrap");
				attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
				writer.startElement("tr");
				writer.addElement("td", "LWD Run#", attr);
				writer.addElement("td", "Date In", attr);
				writer.addElement("td", "Date Out", attr);
				writer.addElement("td", "Log Start Depth (" + depthInUomSymbol + ")", attr);
				writer.addElement("td", "Log End Depth (" + depthOutUomSymbol + ")", attr);
				writer.addElement("td", "Witness", attr);
				writer.addElement("td", "LWD Co.", attr);
				writer.addElement("td", "Engineer#1", attr);
				writer.addElement("td", "Engineer#2", attr);
				writer.addElement("td", "Engineer#3", attr);
				writer.addElement("td", "Objectives", attr);
				writer.addElement("td", "Run Summary", attr);
				if (max > 0) {
					for (int i=0; i<max; i++) {
						int toolNumber = i + 1;
						attr = new SimpleAttributes();
						attr.addAttribute("colspan", "3");
						attr.addAttribute("nowrap", "nowrap");
						attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
						writer.addElement("td", "Tool " + toolNumber, attr);
					}
				}
				writer.endElement(); // end first Header <tr>
				
				if (max > 0) {
					writer.startElement("tr");
					for (int i=0; i<max; i++) {
						attr = new SimpleAttributes();
						attr.addAttribute("nowrap", "nowrap");
						attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
						writer.addElement("td", "Tool Type", attr);
						writer.addElement("td", "Tool Desc", attr);
						writer.addElement("td", "Dist.from Bit ("+ disToBitUomSymbol +")", attr);
					}
					writer.endElement(); // end second Header <tr>
				}
				
				//Sort LWD Run number
				List<Object[]> mapList = new ArrayList();
				for (Map.Entry<String, Object>item : LwdSuiteLists.entrySet()) {
					mapList.add(new Object[] {item.getKey(), item.getValue()});
				}
				Collections.sort(mapList, new LwdRunComparator());
				
				//Use the map to form the record set
				for(Object[] obj_array : mapList) {
					Map<String, Object> LwdSuiteMap = (Map<String, Object>)obj_array[1];

					writer.startElement("tr");
					SimpleAttributes attrTextRightAlign = new SimpleAttributes();
					attrTextRightAlign.addAttribute("style", "text-align:right;");
					writer.addElement("td", LwdSuiteMap.get("LwdRun") != null? nullToEmptyString(LwdSuiteMap.get("LwdRun")) : "", attrTextRightAlign);
					writer.addElement("td", LwdSuiteMap.get("LwdDateIn") != null? nullToEmptyString(LwdSuiteMap.get("LwdDateIn")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdDateOut") != null? nullToEmptyString(LwdSuiteMap.get("LwdDateOut")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdLogStartDepth") != null? nullToEmptyString(LwdSuiteMap.get("LwdLogStartDepth")) : "", attrTextRightAlign);
					writer.addElement("td", LwdSuiteMap.get("LwdLogEndDepth") != null? nullToEmptyString(LwdSuiteMap.get("LwdLogEndDepth")) : "", attrTextRightAlign);
					writer.addElement("td", LwdSuiteMap.get("LwdWitness") != null? nullToEmptyString(LwdSuiteMap.get("LwdWitness")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdCompany") != null? nullToEmptyString(LwdSuiteMap.get("LwdCompany")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdEngineer1") != null? nullToEmptyString(LwdSuiteMap.get("LwdEngineer1")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdEngineer2") != null? nullToEmptyString(LwdSuiteMap.get("LwdEngineer2")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdEngineer3") != null? nullToEmptyString(LwdSuiteMap.get("LwdEngineer3")) : "");
					writer.addElement("td", LwdSuiteMap.get("LwdObjectives") != null? nullToEmptyString(LwdSuiteMap.get("LwdObjectives")) : "");
					
					Map<String, Object> LwdMaps = (Map<String, Object>) LwdSuiteMap.get("LwdSummary");
					String comment = "";
					for (Map.Entry<String, Object>item2 : LwdMaps.entrySet()) {
						Map<String, Object> LwdMap = (Map<String, Object>)item2.getValue();
						comment = "-" + LwdMap.get("comment") + "\n" + comment;
					}
					writer.addElement("td", StringUtils.isNotBlank(comment) ? comment : "");
					
					
					Map<String, Object> LwdTools = (Map<String, Object>) LwdSuiteMap.get("LwdTool");
					for (Map.Entry<String, Object>item2 : LwdTools.entrySet()) {
						String toolDescr = null;
						
						Map<String, Object> LwdTool = (Map<String, Object>)item2.getValue();
						
						if (LwdTool.get("toolDesc") != null) {
							if (lwdToolLookup.containsKey(LwdTool.get("toolDesc"))) toolDescr = nullToEmptyString(lwdToolLookup.get(LwdTool.get("toolDesc")).getValue());
						}
						attr = new SimpleAttributes();
						attr.addAttribute("style", "background-color:#FFAAFF;");
						
						writer.addElement("td", LwdTool.get("toolType") != null? nullToEmptyString(LwdTool.get("toolType")) : "", attr);
						writer.addElement("td", toolDescr != null? toolDescr : "");
						writer.addElement("td", LwdTool.get("toolDistFromBit") != null? nullToEmptyString(LwdTool.get("toolDistFromBit")) : "", attrTextRightAlign);
					}
					if (LwdTools.size() < max) {
						int colSpan = (max - LwdTools.size()) * 3;
						attr = new SimpleAttributes();
						attr.addAttribute("colspan", String.valueOf(colSpan));
						writer.addElement("td", "", attr);
					}
					
					writer.endElement();
				}
				writer.endElement();
			}
		}
		
		writer.close();
		return bytes.toString("utf-8");
	}
	
	public String queryWirelineWitnessOSL() throws Exception{
		
		List<String> operations = new ArrayList<String>();		
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		String selectedCompany = "";
		if (this.getFilter("companyUid") != null) {
			Map<String, String> companyFilters = (Map<String, String>) this.getFilter("companyUid");
			int i = companyFilters.size();
			for (Map.Entry<String, String> entry:companyFilters.entrySet()) {
				if (i==companyFilters.size()) {
					selectedCompany = "'" + entry.getValue() + "'";
				} else {
					selectedCompany += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedConveyanceMethod = "";
		if (this.getFilter("conveyanceMethod") != null) {
			Map<String, String> conveyanceMethodFilters = (Map<String, String>) this.getFilter("conveyanceMethod");
			int i = conveyanceMethodFilters.size();
			for (Map.Entry<String, String> entry:conveyanceMethodFilters.entrySet()) {
				if (i==conveyanceMethodFilters.size()) {
					selectedConveyanceMethod = "'" + entry.getValue() + "'";
				} else {
					selectedConveyanceMethod += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedTools = "";
		if (this.getFilter("toolCode") != null) {
			Map<String, String> toolsFilters = (Map<String, String>) this.getFilter("toolCode");
			int i = toolsFilters.size();
			for (Map.Entry<String, String> entry:toolsFilters.entrySet()) {				
				if (i==toolsFilters.size()) {
					selectedTools = "'" + entry.getValue() + "'";
				} else {
					selectedTools += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedClass = "";
		if (this.getFilter("wirelineClassCode") != null) {
			Map<String, String> classFilters = (Map<String, String>) this.getFilter("wirelineClassCode");
			int i = classFilters.size();
			for (Map.Entry<String, String> entry:classFilters.entrySet()) {
				if (i==classFilters.size()) {
					selectedClass = "'" + entry.getValue() + "'";
				} else {
					selectedClass += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedTask = "";
		if (this.getFilter("wirelineTaskCode") != null) {
			Map<String, String> taskFilters = (Map<String, String>) this.getFilter("wirelineTaskCode");
			int i = taskFilters.size();
			for (Map.Entry<String, String> entry:taskFilters.entrySet()) {
				if (i==taskFilters.size()) {
					selectedTask = "'" + entry.getValue() + "'";
				} else {
					selectedTask += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String searchText = nullToEmptyString(this.getFilter("wirelineComments"));
		
		String actLogOpUri="xml://WirelineWitness.act_log_op?key=code&amp;value=label"; 											
		Map<String,LookupItem> actLogOpCatList = LookupManager.getConfiguredInstance().getLookup(actLogOpUri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
		
		String actLogClassUri="xml://WirelineWitness.act_log_class?key=code&amp;value=label"; 												
		Map<String,LookupItem> actLogClassCatList = LookupManager.getConfiguredInstance().getLookup(actLogClassUri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
		
		String companyUri = "db://LookupCompany?key=lookupCompanyUid&value=companyName";
		Map<String, LookupItem> companyList = LookupManager.getConfiguredInstance().getLookup(companyUri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
		
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);			
			
			writer.startElement("root");
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			SimpleAttributes tableattrib = new SimpleAttributes();
			tableattrib.addAttribute("border","1");
			
			SimpleAttributes operationnameattributes = new SimpleAttributes();
			operationnameattributes.addAttribute("colspan", "12");
			operationnameattributes.addAttribute("style", "font-weight:bold;background-color:#3B9C9C;");
			
			SimpleAttributes HeaderAttributes8 = new SimpleAttributes();
			HeaderAttributes8.addAttribute("width", "8");
			HeaderAttributes8.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes HeaderAttributes3 = new SimpleAttributes();
			HeaderAttributes3.addAttribute("width", "3");
			HeaderAttributes3.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes HeaderAttributes10 = new SimpleAttributes();
			HeaderAttributes10.addAttribute("width", "10");
			HeaderAttributes10.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes HeaderAttributes6 = new SimpleAttributes();
			HeaderAttributes6.addAttribute("width", "6");
			HeaderAttributes6.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes HeaderAttributes33 = new SimpleAttributes();
			HeaderAttributes33.addAttribute("width", "33");
			HeaderAttributes33.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes TDAttributes8 = new SimpleAttributes();
			TDAttributes8.addAttribute("width", "8");
			
			SimpleAttributes TDAttributes3 = new SimpleAttributes();
			TDAttributes3.addAttribute("width", "3");
			
			SimpleAttributes TDRightAttributes3 = new SimpleAttributes();
			TDRightAttributes3.addAttribute("width", "3");
			TDRightAttributes3.addAttribute("style", "text-align:right;");
			TDRightAttributes3.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDAttributes10 = new SimpleAttributes();
			TDAttributes10.addAttribute("width", "10");
			
			SimpleAttributes TDAttributes6 = new SimpleAttributes();
			TDAttributes6.addAttribute("width", "6");
			
			SimpleAttributes TDCenterAttributes6 = new SimpleAttributes();
			TDCenterAttributes6.addAttribute("width", "6");
			TDCenterAttributes6.addAttribute("style", "text-align:center;");
			TDCenterAttributes6.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDRightAttributes6 = new SimpleAttributes();
			TDRightAttributes6.addAttribute("width", "6");
			TDRightAttributes6.addAttribute("style", "text-align:right;");
			TDRightAttributes6.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDAttributes33 = new SimpleAttributes();
			TDAttributes33.addAttribute("width", "33");
			
			SimpleAttributes classattributes = new SimpleAttributes();
			classattributes.addAttribute("colspan", "12");
			
			SimpleAttributes classHeaderAttributes = new SimpleAttributes();
			classHeaderAttributes.addAttribute("colspan", "12");
			classHeaderAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			
			SimpleAttributes operationattributes = new SimpleAttributes();
			operationattributes.addAttribute("colspan", "12");
			
			SimpleAttributes operationHeaderAttributes = new SimpleAttributes();
			operationHeaderAttributes.addAttribute("colspan", "12");
			operationHeaderAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			
			SimpleAttributes wellClassattributes = new SimpleAttributes();
			wellClassattributes.addAttribute("colspan", "12");
			
			SimpleAttributes wellClassHeaderAttributes = new SimpleAttributes();
			wellClassHeaderAttributes.addAttribute("colspan", "12");
			wellClassHeaderAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			
			SimpleAttributes wellOperationattributes = new SimpleAttributes();
			wellOperationattributes.addAttribute("colspan", "12");
			
			SimpleAttributes wellOperationHeaderAttributes = new SimpleAttributes();
			wellOperationHeaderAttributes.addAttribute("colspan", "12");
			wellOperationHeaderAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			
			Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
			for (String operationUid : operations) {
				String operationName=CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid);
				
				CustomFieldUom mdMslUomSymbolConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "mdMsl");
				CustomFieldUom durationUomSymbolConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "duration");
				
				List<WirelineSuite> wirelineSuiteLists;
				String[] paramsFields = {"operationUid"};
				Object[] paramsValues = {operationUid};
				
				String wirelineSuiteQuery = "SELECT DISTINCT ws.operationUid,ws.wirelineSuiteUid,wr.wirelineRunUid,ws.suiteNumber,wr.runNumber,wr.svcCo " +
						"FROM WirelineSuite ws,WirelineRun wr, WirelineLog wl " +
						(selectedTools.length()>0?", WirelineTool wt ":"") +
						"WHERE (ws.isDeleted = false or ws.isDeleted is null) AND " +
						"(wr.isDeleted = false or wr.isDeleted is null) AND ws.operationUid=:operationUid AND ws.wirelineSuiteUid=wr.wirelineSuiteUid AND" +
						(selectedTools.length()>0?"(wt.isDeleted = false or wt.isDeleted is null) AND wr.wirelineRunUid=wt.wirelineRunUid AND ":"") +
						"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
						(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
						(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
						(selectedTools.length()>0?" AND wt.toolCode IN ("+selectedTools+") ":"") +
						(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
						(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
						(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"");
				wirelineSuiteLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineSuiteQuery, paramsFields, paramsValues, qp);
				
				if (wirelineSuiteLists.size() > 0)
				{					
					writer.startElement("table", tableattrib);
					writer.startElement("tr");
					writer.addElement("td", "Well Name : " + operationName, operationnameattributes);
					writer.endElement();
				
					writer.startElement("tr");
					writer.addElement("td", "Day", HeaderAttributes10);
					writer.addElement("td", "Suite #", HeaderAttributes3);
					writer.addElement("td", "Run #", HeaderAttributes3);
					writer.addElement("td", "Company", HeaderAttributes10);
					writer.addElement("td", "Tools", HeaderAttributes8);
					writer.addElement("td", "Start Time", HeaderAttributes6);
					writer.addElement("td", "End Time", HeaderAttributes6);
					writer.addElement("td", "Duration ("+durationUomSymbolConverter.getUomSymbol()+")", HeaderAttributes3);
					writer.addElement("td", "MD ("+mdMslUomSymbolConverter.getUomSymbol()+")", HeaderAttributes6);
					writer.addElement("td", "Class", HeaderAttributes6);
					writer.addElement("td", "Operation", HeaderAttributes6);
					writer.addElement("td", "Descr", HeaderAttributes33);
					writer.endElement();
					for(Object objNumberofWirelineSuites : wirelineSuiteLists){
						Object[] objRstNumWirelineSuites = (Object[]) objNumberofWirelineSuites;
						String wirelineRunUid=nullToEmptyString(objRstNumWirelineSuites[2]);
						
						List<WirelineTool> wirelineToolLists;
						String[] wirelineToolparamsFields = {"operationUid","wirelineRunUid"};
						Object[] wirelineToolparamsValues = {operationUid,wirelineRunUid};
												
						String wirelineToolQuery = "SELECT wt.wirelineToolUid,wt.toolCode from WirelineRun wr, WirelineTool wt WHERE " +
								"wt.operationUid=:operationUid AND wt.wirelineRunUid=:wirelineRunUid AND wr.wirelineRunUid=wt.wirelineRunUid " +
								"AND (wt.isDeleted = false or wt.isDeleted is null) AND (wr.isDeleted = false OR wr.isDeleted is null)";
						wirelineToolLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineToolQuery, wirelineToolparamsFields, wirelineToolparamsValues, qp);				
						String toolCodeList=null;
						if(wirelineToolLists != null && !wirelineToolLists.isEmpty()){
							for(Object objResultwirelineTool: wirelineToolLists){
								Object[] objRstwirelineTool = (Object[]) objResultwirelineTool;
								if (toolCodeList==null)
								{
									toolCodeList= nullToEmptyString(objRstwirelineTool[1]);
								}
								else{
									toolCodeList=toolCodeList+","+ nullToEmptyString(objRstwirelineTool[1]);
								}
							}
						}
						
						List<WirelineLog> wirelineLogLists;
						String[] wirelineLogparamsFields = {"operationUid","wirelineRunUid"};
						Object[] wirelineLogparamsValues = {operationUid,wirelineRunUid};
						
						String wirelineLogQuery = "SELECT DISTINCT wl.operationUid,wl.dailyUid,wl.startTime,wl.endTime,wl.duration,wl.mdMsl,wl.actLogClass,wl.actLogOp,wl.descr " +
								"FROM WirelineRun wr, WirelineLog wl WHERE wl.operationUid=:operationUid AND wl.wirelineRunUid=:wirelineRunUid " +
								"AND wr.wirelineRunUid=wl.wirelineRunUid AND (wl.isDeleted = false or wl.isDeleted is null) AND " +
								"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
								(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
								(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
								(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
								(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
								(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"");
								
						wirelineLogLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineLogQuery, wirelineLogparamsFields, wirelineLogparamsValues, qp);				
						if(wirelineLogLists != null && !wirelineLogLists.isEmpty()){
							
							for(Object objResultwirelineLog: wirelineLogLists){
								operationUomContext.setOperationUid(operationUid);
								CustomFieldUom mdMslConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "mdMsl",operationUomContext);
								CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "duration",operationUomContext);
								
								SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
								SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm");
								
								Object[] objRstwirelineLog = (Object[]) objResultwirelineLog;
								
								Double durationValue=0.0;
								Double mdMslValue = 0.0;
							
								String dailyUid= null;
								
								writer.startElement("tr");//start items
								
								if (objRstwirelineLog[1] != null) {												
									dailyUid=nullToEmptyString(objRstwirelineLog[1]);
								}
								Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
								
								writer.addElement("td", nullToEmptyString(df.format(daily.getDayDate())), TDAttributes10); //day
									
								writer.addElement("td", objRstNumWirelineSuites[3] != null? nullToEmptyString(objRstNumWirelineSuites[3]) : "", TDAttributes3);//suite
								
								writer.addElement("td", objRstNumWirelineSuites[4] != null? nullToEmptyString(objRstNumWirelineSuites[4]) : "", TDAttributes3);//run
								if (objRstNumWirelineSuites[5] != null) {											
									if (companyList.containsKey(nullToEmptyString(objRstNumWirelineSuites[5]))) {
										LookupItem selectedLookup = companyList.get(nullToEmptyString(objRstNumWirelineSuites[5]));											
										writer.addElement("td", (String) selectedLookup.getValue(), TDAttributes10); //company
									} else {
										writer.addElement("td", nullToEmptyString(objRstNumWirelineSuites[5]), TDAttributes10); //company
									}
								}
								else {
									writer.addElement("td", "", TDAttributes10);
								}
								
								writer.addElement("td", toolCodeList != null? toolCodeList : "", TDAttributes8); //tools
								
								if (objRstwirelineLog[2] != null) {
									Date startTimeValue= (Date) objRstwirelineLog[2];
									writer.addElement("td", timeformat.format(startTimeValue), TDCenterAttributes6);//starttime
								} else {
									writer.addElement("td", null, TDCenterAttributes6);//starttime
								}
									
								if (objRstwirelineLog[3] != null) {
									Date endTimeValue= (Date) objRstwirelineLog[3];
									writer.addElement("td", timeformat.format(endTimeValue), TDCenterAttributes6);//endtime
								} else {
									writer.addElement("td", null, TDCenterAttributes6);//endtime
								}
								
								if (objRstwirelineLog[4] != null) durationValue= Double.parseDouble(nullToEmptyString(objRstwirelineLog[4]));
									durationConverter.setBaseValue(durationValue);	
									writer.addElement("td",durationConverter.formatOutputPrecision(), TDRightAttributes3); //duration
								
								if (objRstwirelineLog[5] != null) mdMslValue = Double.parseDouble(nullToEmptyString(objRstwirelineLog[5]));
								mdMslConverter.setBaseValue(mdMslValue);
								writer.addElement("td",mdMslConverter.formatOutputPrecision(), TDRightAttributes6); //MD
								
								if (objRstwirelineLog[6] != null) {	
									if(actLogClassCatList.containsKey(objRstwirelineLog[6]))  {
										writer.addElement("td",nullToEmptyString(actLogClassCatList.get(objRstwirelineLog[6]).getValue()), TDAttributes6); //class
									} else {
										writer.addElement("td",nullToEmptyString(objRstwirelineLog[6]), TDAttributes6); //class
									}
								} else {
									writer.addElement("td",null, TDAttributes6);
								}
									
								if (objRstwirelineLog[7] != null) {
									if(actLogOpCatList.containsKey(objRstwirelineLog[7]))  {
										writer.addElement("td",nullToEmptyString(actLogOpCatList.get(objRstwirelineLog[7]).getValue()), TDAttributes6); //operation
									} else {
										writer.addElement("td",nullToEmptyString(objRstwirelineLog[7]), TDAttributes6);
									}
								} else {
									writer.addElement("td",null, TDAttributes6);
								}
								
								writer.addElement("td", objRstwirelineLog[8] != null? nullToEmptyString(objRstwirelineLog[8]) : "", TDAttributes8); //descr
								
								writer.endElement();//end items
							}	
						
						}//wirelinerun end
						
						List<WirelineLog> wirelineLogClassLists;
						String[] wirelineLogClassparamsFields = {"operationUid","wirelineRunUid"};
						Object[] wirelineLogClassparamsValues = {operationUid,wirelineRunUid};
						String wirelineLogClassQuery = "SELECT wl.actLogClass, sum(wl.duration) FROM WirelineRun wr, WirelineLog wl " +
								(selectedTools.length()>0?", WirelineTool wt ":"") +
								"WHERE wl.operationUid=:operationUid AND wl.wirelineRunUid=:wirelineRunUid AND wr.wirelineRunUid=wl.wirelineRunUid " +
								"AND (wl.isDeleted = false or wl.isDeleted is null) AND " +
								(selectedTools.length()>0?"(wt.isDeleted = false or wt.isDeleted is null) AND wr.wirelineRunUid=wt.wirelineRunUid AND ":"") +
								"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
								(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
								(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
								(selectedTools.length()>0?" AND wt.toolCode IN ("+selectedTools+") ":"") +
								(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
								(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
								(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"") +
								"GROUP BY wl.actLogClass";
						wirelineLogClassLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineLogClassQuery, wirelineLogClassparamsFields, wirelineLogClassparamsValues, qp);				
						writer.startElement("tr"); //class header
						writer.addElement("td", "Run " + nullToEmptyString(objRstNumWirelineSuites[4])+" Class Break Down", classHeaderAttributes);
						writer.endElement();// end class header
						
						operationUomContext.setOperationUid(operationUid);
						CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "duration",operationUomContext);
						if(wirelineLogClassLists != null && !wirelineLogClassLists.isEmpty()){
							for(Object objResultwirelineLogClass: wirelineLogClassLists){
								Object[] objRstwirelineLogClass = (Object[]) objResultwirelineLogClass;
								writer.startElement("tr"); //class Items
								String matchingCode="0";
								Double durationValue=0.0;
								if (objRstwirelineLogClass[0] != null) {	
									
									classattributes = new SimpleAttributes();
									classattributes.addAttribute("width", "10");
									classattributes.addAttribute("style", "color:#0000FF");
									
									for (Map.Entry<String, LookupItem> entry : actLogClassCatList.entrySet())
									{
										String catShortCode = entry.getKey();
										String catDescription = nullToEmptyString(entry.getValue().getValue());
										if (catShortCode.equals(objRstwirelineLogClass[0])){
											writer.addElement("td",catDescription, classattributes); //operation
											matchingCode="1";
										}
									}
									if(matchingCode.equals("0")){
										writer.addElement("td",nullToEmptyString(objRstwirelineLogClass[0]),classattributes); 
									}
								}								
								
								if (objRstwirelineLogClass[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objRstwirelineLogClass[1]));
								durationConverter.setBaseValue(durationValue);	
								classattributes = new SimpleAttributes();
								classattributes.addAttribute("colspan", "11");
								classattributes.addAttribute("width", "90");
								classattributes.addAttribute("style", "color:#0000FF");
								writer.addElement("td",durationConverter.formatOutputPrecision(),classattributes); //duration
								
								writer.endElement(); //end class items
							}
						} //Class end
						
						List<WirelineLog> wirelineLogOperationLists;
						String[] wirelineLogOperationparamsFields = {"operationUid","wirelineRunUid"};
						Object[] wirelineLogOperationparamsValues = {operationUid,wirelineRunUid};
						String wirelineLogOperationQuery = "SELECT wl.actLogOp, sum(wl.duration) from WirelineRun wr, WirelineLog wl " +
								(selectedTools.length()>0?", WirelineTool wt ":"") +
								"WHERE wl.operationUid=:operationUid AND wl.wirelineRunUid=:wirelineRunUid AND wr.wirelineRunUid=wl.wirelineRunUid " +
								"AND (wl.isDeleted = false or wl.isDeleted is null) AND " +
								(selectedTools.length()>0?"(wt.isDeleted = false or wt.isDeleted is null) AND wr.wirelineRunUid=wt.wirelineRunUid AND ":"") +
								"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
								(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
								(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
								(selectedTools.length()>0?" AND wt.toolCode IN ("+selectedTools+") ":"") +
								(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
								(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
								(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"") +
								"GROUP BY wl.actLogOp";
						wirelineLogOperationLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineLogOperationQuery, wirelineLogOperationparamsFields, wirelineLogOperationparamsValues, qp);				
						writer.startElement("tr"); //operation header
						
						writer.addElement("td", "Run " + nullToEmptyString(objRstNumWirelineSuites[4])+" Operation Break Down", operationHeaderAttributes);
						writer.endElement();// end operation header
						
						if(wirelineLogOperationLists != null && !wirelineLogOperationLists.isEmpty()){ //operation breakdown start								
							for(Object objResultwirelineLogOperation: wirelineLogOperationLists){
								Object[] objRstwirelineLogOperation = (Object[]) objResultwirelineLogOperation;
								writer.startElement("tr"); //operation Items
								String matchingCode="0";
								Double durationValue=0.0;
								if (objRstwirelineLogOperation[0] != null) {	
									
									operationattributes = new SimpleAttributes();
									operationattributes.addAttribute("width", "10");
									operationattributes.addAttribute("style", "color:#0000FF");
									
									for (Map.Entry<String, LookupItem> entry : actLogOpCatList.entrySet())
									{
										String catShortCode = entry.getKey();
										String catDescription = nullToEmptyString(entry.getValue().getValue());
										if (catShortCode.equals(objRstwirelineLogOperation[0])){
											writer.addElement("td",catDescription, operationattributes); //operation
											matchingCode="1";
										}
									}
									if(matchingCode.equals("0")){
										writer.addElement("td",nullToEmptyString(objRstwirelineLogOperation[0]), operationattributes); 
									}
								}							
								
								if (objRstwirelineLogOperation[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objRstwirelineLogOperation[1]));
								durationConverter.setBaseValue(durationValue);	
								operationattributes = new SimpleAttributes();
								operationattributes.addAttribute("colspan", "11");
								operationattributes.addAttribute("width", "90");
								operationattributes.addAttribute("style", "color:#0000FF");
								writer.addElement("td",durationConverter.formatOutputPrecision(), operationattributes); //duration
								
								writer.endElement(); //end class items
							}
						} //operation breakdown end
					}//wirelinesuite end
					writer.endElement(); // end table
					
					tableattrib = new SimpleAttributes();	
					tableattrib.addAttribute("border","1");
					writer.startElement("table", tableattrib); //start second table
					List<WirelineLog> wirelineLogWellClassLists;
					String[] wirelineLogWellClassparamsFields = {"operationUid"};
					Object[] wirelineLogWellClassparamsValues = {operationUid};
					String wirelineLogWellClassQuery = "SELECT wl.actLogClass, sum(wl.duration) FROM WirelineRun wr, WirelineLog wl " +
							(selectedTools.length()>0?", WirelineTool wt ":"") +
							"WHERE wl.operationUid=:operationUid AND wr.wirelineRunUid=wl.wirelineRunUid AND (wl.isDeleted = false or wl.isDeleted is null) AND " +
							(selectedTools.length()>0? "(wt.isDeleted = false or wt.isDeleted is null) AND wr.wirelineRunUid=wt.wirelineRunUid AND ":"") +
							"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
							(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
							(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
							(selectedTools.length()>0?" AND wt.toolCode IN ("+selectedTools+") ":"") +
							(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
							(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
							(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"") +
							"GROUP BY wl.actLogClass";
					wirelineLogWellClassLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineLogWellClassQuery, wirelineLogWellClassparamsFields, wirelineLogWellClassparamsValues, qp);				
					writer.startElement("tr"); //well Class header
					writer.addElement("td", "Well Class Break Down", wellClassHeaderAttributes);
					writer.endElement();// end well class header
					operationUomContext.setOperationUid(operationUid);
					
					CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), WirelineLog.class, "duration",operationUomContext);
					
					if(wirelineLogWellClassLists != null && !wirelineLogWellClassLists.isEmpty()){ //operation breakdown start								
						for(Object objResultwirelineLogWellClass: wirelineLogWellClassLists){
							Object[] objRstwirelineLogWellClass = (Object[]) objResultwirelineLogWellClass;
							writer.startElement("tr"); //operation Items
							String matchingCode="0";
							Double durationValue=0.0;
							if (objRstwirelineLogWellClass[0] != null) {	
								wellClassattributes = new SimpleAttributes();
								wellClassattributes.addAttribute("width", "10");
								wellClassattributes.addAttribute("style", "color:#0000FF");
								for (Map.Entry<String, LookupItem> entry : actLogClassCatList.entrySet())
								{
									String catShortCode = entry.getKey();
									String catDescription = nullToEmptyString(entry.getValue().getValue());
									if (catShortCode.equals(objRstwirelineLogWellClass[0])){
										writer.addElement("td",catDescription, wellClassattributes); //operation
										matchingCode="1";
									}
									
								}
								if(matchingCode.equals("0")){
									writer.addElement("td",nullToEmptyString(objRstwirelineLogWellClass[0]), wellClassattributes); 
								}
							}								
							
							if (objRstwirelineLogWellClass[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objRstwirelineLogWellClass[1]));
							durationConverter.setBaseValue(durationValue);	
							wellClassattributes = new SimpleAttributes();
							wellClassattributes.addAttribute("colspan", "11");
							wellClassattributes.addAttribute("width", "90");
							wellClassattributes.addAttribute("style", "color:#0000FF");
							writer.addElement("td",durationConverter.formatOutputPrecision(), wellClassattributes); //duration
							writer.endElement(); //end class items
						}
					}
					
					List<WirelineLog> wirelineLogWellOperationLists;
					String[] wirelineLogWellOperationparamsFields = {"operationUid"};
					Object[] wirelineLogWellOperationparamsValues = {operationUid};
					String wirelineLogWellOperationQuery = "SELECT wl.actLogOp, sum(wl.duration) FROM WirelineRun wr, WirelineLog wl " +
							(selectedTools.length()>0?", WirelineTool wt ":"") +
							"WHERE wl.operationUid=:operationUid AND wr.wirelineRunUid=wl.wirelineRunUid AND (wl.isDeleted = false or wl.isDeleted is null) AND " +
							(selectedTools.length()>0? "(wt.isDeleted = false or wt.isDeleted is null) AND wr.wirelineRunUid=wt.wirelineRunUid AND ":"") +
							"(wl.isDeleted = false or wl.isDeleted is null) AND wr.wirelineRunUid=wl.wirelineRunUid " +
							(selectedCompany.length()>0?" AND wr.svcCo IN ("+selectedCompany+") ":"") +
							(selectedConveyanceMethod.length()>0?" AND wr.conveyanceMethod IN ("+selectedConveyanceMethod+") ":"") +
							(selectedTools.length()>0?" AND wt.toolCode IN ("+selectedTools+") ":"") +
							(selectedClass.length()>0?" AND wl.actLogClass IN ("+selectedClass+") ":"") +
							(selectedTask.length()>0?" AND wl.actLogOp IN ("+selectedTask+") ":"") +
							(searchText.length()>0?" AND wl.descr LIKE '%" + searchText +"%' ":"") +
							"GROUP BY wl.actLogOp";
					wirelineLogWellOperationLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(wirelineLogWellOperationQuery, wirelineLogWellOperationparamsFields, wirelineLogWellOperationparamsValues, qp);				
					writer.startElement("tr"); //well task header
					
					writer.addElement("td", "Well Operation Break Down", wellOperationHeaderAttributes);
					writer.endElement();// end well task header
					operationUomContext.setOperationUid(operationUid);
					
					if(wirelineLogWellOperationLists != null && !wirelineLogWellOperationLists.isEmpty()){ //operation breakdown start								
						for(Object objResultwirelineLogWellOperation: wirelineLogWellOperationLists){
							Object[] objRstwirelineLogWellOperation = (Object[]) objResultwirelineLogWellOperation;
							writer.startElement("tr"); //operation Items
							String matchingCode="0";
							Double durationValue=0.0;
							if (objRstwirelineLogWellOperation[0] != null) {	
								wellOperationattributes = new SimpleAttributes();
								wellOperationattributes.addAttribute("style", "color:#0000FF");
								wellOperationattributes.addAttribute("width", "10");
								for (Map.Entry<String, LookupItem> entry : actLogOpCatList.entrySet())
								{
									String catShortCode = entry.getKey();
									String catDescription = nullToEmptyString(entry.getValue().getValue());
									if (catShortCode.equals(objRstwirelineLogWellOperation[0])){
										writer.addElement("td",catDescription, wellOperationattributes); //operation
										matchingCode="1";
									}
									
								}
								if(matchingCode.equals("0")){
									writer.addElement("td",nullToEmptyString(objRstwirelineLogWellOperation[0]), wellOperationattributes); 
								}
							}							
							
							if (objRstwirelineLogWellOperation[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objRstwirelineLogWellOperation[1]));
							durationConverter.setBaseValue(durationValue);	
							wellOperationattributes = new SimpleAttributes();
							wellOperationattributes.addAttribute("colspan", "11");
							wellOperationattributes.addAttribute("width", "90");
							wellOperationattributes.addAttribute("style", "color:#0000FF");
							writer.addElement("td",durationConverter.formatOutputPrecision(), wellOperationattributes); //duration
							writer.endElement(); //end task items
						}
					}
					writer.endElement(); // end second table
				}
			}//operation end
			
			writer.endElement(); // end root
			writer.close();
		
		return bytes.toString("utf-8");
	}
	
	private class LwdRunComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Map<String, Object> in1 = (Map<String, Object>) o1[1];
				Map<String, Object> in2 = (Map<String, Object>) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(nullToEmptyString(in1.get("LwdRun")));
				String f2 = WellNameUtil.getPaddedStr(nullToEmptyString(in2.get("LwdRun")));
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public String queryDayDepthAnalysisRushmoreSL() throws Exception {
		
		String uri = "xml://bitsize?key=code&amp;value=label";
		Map<String, LookupItem> bitSizeLookup = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
		
		List<String> operations = new ArrayList<String>();
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		writer.startElement("root");
		
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		List<String> dayNumberList = new ArrayList<String>();
		Map<String, Object> opsMaps = new LinkedHashMap<String, Object>();
		
		String depthMdMslUom = null;
		Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
		for (String operationUid : operations) {
			
			operationUomContext.setOperationUid(operationUid);
			
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			String strSql = "SELECT rd.operationUid, rd.reportNumber, rd.depthMdMsl, rd.dailyUid from Daily d, ReportDaily rd where rd.dailyUid=d.dailyUid AND (rd.isDeleted = false or rd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rd.operationUid = :operationUid and rd.reportType = :reportType ORDER BY rd.reportDatetime";
			String[] paramsFields = {"operationUid", "reportType"};
			String[] paramsValues = {operationUid, reportType};
			List<Object[]> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, queryProperties);
			Map<String, Object> dailyMaps = new LinkedHashMap<String, Object>();
			if (reportDailyList != null & reportDailyList.size() > 0) {
				
				for (Object[] result : reportDailyList) {
					//Get reportNumber, depthMdMsl from report daily
					Double bitDiameter = 0.0;
					String bitDiameterLabel = null;
					Double depthMdMsl = 0.0;
					if(!dayNumberList.contains(result[1])) dayNumberList.add(nullToEmptyString(result[1]));
					Map<String, Object> dailyMap = new LinkedHashMap<String, Object>();
					CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "depthMdMsl", operationUomContext);
					depthMdMsl = result[2]!= null?Double.parseDouble(nullToEmptyString(result[2])): 0.0;
					thisConverter.setBaseValue(depthMdMsl);
					dailyMap.put("dayNumber", result[1]);
					dailyMap.put("depthMdMsl", thisConverter.formatOutputPrecision());
					depthMdMslUom = thisConverter.getUomSymbol();
					
					//Get bitDiameter
					String sql = "SELECT bt.bitDiameter FROM Bharun b, Bitrun bt WHERE " +
							"b.bharunUid=bt.bharunUid AND (b.isDeleted is null or b.isDeleted=false) " +
							"AND (bt.isDeleted is null or bt.isDeleted=false) " +
							"AND bt.dailyUidOut =:thisDailyUid";
					String[] paramsFields2 = {"thisDailyUid"};
					String[] paramsValues2 = {nullToEmptyString(result[3])};
					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields2, paramsValues2, queryProperties);
					
					if(list.size() > 0) {
						Object a = (Object) list.get(0);
						thisConverter.setReferenceMappingField(Bitrun.class, "bitDiameter");
						if(a != null) bitDiameter = Double.parseDouble(nullToEmptyString(a));
						thisConverter.setBaseValue(bitDiameter);
						
						if(bitSizeLookup.containsKey(thisConverter.formatOutputPrecision())) {
							bitDiameterLabel = nullToEmptyString(bitSizeLookup.get(thisConverter.formatOutputPrecision()).getValue());
						}
					}
					dailyMap.put("bitDiameter", bitDiameterLabel);
					dailyMaps.put(nullToEmptyString(result[1]), dailyMap);
				}
				
			}
			opsMaps.put(operationUid, dailyMaps);
		}
		
		SimpleAttributes attr = new SimpleAttributes();
		attr.addAttribute("border", "1");
		writer.startElement("table", attr);
		writer.startElement("tr");
		attr = new SimpleAttributes();
		attr.addAttribute("nowrap", "nowrap");
		attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;");
		writer.addElement("td", "Well Name", attr);
		for (Map.Entry<String, Object>item : opsMaps.entrySet()) {
			attr = new SimpleAttributes();
			attr.addAttribute("colspan", "2");
			attr.addAttribute("nowrap", "nowrap");
			attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
			writer.addElement("td", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), item.getKey(), null, true), attr);
		}
		writer.endElement();
		
		writer.startElement("tr");
		attr = new SimpleAttributes();
		attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
		writer.addElement("td", "Day", attr);
		for (Map.Entry<String, Object>item : opsMaps.entrySet()) {
			item.getKey();
			attr = new SimpleAttributes();
			attr.addAttribute("style", "background-color:#CEFFFF;font-weight:bold;text-align:center;");
			attr.addAttribute("nowrap", "nowrap");
			writer.addElement("td", "Actual (" + depthMdMslUom +")", attr);
			writer.addElement("td", "Hole Size", attr);
		}
		writer.endElement();
		Collections.sort(dayNumberList, new dayComparator());
		for (String reportNumber : dayNumberList) {
			writer.startElement("tr");
			writer.addElement("td", reportNumber);
			for (Map.Entry<String, Object>item : opsMaps.entrySet()) {
				Map<String, Object> dailyItem = (Map<String, Object>)item.getValue();
				if(dailyItem.containsKey(reportNumber)) {
					Map<String, Object> dailyItem2 = (Map<String, Object>)dailyItem.get(reportNumber);
					attr = new SimpleAttributes();
					attr.addAttribute("style", "text-align:right;");
					attr.addAttribute("nowrap", "nowrap");
					writer.addElement("td", dailyItem2.get("depthMdMsl")!=null?nullToEmptyString(dailyItem2.get("depthMdMsl")):"", attr);
					writer.addElement("td", dailyItem2.get("bitDiameter")!=null?nullToEmptyString(dailyItem2.get("bitDiameter")):"-", attr);
				} else {
					writer.addElement("td", "");
					writer.addElement("td", "");
				}
					
			}
			writer.endElement();
		}
		writer.endElement();
		writer.close();
		return bytes.toString("utf-8");
	}
	
	public String queryLwdWitnessOSL()throws Exception{
		
		List<String>operations = new ArrayList();
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		String selectedCompany = "";
		if (this.getFilter("companyUid") != null) {
			Map<String, String> companyFilters = (Map<String, String>) this.getFilter("companyUid");
			int i = companyFilters.size();
			for (Map.Entry<String, String> entry:companyFilters.entrySet()) {
				if (i==companyFilters.size()) {
					selectedCompany = "'" + entry.getValue() + "'";
				} else {
					selectedCompany += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedHoleSize = "";
		if (this.getFilter("bitDiameter") != null) {
			Map<String, String> holeSizeFilters = (Map<String, String>) this.getFilter("bitDiameter");
			int i = holeSizeFilters.size();
			for (Map.Entry<String, String> entry:holeSizeFilters.entrySet()) {
				if (i==holeSizeFilters.size()) {
					selectedHoleSize = "'" + entry.getValue() + "'";
				} else {
					selectedHoleSize += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedTools = "";
		if (this.getFilter("toolDesc") != null) {
			Map<String, String> toolsFilters = (Map<String, String>) this.getFilter("toolDesc");
			int i = toolsFilters.size();
			for (Map.Entry<String, String> entry:toolsFilters.entrySet()) {
				if (i==toolsFilters.size()) {
					selectedTools = "'" + entry.getValue() + "'";
				} else {
					selectedTools += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedClass = "";
		if (this.getFilter("lwdClassCode") != null) {
			Map<String, String> classFilters = (Map<String, String>) this.getFilter("lwdClassCode");
			int i = classFilters.size();
			for (Map.Entry<String, String> entry:classFilters.entrySet()) {
				if (i==classFilters.size()) {
					selectedClass = "'" + entry.getValue() + "'";
				} else {
					selectedClass += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String selectedTask = "";
		if (this.getFilter("lwdTaskCode") != null) {
			Map<String, String> taskFilters = (Map<String, String>) this.getFilter("lwdTaskCode");
			int i = taskFilters.size();
			for (Map.Entry<String, String> entry:taskFilters.entrySet()) {
				if (i==taskFilters.size()) {
					selectedTask = "'" + entry.getValue() + "'";
				} else {
					selectedTask += ",'" + entry.getValue() + "'";
				}
				i--;
			}
		}
		
		String searchText = nullToEmptyString(this.getFilter("lwdComments"));
		
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			String lookup = "xml://bitsize?key=code&amp;value=label";
			Map<String, LookupItem> holeSizeList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
			
			String companyLookupLabels = "xml://LwdToolLookup.lwdCompany?key=code&amp;value=label";
			Map<String, LookupItem> companyList = LookupManager.getConfiguredInstance().getLookup(companyLookupLabels, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
			
			String actLogOpUri="xml://LwdWitness.act_log_op?key=code&amp;value=label"; 											
			Map<String,LookupItem> actLogOpCatList = LookupManager.getConfiguredInstance().getLookup(actLogOpUri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			
			String actLogClassUri="xml://LwdWitness.act_log_class?key=code&amp;value=label"; 												
			Map<String,LookupItem> actLogClassCatList = LookupManager.getConfiguredInstance().getLookup(actLogClassUri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			
			SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm");
			
			SimpleAttributes operationnameattributes = new SimpleAttributes();
			operationnameattributes.addAttribute("colspan", "12");
			operationnameattributes.addAttribute("style", "font-weight:bold;background-color:#3B9C9C;");
			
			SimpleAttributes runattributes = new SimpleAttributes();
			runattributes.addAttribute("colspan", "12");
			runattributes.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			
			SimpleAttributes HeaderAttributes10 = new SimpleAttributes();
			HeaderAttributes10.addAttribute("width", "10");
			HeaderAttributes10.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes10.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes6 = new SimpleAttributes();
			HeaderAttributes6.addAttribute("width", "6");
			HeaderAttributes6.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes6.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes5 = new SimpleAttributes();
			HeaderAttributes5.addAttribute("width", "5");
			HeaderAttributes5.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes5.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes4 = new SimpleAttributes();
			HeaderAttributes4.addAttribute("width", "4");
			HeaderAttributes4.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes4.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes30 = new SimpleAttributes();
			HeaderAttributes30.addAttribute("width", "30");
			HeaderAttributes30.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes30.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes3 = new SimpleAttributes();
			HeaderAttributes3.addAttribute("width", "3");
			HeaderAttributes3.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes3.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes8 = new SimpleAttributes();
			HeaderAttributes8.addAttribute("width", "8");
			HeaderAttributes8.addAttribute("style", "font-weight:bold;background-color:#CEFFFF;");
			HeaderAttributes8.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDCenterAttributes = new SimpleAttributes();
			TDCenterAttributes.addAttribute("style", "text-align:center;");
			TDCenterAttributes.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDRightAttributes = new SimpleAttributes();
			TDRightAttributes.addAttribute("style", "text-align:right;");
			
			Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
			for (String operationUid : operations) {
				
				Map<String, String> dailyMap = getDailyMap(operationUid);
				
				operationUomContext.setOperationUid(operationUid);
				SimpleAttributes attr = new SimpleAttributes();
				
				String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);

				//LWD Data Query			
				String lwdSuiteQuery = "SELECT DISTINCT ls.operationUid, ls.lwdSuiteUid, ls.suiteNum, ls.companyUid " +
								"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br " +
								(selectedTools.length()>0?", LwdTool lt ":"")+
								"WHERE (ls.isDeleted=false OR ls.isDeleted is null) " +
								"AND (lr.isDeleted=false OR lr.isDeleted is null) " +
								(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
								"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
								"AND (br.isDeleted=false OR br.isDeleted is null) " +
								"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
								"AND lr.lwdUid = lw.lwdUid " +
								"AND lr.bharunUid = br.bharunUid " +
								"AND ls.operationUid = :operationUid " +									
								(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"")+
								(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"")+
								(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"")+
								(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"")+
								(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"")+
								(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"")+
								"ORDER BY ls.suiteNum";
				
				String[] paramsFields = {"operationUid"};
				Object[] paramsValues = {operationUid};
				
				List <Object[]> lwdSuiteList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdSuiteQuery, paramsFields, paramsValues, qp);
				
				CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdWitness.class, "duration", operationUomContext);
				CustomFieldUom mdMslConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), LwdWitness.class, "mdMsl", operationUomContext);
				CustomFieldUom bitDiameterConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "bitDiameter", operationUomContext);
				
				if (lwdSuiteList.size()>0) {
					
					//Draw html table
					attr.addAttribute("style","borderStyle: solid;borderColor:black;");
					attr.addAttribute("border","1");
					attr.addAttribute("width","100%");
					writer.startElement("table", attr);
					
					//Draw Table Header Row 1				
					writer.startElement("tr");
					writer.addElement("td", "Well : " + fullWellName, operationnameattributes);
					writer.endElement();
					
					Collections.sort(lwdSuiteList, new RunNumberComparator());
					for(Object[] objNumberofLwdSuites : lwdSuiteList){
						
						String lwdSuiteUid = nullToEmptyString(objNumberofLwdSuites[1]);
						String runNumber = nullToEmptyString(objNumberofLwdSuites[2]);
						String companyUid = nullToEmptyString(objNumberofLwdSuites[3]);
						
						//Draw Table Header Row 2
						writer.startElement("tr");
						writer.addElement("td","Run " +runNumber+ " Summary", runattributes);
						writer.endElement();
						//Draw Table Header Row 3
						writer.startElement("tr");
						writer.addElement("td","Day", HeaderAttributes6);
						writer.addElement("td","Run #", HeaderAttributes3);
						writer.addElement("td","Company", HeaderAttributes8);
						writer.addElement("td","Tools", HeaderAttributes10);
						writer.addElement("td","Start Time", HeaderAttributes6);
						writer.addElement("td","End Time", HeaderAttributes6);
						writer.addElement("td","Duration ("+durationConverter.getUomSymbol()+")", HeaderAttributes5);
						writer.addElement("td","MD ("+mdMslConverter.getUomSymbol()+")", HeaderAttributes4);
						writer.addElement("td","Hole Size", HeaderAttributes6);
						writer.addElement("td","Class", HeaderAttributes8);
						writer.addElement("td","Operation", HeaderAttributes8);
						writer.addElement("td","Comments", HeaderAttributes30);
						writer.endElement();
						
						List<LwdTool> lwdToolLists;
						String[] lwdParamsFields = {"operationUid", "lwdSuiteUid"};
						Object[] lwdParamsValues = {operationUid, lwdSuiteUid};
												
						String lwdToolQuery = "SELECT lt.lwdToolUid, lt.toolType FROM LwdSuite ls, LwdTool lt WHERE (ls.isDeleted = false or ls.isDeleted is null) AND " +
								"(lt.isDeleted = false or lt.isDeleted is null) AND " +
								"ls.operationUid=:operationUid AND ls.lwdSuiteUid=lt.lwdSuiteUid AND ls.lwdSuiteUid=:lwdSuiteUid";
						
						lwdToolLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdToolQuery, lwdParamsFields, lwdParamsValues);
						String toolType = null;
						if(lwdToolLists != null && lwdToolLists.size() > 0){
							for(Object objResultLwdTool: lwdToolLists){
								Object[] objRstLwdTool = (Object[]) objResultLwdTool;
								if (toolType==null)
								{
									if(objRstLwdTool[1] != null) toolType= nullToEmptyString(objRstLwdTool[1]);
								}
								else{
									if(objRstLwdTool[1] != null) toolType = toolType + ", " + nullToEmptyString(objRstLwdTool[1]);
								}
							}
						}
						
						String lwdquery = "SELECT DISTINCT lr.operationUid, lw.startTime, lw.endTime, lw.duration, lw.mdMsl, br.bitDiameter, " +
										"lw.dailyUid, lw.classCode, lw.taskCode, lw.comments " +
										"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br, Daily d " +
										(selectedTools.length()>0?", LwdTool lt ":"")+
										"WHERE (lr.isDeleted=false OR lr.isDeleted is null) " +
										"AND (ls.isDeleted=false OR ls.isDeleted is null) " +
										"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
										"AND (br.isDeleted=false OR br.isDeleted is null) " +
										"AND (d.isDeleted=false OR d.isDeleted is null) " +
										(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
										"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
										"AND lw.dailyUid = d.dailyUid " +
										"AND lr.lwdUid = lw.lwdUid " +
										"AND lr.bharunUid = br.bharunUid " +
										"AND lr.operationUid = :operationUid " +
										"AND lr.lwdSuiteUid = :lwdSuiteUid " +
										(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"")+
										(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"")+
										(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"")+
										(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"")+
										(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"")+
										(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"")+
										" ORDER BY d.dayDate, lw.startTime";
						
						List <Object[]> lwdLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdquery, lwdParamsFields, lwdParamsValues, qp);
						
						if(lwdLists.size() > 0) {
							for(Object[] objLwd : lwdLists){
								
								writer.startElement("tr");
								if (objLwd[6] != null) {
									String dailyUid = nullToEmptyString(objLwd[6]);
									writer.addElement("td", dailyMap.containsKey(dailyUid)?dailyMap.get(dailyUid):"");
								} else {
									writer.addElement("td", "");
								}
								
								//run number
								writer.addElement("td", runNumber);
								
								//company
								String companyLabel = "";
								if (companyList != null && StringUtils.isNotBlank(companyUid)) {
									LookupItem lookupItem = companyList.get(companyUid);
									if (lookupItem != null) {
										companyLabel = nullToEmptyString(lookupItem.getValue());
									}
									else
										companyLabel = "";
								}
								writer.addElement("td", companyLabel);
								
								//tooltype
								writer.addElement("td", toolType);
								
								//starttime
								if (objLwd[1] != null) {
									Date startTimeValue = (Date) objLwd[1];
									String startTime = timeformat.format(startTimeValue);
									writer.addElement("td", startTime, TDCenterAttributes);//starttime
								} else {
									writer.addElement("td", "");
								}
								
								//endtime
								if (objLwd[2] != null) {
									Date endTimeValue = (Date) objLwd[2];
									String endTime = timeformat.format(endTimeValue);
									writer.addElement("td", endTime.startsWith("23:59")?"24:00":endTime, TDCenterAttributes);//endtime
								} else {
									writer.addElement("td", "");
								}
								
								//duration
								if (objLwd[3] != null) {
									durationConverter.setBaseValue(Double.parseDouble(objLwd[3].toString()));
									writer.addElement("td", durationConverter.formatOutputPrecision(), TDRightAttributes);
								} else {
									writer.addElement("td", "");
								}
								
								//mdmsl
								if (objLwd[4] != null) {
									mdMslConverter.setBaseValue(Double.parseDouble(objLwd[4].toString()));
									writer.addElement("td", mdMslConverter.formatOutputPrecision(), TDRightAttributes);
								} else {
									writer.addElement("td", "");
								}
								
								//holesize
								if (objLwd[5] != null) {
									String holeSize = "";
									bitDiameterConverter.setBaseValue(Double.parseDouble(objLwd[5].toString()));
									if (holeSizeList.containsKey(bitDiameterConverter.formatOutputPrecision())) holeSize = holeSizeList.get(bitDiameterConverter.formatOutputPrecision()).getValue().toString();
									writer.addElement("td", holeSize);
								} else {
									writer.addElement("td", "");
								}
								
								//class
								if (objLwd[7] != null) {
									String className = "";
									className = actLogClassCatList.containsKey(objLwd[7])?nullToEmptyString(actLogClassCatList.get(objLwd[7]).getValue()):objLwd[7].toString();
									writer.addElement("td", className);
								} else {
									writer.addElement("td", "");
								}
								
								//task
								if (objLwd[8] != null) {
									String taskName = "";
									taskName = actLogOpCatList.containsKey(objLwd[8])?nullToEmptyString(actLogOpCatList.get(objLwd[8]).getValue()):objLwd[8].toString();
									writer.addElement("td", taskName);
								} else {
									writer.addElement("td", "");
								}
								
								writer.addElement("td", objLwd[9] != null? nullToEmptyString(objLwd[9]) : ""); //descr
								
								writer.endElement();  //END TR
							}
							
							//RUN CLASS BREAKDOWN
							String[] lwdWitnessParamsFields = {"operationUid", "lwdSuiteUid"};
							Object[] lwdWitnessParamsValues = {operationUid, lwdSuiteUid};
							String lwdWitnessClassQuery = "SELECT lw.classCode, sum(lw.duration) " +
										"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br " +
										(selectedTools.length()>0?", LwdTool lt ":"")+
										"WHERE (lr.isDeleted=false OR lr.isDeleted is null) " +
										"AND (ls.isDeleted=false OR ls.isDeleted is null) " +
										"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
										"AND (br.isDeleted=false OR br.isDeleted is null) " +
										(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
										"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
										"AND lr.lwdUid = lw.lwdUid " +
										"AND lr.bharunUid = br.bharunUid " +
										"AND lr.operationUid = :operationUid " +
										"AND lr.lwdSuiteUid = :lwdSuiteUid " +
										(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"") +
										(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"") +
										(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"") +
										(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"") +
										(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"") +
										(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"") +
										"GROUP BY lw.classCode";
							
							List<Object[]> lwdWitnessClassLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdWitnessClassQuery, lwdWitnessParamsFields, lwdWitnessParamsValues, qp);				
							writer.startElement("tr"); //class header
							SimpleAttributes breakdownAttributes = new SimpleAttributes();
							breakdownAttributes.addAttribute("colspan", "12");
							breakdownAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
							writer.addElement("td", "Run " +runNumber+" Class Break Down", breakdownAttributes);
							writer.endElement();// end class header
							
							if(lwdWitnessClassLists != null && lwdWitnessClassLists.size() >0){
								for(Object[] objResultLwdWitnessClass: lwdWitnessClassLists){
									writer.startElement("tr"); //class Items
									String matchingCode="0";
									Double durationValue=0.0;
									if (objResultLwdWitnessClass[0] != null) {	
										
										breakdownAttributes = new SimpleAttributes();
										breakdownAttributes.addAttribute("width", "10");
										breakdownAttributes.addAttribute("style", "color:#0000FF");
										
										for (Map.Entry<String, LookupItem> entry : actLogClassCatList.entrySet())
										{
											String catShortCode = entry.getKey();
											String catDescription = nullToEmptyString(entry.getValue().getValue());
											if (catShortCode.equals(objResultLwdWitnessClass[0])){
												writer.addElement("td",catDescription, breakdownAttributes); //operation
												matchingCode="1";
											}
										}
										if(matchingCode.equals("0")){
											writer.addElement("td",nullToEmptyString(objResultLwdWitnessClass[0]), breakdownAttributes); 
										}
									}								
									
									if (objResultLwdWitnessClass[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objResultLwdWitnessClass[1]));
									durationConverter.setBaseValue(durationValue);	
									breakdownAttributes = new SimpleAttributes();
									breakdownAttributes.addAttribute("colspan", "11");
									breakdownAttributes.addAttribute("width", "90");
									breakdownAttributes.addAttribute("style", "color:#0000FF");
									writer.addElement("td", durationConverter.formatOutputPrecision(), breakdownAttributes); //duration
									
									writer.endElement(); //end class items
								}
							} //RUN CLASS BREAKDOWN end
							
							//RUN TASK BREAKDOWN
							String lwdWitnessTaskQuery = "SELECT lw.taskCode, sum(lw.duration) " +
										"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br " +
										(selectedTools.length()>0?", LwdTool lt ":"")+
										"WHERE (lr.isDeleted=false OR lr.isDeleted is null) " +
										"AND (ls.isDeleted=false OR ls.isDeleted is null) " +
										"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
										"AND (br.isDeleted=false OR br.isDeleted is null) " +
										(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
										"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
										"AND lr.lwdUid = lw.lwdUid " +
										"AND lr.bharunUid = br.bharunUid " +
										"AND lr.operationUid = :operationUid " +
										"AND lr.lwdSuiteUid = :lwdSuiteUid " +
										(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"")+
										(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"")+
										(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"")+
										(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"")+
										(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"")+
										(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"") +
										"GROUP BY lw.taskCode";
							
							List<Object[]> lwdWitnessTaskLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdWitnessTaskQuery, lwdWitnessParamsFields, lwdWitnessParamsValues, qp);				
							writer.startElement("tr"); //class header
							breakdownAttributes.addAttribute("colspan", "12");
							breakdownAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
							writer.addElement("td", "Run " +runNumber+" Operation Break Down", breakdownAttributes);
							writer.endElement();// end class header
							
							if(lwdWitnessTaskLists != null && lwdWitnessTaskLists.size() >0){
								for(Object[] objResultLwdWitnessTask: lwdWitnessTaskLists){
									writer.startElement("tr"); //class Items
									String matchingCode="0";
									Double durationValue=0.0;
									if (objResultLwdWitnessTask[0] != null) {	
										
										breakdownAttributes = new SimpleAttributes();
										breakdownAttributes.addAttribute("width", "10");
										breakdownAttributes.addAttribute("style", "color:#0000FF");
										
										for (Map.Entry<String, LookupItem> entry : actLogOpCatList.entrySet())
										{
											String catShortCode = entry.getKey();
											String catDescription = nullToEmptyString(entry.getValue().getValue());
											if (catShortCode.equals(objResultLwdWitnessTask[0])){
												writer.addElement("td",catDescription, breakdownAttributes); //operation
												matchingCode="1";
											}
										}
										if(matchingCode.equals("0")){
											writer.addElement("td",nullToEmptyString(objResultLwdWitnessTask[0]), breakdownAttributes); 
										}
									}								
									
									if (objResultLwdWitnessTask[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objResultLwdWitnessTask[1]));
									durationConverter.setBaseValue(durationValue);	
									breakdownAttributes = new SimpleAttributes();
									breakdownAttributes.addAttribute("colspan", "11");
									breakdownAttributes.addAttribute("width", "90");
									breakdownAttributes.addAttribute("style", "color:#0000FF");
									writer.addElement("td", durationConverter.formatOutputPrecision(), breakdownAttributes); //duration
									
									writer.endElement(); //end class items
								}
							} //Run Class Breakdown end
						}
					}
					writer.endElement(); //End Table
					
					attr.addAttribute("border","1");
					writer.startElement("table", attr); //start second table
					String[] lwdWitnessWellClassparamsFields = {"operationUid"};
					Object[] lwdWitnessWellClassparamsValues = {operationUid};
					String lwdWitnessWellClassQuery = "SELECT lw.classCode, sum(lw.duration) " +
										"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br " +
										(selectedTools.length()>0?", LwdTool lt ":"")+
										"WHERE (lr.isDeleted=false OR lr.isDeleted is null) " +
										"AND (ls.isDeleted=false OR ls.isDeleted is null) " +
										"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
										"AND (br.isDeleted=false OR br.isDeleted is null) " +
										(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
										"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
										"AND lr.lwdUid = lw.lwdUid " +
										"AND lr.bharunUid = br.bharunUid " +
										"AND lr.operationUid = :operationUid " +
										(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"") +
										(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"") +
										(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"") +
										(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"") +
										(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"") +
										(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"") +
										"GROUP BY lw.classCode";
					List<Object[]> lwdWitnessWellClassLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdWitnessWellClassQuery, lwdWitnessWellClassparamsFields, lwdWitnessWellClassparamsValues, qp);				
					writer.startElement("tr"); //well Class header
					SimpleAttributes wellBreakdownAttributes = new SimpleAttributes();
					wellBreakdownAttributes.addAttribute("colspan", "12");
					wellBreakdownAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
					writer.addElement("td", "Well Class Break Down", wellBreakdownAttributes);
					writer.endElement();// end well class header
					
					if(lwdWitnessWellClassLists != null && lwdWitnessWellClassLists.size() > 0){ //well class breakdown start								
						for(Object[] objResultLwdWitnessWellClass: lwdWitnessWellClassLists){
							writer.startElement("tr"); //operation Items
							String matchingCode="0";
							Double durationValue=0.0;
							if (objResultLwdWitnessWellClass[0] != null) {	
								wellBreakdownAttributes = new SimpleAttributes();
								wellBreakdownAttributes.addAttribute("width", "10");
								wellBreakdownAttributes.addAttribute("style", "color:#0000FF");
								for (Map.Entry<String, LookupItem> entry : actLogClassCatList.entrySet())
								{
									String catShortCode = entry.getKey();
									String catDescription = nullToEmptyString(entry.getValue().getValue());
									if (catShortCode.equals(objResultLwdWitnessWellClass[0])){
										writer.addElement("td",catDescription, wellBreakdownAttributes); //operation
										matchingCode="1";
									}
									
								}
								if(matchingCode.equals("0")){
									writer.addElement("td",nullToEmptyString(objResultLwdWitnessWellClass[0]), wellBreakdownAttributes); 
								}
							}								
							
							if (objResultLwdWitnessWellClass[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objResultLwdWitnessWellClass[1]));
							durationConverter.setBaseValue(durationValue);	
							wellBreakdownAttributes = new SimpleAttributes();
							wellBreakdownAttributes.addAttribute("colspan", "11");
							wellBreakdownAttributes.addAttribute("width", "90");
							wellBreakdownAttributes.addAttribute("style", "color:#0000FF");
							writer.addElement("td",durationConverter.formatOutputPrecision(), wellBreakdownAttributes); //duration
							writer.endElement(); //end class items
						}
					}
					
					String[] lwdWitnessWellOperationparamsFields = {"operationUid"};
					Object[] lwdWitnessWellOperationparamsValues = {operationUid};
					String lwdWitnessWellOperationQuery = "SELECT lw.taskCode, sum(lw.duration) " +
										"FROM LwdSuite ls, Lwd lr, LwdWitness lw, Bitrun br " +
										(selectedTools.length()>0?", LwdTool lt ":"")+
										"WHERE (lr.isDeleted=false OR lr.isDeleted is null) " +
										"AND (ls.isDeleted=false OR ls.isDeleted is null) " +
										"AND (lw.isDeleted=false OR lw.isDeleted is null) " +
										"AND (br.isDeleted=false OR br.isDeleted is null) " +
										(selectedTools.length()>0?"AND (lt.isDeleted=false OR lt.isDeleted is null) AND ls.lwdSuiteUid = lt.lwdSuiteUid ":"")+
										"AND ls.lwdSuiteUid = lr.lwdSuiteUid " +
										"AND lr.lwdUid = lw.lwdUid " +
										"AND lr.bharunUid = br.bharunUid " +
										"AND lr.operationUid = :operationUid " +
										(selectedCompany.length()>0?" AND ls.companyUid IN (" +selectedCompany+ ") ":"") +
										(selectedHoleSize.length()>0?" AND br.bitDiameter IN (" +selectedHoleSize+ ") ":"") +
										(selectedTools.length()>0?" AND lt.toolDesc IN (" +selectedTools+ ") ":"") +
										(selectedClass.length()>0?" AND lw.classCode IN (" +selectedClass+ ") ":"") +
										(selectedTask.length()>0?" AND lw.taskCode IN (" +selectedTask+ ") ":"") +
										(searchText.length()>0?" AND lw.comments LIKE '%" + searchText +"%' ":"") +
										"GROUP BY lw.taskCode";
					List<Object[]> lwdWitnessWellOperationLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lwdWitnessWellOperationQuery, lwdWitnessWellOperationparamsFields, lwdWitnessWellOperationparamsValues, qp);				
					writer.startElement("tr"); //well task header
					wellBreakdownAttributes.addAttribute("colspan", "12");
					wellBreakdownAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
					writer.addElement("td", "Well Operation Break Down", wellBreakdownAttributes);
					writer.endElement();// end well task header
					operationUomContext.setOperationUid(operationUid);
					
					if(lwdWitnessWellOperationLists != null && lwdWitnessWellOperationLists.size() >0){ //operation breakdown start								
						for(Object[] objResultLwdWitnessWellOperation: lwdWitnessWellOperationLists){
							writer.startElement("tr"); //operation Items
							String matchingCode="0";
							Double durationValue=0.0;
							if (objResultLwdWitnessWellOperation[0] != null) {	
								wellBreakdownAttributes = new SimpleAttributes();
								wellBreakdownAttributes.addAttribute("style", "color:#0000FF");
								wellBreakdownAttributes.addAttribute("width", "10");
								for (Map.Entry<String, LookupItem> entry : actLogOpCatList.entrySet())
								{
									String catShortCode = entry.getKey();
									String catDescription = nullToEmptyString(entry.getValue().getValue());
									if (catShortCode.equals(objResultLwdWitnessWellOperation[0])){
										writer.addElement("td",catDescription, wellBreakdownAttributes); //operation
										matchingCode="1";
									}
									
								}
								if(matchingCode.equals("0")){
									writer.addElement("td",nullToEmptyString(objResultLwdWitnessWellOperation[0]), wellBreakdownAttributes); 
								}
							}
							
							if (objResultLwdWitnessWellOperation[1] != null) durationValue= Double.parseDouble(nullToEmptyString(objResultLwdWitnessWellOperation[1]));
							durationConverter.setBaseValue(durationValue);	
							wellBreakdownAttributes = new SimpleAttributes();
							wellBreakdownAttributes.addAttribute("colspan", "11");
							wellBreakdownAttributes.addAttribute("width", "90");
							wellBreakdownAttributes.addAttribute("style", "color:#0000FF");
							writer.addElement("td",durationConverter.formatOutputPrecision(), wellBreakdownAttributes); //duration
							writer.endElement(); //end task items
						}
					}
					writer.endElement(); // end second table
				}	
			}

			writer.close();
			return bytes.toString("utf-8");
	}

	public String queryActivityCodesBreakdownByOperationOSL()throws Exception{
		
		List<String> operations = new ArrayList<String>();		
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		//Declarations
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);			
		SimpleAttributes tableAttr = new SimpleAttributes();
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		//Variables
		double TotalP = 0.0;
		double TotalTP = 0.0;
		double TotalTU = 0.0;
		double TotalU = 0.0;
		double TotalHours = 0.0;
		
		//Main Container
		Map<String, Object> ActivityDurationList = new HashMap<String, Object>();
		
		
		writer.startElement("root");
		
		//Data Extraction - Begin
		Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
		for (String operationUid : operations) {
			
			//Data Container
			Map<String, Object> ActivityDurationListDetail = new HashMap<String, Object>();
			
			//Declaration
			String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);	
			operationUomContext.setOperationUid(operationUid);
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration",operationUomContext);
			
			double LineTotal = 0.0;
			double OpsTotalP = 0.0;
			double OpsTotalTP = 0.0;
			double OpsTotalTU = 0.0;
			double OpsTotalU = 0.0;
			
			ActivityDurationList.put(operationUid, ActivityDurationListDetail);	
			ActivityDurationListDetail.put("operationName",nullToEmptyString(fullWellName));
			
			//Duration Breakdown 
			String activityQuery = "SELECT a.operationUid,a.internalClassCode,sum(a.activityDuration) AS actDuration, o.operationName " +
					"FROM Activity a, Operation o WHERE (a.isDeleted = false or a.isDeleted is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND (a.isOffline=0 or a.isOffline is null) AND (a.isSimop=0 or a.isSimop is null) " +
					"AND a.operationUid=o.operationUid " +
					"AND o.operationUid=:operationUid " +
					"GROUP BY a.internalClassCode,a.operationUid";		
			List <Object[]> ActivityDurationDetail  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, new String[]{"operationUid"}, new Object[]{operation.getOperationUid()},qp);
			for (Object[] ActivityDurationDetailobj:ActivityDurationDetail){
				
				Double durationValue;
				durationValue= Double.parseDouble(nullToEmptyString(ActivityDurationDetailobj[2]));
					
				if ("P".equals(ActivityDurationDetailobj[1])){
					if (ActivityDurationDetailobj[2] != null){
						durationConverter.setBaseValue(durationValue);
						ActivityDurationListDetail.put("actDurationP", durationConverter.formatOutputPrecision());
						OpsTotalP = OpsTotalP + (durationValue);
					}
				}
				
				if ("TP".equals(ActivityDurationDetailobj[1])){
					if (ActivityDurationDetailobj[2] != null){
						durationConverter.setBaseValue(durationValue);
						ActivityDurationListDetail.put("actDurationTP", durationConverter.formatOutputPrecision());
						OpsTotalTP = OpsTotalTP + durationValue;
					}
				}
				
				if ("U".equals(ActivityDurationDetailobj[1])){
					if (ActivityDurationDetailobj[2] != null){
						durationConverter.setBaseValue(durationValue);
						ActivityDurationListDetail.put("actDurationU", durationConverter.formatOutputPrecision());
						OpsTotalU = OpsTotalU + durationValue;
					}
				}
				
				if ("TU".equals(ActivityDurationDetailobj[1])){
					if (ActivityDurationDetailobj[2] != null){
						durationConverter.setBaseValue(durationValue);
						ActivityDurationListDetail.put("actDurationTU", durationConverter.formatOutputPrecision());
						OpsTotalTU = OpsTotalTU + durationValue;
					}
				}
				ActivityDurationList.put(operationUid, ActivityDurationListDetail);	
			}
			LineTotal = OpsTotalP + OpsTotalTP + OpsTotalU + OpsTotalTU;
			TotalP = TotalP + OpsTotalP;
			TotalTP = TotalTP + OpsTotalTP;
			TotalTU = TotalTU + OpsTotalU;
			TotalU = TotalU + OpsTotalTU;
			TotalHours = TotalHours + LineTotal;
			durationConverter.setBaseValue(LineTotal);
			ActivityDurationListDetail.put("LineTotal", durationConverter.formatOutputPrecision());		
		}
		
		//Data Container
		Map<String, Object> ActivityDurationSummary = new HashMap<String, Object>();
		CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration",operationUomContext);
		durationConverter.setBaseValue(TotalP);
		ActivityDurationSummary.put("TotalP", durationConverter.formatOutputPrecision());
		durationConverter.setBaseValue(TotalTP);
		ActivityDurationSummary.put("TotalTP", durationConverter.formatOutputPrecision());
		durationConverter.setBaseValue(TotalU);
		ActivityDurationSummary.put("TotalTU", durationConverter.formatOutputPrecision());
		durationConverter.setBaseValue(TotalTU);
		ActivityDurationSummary.put("TotalU", durationConverter.formatOutputPrecision());
		durationConverter.setBaseValue(TotalHours);
		ActivityDurationSummary.put("TotalHours", durationConverter.formatOutputPrecision());	
		
		//Data Extraction - End
		
		//Draw HTML table
		tableAttr.addAttribute("style","borderStyle: solid;borderColor:black;");
		tableAttr.addAttribute("border","1");
		tableAttr.addAttribute("width","100%");
		writer.startElement("table", tableAttr);
		//Draw Table Header Row 1
		writer.startElement("tr");
		writer.addElement("td", "Codes");
		writer.addElement("td", "P (hrs)");
		writer.addElement("td", "TP (hrs)");
		writer.addElement("td", "U (hrs)");
		writer.addElement("td", "TU (hrs)");
		writer.addElement("td", "Line Total");
		writer.endElement();
		//Draw Table Header Row 2
		writer.startElement("tr");
		writer.addElement("td", "Code Description");
		writer.addElement("td", "PROGRAMMED EVENT");
		writer.addElement("td", "TROUBLE - DURING PROGRAM");
		writer.addElement("td", "UNPROGRAMMED EVENT");
		writer.addElement("td", "TROUBLE - DURING UNPROG");
		writer.addElement("td", "");
		writer.endElement();
		
		
		//Table Result
		//Retrieve all Data Containers 1 into Array
		List<Object[]> mapList = new ArrayList();
		for (Map.Entry<String, Object>item : ActivityDurationList.entrySet()) {
			mapList.add(new Object[] {item.getKey(), item.getValue()});
		}
		
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			writer.startElement("tr");
			Object[] obj_array = (Object[]) i.next();
			Map<String, Object> ActivityDurationDetail = (Map<String, Object>)obj_array[1];
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("operationName")));
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("actDurationP")));
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("actDurationTP")));
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("actDurationU")));
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("actDurationTU")));
			writer.addElement("td", nullToEmptyString(ActivityDurationDetail.get("LineTotal")));
			writer.endElement();
		}
		
		writer.startElement("tr");
		writer.addElement("td","Total Duration");
		writer.addElement("td", nullToEmptyString(ActivityDurationSummary.get("TotalP")));
		writer.addElement("td", nullToEmptyString(ActivityDurationSummary.get("TotalTP")));
		writer.addElement("td", nullToEmptyString(ActivityDurationSummary.get("TotalU")));
		writer.addElement("td", nullToEmptyString(ActivityDurationSummary.get("TotalTU")));
		writer.addElement("td", nullToEmptyString(ActivityDurationSummary.get("TotalHours")));
		writer.endElement();
	
		writer.endElement();
		
	writer.close();
	return bytes.toString("utf-8");	
	}
	
	public String queryBitByHoleSectionOSL() throws Exception{
		
		List<String> operations = new ArrayList<String>();		
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);			
		writer.startElement("root"); //start root
		if (allOperations.size() > 0) {
			for (Map.Entry<String, String> entry:allOperations.entrySet()) {
				operations.add(entry.getValue());
			}
				
			String operationsuidlist = "";				
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			for (String operationUid : operations) {
				if(operationsuidlist=="")
					operationsuidlist= "'" + operationUid + "'";
				else
					operationsuidlist+=",'" + operationUid + "'";
			}
			
			String bitRunDiameterQuery = "SELECT distinct br.bitDiameter FROM Bitrun br WHERE (br.isDeleted = false or br.isDeleted is null) AND br.operationUid IN("+operationsuidlist+") ORDER BY br.bitDiameter DESC";
			List <Double> bitRunDiameter   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(bitRunDiameterQuery,qp);	
			
			Map<String, Object> bitRunDiameterLists = new LinkedHashMap<String, Object>();
			for (Double result : bitRunDiameter) {// holesize start
				if (result!=null)
				{	
					DecimalFormat Decimalformat = new DecimalFormat("#0.000000");
					String bitDiameter=CommonUtils.roundUpFormat(Decimalformat, Double.parseDouble(nullToEmptyString(result)));
					
					Map<String, Object> OperationUidMap = new LinkedHashMap<String, Object>();
					for (String operationUid : operations) {
							String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
							
							List<String> bitrunUidList;
							String[] paramsFields = {"operationUid","bitDiameter"};
							Object[] paramsValues = {operationUid,Double.parseDouble(bitDiameter)};
							
							String bitRunQuery = "SELECT distinct br.bitrunUid from Bitrun br, Bharun bha, BharunDailySummary bds " +
									"WHERE (br.isDeleted = false or br.isDeleted is null) AND (bha.isDeleted = false or bha.isDeleted is null) AND (bds.isDeleted = false or bds.isDeleted is null) " +
									"AND bds.bharunUid = bha.bharunUid AND bds.bharunUid = br.bharunUid AND br.operationUid=:operationUid AND br.bitDiameter=:bitDiameter AND br.bharunUid=bha.bharunUid " +
									"ORDER BY br.depthInMdMsl,br.depthOutMdMsl";
							bitrunUidList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitRunQuery, paramsFields, paramsValues, qp);		
							Map<String, Object> bitrunUidMap = new LinkedHashMap<String, Object>();
							
							if(bitrunUidList.size()>0){
								for(String bitrunUid : bitrunUidList){
									
									List<String> bhadailyList;
									String[] bhadailyparamsFields = {"operationUid","bitrunUid","reportType"};
									Object[] bhadailyparamsValues = {operationUid,bitrunUid,reportType};
									
									String bhadailyQuery = "SELECT distinct bds.dailyUid from Bitrun br, BharunDailySummary bds, Daily d, ReportDaily rd " +
											"WHERE (d.dailyUid=bds.dailyUid) AND (rd.dailyUid=bds.dailyUid) AND (d.dailyUid=rd.dailyUid) " +
											"AND (rd.reportType=:reportType) AND (d.isDeleted is null or d.isDeleted=false) AND (bds.isDeleted = false or bds.isDeleted is null) " +
											"AND (rd.isDeleted is null or rd.isDeleted=false) AND (br.isDeleted = false or br.isDeleted is null) " +
											"AND br.operationUid=:operationUid " +
											"AND br.bitrunUid=:bitrunUid AND br.bharunUid=bds.bharunUid ORDER BY rd.reportDatetime";
									bhadailyList   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bhadailyQuery, bhadailyparamsFields, bhadailyparamsValues, qp);		
									Map<String, Object> DailyUidMap = new LinkedHashMap<String, Object>();
									if(bhadailyList.size()>0){ //bhadaily 							
										for(String dailyUid : bhadailyList){
											List<String> activityList;
											String[] ActivityparamsFields = {"operationUid","dailyUid"};
											Object[] ActivityparamsValues = {operationUid,dailyUid};
											
											String ActivityQuery = "SELECT a.activityUid from Activity a WHERE (a.isDeleted = false or a.isDeleted is null) " +
													"AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) AND (isOffline IS NULL OR isOffline=false) " +
													"AND a.operationUid=:operationUid AND a.dailyUid=:dailyUid ORDER BY a.startDatetime";
											activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ActivityQuery, ActivityparamsFields, ActivityparamsValues, qp);		
											
											List<String> arrayActivityList = new ArrayList<String>();
											if(activityList.size() > 0) {
												for(String activityUid: activityList){
													arrayActivityList.add(activityUid);
												}
												DailyUidMap.put(dailyUid,arrayActivityList);
											}
											
										}
										if (DailyUidMap.size() > 0) bitrunUidMap.put(bitrunUid,DailyUidMap);
									}//bhadaily
								}
								if (bitrunUidMap.size() > 0) OperationUidMap.put(operationUid,bitrunUidMap);
							}
					}//operation end
					bitRunDiameterLists.put(nullToEmptyString(bitDiameter), OperationUidMap);
				}//end if holesize not null
			}// hole size end
			
			SimpleAttributes tableBorderAttrib = new SimpleAttributes();
			tableBorderAttrib = new SimpleAttributes();
			tableBorderAttrib.addAttribute("border","1");
			
			SimpleAttributes BitTableAttrib = new SimpleAttributes();
			BitTableAttrib.addAttribute("border","0");
			
			SimpleAttributes HoleSizeattributes = new SimpleAttributes();
			HoleSizeattributes.addAttribute("colspan", "3");
			HoleSizeattributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			
			SimpleAttributes HeaderAttributes = new SimpleAttributes();
			HeaderAttributes.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes HeaderAttributes10 = new SimpleAttributes();
			HeaderAttributes10.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes10.addAttribute("nowrap", "nowrap");
			HeaderAttributes10.addAttribute("width", "10");
			
			SimpleAttributes HeaderAttributes80 = new SimpleAttributes();
			HeaderAttributes80.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes80.addAttribute("nowrap", "nowrap");
			HeaderAttributes80.addAttribute("width", "80");
			
			SimpleAttributes HeaderAttributes5 = new SimpleAttributes();
			HeaderAttributes5.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes5.addAttribute("nowrap", "nowrap");
			HeaderAttributes5.addAttribute("width", "5");
			
			SimpleAttributes HeaderAttributes4 = new SimpleAttributes();
			HeaderAttributes4.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes4.addAttribute("nowrap", "nowrap");
			HeaderAttributes4.addAttribute("width", "4");
			
			SimpleAttributes HeaderAttributes6 = new SimpleAttributes();
			HeaderAttributes6.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes6.addAttribute("nowrap", "nowrap");
			HeaderAttributes6.addAttribute("width", "6");
			
			SimpleAttributes HeaderAttributes7 = new SimpleAttributes();
			HeaderAttributes7.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes7.addAttribute("nowrap", "nowrap");
			HeaderAttributes7.addAttribute("width", "7");
			
			SimpleAttributes HeaderAttributes75 = new SimpleAttributes();
			HeaderAttributes75.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes75.addAttribute("nowrap", "nowrap");
			HeaderAttributes75.addAttribute("width", "75");
			
			SimpleAttributes HeaderAttributes85 = new SimpleAttributes();
			HeaderAttributes85.addAttribute("style", "font-weight:bold;background-color:#C0C0C0;");
			HeaderAttributes85.addAttribute("nowrap", "nowrap");
			HeaderAttributes85.addAttribute("width", "85");
			
			SimpleAttributes TDNOWRAP = new SimpleAttributes();
			TDNOWRAP.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes TDNOWRAP100 = new SimpleAttributes();
			TDNOWRAP100.addAttribute("nowrap", "nowrap");
			TDNOWRAP100.addAttribute("width", "100");
			
			SimpleAttributes TDNOWRAP10 = new SimpleAttributes();
			TDNOWRAP10.addAttribute("nowrap", "nowrap");
			TDNOWRAP10.addAttribute("width", "10");
			
			SimpleAttributes TDNOWRAP85 = new SimpleAttributes();
			TDNOWRAP85.addAttribute("nowrap", "nowrap");
			TDNOWRAP85.addAttribute("width", "85");
			
			SimpleAttributes tdRightAlign = new SimpleAttributes();
			tdRightAlign.addAttribute("style", "text-align:right;");
			tdRightAlign.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes tdRightAlign5 = new SimpleAttributes();
			tdRightAlign5.addAttribute("style", "text-align:right;");
			tdRightAlign5.addAttribute("nowrap", "nowrap");
			tdRightAlign5.addAttribute("width", "5");
			
			SimpleAttributes tdCenterAlign5 = new SimpleAttributes();
			tdCenterAlign5.addAttribute("style", "text-align:center;");
			tdCenterAlign5.addAttribute("nowrap", "nowrap");
			tdCenterAlign5.addAttribute("width", "5");
			
			SimpleAttributes tdRightAlign10 = new SimpleAttributes();
			tdRightAlign10.addAttribute("style", "text-align:right;");
			tdRightAlign10.addAttribute("nowrap", "nowrap");
			tdRightAlign10.addAttribute("width", "10");
			
			SimpleAttributes tdRightAlign85 = new SimpleAttributes();
			tdRightAlign85.addAttribute("style", "text-align:right;");
			tdRightAlign85.addAttribute("nowrap", "nowrap");
			tdRightAlign85.addAttribute("width", "85");
			
			SimpleAttributes tdLeftAlign = new SimpleAttributes();
			tdLeftAlign.addAttribute("style", "text-align:left;");
			tdLeftAlign.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes tdCenterAlign = new SimpleAttributes();
			tdCenterAlign.addAttribute("style", "text-align:center;");
			tdCenterAlign.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes codesBold = new SimpleAttributes();
			codesBold.addAttribute("style", "font-weight:bold;");
			codesBold.addAttribute("nowrap", "nowrap");
			
			SimpleAttributes description75 = new SimpleAttributes();
			description75.addAttribute("width", "75");
			
			int counter=0;
			String bitDiameter="";
			Double countTotalDuration=0.0;
			
			for (Map.Entry<String, Object>entryBitDiameter : bitRunDiameterLists.entrySet()) {// key = diameter
				String lookup="xml://bitsize?key=code&amp;value=label"; 												
				Map<String,LookupItem> catList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
				for (Map.Entry<String, LookupItem> entry : catList.entrySet())
				{
					String catShortCode = entry.getKey();
					String catDescription = nullToEmptyString(entry.getValue().getValue());
					if (catShortCode.equals(entryBitDiameter.getKey())){
						bitDiameter=catDescription;
					}
				}
				counter=counter+1;
				writer.startElement("table", tableBorderAttrib); //start table
					writer.startElement("tr"); //tr1
						writer.addElement("td", bitDiameter,HoleSizeattributes);
					writer.endElement(); //end tr1
				writer.startElement("tr"); //Top Header
					writer.addElement("td", "Well",HeaderAttributes10);
					writer.addElement("td", " ",HeaderAttributes85);
					writer.addElement("td", "Total Duration",HeaderAttributes5);
				writer.endElement(); //End Top Header
				Map<String, Object> OperationMapList = new LinkedHashMap<String, Object>();
				OperationMapList=(Map<String, Object>) entryBitDiameter.getValue();
				for (Map.Entry<String, Object>entryOperation : OperationMapList.entrySet()) //key = operation
				{					
					countTotalDuration=0.0;
					String operationUid=entryOperation.getKey();
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
					operationUomContext.setOperationUid(operationUid);
					
					CustomFieldUom depthInConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "depthInMdMsl",operationUomContext);
					CustomFieldUom depthOutConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Bitrun.class, "depthOutMdMsl",operationUomContext);
					CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration",operationUomContext);
					CustomFieldUom depthMdMslConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "depthMdMsl",operationUomContext);
					
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
					writer.startElement("tr");//start tr2
					String FullWellName=CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
					writer.addElement("td", FullWellName, TDNOWRAP10);
					writer.startElement("td", TDNOWRAP85); //start TD1
					
					Map<String, Object> BitMapList = new LinkedHashMap<String, Object>();
					BitMapList=(Map<String, Object>) entryOperation.getValue();
					entryOperation.getKey();
					
					writer.startElement("table",BitTableAttrib); //table 1
					for (Map.Entry<String, Object>entryBit : BitMapList.entrySet()) // key=Bit
					{
						writer.startElement("tr");//tr5
						writer.startElement("td",TDNOWRAP100);//td2
						writer.startElement("table", tableBorderAttrib);//table bit
						writer.startElement("tr");//tr3
						writer.addElement("td", "Bit #",HeaderAttributes5);
						writer.addElement("td", "Depth In (" + depthInConverter.getUomSymbol() + ")",HeaderAttributes5);
						writer.addElement("td", "Depth Out (" + depthOutConverter.getUomSymbol() + ")",HeaderAttributes5);
						writer.addElement("td", "Detail",HeaderAttributes85);		
						writer.endElement(); //end tr3
						writer.startElement("tr");//tr4
						String bitrunUid=entryBit.getKey();
						
						List<Bitrun> bitrunNumberLists;
						String[] bitrunNumberparamsFields = {"operationUid","bitrunUid"};
						Object[] bitrunNumberparamsValues = {operationUid,bitrunUid};
						String bitrunNumberQuery = "SELECT br.operationUid, br.bitrunNumber,br.depthInMdMsl,br.depthOutMdMsl FROM Bitrun br " +
								"WHERE (br.isDeleted = false or br.isDeleted is null) AND br.operationUid=:operationUid AND br.bitrunUid=:bitrunUid";
						bitrunNumberLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitrunNumberQuery, bitrunNumberparamsFields, bitrunNumberparamsValues, qp);
						
						for(Object objResultbitrunRecords: bitrunNumberLists){
							Object[] objResultbitrunRecord = (Object[]) objResultbitrunRecords;
							Double bitdepthInMdMsl =0.0;
							Double bitdepthOutMdMsl=0.0;
							String bitNumber="";
							if (objResultbitrunRecord[1]!=null) bitNumber = nullToEmptyString(objResultbitrunRecord[1]);
							if (objResultbitrunRecord[2]!=null)	bitdepthInMdMsl = Double.parseDouble(nullToEmptyString(objResultbitrunRecord[2]));
							if (objResultbitrunRecord[3]!=null) bitdepthOutMdMsl = Double.parseDouble(nullToEmptyString(objResultbitrunRecord[3]));
							
							writer.addElement("td", bitNumber,tdCenterAlign5);
							depthInConverter.setBaseValue(bitdepthInMdMsl);
							writer.addElement("td",depthInConverter.formatOutputPrecision(),tdRightAlign5);
							depthOutConverter.setBaseValue(bitdepthOutMdMsl);
							writer.addElement("td",depthOutConverter.formatOutputPrecision(),tdRightAlign5);
						}
						
						
						Map<String, Object> DailyMapList = new LinkedHashMap<String, Object>();
						DailyMapList=(Map<String, Object>) entryBit.getValue();
						entryBit.getKey();
						writer.startElement("td",tdRightAlign85);//td3
						//Start Activity Record
						writer.startElement("table", tableBorderAttrib); //table 2
						
						for (Map.Entry<String, Object>entryDaily : DailyMapList.entrySet()) //key= daily
						{
							String dailyUid=entryDaily.getKey();
							
							List<ReportDaily> reportDailyLists;
							String[] reportDailyparamsFields = {"operationUid","dailyUid","reportType"};
							Object[] reportDailyparamsValues = {operationUid,dailyUid,reportType};
							String reportDailyQuery = "SELECT d.dayDate,d.dayNumber FROM Daily d, ReportDaily rd " +
									"WHERE d.dailyUid = rd.dailyUid AND (d.isDeleted = false or d.isDeleted is null) " +
									"AND (rd.isDeleted = false or rd.isDeleted is null) AND d.operationUid=:operationUid " +
									"AND d.dailyUid=:dailyUid AND rd.reportType=:reportType";
							reportDailyLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailyQuery, reportDailyparamsFields, reportDailyparamsValues, qp);
							
							writer.startElement("tr");//tr 10
							writer.startElement("td",TDNOWRAP100);// td 6
							writer.startElement("table", tableBorderAttrib);//table day
							writer.startElement("tr"); //tr6
							writer.addElement("td", "Day #",HeaderAttributes4);
							writer.addElement("td", "Date",HeaderAttributes5);
							writer.addElement("td", "Duration (" + durationConverter.getUomSymbol() + ")",HeaderAttributes6);
							writer.addElement("td", "Detail",HeaderAttributes85);
							writer.endElement(); //tr6
							
							for(Object objResultreportDailyRecords: reportDailyLists){
								Object[] objResultreportDailyRecord = (Object[]) objResultreportDailyRecords;
								Date dayDate=(Date) objResultreportDailyRecord[0];
								String dayNumber = nullToEmptyString(objResultreportDailyRecord[1]);
								writer.startElement("tr");//tr7
								writer.addElement("td", dayNumber,tdCenterAlign5);
								writer.addElement("td", df.format(dayDate),tdCenterAlign5);
								List<String> ActivityList = new ArrayList<String>();							
								ActivityList=(List<String>) entryDaily.getValue();
								String activityUidList="";
								if(ActivityList.size() > 0) {
									for(String activityUid: ActivityList){
										if(activityUidList=="")
											activityUidList= "'" + activityUid + "'";
										else
											activityUidList+=",'" + activityUid + "'";
									}
									String activityQuery = "SELECT sum(a.activityDuration) as duration FROM Activity a " +
											"WHERE (a.isDeleted = false or a.isDeleted is null) AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) " +
											"AND (isOffline IS NULL OR isOffline=false)AND a.activityUid IN ("+activityUidList+")";
									List <Double> activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(activityQuery,qp);	
									for (Double activityresult : activityLists) {
										Double activityDuration=0.0;
										activityDuration=Double.parseDouble(nullToEmptyString(activityresult));
										durationConverter.setBaseValue(activityDuration);	
										writer.addElement("td", durationConverter.formatOutputPrecision(),tdRightAlign5);
										countTotalDuration =countTotalDuration+Double.parseDouble(durationConverter.formatOutputPrecision());
									}
								}
								writer.startElement("td",tdRightAlign85);//td4
							}
							List<String> ActivityList = new ArrayList<String>();							
							ActivityList=(List<String>) entryDaily.getValue();
							String activityUidList="";
							if(ActivityList.size() > 0) {
								for(String activityUid: ActivityList){
									if(activityUidList=="")
										activityUidList= "'" + activityUid + "'";
									else
										activityUidList+=",'" + activityUid + "'";
								}
								
								String ActivityQuery = "SELECT a.operationUid, a.startDatetime, a.endDatetime, a.classCode, a.phaseCode, a.jobTypeCode, a.taskCode, a.rootCauseCode, a.depthMdMsl, a.activityDuration, a.activityDescription " +
										"FROM Activity a WHERE (a.isDeleted = false or a.isDeleted is null) AND dayPlus=0 AND (isSimop IS NULL OR isSimop=false) " +
										"AND (isOffline IS NULL OR isOffline=false) AND a.activityUid IN ("+activityUidList+") ORDER BY a.startDatetime";
								List<Object[]> ActivityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().find(ActivityQuery, qp);
								writer.startElement("table", tableBorderAttrib);//table activity
								writer.startElement("tr");//tr8
								writer.addElement("td", "Start",HeaderAttributes4);
								writer.addElement("td", "End",HeaderAttributes4);
								writer.addElement("td", "Codes",HeaderAttributes5);
								writer.addElement("td", "Depth (" + depthMdMslConverter.getUomSymbol() + ")",HeaderAttributes5);
								writer.addElement("td", "Duration (" + durationConverter.getUomSymbol() + ")",HeaderAttributes7);
								writer.addElement("td", "Description",HeaderAttributes75);
								writer.endElement();//tr8
								
								for(Object objResultActivityRecords: ActivityLists){
									Object[] objResultActivityRecord = (Object[]) objResultActivityRecords;
									Date startDatetime=null;
									Date endDatetime=null;
									String classCode="";
									String phaseCode="";
									String taskCode="";
									String rootCauseCode="";
									Double depthMdMsl=0.0;
									Double activityDuration=0.0;
									String activityDescription="";
									
									if (objResultActivityRecord[1]!=null) startDatetime=(Date) objResultActivityRecord[1];
									if (objResultActivityRecord[2]!=null) endDatetime=(Date) objResultActivityRecord[2];
									if (objResultActivityRecord[3]!=null) classCode=nullToEmptyString(objResultActivityRecord[3]);
									if (objResultActivityRecord[4]!=null) phaseCode=nullToEmptyString(objResultActivityRecord[4]);
									if (objResultActivityRecord[6]!=null) taskCode=nullToEmptyString(objResultActivityRecord[6]);
									if (objResultActivityRecord[7]!=null) rootCauseCode=nullToEmptyString(objResultActivityRecord[7]);
									if (objResultActivityRecord[8]!=null) depthMdMsl=Double.parseDouble(nullToEmptyString(objResultActivityRecord[8]));
									if (objResultActivityRecord[9]!=null) activityDuration=Double.parseDouble(nullToEmptyString(objResultActivityRecord[9]));
									if (objResultActivityRecord[10]!=null) activityDescription=nullToEmptyString(objResultActivityRecord[10]);
									writer.startElement("tr");//tr9
									String startTime = tf.format(startDatetime);
									writer.addElement("td", startTime, tdCenterAlign5);
									String endTime = tf.format(endDatetime);
									if (endTime.startsWith("00:00") || endTime.startsWith("23:59")) endTime = "24:00";
									writer.addElement("td", endTime, tdCenterAlign5);
									//Codes
									writer.startElement("td",tdRightAlign5);
										writer.startElement("table");
											writer.startElement("tr");
												writer.addElement("td", "Cls:", TDNOWRAP);
												writer.addElement("td", classCode, codesBold);
											writer.endElement();
											writer.startElement("tr");
												writer.addElement("td", "Phs:", TDNOWRAP);
												writer.addElement("td", phaseCode, codesBold);
											writer.endElement();
											writer.startElement("tr");
												writer.addElement("td", "Ops:", TDNOWRAP);
												writer.addElement("td", taskCode,codesBold);
											writer.endElement();
											writer.startElement("tr");
												writer.addElement("td", "RC:", TDNOWRAP);
												writer.addElement("td", rootCauseCode, codesBold);
											writer.endElement();
										writer.endElement();
									writer.endElement();
									//End Codes
									depthMdMslConverter.setBaseValue(depthMdMsl);
									writer.addElement("td", depthMdMslConverter.formatOutputPrecision(), tdRightAlign5);
									durationConverter.setBaseValue(activityDuration);
									writer.addElement("td", durationConverter.formatOutputPrecision(), tdRightAlign5);
									writer.addElement("td", activityDescription, description75);
									writer.endElement();//tr9
								}
								writer.endElement();//end table activity
							}
							writer.endElement();//td4
							writer.endElement();//tr7
							writer.endElement(); //end table day
							writer.endElement(); //td 6
							writer.endElement(); //tr 10
						}//key= daily
						
						writer.endElement(); //end table 2 //END Activity Record
						writer.endElement();//td3
						writer.endElement();//tr4
						writer.endElement();//end table bit
						writer.endElement();//tr5
						writer.endElement();//td2
						
					}// key=Bit
					writer.endElement(); // table 1
					writer.endElement(); //end TD1
					writer.addElement("td",nullToEmptyString(countTotalDuration), tdRightAlign5);
					writer.endElement();//end tr2
				} //key = operation 
				writer.endElement();// end table
				
			}// key = diameter
		}
		writer.endElement(); // end root
		writer.close();
		return bytes.toString("utf-8");
	}
	
	/**	
	 * Customised query for OSL.
	 * Query on Actual Daily Time Breakdown for Activity.
	 * @return result in HTML.
	 */
	public String queryForActivityCodesDailyActualTimeBreakdownForOSL()throws Exception {

		//Declarations
		SimpleAttributes tableAttr = new SimpleAttributes();
		tableAttr.addAttribute("style", "borderStyle:solid;borderColor:blue;font-weight:bold;background-color:#C0C0C0;");
		tableAttr.addAttribute("border", "1");
		tableAttr.addAttribute("width", "100%");
		
		SimpleAttributes tdHeaderAttr = new SimpleAttributes();
		tdHeaderAttr.addAttribute("colspan", "10");
		tdHeaderAttr.addAttribute("style", "text-align:center;font-weight:bold;background-color:#9AFEFF;");

		SimpleAttributes tdSubHeaderAttr = new SimpleAttributes();
		tdSubHeaderAttr.addAttribute("style", "text-align:center;font-weight:bold;background-color:#9AFEFF;");
		tdSubHeaderAttr.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes tdAttr = new SimpleAttributes();
		tdAttr.addAttribute("nowrap", "nowrap");

		SimpleAttributes tdRightAttr = new SimpleAttributes();
		tdAttr.addAttribute("nowrap", "nowrap");
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		
		writer.startElement("root");
		writer.startElement("table", tableAttr); //<table>		
		//Get user selected operation(s)
		Map<String, String> OperationMap = (Map<String, String>) this.getFilter("operationUid");
		List<String> OperationList = new ArrayList<String>(OperationMap.values());
		Collections.sort(OperationList, new operationComparator(this.getCurrentUserSession()));
				
		//Loop by operationUid
		for (String operationUid: OperationList) {
			
			//Get result set
			String strSql = "SELECT a.operationUid, w.wellName, o.operationName, wb.wellboreType, " +
								"rd.reportNumber, rd.depthMdMsl, a.phaseCode, a.classCode, " +
								"SUM(a.activityDuration), a.rootCauseCode, a.dailyUid, o.operationCode " +
							"FROM Activity a, Well w, Operation o, Wellbore wb, ReportDaily rd " +
							"WHERE a.wellUid=w.wellUid AND a.operationUid=o.operationUid AND " +
								"a.wellboreUid=wb.wellboreUid AND a.dailyUid=rd.dailyUid AND " +
								"(a.isDeleted IS NULL OR a.isDeleted = false) AND (a.carriedForwardActivityUid='' OR a.carriedForwardActivityUid IS NULL) AND " +
								"(a.isOffline=0 OR a.isOffline IS NULL) AND (a.isSimop=0 OR a.isSimop IS NULL) AND " +
								"rd.reportType='DDR' AND a.operationUid=:operationUid " +
							"GROUP BY a.operationUid, w.wellName, o.operationName, wb.wellboreType, " +
								"rd.reportNumber, rd.depthMdMsl, a.phaseCode, a.classCode, " +
								"a.dailyUid " +
							"ORDER BY a.startDatetime";
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			
			List<Object[]> ActivityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"operationUid"}, new Object[]{operationUid}, qp);
			
			//Get report# when it has more than one phase code
			Map<String, String> dailyUidDiffPhase = new HashMap<String, String>();
			String reportNoValue = "";
			String phaseValue = "";
			String reportNoValueTmp = "";
			String phaseValueTmp = "";
	
			for (Object[] arrObj: ActivityList) {
				reportNoValue = nullToEmptyString(arrObj[4]); //Day
				phaseValue = nullToEmptyString(arrObj[6]); //Phase
				if (reportNoValueTmp.equals(reportNoValue)) {
					if (!phaseValueTmp.equals(phaseValue)) {
						dailyUidDiffPhase.put(nullToEmptyString(arrObj[10]), null); //Stores dailyUid
					}
				}
				reportNoValueTmp = reportNoValue;
				phaseValueTmp = phaseValue;
			}
		
			operationUomContext.setOperationUid(operationUid);
			String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
			if (ActivityList.size()>0) {
				//Header section
				writer.startElement("tr"); //<tr>
				writer.addElement("td", fullWellName, tdHeaderAttr); //Operation/Sidetrack#
				writer.endElement(); //<tr>
			
				//Sub header section
				writer.startElement("tr"); //<tr>
				writer.addElement("td", "Well Name", tdSubHeaderAttr); //(1)
				writer.addElement("td", "Operation Name", tdSubHeaderAttr); //(2)
				writer.addElement("td", "Wellbore Type", tdSubHeaderAttr); //(3)
				writer.addElement("td", "Day", tdSubHeaderAttr); //(4)
				
				CustomFieldUom depthUOM = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "depthMdMsl", operationUomContext);
				writer.addElement("td", "Actual Depth (" + depthUOM.getUomSymbol() + ")" , tdSubHeaderAttr); //(5)
				
				writer.addElement("td", "Phase", tdSubHeaderAttr); //(6)
				writer.addElement("td", "Class", tdSubHeaderAttr); //(7)
				
				CustomFieldUom durationUOM = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
				writer.addElement("td", "Duration (" + durationUOM.getUomSymbol() + ")", tdSubHeaderAttr); //(8)
				
				writer.addElement("td", "NPT Code", tdSubHeaderAttr); //(9)
				writer.addElement("td", "NPT Desc (Rushmore)", tdSubHeaderAttr); //(10)
				writer.endElement(); //</tr>
			}
			
			//Populate detail rows
			for (Object[] arrObj: ActivityList) {
	
				if (!dailyUidDiffPhase.isEmpty()){
					if (dailyUidDiffPhase.containsKey(nullToEmptyString(arrObj[10]))) { //dailyUid
						tdAttr.addAttribute("style", "color:#ff0000"); //Red
						tdRightAttr.addAttribute("style", "color:#ff0000;text-align:right;");
						tdRightAttr.addAttribute("nowrap", "nowrap");						
					}
					else {
						tdAttr.addAttribute("style", "color:#000000"); //Black
						tdRightAttr.addAttribute("style", "color:#000000;text-align:right;");
						tdRightAttr.addAttribute("nowrap", "nowrap");
					}
				}
	
				writer.startElement("tr"); //<tr>
				writer.addElement("td", nullToEmptyString(arrObj[1]), tdAttr); //Project
				writer.addElement("td", nullToEmptyString(arrObj[2]), tdAttr); //SideTrack#
				writer.addElement("td", nullToEmptyString(arrObj[3]), tdAttr); //SideTrack Desc
				writer.addElement("td", nullToEmptyString(arrObj[4]), tdRightAttr); //Day
				
				//Get Actual Depth in base value
				CustomFieldUom depthConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "depthMdMsl", operationUomContext);
				String depthValue = nullToEmptyString(arrObj[5]);
				if (depthValue!="") {
					depthConverter.setBaseValue(Double.parseDouble(depthValue));
					depthValue = nullToEmptyString(depthConverter.formatOutputPrecision());
				}
				writer.addElement("td", emptyToDash(depthValue), tdRightAttr); //Actual Depth
				
				String altPhaseCode = getPhaseAltCode(nullToEmptyString(arrObj[6]), nullToEmptyString(arrObj[11]));
				String phaseCode = nullToEmptyString(arrObj[6]);
				if(!altPhaseCode.equals(phaseCode)) {
					phaseCode = altPhaseCode + " ~ (" + nullToEmptyString(arrObj[6]) +")";
				}
				writer.addElement("td", emptyToDash(phaseCode), tdAttr); //Phase
				writer.addElement("td", emptyToDash(getClassName(nullToEmptyString(arrObj[7]))), tdAttr); //Class
				
				//Get Activity Duration in base value
				CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
				String durationValue = nullToEmptyString(arrObj[8]); //Activity Duration
				if (StringUtils.isNotBlank(durationValue)) {
					durationConverter.setBaseValue(Double.parseDouble(durationValue));
					durationValue = nullToEmptyString(durationConverter.formatOutputPrecision());
				}
				writer.addElement("td", durationValue, tdRightAttr); //Activity Duration
				
				writer.addElement("td", emptyToDash(nullToEmptyString(arrObj[9])), tdAttr); //rootCause/NPTCode
				writer.addElement("td", emptyToDash(getRootCauseName(nullToEmptyString(arrObj[9]))), tdAttr); //rootCauseName/NPTDesc
				writer.endElement(); //</tr>
			}			
		}
		writer.endElement(); //</table>
		writer.close();
		
		return bytes.toString("utf-8");
	}
	
	/**	
	 * Customised query for OSL.
	 * Query Day Depth Cost Time Breakdown for Activity.
	 * @return result in HTML.
	 */
	public String queryDayDepthCostTimeBreakdownOSL()throws Exception{
		//Filters
		List<String> operations = new ArrayList<String>(); 
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		SimpleAttributes tableAttr = new SimpleAttributes();
		tableAttr.addAttribute("style", "borderStyle:solid;borderColor:blue;font-weight:bold;background-color:#C0C0C0;");
		tableAttr.addAttribute("border", "1");
		tableAttr.addAttribute("width", "100");
		
		SimpleAttributes tdHeaderAttr = new SimpleAttributes();
		tdHeaderAttr.addAttribute("colspan", "9");
		tdHeaderAttr.addAttribute("style", "text-align:center;font-weight:bold;background-color:#9AFEFF;");
		
		SimpleAttributes tdSubHeaderAttr = new SimpleAttributes();
		tdSubHeaderAttr.addAttribute("style", "text-align:center;font-weight:bold;background-color:#9AFEFF;");
		tdSubHeaderAttr.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes tdDataAttr = new SimpleAttributes();
		tdDataAttr.addAttribute("style", "text-align:right;");
		tdDataAttr.addAttribute("nowrap", "nowrap");

		//Declarations
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		writer.startElement("root");
		
		DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
		
		//Sorted the Operation Name 
		Collections.sort(operations, new operationComparator(this.getCurrentUserSession()));
		
		for (String operationUid : operations) {
			operationUomContext.setOperationUid(operationUid);
			String fullWellName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
			String currentActiveDvdPlan = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			
			String[] paramsFields = {"currentActiveDvdPlan"};
			Object[] paramsValues = {currentActiveDvdPlan};
			
			CustomFieldUom actualDurationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Activity.class, "activityDuration", operationUomContext);
			CustomFieldUom plannedDurationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), OperationPlanPhase.class, "p50Duration", operationUomContext);
			CustomFieldUom actualDepthConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "depthMdMsl", operationUomContext);
			CustomFieldUom plannedCostConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), OperationPlanPhase.class, "phaseBudget", operationUomContext);
			CustomFieldUom actualCostConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), ReportDaily.class, "daycost", operationUomContext);
			CustomFieldUom plannedDepthConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), OperationPlanPhase.class, "depthMdMsl", operationUomContext);
			
			String actualDurationUom = actualDurationConverter.getUomSymbol();
			String plannedDepthUom = plannedDepthConverter.getUomSymbol();
			String actualDepthUom = actualDepthConverter.getUomSymbol();
			String plannedCostUom = plannedCostConverter.getUomSymbol();
			String actualCostUom = actualCostConverter.getUomSymbol();
			
			Map<String, DVDCostTimeBreakDownObject> dvdDays = new LinkedHashMap<String, DVDCostTimeBreakDownObject>();
			Double cumCost = 0.0;
			
			String reportDailySql = "SELECT rd.operationUid, rd.dailyUid, rd.depthMdMsl, rd.daycost FROM ReportDaily rd, Daily d WHERE rd.dailyUid=d.dailyUid AND rd.operationUid=:operationUid AND rd.reportType=:reportType AND (rd.isDeleted is null or rd.isDeleted=false) AND (d.isDeleted is null or d.isDeleted=false) ORDER BY rd.reportDatetime";
			String[] paramsFields1 = {"operationUid", "reportType"};
			Object[] paramsValues1 = {operationUid, reportType};
			
			List<Object[]> reportDailyCount = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailySql, paramsFields1, paramsValues1, qp);
			//Query Actual Days (Depth, Day Cost)
			if (reportDailyCount.size() > 0){
				
				Integer dayNum = 1;
					
				for(Object[] objResultDailyRecord: reportDailyCount) {
					operationUomContext.setOperationUid(operationUid);
					String dailyUid = nullToEmptyString(objResultDailyRecord[1]);
					Double depthMdMsl = 0.0;
					if(objResultDailyRecord[2] != null) depthMdMsl = Double.parseDouble(objResultDailyRecord[2].toString());
					Double dayCost = 0.0;
					if(objResultDailyRecord[3] != null) dayCost = Double.parseDouble(objResultDailyRecord[3].toString());
					
					//Calculate Activity Duration By Class Code
					Double totalP = activityDurationByClassCode(dailyUid, "P");
					actualDurationConverter.setBaseValue(totalP);
					totalP = Double.parseDouble(actualDurationConverter.formatOutputPrecision());
					
					Double totalTP = activityDurationByClassCode(dailyUid, "TP");
					actualDurationConverter.setBaseValue(totalTP);
					totalTP = Double.parseDouble(actualDurationConverter.formatOutputPrecision());
					
					Double totalTU = activityDurationByClassCode(dailyUid, "TU");
					actualDurationConverter.setBaseValue(totalTU);
					totalTU = Double.parseDouble(actualDurationConverter.formatOutputPrecision());
					
					Double totalU = activityDurationByClassCode(dailyUid, "U");
					actualDurationConverter.setBaseValue(totalU);
					totalU = Double.parseDouble(actualDurationConverter.formatOutputPrecision());

					cumCost += dayCost;
					actualCostConverter.setBaseValue(cumCost);
					DVDCostTimeBreakDownObject data;
					
					actualDepthConverter.setBaseValue(depthMdMsl);
					
					if(dvdDays.get(dayNum.toString()) != null) {
						
						data = dvdDays.get(dayNum.toString());
						data.setActualCumCost(nullToEmptyString(actualCostConverter.formatOutputPrecision()));
						data.setActualDepth(actualDepthConverter.formatOutputPrecision());
						data.setDurationP(nullToEmptyString(totalP));
						data.setDurationTP(nullToEmptyString(totalTP));
						data.setDurationTU(nullToEmptyString(totalTU));
						data.setDurationU(nullToEmptyString(totalU));
					} else {
						data = new DVDCostTimeBreakDownObject();
						data.setActualCumCost(nullToEmptyString(actualCostConverter.formatOutputPrecision()));
						data.setActualDepth(actualDepthConverter.formatOutputPrecision());
						data.setDurationP(nullToEmptyString(totalP));
						data.setDurationTP(nullToEmptyString(totalTP));
						data.setDurationTU(nullToEmptyString(totalTU));
						data.setDurationU(nullToEmptyString(totalU));
					}
					
					dvdDays.put(dayNum.toString(), data);
					dayNum++;
				} 
			}
			
			//Query Planned DVD (plannedDuration, plannedBudget and plannedDepth)
			String dvdSql = "SELECT operationUid, p50Duration, phaseBudget, depthMdMsl FROM OperationPlanPhase WHERE operationPlanMasterUid=:currentActiveDvdPlan AND (isDeleted is null or isDeleted = false) order by phaseSequence";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(dvdSql, paramsFields, paramsValues, qp);
			
			Object[] prevData = null;
			Double prevBalance = null;
			Integer dayCounter = 1;
			Double cumDepth = 0.0;
			Double cumPCost = 0.0;
			Double currentProgress = 0.0;
			
			for (Object[] pd : list)
			{
				operationUomContext.setOperationUid(operationUid);
				Double phaseDuration = 0.0;
				if(pd[1] != null) phaseDuration = (Double) pd[1];
				Double phaseBudget= 0.0;
				if(pd[2] != null) phaseBudget= (Double) pd[2];
				Double phaseDepth = 0.0;
				if(pd[3] != null) phaseDepth = (Double) pd[3];
				Double preDepth = 0.0;
				
				plannedCostConverter.setBaseValue(phaseBudget);
				plannedDurationConverter.setBaseValue(phaseDuration);
				
				phaseDuration = plannedDurationConverter.getConvertedValue();
				phaseBudget = plannedCostConverter.getConvertedValue();
				Boolean isRun = true;
				if (prevData==null) {
					currentProgress = phaseDepth-0;
				}
				else {					
					preDepth = (Double) prevData[3];
					currentProgress = phaseDepth - preDepth;
				}
					
				Double durationToProcess = phaseDuration;
				if (prevBalance !=null && prevBalance >0.0)
				{
					Double durationToCount = 1-prevBalance;
					if (durationToProcess >= durationToCount)
					{
						durationToProcess -= durationToCount;
						cumDepth += durationToCount/phaseDuration*currentProgress;
						cumPCost += durationToCount/phaseDuration*phaseBudget;
						
						plannedCostConverter.setBaseValue(cumPCost);
						plannedDepthConverter.setBaseValue(cumDepth);
						
						if(dvdDays.get(dayCounter.toString()) != null) {
							
							DVDCostTimeBreakDownObject data = dvdDays.get(dayCounter.toString());
							data.setPlannedBudget(nullToEmptyString(plannedCostConverter.formatOutputPrecision()));
							data.setPlannedDepth(nullToEmptyString(plannedDepthConverter.formatOutputPrecision()));
							dvdDays.put(dayCounter.toString(), data);
						}
						prevBalance=0.0;
						dayCounter++;
					}else{
						durationToCount = durationToProcess;
						prevBalance += durationToProcess; 
						cumDepth += durationToCount/phaseDuration*currentProgress;
						cumPCost += durationToCount/phaseDuration*phaseBudget;
						isRun = false;
					}
					
				}
				
				if (isRun) {
					Double mod = durationToProcess % 1.0;
					Integer div = (durationToProcess.intValue() / 1);
					for (Integer i=1; i<=div;i++)
					{
						cumDepth += 1/phaseDuration*currentProgress;
						cumPCost += 1/phaseDuration*phaseBudget;
						plannedCostConverter.setBaseValue(cumPCost);
						plannedDepthConverter.setBaseValue(cumDepth);
						if(dvdDays.get(dayCounter.toString()) != null) {
							
							DVDCostTimeBreakDownObject data = dvdDays.get(dayCounter.toString());
							data.setPlannedBudget(nullToEmptyString(plannedCostConverter.formatOutputPrecision()));
							data.setPlannedDepth(nullToEmptyString(plannedDepthConverter.formatOutputPrecision()));
							dvdDays.put(dayCounter.toString(), data);
						}
						dayCounter++;
					}
					if (mod>0.0)
					{
						cumDepth += mod/phaseDuration*currentProgress;
						cumPCost += mod/phaseDuration*phaseBudget;
						prevBalance = mod;
					}
				}
				prevData = pd;
			}
			
			writer.startElement("table", tableAttr); //<table>
			writer.startElement("tr"); //<tr>
			writer.addElement("td", fullWellName, tdHeaderAttr); //Operation		
			writer.endElement(); //<tr>
			
			writer.startElement("tr");
			writer.addElement("td", "Day", tdSubHeaderAttr);
			writer.addElement("td", "Planned Depth", tdSubHeaderAttr);
			writer.addElement("td", "Planned Budget", tdSubHeaderAttr);
			writer.addElement("td", "Actual Depth", tdSubHeaderAttr);
			writer.addElement("td", "Cum Cost", tdSubHeaderAttr);
			writer.addElement("td", "Programmed", tdSubHeaderAttr);
			writer.addElement("td", "Unprogrammed", tdSubHeaderAttr);
			writer.addElement("td", "Trouble During Programmed", tdSubHeaderAttr);
			writer.addElement("td", "Trouble During Unprogrammed", tdSubHeaderAttr);
			writer.endElement();
			writer.startElement("tr");
			writer.addElement("td", "", tdSubHeaderAttr);
			writer.addElement("td", "(" + plannedDepthUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + plannedCostUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualDepthUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualCostUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualDurationUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualDurationUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualDurationUom + ")", tdSubHeaderAttr);
			writer.addElement("td", "(" + actualDurationUom + ")", tdSubHeaderAttr);
			writer.endElement();
			for (Map.Entry<String, DVDCostTimeBreakDownObject>item : dvdDays.entrySet()) {
				DVDCostTimeBreakDownObject resultObj = item.getValue();
				writer.startElement("tr");
				writer.addElement("td", item.getKey(), tdDataAttr);
				writer.addElement("td", resultObj.getPlannedDepth()!=null?resultObj.getPlannedDepth().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getPlannedBudget()!=null?resultObj.getPlannedBudget().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getActualDepth()!=null?resultObj.getActualDepth().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getActualCumCost()!=null?resultObj.getActualCumCost().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getDurationP()!=null?resultObj.getDurationP().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getDurationTP()!=null?resultObj.getDurationTP().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getDurationTU()!=null?resultObj.getDurationTU().toString():"0.0", tdDataAttr);
				writer.addElement("td", resultObj.getDurationU()!=null?resultObj.getDurationU().toString():"0.0", tdDataAttr);
				writer.endElement();
			}
			writer.endElement();
		}

		writer.close();
		return bytes.toString("utf-8");	
	}
	
	private String queryForRigMoveForSantos() throws Exception {
		
		List<String> operations = new ArrayList<String>(); 
		Map<String, String> allOperations = (Map<String, String>) this.getFilter("operationUid");
		for (Map.Entry<String, String> entry:allOperations.entrySet()) {
			operations.add(entry.getValue());
		}
		
		SimpleAttributes tableAttr = new SimpleAttributes();
		tableAttr.addAttribute("style", "borderStyle:solid;borderColor:blue;font-weight:bold;");
		tableAttr.addAttribute("border", "1");
		tableAttr.addAttribute("width", "100");
		
		SimpleAttributes tdHeaderAttr = new SimpleAttributes();
		tdHeaderAttr.addAttribute("style", "text-align:center;font-weight:bold;");
		SimpleAttributes tdHeaderAttrBg = new SimpleAttributes();
		tdHeaderAttrBg.addAttribute("style", "text-align:center;font-weight:bold;background-color:#9AFEFF;");

		SimpleAttributes tdSubHeaderAttr = new SimpleAttributes();
		tdSubHeaderAttr.addAttribute("style", "font-weight:bold;background-color:#9AFEFF;");
		tdSubHeaderAttr.addAttribute("nowrap", "nowrap");
		
		SimpleAttributes tdDataAttr = new SimpleAttributes();
		tdDataAttr.addAttribute("nowrap", "nowrap");

		SimpleAttributes tdDataAttrRight = new SimpleAttributes();
		tdDataAttrRight.addAttribute("nowrap", "nowrap");
		tdDataAttrRight.addAttribute("text-align", "right");
		
		//Declarations
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
		writer.startElement("root");
		
		//create header
		writer.startElement("table", tableAttr);
		writer.startElement("tr");
		tdHeaderAttr.addAttribute("colspan", "9");
 		writer.addElement("td", "", tdHeaderAttr);
 		tdHeaderAttrBg.addAttribute("colspan", "4");
 		writer.addElement("td", "Pre Spud", tdHeaderAttrBg);
		tdHeaderAttr.addAttribute("colspan", "6");
 		writer.addElement("td", "", tdHeaderAttr);
 		tdHeaderAttrBg.addAttribute("colspan", "7");
 		writer.addElement("td", "External Factos", tdHeaderAttrBg);
		tdHeaderAttr.addAttribute("colspan", "2");
 		writer.addElement("td", "Santos Input Only", tdHeaderAttr);
		writer.endElement(); //tr

		writer.startElement("tr");
		writer.addElement("td", "Rig Used", tdSubHeaderAttr);
		writer.addElement("td", "Rig Type", tdSubHeaderAttr);
		writer.addElement("td", "Wellname", tdSubHeaderAttr);
		writer.addElement("td", "State", tdSubHeaderAttr);
		writer.addElement("td", "Rig move contractor", tdSubHeaderAttr);
		writer.addElement("td", "Rig move from", tdSubHeaderAttr);
		writer.addElement("td", "Rig move distance", tdSubHeaderAttr);
		writer.addElement("td", "Rig transport configuration", tdSubHeaderAttr);
		writer.addElement("td", "Camp moved", tdSubHeaderAttr);
		/* PreSpud */
		writer.addElement("td", "Mob. start date", tdSubHeaderAttr);
		writer.addElement("td", "On location date", tdSubHeaderAttr);
		writer.addElement("td", "Spud Date", tdSubHeaderAttr);
		writer.addElement("td", "Total Rig Move Time (days)", tdSubHeaderAttr);
		
		writer.addElement("td", "Loads moved before Rig Release", tdSubHeaderAttr);
		writer.addElement("td", "Number of Santos Loads", tdSubHeaderAttr);
		//writer.addElement("td", "Loads above contract number", tdSubHeaderAttr);
		writer.addElement("td", "PAD Move", tdSubHeaderAttr);
		writer.addElement("td", "Total number of loads moved", tdSubHeaderAttr);
		writer.addElement("td", "Roads", tdSubHeaderAttr);
		writer.addElement("td", "Roads Type", tdSubHeaderAttr);
		/* External Factors */
		writer.addElement("td", "Weather", tdSubHeaderAttr);
		writer.addElement("td", "Other drilling rig moves", tdSubHeaderAttr);
		writer.addElement("td", "EHS", tdSubHeaderAttr);
		writer.addElement("td", "Road/Lease conditions", tdSubHeaderAttr);
		writer.addElement("td", "Road closure", tdSubHeaderAttr);
		writer.addElement("td", "Other (Police, Council, Programme,..)", tdSubHeaderAttr);
		writer.addElement("td", "Comments", tdSubHeaderAttr);
		/* Santos Input Only */
		writer.addElement("td", "Contract duration (RMS)", tdSubHeaderAttr);
		writer.addElement("td", "Timing (Note c)", tdSubHeaderAttr);
		writer.endElement(); //tr
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentUserSession().getUserLocale());
		
		String queryString = "SELECT op.operationUid, w, wb, op " +
				" FROM Operation op, Wellbore wb, Well w " +
				" WHERE (op.isDeleted=false or op.isDeleted is null) " +
				" AND (wb.isDeleted=false or wb.isDeleted is null) " +
				" AND (w.isDeleted=false or w.isDeleted is null) " +
				" AND wb.wellboreUid = op.wellboreUid " +
				" AND wb.wellUid = w.wellUid " +
				" AND op.operationUid in ('" + StringUtils.join(operations, "','") + "') " +
				" order by op.rigOnHireDate";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString, qp);
		for (Object[] rec : result) {
			String operationUid = nullToEmptyString(rec[0]);
			Well well = (Well) rec[1];
			Operation operation = (Operation) rec[3];
			operationUomContext.setOperationUid(operationUid);
			
			RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, nullToEmptyString(operation.getRigInformationUid()));
			
			writer.startElement("tr");
			writer.addElement("td", (rig!=null?rig.getRigName():""), tdDataAttr);
			writer.addElement("td", (rig!=null?rig.getDrillingEquipmentDescription():""), tdDataAttr);
			writer.addElement("td", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid), tdDataAttr);
			writer.addElement("td", getLookupValue(well.getState(), "xml://well.state?key=code&amp;value=label"), tdDataAttr);
			writer.addElement("td", getLookupValue(operation.getRigMoveContractor(), "db://LookupCompany?key=lookupCompanyUid&value=companyName"), tdDataAttr);
			writer.addElement("td", nullToEmptyString(operation.getRigMoveFrom()), tdDataAttr);
			
			if (operation.getRigMoveDistance()!=null) {
				thisConverter.setReferenceMappingField(Operation.class, "rigMoveDistance", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(operation.getRigMoveDistance());
				writer.addElement("td", thisConverter.getFormattedValue(), tdDataAttr);
			} else {
				writer.addElement("td", "", tdDataAttr);
			}
			String rigTransportConfigNo = getLookupValue(operation.getRigTransportConfigNo(), "xml://operation.rigTransportConfigNo?key=code&amp;value=label"); 
			writer.addElement("td", StringUtils.isNotBlank(rigTransportConfigNo)?rigTransportConfigNo:"None Specified", tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getCampMovedFlag()), tdDataAttr);
			/* PreSpud */
			writer.addElement("td", this.formatDatetime(operation.getRigOnHireDate(), true), tdDataAttr);
			writer.addElement("td", this.formatDatetime(operation.getOnlocDateOnlocTime(), true), tdDataAttr);
			writer.addElement("td", this.formatDatetime(operation.getSpudDate(), true), tdDataAttr);
			if (operation.getOnlocDateOnlocTime()!=null && operation.getRigOnHireDate()!=null) {
				thisConverter.setReferenceMappingField(Operation.class, "daysSpentPriorToSpud", operationUomContext.getUomTemplateUid());
				long rigMoveDuration = operation.getOnlocDateOnlocTime().getTime() - operation.getRigOnHireDate().getTime();
				thisConverter.setBaseValue(rigMoveDuration / 1000.0);
				writer.addElement("td", thisConverter.formatOutputPrecision(), tdDataAttr); 
			} else {
				writer.addElement("td", "", tdDataAttr);
			}
			
			writer.addElement("td", nullToEmptyString(operation.getTotalLoadsBeforeRigRelease()), tdDataAttr);
			writer.addElement("td", nullToEmptyString(operation.getTotalOperatorLoads()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getIsPadExist()), tdDataAttr);
			//writer.addElement("td", nullToEmptyString(operation.getTotalLoadsAboveContractNo()), tdDataAttr);
			writer.addElement("td", nullToEmptyString(operation.getTotalLoadsMoved()), tdDataAttr);
			writer.addElement("td", getLookupValue(operation.getRoadCondition(), "xml://operation.roadCondition?key=code&amp;value=label"), tdDataAttr);
			writer.addElement("td", getLookupValue(operation.getRoadType(), "xml://operation.roadType?key=code&amp;value=label"), tdDataAttr);
			/* External Factors */
			writer.addElement("td", yesNoFlag(operation.getWeatherFlag()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getRigMoveFlag()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getEhsFlag()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getRoadLeaseConditionFlag()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getRoadClosureFlag()), tdDataAttr);
			writer.addElement("td", yesNoFlag(operation.getOtherExternalFactorsFlag()), tdDataAttr);
			writer.addElement("td", nullToEmptyString(operation.getRigMoveComment()));
			/* Santos Input Only */
			writer.addElement("td", "", tdDataAttr);
			writer.addElement("td", "", tdDataAttr);

			writer.endElement();
		}

		writer.endElement();	//table
		writer.endAllElements(); //root
	
		writer.close();
		return bytes.toString("utf-8");	
	}
	
	private String getLookupValue(String value, String lookup) {
		if (StringUtils.isBlank(value)) return "";
		try {
			Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
			if (lookupList!=null) {
				if (lookupList.containsKey(value)) {
					LookupItem item = lookupList.get(value);
					return nullToEmptyString(item.getValue());
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return value;
	}
	
	private String yesNoFlag(Boolean value) {
		if (value==null) return "N/A";
		return value?"Yes":"No"; 
	}
	
	private String formatDatetime(Date value, Boolean includeTime) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy" + (includeTime?" HH:mm":""));
		if (value!=null) return df.format(value);
		return "";
	}
	
	
	private class RunNumberComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[2];
				Object in2 = o2[2];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
}

