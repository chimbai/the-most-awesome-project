(function(window){
	
	function Accordion(config,container) {
	    this.config = config;
	    if (config && config.css)
	    	this.css= config.css;
	    this.uiElement = document.createElement("div");
	    if (config){
	    	if (config.width)
	    		this.uiElement.style.width = (parseInt(config.width)+45)+"px";
	    	if (config.height)
	    		this.uiElement.style.height = (parseInt(config.height)+5)+"px";
	    }
	    var containerCSS = "containerCSS";
	    if (this.css && this.css.container)
	    	containerCSS = this.css.container;
	    this.uiElement.className=containerCSS;
	    if (container){
		    this.container = container;
		    this.container.appendChild(this.uiElement);
	    }
	    this.childrens = [];
	}

	Accordion.prototype.addChild = function (id, headerText, childContainer) {
	    var child = {id:id,active:false};
	    var header = document.createElement("h3");
	    header.appendChild(document.createTextNode(headerText));
	    header.onclick = this.onToggle.bindAsEventListener(this, id);
	    var headerCss = "accordionHeader";
	    if (this.css && this.css.header) headerCss = this.css.header;
	    $(header).addClass(headerCss);
	    this.uiElement.appendChild(header);
	    var contentCss = "accordionContent";
	    if (this.css && this.css.content) contentCss = this.css.content;
	    $(childContainer).addClass(contentCss);
	    this.uiElement.appendChild(childContainer);
	    child.header = header;
	    child.container = childContainer;
	    this.childrens.push(child);
	};

	Accordion.prototype.onToggle = function (event,id) {
		this.toggle(id);
	}
	Accordion.prototype.toggle = function (id) {

		for (var i=0;i<this.childrens.length;i++) {
	        var child = this.childrens[i];
	        var container = $(child.container);
	        if (id === child.id ) {
	        	if (!child.active){
		            container.slideToggle("slow");
		            child.active = true;
	        	}
	        } else {
	        	if (child.active){
	        		container.slideUp("slow");
	        		child.active = false;
	        	}
	        }
	    }
	};
	
	

	D3.Accordion = Accordion;
	
	
	
})(window);