(function(window){
	D3.inherits(TransportDailyParamNodeListener,D3.EmptyNodeListener);
	
	function TransportDailyParamNodeListener() {}
	
	TransportDailyParamNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
		var importBtn = this.constructor.uber.getCustomRootButton(id,commandBeanProxy,btnDefinition);
	    if (importBtn != null) {
	    	return importBtn;
	    }
		if (id == "wellIT-import-transportdailyparam") {
			var importBtn = document.createElement("button");
			importBtn.className = "rootButtons";
		    importBtn.appendChild(document.createTextNode("WELS Import"));
		    importBtn.onclick = this.onclickWELSImport.bindAsEventListener(this);
		    return importBtn;
		}
        return null;
	};
	
	TransportDailyParamNodeListener.prototype.onclickWELSImport = function() {
		this.commandBeanProxy.addAdditionalFormRequestParams("action", "importWellIT");
		this.commandBeanProxy.submitForServerSideProcess();
    };
	
	D3.CommandBeanProxy.nodeListener = TransportDailyParamNodeListener;
})(window);