(function(window) {
	RigStateManagementNodeListener.JOB_ENDED = 0;
	RigStateManagementNodeListener.WEB_SERVICE_URL = "rigstatemanagementwebservice.html";
	RigStateManagementNodeListener.MAIN_ACTION_UPLOAD_SINGLE_FILE = "upload_single_file";
	RigStateManagementNodeListener.CANCEL_PAYLOAD = "cancel_payload";
	D3.inherits(RigStateManagementNodeListener, D3.EmptyNodeListener);
	function RigStateManagementNodeListener() {
		this.commandBeanProxy = null;
	}
	
	RigStateManagementNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
		if (id == "reprocess") {
			var reprocessBtn = document.createElement("button"); 
			reprocessBtn.className = "rootButtons";
			reprocessBtn.appendChild(document.createTextNode("Reprocess Operation"));
			reprocessBtn.onclick = this.onclickReprocess.bindAsEventListener(this);
			this.startStatusCheck();
			return reprocessBtn;
		} else if (id == "flush") {
			var flushBtn = document.createElement("button");
			flushBtn.className = "rootButtons";
			flushBtn.appendChild(document.createTextNode("Flush Operation"));
			flushBtn.onclick = this.onclickFlush.bindAsEventListener(this);
			this.startStatusCheck();
			return flushBtn;
		} else if (id == "payload") {
			this.buildFileUploadControl();
			var payloadBtn = document.createElement("button");
			payloadBtn.className = "rootButtons";
			payloadBtn.appendChild(document.createTextNode("Import Payload"));
			payloadBtn.onclick = this.onclickPayload.bindAsEventListener(this);
			this.startStatusCheck();
			return payloadBtn;
		} else if (id == "influxdbMigrate") {
			var influxdbMigrateBtn = document.createElement("button");
			influxdbMigrateBtn.className = "rootButtons";
			influxdbMigrateBtn.appendChild(document.createTextNode("InfluxDB Migrate"));
			influxdbMigrateBtn.onclick = this.onclickInfluxdbMigrate.bindAsEventListener(this);
			return influxdbMigrateBtn;
		}
		return null;
	};
	
	RigStateManagementNodeListener.prototype.buildFileUploadControl = function(){
		this._fileForm = document.createElement("form");
        this._fileForm.action = "upload";
        this._fileForm.method = "post";
        this._fileForm.id = "fileForm";
        this._fileForm.enctype = "multipart/form-data";
		this.buildFileUpload();
		this._fileForm.appendChild(this._fileUpload);
	}
	
	RigStateManagementNodeListener.prototype.buildFileUpload = function(){
		this._fileUpload = document.createElement("input");
		this._fileUpload.type = "file";
		this._fileUpload.id = "fileUploadCtl";
		this._fileUpload.style.display = "none";
		$(this._fileUpload).on("change", {context: this}, this.fileUploadOnChange);
	}
	
	RigStateManagementNodeListener.prototype.getSelectedOperationUids = function() {
		var uids = this.commandBeanProxy.rootNode.getFieldValue("@lookupWellsOperationSelection");
		if (uids.length == 0) {
			D3.UIManager.popup("Warning", "Please select an operation.");
			return false;
		}
		return uids;
	};
	
	RigStateManagementNodeListener.prototype.resetScreen = function() {
		this.commandBeanProxy.rootNode.setFieldValue("@lookupWellsOperationSelection","");
		$('.select2-selection__clear').trigger('mousedown');
	}
	
	RigStateManagementNodeListener.prototype.onclickInfluxdbMigrate = function() {
		var self = this;
		D3.UIManager.confirm("InfluxDB Migrate", "Are you sure you want to migrate Rig State Raw to influxDB?", function() {
			var params = {};
			params._invokeCustomFilter = "influxdb_migrate";
			self.commandBeanProxy.customInvoke(params, self.submitCompleted, self, "json");
			self.setStatusMessage("Preparing to migrate RSR to influxDB...");
			self.startStatusCheck();
		});
	};
	
	RigStateManagementNodeListener.prototype.onclickReprocess = function() {
		var page = this.commandBeanProxy.getCommandBeanUrl();
		var self = this;
		var formData = new FormData();
		var operationUid = this.getSelectedOperationUids();
		formData.append('_invokeCustomFilter', "queryIsValidRigStateDefinition");
		formData.append('operationUids', operationUid);
        
		$.ajax({
          url: page,
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
       }).done(function(data, textStatus, jqXHR) {
           var response = JSON.parse(jqXHR.responseText);
           self.onclickReprocessCompleteHandler(response.isValidRigStateDefinition, operationUid);
       }).fail(function( jqXHR, textStatus, errorThrown) {
    	   var response = JSON.parse(jqXHR.responseText);
    	   D3.UIManager.popup("Unexpected", response.message);
       }).always(function() {

       });
	};
	
	RigStateManagementNodeListener.prototype.onclickReprocessCompleteHandler = function(isValid, operationUid) {
		this.callReprocess(operationUid, 'refresh', isValid);
	};

	RigStateManagementNodeListener.prototype.submitCheckCompleted = function(response) {
		if (response.success) { //still valid
			return true;
		} else {
			return D3.UIManager.confirm("Invalid Rig State Definition", "Are you sure you want to Flush States?", function() {
				var params = {};
				params._invokeCustomFilter = "flush";
				params.uids = operationUids; 
				self.commandBeanProxy.customInvoke(params, self.submitFlushCompleted, self, "json");
			}, null, this);
		}
	};
	
	RigStateManagementNodeListener.prototype.onclickFlush = function() {
		this.requireReload = true;
		var operationUids = this.getSelectedOperationUids();
		if (operationUids) {
			var self = this;
			D3.UIManager.confirm("Flush States", "Are you sure you want to Flush States?", function() {
				var params = {};
				params._invokeCustomFilter = "flush";
				params.uids = operationUids; 
				self.commandBeanProxy.customInvoke(params, self.submitFlushCompleted, self, "json");
			}, null, this);
		}
	};
	
	RigStateManagementNodeListener.prototype.onclickFlushDayConfirm = function(popup) {
		popup.dialog("close");
	};
	
	RigStateManagementNodeListener.prototype.startStatusCheck = function() {
		this.scheduleNextStatusCheck();
	};
	
	RigStateManagementNodeListener.prototype.scheduleNextStatusCheck = function() {
		var self = this;
		setTimeout(function() {
			self.checkStatus();
		}, 1000);
	};
	
	RigStateManagementNodeListener.prototype.checkStatus = function() {
		var params = {};
		params._invokeCustomFilter = 'checkStatus';
		this.commandBeanProxy.customInvoke(params, this.statusCheckCallback, this, "json");
	};
	
	RigStateManagementNodeListener.prototype.submitCompleted = function(response) {
		
	};
	
	RigStateManagementNodeListener.prototype.submitFlushCompleted = function(response) {
		this.resetScreen();
		this.setStatusMessage("Preparing to flush states...");
		this.startStatusCheck();
	};
	
	RigStateManagementNodeListener.prototype.statusCheckCallback = function(response) {
		this.prepareDialog();
		if (response.success) {
			if (response.code != RigStateManagementNodeListener.JOB_ENDED) {
				this.setStatusMessage(response.message);
				this.scheduleNextStatusCheck();
			} else {
				if(this.requireReload){
					this.setStatusMessage("Reloading ...");
					location.reload(true);
					this.requireReload = false;
				} else this.statusDialog.dialog("close");
			}
		} else {
			this.statusDialog.dialog("close");
			D3.UIManager.popup("Error", response.message);
		}
	};
	
	RigStateManagementNodeListener.prototype.prepareDialog = function() {
		if (this.dialogContent != null) return;
		
		var dialog = $("<div id='statusDialog'></div>");
		this.dialogContent = $("<div class='dialogContent' style='white-space:pre;'></div>");
		dialog.append(this.dialogContent);
		
		var self = this;
		
		this.statusDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
	
	RigStateManagementNodeListener.prototype.setStatusMessage = function(message) {
		this.prepareDialog();
		this.dialogContent.empty();
		this.dialogContent.append(message);
		this.statusDialog.dialog("open");
	};
	
	RigStateManagementNodeListener.prototype.showPayloadImportWindow = function(){
		this.importBoxShow();
	};
	
	RigStateManagementNodeListener.prototype.onclickPayload = function() {
		this._fileUpload.click();
	};
	
	RigStateManagementNodeListener.prototype.fileUploadOnChange = function(e) {
		var context = e.data.context;
		var fileToBeUploaded = e.target.files[0];
        if(fileToBeUploaded == undefined || fileToBeUploaded === null){
        	D3.UIManager.popup("Error", "Expecting a file but nothing is returned.");
        } else {
        	context.setStatusMessage("Loading ...");
        	context.refreshUploadHandler(fileToBeUploaded, context);
        }
	};
	
	//do uploading and process returned prompt for well mapping selection
	RigStateManagementNodeListener.prototype.refreshUploadHandler = function(file, context){
		var page = this.commandBeanProxy.getCommandBeanUrl();
		var self = this;
		var formData = new FormData();
		formData.append('_invokeCustomFilter', RigStateManagementNodeListener.MAIN_ACTION_UPLOAD_SINGLE_FILE);
		formData.append('file', file);
        
		$.ajax({
          url: page,
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
       }).done(function(data, textStatus, jqXHR) {
           var response = JSON.parse(jqXHR.responseText);
           self.uploadCompleteHandler(response);
           context.statusDialog.dialog("close");
       }).fail(function( jqXHR, textStatus, errorThrown) {
    	   var response = JSON.parse(jqXHR.responseText);
    	   //D3.UIManager.popup("Unexpected", response.message);
    	   context.uploadProcessCompleted(false, response.message);
       }).always(function() {

       });
	};
	
	//process error, get user input for multiple well mapping
	RigStateManagementNodeListener.prototype.uploadCompleteHandler = function(response) {
		if (response != null) {
			if(response.success) {
				//if more than 1 mapping defined, pop up mapping selector
				this._depotWellOps = new DepotWellOpsMappingLookup(this, response.lookupDepotWellOpsMapping, response.tempFile);
				this._depotWellOps.show();
			} else {
				var msg = D3.LanguageManager.getLabel("File: " + this._fileUpload.files[0].name + " \n[ " + response.message + " ]");
				this.uploadProcessCompleted(false, msg);
			}
		} else {
			this.uploadProcessCompleted(false, D3.LanguageManager.getLabel('[WARNING] Unspecified error when uploading payload.'));
		}
	};
    
    RigStateManagementNodeListener.prototype.uploadProcessCompleted = function(success, msg) {
    	this.requireReload = true;
    	if(!success) {
			D3.UIManager.popup("Error when uploading the payload", msg);
			this.requireReload = false;
		}
		this.startStatusCheck();
		this.buildFileUpload();
	};
	
	D3.CommandBeanProxy.nodeListener = RigStateManagementNodeListener;
	
	RigStateManagementNodeListener.prototype.callReprocess = function(uids, invokeCustomFilter, isValidRigStateDefinition) {
		if (uids) {
			var self = this;
			var msg = "Are you sure you want to Reprocess States?";
			if(!isValidRigStateDefinition) msg += "\n WARNING: Rig State Definition is no longer valid.";
			D3.UIManager.confirm("Reprocess States", msg, function() {
				var params = {};
				params._invokeCustomFilter = invokeCustomFilter;
				params.uids = uids;
				self.commandBeanProxy.customInvoke(params, self.submitCompleted, self, "json");
				self.setStatusMessage("Preparing to reprocess states...");
				self.startStatusCheck();
			}, null, this);
		}
	}
	
	RigStateManagementNodeListener.prototype.callReprocessLog = function(uids, invokeCustomFilter, isValidRigStateDefinition) {
		if (uids) {
			var self = this;
			var msg = "Are you sure you want to Reprocess Log?";
			if(!isValidRigStateDefinition) msg += "\n WARNING: Rig State Definition is no longer valid.";
			D3.UIManager.confirm("Reprocess Log", msg, function() {
				var params = {};
				params._invokeCustomFilter = invokeCustomFilter;
				params.uids = uids;
				self.commandBeanProxy.customInvoke(params, self.submitCompleted, self, "json");
				self.setStatusMessage("Preparing to reprocess log...");
				self.startStatusCheck();
			}, null, this);
		}
	}
	
	RigStateManagementNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		if (id == "reprocess-selected" && parentNode.children["ReportDaily"].getLength() > 0) {
			RigStateManagementNodeListener.REPROCESS_SELECTED = buttonDeclaration.label;
			var config = {
					tag:"button",
					text:RigStateManagementNodeListener.REPROCESS_SELECTED,
					css:"DefaultButton",
					context:this
			};
			return D3.UI.createElement(config);
		}else if (id == "reprocess-log" && parentNode.children["ReportDaily"].getLength() > 0) {
			RigStateManagementNodeListener.REPROCESS_LOG = buttonDeclaration.label;
			var config = {
					tag:"button",
					text:RigStateManagementNodeListener.REPROCESS_LOG,
					css:"DefaultButton",
					context:this
			};
			return D3.UI.createElement(config);
		} else if (id == "flushday" && parentNode.children["ReportDaily"].getLength() > 0) {
			var config = {
					tag:"button",
					text:buttonDeclaration.label,
					css:"DefaultButton",
					context:this
			};
			return D3.UI.createElement(config);
		} else {
			return null;
		}
	}
	
	RigStateManagementNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "reprocess-selected" && node.children["ReportDaily"].getLength() > 0) {
			var selectedNodes = this.commandBeanProxy.rootNode.collectSelectedChildNodes();
			var dailyNodes = selectedNodes.ReportDaily; 
			var uids = [];
			if(dailyNodes.length > 0){
				this.requireReload = true;
				D3.forEach(dailyNodes, function(node) {
					var reportDailyUid = node.primaryKeyValue;
					uids.push(reportDailyUid);
				});
				this.callReprocess(uids, "refresh_daily", true);
			} else {
				D3.UIManager.popup("Warning", "Please select at least one day.");
			}
		}
		if (id == "reprocess-log" && node.children["ReportDaily"].getLength() > 0) {
			var selectedNodes = this.commandBeanProxy.rootNode.collectSelectedChildNodes();
			var dailyNodes = selectedNodes.ReportDaily; 
			var uids = [];
			if(dailyNodes.length > 0){
				this.requireReload = true;
				D3.forEach(dailyNodes, function(node) {
					var reportDailyUid = node.primaryKeyValue;
					uids.push(reportDailyUid);
				});
				this.callReprocessLog(uids, "refresh_log", true);
			} else {
				D3.UIManager.popup("Warning", "Please select at least one day.");
			}
		}
		if (id == "flushday" && node.children["ReportDaily"].getLength() > 0) {
			this.requireReload = true;
			var operationUid = this.getSelectedOperationUids();
			if (operationUid) {
				var self = this;
				var selectedNodes = this.commandBeanProxy.rootNode.collectSelectedChildNodes();
				var reportDailyNodes = selectedNodes.ReportDaily;
				if (reportDailyNodes.length > 0) {
					D3.UIManager.confirm("Flush Day", "You are about to delete rig states from the selected day(s). This action is irreversible.<br/><br/>Do you want to proceed?", function() {
						var uids = [];
						D3.forEach(reportDailyNodes, function(node) {
							uids.push(node.primaryKeyValue);
						});
						var params = {};
						params._invokeCustomFilter = "flushday";
						params.uids = uids;
						self.commandBeanProxy.customInvoke(params, self.submitCompleted, self, "json");
						self.setStatusMessage("Preparing to flush day...");
						self.startStatusCheck();
					}, null, this);
				} else {
					D3.UIManager.popup("Warning", "Please select at least one day.");
				}
			}
		}
	};
	
	//Well Mapping Pop Up
	function DepotWellOpsMappingLookup(that, lookup, tempFilePath) {
		this._caller = that;
		this._lookupItems = lookup;
		this._tempFilePath = tempFilePath;
		this._commandBeanProxy = that.commandBeanProxy;
		
		this.title = "Multiple Operations Detected";
		this.dropdownLabel = "This payload matches multiple operations. \nPlease select the correct well/ops to import into: ";
		this.buttonConfirmLabel = "Ok";
		this.buttonCancelLabel = "Cancel";
		
		this.init();
	}
	
	DepotWellOpsMappingLookup.prototype.dispose = function() {
		this._lookupItems = null;
		this._tempFilePath = null;
		this.box = null;
	};
	
	DepotWellOpsMappingLookup.prototype.init = function() {
		if(!this.box) {
			var _boxDiv = "<div class='popUpContainer'></div>";
			this.box = $(_boxDiv);
			this.build(this.box);
		}
	};
	
	DepotWellOpsMappingLookup.prototype.build = function(parent) {
		var tableCfg = "<table width='100%' height='100%'>" +
				"<tr>" +
					"<td class='popUpLineOne' colspan='2'/>" +
					"</td>" +
				"</tr>" +
				"<tr>" +
					"<td class='popUpLineTwo-DropDown'/>" +
					"<td class='popUpLineTwo-ConfirmButton'>" +
					"</td>" +
				"</tr>" +
					"<tr>" +
						"<td class='popUpLineThree-Text' colspan='2'></td>" +
					"</tr>" +
				"</table>";
		parent.append(tableCfg);
		
		//var labelText = this.headerText;
		var label = this.dropdownLabel;
		
		var _importPopupLineOne = parent.find(".popUpLineOne").get(0);
		var _importPopupLineTwoDropDown = parent.find(".popUpLineTwo-DropDown").get(0);
		var _importPopupLineTwoConfirmButton = parent.find(".popUpLineTwo-ConfirmButton").get(0);
		_importPopupLineTwoConfirmButton.style.textAlign = "right";
		
		//Label for dropdown
		this._popupLineOne = document.createElement("label");
		this._popupLineOne.style.whiteSpace = "pre";
		this._popupLineOne.appendChild(document.createTextNode(label));
		_importPopupLineOne.appendChild(this._popupLineOne);
		
		this.buildDropDown(_importPopupLineTwoDropDown);
		this.buildConfirmButton(_importPopupLineTwoConfirmButton);
		this.buildCancelButton(_importPopupLineTwoConfirmButton);
	};
	
	DepotWellOpsMappingLookup.prototype.buildDropDown = function(container) {
		D3.UI.removeAllChildNodes(container);
		this.lookupList = null;
		var select = document.createElement("select");
		select.id = "itemSelect";
		$(select).on("change", {context: this}, this.onChangeDropDown);
		this.assignItems(select, this._lookupItems);
		this.dropDownSelect = select;
		container.appendChild(this.dropDownSelect);
	};
	
	DepotWellOpsMappingLookup.prototype.assignItems = function(container, items){
		var r = items;
		$(container).empty();
		option = document.createElement("option");
		option.setAttribute("value", "");
		option.appendChild(document.createTextNode(""));
		container.options.add(option);
		
		$.each(r,function(index, item){
			option = document.createElement("option");
			option.setAttribute("value", item[0]);
			option.setAttribute("message", item[2]);
			option.appendChild(document.createTextNode(item[1]));
			container.options.add(option);
		});
	};
	
	DepotWellOpsMappingLookup.prototype.onChangeDropDown = function(e) {
		var context = e.data.context;
		context.validateConfirmButton();
	};
	
	DepotWellOpsMappingLookup.prototype.buildConfirmButton = function(container) {
		var confirmButtonConfig = {
				tag:"button",
				text:this.buttonConfirmLabel,
				css:"DefaultButton",
				callbacks:{
					onclick:this._confirmButton_onClick
				},
				context:this
		}
		this._confirmButton = D3.UI.createElement(confirmButtonConfig);
		this.enableButton(this._confirmButton, false);
		container.appendChild(this._confirmButton);
	};
	
	DepotWellOpsMappingLookup.prototype.buildCancelButton = function(container) {
		var cancelButtonConfig = {
				tag:"button",
				text:this.buttonCancelLabel,
				css:"DefaultButton",
				callbacks:{
					onclick:this._cancelButton_onClick
				},
				context:this
		}
		this._cancelButton = D3.UI.createElement(cancelButtonConfig);
		container.appendChild(this._cancelButton);
	};
	
	DepotWellOpsMappingLookup.prototype.enableButton = function(button, value) {
		if (value)
			$(button).removeAttr("disabled");
		else
			$(button).attr("disabled","disabled");
	};
	
	DepotWellOpsMappingLookup.prototype.validateConfirmButton = function() {
		var itemUid = $(this.dropDownSelect).val();
		if(itemUid != null && itemUid != "") {
			this.enableButton(this._confirmButton, true);
		} else {
			this.enableButton(this._confirmButton, false);
		}
	};
	
	DepotWellOpsMappingLookup.prototype._confirmButton_onClick = function(){
		var message = this.dropDownSelect.options[this.dropDownSelect.selectedIndex].getAttribute("message");
		if(message!=undefined && message!="") {
			D3.UIManager.confirm("Import Payload", message, function(){
				this.invokeCustomFilter();
			}, this._abort, this);
		} else { 
			this.invokeCustomFilter();
		}
	};
	
	DepotWellOpsMappingLookup.prototype._cancelButton_onClick = function(){ 
		this._cancelPayload();
		this.hideBox();
	};
	
	DepotWellOpsMappingLookup.prototype._abort = function(){ 
		if(this.box.hasClass("ui-dialog-content") && this.box.dialog('isOpen')){
			
		} else {
			this._cancelPayload();
		}
	};
	
	DepotWellOpsMappingLookup.prototype._cancelPayload = function(){ 
		//execute only if multi well selector is not shown
		var params = {};
		params._invokeCustomFilter = RigStateManagementNodeListener.CANCEL_PAYLOAD;
		params.tempFilePath = this._tempFilePath;
		this._commandBeanProxy.customInvoke(params,this.payloadProcessCompleted,this);
	};
	
	DepotWellOpsMappingLookup.prototype.invokeCustomFilter = function(){
		var params = {};
		params._invokeCustomFilter = "payload";
		params.uids = [$(this.dropDownSelect).val()];
		params.tempFilePath = this._tempFilePath;
		this._commandBeanProxy.customInvoke(params,this.payloadProcessCompleted,this);
		this._caller.uploadProcessCompleted(true, "Preparing to process payload...");
		this.hideBox();
	};
	
	DepotWellOpsMappingLookup.prototype.hideBox = function(){
		if(this.box.hasClass("ui-dialog-content") && this.box.dialog('isOpen')) this.box.dialog("close");
	};
	
	DepotWellOpsMappingLookup.prototype.payloadProcessCompleted = function(){	}
	
	DepotWellOpsMappingLookup.prototype.show = function() {
		var obj = $(this.dropDownSelect);
		var count = obj.get(0).options.length;
		if(count <= 2){ //empty and 1 operation
			var option = obj.get(0).options[obj.get(0).options.length - 1];
			option.setAttribute("selected", "true");
			this.validateConfirmButton();
			this._confirmButton_onClick();
		} else this.showBox();
	};
	
	DepotWellOpsMappingLookup.prototype.showBox = function() {
		this.box.dialog({
			width: "auto",
			height: "130",
			title: this.title,
			resizable: false,
			modal: true,
			closeOnEscape: false,
			open: function(event, ui) {
		        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
		    }
		});
	};
})(window);