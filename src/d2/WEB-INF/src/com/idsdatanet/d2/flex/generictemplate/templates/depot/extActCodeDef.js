(function(window) {
	
	D3.inherits(DefName,D3.AbstractFieldComponent);
	function DefName() {
		D3.AbstractFieldComponent.call(this);
	}
	
	DefName.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = DefName.uber.getFieldRenderer.call(this);
			this._fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}
		return this._fieldRenderer;
	};
	
	// on the event you click the pagelisting record, it will run
	DefName.prototype.onclick = function(event){
		// set the refFields="@selectedDef" with the selected ActExtCodeDef's uid
		this._node.parent.setFieldValue("@selectedDef", this._node.getFieldValue("activityExtCodeDefUid"));
		// after set the @selectedDef by uid, submit the root node and the target field to save/persist our change
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node.parent, "@selectedDef");
	}

	// Use a ComboBoxField (dropdowns like in commonlookup which uses inString=true, or autoComplete=true)
	D3.inherits(FilterOptionsDropDown,D3.ComboBoxField);
	function FilterOptionsDropDown() {
		D3.ComboBoxField.call(this, false);
	}

	// similar to onChange for ComboBoxField 
	// submitForServerSideProcess then submitCurrentNodeAndReload. Checkout ActivityExtCodeMappingDataNodeListener for submitForServerSideProcess
	FilterOptionsDropDown.prototype.afterDataChanged = function(event) {
		this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);
		// reloads only the main content
		this._node.parent.commandBeanProxy.submitCurrentNodeAndReload(this._node.parent);
	}
	
	// Initialize node listeners for this screen
	D3.inherits(ExternalActivityCodeDefinitionNodeListener,D3.EmptyNodeListener);
	
	function ExternalActivityCodeDefinitionNodeListener(){}
	
	// grabs all fields on screen load with customField=<name> and initialize which kind of field
	ExternalActivityCodeDefinitionNodeListener.prototype.getCustomFieldComponent = function(id, node){
		// initialize all these customFields with the ComboBoxField object above
		var filters = ["operationCode","phaseCode","taskCode","userCode","jobTypeCode","isActive"];
		if(filters.includes(id)){
			return new FilterOptionsDropDown();
		} else if(id == "defName"){
			return new DefName();
		}
		return null;
	}
	
	D3.CommandBeanProxy.nodeListener = ExternalActivityCodeDefinitionNodeListener;
	
})(window);