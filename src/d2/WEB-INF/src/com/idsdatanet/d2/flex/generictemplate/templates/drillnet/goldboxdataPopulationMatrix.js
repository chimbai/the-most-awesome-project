(function(window) {
	function DataPopulationMatrix(moduleContain, headerContain, mainContain, data){
		this.commandBeanProxy = new D3.CommandBeanProxy();
		this._moduleContain = moduleContain;
		this._headerContain = headerContain;
		this._mainContain = mainContain;
		this.showIframe = false;
		this.showpopout= false;
		this.UIblocked = false;
		this._data = data;
	};

	DataPopulationMatrix.prototype.initData = function(){
		
		var page = "webservice/goldboxmatrixservice.html?operationUid=";
		const params = new URLSearchParams(window.location.search);
        op = params.get("operationUid");
        if(op != null){
        	 page = page + op;
        }else{
        	 page = page;
        }

        var self = this;
        self.showPopout("Please wait", "Loading Content...");
        
        $.ajax({
            url: page,
            success: function (result) {	
            	
  			}    
         }).done(function(data, textStatus, jqXHR) {
             var response = JSON.parse(jqXHR.responseText);
             
             	self.initDays(response);
             	self.getBody(response);
             	self.closePopout();
                 
         }).fail(function( jqXHR, textStatus, errorThrown) {
      	   var response = JSON.parse(jqXHR.responseText);
      	   D3.UIManager.popup("Unexpected", response.message);
         });
	};

	DataPopulationMatrix.prototype.initDays = function(response){
		this._days = response.days;
		this._colSpan = this._days.length;
		this.headerTemplate = "<tbody>";
		this.headerTemplate = this.headerTemplate + "</tbody>";
		var headerContainer = $(this.headerTemplate);
		this._headerContain.appendChild(headerContainer.get(0));
	};

	DataPopulationMatrix.prototype.btn_onClick = function(item, mainItem){
		if ($(this).find("img").get(0).attributes[0].value=="images/up.gif"){
			$(this).find("img").get(0).attributes[0].value="images/down.gif";
		}else{
			$(this).find("img").get(0).attributes[0].value="images/up.gif"
		}
		$(this).nextUntil('tr.cheader').slideToggle(50);
		$(mainItem).nextUntil('tr.cheader').slideToggle(50);
	}
	DataPopulationMatrix.prototype.getHeader = function(){
	    this.headerTemplate = this.headerTemplate + "<tr>";
	 	D3.forEach(this._days,function(days){
	 		var days = days.days;
	 		var length = this._days.length;
	 		this.headerTemplate = this.headerTemplate + "<td style='width: 30px;'></td>";
	 	},this);
	 	this.headerTemplate = this.headerTemplate + "</tr>";
	}
	
	//show iframe
	DataPopulationMatrix.prototype.iframe = function(url, day, wellname, wellborename, operationname, errmsg){
		if(url){
			if(!this.showIframe){
				parent.containerManager.toggleMainBlockUI(true);
				var me = this;
				var closeFunction = function(){me.closeIframe();};
				if(Number.isInteger(parseInt(day))){
					var dialog = this.createDialog({
						closeFunction: closeFunction,
						title : wellname + " >> " + wellborename + " >> " + operationname + " on Days: " + day
					});
				}else {
					var dialog = this.createDialog({
						closeFunction: closeFunction,
						title : day + " >> " + wellname + " >> " + wellborename
					});
				}
				
				var btnDiv = document.createElement("span")
				var btnClose = D3.UIManager.createRegularButton(D3.LanguageManager.getLabel("Close"), {width: "60px"});
				btnClose.onclick = closeFunction;
				btnDiv.appendChild(btnClose);
				var headerDiv = document.createElement("div");
				headerDiv.style.textAlign = "left";
				headerDiv.style.padding = "0 5px";
				headerDiv.style.overflowY = "auto";
				headerDiv.style.maxHeight = "60px";
				if(errmsg != undefined){
					var err = '';
					errmsg = errmsg.split('::');
					errmsg.sort();
					for(var s in errmsg){
						if(errmsg[s] != ''){
							headerDiv.appendChild(document.createTextNode(errmsg[s]));	
							headerDiv.appendChild(document.createElement("br"));	
						}
					}
					headerDiv.style.color = "#ff6464";
												
				}else if(!Number.isInteger(parseInt(day))){
					if(operationname != undefined){
						var err = '';
						errmsg = operationname.split('::');
						errmsg.sort();
						for(var s in errmsg){
							if(errmsg[s] != ''){
								headerDiv.appendChild(document.createTextNode(errmsg[s]));	
								headerDiv.appendChild(document.createElement("br"));	
							}
						}
						headerDiv.style.color = "#ff6464";
					}
				}
				dialog.appendChild(headerDiv)
				
				var iframe = document.createElement("iframe");
				iframe.style.right = '0';
				iframe.style.left = '0';
				iframe.style.bottom = '0';
				iframe.style.width = '100%';
				iframe.style.height = '100%';
				iframe.src = url;
				$(iframe).on('load', function () {
					$(iframe).contents().find('head').append(
							'<style type="text/css">.RootButton { display: none !important; } '+
							'.BaseNodesGroupContainerFooter { display: none !important; }'+
							'</style>');
				});
				this.iframe = iframe;
				
				dialog.appendChild(iframe);
				
				var modal = document.createElement("div");
				modal.style.position = "fixed";
				modal.style.top = 0;
				modal.style.left = 0;
				modal.style.width = "100%";
				modal.style.height = "90%";
				modal.style.zIndex = 100;
				modal.style.backgroundColor = "rgba(0,0,0,0.2)";
				modal.appendChild(dialog);
				
				document.body.appendChild(modal);
				this.showPopout("Please Wait", "Loading Content...");
				
				this.commandBeanProxy.dispatchEvent(new D3.CommandBeanProxyEvent(D3.CommandBeanProxyEvent.REASON_EDIT_MODE_CHANGED));
				this.popup = {dialog: dialog, rootElement: modal};
				this.showIframe = true;
				this.btnClose = btnClose;
//				document.body.removeChild(d);
//				delete d;
				this.closePopout();
			} else {
				this.iframe.src = url;
			}
		}
	}
	
	DataPopulationMatrix.prototype.closePopout = function() {
		if(this.showpopout){
			this.showpopout.parentNode.removeChild(this.showpopout);
			delete this.showpopout;
		}
	}
	
	DataPopulationMatrix.prototype.showPopout = function(title, msg) {
		var e1 = document.createElement("div");
		e1.className = "ui-dialog ui-corner-all ui-widget ui-widget-content ui-front";
		e1.style.textAlign = "left";
		e1.style.position = "relative";
		e1.style.display = "inline-block";
		e1.style.top = "50%";
		e1.style.transform = "translateY(-50%)";
		e1.style.minWidth = "200px";
		var e2 = document.createElement("div");
		e2.className = "ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix";
		e1.appendChild(e2);
		var e3 = document.createElement("span");
		e3.className = "ui-dialog-title";
		e3.appendChild(document.createTextNode(title));
		e2.appendChild(e3);
		
		var e6 = document.createElement("div");
		e6.className = "ui-dialog-content ui-widget-content";
		e6.style.padding = "15px 10px";
		e6.appendChild(document.createTextNode(msg));
		e1.appendChild(e6);
		
		var d = document.createElement("div");
		d.style.position = "absolute";
		d.style.top = 0;
		d.style.left = 0;
		d.style.bottom = 0;
		d.style.right = 0;
		d.style.zIndex = 100;
		d.style.textAlign = "center";
		d.appendChild(e1);
		
		document.body.appendChild(d);
		this.showpopout = d;
	}
	
	DataPopulationMatrix.prototype.createDialog = function(options) {

		var dialog = document.createElement("div");
		var style = dialog.style;
		style.position = "absolute";
		style.backgroundColor = "#f2f2f2";
		style.boxShadow = "0px 3px 6px #949494";
		style.width = "100%";
		style.height = "100%";
		
		var titleBar = document.createElement("div");
		style = titleBar.style;
		style.padding = ".4em .8em"; 
		style.position = "relative";
		style.color = "#fff";
		style.backgroundColor = "#5c9ccc";
		style.cursor = "move";
		dialog.appendChild(titleBar);
		
		var titleText = document.createElement("div");
		style = titleText.style;
//		style.whiteSpace = "nowrap";
		style.width = "90%";
		style.overflow = "hidden";
		style.textOverflow = "ellipsis";
		style.fontFamily = "Helvetica, Verdana, Arial";
		style.fontSize = "13px";
		titleBar.appendChild(titleText);
		
		var closeButton = document.createElement("button");
		closeButton.type = "button"
		style = closeButton.style;
		style.position = "absolute";
		style.right = ".5em";
		style.top = "50%";
		style.margin = "-10px 0 0 0";
		style.width = "121px";
		style.height = "20px";
		closeButton.appendChild(document.createTextNode("Return to Matrix"));
		titleBar.appendChild(closeButton);
		
		var closeIcon = document.createElement("span");
		closeIcon.className = "ui-icon ui-icon-closethick";
		closeIcon.style.margin = "-1px 0 0 -6px";
//		closeButton.appendChild(closeIcon);

		titleText.appendChild(document.createTextNode(options.title));
		closeButton.onclick = options.closeFunction;
		
		return dialog;
	}

	DataPopulationMatrix.prototype.closeIframe = function(event) {
		delete this.popup;
		this.commandBeanProxy.reloadParentHtmlPage(true);
		this.commandBeanProxy.pageRedirect(document.referrer,D3.LanguageManager.getLabel("redirect back to previous page"));
		this.UIblocked = false;
		parent.containerManager.toggleMainBlockUI(false);
	}

	DataPopulationMatrix.prototype.getBody = function(response){
		this.moduleTemplate = "<tbody>";
		this.mainTemplate = "<tbody>";
		var self = this;
		const alltablenames = [];
		var totaldays = response.days['length'];	
		var alldays = [];
		var dailydata = [];
		
		for(var d in response.days){
			alldays[d] = {
				"dailyUid":response.days[d].dailyUid,
				"days":response.days[d].days,
			};
		}
		delete response.days;
		delete response.Status;
	 	for (var key in response) {
	 		if (key!=""){
	 			if(key == 'Daily'){
		 			this.moduleTemplate = this.moduleTemplate + "<tr class='cheader'>" +
		 			"<td valign='middle'><strong>" + key + "</strong> <img src='images/up.gif'></td>" +
		 			"</tr>";
		 			var wid = this._colSpan * 31;
		 			this.mainTemplate = this.mainTemplate + "<tr class='cheader'>";
		 			var x = 0;
		 			for (i=0; i<this._colSpan; i++) {
		 				x++;
		 				 this.mainTemplate = this.mainTemplate +
		 				"<td>"+x+"</td>";
		 			}
	 			}else if(key == 'Non-Daily'){
	 				this.moduleTemplate = this.moduleTemplate + "<tr class='cheader'>" +
		 			"<td valign='middle'><strong>" + key + "</strong> <img src='images/up.gif'></td>" +
		 			"</tr>";
		 			var wid = this._colSpan * 31;
		 			this.mainTemplate = this.mainTemplate + "<tr class='cheader'>";
		 			for (i=0; i<this._colSpan; i++) {
		 				 this.mainTemplate = this.mainTemplate +
		 				"<td>&nbsp;</td>";
		 			}
	 			}
	 		   this.mainTemplate = this.mainTemplate +  "</tr>";
	 		   var diff = null;
	 		   
	 		   for(var tablenames in response[key]){
	 			   if(!alltablenames.includes(response[key][tablenames].tableName)) alltablenames.push(response[key][tablenames].tableName);
	 			  alltablenames.sort();
	 		   }
	 		   	 	//Daily section
	 				  if(key == 'Daily'){
	 					  for(var module in response[key]){
//		 					 if(!response[key][module]['tableName'].includes(this._moduleName)){
	 						  	
			 					this._moduleName = response[key][module]['tableName'];				 				
				 				this.moduleTemplate = this.moduleTemplate + "<tr>" +
					 			"<td>" +  
					 			"<span class='fieldContainer'>" +
					 			"<div class='fieldRenderer' data-toggle='tooltip' data-placement='top' title='" + this._moduleName + "'>" + self.getModuleNameLabel(this._moduleName) + "</div>" +
					 			"</td></span>" +
					 			"</tr>";
				 				this.mainTemplate = this.mainTemplate + "<tr>";
				 				var wellname = response[key][module].wellName;
				 				var wellborename = response[key][module].wellboreName;
				 				var operationname = response[key][module].operationName;
				 				dailydata = [
				 					  ...[...alldays, ...response[key][module].days]
				 					    .reduce(
				 					      (acc, curr) => acc.set(curr.dailyUid, { ...acc.get(curr.dailyUid), ...curr }),
				 					      new Map()
				 					    )
				 					    .values(),
				 					];
				 				//loop inside the status
				 				
				 				if(response[key][module].days.length >0){
				 					//check days of each module
					 				for(var day in dailydata){
//					 					if(response[key][module].days[day] != undefined){
					 						diff = totaldays - dailydata.length;
						 					var dailyUid = dailydata[day].dailyUid;
						 					var reportNumber = dailydata[day].reportNumber;
						 					var redirecturl = '';
						 					var screen = '';
						 					var errCount = 0;
						 					var pendCount = 0;
						 					var doneCount = 0;
						 					var pubsCount = 0;
						 					let errObj = [];
						 					
//						 					if(alldays[day].dailyUid == dailyUid){
								 					if(this._moduleName == 'RigInformation') screen = "alt4_riginformation.html";
								 					else if(this._moduleName == "Bharun" || this._moduleName == "BharunDailySummary") screen = "bharunwithoutbitrun.html";
													else if(this._moduleName == "Bit") screen = "bitrun.html";
													else if(this._moduleName == "HSE") screen = "hseincident.html";
													else if(this._moduleName == "WellProductionTest") screen = "wellproductiontestalt.html";
													else if(this._moduleName == "BhaComponent") screen = "bharunwithoutbitrun.html";
													else if(this._moduleName == "ActivityRsd") screen = "activityrigstate.html";
													else if(this._moduleName == "SurveyStation") screen = "surveyreference.html";
													else if(this._moduleName == "CementJob" || this._moduleName == "CementStage" || this._moduleName == "CementFluid" || this._moduleName == "CementAdditive" ) screen = "cementjob.html";
								 					else screen = this._moduleName.toLowerCase() + ".html";
							 						redirectURL = window.location.href.substr(0, window.location.href.indexOf("goldboxmatrix.html")) + screen + window.location.href.split("goldboxmatrix.html").pop() + "&gotoday=" + dailyUid;
							 						
						 								var status = new Array();
						 								var errDetails = [];
						 								var errorcode = '';
						 								
						 								//check status of each day and count the status
						 								for(var s in dailydata[day].status){
						 									status.push(dailydata[day].status[s].status);
						 									if(dailydata[day].status[s].errorMessage != undefined && dailydata[day].status[s].errorMessage != '') {
						 										errObj.push(dailydata[day].status[s].errorMessage);
						 									}
						 									
						 									if(status[s] === 'ERROR'){
								 								errCount ++;
								 							}else if(status[s] === 'PENDING'){
								 								pendCount ++;
								 							}else if(status[s] === 'DONE'){
								 								doneCount ++;
								 							}else if(status[s] === 'PUBLISHED'){
								 								pubsCount ++;
								 							}
						 								}
						 								
						 								var index = 0;
						 								var color1 = [];
						 								var colorcode = null;
						 								var width = 0;
						 								if(errCount != 0){
						 									colorcode = '#ff6464';
						 									index = index + 1;
						 									color1.push(colorcode);
						 								}
						 								if(pendCount != 0){
						 									index = index + 1;
						 									colorcode = '#f7c602';
						 									color1.push(colorcode);
						 								}
						 								if(doneCount != 0){
						 									index = index + 1;
						 									colorcode = '#82FA58';
						 									color1.push(colorcode);
						 								}
						 								if(pubsCount != 0){
						 									index = index + 1;
						 									colorcode = '#afeeee';
						 									color1.push(colorcode);
						 								}
						 								
						 								var err = parseFloat(errCount === null ? 0 : errCount);
							 							var pend = parseFloat(pendCount === null ? 0 : pendCount);
							 							var done = parseFloat(doneCount === null ? 0 : doneCount);
							 							var pubs = parseFloat(pubsCount === null ? 0 : pubsCount);
							 							var totalcount = err + pend + done + pubs;
							 							if(index == 1){
							 								width = 34;
							 							}else if(index ==2){
							 								width = 17;
							 							}else if(index ==3){
							 								width = 11.3;
							 							}else if(index ==4){
							 								width = 8.5;
							 							}
						 								
							 							//process error message and count
								 						if (status != undefined && status.length > 0){	
								 							errmsg = "Error: " + errCount + "/" + totalcount + "\nPending: " + pendCount + "/" + totalcount + "\nDone: " + doneCount + "/" + totalcount + "\nPublished: " + pubsCount + "/" + totalcount;
								 							var err = '';
								 							var errdisplay = '';
		
								 							if(!errObj.includes(undefined) && errObj.length>0){
								 								errObj = errObj.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
									 							for(var s in errObj){
									 								err = err + s + " (" + errObj[s] + ") " +  "\n"; 
									 							}
								 							}else {
								 								err = '';
								 							}
								 							
								 							if(err != ''){
								 								errdisplay = errmsg + "\n\n" + err;
									 							err = err.replaceAll("\n", '::');
								 							}else{
								 								errdisplay = errmsg + "\n\n";
								 							}
								 							
							 								this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.iframe('"+redirectURL+"', '"+reportNumber+"', '"+wellname+"', '"+wellborename+"', '"+operationname+"', '"+err+"')\"" +
															" style='width:30px;padding:0px;' title='"+errdisplay+"'>";
							 								for(var color in color1){
							 									this.mainTemplate = this.mainTemplate + "<div>" +
																		"<div style='width:"+width+"px;height:25px;background-color:"+color1[color]+";margin:0;position:relative;float:left;'>&nbsp;</div>" +
																	"</div>";	
						 										}
							 								this.mainTemplate = this.mainTemplate + "</td>";
									 					}else{
									 						this.mainTemplate = this.mainTemplate + "<td class='grey'></td>";
									 					}
//						 						}else{
//						 							this.mainTemplate = this.mainTemplate + "<td class='grey'></td>";
//						 						}
//					 						}else{
//					 							this.mainTemplate = this.mainTemplate + "<td class='grey'></td>";
//					 						}
						 				}
						 				for(var a = 0; a<diff;a++){
				 							this.mainTemplate = this.mainTemplate + "<td class='grey'></td>";
				 						}
				 					}else{
				 						for(var b = 0; b<totaldays;b++){
				 							this.mainTemplate = this.mainTemplate + "<td class='grey'></td>";
				 						}
				 					}
//		 					 }
	 					  }	
	 					  
	 					  //non daily section
	 				  	}else if(key="Non-Daily"){
	 			 			D3.forEach(response[key],function(data){
	 			 				
	 			 				if(data.tableName !== this._moduleName){
	 			 					
	 			 					this._moduleName = data.tableName;
	 				 				this.moduleTemplate = this.moduleTemplate + "<tr>" +
	 					 			"<td>" +  
	 					 			"<span class='fieldContainer'>" +
	 					 			"<div class='fieldRenderer' data-toggle='tooltip' data-placement='top' title='" + this._moduleName + "'>" + self.getModuleNameLabel(this._moduleName) + "</div>" +
	 					 			"</td></span>" +
	 					 			"</tr>";
	 				 				this.mainTemplate = this.mainTemplate + "<tr>";
	 				 				for (var day in this._days) {
	 				 					var dailyUid = data.dailyUid;
	 				 					var status = data.status;
	 				 					var wellname = data.wellName;
	 				 					var wellborename = data.wellboreName;
	 				 					var operationname = data.operationName;
	 				 					var redirecturl = '';
	 				 					var screen = '';
	 				 					var labels = [];
	 				 					
	 				 					if(data.tableName == 'RigInformation') screen = "alt4_riginformation.html";
	 				 					else if(data.tablenames == "Bharun" || data.tablenames == "BhaComponent") screen = "bharunwithoutbitrun.html";
	 									else if(data.tablenames == "Bit") screen = "bitrun.html";
	 									else if(data.tablenames == "HSE") screen = "hseincident.html";
	 									else if(data.tablenames == "WellProductionTest") screen = "wellproductiontestalt.html";
	 									else if(data.tablenames == "ActivityRsd") screen = "activityrigstate.html";
	 									else if(data.tablenames == "SurveyStation") screen = "surveyreference.html";
	 									else if(this._moduleName == "UnwantedEvent") screen = "activitynptevent.html";
	 				 					else screen = data.tableName.toLowerCase() + ".html";
	 			 						redirectURL = window.location.href.substr(0, window.location.href.indexOf("goldboxmatrix.html")) + screen + window.location.href.split("goldboxmatrix.html").pop() + "&gotoday=" + dailyUid;
	 			 						
	 			 						if (data.status != undefined){
	 				 						if(data.status.includes("ERROR")){
	 				 							if(data.errorMessage != undefined) var errmsg = data.errorMessage;
	 				 							else errmsg = '';
	 			 								 this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.iframe('"+redirectURL+"', '"+wellname+"', '"+wellborename+"', '"+operationname+"','"+errmsg+"')\"" +
	 				 										" style='width: 32px;' class='red' colspan='"+totaldays+"' title='"+errmsg+"'>&nbsp;</td> ";
	 				 						}else {
	 				 							if(data.status.includes("PENDING")){
	 				 								 this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.iframe('"+redirectURL+"', '"+wellname+"', '"+wellborename+"', '"+operationname+"',)\"" +
	 			 										" style='width: 32px;' class='yellow' colspan='"+totaldays+"'>&nbsp;</td>";
	 				 							}else{
	 				 								if(data.status.includes("DONE")){
	 				 									this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.iframe('"+redirectURL+"', '"+wellname+"', '"+wellborename+"', '"+operationname+"',)\"" +
	 			 										" style='width: 32px;' class='green' colspan='"+totaldays+"'>&nbsp;</td>";
	 				 								}else{
	 				 									if(data.status.includes("PUBLISHED")){
	 				 										this.mainTemplate = this.mainTemplate + "<td onclick=\"window.D3.DPM.iframe('"+redirectURL+"', '"+wellname+"', '"+wellborename+"', '"+operationname+"',)\"" +
	 				 										" style='width: 32px;' class='blue' colspan='"+totaldays+"'>&nbsp;</td>";
	 				 									}else{
	 				 										this.mainTemplate = this.mainTemplate +	 "<td></td>";
	 				 									}
	 				 								}
	 				 							}
	 				 						}
	 				 					}else{
	 				 						this.mainTemplate = this.mainTemplate + "<td></td>";
	 				 					}
	 				 				};
	 				 				this.mainTemplate = this.mainTemplate + "</tr>";
	 			 				}
	 			 			},this); 
						}	
	 				  }
	 				this.mainTemplate = this.mainTemplate + "</tr>";
 				}
 
	 	this.moduleTemplate = this.moduleTemplate + "</tbody>";
	 	var moduleContainer = $(this.moduleTemplate);
	 	this._moduleContain.appendChild(moduleContainer.get(0));
	 	
	 	this.mainTemplate = this.mainTemplate + "</tbody>";
		var mainContainer = $(this.mainTemplate);
		this._mainContain.appendChild(mainContainer.get(0));
		
		var list2 = mainContainer.find(".cheader");
		this._thm = {};
		for (i=0; i<list2.length; i++) {
			this._thm[i] = list2.get(i);
		}
		
		var list = moduleContainer.find(".cheader");
			this._th = {};
			for (i=0; i<list.length; i++) {
				this._th[i] = list.get(i);
				this._th[i].onclick = this.btn_onClick.bindAsEventListener(this._th[i], this._thm[i]);
			}
		
		$(this._headerContain).css('width', this._colSpan *30 );
		$(this._mainContain).css('width', this._colSpan *30 );
		
		$('[data-toggle="tooltip"]').tooltip({
	        container: "body"
	    });
		
		//legend
		var legend = this.getStatusLegend();
		var legendtemplate = $(legend);
		for(var x = 0; x<legendtemplate.length; x++){
			this._data.appendChild(legendtemplate.get(x));
		}
	}
	
	//legend
	DataPopulationMatrix.prototype.getStatusLegend = function(){
		var psLegend = "<br/>" +
			"<table class='legend-table' cellspacing='0'>"+
				"<tr>"+
					"<td colspan='2'><B>"+D3.LanguageManager.getLabel("Data QC Status")+"</B></td>"+
				"</tr>"+
				"<tr valign='center'>"+
					"<td class='yellow'>&nbsp;</td>	"+						
					"<td nowrap style='width:5000px;'>"+
						"Pending"+
					"</td>"+
				"</tr><tr>"+
					"<td class='green'>&nbsp;</td>	"+						
					"<td nowrap style='width:5000px;'>"+
						"Done"+
					"</td>"+
				"</tr><tr>"+
					"<td class='blue'>&nbsp;</td>	"+						
					"<td nowrap style='width:5000px;'>"+
						"Published"+
					"</td>"+
				"</tr><tr>"+
					"<td class='red'>&nbsp;</td>	"+						
					"<td nowrap style='width:5000px;'>"+
						"Error"+
					"</td>"+
				"</tr>"+
				"<tr>"+
				"<td style='background-color:#808080;'>&nbsp;</td>	"+						
				"<td nowrap style='width:5000px;'>"+
					"No Data"+
				"</td>"+
				"</tr></table>";
			
//			"</table>" + "<button style='float:right;'>Remove from Sync</button>"
			
		return psLegend;
	}

	
	DataPopulationMatrix.prototype.getModuleNameLabel = function(label) {
		var result = label.replace( /([A-Z])/g, " $1");
		var finalResult = result.charAt(0).toUpperCase() + result.slice(1);
		return finalResult;
	};
	
	window.D3.DataPopulationMatrix = DataPopulationMatrix;
})(window);
