(function(window){
	D3.inherits(BitrunNumberCustomField,D3.TextInputField);
	
	function BitrunNumberCustomField(){
		D3.TextInputField.call(this);
	}
	BitrunNumberCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			fieldEditor = BitrunNumberCustomField.uber.getFieldEditor.call(this);
			fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			fieldEditor = BitrunNumberCustomField.uber.getFieldEditor.call(this);
		}			
		return fieldEditor;
	}
	BitrunNumberCustomField.prototype.onKeyUp = function(event){
		this.checkBitnumberExist();
	}
	BitrunNumberCustomField.prototype.checkBitnumberExist = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	BitrunNumberCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "checkBitrunNumberExist";
		params.name = this.getFieldEditor().value;
		params.bitrunUid = this._node.getFieldValue("bitrunUid");
		params.isNewRecord = this._node.atts.isNewRecord? "1":"";
		return params;
	}
	
	BitrunNumberCustomField.prototype.loaded = function(response) {
		var self = this;
		this.fieldContainer.clearFieldUserMsgContainer();
		var isDuplicate = false;
		$(response).find('item').each(function(){
			if ($(this).attr('name')=='duplicate'){
				if ($(this).attr('value')=='true'){
					isDuplicate = true;
				}
			}
		});
		if (isDuplicate){
			var msg = D3.LanguageManager.getLabel("warning.bitrunNumberExist");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);

		}	
	}
	
	D3.inherits(BharunNumberLinkField,D3.HyperLinkField);
	
	function BharunNumberLinkField() {
		D3.HyperLinkField.call(this);
		this._linkButton = null;
	}
	
	BharunNumberLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var bharun = this._node.getFieldValue("@bharun");
			data = bharun.split("&");
			var params = {};
			for (var i=0;i<data.length;i++){
				 var parts = data[i].split("=");
				 params[parts[0]] = parts[1];
			}
		   

			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				css:"bitrun-content",
				context:this
			});
			this._linkButton = D3.UI.createElement({
				tag:"a",
				text:this._config.label,
				css:"bharunLink",
				data:{
					bharun:params.bharunUid,
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = "bharunwithoutbitrun.html?&gotoFieldName=bharunUid&gotoFieldUid=" + data.bharun;
						var msg = D3.LanguageManager.getLabel("message.bharunNumberLinkField");
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
			this._fieldRenderer.appendChild(this._linkButton);
		}
		return this._fieldRenderer;
	}
	
	BharunNumberLinkField.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(BitrunNodeListener,D3.EmptyNodeListener);
	
	function BitrunNodeListener() {
	}
		
	BitrunNodeListener.prototype.customButtonTriggered = function(node, id, button){
		if (id == "copy_bit_run_from_previous_run") {
			this.copyBitrunFromPreviousRunPopup(node, id, button);
		}
	}
	
	BitrunNodeListener.prototype.copyBitrunFromPreviousRunPopup = function(node, id, button){
		
		this._node = node;
		this._pnlEditor = this.createElement("div");
		var datumHBox = this.createElement("div",null,"centerdiv");	
		datumHBox.appendChild(this.getMainContain());
		this._pnlEditor.appendChild(datumHBox);
		this._pnlEditor.self = this;

		var btnConfirm = {
				text:D3.LanguageManager.getLabel("button.confirm"),
				"class":"PopupConfirmButton",
				click:function(){
					$(this).dialog('close');
					$(this).get(0).self.btnConfirm_onClick(node);
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				"class":"PopupCancelButton",
				click:function(){
	                $(this).dialog('close');
				}
		};
		var btnClose = {
				text:D3.LanguageManager.getLabel("button.close"),
				"class":"RootButton",
				click:function(){
	                $(this).dialog('close');
				}
		};
		$(this._pnlEditor).dialog({
	        resizable: false,
	        width:300,
	        height:120,
	        modal: true,
	        draggable:false,
	        title: D3.LanguageManager.getLabel("popup.bitrunFromPrevTitle"),	        
	        open: function(event, ui) { 
	            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
	        },
	    });
		if (this._bitNumber._items.length>0){
			$(this._pnlEditor).dialog('option','buttons',[btnConfirm,btnCancel]);
		}else{
			$(this._pnlEditor).dialog('option','buttons',[btnClose]);
		}
	}
	BitrunNodeListener.prototype.btnConfirm_onClick = function(node){
		node.commandBeanProxy.addAdditionalFormRequestParams("copy_bit_run_from_previous_run", this._combo.value);
		node.commandBeanProxy.addAdditionalFormRequestParams("_action", "copy_bit_run_from_previous_run");
		node.commandBeanProxy.submitForServerSideProcess(node, null, true);
	}
	BitrunNodeListener.prototype.btnCancel_onClick = function(node){
		node.cancelAllActions();
	}
	BitrunNodeListener.prototype.getMainContain = function(){
		this._bitNumber = this.loadBitNumber();
		var main = this.createElement("div",null,"VBox");
		if (this._bitNumber._items.length > 0){
			main.appendChild(this.createElement("label",D3.LanguageManager.getLabel("popup.bitLabel")+"  ","popupText"));
			this._combo = this.createElement("select",null,"fieldEditor comboBox");
			this._bitNumber.forEachActiveLookup(this, function(item) {
				if (item && !item.firstBlankItem) {
					var option = document.createElement("option");
					option.value = item.data;
					option.appendChild(document.createTextNode(item.label));
					this._combo.appendChild(option);
				}
			});
			main.appendChild(this._combo);
		}else{
			main.appendChild(this.createElement("label",D3.LanguageManager.getLabel("warning.noBitrunNumber"),"popupText"));
		}
		return main;
	}
	BitrunNodeListener.prototype.loadBitNumber = function(){
		var params = {};
		
		params._invokeCustomFilter = "CopyBitRunNumber";
		params._action = "copy_bit_run_from_previous_run";
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();

		$(response).find('item').each(function(){
			var newItem = new D3.LookupItem();
			newItem.data = $(this).attr('key');
			newItem.label = $(this).attr('value');
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}

	BitrunNodeListener.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	BitrunNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "copy_bit_run_from_previous_run") {
			btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>")
			
			return btn.get(0);
		} else {
			return null;
		}
	}

	BitrunNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id=="bharunNumLink"){
			return new BharunNumberLinkField();
		}else if (id=="bitrunNumber") {
			return new BitrunNumberCustomField();
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = BitrunNodeListener;
	
})(window);