(function(window){
	D3.inherits(AssociateAfeButton,D3.AbstractFieldComponent);
	
	function AssociateAfeButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	AssociateAfeButton.prototype.getFieldRenderer = function(){

		this._fieldRenderer = D3.UI.createElement({
			tag:"button", 
			text:"Associate AFE",
			css:"RootButton",
			callbacks:{
				onclick:this.associateAfe
			},
			context:this
		}) ;
		this._fieldRenderer.setAttribute('style', 'width:150px');
		return this._fieldRenderer;
		
	}
	
	AssociateAfeButton.prototype.associateAfe = function(){
		this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_action","_associate_afe");
		this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_costAfeMasterUid", this._node.getFieldValue("costAfeMasterUid"));	
		this._node.commandBeanProxy.submitForServerSideProcess();
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	AssociateAfeButton.prototype.refreshFieldRenderer = function(){
	}
	
	
	D3.inherits(DuplicateAfeButton,D3.AbstractFieldComponent);
	
	function DuplicateAfeButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	DuplicateAfeButton.prototype.getFieldRenderer = function(){

		this._fieldRenderer = D3.UI.createElement({
			tag:"button",
			text:"Duplicate this AFE",
			css:"RootButton",
			callbacks:{
				onclick:this.duplicateAfe
			},
			context:this
		}) ;
		this._fieldRenderer.setAttribute('style', 'width:150px');
		return this._fieldRenderer;
		
	}
	
	DuplicateAfeButton.prototype.duplicateAfe = function(){
		var wantDup  = confirm("Are you sure you want to Duplicate this AFE?");
		if (wantDup){
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_action","_duplicate_afe");
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_costAfeMasterUid", this._node.getFieldValue("costAfeMasterUid"));	
			this._node.commandBeanProxy.submitForServerSideProcess();
			this._node.commandBeanProxy.reloadParentHtmlPage(false);
		}
	}
	
	DuplicateAfeButton.prototype.refreshFieldRenderer = function(){
	}
	
	
	D3.inherits(setMasterButton,D3.AbstractFieldComponent);
	
	function setMasterButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	setMasterButton.prototype.getFieldRenderer = function(){

		this._fieldRenderer = D3.UI.createElement({
			tag:"button",
			text:"Set as Master",
			css:"RootButton",
			callbacks:{
				onclick:this.duplicateAfe
			},
			context:this
		}) ;
		this._fieldRenderer.setAttribute('style', 'width:150px');
		return this._fieldRenderer;
		
	}
	
	setMasterButton.prototype.duplicateAfe = function(){
		var wantSet  = confirm("Are you sure you want to Set this AFE as Master?");
		if (wantSet){
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_action","_set_as_master_afe");
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_costAfeMasterUid", this._node.getFieldValue("costAfeMasterUid"));
			this._node.commandBeanProxy.submitForServerSideProcess();
			this._node.commandBeanProxy.reloadParentHtmlPage(false);
		}
	}
	
	setMasterButton.prototype.refreshFieldRenderer = function(){
	}
	
	
	D3.inherits(costAfeDetailLinkField,D3.AbstractFieldComponent);
	
	function costAfeDetailLinkField(){
		D3.AbstractFieldComponent.call(this);
	}
	
	costAfeDetailLinkField.prototype.getFieldRenderer = function(){
		if (this._renderer == null){
			this._renderer = "";
		}
		return this._renderer;
		
	}
	
	costAfeDetailLinkField.prototype.createRenderer = function(){

	}
	
	
	costAfeDetailLinkField.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(operationsLinkField,D3.AbstractFieldComponent);
	
	function operationsLinkField(){
		D3.AbstractFieldComponent.call(this);
	}
	
	operationsLinkField.prototype.getFieldRenderer = function(){
		this._fieldRenderer = this.showFTRText();
		return this._fieldRenderer;
		
	}
	
	operationsLinkField.prototype.showFTRText = function() {
		var contConfig = {
				tag:"table",
			//	css:"reportList-content"
		}
		var content = D3.UI.createElement(contConfig);
		
		
		    var opsLists = this._node.getFieldValue("@operationUids");
		    var opsList = opsLists.split("\n");

		    var tableConfig = "";
		    var self = this;
		    for (var i = 0; i < opsList.length; i++) {
		    	if (opsList[i] != ""){
			    	var ops = opsList[i].split("::");
			    	
			    	tableConfig =  "<div id='" +  ops[0] + "' opsid='"+ ops[1] + "' class='ui-icon ui-icon-close'>Remove Tab</div>" + ops[0] +" " +  ops[1] +" " + ops[2] ;
			    	var renderer = $(tableConfig);
			    	var fieldRenderer = D3.UI.createElement({
						tag:"button",
						text:"x",
						css:"",
						attributes:{
							title:"Remove",
						},
						data:{
							costid:ops[0],
							opsid:ops[1]
						},
						callbacks:{
							//onclick:this.testf
							onclick:function(event){
								var wantDel  = confirm("Are you sure you want to remove selected operation?");
								if (wantDel) {
									var button = event.target;
									var data = $(button).data();
									//this.testf(data.costid);
									this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_action","_remove_operation_afe");
									this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_operationAfeUid", data.costid);	
									this._node.commandBeanProxy.submitForServerSideProcess();
									this._node.commandBeanProxy.reloadParentHtmlPage(false);
								}
							}
						},
						context:this
					}) ;
			    	fieldRenderer.style = "border: none; vertical-align: top;";
			    	
			    	var tr = document.createElement("tr");
			    	var td = document.createElement("td");
			    	
			    	tr.appendChild(td);
			    	td.appendChild(fieldRenderer);
			    	
			    //	content.appendChild(fieldRenderer);
			    	var label = document.createElement("label");
			    	label.style="width:90%; word-wrap: break-word; white-space: initial;";

					label.appendChild(document.createTextNode(ops[2]));
					td.appendChild(label);
					
					content.appendChild(tr);
			    //	content.appendChild(label);
			    //	content.appendChild(document.createElement("br"));
		    	}
		    }

			
		
		this._renderer = content;
		return this._renderer;
	}
	
	operationsLinkField.prototype.testf = function(x) {
		
		alert(x);
	}
	
	operationsLinkField.prototype.refreshFieldRenderer = function(){
	}
	
	
	D3.inherits(CostAfeListNodeListener,D3.EmptyNodeListener);
	
	function CostAfeListNodeListener() {
	}
	
	CostAfeListNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "associate_afe_button"){
			return new AssociateAfeButton();
		}else if(id == "duplicate_afe_button"){
			return new DuplicateAfeButton();
		}else if (id == "set_as_master_afe") {
			return new setMasterButton();
		}else if (id == "cost_afe_detail_link"){
			return new costAfeDetailLinkField()
		}else if (id == "operations_link"){
			return new operationsLinkField()
		}
	}

	
	D3.CommandBeanProxy.nodeListener = CostAfeListNodeListener;
	
})(window);