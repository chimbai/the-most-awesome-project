package com.idsdatanet.d2.flex.dat;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.CementFluid;
import com.idsdatanet.d2.core.model.CostAfeDetail;
import com.idsdatanet.d2.core.model.CostCurrencyLookup;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DatFilter;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.PlugAndAbandonDetail;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class DatUtils extends BlazeRemoteClassSupport{

	private final Integer ROW_PER_PAGE = 200;
	private final Integer LIMIT_NUMBER_OPERATIONS = 3000;
	private final String[] monthName = new String [] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	
	private Locale getLocale(HttpServletRequest request) throws Exception {
		return UserSession.getInstance(request).getUserLocale();
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString().trim();
		}
		return "";
	}
	private Double nullToZero(Double value) {
		if (value==null) return 0.0;
		return value;
	}
	private double getPhaseTotalByCode(String opsid, String code ) throws Exception {
		double avalue;
		String[] paramsFields = {"opsid","code"};
		Object[] paramsValues = {opsid,code};
		
		String phasetotalquery = "SELECT SUM(quantity * item_cost) AS phase_total FROM CostDailysheet " +
								 "WHERE (isDeleted=false or isDeleted is null) " +	
								 "AND operationUid = :opsid AND phase_code = :code";	
		
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(phasetotalquery,paramsFields, paramsValues);
		if (result.get(0) != null){
			avalue = (Double) result.get(0);
		return avalue;
		}
		return 0.0;
	}
	
	public String queryCustomHtmlToFlex(String callFunctionName, String filters) {
		try {
			DatCustomHtmlToFlexObject query = new DatCustomHtmlToFlexObject(); 
			query.setCallFunction(callFunctionName);
			query.setFilter(filters);
			query._httpRequest = this.getCurrentHttpRequest();
			return query.getHtmlResult();
		} catch (Exception e) {
			e.printStackTrace();
			return "<html><errorMessage>"+e.getMessage()+"</errorMessage></html>";
		}
	}
	
	private double getDurationBasedOnUserCode(String opsid, String code ) throws Exception {
		double avalue;
		String[] paramsFields = {"opsid","code"};
		Object[] paramsValues = {opsid,code};
		
		String strSql = "SELECT SUM(activityDuration) FROM Activity " +
						"WHERE (isDeleted = false or isDeleted is null) " +
						"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
						"AND (isSimop=false or isSimop is null) " +
						"AND operationUid = :opsid " +
						"AND userCode = :code " +
						"AND (isOffline=false or isOffline is null)";
						
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields, paramsValues);
		if (result.get(0) != null){
			avalue = (Double) result.get(0);
		return avalue;
		}
		return 0.0;
	}
	private Double calculateActivityDurationByPhase (String opsid,String code) throws Exception {
		
		String strSql = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND operationUid = :opsid " +
					"AND phaseCode = :code " +
					"AND (isOffline=false or isOffline is null)";

		String[] paramsFields = {"opsid","code"};
		Object[] paramsValues = {opsid,code};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields, paramsValues);
		Object a = (Object) lstResult.get(0);
		Double totalActivityDuration = 0.00;
		if (a != null) totalActivityDuration = Double.parseDouble(a.toString());
		
		return totalActivityDuration;
		
	}
	public String getCannedQueryFilterOptions(String strName, String strSql, String className, String fieldName, String datatype, String lookup) {
		String xml = "<"+strName+">";
		Map<String, LookupItem> lookupList = new HashMap<String, LookupItem>();
		CustomFieldUom thisConverter = null;
		try {			
			if (!"".equals(lookup)) {
				String[] splitLookup = StringUtils.split(StringUtils.deleteWhitespace(lookup), "|");
				try {
					for (String lookupURI : splitLookup) {
						try {
							UserSession session = this.getCurrentUserSession();
							if (!"".equals(lookup)) {
								Map<String, LookupItem> splitLookupList = LookupManager.getConfiguredInstance().getLookup(lookupURI, new UserSelectionSnapshot(session), new LookupCache());
								for (Map.Entry<String, LookupItem> item : splitLookupList.entrySet()) {
									lookupList.put(item.getKey(), item.getValue());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					xml= "<"+strName+"/>";
				}
			}
			
			try {
				if ("number".equals(datatype)) {
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Class.forName("com.idsdatanet.d2.core.model."+className), fieldName);
				}
			} catch (Exception e){
				e.printStackTrace();
			}
			try {
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
				writer.startElement(strName);
				for (Object value:rs) {
					if (value==null) continue;
					if ("".equals(value)) continue;
					String strVal = value.toString();
					String sortVal = value.toString();
					SimpleAttributes atts = new SimpleAttributes();
					atts.addAttribute("value", strVal);
					if ("number".equals(datatype)) {
						if (thisConverter!=null) {
							strVal = thisConverter.getFormattedValue(Double.parseDouble(strVal));
							String uom = thisConverter.getUomSymbol();
							strVal = strVal.replaceFirst(uom, "");
							strVal = strVal.trim();
						}
					} else if ("date".equals(datatype)) {
						SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
						strVal = df.format(value);
					}
					String label = strVal;
					if ("operationUid".equals(strName)) {
						label = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), strVal, null, true);
						sortVal = label; 
					} else {
						if (lookupList!=null) {
							if (lookupList.get(strVal)!=null) {
								LookupItem item = lookupList.get(strVal);
								if (!"".equals(item.getValue().toString())) {
									label = item.getValue().toString();
									sortVal = label; 
								}
							}
						}
					}
					atts.addAttribute("label", label);
					atts.addAttribute("labelSort", sortVal);
					writer.addElement("selection", null, atts);
				}
				writer.close();
				xml = bytes.toString("utf-8");
			} catch (Exception e) {
				e.printStackTrace();
				xml = "<"+strName+"/>";
			}
				
			
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<"+strName+"/>";
		}
		
		return xml;
	}
	
	Double convertTo12Or24Day(Double duration, OperationUomContext operationUomContext) throws Exception{//based on each custome
		try {
			CustomFieldUom durationConverter = new CustomFieldUom(this.getCurrentHttpRequest().getLocale(), Operation.class, "daysSpentPriorToSpud", operationUomContext);
			if (duration!=null && duration >= 0){
				durationConverter.setBaseValue(nullToZero(duration));
				return durationConverter.getConvertedValue();		
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.00;
	}
	
	public String getActivityCodeList() {
		String xml = "<root/>";
		String[] tableNames = {"LookupClassCode", "LookupPhaseCode", "LookupTaskCode", "LookupRootCauseCode", "LookupUserCode", "LookupJobType"};
		
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			String lookup = "xml://operationtype?key=code&amp;value=label";
			Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(lookup, new UserSelectionSnapshot(this.getCurrentUserSession()), new LookupCache());
			for (String tableName : tableNames) {
				List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select shortCode, name from " + tableName + " where (isDeleted = false or isDeleted is null) group by shortCode, name");
				for (Object[] obj: objs) {
					String shortCode = (String) obj[0];
					String name = (String) obj[1];
					
					writer.startElement(tableName);
					writer.addElement("shortCode", shortCode);
					writer.addElement("name", name + " (" + shortCode + ")");
					
					List<String> objs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select operationCode from " + tableName + " " +
							"where (isDeleted = false or isDeleted is null) " +
							"and shortCode=:shortCode " +
							"and name=:name " +
							"group by operationCode",new String[]{"shortCode","name"}, new Object[]{shortCode, name});
					for (String operationCode: objs2) {
						if (StringUtils.isNotBlank(operationCode)) {
							writer.addElement("operationCode", operationCode);
						} else {
							for (Map.Entry<String, LookupItem> item : lookupList.entrySet()) {
								writer.addElement("operationCode", item.getKey());
							}
						}
					}
					writer.endElement();
				}
				
				/*String fieldName = "";
				if ("LookupClassCode".equals(tableName)) {
					fieldName = "classCode";
				} else if ("LookupPhaseCode".equals(tableName)) {
					fieldName = "phaseCode";
				} else if ("LookupTaskCode".equals(tableName)) {
					fieldName = "taskCode";
				} else if ("LookupRootCauseCode".equals(tableName)) {
					fieldName = "rootCauseCode";
				}
				if (!"".equals(fieldName)) {
					String strSql = "select a." + fieldName + ", o.operationCode from Activity a, Operation o where (a.isDeleted = false or a.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null) and a.operationUid=o.operationUid group by a." + fieldName + ", o.operationCode order by a." + fieldName;
					List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
					for (Object[] obj: list) {
						if (obj[0]!=null && obj[1]!=null) {
							String shortCode = obj[0].toString();
							String operationCode = obj[1].toString();
							String key = operationCode + "::" + shortCode;
							
							if (StringUtils.isNotBlank(shortCode) && !codes.containsKey(tableName+key) && !codes.containsKey(tableName+"::" + shortCode)) {
								writer.startElement(tableName);
								String operationName = operationCode;
								if (lookupList.containsKey(operationCode)) {
									operationName = (String) lookupList.get(operationCode).getValue();
								}
								writer.addElement("key", key);
								writer.addElement("operationCode", operationCode);
								writer.addElement("operationName", operationName);
								writer.addElement("shortCode", shortCode);
								writer.addElement("name", shortCode + " [" + operationName + "] * [OLD]");
								writer.endElement();
								codes.put(tableName+key, key);
							}
						}
					}
				}*/
			}
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		
		return xml;
	}
	
	public String getFilterDefaultValues() {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			QueryProperties qp = new QueryProperties();
			//qp.setDatumConversionEnabled(false);
			
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			
			String gwpValue = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "startDateIsNumberDaysBeforeEndDate");
			Integer numberOfDays = 0;
			if (StringUtils.isNotBlank(gwpValue)) {
				numberOfDays = Integer.parseInt(gwpValue);
			}
			
			String uri = "db://LookupPurposeType?key=code&value=name&order=name";
			Map<String, LookupItem> purposeLookup = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			uri = "db://RigInformation?key=rigInformationUid&value=rigName";
			Map<String, LookupItem> rigLookup = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			uri = "xml://operationtype?key=code&value=label";
			Map<String, LookupItem> operationCodeLookup = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(this.getCurrentUserSession()), null);
			
			String strSql = "SELECT sum(coalesce(daycompletioncost,0.0) + coalesce(daycost,0.0)), min(reportDatetime), max(reportDatetime) from ReportDaily where (isDeleted=false or isDeleted is null) " +
					"and operationUid in (select operationUid from Operation where (isDeleted=false or isDeleted is null))";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			if (list.size()>0) {
				Object[] rec = list.get(0);
				writer.addElement("totalCost", this.nullToEmptyString(rec[0]));
				SimpleAttributes attributes = new SimpleAttributes();
				if (numberOfDays>0) {
					String minDate = df.format(new Date());
					if (rec[2]!=null) {
						Date date = (Date) rec[2];
						date = DateUtils.addDays(date, numberOfDays * -1);
						minDate = df.format(date);
					}
					attributes.addAttribute("min", minDate);
				} else {
					attributes.addAttribute("min", df.format(rec[1]==null?new Date():(Date) rec[1]));
				}
				attributes.addAttribute("max", df.format(rec[2]==null?new Date():(Date) rec[2]));
				writer.addElement("startDate", "", attributes);
			}
			strSql = "SELECT min(spudDate), max(spudDate), sum(coalesce(afe,0.0)) from Operation where (isDeleted=false or isDeleted is null)";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
			if (list.size()>0) {
				Object[] rec = list.get(0);
				SimpleAttributes attributes = new SimpleAttributes();
				if (numberOfDays>0) {
					String minDate = df.format(new Date());
					if (rec[1]!=null) {
						Date date = (Date) rec[1];
						date = DateUtils.addDays(date, numberOfDays * -1);
						minDate = df.format(date);
					}
					attributes.addAttribute("min", minDate);
				} else {
					attributes.addAttribute("min", df.format(rec[0]==null?new Date():(Date) rec[0]));
				}
				attributes.addAttribute("max", df.format(rec[1]==null?new Date():(Date) rec[1]));
				writer.addElement("spudDate", "", attributes);
				writer.addElement("afeTotal", this.nullToEmptyString(rec[2]));
			}
			
			strSql = "SELECT min(coalesce(plannedTdMdMsl,0.0)),max(coalesce(plannedTdMdMsl,0.0)), " +
					"min(coalesce(plannedTdTvdMsl,0.0)), max(coalesce(plannedTdTvdMsl,0.0)) " +
					"from Wellbore where (isDeleted=false or isDeleted is null) " +
					"and wellboreUid in (select wellboreUid from Operation where (isDeleted=false or isDeleted is null))";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
			if (list.size()>0) {
				Object[] rec = list.get(0);
				SimpleAttributes attributes = new SimpleAttributes();
				attributes.addAttribute("min", this.nullToEmptyString(rec[0]));
				attributes.addAttribute("max", this.nullToEmptyString(rec[1]));
				writer.addElement("tdMdMsl", "", attributes);

				attributes = new SimpleAttributes();
				attributes.addAttribute("min", this.nullToEmptyString(rec[2]));
				attributes.addAttribute("max", this.nullToEmptyString(rec[3]));
				writer.addElement("tdTvdMsl", "", attributes);
			}
			strSql = "SELECT min(waterDepth), max(waterDepth) from Well where (isDeleted=false or isDeleted is null) " +
					"and wellUid in (select wellUid from Operation where (isDeleted=false or isDeleted is null))";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
			if (list.size()>0) {
				Object[] rec = list.get(0);
				SimpleAttributes attributes = new SimpleAttributes();
				attributes.addAttribute("min", this.nullToEmptyString(rec[0]));
				attributes.addAttribute("max", this.nullToEmptyString(rec[1]));
				writer.addElement("waterDepth", "", attributes);
			}
			
			this.addFilterList(writer, "operationCode", "Operation", "operationCode", operationCodeLookup);
			this.addFilterList(writer, "rigName", "Operation", "rigInformationUid", rigLookup);
			this.addFilterList(writer, "purposeType", "Well", "purposeType", purposeLookup);
			this.addFilterList(writer, "country", "Well", "country", null);
			this.addFilterList(writer, "field", "Well", "field", null);
			this.addFilterList(writer, "block", "Well", "block", null);
			
			writer.addElement("onOffShore", nullToEmptyString(GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "defaultOnOffShore")));
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	private void addFilterList(SimpleXmlWriter writer, String tagName, String table, String fieldName, Map<String, LookupItem> lookupMap) throws Exception {
		String operationConditions = "";
		if ("ReportDaily".equals(table)) {
			operationConditions = " and operationUid in (select operationUid from Operation where (isDeleted=false or isDeleted is null))";
		} else if ("Well".equals(table)) {	
			operationConditions = " and wellUid in (select wellUid from Operation where (isDeleted=false or isDeleted is null))";
		}
		String strSql = "SELECT " + fieldName + " from " + table + " where (isDeleted=false or isDeleted is null) " + operationConditions + " group by " + fieldName;
		List<String> strList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		writer.startElement(tagName);
		ArrayList list = new ArrayList();
		for (String result : strList) {
			if (StringUtils.isBlank(result)) continue;
			String[] values = result.split("\t");
			for (String value: values) {
				if (StringUtils.isBlank(value)) continue;
				value = value.trim();
				if (list.contains(value)) continue;
				list.add(value);
				String label = value;
				if (lookupMap!=null) {
					if (lookupMap.containsKey(value)) {
						label = lookupMap.get(value).getValue().toString();
					}
				}
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("label", label);
				attr.addAttribute("value", value);
				writer.addElement("List", value, attr);
			}
		}
		writer.endElement();
	}
	
	public String getWellDetailList(String[] operationUidList) {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			String strOperationUids = StringUtils.join(operationUidList, "','"); 
			String strSql = "FROM Well w, Wellbore wb, Operation o " +
				"WHERE (w.isDeleted=false or w.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (o.isDeleted=false or o.isDeleted is null) " +
				"AND w.wellUid=wb.wellUid " +
				"AND wb.wellboreUid=o.wellboreUid " +
				"AND o.operationUid in ('"+strOperationUids+"') ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (Object[] rec : list) {
				Well well = (Well) rec[0];
				Operation operation = (Operation) rec[2];
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("wellUid", well.getWellUid());
				attr.addAttribute("wellName", well.getWellName());
				if ("1".equals(GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "showMapInDat"))) {
					attr.addAttribute("latNs", nullToEmptyString(well.getLatNs()));
					attr.addAttribute("latDeg", nullToEmptyString(well.getLatDeg()));
					attr.addAttribute("latMinute", nullToEmptyString(well.getLatMinute()));
					attr.addAttribute("latSecond", nullToEmptyString(well.getLatSecond()));
					attr.addAttribute("longEw", nullToEmptyString(well.getLongEw()));
					attr.addAttribute("longDeg", nullToEmptyString(well.getLongDeg()));
					attr.addAttribute("longMinute", nullToEmptyString(well.getLongMinute()));
					attr.addAttribute("longSecond", nullToEmptyString(well.getLongSecond()));
				}
				attr.addAttribute("operationUid", operation.getOperationUid());
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operation.getOperationUid(), null, true);
				if (StringUtils.isBlank(operationName)) operationName = operation.getOperationName();
				attr.addAttribute("operationName", operationName);
				attr.addAttribute("sortName", WellNameUtil.getPaddedStr(operationName));
				writer.addElement("Operation", null, attr);
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	public String getWellList(String wellNameLike, String extraSeachConditionXML, Integer pageNo) {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
			
			String extraSeachCondition = "";
			if (!"".equals(extraSeachConditionXML)) {
				SimpleXMLElement filtersXml = SimpleXMLElement.loadXMLString(extraSeachConditionXML);
				String onOffShoreCondition = "";
				String reportDailyCondition = "";
				for(Iterator i = filtersXml.getChild().iterator(); i.hasNext(); ){
					SimpleXMLElement filter = (SimpleXMLElement) i.next();
					String fieldName =  filter.getTagName();
					if ("onShore".equals(fieldName)) {
						if ("true".equals(filter.getText())) onOffShoreCondition += ("".equals(onOffShoreCondition)?"":" OR ") + "w.onOffShore='ON'";;
					} else if ("offShore".equals(fieldName)) {
						if ("true".equals(filter.getText())) onOffShoreCondition += ("".equals(onOffShoreCondition)?"":" OR ") + "w.onOffShore='OFF'";
					} else {
						if ("dateRange".equals(filter.getAttribute("type")) || "slider".equals(filter.getAttribute("type"))) {
							String minValue = filter.getAttribute("min");
							String maxValue = filter.getAttribute("max");
							if ("totalCost".equals(fieldName)) {
								reportDailyCondition += " AND (sum(coalesce(rd.daycompletioncost,0.0) + coalesce(rd.daycost,0.0)) between '"+minValue+"' and '"+maxValue+"')";
								continue;
							} else if ("startDate".equals(fieldName)) {
								reportDailyCondition += " AND (rd.reportDatetime between '"+minValue+" 00:00:00' and '"+maxValue+" 23:59:59')";
								continue;
							} else if ("afeTotal".equals(fieldName)) {
								fieldName = "o.afe";
							}
							else if ("tdMdMsl".equals(fieldName))  {
								thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdMdMsl");
								thisConverter.setBaseValueFromUserValue(Double.parseDouble(minValue));
								minValue = (thisConverter.getBasevalue() - 1.0) + "";
								thisConverter.setBaseValueFromUserValue(Double.parseDouble(maxValue));
								maxValue = (thisConverter.getBasevalue() + 1.0) + "";
								fieldName = "wb.plannedTdMdMsl";
							}
							else if ("tdTvdMsl".equals(fieldName)) {
								thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdTvdMsl");
								thisConverter.setBaseValueFromUserValue(Double.parseDouble(minValue));
								minValue = (thisConverter.getBasevalue() - 1.0) + "";
								thisConverter.setBaseValueFromUserValue(Double.parseDouble(maxValue));
								maxValue = (thisConverter.getBasevalue() + 1.0) + "";
								fieldName = "wb.plannedTdTvdMsl";
							}
							else if ("spudDate".equals(fieldName)) {
								fieldName = "o.spudDate";
							}
							else {
								thisConverter.setReferenceMappingField(Well.class, fieldName);
								if (thisConverter.isUOMMappingAvailable()) {
									thisConverter.setBaseValueFromUserValue(Double.parseDouble(minValue));
									minValue = (thisConverter.getBasevalue() - 1.0) + "";
									thisConverter.setBaseValueFromUserValue(Double.parseDouble(maxValue));
									maxValue = (thisConverter.getBasevalue() + 1.0) + "";
								}
								fieldName = "w." + fieldName;
							}
							
							if ("dateRange".equals(filter.getAttribute("type"))) {
								extraSeachCondition += " AND ("+fieldName+" between '"+minValue+" 00:00:00' and '"+maxValue+" 23:59:59' or "+fieldName+" is null) ";
							} else {
								extraSeachCondition += " AND (("+fieldName+">='"+minValue+"' and "+fieldName+"<='"+maxValue+"') or "+fieldName+" is null) ";
							}
						} else if ("list".equals(filter.getAttribute("type"))) {
							String selectedList = "";
							if ("rigName".equals(fieldName)) {
								fieldName = "o.rigInformationUid";
							} else if ("operationCode".equals(fieldName)) {
								fieldName = "o.operationCode";
							} else {
								fieldName = "w."+fieldName;
							}
							for(Iterator j = filter.getChild().iterator(); j.hasNext(); ){
								SimpleXMLElement selectedValue = (SimpleXMLElement) j.next();
								if ("item".equals(selectedValue.getTagName())) {
									String value = selectedValue.getAttribute("value");
									value = value.replace("'", "\'");
									if (!"".equals(selectedList)) selectedList += " OR ";
									selectedList += fieldName + "='"+value+"'";
									selectedList += " OR " + fieldName + " like '"+value+"\t%'";
									selectedList += " OR " + fieldName + " like '%\t"+value+"'";
									selectedList += " OR " + fieldName + " like '%\t"+value+"\t%'";
								}
							}
							
							if (!"".equals(selectedList)) extraSeachCondition += " AND ("+selectedList+")";
							
						}
					}
				}
				if ("".equals(onOffShoreCondition)) onOffShoreCondition = "w.onOffShore is null";
				extraSeachCondition += " AND ("+onOffShoreCondition+")";
				
				if (!"".equals(reportDailyCondition)) {
					reportDailyCondition = "select rd.operationUid from ReportDaily rd where (rd.isDeleted=false or rd.isDeleted is null) " + reportDailyCondition;
					extraSeachCondition += " AND o.operationUid in ("+reportDailyCondition+")";
				}
				
			}
			//apply Ops Team and tight hole filter
			String allOperationsNotAssignedToOpsTeam = "o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE " + 
					"e.opsTeamUid = a.opsTeamUid AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR ";
			if (BooleanUtils.isTrue(this.getCurrentUserSession().getCurrentUser().getAccessToOpsTeamOperationsOnly())) {
				allOperationsNotAssignedToOpsTeam = "";
			}
			extraSeachCondition += " AND (o.isCampaignOverviewOperation = '0' OR o.isCampaignOverviewOperation IS NULL) AND o.sysOperationSubclass='well' AND " + 
					"(((o.tightHole = '0' OR o.tightHole IS NULL) AND (" + allOperationsNotAssignedToOpsTeam + "o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " + 
					"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " + 
					"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = '1' AND o.operationUid IN " + 
					"(SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid))) ";
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDatumConversionEnabled(false);
			
			writer.startElement("root");
			String strSql = "SELECT COUNT(*) FROM Well w, Wellbore wb, Operation o " +
					"WHERE (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND (w.wellName like :keyword or wb.wellboreName like :keyword or o.operationName like :keyword) " + 
					extraSeachCondition;
			List<Long> countList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"keyword", "userUid"}, new Object[]{wellNameLike, this.getCurrentUserSession().getUserUid()});
			Long totalRecords = new Long("0");
			if (countList.size()>0) totalRecords = countList.get(0);
			Long pages = (totalRecords - (totalRecords % ROW_PER_PAGE)) / ROW_PER_PAGE;
			if (totalRecords % ROW_PER_PAGE>0) pages +=1;
			if (pages==0) pages = new Long("1");
			writer.addElement("totalPages", pages.toString());
			writer.addElement("totalRecords", totalRecords.toString());
			writer.addElement("currentPage", pageNo.toString());
			writer.addElement("operationLimit", LIMIT_NUMBER_OPERATIONS.toString());
			
			
			qp.setRowsToFetch(pageNo * ROW_PER_PAGE, ROW_PER_PAGE);
			strSql = "SELECT w.wellUid, o.operationUid, w.wellName, wb.wellboreName, o.operationName FROM Well w, Wellbore wb, Operation o " +
				"WHERE (w.isDeleted=false or w.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (o.isDeleted=false or o.isDeleted is null) " +
				"AND w.wellUid=wb.wellUid " +
				"AND wb.wellboreUid=o.wellboreUid " +
				"AND (w.wellName like :keyword or wb.wellboreName like :keyword or o.operationName like :keyword) " + 
				extraSeachCondition +
				" ORDER BY w.wellName, wb.wellboreName, o.operationName ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"keyword", "userUid"}, new Object[]{wellNameLike, this.getCurrentUserSession().getUserUid()}, qp);
			List<String> wellUidList = new ArrayList<String>();
			List<String> operationUidList = new ArrayList<String>();
			for (Object[] rec:list) {
				String wellUid = rec[0].toString();
				String operationUid = rec[1].toString();
				if (!wellUidList.contains(wellUid)) wellUidList.add(wellUid);
				operationUidList.add(operationUid);
			}
			String strOperationUids = StringUtils.join(operationUidList, "','");
			strOperationUids = "'"+strOperationUids+"'";
			String strWellUids = StringUtils.join(wellUidList, "','");
			strWellUids = "'"+strWellUids+"'";
			
			strSql = "from Well where (isDeleted is null or isDeleted = false) and wellUid in ("+strWellUids+") order by wellName";
			List<Well> wellRs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {}, new Object[]{});
			for (Well well:wellRs) {
				strSql = "from Operation o, Wellbore w " +
						"where (o.isDeleted is null or o.isDeleted = false) " +
						"and (w.isDeleted is null or w.isDeleted = false) " +
						"and w.wellUid=:wellUid " +
						"and w.wellboreUid=o.wellboreUid " +
						"and o.operationUid in ("+strOperationUids+") " +
						"order by o.operationName";
				List<Object[]> operationRs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,new String[]{"wellUid"}, new Object[] {well.getWellUid()});
				if (operationRs.size()>0) {
					Map<String,Object> wellMap = new HashMap<String,Object>();
					wellMap.put("wellUid", well.getWellUid());
					wellMap.put("wellName", well.getWellName());
					if ("1".equals(GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "showMapInDat"))) {
						wellMap.put("latNs", well.getLatNs());
						wellMap.put("latDeg", well.getLatDeg());
						wellMap.put("latMinute", well.getLatMinute());
						wellMap.put("latSecond", well.getLatSecond());
						wellMap.put("longEw", well.getLongEw());
						wellMap.put("longDeg", well.getLongDeg());
						wellMap.put("longMinute", well.getLongMinute());
						wellMap.put("longSecond", well.getLongSecond());
					}
					writer.startElement("well",wellMap);
					for (Object[] obj:operationRs) {
						Operation operation = (Operation) obj[0];
						String operationUid = operation.getOperationUid();
						Map<String,Object> operationMap = new HashMap<String,Object>();
						operationMap.put("operationUid", operationUid);
						String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
						if ("".equals(operationName)) operationName = operation.getOperationName();
						operationMap.put("operationName", operationName);
						operationMap.put("sortName", WellNameUtil.getPaddedStr(operationName));
						writer.addElement("Operation", null, operationMap);
					}
					writer.endElement();
				}
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	public String costTracker(String operationUid) {
		String xml = "";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid order by dayDate";
			List<Daily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (Daily daily:rs) {
				SimpleAttributes attributes = new SimpleAttributes();
				attributes.addAttribute("dailyUid", daily.getDailyUid());
				attributes.addAttribute("reportDatetime", df.format(daily.getDayDate()));
				
				String reportNumber = "-";
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), this.getCurrentUserSession().getCachedAllAccessibleOperations().get(daily.getOperationUid()), daily.getDailyUid(), false);
				if (reportDaily!=null) reportNumber = reportDaily.getReportNumber();
				attributes.addAttribute("reportNumber", reportNumber);
				writer.addElement("ReportDaily", null, attributes);
			}
			
			//List<String> costSupplementaryAfeUid = this.getCostAfeMasterUid(operationUid, CostNetConstants.SUPPLEMENTARY_AFE); //TODO: remark this line
			strSql = "select a.costAfeMasterUid, a.afeType, b.afeNumber FROM OperationAfe a, CostAfeMaster b WHERE (a.isDeleted=false or a.isDeleted is null) and (b.isDeleted=false or b.isDeleted is null) AND " +
					"a.operationUid=:operationUid and a.costAfeMasterUid = b.costAfeMasterUid order by a.afeType";
			List<String[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (Object[] afe : afeList) {
				SimpleAttributes attributes = new SimpleAttributes();
				attributes.addAttribute("afeMasterUid", afe[0].toString());
				attributes.addAttribute("afeType", afe[1].toString());
				attributes.addAttribute("afeNumber", nullToEmptyString(afe[2]));
				
				writer.addElement("CostAfe", null, attributes);
			}
			
			Map<String, String> categoryMap = new HashMap<String, String>();
			Map<String, String> vendorMap = new HashMap<String, String>();
			Map<String, Map> map = new HashMap<String, Map>();
			Map<String, CostCurrencyLookup> currencies = new HashMap<String, CostCurrencyLookup>();
			CustomFieldUom currencyConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost");
			String primaryCurrency = currencyConverter.getUomSymbol();
			
			strSql = "FROM LookupCompany WHERE (isDeleted=false or isDeleted is null)";
			List<LookupCompany> companyLookup = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (LookupCompany vendor : companyLookup) {
				//if (!StringUtils.isNotBlank(vendor.getCompanyName())) continue;					
				vendorMap.put(vendor.getLookupCompanyUid(), vendor.getCompanyName());
			}
			
			Map<String , String> itemMap;
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
			if (operation!=null) {
				if (operation.getAmountSpentPriorToSpud()!=null) {
					itemMap = new HashMap<String, String>();
					itemMap.put("category", "Amount Spent Prior To Spud");
					itemMap.put("accountCode", "Amount Spent Prior To Spud");
					itemMap.put("shortDescription", "Amount Spent Prior To Spud");
					itemMap.put("vendor", "Amount Spent Prior To Spud");
					itemMap.put("costToDate", operation.getAmountSpentPriorToSpud().toString());
					itemMap.put("AfeTotal", "0.0");
					map.put("amountSpentPriorToSpud", itemMap);
				}
			}			
			
			strSql = "select a.costAfeMasterUid FROM OperationAfe a, CostAfeMaster b WHERE (a.isDeleted=false or a.isDeleted is null) and (b.isDeleted=false or b.isDeleted is null) AND " +
					"a.operationUid=:operationUid and a.costAfeMasterUid = b.costAfeMasterUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (String costAfeMasterUid:list) {
				strSql = "FROM CostCurrencyLookup WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid";
				List<CostCurrencyLookup> currencyLookup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
				for (CostCurrencyLookup currency : currencyLookup) {
					if (!StringUtils.isNotBlank(currency.getCurrencySymbol())) continue;
					if (currency.getIsPrimaryCurrency() != null) {
						if (currency.getIsPrimaryCurrency()) {
							primaryCurrency = currency.getCurrencySymbol();
						}
					}
					currencies.put(currency.getCurrencySymbol(), currency);
				}				
				
				strSql = "FROM CostAfeDetail WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid";
				List<CostAfeDetail> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
				for (CostAfeDetail item : details) {					
					String accountCode = nullToEmptyString(item.getAccountCode());
					accountCode = replaceHtmlChar(accountCode);
					String shortDescription = nullToEmptyString(item.getShortDescription());
					shortDescription = replaceHtmlChar(shortDescription);
					String itemDescription = nullToEmptyString(item.getItemDescription());
					itemDescription = replaceHtmlChar(itemDescription);
					String category = nullToEmptyString(item.getCategory());
					String vendor = nullToEmptyString(item.getLookupCompanyUid());
					String vendorName = null; 
					if (vendorMap.containsKey(vendor)) {
						vendorName = vendorMap.get(vendor);
					}					
					String key =  accountCode+"::"+shortDescription+"::"+itemDescription;
					String afeMasterUid = nullToEmptyString(item.getCostAfeMasterUid());
					
					if (map.containsKey(key)) {
						itemMap = (Map<String, String>) map.get(key);
						if (StringUtils.isNotBlank(category)) itemMap.put("category", category);
						if (StringUtils.isNotBlank(vendor)) itemMap.put("vendor", vendor);
					} else {
						itemMap = new HashMap<String, String>();
						itemMap.put("category", nullToEmptyString(item.getCategory()));
						itemMap.put("vendor", nullToEmptyString(vendorName));
						itemMap.put("accountCode", accountCode + " " + shortDescription);
						itemMap.put("shortDescription", StringUtils.isNotBlank(itemDescription)?itemDescription:"-");
						itemMap.put("costToDate", "0.0");
						itemMap.put("AfeTotal", "0.0");
						map.put(key, itemMap);
					}
					
					if (!categoryMap.containsKey(("".equals(item.getCategory())?"-":item.getCategory()))) {
						categoryMap.put(("".equals(item.getCategory())?"-":item.getCategory()), item.getCategory());
					} 				
					
					Double itemTotal = null; 
					if (item.getItemCost()!=null && item.getEstimatedDays()!=null && item.getQuantity()!=null) {
						itemTotal = item.getItemCost() * item.getEstimatedDays() * item.getQuantity();
						if (currencies.containsKey(item.getCurrency())) {
							Double conversionRate = currencies.get(item.getCurrency()).getConversionRate();
							if (conversionRate!=null && conversionRate > 0.0) {
								itemTotal = itemTotal / conversionRate;
							}
						}
						
						//if (itemMap.containsKey(afeMasterUid)) {
						//	itemTotal += Double.parseDouble(itemMap.get(afeMasterUid));
						//	itemMap.put(afeMasterUid, itemTotal.toString());
						//} else {
						//	itemMap.put(afeMasterUid, itemTotal.toString());
						//}
						Double afe = 0.0;
						if (itemMap.containsKey(afeMasterUid)) {
							afe = Double.parseDouble(itemMap.get(afeMasterUid)) + itemTotal;
						} else {
							afe = itemTotal;
						}
						itemMap.put(afeMasterUid, afe.toString());
						
						//Double previousTotal = null;
						//if (itemMap.containsKey("AfeTotal")) {
						//	previousTotal = Double.parseDouble(itemMap.get("AfeTotal"));
						//}
						//if (previousTotal!=null) {
						//	itemTotal += previousTotal;
						//}
						//itemMap.put("AfeTotal", itemTotal.toString());
						Double afeTotal = 0.0;
						if (itemMap.containsKey("AfeTotal")) {
							afeTotal = Double.parseDouble(itemMap.get("AfeTotal")) + itemTotal;
						}
						itemMap.put("AfeTotal", afeTotal.toString());
					}
					map.put(key, itemMap);
				}
			}
			
			strSql = "SELECT c.category, c.accountCode, c.afeShortDescription, c.afeItemDescription, c.dailyUid, sum(coalesce(c.quantity,0.0) * coalesce(c.itemCost,0.0)) as itemTotal, c.currency, c.lookupCompanyUid " +
					"FROM CostDailysheet c, Daily d " +
					"WHERE (c.isDeleted=false or c.isDeleted is null) " +
					"and (d.isDeleted=false or d.isDeleted is null) " +
					"and d.dailyUid=c.dailyUid and c.operationUid=:operationUid " +
					"group by c.category, c.accountCode, c.afeShortDescription, c.afeItemDescription, c.dailyUid, c.currency,c.lookupCompanyUid";
			List<Object[]> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (Object[] item:rs2) {
				String category = nullToEmptyString(item[0]);
				String accountCode = nullToEmptyString(item[1]);
				accountCode = replaceHtmlChar(accountCode);
				String afeShortDescription = nullToEmptyString(item[2]);
				afeShortDescription = replaceHtmlChar(afeShortDescription);
				String afeItemDescription = nullToEmptyString(item[3]);
				afeItemDescription = replaceHtmlChar(afeItemDescription);
				String dailyUid = nullToEmptyString(item[4]);
				Double itemTotal = Double.parseDouble(item[5].toString());
				String currency = nullToEmptyString(item[6]);
				String vendor = nullToEmptyString(item[7]);
				String vendorName = null; 
				if (vendorMap.containsKey(vendor)) {
					vendorName = vendorMap.get(vendor);
				}					
				
				String key =  accountCode+"::"+afeShortDescription+"::"+afeItemDescription;
				if (map.containsKey(key)) {
					itemMap = (Map<String , String>) map.get(key);
					if (StringUtils.isNotBlank(category)) itemMap.put("category", category);
				} else {
					itemMap = new HashMap<String, String>();
					itemMap.put("category", category);
					itemMap.put("accountCode", accountCode + " " + afeShortDescription);
					itemMap.put("shortDescription", (StringUtils.isBlank(afeItemDescription)? "-" : afeItemDescription));
					itemMap.put("vendor", (StringUtils.isBlank(vendorName)? "-" : vendorName));
					itemMap.put("costToDate", "0.0");
					itemMap.put("AfeTotal", "0.0");
					map.put(key, itemMap);
				}

				
				if (currencies.containsKey(currency)) {
					Double conversionRate = currencies.get(currency).getConversionRate();
					if (conversionRate!=null && conversionRate > 0.0) {
						itemTotal = itemTotal / conversionRate;
					}
				}
				Double costToDate = Double.parseDouble(itemMap.get("costToDate"));
				costToDate += itemTotal;
				itemMap.put("costToDate", costToDate.toString());
				if (itemMap.containsKey(dailyUid)) {
					itemTotal += Double.parseDouble(itemMap.get(dailyUid));
					itemMap.put(dailyUid, itemTotal.toString());
				} else {
					itemMap.put(dailyUid, itemTotal.toString());
				}
				
				if (!categoryMap.containsKey(("".equals(category)?"-":category))) {
					categoryMap.put(("".equals(category)?"-":category), category);
				} 
			}
			Integer zeroCost = 0;
			for (Map.Entry<String, Map> item:map.entrySet() ) {
				itemMap = item.getValue();
				double afeTotal = Double.parseDouble(itemMap.get("AfeTotal"));
				double costToDate = Double.parseDouble(itemMap.get("costToDate"));
				if (afeTotal==0.0 && costToDate==0.0) {
					zeroCost++;
					continue;
				}
				writer.addElement("CostDailysheet", null, item.getValue());
			}
			Integer categoryMapSize = categoryMap.size();
			Integer accountCodeMapSize = map.size() - zeroCost;
			writer.addElement("CurrencySymbol", primaryCurrency);
			writer.addElement("CategoryCounter", categoryMapSize.toString());
			writer.addElement("AccountCodeCounter", accountCodeMapSize.toString());
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	public String loadFilters() {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			//add default filter;
			SimpleAttributes attributes = new SimpleAttributes();
			attributes.addAttribute("datFilterUid", "");
			attributes.addAttribute("queryName", "Default Filter");
			attributes.addAttribute("queryFilters", "<filters/>");
			writer.addElement("filter", "", attributes);
			
			String strSql = "FROM DatFilter where (isDeleted=false or isDeleted is null) order by queryName";
			List<DatFilter> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (DatFilter filter : rs) {
				attributes = new SimpleAttributes();
				attributes.addAttribute("datFilterUid", filter.getDatFilterUid());
				attributes.addAttribute("queryName", filter.getQueryName());
				attributes.addAttribute("queryFilters", filter.getQueryFilters());
				writer.addElement("filter", "", attributes);
			}
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	public Boolean deleteFilter(String datFilterUid) {
		try {
			DatFilter filter = (DatFilter) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(DatFilter.class, datFilterUid);
			if (filter!=null) {
				filter.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filter);
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Boolean saveFilter(String queryName, String xmlFilters) {
		try {
			String strSql = "FROM DatFilter where (isDeleted=false or isDeleted is null) and queryName=:queryName";
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "queryName", queryName);
			if (rs.size()>0) return false;
			
			DatFilter filter = new DatFilter();
			filter.setQueryName(queryName);
			filter.setQueryFilters(xmlFilters);
			filter.setUserUid(this.getCurrentUserSession().getCurrentUser().getUserUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(filter);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public String queryBenchmark(String[] operationUids, String[] classCodes, String[] phaseCodes, String[] taskCodes, String[] rootCauseCodes, String[] userCodes, String[] jobTypeCodes) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			
			Map<String, String> classCodeMap = new HashMap<String, String>();
			Map<String, Double> maxDepthProgress = new HashMap<String, Double>();
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration");
			writer.addElement("UOMDuration", durationConverter.getUomLabel());
			writer.addElement("duration_uomSymbol", durationConverter.getUomSymbol());
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			List listOperationUids = new ArrayList();
			for (String operationUid : operationUids) {
				listOperationUids.add(operationUid);
			}
			List listClassCodes = new ArrayList();
			for (String classCode : classCodes) {
				listClassCodes.add(classCode);
			}
			List listPhaseCodes = new ArrayList();
			for (String phaseCode : phaseCodes) {
				listPhaseCodes.add(phaseCode);
			}
			List listTaskCodes = new ArrayList();
			for (String taskCode : taskCodes) {
				listTaskCodes.add(taskCode);
			}
			List listRootCauseCodes = new ArrayList();
			for (String rootCauseCode : rootCauseCodes) {
				listRootCauseCodes.add(rootCauseCode);
			}
			List listUserCodes = new ArrayList();
			for (String userCode : userCodes) {
				listUserCodes.add(userCode);
			}
			List listJobTypeCodes = new ArrayList();
			for (String jobTypeCode : jobTypeCodes) {
				listUserCodes.add(jobTypeCode);
			}
			
			List<String> listParamNames = new ArrayList();
			List listParamValues = new ArrayList();
			
			listParamNames.add("operationUid");
			listParamValues.add(listOperationUids);
			
			if (listClassCodes.size()>0) {
				listParamNames.add("classCode");
				listParamValues.add(listClassCodes);
			}
			
			if (listPhaseCodes.size()>0) {
				listParamNames.add("phaseCode");
				listParamValues.add(listPhaseCodes);
			}
			if (listTaskCodes.size()>0) {
				listParamNames.add("taskCode");
				listParamValues.add(listTaskCodes);
			}
			if (listRootCauseCodes.size()>0) {
				listParamNames.add("rootCauseCode");
				listParamValues.add(listRootCauseCodes);
			}
			if (listUserCodes.size()>0) {
				listParamNames.add("userCode");
				listParamValues.add(listUserCodes);
			}
			if (listJobTypeCodes.size()>0) {
				listParamNames.add("jobTypeCode");
				listParamValues.add(listJobTypeCodes);
			}
			String[] paramNames = new String[listParamNames.size()];
			String[] paramNames2 = new String[listParamNames.size() + 1];
			Integer i  = 0;
			for (String param : listParamNames) {
				paramNames[i] = param;
				paramNames2[i] = param;
				i++;
			}
			Object[] paramValues = new Object[listParamValues.size()];
			Object[] paramValues2 = new Object[listParamValues.size() + 1];
			i = 0;
			for (Object param : listParamValues) {
				paramValues[i] = param;
				paramValues2[i] = param;
				i++;
			}
			paramNames2[paramNames2.length-1] = "thisPhaseCode";
			
			SimpleAttributes attributes = new SimpleAttributes();
			String queryString = "SELECT a.operationUid, SUM(a.activityDuration) FROM Activity a, Daily d " +
				"where (a.isDeleted=false or a.isDeleted is null) " +
				"and (d.isDeleted=false or d.isDeleted is null) " +
				"and a.dailyUid=d.dailyUid " +
				"and a.operationUid in (:operationUid) " +
				"and (a.isSimop=false or a.isSimop is null) " +
				"and (a.isOffline=false or a.isOffline is null) " +
				"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
				(listClassCodes.size()>0?" and a.classCode in (:classCode) ":"")+
				(listPhaseCodes.size()>0?" and a.phaseCode in (:phaseCode) ":"")+
				(listTaskCodes.size()>0?" and a.taskCode in (:taskCode) ":"")+
				(listRootCauseCodes.size()>0?" and a.rootCauseCode in (:rootCauseCode) ":"")+
				(listUserCodes.size()>0?" and a.userCode in (:userCode) ":"")+
				(listJobTypeCodes.size()>0?" and a.jobTypeCode in (:jobTypeCode) ":"")+
				"group by a.operationUid";
			List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);
			for (Object[] rec : rs) {
				if (rec[0]==null || rec[1]==null) continue;
				String operationUid = rec[0].toString();
				String operationCode = null;
				Double duration = Double.parseDouble(rec[1].toString());
				durationConverter.setBaseValue(duration);
				_Operation operation = this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid);
				if (operation!=null) {
					operationCode = operation.getOperationCode();
				}

				attributes = new SimpleAttributes();
				attributes.addAttribute("operationUid", operationUid);
				attributes.addAttribute("duration", durationConverter.getConvertedValue()+"");
				attributes.addAttribute("durationLabel", durationConverter.formatOutputPrecision());
				String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true);
				attributes.addAttribute("operationName", operationName);
				attributes.addAttribute("operationName_sort", WellNameUtil.getPaddedStr(operationName));
				
				paramValues[0] = operationUid;
				paramValues2[0] = operationUid;
				queryString = "SELECT a.operationUid, a.classCode, SUM(a.activityDuration) FROM Activity a,Daily d " +
					"where (a.isDeleted=false or a.isDeleted is null) " +
					"and (d.isDeleted=false or d.isDeleted is null) " +
					"and a.dailyUid=d.dailyUid " +
					"and a.operationUid=:operationUid " +
					"and (a.isSimop=false or a.isSimop is null) " +
					"and (a.isOffline=false or a.isOffline is null) " +
					"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					(listClassCodes.size()>0?" and a.classCode in (:classCode) ":"")+
					(listPhaseCodes.size()>0?" and a.phaseCode in (:phaseCode) ":"")+
					(listTaskCodes.size()>0?" and a.taskCode in (:taskCode) ":"")+
					(listRootCauseCodes.size()>0?" and a.rootCauseCode in (:rootCauseCode) ":"")+
					(listUserCodes.size()>0?" and a.userCode in (:userCode) ":"")+
					(listJobTypeCodes.size()>0?" and a.jobTypeCode in (:jobTypeCode) ":"")+
					"group by a.operationUid, a.classCode " +
					"order by a.classCode";
				List<Object[]>  rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);
				for (Object[] rec2 : rs2) {
					if (rec2[1]==null) continue;
					String classCode = rec2[1].toString();
					if ("".equals(classCode)) continue;
					duration = Double.parseDouble(rec2[2].toString());
					durationConverter.setBaseValue(duration);
					attributes.addAttribute(replaceAttrChar(classCode), durationConverter.getConvertedValue()+"");
					
					if (!classCodeMap.containsKey(classCode)) {
						classCodeMap.put(classCode, classCode);
					}
				}
				writer.startElement("Operation", attributes);
				
				queryString = "SELECT a.operationUid, coalesce(a.phaseCode,''), sum(coalesce(a.activityDuration,0.0)), min(coalesce(a.depthMdMsl,0.0)), max(coalesce(a.depthMdMsl,0.0)), min(d.dayDate) FROM Activity a, Daily d " +
						"where (a.isDeleted=false or a.isDeleted is null) " +
						"and (d.isDeleted=false or d.isDeleted is null) " +
						"and a.dailyUid=d.dailyUid " +
						"and a.operationUid=:operationUid " +
						"and (a.isSimop=false or a.isSimop is null) " +
						"and (a.isOffline=false or a.isOffline is null) " +
						"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						(listClassCodes.size()>0?" and a.classCode in (:classCode) ":"")+
						(listPhaseCodes.size()>0?" and a.phaseCode in (:phaseCode) ":"")+
						(listTaskCodes.size()>0?" and a.taskCode in (:taskCode) ":"")+
						(listRootCauseCodes.size()>0?" and a.rootCauseCode in (:rootCauseCode) ":"")+
						(listUserCodes.size()>0?" and a.userCode in (:userCode) ":"")+
						(listJobTypeCodes.size()>0?" and a.jobTypeCode in (:jobTypeCode) ":"")+
						"group by a.operationUid, coalesce(a.phaseCode,'') ";

				rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);

				List<Object[]> sortedList = rs2;
				//sort the list by d.daydate
				Collections.sort(sortedList, new Comparator() {
						public int compare(Object o1, Object o2) {
							Date object1 = (Date) Array.get(o1, 5);
							Date object2 = (Date) Array.get(o2, 5);
							if (object1==null || object2==null) return 0;
							return (object1).compareTo(object2);
						}
				});
				
				Double previousPhaseEndDepth = 0.0;
				int counter = 0;
				for (Object[] rec2 : sortedList) {
					if (rec2[2]==null) continue;
					counter++;
					String phaseCode = StringUtils.isBlank((String)rec2[1])?"UNKNOWN":(String)rec2[1];
					duration = Double.parseDouble(rec2[2].toString());
					Double startDepth = Double.parseDouble(rec2[3].toString());
					Double endDepth = Double.parseDouble(rec2[4].toString());
					if (duration.equals(0.0)) continue;
					durationConverter.setBaseValue(duration);
					
					operationUomContext.setOperationUid(operationUid);
					CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "depthMdMsl", operationUomContext);
					int depthUomPrecision = 0;
					if (depthConverter.getUOMMapping()!=null) depthUomPrecision = depthConverter.getUOMMapping().getUnitOutputPrecision();
					String datumLabel = "";
					//Datum datum = DatumManager.getDatum(operationUomContext.getDatumUid(), this.getCurrentUserSession().getUserLocale(), this.getCurrentUserSession().getCurrentGroupUid());
					//if (datum!=null) datumLabel = " " + datum.getSimplifiedDatumCode();
					
					depthConverter.setBaseValue(startDepth);
					startDepth = depthConverter.getConvertedValue();
					String startDepthLabel = depthConverter.getFormattedValue();
					depthConverter.setBaseValue(endDepth);
					endDepth = depthConverter.getConvertedValue();
					String endDepthLabel = depthConverter.getFormattedValue();
					
					if (depthUomPrecision>0) {
						startDepth = Math.round(startDepth * Math.pow(10, depthUomPrecision)) / Math.pow(10, depthUomPrecision);
						endDepth = Math.round(endDepth * Math.pow(10, depthUomPrecision)) / Math.pow(10, depthUomPrecision);
					} else {
						startDepth = Math.round(startDepth) * 1.0;
						endDepth = Math.round(endDepth) * 1.0;
					}
					
					if(counter==1){				
						 //get start depth of first phase 
						Double firstPhaseStartDepth = 0.0;
						String sql = "SELECT op.operationUid, wb.kickoffMdMsl  FROM Wellbore wb, Operation op WHERE wb.wellboreUid = op.wellboreUid AND op.operationUid=:operationUid";
						List<Object[]> sqlResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", operationUid, queryProperties);
						if(sqlResult!=null && sqlResult.size()>0){
							String kickOffDepth = nullToEmptyString(sqlResult.get(0)[1]);
				 
							if(StringUtils.isNotEmpty(kickOffDepth)){
								firstPhaseStartDepth = Double.parseDouble(kickOffDepth);
							}else{
								List<Object[]> sqlResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT operationUid, spudMdMsl FROM Operation WHERE operationUid=:operationUid", "operationUid", operationUid);
								if(sqlResult2!=null && sqlResult2.size()>0){
									String spudMd = nullToEmptyString(sqlResult2.get(0)[1]);
									if(StringUtils.isNotBlank(spudMd)){
										firstPhaseStartDepth = Double.parseDouble(spudMd);				
									}
								}
							}
						}
						
						depthConverter.setBaseValue(firstPhaseStartDepth);
						firstPhaseStartDepth  = depthConverter.getConvertedValue();
						startDepthLabel = depthConverter.getFormattedValue();
						
					}else{
						//assign current start to next phase's prev end depth
						if (!startDepth.equals(endDepth)) {
							depthConverter.setBaseValue(previousPhaseEndDepth);
							previousPhaseEndDepth = depthConverter.getConvertedValue();
							startDepthLabel = depthConverter.getFormattedValue();
							startDepth = depthConverter.getConvertedValue();
							if (depthUomPrecision>0) {
								startDepth = Math.round(startDepth * Math.pow(10, depthUomPrecision)) / Math.pow(10, depthUomPrecision);
							} else {
								startDepth = Math.round(startDepth) * 1.0;
							}
						}
					}
					
					attributes = new SimpleAttributes();
					attributes.addAttribute("phaseCode", phaseCode);
					attributes.addAttribute("duration", durationConverter.getConvertedValue()+"");
					attributes.addAttribute("durationLabel", durationConverter.formatOutputPrecision());
					if (!startDepth.equals(endDepth)) {
						attributes.addAttribute("startDepthLabel", startDepthLabel + datumLabel);
						attributes.addAttribute("endDepthLabel", endDepthLabel + datumLabel);
						attributes.addAttribute("depthDifferent", (endDepth - startDepth) + "");
						
						if (maxDepthProgress.containsKey(phaseCode)) {
							if (maxDepthProgress.get(phaseCode) < endDepth - startDepth) {
								maxDepthProgress.put(phaseCode, endDepth - startDepth);
							}
						} else {
							maxDepthProgress.put(phaseCode, endDepth - startDepth);
						}
						previousPhaseEndDepth = Double.parseDouble(rec2[4].toString()); //assign endDepth to previousPhaseEndDepth
					}
					
					
					
					paramValues2[paramValues2.length - 1] = phaseCode;
					queryString = "SELECT a.operationUid, coalesce(a.classCode,''), sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d " +
						"where (a.isDeleted=false or a.isDeleted is null) " +
						"and (d.isDeleted=false or d.isDeleted is null) " +
						"and d.dailyUid=a.dailyUid " +
						"and a.operationUid=:operationUid " +
						"and a.phaseCode=:thisPhaseCode " +
						"and (a.isSimop=false or a.isSimop is null) " +
						"and (a.isOffline=false or a.isOffline is null) " +
						"and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						(listClassCodes.size()>0?" and a.classCode in (:classCode) ":"")+
						(listPhaseCodes.size()>0?" and a.phaseCode in (:phaseCode) ":"")+
						(listTaskCodes.size()>0?" and a.taskCode in (:taskCode) ":"")+
						(listRootCauseCodes.size()>0?" and a.rootCauseCode in (:rootCauseCode) ":"")+
						(listUserCodes.size()>0?" and a.userCode in (:userCode) ":"")+
						(listJobTypeCodes.size()>0?" and a.jobTypeCode in (:jobTypeCode) ":"")+
						"group by a.operationUid, coalesce(a.classCode,'') ";
					List<Object[]> rs3  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames2, paramValues2, queryProperties);
					for (Object[] rec3 : rs3) {
						if (rec3[1]==null) rec3[1] = "";
						String classCode = rec3[1].toString();
						if ("".equals(classCode)) continue;
						duration = Double.parseDouble(rec3[2].toString());
						durationConverter.setBaseValue(duration);
						attributes.addAttribute(replaceAttrChar(classCode), durationConverter.getConvertedValue() + "");
						
					}
					
					queryString = "FROM LookupPhaseCode where (isDeleted=false or isDeleted is null) " +
							"AND (operationCode=:operationCode or operationCode='' or operationCode is null) " +
							"AND shortCode=:shortCode";
					List<LookupPhaseCode> phaseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationCode", "shortCode"}, new Object[] {operationCode, phaseCode});
					if (phaseList.size()>0) {
						attributes.addAttribute("phaseName", phaseList.get(0).getName());
					}
					
					writer.addElement("phase_" + replaceAttrChar(phaseCode), "", attributes);
				}
				
				writer.endElement();
			}
			for (Map.Entry<String, Double> entry: maxDepthProgress.entrySet()) {
				attributes = new SimpleAttributes();
				attributes.addAttribute("phaseCode", entry.getKey());
				attributes.addAttribute("depthDifferent", entry.getValue() + "");
				writer.addElement("MaxDepth", "", attributes);
			}
			
			for (Map.Entry<String, String> entry: classCodeMap.entrySet()) {
				writer.addElement("ClassCode", entry.getKey());
			}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public String queryAFEByCurrency(String[] operations) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;
			writer.startElement("root");

			for (String operationUid : operations) {
				Map<String, Object> costAfeDetails = new HashMap<String, Object>();
				Map<String, CostCurrencyLookup> currencies = new HashMap<String, CostCurrencyLookup>();
				CustomFieldUom currencyConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost");
				String primaryCurrency = currencyConverter.getUomSymbol();
				
				attributes = new SimpleAttributes();
				attributes.addAttribute("operationUid", operationUid);
				attributes.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
				writer.startElement("datarow", null, attributes);
				
				String strSql = "SELECT costAfeMasterUid FROM OperationAfe WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid";
				List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
				for (String costAfeMasterUid:list) {
					strSql = "FROM CostCurrencyLookup WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid";
					List<CostCurrencyLookup> currencyLookup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
					for (CostCurrencyLookup currency : currencyLookup) {
						if (!StringUtils.isNotBlank(currency.getCurrencySymbol())) continue;
						if (currency.getIsPrimaryCurrency() != null) {
							if (currency.getIsPrimaryCurrency()) {
								primaryCurrency = currency.getCurrencySymbol();
							}
						}
						currencies.put(currency.getCurrencySymbol(), currency);
					}
					
					
					strSql = "FROM CostAfeDetail WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid";
					List<CostAfeDetail> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
					for (CostAfeDetail item : details) {
						String key = item.getAccountCode() + "::" + item.getShortDescription();
						Map<String, Object> costAfeDetail = new HashMap<String, Object>();
						if (costAfeDetails.containsKey(key)) {
							costAfeDetail = (Map<String, Object>) costAfeDetails.get(key);
						} else {
							costAfeDetail.put("accountCode", item.getAccountCode());
							costAfeDetail.put("shortDescription", item.getShortDescription());
						}
						
						Double itemTotal = null; 
						Double itemTotalInPrimary = null;
						if (item.getItemCost()!=null && item.getEstimatedDays()!=null && item.getQuantity()!=null) {
							itemTotal = item.getItemCost() * item.getEstimatedDays() * item.getQuantity();
							itemTotalInPrimary = itemTotal;
						}
						if (itemTotal!=null) {
							String currentCurrency = primaryCurrency;
							if (currencies.containsKey(item.getCurrency())) {
								currentCurrency = item.getCurrency();
								
								Double conversionRate = currencies.get(item.getCurrency()).getConversionRate();
								if (conversionRate!=null && conversionRate > 0.0) {
									itemTotalInPrimary = itemTotal / conversionRate;
								}
							}
							Double previousTotal = null;
							Double previousTotalInPrimary = null;
							if (costAfeDetail.containsKey(currentCurrency)) {
								previousTotal = (Double) costAfeDetail.get(currentCurrency);
							}
							if (costAfeDetail.containsKey("total")) {
								previousTotalInPrimary = (Double) costAfeDetail.get("total");
							}
							if (previousTotal!=null) {
								itemTotal += previousTotal;
							}
							if (previousTotalInPrimary!=null) {
								itemTotalInPrimary += previousTotalInPrimary;
							}
							costAfeDetail.put(currentCurrency, itemTotal);
							costAfeDetail.put("total", itemTotalInPrimary);
						}
						
						costAfeDetails.put(key, costAfeDetail);
					}
				}
				
				//Sorting by account code
				List<Object[]> mapList = new ArrayList();
				for (Map.Entry<String, Object>item : costAfeDetails.entrySet()) {
					mapList.add(new Object[] {item.getKey(), item.getValue()});
				}
				Collections.sort(mapList, new AccountCodeComparator());
				for(Iterator i = mapList.iterator(); i.hasNext(); ){
					Object[] obj_array = (Object[]) i.next();
					Map<String, Object> costAfeDetail = (Map<String, Object>)obj_array[1];
					attributes = new SimpleAttributes();
					for (Map.Entry<String, Object> attr : costAfeDetail.entrySet()) {
						if (attr.getValue()!=null) attributes.addAttribute(replaceAttrChar(attr.getKey()), attr.getValue().toString());
					}
					writer.addElement("item", "", attributes);
				}
				
				for (Map.Entry<String, CostCurrencyLookup> currency : currencies.entrySet()) {
					attributes = new SimpleAttributes();
					attributes.addAttribute("symbol", currency.getValue().getCurrencySymbol());
					writer.addElement("Currency", "", attributes);
				}
				
				attributes = new SimpleAttributes();
				attributes.addAttribute("symbol", primaryCurrency);
				writer.addElement("PrimaryCurrency", "", attributes);
				writer.endElement();
			}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public String queryNptDowntimeSummary(String groupBy, String operations, int year) {
		String result = "<root/>";
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration");
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			Double nptDuration = 0.0;
			Double totalDuration = 0.0;
			Double nptPercent = 0.0;
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;
			writer.startElement("root");
			
			Double q1_npt = 0.0;
			Double q1_time = 0.0;
			Double q2_npt = 0.0;
			Double q2_time = 0.0;
			Double q3_npt = 0.0;
			Double q3_time = 0.0;
			Double q4_npt = 0.0;
			Double q4_time = 0.0;
			Double t_npt = 0.0;
			Double t_time = 0.0;
			
			Double totalNptHours = 0.0;
			String strSql = "select sum(a.activityDuration) FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND d.operationUid in (" + operations + ")" +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " +
					"AND a.internalClassCode in ('TP','TU') ";
			List<Double> listTotalDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql, qp);
			if (listTotalDuration.size()>0) {
				if (listTotalDuration.get(0)!=null) totalNptHours = listTotalDuration.get(0);
			}
			durationConverter.setBaseValue(totalNptHours);
			writer.addElement("TotalNptHours", durationConverter.formatOutputPrecision());
			
			if ("rig".equals(groupBy)) {
				strSql = "SELECT o.rigInformationUid, sum(coalesce(a.activityDuration,0.0)) FROM Daily d, Operation o, Activity a " +
					"WHERE (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND o.operationUid=d.operationUid " +
					"AND d.operationUid in (" + operations + ")" +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " +
					"GROUP BY o.rigInformationUid " +
					"ORDER BY o.rigInformationUid ";
			} else if ("general".equals(groupBy)) {
				strSql = "SELECT d.isDeleted, sum(coalesce(a.activityDuration,0.0)) FROM Daily d, Operation o, Activity a " +
					"WHERE (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND o.operationUid=d.operationUid " +
					"AND d.operationUid in (" + operations + ")" +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " +
					"GROUP BY d.isDeleted " +
					"ORDER BY d.isDeleted ";
			} else if ("country".equals(groupBy)) {
				strSql = "SELECT w.country, sum(coalesce(a.activityDuration,0.0)) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
					"WHERE (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND o.operationUid in (" + operations + ") " +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " +
					"GROUP BY w.country " +
					"ORDER BY w.country ";
			} else if ("rootCause".equals(groupBy)) {
				strSql = "SELECT a.rootCauseCode, sum(coalesce(a.activityDuration,0.0)) FROM Daily d, Activity a " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND d.operationUid in (" + operations + ")" +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " +
					"AND a.internalClassCode in ('TP','TU') " +
					"GROUP BY a.rootCauseCode " +
					"ORDER BY a.rootCauseCode ";
			} else {
				return result;
			}
			
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			
			// sort by sum duration DESC
			Collections.sort(list, new Comparator() {
				public int compare(Object o1, Object o2) {
					Object[] object1 = (Object[]) o1;
					Object[] object2 = (Object[]) o2;
					if (object1==null || object1[1]==null || object2==null || object2[1]==null) return 0;
					Double d1 = (Double) object1[1];
					Double d2 = (Double) object2[1];
					return d2.compareTo(d1);
				}
			});
			
			for (Object[] a : list) {
				List listA = null;
				attributes = new SimpleAttributes();
				String value = nullToEmptyString(a[0]);
				if ("rootCause".equals(groupBy)) {
					String name = value;
					strSql = "FROM LookupRootCauseCode WHERE (isDeleted=false or isDeleted is null) and (shortCode=:shortCode " + (StringUtils.isBlank(value)?" or shortCode='' or shortCode is null":"") + ")";
					List<LookupRootCauseCode> rootCauseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "shortCode", value);
					if (rootCauseList.size()>0) name = rootCauseList.get(0).getName();
					attributes.addAttribute("title", year + " Monthly NPT Summary (" + name + ")");
					attributes.addAttribute("rootCause", name);
					attributes.addAttribute("rootCauseCode", value);
				} else if ("rig".equals(groupBy)) {
					String rigName = value;
					RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, value);
					if(rig!=null) rigName = rig.getRigName();
					attributes.addAttribute("title", year + " Monthly NPT Summary (" + rigName + ")");
					attributes.addAttribute("rigName", rigName);
					attributes.addAttribute("year",year+"");
					attributes.addAttribute("rigInformationUid", value);
				} else if ("general".equals(groupBy)) {
					attributes.addAttribute("title", year + " Quarterly NPT Summary");
					attributes.addAttribute("year", year+"");
				} else if ("country".equals(groupBy)){
					attributes.addAttribute("country",value);
					attributes.addAttribute("title", year + " Monthly NPT Summary (" + value + ")");
					attributes.addAttribute("isCountry","true");
				} else {
					attributes.addAttribute("title", year + " Monthly NPT Summary (" + value + ")");
				}
				writer.startElement("datarow", attributes);
				
				int quarter = 0;
				Double TotalNPT = 0.0;
				Double TotalTime = 0.0;
				Double TotalNPTPercent = 0.0;
				for (int i=0; i<12;i++) {
					if (i%3==0) {
						//insert quarterly records
						nptDuration = 0.0;
						totalDuration = 0.0;
						nptPercent = 0.0;
						attributes = new SimpleAttributes();
						attributes.addAttribute("isQuarter", "true");
						Calendar cal = new GregorianCalendar(year, i+2, 1);
						int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
						
						listA = null;
						if ("country".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
								"WHERE (w.isDeleted=false or w.isDeleted is null) " +
								"AND (wb.isDeleted=false or wb.isDeleted is null) " +
								"AND (o.isDeleted=false or o.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isDeleted=false or a.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND w.wellUid=wb.wellUid " +
								"AND wb.wellboreUid=o.wellboreUid " +
								"AND o.operationUid=d.operationUid " +
								"AND d.dailyUid=a.dailyUid " +
								"AND w.country=:country " +
								"AND o.operationUid in (" + operations + ") " +
								"AND a.internalClassCode in ('TP','TU') " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-01' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "country", value, qp);
						} else if ("general".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								"AND a.operationUid in (" + operations + ") " +
								"AND a.internalClassCode in ('TP','TU') " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-01' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
						} else if ("rootCause".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								"AND (a.rootCauseCode=:rootCauseCode " + (StringUtils.isBlank(value)?" or a.rootCauseCode='' or a.rootCauseCode is null":"") + ") " +
								"AND a.operationUid in (" + operations + ") " +
								"AND a.internalClassCode in ('TP','TU') " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-01' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rootCauseCode", value, qp);
						} else if ("rig".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a, Operation o " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (o.isDeleted=false or o.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								"AND a.operationUid=o.operationUid " +
								"AND o.rigInformationUid=:rigInformationUid " +
								"AND a.operationUid in (" + operations + ") " +
								"AND a.internalClassCode in ('TP','TU') " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-01' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", value, qp);
						}
						if (listA!=null) { 
							if (listA.size()>0) {
								Object b = listA.get(0);
								if (b!=null) nptDuration = Double.parseDouble(b.toString());
							}
						}
						
						listA = null;
						if ("country".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
								"WHERE (w.isDeleted=false or w.isDeleted is null) " +
								"AND (wb.isDeleted=false or wb.isDeleted is null) " +
								"AND (o.isDeleted=false or o.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isDeleted=false or a.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND w.wellUid=wb.wellUid " +
								"AND wb.wellboreUid=o.wellboreUid " +
								"AND o.operationUid=d.operationUid " +
								"AND d.dailyUid=a.dailyUid " +
								"AND w.country=:country " +
								"AND o.operationUid in (" + operations + ") " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "country",value, qp);
						} else if ("general".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								"AND a.operationUid in (" + operations + ") " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
						} else if ("rootCause".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								//"AND a.rootCauseCode=:rootCauseCode " +
								//"AND a.internalClassCode in ('TP','TU') " +
								"AND a.operationUid in (" + operations + ") " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql, qp);
						} else if ("rig".equals(groupBy)) {
							strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a, Operation o " +
								"WHERE (a.isDeleted=false or a.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (o.isDeleted=false or o.isDeleted is null) " +
								"AND (a.isSimop=false or a.isSimop is null) " +
								"AND (a.isOffline=false or a.isOffline is null) " +
								"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
								"AND a.dailyUid=d.dailyUid " +
								"AND a.operationUid=o.operationUid " +
								"AND o.rigInformationUid=:rigInformationUid " +
								"AND a.operationUid in (" + operations + ") " +
								"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+3) +"-"+days+"' ";
							listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", value, qp);
						}
						if (listA!=null) {
							if (listA.size()>0) {
								Object b = listA.get(0);
								if (b!=null) totalDuration = Double.parseDouble(b.toString());
							}
						}
						
						durationConverter.setBaseValue(nptDuration);
						attributes.addAttribute("nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
						durationConverter.setBaseValue(totalDuration);
						attributes.addAttribute("totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
						if (totalDuration > 0.0) nptPercent = nptDuration * 100 / totalDuration;
						attributes.addAttribute("nptPercent", formatter.format(nptPercent) + "");
						
						quarter++;
						attributes.addAttribute("name", "Q" + quarter);
						writer.addElement("record", "", attributes);

						
						TotalNPT = TotalNPT + nptDuration;
						TotalTime = TotalTime + totalDuration;
						//TotalNPTPercent = TotalNPTPercent + nptPercent;
						
						if (quarter == 1){
							q1_npt = q1_npt + nptDuration;
							q1_time = q1_time + totalDuration;
						}
						else if (quarter == 2) {
							q2_npt = q2_npt + nptDuration;
							q2_time = q2_time + totalDuration;
						}
						else if (quarter == 3) {
							q3_npt = q3_npt + nptDuration;
							q3_time = q3_time + totalDuration;			
						}
						else if (quarter == 4) {
							q4_npt = q4_npt + nptDuration;
							q4_time = q4_time + totalDuration;
						}		
					}
					
					nptDuration = 0.0;
					totalDuration = 0.0;
					nptPercent = 0.0;
					attributes = new SimpleAttributes();
					Calendar cal = new GregorianCalendar(year, i, 1);
					int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					
					listA = null;
					if ("country".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
							"WHERE (w.isDeleted=false or w.isDeleted is null) " +
							"AND (wb.isDeleted=false or wb.isDeleted is null) " +
							"AND (o.isDeleted=false or o.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isDeleted=false or a.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND w.wellUid=wb.wellUid " +
							"AND wb.wellboreUid=o.wellboreUid " +
							"AND o.operationUid=d.operationUid " +
							"AND d.dailyUid=a.dailyUid " +
							"AND w.country=:country " +
							"AND o.operationUid in (" + operations + ") " +
							"AND a.internalClassCode in ('TP','TU') " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "country", value, qp);
					} else if ("rig".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a, Operation o " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (o.isDeleted=false or o.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND a.operationUid=o.operationUid " +
							"AND o.rigInformationUid=:rigInformationUid " +
							"AND o.operationUid in (" + operations + ") " +
							"AND a.internalClassCode in ('TP','TU') " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", value, qp);
					} else if ("general".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND a.operationUid in (" + operations + ") " +
							"AND a.internalClassCode in ('TP','TU') " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
					} else if ("rootCause".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND a.rootCauseCode=:rootCauseCode " +
							"AND a.operationUid in (" + operations + ") " +
							"AND a.internalClassCode in ('TP','TU') " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rootCauseCode", value, qp);
					}
					if (listA!=null) {
						if (listA.size()>0) {
							Object b = listA.get(0);
							if (b!=null) nptDuration = Double.parseDouble(b.toString());
						}
					}
					
					listA = null;
					if ("country".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
							"WHERE (w.isDeleted=false or w.isDeleted is null) " +
							"AND (wb.isDeleted=false or wb.isDeleted is null) " +
							"AND (o.isDeleted=false or o.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isDeleted=false or a.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND w.wellUid=wb.wellUid " +
							"AND wb.wellboreUid=o.wellboreUid " +
							"AND o.operationUid=d.operationUid " +
							"AND d.dailyUid=a.dailyUid " +
							"AND w.country=:country " +
							"AND o.operationUid in (" + operations + ") " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "country", value, qp);
					} else if ("rig".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a, Operation o " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (o.isDeleted=false or o.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND a.operationUid=o.operationUid " +
							"AND o.rigInformationUid=:rigInformationUid " +
							"AND o.operationUid in (" + operations + ") " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", value, qp);
					} else if ("rootCause".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							//"AND a.rootCauseCode=:rootCauseCode " +
							"AND a.operationUid in (" + operations + ") " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql, qp);
					} else if ("general".equals(groupBy)) {
						strSql = "SELECT sum(a.activityDuration) FROM Daily d, Activity a " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND a.operationUid in (" + operations + ") " +
							"AND d.dayDate between '" + year + "-"+ (i+1) +"-1' and '" + year + "-"+ (i+1) +"-"+days+"' ";
						listA = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{}, new Object[]{}, qp);
					}
					if (listA!=null) {
						if (listA.size()>0) {
							Object b = listA.get(0);
							if (b!=null) totalDuration = Double.parseDouble(b.toString());
						}
					}
					durationConverter.setBaseValue(nptDuration);
					attributes.addAttribute("nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
					durationConverter.setBaseValue(totalDuration);
					attributes.addAttribute("totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
					if (totalDuration > 0.0) nptPercent = nptDuration * 100 / totalDuration;
					attributes.addAttribute("nptPercent", formatter.format(nptPercent) + "");

					attributes.addAttribute("name", monthName[i]);
					writer.addElement("record", "", attributes);
					
				}
				
				t_npt = t_npt + TotalNPT;
				t_time = t_time + TotalTime;
				
				attributes = new SimpleAttributes();
				durationConverter.setBaseValue(TotalNPT);
				//TotalNPT = durationConverter.getConvertedValue();
				attributes.addAttribute("TotalNPT", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
				durationConverter.setBaseValue(TotalTime);
				//TotalTime = durationConverter.getConvertedValue();
				attributes.addAttribute("TotalTime", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
				TotalNPTPercent = (TotalNPT/TotalTime)*100;
				attributes.addAttribute("TotalNPTPercent", formatter.format(TotalNPTPercent));
				
				if (totalNptHours>0.0) {
					Double downtimePercent = TotalNPT / totalNptHours * 100.0;
					attributes.addAttribute("percentOfDowntime", formatter.format(downtimePercent));
				}
				
				writer.addElement("total", "", attributes);
				
				writer.endElement();
			}
			
			attributes = new SimpleAttributes();
			durationConverter.setBaseValue(q1_npt);
			attributes.addAttribute("q1_nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			durationConverter.setBaseValue(q1_time);
			attributes.addAttribute("q1_totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			durationConverter.setBaseValue(q2_npt);
			attributes.addAttribute("q2_nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			durationConverter.setBaseValue(q2_time);
			attributes.addAttribute("q2_totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			durationConverter.setBaseValue(q3_npt);
			attributes.addAttribute("q3_nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			durationConverter.setBaseValue(q3_time);
			attributes.addAttribute("q3_totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			durationConverter.setBaseValue(q4_npt);
			attributes.addAttribute("q4_nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			durationConverter.setBaseValue(q4_time);
			attributes.addAttribute("q4_totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			durationConverter.setBaseValue(t_npt);
			attributes.addAttribute("t_nptDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			durationConverter.setBaseValue(t_time);
			attributes.addAttribute("t_totalDuration", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			writer.addElement("recordTotal", "", attributes);	
			
			
			//String currentList = nullToEmptyString(a);
			//String lastList = nullToEmptyString(list.get(list.size()-1));
			//if (operations!= null && currentList.equals(lastList)){
				
				String[] ops = operations.split(",");
				for (String operationUid : ops){
					Double nptDurationOp = 0.0;
					Double totalDurationOp = 0.0;

					if ("''".equals(operationUid)) continue;
					attributes = new SimpleAttributes();
					attributes.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid.replace("'", "")));
					
					strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
					"WHERE (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=a.dailyUid " +
					"AND o.operationUid in (" + operationUid + ") " +
					"AND a.internalClassCode in ('TP','TU') " +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " ;
					
					List listOps = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql, qp);
					if (listOps!=null) { 
						if (listOps.size()>0) {
							Object npt = listOps.get(0);
							if (npt!=null) nptDurationOp = Double.parseDouble(npt.toString());
						}
						durationConverter.setBaseValue(nptDurationOp);
						attributes.addAttribute("nptDurationOp", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
					}
					
					listOps = null;
					strSql = "SELECT sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
					"WHERE (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=a.dailyUid " +
					"AND o.operationUid in (" + operationUid + ") " +
					"AND d.dayDate between '" + year + "-01-01' and '" + year + "-12-31' " ;
					
					listOps = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql, qp);
					if (listOps!=null) { 
						if (listOps.size()>0) {
							Object total = listOps.get(0);
							if (total!=null) totalDurationOp = Double.parseDouble(total.toString());
						}
						durationConverter.setBaseValue(totalDurationOp);
						attributes.addAttribute("totalDurationOp", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
					}
					
					writer.addElement("opSummary", "", attributes);
				}
			//}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
		
	}
	
	
	public String queryGeneralLedger(String[] operations, String dayDate) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;
			writer.startElement("root");

			Map<String, String> modDemodCodes = new HashMap<String, String>();
			String modDemodAccountCode = GroupWidePreference.getValue(this.getCurrentUserSession().getCurrentGroupUid(), "costMobDemodAccountCode");
			if (modDemodAccountCode!=null && !"".equals(modDemodAccountCode)) {
				String[] splitModDemodCodes = modDemodAccountCode.split(",");
				for (String code : splitModDemodCodes) {
					modDemodCodes.put(code.trim(), code.trim());
				}
			}
			
			for (String operationUid : operations) {
				Map<String, Object> costItems = new HashMap<String, Object>();
				Map<String, CostCurrencyLookup> currencies = new HashMap<String, CostCurrencyLookup>();
				CustomFieldUom currencyConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost");
				String primaryCurrency = currencyConverter.getUomSymbol();
				
				attributes = new SimpleAttributes();
				attributes.addAttribute("operationUid", operationUid);
				attributes.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
				
				Double amountSpentPriorToSpud = 0.0;
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				if (operation!=null) {
					if (operation.getAmountSpentPriorToSpud()!=null) {
						//attributes.addAttribute("amountSpentPriorToSpud", operation.getAmountSpentPriorToSpud().toString());
						amountSpentPriorToSpud = operation.getAmountSpentPriorToSpud();
					}
				}
				
				writer.startElement("datarow", null, attributes);
				
				
				String strSql = "SELECT costAfeMasterUid FROM OperationAfe WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid";
				List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
				for (String costAfeMasterUid:list) {
					strSql = "FROM CostCurrencyLookup WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid order by isPrimaryCurrency desc";
					List<CostCurrencyLookup> currencyLookup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
					for (CostCurrencyLookup currency : currencyLookup) {
						if (!StringUtils.isNotBlank(currency.getCurrencySymbol())) continue;
						if (currency.getIsPrimaryCurrency() != null) {
							if (currency.getIsPrimaryCurrency()) {
								primaryCurrency = currency.getCurrencySymbol();
							}
						}
						currencies.put(currency.getCurrencySymbol(), currency);
						writer.addElement("CostCurrencyLookup", currency.getCurrencySymbol());
					}
					strSql = "FROM CostAfeDetail WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterUid=:costAfeMasterUid";
					List<CostAfeDetail> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "costAfeMasterUid", costAfeMasterUid);
					for (CostAfeDetail item : details) {
						String key = item.getAccountCode() + "::" + item.getShortDescription();
						Map<String, Object> costItem = new HashMap<String, Object>();
						if (costItems.containsKey(key)) {
							costItem = (Map<String, Object>) costItems.get(key);
						} else {
							costItem.put("accountCode", item.getAccountCode());
							costItem.put("shortDescription", item.getShortDescription());
							if (modDemodCodes.containsKey(key)) {
								if (amountSpentPriorToSpud>0.0) {
									costItem.put(primaryCurrency, amountSpentPriorToSpud);
									costItem.put("wellTotal", amountSpentPriorToSpud);
									amountSpentPriorToSpud = 0.0;
								}
							}
						}
						
						Double itemTotal = null; 
						if (item.getItemCost()!=null && item.getEstimatedDays()!=null && item.getQuantity()!=null) {
							itemTotal = item.getItemCost() * item.getEstimatedDays() * item.getQuantity();
							if (currencies.containsKey(item.getCurrency())) {
								Double conversionRate = currencies.get(item.getCurrency()).getConversionRate();
								if (conversionRate!=null && conversionRate > 0.0) {
									itemTotal = itemTotal / conversionRate;
								}
							}
							Double previousTotal = null;
							if (costItem.containsKey("afeTotal")) {
								previousTotal = (Double) costItem.get("afeTotal");
							}
							if (previousTotal!=null) {
								itemTotal += previousTotal;
							}
							costItem.put("afeTotal", itemTotal);
						}
						costItems.put(key, costItem);
					}
				}
				
				String dailyUid = null;
				strSql = "SELECT dailyUid FROM Daily WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid AND dayDate='" + dayDate + "'";
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
				if (list.size()>0) dailyUid = list.get(0);
				
				if (dailyUid!=null) {
					strSql = "SELECT c.accountCode, c.afeShortDescription, c.currency, SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM CostDailysheet c, Daily d " +
							"WHERE (d.isDeleted=false or d.isDeleted is null) " +
							"AND (c.isDeleted=false or c.isDeleted is null) " +
							"AND d.dailyUid=c.dailyUid " +
							"AND c.operationUid=:operationUid " +
							"AND d.dayDate<='" + dayDate + "' " +
							"GROUP BY c.accountCode, c.afeShortDescription, c.currency ";
					List<Object[]> costToDateList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
					for (Object[] o : costToDateList) {
						String accountCode = (String) o[0];
						String shortDescription = (String) o[1];
						String currency = (String) o[2];
						Double itemTotal = 0.0;
						Double thisItemTotal = 0.0;
						if (o[3]!=null) {
							itemTotal = Double.parseDouble(o[3].toString());
							thisItemTotal = Double.parseDouble(o[3].toString());
						}
						 
						String key = accountCode + "::" + shortDescription;
						Map<String, Object> costItem = new HashMap<String, Object>();
						if (costItems.containsKey(key)) {
							costItem = (Map<String, Object>) costItems.get(key);
						} else {
							costItem.put("accountCode", accountCode);
							costItem.put("shortDescription", shortDescription);
							if (modDemodCodes.containsKey(key)) {
								if (amountSpentPriorToSpud>0.0) {
									costItem.put("wellTotal", amountSpentPriorToSpud);
									costItem.put(primaryCurrency, amountSpentPriorToSpud);
									amountSpentPriorToSpud = 0.0;
								}
							}
						}
						
						
						Double previousTotal = null;
						if (StringUtils.isNotBlank(currency)) {
							if (costItem.containsKey(currency))  previousTotal = (Double) costItem.get(currency);
							if (previousTotal!=null) itemTotal += previousTotal;
							costItem.put(currency, itemTotal);
						} else if (StringUtils.isNotBlank(primaryCurrency)) {
							if (costItem.containsKey(primaryCurrency))  previousTotal = (Double) costItem.get(primaryCurrency);
							if (previousTotal!=null) itemTotal += previousTotal;
							costItem.put(primaryCurrency, itemTotal);
						}
						
						if (currencies.containsKey(currency)) {
							Double conversionRate = currencies.get(currency).getConversionRate();
							if (conversionRate!=null && conversionRate > 0.0) {
								//itemTotal = itemTotal / conversionRate;
								thisItemTotal = thisItemTotal / conversionRate; 
							}
						}
						previousTotal = null;
						if (costItem.containsKey("wellTotal")) {
							previousTotal = (Double) costItem.get("wellTotal");
						}
						if (previousTotal!=null) {
							thisItemTotal += previousTotal;
						}
						costItem.put("wellTotal", thisItemTotal);
						costItems.put(key, costItem);
					}
					
					strSql = "SELECT c.accountCode, c.afeShortDescription, c.currency, SUM(coalesce(c.itemCost,0.0) * coalesce(c.quantity,0.0)) FROM CostDailysheet c " +
							"WHERE (c.isDeleted=false or c.isDeleted is null) " +
							"AND c.dailyUid=:dailyUid " +
							"GROUP BY c.accountCode, c.afeShortDescription, c.currency ";
					List<Object[]> dailyCostList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid);
					for (Object[] o : dailyCostList) {
						String accountCode = (String) o[0];
						String shortDescription = (String) o[1];
						String currency = (String) o[2];
						Double itemTotal = 0.0;
						if (o[3]!=null) itemTotal = Double.parseDouble(o[3].toString());
						String key = accountCode + "::" + shortDescription;
						Map<String, Object> costItem = new HashMap<String, Object>();
						if (costItems.containsKey(key)) {
							costItem = (Map<String, Object>) costItems.get(key);
						} else {
							costItem.put("accountCode", accountCode);
							costItem.put("shortDescription", shortDescription);
						}
						
						if (currencies.containsKey(currency)) {
							Double conversionRate = currencies.get(currency).getConversionRate();
							if (conversionRate!=null && conversionRate > 0.0) {
								itemTotal = itemTotal / conversionRate;
							}
						}
						Double previousTotal = null;
						if (costItem.containsKey("dailyTotal")) {
							previousTotal = (Double) costItem.get("dailyTotal");
						}
						if (previousTotal!=null) {
							itemTotal += previousTotal;
						}
						costItem.put("dailyTotal", itemTotal);
						costItems.put(key, costItem);
					}
				}
				
				//Sorting by account code
				List<Object[]> mapList = new ArrayList();
				for (Map.Entry<String, Object>item : costItems.entrySet()) {
					mapList.add(new Object[] {item.getKey(), item.getValue()});
				}
				Collections.sort(mapList, new AccountCodeComparator());
				for(Iterator i = mapList.iterator(); i.hasNext(); ){
					Object[] obj_array = (Object[]) i.next();
					Map<String, Object> costAfeDetail = (Map<String, Object>)obj_array[1];
					attributes = new SimpleAttributes();
					for (Map.Entry<String, Object> attr : costAfeDetail.entrySet()) {
						if (attr.getValue()!=null) attributes.addAttribute(replaceAttrChar(attr.getKey()), attr.getValue().toString());
					}
					writer.addElement("item", "", attributes);
				}
				
				if (amountSpentPriorToSpud>0.0) {
					writer.addElement("amountSpentPriorToSpud", amountSpentPriorToSpud.toString());
				}
				
				attributes = new SimpleAttributes();
				attributes.addAttribute("symbol", primaryCurrency);
				writer.addElement("PrimaryCurrency", "", attributes);
				writer.endElement();
				
			}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	private String replaceHtmlChar(String value) {
		value = replaceString(value, "&amp;", "&");
		value = replaceString(value, "&nbsp;", " ");
		value = replaceString(value, "\\\";", "\"");
		value = replaceString(value, "\\", "");
		return value;
	}
	
	private String replaceString(String value, String oldString, String newString) {
		while (value.indexOf(oldString)>=0) value = value.replace(oldString, newString);
		return value;
	}
	
	private String replaceAttrChar(String value) {
		String[] replaceChar = {" ", "\\","(",")[","]","\"","'","?","/","<",">",":",";","{","}","+","=","-","*","&","^","%","$","#","@","!","~","`"};
		for (String old : replaceChar) {
			if(value.indexOf(old)>0) value = replaceString(value,old,"");
		}
		return value;
	}

	private class AccountCodeComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Map<String, Object> in1 = (Map<String, Object>) o1[1];
				Map<String, Object> in2 = (Map<String, Object>) o2[1];
				
				String f1 = in1.get("accountCode").toString() + "::" + in1.get("shortDescription").toString();
				String f2 = in2.get("accountCode").toString() + "::" + in2.get("shortDescription").toString();
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	
	public String queryActivityCodesBreakdownByRig(String[] operations) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;
			writer.startElement("root");
			SimpleAttributes items;
			String operationsuidlist = "";	
			CustomFieldUom thisformat = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
			
			for (String operationUid : operations) {
		
				if(operationsuidlist=="")
					operationsuidlist= "'" + operationUid + "'";
				else
					operationsuidlist+=",'" + operationUid + "'";
			}
			
				attributes = new SimpleAttributes();				
				List <RigInformation> rignamelist;
				String getrignamesql = "SELECT distinct a.rigInformationUid,a.rigName FROM RigInformation a, Operation o WHERE (a.isDeleted=false or a.isDeleted is null) AND o.rigInformationUid=a.rigInformationUid AND o.operationUid IN("+operationsuidlist+")";
				
				rignamelist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(getrignamesql);					
				for(Object objResultRigs : rignamelist){
					double TotalP = 0.0;
					double TotalTP = 0.0;
					double TotalTU = 0.0;
					double TotalU = 0.0;
					Object[] objRstRig = (Object[]) objResultRigs;
					if (objRstRig[0] != null) attributes.addAttribute("rigInformationUid", objRstRig[0].toString());
					if (objRstRig[1] != null) attributes.addAttribute("rigName", objRstRig[1].toString());
					writer.startElement("datarow", null, attributes);
					
					for (String operationUid : operations){
						List<Activity> activityLists;
						String operationUidformat="'"+operationUid+"'";
						String activityQuery = "SELECT a.operationUid,a.internalClassCode,sum(a.activityDuration/3600.00) AS actDuration, o.operationName FROM Activity a, Operation o WHERE (a.isDeleted = false or a.isDeleted is null) and a.operationUid IN("+operationUidformat+") AND o.rigInformationUid IN('"+objRstRig[0]+"') AND a.operationUid=o.operationUid AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) AND (a.isOffline=0 or a.isOffline is null) AND (a.isSimop=0 or a.isSimop is null) GROUP BY a.internalClassCode,a.operationUid";
						activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(activityQuery);				
						int counter = activityLists.size();
						
						if(activityLists != null && !activityLists.isEmpty()){
							
							items = new SimpleAttributes();
							for(Object objResultActivity: activityLists){							
								Object[] objRstactivity = (Object[]) objResultActivity;
								
								if ("P".equals(objRstactivity[1])){
									if (objRstactivity[0] != null) {
										items.addAttribute("operationUid", objRstactivity[0].toString());
										items.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), objRstactivity[0].toString()));
									}
									if (objRstactivity[1] != null) items.addAttribute("classCodeP", objRstactivity[1].toString());
									if (objRstactivity[2] != null){
										thisformat.setReferenceMappingField(Activity.class, "activityDuration");
										items.addAttribute("actDurationP", thisformat.formatOutputPrecision((Double) objRstactivity[2]));
										TotalP = TotalP + (Double) objRstactivity[2];
									}
								}
								if ("TP".equals(objRstactivity[1])){
									if (objRstactivity[0] != null) {
										items.addAttribute("operationUid", objRstactivity[0].toString());
										items.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), objRstactivity[0].toString()));
									}
									if (objRstactivity[1] != null) items.addAttribute("classCodeTP", objRstactivity[1].toString());
									if (objRstactivity[2] != null){ 
										thisformat.setReferenceMappingField(Activity.class, "activityDuration");
										items.addAttribute("actDurationTP", thisformat.formatOutputPrecision((Double) objRstactivity[2]));
										TotalTP = TotalTP + (Double) objRstactivity[2];
									}
								}
								if ("TU".equals(objRstactivity[1])){
									if (objRstactivity[0] != null) {
										items.addAttribute("operationUid", objRstactivity[0].toString());
										items.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), objRstactivity[0].toString()));
									}
									if (objRstactivity[1] != null) items.addAttribute("classCodeTU", objRstactivity[1].toString());
									if (objRstactivity[2] != null){
										thisformat.setReferenceMappingField(Activity.class, "activityDuration");
										items.addAttribute("actDurationTU", thisformat.formatOutputPrecision((Double) objRstactivity[2]));
										TotalTU = TotalTU + (Double) objRstactivity[2];
									}
								}
								if ("U".equals(objRstactivity[1])){
									if (objRstactivity[0] != null) {
										items.addAttribute("operationUid", objRstactivity[0].toString());
										items.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), objRstactivity[0].toString()));
									}
									if (objRstactivity[1] != null) items.addAttribute("classCodeU", objRstactivity[1].toString());
									if (objRstactivity[2] != null){
										thisformat.setReferenceMappingField(Activity.class, "activityDuration");
										items.addAttribute("actDurationU", thisformat.formatOutputPrecision((Double) objRstactivity[2]));
										TotalU = TotalU + (Double) objRstactivity[2];
									}
								}
								counter=counter-1;
								if (counter==0){
									writer.addElement("item", "", items);
								}
								
							}
							
						}
												
					}
					items = new SimpleAttributes();
					thisformat.setReferenceMappingField(Activity.class, "activityDuration");
					items.addAttribute("TotalP", thisformat.formatOutputPrecision(TotalP));
					items.addAttribute("TotalTP", thisformat.formatOutputPrecision(TotalTP));
					items.addAttribute("TotalTU", thisformat.formatOutputPrecision(TotalTU));
					items.addAttribute("TotalU", thisformat.formatOutputPrecision(TotalU));
					writer.addElement("total", "", items);	
					writer.endElement();
				}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;		
					
	}
				
	
	//************************ Actual DVD *************************
	public String queryActualDVD(String operationUid) {
		String xml = "";
		String prevPhaseCode = null;
		Double durationByPhase = 0.0;
		
		Double phaseCost = 0.0;
		Double maxDayDepth = 0.0;
		Double prevDayDepth = 0.0;
		String operationCode = null;
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes detailsAttributes = new SimpleAttributes();
			writer.startElement("root");
			
			_Operation operation = this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid);
			if (operation!=null) operationCode = operation.getOperationCode();
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			CustomFieldUom dayCostConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			CustomFieldUom durationConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration", operationUomContext);
			CustomFieldUom depthConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "depthMdMsl", operationUomContext);
			int costUOMPrecision= 0;
			if (dayCostConverter.getUOMMapping()!=null) costUOMPrecision = dayCostConverter.getUOMMapping().getUnitOutputPrecision();			
			
			List<Daily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily " +
					"where (isDeleted=false or isDeleted is null) " +
					"AND operationUid=:operationUid " +
					"order by dayDate", "operationUid", operationUid, qp);
			
			for(Daily daily : rs) {
				Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(daily.getDailyUid());
				if (dayCost==null) {
					ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), daily.getDailyUid());
					if (reportDaily!=null && reportDaily.getDaycost() != null) dayCost = reportDaily.getDaycost();
				}
				if (dayCost==null) dayCost=0.0;
				
				Double totalHours = 0.0;
				String strSql = "SELECT SUM(activityDuration) FROM Activity " +
						"where (isDeleted=false or isDeleted is null) " +						
						"AND dailyUid=:dailyUid " +
						"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
						"AND (isOffline=0 or isOffline is null) " +
						"AND (isSimop=0 or isSimop is null)";
				List<Double> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", daily.getDailyUid(), qp);
				if (lstResult.size()>0) {
					Double a = lstResult.get(0);
					if (a != null) {
						durationConverter.setBaseValue(Double.parseDouble(a.toString()));
						totalHours = durationConverter.getConvertedValue();
					}
				}
				
				String activityQuery = "SELECT a.operationUid, a from Activity a " +
						"where (a.isDeleted = false or a.isDeleted is null) " +
						"and a.dailyUid=:dailyUid " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND (a.isOffline=0 or a.isOffline is null) " +
						"AND (a.isSimop=0 or a.isSimop is null) order by a.endDatetime";
				List<Object[]> activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, "dailyUid", daily.getDailyUid(), qp);				
				
				for(Object[] rec : activityLists) {
					Activity activity = (Activity) rec[1];
					String currentPhaseCode = null;
					if (activity.getPhaseCode() != null) {
						if (prevPhaseCode != null) {
							currentPhaseCode = activity.getPhaseCode();
							if (currentPhaseCode.equalsIgnoreCase(prevPhaseCode)) {
								Double duration = activity.getActivityDuration();
								if(duration != null && totalHours > 0) {
									durationByPhase += (duration);
									durationConverter.setBaseValue(duration);
									phaseCost += (dayCost * (durationConverter.getConvertedValue() / totalHours));										
								}
								if (activity.getDepthMdMsl() != null) maxDayDepth = activity.getDepthMdMsl();
								if(maxDayDepth < prevDayDepth) maxDayDepth = prevDayDepth;
								
							} else {									
								durationConverter.setBaseValue(durationByPhase);
								depthConverter.setBaseValue(maxDayDepth);
								detailsAttributes.addAttribute("duration", durationConverter.getConvertedValue()+"");
								detailsAttributes.addAttribute("durationLabel", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
								detailsAttributes.addAttribute("maxDepth", depthConverter.formatOutputPrecision());
								
								if (costUOMPrecision>0) {
									phaseCost = Math.round(phaseCost * Math.pow(10, costUOMPrecision)) / Math.pow(10, costUOMPrecision);
								} else {
									phaseCost = Math.round(phaseCost) * 1.0;
								}
								detailsAttributes.addAttribute("phaseCost", phaseCost.toString());
								detailsAttributes.addAttribute("phaseCostLabel", dayCostConverter.formatOutputPrecision(phaseCost));
								
								writer.addElement("datarow", null, detailsAttributes);
								prevPhaseCode = null;
								prevDayDepth = 0.0;
								maxDayDepth = 0.0;
								durationByPhase = 0.0;
								phaseCost = 0.0;
								
								if (prevPhaseCode == null) {
									currentPhaseCode = activity.getPhaseCode();
									if (activity.getDepthMdMsl() != null) maxDayDepth = activity.getDepthMdMsl();
									String queryString = "FROM LookupPhaseCode where (isDeleted=false or isDeleted is null) " +
											"AND (operationCode=:operationCode or operationCode='' or operationCode is null) " +
											"AND shortCode=:shortCode";
									List<LookupPhaseCode> phaseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationCode", "shortCode"}, new Object[] {operationCode, currentPhaseCode});
									if (phaseList.size()>0) {
										detailsAttributes.addAttribute("phaseName", phaseList.get(0).getName() + " - " + currentPhaseCode);
									} else {
										detailsAttributes.addAttribute("phaseName", currentPhaseCode);
									}
									
									detailsAttributes.addAttribute("phaseCode", currentPhaseCode);
									
									Double duration = activity.getActivityDuration();								
									
									if(duration != null && totalHours > 0) {
										durationByPhase = (duration);
										durationConverter.setBaseValue(duration);
										phaseCost = (dayCost * (durationConverter.getConvertedValue() / totalHours));
									}
									prevPhaseCode = activity.getPhaseCode();
									if (activity.getDepthMdMsl() != null) prevDayDepth = activity.getDepthMdMsl();
								}
							}
							
						} else {
							//First record
							prevPhaseCode = activity.getPhaseCode();
							currentPhaseCode = activity.getPhaseCode();
							if (activity.getDepthMdMsl() != null) prevDayDepth = activity.getDepthMdMsl();
							if (activity.getDepthMdMsl() != null) maxDayDepth = activity.getDepthMdMsl();
							
							String queryString = "FROM LookupPhaseCode where (isDeleted=false or isDeleted is null) " +
									"AND (operationCode=:operationCode or operationCode='' or operationCode is null) " +
									"AND shortCode=:shortCode";
							List<LookupPhaseCode> phaseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationCode", "shortCode"}, new Object[] {operationCode, currentPhaseCode});
							if (phaseList.size()>0) {
								detailsAttributes.addAttribute("phaseName", phaseList.get(0).getName() + " - " + currentPhaseCode);
							} else {
								detailsAttributes.addAttribute("phaseName", currentPhaseCode);
							}
							
							detailsAttributes.addAttribute("phaseCode", currentPhaseCode);

							Double duration = activity.getActivityDuration();								
							
							if(duration != null && totalHours > 0) {
								durationByPhase = (duration);
								durationConverter.setBaseValue(duration);
								phaseCost = (dayCost * (durationConverter.getConvertedValue() / totalHours));
							}
						}
					}						
					
				}					
			}
			
			durationConverter.setBaseValue(durationByPhase);
			depthConverter.setBaseValue(maxDayDepth);
			detailsAttributes.addAttribute("duration", durationConverter.getConvertedValue()+"");
			detailsAttributes.addAttribute("durationLabel", durationConverter.formatOutputPrecision(durationConverter.getConvertedValue()));
			
			if (costUOMPrecision>0) {
				phaseCost = Math.round(phaseCost * Math.pow(10, costUOMPrecision)) / Math.pow(10, costUOMPrecision);
			} else {
				phaseCost = Math.round(phaseCost) * 1.0;
			}
			detailsAttributes.addAttribute("phaseCost", phaseCost.toString());
			detailsAttributes.addAttribute("phaseCostLabel", dayCostConverter.formatOutputPrecision(phaseCost));
			detailsAttributes.addAttribute("maxDepth", depthConverter.formatOutputPrecision());
			writer.addElement("datarow", "", detailsAttributes);
			prevPhaseCode = null;
			durationByPhase = 0.0;
			phaseCost = 0.0;
			
			SimpleAttributes attributes = new SimpleAttributes();
			attributes.addAttribute("symbol", durationConverter.getUomSymbol());
			writer.addElement("UOMDuration", "", attributes);
			
			attributes = new SimpleAttributes();
			attributes.addAttribute("symbol", dayCostConverter.getUomSymbol());
			writer.addElement("UOMCurrency", "", attributes);
			
			attributes = new SimpleAttributes();
			attributes.addAttribute("symbol", depthConverter.getUomSymbol());
			writer.addElement("UOMDepth", "", attributes);
			
			writer.endElement();
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		
		return xml;
	}
	
	public String queryPlannedVsActualDVD(String operationUid) {
		String result = "<root/>";
		try {
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			operationUomContext.setOperationUid(operationUid);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			_Operation operation = this.getCurrentUserSession().getCachedAllAccessibleOperations().get(operationUid);
			SimpleAttributes attributes;
			List plannedPhases = new ArrayList();
			List actualPhases = new ArrayList();
			//List dvdResult = new ArrayList();
			Map<String, Double> dailyCost = new HashMap<String, Double>();
			Map<String, Double> dailyDuration = new HashMap<String, Double>();
			Integer seq = 0;
			String queryString = "";

			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "daycost", operationUomContext);
			if (costConverter.isUOMMappingAvailable()) writer.addElement("currencySymbol", costConverter.getUomSymbol());
			
			String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			if (dvdPlanUid!=null) {
				queryString = "SELECT phaseCode, coalesce(p50Duration,0.0), coalesce(phaseBudget,0.0) FROM OperationPlanPhase WHERE (isDeleted=false or isDeleted is null) and operationPlanMasterUid=:dvdPlanUid ORDER BY sequence, depthMdMsl";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dvdPlanUid", dvdPlanUid, qp);
				for (Object[] phase : list) {
					Map<String, Object> phaseObj = new HashMap<String, Object>();
					String phaseCode = (String) phase[0]; 
					if (StringUtils.isBlank(phaseCode)) phaseCode = "-";
					seq += 100;
					phaseObj = new HashMap<String, Object>();
					phaseObj.put("phaseCode", phaseCode);
					phaseObj.put("duration", phase[1]);
					phaseObj.put("cost", phase[2]);
					phaseObj.put("sequence", seq);
					plannedPhases.add(phaseObj);
				}
			}
			
			String previousCode = "";
			Double duration = 0.0;
			Double cost = 0.0;
			Map<String, Object> phaseObj = null;
			queryString = "SELECT a.dailyUid, a.phaseCode, coalesce(a.activityDuration,0.0) FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					"AND d.operationUid=:operationUid " +
					"ORDER BY d.dayDate, a.startDatetime ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
			for (Object[] rec : list) {
				String dailyUid = (String) rec[0];
				String phaseCode = (String) rec[1];
				if (StringUtils.isBlank(phaseCode)) phaseCode = "-";
				
				if (!phaseCode.equals(previousCode)) {
					if (!"".equals(previousCode)) {
						phaseObj = new HashMap<String, Object>();
						phaseObj.put("phaseCode", previousCode);
						phaseObj.put("duration", duration);
						phaseObj.put("cost", cost);
						actualPhases.add(phaseObj);
					}
					previousCode = phaseCode;
					duration = 0.0;
					cost = 0.0;
				}
				
				
				Double activityDuration = Double.parseDouble(rec[2].toString());
				
				Double dayCost = 0.0;
				if (dailyCost.containsKey(dailyUid)) {
					dayCost = dailyCost.get(dailyUid); 
				} else {
					dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(dailyUid);
					if (dayCost==null) {
						ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), operation, dailyUid, false);
						if (rd!=null) {
							dayCost = rd.getDaycost();
						}
					}
					if (dayCost==null) dayCost = 0.0;
					dailyCost.put(dailyUid, dayCost);
				}
				
				Double dayDuration = 0.0;
				if (dailyDuration.containsKey(dailyUid)) {
					dayDuration = dailyDuration.get(dailyUid);
				} else {
					queryString = "select sum(coalesce(activityDuration,0.0)) FROM Activity " +
							"WHERE (isDeleted=false or isDeleted is null) " +
							"AND dailyUid=:dailyUid " +
							"AND (isSimop=false or isSimop is null) " +
							"AND (isOffline=false or isOffline is null) " +
							"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) ";
					List<Double> tmpList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", dailyUid, qp);
					if (tmpList.size()>0) {
						dayDuration = tmpList.get(0);
					}
					if (dayDuration==null) dayDuration = 0.0;
					dailyDuration.put(dailyUid, dayDuration);
				}
				
				Double thisCost = 0.0;
				if (dayDuration>0.0) thisCost = activityDuration * dayCost / dayDuration;
				
				duration = duration + activityDuration;
				cost = cost + thisCost;
			}
			if (!"".equals(previousCode)) {
				phaseObj = new HashMap<String, Object>();
				phaseObj.put("phaseCode", previousCode);
				phaseObj.put("duration", duration);
				phaseObj.put("cost", cost);
				actualPhases.add(phaseObj);
			}
			
			for (Object plannedPhase : plannedPhases) {
				Map<String, Object> phase = (Map<String, Object>)plannedPhase;
				seq = (Integer) phase.get("sequence");
				String phaseCode = (String) phase.get("phaseCode");
				
				for (Object actualPhase : actualPhases) {
					Map<String, Object> phase2 = (Map<String, Object>)actualPhase;
					String phaseCode2 = (String) phase2.get("phaseCode");
					if (phaseCode2.equals(phaseCode) && !phase2.containsKey("sequence")) {
						phase2.put("sequence", seq);
						break;
					}
				}
			}
			
			seq = 0;
			for (Object actualPhase : actualPhases) {
				Map<String, Object> phase2 = (Map<String, Object>)actualPhase;
				if (!phase2.containsKey("sequence")) {
					seq++;
					phase2.put("sequence", seq);
				} else{
					seq = (Integer) phase2.get("sequence");
				}
			}
			
			int plannedIndex = 0;
			int actualIndex = 0;
			while (plannedIndex < plannedPhases.size() || actualIndex < actualPhases.size()) {
				Map<String, Object> plannedPhase = null;
				Map<String, Object> actualPhase = null; 
				Integer plannedSeq = null;
				Integer actualSeq = null;
				if (plannedIndex<plannedPhases.size()) {
					plannedPhase = (Map<String, Object>)plannedPhases.get(plannedIndex);
					plannedSeq = (Integer) plannedPhase.get("sequence");
				}
				if (actualIndex<actualPhases.size()) {
					actualPhase = (Map<String, Object>)actualPhases.get(actualIndex);
					actualSeq = (Integer) actualPhase.get("sequence");
				}
				
				attributes = new SimpleAttributes();
				Double plannedCost = 0.0;
				Double actualCost = 0.0;
				if (actualSeq==null) {
					attributes.addAttribute("phaseCode", nullToEmptyString(plannedPhase.get("phaseCode")));
					attributes.addAttribute("phaseName", this.getPhaseName((String) plannedPhase.get("phaseCode")));
					if (plannedPhase.get("cost")!=null) plannedCost = (Double) plannedPhase.get("cost");
					attributes.addAttribute("plannedCost", plannedCost.toString());
					attributes.addAttribute("actualCost", "");
					plannedIndex++;
				} else if (plannedSeq==null) {
					attributes.addAttribute("phaseCode", nullToEmptyString(actualPhase.get("phaseCode")));
					attributes.addAttribute("phaseName", this.getPhaseName((String) actualPhase.get("phaseCode")));
					attributes.addAttribute("plannedCost", "");
					if (actualPhase.get("cost")!=null) actualCost = (Double)actualPhase.get("cost"); 
					attributes.addAttribute("actualCost", actualCost.toString());
					actualIndex++;
				} else {
					if (plannedSeq < actualSeq) {
						attributes.addAttribute("phaseCode", nullToEmptyString(plannedPhase.get("phaseCode")));
						attributes.addAttribute("phaseName", this.getPhaseName((String) plannedPhase.get("phaseCode")));
						if (plannedPhase.get("cost")!=null) plannedCost = (Double) plannedPhase.get("cost");
						attributes.addAttribute("plannedCost", plannedCost.toString());
						attributes.addAttribute("actualCost", "");
						plannedIndex++;
					} else if (plannedSeq==null || plannedSeq > actualSeq) {
						attributes.addAttribute("phaseCode", nullToEmptyString(actualPhase.get("phaseCode")));
						attributes.addAttribute("phaseName", this.getPhaseName((String) actualPhase.get("phaseCode")));
						attributes.addAttribute("plannedCost", "");
						if (actualPhase.get("cost")!=null) actualCost = (Double)actualPhase.get("cost"); 
						attributes.addAttribute("actualCost", actualCost.toString());
						actualIndex++;
					} else {
						attributes.addAttribute("phaseCode", nullToEmptyString(actualPhase.get("phaseCode")));
						attributes.addAttribute("phaseName", this.getPhaseName((String) actualPhase.get("phaseCode")));
						//String formattedValue = "";
						if (plannedPhase.get("cost")!=null) plannedCost = (Double) plannedPhase.get("cost"); 
						attributes.addAttribute("plannedCost", plannedCost.toString());
						if (actualPhase.get("cost")!=null) actualCost = (Double)actualPhase.get("cost");
						attributes.addAttribute("actualCost",actualCost.toString());
						plannedIndex++;
						actualIndex++;
					}
				}
				Double diffCost = plannedCost - actualCost;
				attributes.addAttribute("diffCost",diffCost.toString());
				writer.addElement("datarow", "", attributes);
			}

			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	private String getPhaseName(String phaseCode) {
		String phaseName = phaseCode;
		try {
			List<LookupPhaseCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode=:shortCode", "shortCode", phaseCode);
			if (list.size()>0) {
				LookupPhaseCode code = list.get(0);
				phaseName = code.getName() + " (" + phaseCode + ")";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return phaseName;
		
	}
	public String queryDatNoticeOfCompletionOfWells(String[] strOperationUid){
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
				
			QueryProperties qp = new QueryProperties();
		//	qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
			String strOperationUids = StringUtils.join(strOperationUid, "','"); 
			String queryString = "select o.operationUid,w,wb,o FROM Well w, Wellbore wb, Operation o " +
					"WHERE (o.isDeleted=false or o.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"and o.operationUid IN ('" + strOperationUids + "')";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);						
			for (Object[] rec : list) {
				Well well = (Well) rec[1];				
				Operation operation = (Operation) rec[3];
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("wellUid", well.getWellUid());
				attr.addAttribute("wellName", well.getWellName());
				attr.addAttribute("latNs", nullToEmptyString(well.getLatNs()));
				attr.addAttribute("latDeg", nullToEmptyString(well.getLatDeg()));
				attr.addAttribute("latMinute", nullToEmptyString(well.getLatMinute()));
				attr.addAttribute("latSecond", nullToEmptyString(well.getLatSecond()));
				attr.addAttribute("longEw", nullToEmptyString(well.getLongEw()));
				attr.addAttribute("longDeg", nullToEmptyString(well.getLongDeg()));
				attr.addAttribute("longMinute", nullToEmptyString(well.getLongMinute()));
				attr.addAttribute("longSecond", nullToEmptyString(well.getLongSecond()));
				if (operation.getSpudDate()!=null) {
					attr.addAttribute("spudDatetime", df.format(operation.getSpudDate()));
				}
				if (operation.getzAirgap() != null){
					thisConverter.setReferenceMappingField(Operation.class, "zAirgap");	
					attr.addAttribute("zAirgap",thisConverter.getFormattedValue(operation.getzAirgap()));
				}
				if (well.getGlHeightMsl()!=null) { //Evaluation : Ground
					thisConverter.setReferenceMappingField(Well.class, "glHeightMsl");
					attr.addAttribute("glHeightMsl",thisConverter.getFormattedValue(well.getGlHeightMsl()));
				}
				/*if (operation.getzKbelevation() != null){
					thisConverter.setReferenceMappingField(Operation.class, "zKbelevation");
					attr.addAttribute("zKbelevation",thisConverter.getFormattedValue(operation.getzKbelevation()));
				}*/
				if (operation.getTdDateTdTime()!=null) attr.addAttribute("tdDateTdTime",df.format(operation.getTdDateTdTime()));
				if (operation.getRigreleaseDateRigreleaseTime()!=null) attr.addAttribute("rigReleaseDate", df.format(operation.getRigreleaseDateRigreleaseTime()));
				if (operation.getzPbMdMsl() != null){
					thisConverter.setReferenceMappingField(Operation.class, "zPbMdMsl");
					attr.addAttribute("plugbackdepthMsl", thisConverter.getFormattedValue(operation.getzPbMdMsl()));
				}
				//attr.addAttribute("contractor", nullToEmptyString(operation.getContractorName()));
				if (operation.getRigInformationUid()!=null) {
					RigInformation rig = (RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, operation.getRigInformationUid());
					if (rig!=null && rig.getRigManager()!=null) {
						LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rig.getRigManager());
						if (company!=null && company.getCompanyName()!=null) {
							attr.addAttribute("contractor", company.getCompanyName());
						}
					}
				}
				
				String sumDepthString = "SELECT SUM(a.depthMdMsl) FROM Activity a " +
									"WHERE (a.isDeleted=false or a.isDeleted is null) " +
									"AND a.operationUid = '" + operation.getOperationUid() + "'";
				List<Object> sumdepth = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sumDepthString);
				if (sumdepth != null){
					thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl");
					attr.addAttribute("depthMdMsl", thisConverter.getFormattedValue(nullToZero((Double) sumdepth.get(0))));
				}else{
					attr.addAttribute("depthMdMsl", "");
				}
				
				String fluidTypeString = "SELECT mp.mudMedium FROM MudProperties mp,Daily d  " +
										"WHERE (mp.isDeleted=false or mp.isDeleted is null) " +
										"AND (d.isDeleted=false or d.isDeleted is null) " +
										"AND mp.dailyUid = d.dailyUid " +
										"AND d.operationUid = '" + operation.getOperationUid() + 
										"' ORDER BY d.dayDate DESC";
				List<String> fluidtype = ApplicationUtils.getConfiguredInstance().getDaoManager().find(fluidTypeString);
				if (!fluidtype.isEmpty()){
				attr.addAttribute("fluidType", nullToEmptyString(fluidtype.get(0)));
				
				}		
				writer.startElement("Well",attr);
				String tempOperationUid = operation.getOperationUid();
				//formation
				String queryString2 = "Select f.operationUid, f FROM Formation f " +
					"WHERE (f.isDeleted=false or f.isDeleted is null) " +
					"AND f.wellUid=:wellUid " +
					"ORDER BY f.sampleTopMdMsl ";
				List<Object[]> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2,"wellUid", well.getWellUid(), qp);	
				
				for (Object[] rec2 : list2) {
					Formation formation = (Formation) rec2[1];
					attr = new SimpleAttributes();
					
					attr.addAttribute("formationName",nullToEmptyString(formation.getFormationName()));
					if (formation.getLogTopMdMsl()!= null){
						thisConverter.setReferenceMappingField(Formation.class, "logTopMdMsl");
						attr.addAttribute("logTopMdMsl",thisConverter.getFormattedValue(formation.getLogTopMdMsl()));
					}
					if (formation.getLogTopTvdMsl()!= null){
						thisConverter.setReferenceMappingField(Formation.class, "logTopTvdMsl");
						attr.addAttribute("logTopTvdMsl", thisConverter.getFormattedValue(formation.getLogTopTvdMsl()));
					}				
					if (formation.getSampleTopMdMsl()!= null){
						thisConverter.setReferenceMappingField(Formation.class, "sampleTopTvdMsl");
						attr.addAttribute("SampleTopMdMsl",thisConverter.getFormattedValue(formation.getSampleTopMdMsl()));						
					}
					attr.addAttribute("SampleTopTvdMsl", thisConverter.getFormattedValue(formation.getSampleTopTvdMsl()));					
					writer.addElement("Formation", null, attr);
				}
				
				String queryString3 = "Select cs.operationUid, cs.casingSectionUid,MAX(ct.casingOd),MAX(ct.weight),ct.grade,ct.csgThread, MAX(ct.jointintervalTo) FROM CasingSection cs, CasingTally ct " +
				"WHERE (cs.isDeleted=false or cs.isDeleted is null) " +
				"AND (ct.isDeleted=false or ct.isDeleted is null) " +
				"AND ct.casingSectionUid = cs.casingSectionUid " +
				"AND ct.numJoints > 0 " +
				"AND cs.operationUid = '" + tempOperationUid + "' GROUP BY ct.casingSectionUid";
				List<Object[]> list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString3, qp);	
				
				for (Object[] rec3 : list3) {
					
					attr = new SimpleAttributes();
					if (rec3[2] != null){
						thisConverter.setReferenceMappingField(CasingTally.class, "casingOD");
						attr.addAttribute("casingSize", thisConverter.getFormattedValue((Double) rec3[2]));
					}
					if (rec3[3] != null){
						thisConverter.setReferenceMappingField(CasingTally.class, "weight");
						attr.addAttribute("casingWeight",thisConverter.getFormattedValue((Double) rec3[3]));
					}
					if (rec3[6] != null){
						thisConverter.setReferenceMappingField(CasingTally.class, "jointintervalTo");
						attr.addAttribute("casingDepth", thisConverter.getFormattedValue((Double) rec3[6]));
					}
					attr.addAttribute("casingGrade", nullToEmptyString(rec3[4]));
					attr.addAttribute("casingThread", nullToEmptyString(rec3[5]));
					writer.addElement("Casing", null, attr);
													
				}
				
				String queryString4 = "Select pa.operationUid, pa, pad FROM PlugAndAbandon pa, PlugAndAbandonDetail pad " +
				"WHERE (pa.isDeleted=false or pa.isDeleted is null) " +
				"AND (pad.isDeleted=false or pad.isDeleted is null) " +
				"AND pa.plugAndAbandonUid = pad.plugAndAbandonUid " +
				"AND pa.operationUid = '" + tempOperationUid + "'";
				List<Object[]> list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString4, qp);	
				for (Object[] rec4 : list4) {
					
					PlugAndAbandonDetail plugandabandondetail= (PlugAndAbandonDetail) rec4[2];
					attr = new SimpleAttributes();
					
					attr.addAttribute("plugNumber", nullToEmptyString(plugandabandondetail.getPlugNumber()));
					attr.addAttribute("reason", nullToEmptyString(plugandabandondetail.getReason()));
					if (plugandabandondetail.getDepthTopMdMsl() != null){
						thisConverter.setReferenceMappingField(PlugAndAbandonDetail.class, "depthTopMdMsl");
						attr.addAttribute("depthTopMdMsl", thisConverter.getFormattedValue(plugandabandondetail.getDepthTopMdMsl()));
					}
					if (plugandabandondetail.getDepthBottomMdMsl() != null){
						thisConverter.setReferenceMappingField(PlugAndAbandonDetail.class, "depthBottomMdMsl");
						attr.addAttribute("depthBottomMdMsl", thisConverter.getFormattedValue(plugandabandondetail.getDepthBottomMdMsl()));
					}
					if (plugandabandondetail.getLength() != null){
						thisConverter.setReferenceMappingField(PlugAndAbandonDetail.class, "length");
						attr.addAttribute("length", thisConverter.getFormattedValue(plugandabandondetail.getLength()));
					}
										
					writer.addElement("PlugAndAbandon", null, attr);
				}				
				writer.endElement();
			}
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}

	public String queryDatActivityCodeOutput(String[] operations){
		String result = "<root/>";
		try{
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
			for (String operationUid : operations) {			
				SimpleAttributes attr = new SimpleAttributes();
				operationUomContext.setOperationUid(operationUid);
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				attr.addAttribute("spudDate", df.format(operation.getSpudDate()==null?new Date():(Date)operation.getSpudDate()));				
				String rigqs = "SELECT rigName FROM RigInformation WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid" ;
				List<String> origName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(rigqs, "rigInformationUid", operation.getRigInformationUid());
				if (origName.size()>0) { 
					attr.addAttribute("rigName", nullToEmptyString(origName.get(0)));
				}
				attr.addAttribute("wellName",CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid, null, true));
							
				double linetotal = 0.0 ;
				thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration",operationUomContext);
				attr.addAttribute("durationSymbol", thisConverter.getUomSymbol());
				String totalacttimeqs ="SELECT o.operationUid, a.classCode, sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d, Operation o " + 
									"WHERE (a.isDeleted = false or a.isDeleted is null) " +
									"AND (d.isDeleted = false or d.isDeleted is null) " +
									"AND (o.isDeleted = false or o.isDeleted is null) " +
									"AND d.operationUid = o.operationUid " +
									"AND d.dailyUid=a.dailyUid " +
									"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
									"AND (a.isOffline = 0 or a.isOffline is null) " +
									"AND (a.isSimop = 0 or a.isSimop is null) " +
									"AND o.operationUid =  :operationUid " +
									"GROUP BY a.classCode,a.operationUid";
				
				List <Object[]> actlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalacttimeqs,new String[]{"operationUid"},new Object[]{operationUid},qp);
				for (Object[] actlistobj : actlist) {
					thisConverter.setBaseValue((Double) actlistobj[2]);
					attr.addAttribute("act"+ nullToEmptyString(actlistobj[1]) + "Duration",thisConverter.formatOutputPrecision());		
					linetotal = linetotal + (Double) actlistobj[2];
					
				}
				thisConverter.setBaseValue(linetotal);
				attr.addAttribute("lineDuration",thisConverter.formatOutputPrecision());	
				
				double sumIHPH = 0.0;
				double sumICPC = 0.0;
				String totalphtimeqs ="SELECT o.operationUid, a.phaseCode, sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d, Operation o " + 
										"WHERE (a.isDeleted = false or a.isDeleted is null) " +
										"AND (d.isDeleted=false or d.isDeleted is null) " +
										"AND (o.isDeleted=false or o.isDeleted is null) " +
										"AND o.operationUid= :operationUid " +
										"AND d.operationUid=o.operationUid " +
										"AND d.dailyUid=a.dailyUid " +
										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isOffline = 0 or a.isOffline is null) " +
										"AND (a.isSimop = 0 or a.isSimop is null) " +
										"GROUP BY a.phaseCode,a.operationUid";
				List <Object[]> phlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalphtimeqs,new String[]{"operationUid"},new Object[]{operationUid},qp);
				for (Object[] phobject : phlist){
					thisConverter.setBaseValue((Double) phobject[2]);
					attr.addAttribute("ph"+nullToEmptyString(phobject[1])+"Duration",thisConverter.formatOutputPrecision());	
					if("IH".equals(phobject[1]) || "PH".equals(phobject[1])){							
						sumIHPH = sumIHPH + (Double) phobject[2];
					}
					if ("IC".equals(phobject[1]) || "PC".equals(phobject[1])) {
						sumICPC = sumICPC + (Double) phobject[2];
					}
				}
				thisConverter.setBaseValue(sumIHPH);
				attr.addAttribute("phIPHDuration",thisConverter.formatOutputPrecision());
				thisConverter.setBaseValue(sumICPC);
				attr.addAttribute("phIPCDuration",thisConverter.formatOutputPrecision());
			
				double sumNptIHPH = 0.0;
				double sumNptICPCPA = 0.0;
				double sumCumNptSHIHPH = 0.0;
				double sumCumNptSCICPC = 0.0;
				double sumCumNptOther = 0.0;
				double sumCumNptEPDuration = 0.0;
				double totalCumNpt = 0.0;
				double precentNpt = 0.0;
				String totalnpttimeqs ="SELECT a.operationUid, a.phaseCode, sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d, Operation o " + 
										"WHERE (a.isDeleted = false or a.isDeleted is null) " +
										"AND (d.isDeleted = false or d.isDeleted is null) " +
										"AND (o.isDeleted = false or o.isDeleted is null) " +
										"AND o.operationUid=:operationUid " +
										"AND d.operationUid=o.operationUid " +
										"AND d.dailyUid=a.dailyUid " +
										"AND (a.internalClassCode='TU' or a.internalClassCode='TP') " +
										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isOffline = 0 or a.isOffline is null) " +
										"AND (a.isSimop = 0 or a.isSimop is null) " +
										"GROUP BY a.phaseCode, a.operationUid";
				List <Object[]> nptlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalnpttimeqs,new String[]{"operationUid"},new Object[]{operationUid},qp);
				for (Object[] nptobject : nptlist){
					if("IH".equals(nptobject[1]) || "PH".equals(nptobject[1])) 
						sumNptIHPH = sumNptIHPH + (Double) nptobject[2];
					
					if ("IC".equals(nptobject[1]) || "PC".equals(nptobject[1]) || "PA".equals(nptobject[1])) 
						sumNptICPCPA = sumNptICPCPA + (Double) nptobject[2];

					if ("SH".equals(nptobject[1])|| "IH".equals(nptobject[1])|| "PH".equals(nptobject[1])) 
						sumCumNptSHIHPH = sumCumNptSHIHPH + (Double) nptobject[2];							

					if ("SC".equals(nptobject[1])|| "IC".equals(nptobject[1])|| "PC".equals(nptobject[1]))
						sumCumNptSCICPC = sumCumNptSCICPC + (Double) nptobject[2];

					if ("S".equals(nptobject[1]) || "PA".equals(nptobject[1]) ||  "C".equals(nptobject[1]) || "EI".equals(nptobject[1]) || "PS".equals(nptobject[1]))
						sumCumNptOther = sumCumNptOther + (Double) nptobject[2];
						
					if ("EP".equals(nptobject[1])) 	
						sumCumNptEPDuration = sumCumNptEPDuration + (Double) nptobject[2];
					
					
					thisConverter.setBaseValue((Double) nptobject[2]);
					attr.addAttribute("npt"+nullToEmptyString(nptobject[1])+"Duration",thisConverter.formatOutputPrecision());
				}
				totalCumNpt = sumCumNptOther + sumCumNptSHIHPH + sumCumNptSCICPC + sumCumNptEPDuration;
				if (linetotal>0.0) precentNpt =  Math.round((totalCumNpt/linetotal)*10000) / 100.0;
				
				thisConverter.setBaseValue(sumNptIHPH);
				attr.addAttribute("nptIPHDuration",thisConverter.formatOutputPrecision());
				
				thisConverter.setBaseValue(sumNptICPCPA);
				attr.addAttribute("nptICPCPADuration",thisConverter.formatOutputPrecision());
				
				thisConverter.setBaseValue(sumCumNptSHIHPH);
				attr.addAttribute("cumNptSHIHPHDuration",thisConverter.formatOutputPrecision());
				
				thisConverter.setBaseValue(sumCumNptSCICPC);
				attr.addAttribute("cumNptSCICPCDuration",thisConverter.formatOutputPrecision());
				
				thisConverter.setBaseValue(sumCumNptOther);
				attr.addAttribute("cumNPTOtherDuration",thisConverter.formatOutputPrecision());
				
				thisConverter.setBaseValue(totalCumNpt);
				attr.addAttribute("totalCumNPTDuration",thisConverter.formatOutputPrecision());
				
				attr.addAttribute("percentCumNPTDuration",formatter.format(precentNpt));
		
				double sumTtIHPH = 0.0;
				double sumTtICPCPA = 0.0;
				String totaltroubletimeqs ="SELECT a.operationUid, a.phaseCode, sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d, Operation o " + 
											"WHERE (a.isDeleted=false or a.isDeleted is null) " +
											"AND (d.isDeleted=false or d.isDeleted is null) " +
											"AND (o.isDeleted=false or o.isDeleted is null) " +
											"AND o.operationUid=:operationUid " +
											"AND a.internalClassCode='TP' " +
											"AND d.operationUid=o.operationUid " +
											"AND d.dailyUid=a.dailyUid " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
											"AND (a.isOffline = 0 or a.isOffline is null) " +
											"AND (a.isSimop = 0 or a.isSimop is null) " +
											"GROUP BY a.phaseCode, a.operationUid";
				List <Object[]> troubletimelist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totaltroubletimeqs,new String[]{"operationUid"},new Object[]{operationUid},qp);
			 
				for (Object[] troubletimeobj : troubletimelist){
					if("IH".equals(troubletimeobj[1])|| "PH".equals(troubletimeobj[1])){							
						sumTtIHPH = sumTtIHPH + (Double) troubletimeobj[2];
					}
					if ("IC".equals(troubletimeobj[1])|| "PC".equals(troubletimeobj[1]) || "PA".equals(troubletimeobj[1])){
						sumTtICPCPA = sumTtICPCPA + (Double) troubletimeobj[2];
					}
					thisConverter.setBaseValue((Double) troubletimeobj[2]);
					attr.addAttribute("tt"+nullToEmptyString(troubletimeobj[1])+"Duration",thisConverter.formatOutputPrecision());
				}
				thisConverter.setBaseValue(sumTtIHPH);
				attr.addAttribute("ttIPHDuration",thisConverter.formatOutputPrecision());
				thisConverter.setBaseValue(sumTtICPCPA);
				attr.addAttribute("ttICPCPADuration",thisConverter.formatOutputPrecision());
						
				double ttpeWSPFSHDuration = 0.0;			
				String taskcodelist = "'WSP','LOG','RW','RR','WSP','WTH','FSH'";
				String totaltroubletimepeqs ="SELECT a.operationUid,a.taskCode,sum(coalesce(a.activityDuration,0.0)) FROM Activity a, Daily d, Operation o " + 
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND (o.isDeleted=false or o.isDeleted is null) " +
							"AND o.operationUid= :operationUid " +
							"AND a.internalClassCode='TP' " +
							"AND a.taskCode IN ("+taskcodelist+")" +
							"AND d.operationUid=o.operationUid " +
							"AND d.dailyUid=a.dailyUid " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
							"AND (a.isOffline=0 or a.isOffline is null) " +
							"AND (a.isSimop=0 or a.isSimop is null) " +
							"GROUP BY a.taskCode, a.operationUid";

				List <Object[]> troubletimepelist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totaltroubletimepeqs,new String[]{"operationUid"},new Object[]{operationUid},qp);
				for (Object[] troubletimepeobj : troubletimepelist){
					if ("WSP".equals(troubletimepeobj[1]) || "FSH".equals(troubletimepeobj[1])) {
						ttpeWSPFSHDuration = ttpeWSPFSHDuration + (Double) troubletimepeobj[2];
					}
					thisConverter.setBaseValue((Double) troubletimepeobj[2]);
					attr.addAttribute("ttpe"+nullToEmptyString(troubletimepeobj[1])+"Duration",thisConverter.formatOutputPrecision());
				}
				thisConverter.setBaseValue(ttpeWSPFSHDuration);
				attr.addAttribute("ttpeWSPFDuration",thisConverter.formatOutputPrecision());	
				writer.addElement("activitydata", null, attr);
			}		
			
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public String queryDatEndOfMonthWellSummary(String[] operations){
		String result = "<root/>";
		try{
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			QueryProperties qp = new QueryProperties();
			
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			qp.setUomConversionEnabled(false);
			
			
			SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm");
			String operationids = StringUtils.join(operations, "','"); 
			String queryString = "select o.operationUid,w,wb,o FROM Well w, Wellbore wb, Operation o " +
				"WHERE (o.isDeleted=false or o.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (w.isDeleted=false or w.isDeleted is null) " +
				"AND w.wellUid=wb.wellUid " +
				"AND wb.wellboreUid=o.wellboreUid " +
				"and o.operationUid IN ('" + operationids + "')";
						
			List<Object[]> opslist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);		
				for (Object[] opslistobj : opslist) {	
					Well well = (Well) opslistobj[1];
					Wellbore wellbore = (Wellbore) opslistobj[2]; 
					Operation ops = (Operation) opslistobj[3];					
					SimpleAttributes attr = new SimpleAttributes();		
					operationUomContext.setOperationUid(ops.getOperationUid());
					CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
					
					String hsequery = "SELECT COUNT(hseIncidentUid) FROM HseIncident WHERE (isDeleted=false or isDeleted is null) AND operationUid=:opsid AND incidentCategory IN ('Lost Time Incident','Medical Treatment Incident')";
					List hse = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hsequery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (!hse.isEmpty()){
						if (hse.get(0).equals(0)){
							attr.addAttribute("gotLTI", nullToEmptyString("N"));
						}else{
							attr.addAttribute("gotLTI", nullToEmptyString("Y"));
						}
					}					
					String rigqs = "SELECT rigName FROM RigInformation WHERE (isDeleted=false or isDeleted is null) AND rigInformationUid =:riginfouid";
					List origName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(rigqs, new String[]{"riginfouid"}, new Object[]{ops.getRigInformationUid()},qp);
					
								
					
					attr.addAttribute("wellName", well.getWellName());
					attr.addAttribute("state", well.getState());
					attr.addAttribute("projNo", well.getAfebasis());
					attr.addAttribute("rig", nullToEmptyString(origName.get(0)));
					
					String scsizequery = "SELECT casingOd FROM CasingSection WHERE (isDeleted=false or isDeleted is null) AND operationUid =:opsid AND type = 'SC' ";
					List scsize = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(scsizequery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (!scsize.isEmpty()){
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), CasingSection.class, "casingOd",operationUomContext); 
						if (scsize.get(0) != null ){
							thisConverter.setBaseValue((Double) scsize.get(0));
						}else{
							thisConverter.setBaseValue(0.0);
						}
						attr.addAttribute("scSize", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
						attr.addAttribute("scSize_UOM", thisConverter.getUomSymbol());
					}
					
					String pcsizequery = "SELECT casingOd FROM CasingSection WHERE (isDeleted=false or isDeleted is null) AND operationUid =:opsid AND type = 'PC' ";
					List pcsize = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(pcsizequery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (!pcsize.isEmpty()){	
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), CasingSection.class, "casingOd",operationUomContext); 
						if (pcsize.get(0) != null ){
							thisConverter.setBaseValue((Double) pcsize.get(0));
						}else{
							thisConverter.setBaseValue(0.0);							
						}
						
						attr.addAttribute("pcSize", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
						attr.addAttribute("pcSize_UOM", thisConverter.getUomSymbol());
					}
					
					String TDquery = "SELECT operationUid,depthTvdMsl FROM Daily WHERE (isDeleted=false or isDeleted is null) AND operationUid =:opsid";
					List<Object[]> TDlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(TDquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Daily.class, "depthTvdMsl",operationUomContext);
					for (Object[] TDobj: TDlist){
						if (TDobj[1] != null){
							thisConverter.setBaseValue((Double) TDobj[1]);	
						}else{
							thisConverter.setBaseValue(0.0);	
						}
						
					}
					attr.addAttribute("TD", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					attr.addAttribute("TD_UOM", thisConverter.getUomSymbol());
					
					String emwquery = "SELECT estimatedMudWeight FROM LeakOffTest WHERE (isDeleted=false or isDeleted is null) AND operationUid =:opsid";
					List emw = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(emwquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (!emw.isEmpty()){	
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), LeakOffTest.class, "estimatedMudWeight",operationUomContext);
						if ((Double) emw.get(0) != 0 ){
							thisConverter.setBaseValue((Double) emw.get(0));
						}else{
							thisConverter.setBaseValue(0.0);							
						}
						
						attr.addAttribute("estimatedMudWeight", thisConverter.formatOutputPrecision(thisConverter.getBasevalue()));
					}
					
					
					
					attr.addAttribute("payType", well.getPurposeType());
					
					attr.addAttribute("intention", wellbore.getWellboreFinalPurpose());
					attr.addAttribute("tech", well.getTechnologyType());
					String querystr = "SELECT name FROM Basin WHERE (isDeleted=false or isDeleted is null) AND basinUid =:basinid";
					List basinName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(querystr, new String[]{"basinid"}, new Object[]{ well.getBasin()},qp);
					if (basinName.get(0) != null){
						attr.addAttribute("basin", nullToEmptyString(basinName.get(0)));
					}
								
					attr.addAttribute("block", well.getBlock());
					attr.addAttribute("rigMoveDist", nullToEmptyString(ops.getRigMoveFrom()) + "To" + nullToEmptyString(ops.getRigMoveTo()) + " " + nullToEmptyString(ops.getRigMoveDistance()));
					attr.addAttribute("spudDateTime",df.format(well.getSpudDatetime()==null?new Date():(Date)well.getSpudDatetime()));
					attr.addAttribute("tdDateTime", df.format(ops.getTdDateTdTime()==null?new Date():(Date)ops.getTdDateTdTime()));
								
					attr.addAttribute("releaseDate", df.format(ops.getRigreleaseDateRigreleaseTime()==null?new Date():(Date)ops.getRigreleaseDateRigreleaseTime()));
			
					attr.addAttribute("licenseArea", well.getLicenseNumber());
					attr.addAttribute("state", well.getState());
					if (ops.getAfe()!= null){
						attr.addAttribute("afeCost",thisConverter.formatOutputPrecision(ops.getAfe()));
					}
					if (ops.getAmountSpentPriorToSpud()!= null){
						attr.addAttribute("amtSpendPriorSpud", thisConverter.formatOutputPrecision(ops.getAmountSpentPriorToSpud()));
					}
					
										
					attr.addAttribute("preSpudCost", thisConverter.formatOutputPrecision(getPhaseTotalByCode(ops.getOperationUid(),"PS")));	
					attr.addAttribute("evaluationPhaseCost", thisConverter.formatOutputPrecision(getPhaseTotalByCode(ops.getOperationUid(),"EP") + getPhaseTotalByCode(ops.getOperationUid(),"EI")));
					
					double completionCost = 0.0; 
					completionCost = completionCost + getPhaseTotalByCode(ops.getOperationUid(),"PC") + getPhaseTotalByCode(ops.getOperationUid(),"PC1")+
					getPhaseTotalByCode(ops.getOperationUid(),"PC2") + getPhaseTotalByCode(ops.getOperationUid(),"PC3")+
					getPhaseTotalByCode(ops.getOperationUid(),"C") + getPhaseTotalByCode(ops.getOperationUid(),"CTB");
					
					if (completionCost == 0.0){
						completionCost  = completionCost + getPhaseTotalByCode(ops.getOperationUid(),"PA")+ getPhaseTotalByCode(ops.getOperationUid(),"ABN");
					}
					attr.addAttribute("completionCost", thisConverter.formatOutputPrecision(completionCost));
					
					double casingCost = 0.0;
					casingCost = casingCost + getPhaseTotalByCode(ops.getOperationUid(),"SC") + getPhaseTotalByCode(ops.getOperationUid(),"IC");
					attr.addAttribute("casingCost", thisConverter.formatOutputPrecision(casingCost)); 
					
					double casingCementCost = 0.0;
					casingCementCost = casingCementCost + getPhaseTotalByCode(ops.getOperationUid(),"SC") + getPhaseTotalByCode(ops.getOperationUid(),"IC")+
					getPhaseTotalByCode(ops.getOperationUid(),"PC") + getPhaseTotalByCode(ops.getOperationUid(),"PC1")+
					getPhaseTotalByCode(ops.getOperationUid(),"PC2") + getPhaseTotalByCode(ops.getOperationUid(),"PC3");
					attr.addAttribute("casingCementCost", thisConverter.formatOutputPrecision(casingCementCost));
										
					double holeCost = 0.0;
					holeCost = holeCost + getPhaseTotalByCode(ops.getOperationUid(),"PH") + getPhaseTotalByCode(ops.getOperationUid(),"PH1")+
					getPhaseTotalByCode(ops.getOperationUid(),"PH2") +	getPhaseTotalByCode(ops.getOperationUid(),"PH3");
					attr.addAttribute("holesCost", thisConverter.formatOutputPrecision(holeCost));
					
					String sumWellCostQuery = "SELECT SUM(quantity * item_cost) AS WellTotal FROM CostDailysheet WHERE (isDeleted=false or isDeleted is null) AND operationUid ='" + ops.getOperationUid()+ "'" ;
					List sumWellCost = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sumWellCostQuery);	
					if (sumWellCost.get(0) != null){
					attr.addAttribute("totalWellCost", thisConverter.formatOutputPrecision((Double) sumWellCost.get(0)));
					}
					
					//drilling phase rate 
					
					String maxdepthquery = "SELECT MAX(activityDuration) AS maxDepth FROM Activity " +
												"WHERE (isDeleted=false or isDeleted is null) " +
												"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
												"AND (isSimop=false or isSimop is null) " +
												"AND (isOffline=false or isOffline is null) " +
												"AND operationUid =:opsid " + 
												"AND phaseCode ='PH' AND taskCode = 'D'";
					List maxdepth = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(maxdepthquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);	
					
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration",operationUomContext);
					Double spudToTdDuration = (CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(ops.getSpudDate(), ops.getTdDateTdTime()));
					if (spudToTdDuration==null) spudToTdDuration = 0.0;
					Double scPhaseDuration = (calculateActivityDurationByPhase(ops.getOperationUid(),"SC"));
					if (scPhaseDuration==null) scPhaseDuration = 0.0;
					Double drllgPhaseRateValue = 0.0;
					if (spudToTdDuration - scPhaseDuration > 0.0 || spudToTdDuration - scPhaseDuration < 0.0) {
						if (maxdepth.get(0)!=null) {
							drllgPhaseRateValue = (Double) maxdepth.get(0) /(spudToTdDuration - scPhaseDuration);
						}
					}
					
					attr.addAttribute("drllgPhaseRate", nullToEmptyString(drllgPhaseRateValue));
					attr.addAttribute("preSpudTime", nullToEmptyString(ops.getDaysSpentPriorToSpud()));
					
					attr.addAttribute("surfaceIntCsgTime", thisConverter.formatOutputPrecision(convertTo12Or24Day(calculateActivityDurationByPhase(ops.getOperationUid(),"SC"), operationUomContext)));
					attr.addAttribute("spudToTdDuration", thisConverter.formatOutputPrecision(convertTo12Or24Day(spudToTdDuration, operationUomContext)));
									
					
					double spudtotdnocsg = spudToTdDuration - scPhaseDuration;
					thisConverter.setBaseValue(spudtotdnocsg);
					attr.addAttribute("spudToTdNoCsg", nullToEmptyString(thisConverter.getConvertedValue()));
					
					double spudToRigUpDuration = convertTo12Or24Day((CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(ops.getSpudDate(), ops.getRigreleaseDateRigreleaseTime())), operationUomContext);
					thisConverter.setBaseValue(spudToRigUpDuration);
					attr.addAttribute("spudToRigUpDuration", nullToEmptyString(thisConverter.getConvertedValue()));
					
					
					
					//total evaluation phase
					String totalEvalPhaseQuery = "SELECT SUM(activityDuration) AS actDuration FROM Activity " +
											"WHERE (isDeleted=false or isDeleted is null) " +
											"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
											"AND (isSimop=false or isSimop is null) " +
											"AND (isOffline=false or isOffline is null) " +
											"AND operationUid =:opsid " + 
											"AND phaseCode IN ('EP','EI')";
					List totalEvalPhase = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalPhaseQuery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration",operationUomContext);
					double totalevalphasevalue = 0.0;
					totalevalphasevalue = CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(ops.getSpudDate(), ops.getRigreleaseDateRigreleaseTime());
					if (totalEvalPhase.get(0) != null){
						attr.addAttribute("totalEvalPhase", thisConverter.formatOutputPrecision((Double) totalEvalPhase.get(0)));
						totalevalphasevalue = (totalevalphasevalue - (Double) totalEvalPhase.get(0));	
					}else{
						totalevalphasevalue = totalevalphasevalue - 0.0;
					}
					
					thisConverter.setBaseValue(totalevalphasevalue);
					attr.addAttribute("spudToRigReleaseNoEV", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					
					//total evaluation before TD
					String totalEvalBeforeTDQuery = "SELECT SUM(a.activityDuration) AS actDuration FROM Activity a, ReportDaily rd " +
										"WHERE (a.isDeleted=false or a.isDeleted is null) " +
										"AND (rd.isDeleted=false or rd.isDeleted is null) " +
 										"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isSimop=false or a.isSimop is null) " +
										"AND (a.isOffline=false or a.isOffline is null) " +
										"AND a.operationUid =:opsid " + 
										"AND (a.phaseCode = 'EP' or  a.phaseCode = 'EI') " +
										"AND a.dailyUid = rd.dailyUid " +
										"AND rd.reportDatetime >=:spuddate " +
										"AND rd.reportDatetime <=:tddatetime";
					List totalEvalBeforeTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalBeforeTDQuery, new String[]{"opsid","spuddate","tddatetime"}, new Object[]{ops.getOperationUid(),ops.getTdDateTdTime(),ops.getSpudDate()},qp);
					if (totalEvalBeforeTD.get(0) != null){
						attr.addAttribute("totalEvalBeforeTD", thisConverter.formatOutputPrecision((Double) totalEvalBeforeTD.get(0)));
					}
					//total eval after TD
					String totalEvalAfterTDQuery = "SELECT SUM(a.activityDuration) AS actDuration FROM Activity a, ReportDaily rd " +
										"WHERE (a.isDeleted=false or a.isDeleted is null) " +
										"AND (rd.isDeleted=false or rd.isDeleted is null) " +
											"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
										"AND (a.isSimop=false or a.isSimop is null) " +
										"AND (a.isOffline=false or a.isOffline is null) " +
										"AND a.operationUid =:opsid " + 
										"AND (a.phaseCode = 'EP' or  a.phaseCode = 'EI') " +
										"AND a.dailyUid = rd.dailyUid " +
										"AND rd.reportDatetime >=:tddatetime";
					List totalEvalAfterTD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalEvalAfterTDQuery, new String[]{"opsid","tddatetime"}, new Object[]{ops.getOperationUid(),ops.getTdDateTdTime()},qp);
					if (totalEvalAfterTD.get(0) != null){
					thisConverter.setBaseValue((Double) totalEvalAfterTD.get(0));
					attr.addAttribute("totalEvalAfterTD", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					}
					//NPT duration TP
					String totalNptTPQuery = "SELECT SUM(activityDuration) AS actDuration FROM Activity " +
										"WHERE (isDeleted=false or isDeleted is null) " +
										"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
										"AND (isSimop=false or isSimop is null) " +
										"AND (isOffline=false or isOffline is null) " +
										"AND operationUid =:opsid " +
										"AND internalClassCode = 'TP'";
					List totalnptvalueTP = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalNptTPQuery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					
					if (totalnptvalueTP.get(0) != null){
						thisConverter.setBaseValue((Double) totalnptvalueTP.get(0));	
						attr.addAttribute("totalNptTPDuration", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					}
					//NPT duration TU
					String totalNptTUQuery = "SELECT SUM(activityDuration) AS actDuration FROM Activity " +
										"WHERE (isDeleted=false or isDeleted is null) " +
										"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
										"AND (isSimop=false or isSimop is null) " +
										"AND (isOffline=false or isOffline is null) " +
										"AND operationUid =:opsid " + 
										"AND internalClassCode = 'TU'";
					List totalnptvalueTU = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(totalNptTUQuery,new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (totalnptvalueTU.get(0) != null){
						thisConverter.setBaseValue((Double) totalnptvalueTU.get(0));	
						attr.addAttribute("totalNptDuration", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					}
									
					
					String shdurationq = "SELECT SUM(activityDuration) AS actDuration FROM Activity " +
											"WHERE (isDeleted=false or isDeleted is null) " +
											"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
											"AND (isSimop=false or isSimop is null) " +
											"AND (isOffline=false or isOffline is null) " +
											"AND operationUid =:opsid " + 
											"AND phaseCode = 'SH'";
					List shduration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(shdurationq,  new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
					if (shduration.get(0) != null){								
						attr.addAttribute("surfaceHoleTime", thisConverter.formatOutputPrecision(convertTo12Or24Day((Double) shduration.get(0), operationUomContext)));
					}
					
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration",operationUomContext);				
					thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc( ops.getRigOnHireDate(),ops.getOnlocDateOnlocTime()));				
					attr.addAttribute("rigMoveTime", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(ops.getOnlocDateOnlocTime(),ops.getSpudDate()));
					attr.addAttribute("rigUpTime", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					
					
					String imsnumLTIquery = "SELECT imsNumber FROM HseIncident " +
											"WHERE isDeleted=false or isDeleted is null) " +
											"AND operationUid = '"+ops.getOperationUid()+"' " +
											"AND incidentCategory = 'Lost Time Incident'";
					//List imsnumLTI = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(imsnumLTIquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()});
					List imsnumLTI = ApplicationUtils.getConfiguredInstance().getDaoManager().find(imsnumLTIquery);
					if (imsnumLTI.get(0) != null){
						attr.addAttribute("imsnumLTI",nullToEmptyString(imsnumLTI.get(0)));
					}
					
					String imsnumMTIquery = "SELECT imsNumber FROM HseIncident " +
											"WHERE isDeleted=false or isDeleted is null) " +
											"AND operationUid = '"+ops.getOperationUid()+"' " + 
											"AND incidentCategory = 'Medical Treatment Incident'";
					//List imsnumMTI = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(imsnumMTIquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()});
					List imsnumMTI = ApplicationUtils.getConfiguredInstance().getDaoManager().find(imsnumMTIquery);
					if (imsnumMTI.get(0) != null){				
						attr.addAttribute("imsnumMTI",nullToEmptyString(imsnumMTI.get(0)));
					}					
					thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration",operationUomContext);
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"CMIC"));
					attr.addAttribute("CMIC",  thisConverter.formatOutputPrecision((thisConverter.getConvertedValue())));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"CMPC"));
					attr.addAttribute("CMPC", thisConverter.formatOutputPrecision((thisConverter.getConvertedValue())));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"CMSC"));
					attr.addAttribute("CMSC", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"EVAL"));
					attr.addAttribute("EVAL", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"IBH"));
					attr.addAttribute("IBH", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"ITDWT"));
					attr.addAttribute("ITDWT", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"LOT"));
					attr.addAttribute("LOT", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"NDB"));
					attr.addAttribute("NDB", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"NUB"));
					attr.addAttribute("NUB", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"PACMP"));
					attr.addAttribute("PACMP", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"PAOTH"));
					attr.addAttribute("PAOTH", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"PTDWT"));
					attr.addAttribute("PTDWT", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"RRIC"));
					attr.addAttribute("RRIC", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"RRPC"));
					attr.addAttribute("RRPC", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"RRSC"));
					attr.addAttribute("RRSC", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"RC"));
					attr.addAttribute("RC", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"SCWT"));
					attr.addAttribute("SCWT", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					thisConverter.setBaseValue(getDurationBasedOnUserCode(ops.getOperationUid(),"TOLDP"));
					attr.addAttribute("TOLDP", thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					
					String sqlstr = "SELECT SUM(quantity * item_cost) FROM CostDailysheet " +
								    "WHERE (isDeleted=false or isDeleted is null) " +
								    "AND operationUid=:opsid AND accountCode = '10307' "+
								    "AND phaseCode IN ('SH','SH1','SH2','SH3')";
					List mudshcost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlstr, new String[]{"opsid"},new Object[]{ops.getOperationUid()});
					if (mudshcost.get(0) != null){				
					attr.addAttribute("mudSurfaceCost",thisConverter.formatOutputPrecision((Double) mudshcost.get(0)));
					}	
					
					String mudcoststr = "SELECT SUM(quantity * item_cost) FROM CostDailysheet " +
				    				"WHERE (isDeleted=false or isDeleted is null) " +
				    				"AND operationUid=:opsid AND accountCode = '10307' "+
				    				"AND phaseCode IN ('PH','PH1','PH2','PH3','SC','EP','PC')";
					List mudprocost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(mudcoststr, new String[]{"opsid"},new Object[]{ops.getOperationUid()});
					if (mudprocost.get(0) != null){				
						attr.addAttribute("mudProductionCost",thisConverter.formatOutputPrecision((Double) mudprocost.get(0)));
					}			
					
					String qsiadctob = "SELECT br.operationUid ,SUM(bds.progress),SUM(bds.IADCDuration),SUM(bds.duration)FROM Bharun br, BharunDailySummary bds " +
										"WHERE (br.isDeleted=false or br.isDeleted is null) " +
										"AND (bds.isDeleted=false or bds.isDeleted is null) " +
										"AND bds.bharunUid = br.bharunUid " +
										"AND br.operationUid = :opsid " +
										"GROUP BY br.operationUid";
					List <Object[]> iadctobvalue = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(qsiadctob, new String[]{"opsid"},new Object[]{ops.getOperationUid()},qp);
					
					double footage = 0.0;
					double IADCdur = 0.0;
					double onBtmDur = 0.0;
					double IADCp10kvalue = 0.0;
					double TOBp10kvalue = 0.0;
					for (Object[] iadctobobj:iadctobvalue){
						if (iadctobobj[1] != null){
							footage =  footage + (Double) iadctobobj[1]/0.30480000000000003340608;
						}
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), BharunDailySummary.class, "IADCDuration",operationUomContext);
						if (iadctobobj[2] != null){
							thisConverter.setBaseValue(nullToZero((Double) iadctobobj[2]));
							IADCdur = IADCdur + thisConverter.getConvertedValue();
						}
						thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), BharunDailySummary.class, "duration",operationUomContext);
						if (iadctobobj[3] != null){
							thisConverter.setBaseValue((Double) iadctobobj[3]);
							onBtmDur = onBtmDur + thisConverter.getConvertedValue();
							
						}
					}
					
					IADCp10kvalue = IADCp10kvalue + (1/((footage/IADCdur)*24)*10000);
					TOBp10kvalue = TOBp10kvalue + (1/((footage/onBtmDur)*24)*10000);
					
					attr.addAttribute("IADCp10k",thisConverter.formatOutputPrecision(IADCp10kvalue));
					attr.addAttribute("TOBp10k",thisConverter.formatOutputPrecision(TOBp10kvalue));
					
					
					writer.addElement("wellMonthly", null, attr);
				}
				writer.close();
				result = bytes.toString("utf-8");
			}catch (Exception e) {
			 e.printStackTrace();
			 result = "<root/>";
		}
		return result;
	}
	
	
	public String queryDatActivityDowntimeSumByRig(String[] rigUids, Date startDate, Date endDate){
		String result = "<root/>";
		try{
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			formatter.setMinimumFractionDigits(2);
			formatter.setMaximumFractionDigits(2);
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), Activity.class, "activityDuration");
			SimpleAttributes attr = new SimpleAttributes();
			
			String standardConditionFilters = "" + 
							"WHERE (d.isDeleted = FALSE OR d.isDeleted IS NULL)  " +
							"AND (w.isDeleted = FALSE OR w.isDeleted IS NULL)  " +
							"AND (wb.isDeleted = FALSE OR wb.isDeleted IS NULL)  " +
							"AND (o.isDeleted = FALSE OR o.isDeleted IS NULL)  " +
							"AND d.operationUid=o.operationUid " +
							"AND o.wellboreUid=wb.wellboreUid " +
							(rigUids!=null && rigUids.length>0?"AND o.rigInformationUid in ('"+StringUtils.join(rigUids, "','")+"')":"") +
							"AND w.wellUid=wb.wellUid ";
			String standardConditionFiltersWithActivity = standardConditionFilters + 
						"AND  (a.isDeleted = FALSE OR a.isDeleted IS NULL) " +
						"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND d.dailyUid = a.dailyUid " +
						"";

			Map<String, String> rigMap = new HashMap<String, String>();

			int i = 1;
			
			//query all rigs and total duration
			String queryString = "SELECT ri.rigInformationUid, ri.rigName, sum(a.activityDuration) FROM Well w, Wellbore wb, Operation o, RigInformation ri, Daily d, Activity a " +
					standardConditionFiltersWithActivity + 
					"AND (ri.isDeleted=false or ri.isDeleted is null) " +
					"AND o.rigInformationUid=ri.rigInformationUid  " +
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"GROUP BY ri.rigInformationUid, ri.rigName " +
					"ORDER BY ri.rigName ";
			List<Object[]> riglist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"stdate","eddate"},new Object[]{startDate,endDate}, qp);
			for (Object[] rig : riglist) {
				String rigInformationUid = nullToEmptyString(rig[0]);
				String rigName = nullToEmptyString(rig[1]);
				Double totalDuration = (Double) rig[2];
				if (totalDuration==null) continue;
				
				rigMap.put(rigInformationUid, "R"+i);
				i++;
						
				attr = new SimpleAttributes();
				attr.addAttribute("rigName", rigName);
				attr.addAttribute("rigid", rigMap.get(rigInformationUid));

				Double duration_npt = 0.0;
				thisConverter.setBaseValue(totalDuration);
				attr.addAttribute("sumActDuration", thisConverter.formatOutputPrecision());
				
				//get trouble durations for each rig
				queryString = "SELECT a.internalClassCode, SUM(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Daily d, Activity a "+
						standardConditionFiltersWithActivity +
						"AND o.rigInformationUid =:rigid " +
						"AND a.internalClassCode in ('TP','TU') " +
						"AND d.dayDate BETWEEN :stdate AND :eddate " +
						"GROUP BY a.internalClassCode ";
				List <Object[]> durationList =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,new String[]{"rigid","stdate","eddate"}, new Object[] {rigInformationUid,startDate,endDate}, qp);
				for (Object[] rec : durationList) {
					String internalClassCode = nullToEmptyString(rec[0]);
					Double duration = (Double) rec[1];
					if (duration!=null) {
						thisConverter.setBaseValue(duration);
						attr.addAttribute("sum"+internalClassCode+"ActDuration", thisConverter.formatOutputPrecision());
						if ("TU".equals(internalClassCode) || "TP".equals(internalClassCode)) {
							duration_npt += duration;
						}
					}
				}
				
				if (totalDuration>0.0) {
					Double nptpercent = ((duration_npt) * 100.0 / totalDuration);
					attr.addAttribute("nptPercent", formatter.format(nptpercent));
				}
				writer.addElement("rig",null ,attr);
			} //end of rig for loop
			
			
			//breakdown by task code, class code for each rig
			writer.startElement("codeDuration");
			String querystring = "SELECT a.taskCode, a.classCode, o.rigInformationUid, SUM(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Activity a , Daily d " +
					standardConditionFiltersWithActivity + 
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"AND (a.classCode!='' and a.classCode is not null) " +
					"AND (a.taskCode!='' and a.taskCode is not null) " +
					"GROUP BY a.classCode,a.taskCode,o.rigInformationUid";
			List <Object[]> actbytskcode  =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(querystring,new String[]{"stdate","eddate"}, new Object[]{startDate,endDate},qp);
			for (Object[] actobj: actbytskcode){
				attr = new SimpleAttributes();
				String taskCode = nullToEmptyString(actobj[0]);
				String classCode = nullToEmptyString(actobj[1]);
				String rigid = nullToEmptyString(actobj[2]);
				Double duration = (Double) actobj[3];
				if (duration!=null && duration>0.0 && rigMap.containsKey(rigid)) {
					thisConverter.setBaseValue(duration);
					attr.addAttribute("duration" , thisConverter.formatOutputPrecision(thisConverter.getConvertedValue()));
					writer.addElement("sub_" + rigMap.get(rigid) + "_" + taskCode + "_" + classCode, null,attr);
				}
			}
			writer.endElement();

			//list all task code
			Map<String, String> taskCodeList = new HashMap<String, String>();
			writer.startElement("taskcode");
			String nptcodequery = "SELECT l.shortCode, l.name FROM Well w, Wellbore wb, Operation o, LookupTaskCode l,Activity a, Daily d " +
					standardConditionFiltersWithActivity +
					"AND (l.isDeleted = FALSE OR l.isDeleted IS NULL) " +
					"AND l.shortCode = a.taskCode " +
					"AND (l.operationCode=o.operationCode or l.operationCode='' or l.operationCode is null) " +
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"GROUP BY l.shortCode, l.name " +
					"ORDER BY l.name ";
			List<Object[]> nptcodelist  =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(nptcodequery,new String[]{"stdate","eddate"},new Object[]{startDate,endDate},qp);
			for (Object[] nptcodeobj: nptcodelist){
				attr = new SimpleAttributes();
				attr.addAttribute("code", nullToEmptyString(nptcodeobj[0]));
				attr.addAttribute("name", nullToEmptyString(nptcodeobj[1]));		
				writer.addElement("codeitem", null , attr);
				
				taskCodeList.put(nullToEmptyString(nptcodeobj[0]), nullToEmptyString(nptcodeobj[1]));
			}
			writer.endElement();
			 
			//breakdown task code duration by each rig
			Map<String, Double> durationMap = new HashMap<String, Double>();
			attr = new SimpleAttributes();
			writer.startElement("nptDurationByRig");
			queryString = "SELECT a.taskCode, o.rigInformationUid, SUM(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Activity a, Daily d " +
					standardConditionFiltersWithActivity + 
					"AND a.internalClassCode in ('TU','TP') " +
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"GROUP BY a.taskCode, o.rigInformationUid ";
			List <Object[]> nptcodebyrig  =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"stdate","eddate"}, new Object[]{startDate,endDate}, qp);
			for (Object[] nptcodebyrigobj: nptcodebyrig){
				attr = new SimpleAttributes();
				String rigId = nullToEmptyString(nptcodebyrigobj[1]);
				Double duration = (Double) nptcodebyrigobj[2];
				if (duration!=null && duration>0 && rigMap.containsKey(rigId)) {
					thisConverter.setBaseValue(duration);
					attr.addAttribute("Dduration", thisConverter.formatOutputPrecision());
					attr.addAttribute("Name",  nullToEmptyString(taskCodeList.get(nptcodebyrigobj[0])));
					writer.addElement("nptitem_"+rigMap.get(rigId), null,attr);
					
					if (durationMap.containsKey(rigMap.get(rigId))) {
						Double nptcodebyrigdur = durationMap.get(rigMap.get(rigId));
						nptcodebyrigdur += thisConverter.getConvertedValue();
						durationMap.put(rigMap.get(rigId), nptcodebyrigdur);
					} else {
						durationMap.put(rigMap.get(rigId), thisConverter.getConvertedValue());
					}
				}
			}
			for (Map.Entry<String, Double> entry : durationMap.entrySet()) {
				attr = new SimpleAttributes();
				attr.addAttribute("total", thisConverter.formatOutputPrecision(entry.getValue()));
				writer.addElement("nptgrandtotal_" + entry.getKey(), null,attr);
			}
			writer.endElement();

			//list all task code with duration
			writer.startElement("TotalNptCodeDuration");
			attr = new SimpleAttributes();
			nptcodequery = "SELECT a.taskCode, SUM(a.activityDuration) FROM Well w, Wellbore wb, Operation o, Activity a, Daily d " +
					standardConditionFiltersWithActivity +
					"AND a.internalClassCode in ('TU','TP') " +
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"GROUP BY a.taskCode " +
					"ORDER BY a.taskCode ";
			nptcodelist  =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(nptcodequery,new String[]{"stdate","eddate"},new Object[]{startDate,endDate},qp);
			Double sumtotalduration =0.0;
			for (Object[] nptcodeobj: nptcodelist){
				Double duration = (Double) nptcodeobj[1];
				String taskCode = nullToEmptyString(nptcodeobj[0]);
				if (duration != null && duration>0){
					thisConverter.setBaseValue(duration);
					sumtotalduration = sumtotalduration + duration;
					attr.addAttribute("totalduration", thisConverter.formatOutputPrecision());
					attr.addAttribute("totalname",  nullToEmptyString(taskCodeList.get(taskCode)));
					writer.addElement("npttotalitem", null,attr);
				}
			}
			attr = new SimpleAttributes();
			thisConverter.setBaseValue(sumtotalduration);	
			attr.addAttribute("total", thisConverter.formatOutputPrecision());
			writer.addElement("grandtotal", null,attr);
			writer.endElement();
			
			//List all class code
			String listclasscode = "SELECT a.classCode FROM Well w, Wellbore wb, Operation o, Activity a, Daily d " +
					standardConditionFiltersWithActivity +
					"AND d.dayDate BETWEEN :stdate AND :eddate " +
					"GROUP BY a.classCode " +
					"ORDER BY a.classCode";
 			List <Object> allclasscode  =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(listclasscode,new String[]{"stdate","eddate"},new Object[]{startDate,endDate},qp);
			attr = new SimpleAttributes();
			for (Object listcodeobj:allclasscode){
				String classCode = nullToEmptyString(listcodeobj);
				if (StringUtils.isNotBlank(classCode)) {
					attr = new SimpleAttributes();
					attr.addAttribute("code", classCode);		
					writer.addElement("classcode", null , attr);
				}
			}
 
 
			writer.close();
			result = bytes.toString("utf-8");
	
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}	
		return result;
	}
	
	
	public String queryWellSummaryforSantos(String[] operations){
		String result = "<root/>";
		try{
			DecimalFormat dcf = (DecimalFormat) DecimalFormat.getInstance(this.getCurrentHttpRequest().getLocale());
			dcf.setMinimumFractionDigits(3);
			dcf.setMaximumFractionDigits(3);
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy HH:mm");
			String queryString = "select o.operationUid,w,wb,o FROM Well w, Wellbore wb, Operation o " +
				"WHERE (o.isDeleted=false or o.isDeleted is null) " +
				"AND (wb.isDeleted=false or wb.isDeleted is null) " +
				"AND (w.isDeleted=false or w.isDeleted is null) " +
				"AND w.wellUid=wb.wellUid " +
				"AND wb.wellboreUid=o.wellboreUid " +
				"AND o.operationUid IN ('"+StringUtils.join(operations,"','")+"') " +
				"ORDER BY w.wellName, wb.wellboreName, o.operationName ";
			List<Object[]> opslist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString, qp);
			for (Object[] opslistobj : opslist) {
				
				Well well = (Well) opslistobj[1];
				Wellbore wellbore = (Wellbore) opslistobj[2]; 
				Operation ops = (Operation) opslistobj[3];
				operationUomContext.setOperationUid(ops.getOperationUid());
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()), ReportDaily.class, "depthMdMsl", operationUomContext);
				
				String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(ops.getOperationUid());
				
				SimpleAttributes attr = new SimpleAttributes();
				attr.addAttribute("operationUid", ops.getOperationUid());
				String templatNS = "";
				String templongEW = "";
				if ("N".equals(well.getLatNs())){
					templatNS = "North";
				}else if ("S".equals(well.getLatNs())){
					templatNS = "South";
				}
				if ("E".equals(well.getLongEw())){
					templongEW = "East";
				}else if ("W".equals(well.getLongEw())){
					templongEW = "South";
				}
				attr.addAttribute("wellname", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), ops.getOperationUid(), null, true));
				if (ops.getSpudDate() !=null) attr.addAttribute("spudDate",df.format(ops.getSpudDate()));
				if (ops.getRigOffHireDate()!=null) attr.addAttribute("rigOffHireDate", df.format(ops.getRigOffHireDate()));
				if (ops.getTdDateTdTime() != null) attr.addAttribute("tdDate", nullToEmptyString(df.format(ops.getTdDateTdTime())));
				
				attr.addAttribute("latNs", templatNS);
				if (well.getLatDeg()==null)  well.setLatDeg(0.0);
				thisConverter.setReferenceMappingField(Well.class, "latDeg", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLatDeg());
				attr.addAttribute("latDeg", thisConverter.getFormattedValue());

				if (well.getLatMinute()==null)  well.setLatMinute(0.0);
				thisConverter.setReferenceMappingField(Well.class, "latMinute", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLatMinute());
				attr.addAttribute("latMinute", thisConverter.getFormattedValue());

				if (well.getLatSecond()==null)  well.setLatSecond(0.0);
				thisConverter.setReferenceMappingField(Well.class, "latSecond", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLatSecond());
				attr.addAttribute("latSecond", thisConverter.getFormattedValue());
				
				attr.addAttribute("longEw", templongEW);
				if (well.getLongDeg()==null) well.setLongDeg(0.0);
				thisConverter.setReferenceMappingField(Well.class, "longDeg", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLongDeg());
				attr.addAttribute("longDeg", thisConverter.getFormattedValue());

				if (well.getLongMinute()==null)  well.setLongMinute(0.0);
				thisConverter.setReferenceMappingField(Well.class, "longMinute", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLongMinute());
				attr.addAttribute("longMinute", thisConverter.getFormattedValue());

				if (well.getLongSecond()==null)  well.setLongSecond(0.0);
				thisConverter.setReferenceMappingField(Well.class, "longSecond", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(well.getLongSecond());
				attr.addAttribute("longSecond", thisConverter.getFormattedValue());

				if (well.getGlHeightMsl()!=null) {
					thisConverter.setReferenceMappingField(Well.class, "glHeightMsl", operationUomContext.getUomTemplateUid());
					thisConverter.setBaseValue(well.getGlHeightMsl());
					attr.addAttribute("glElevation", thisConverter.getFormattedValue());
					
					Double rtElevation = well.getGlHeightMsl();
					String datumUid = operationUomContext.getDatumUid();
					List<OpsDatum> datumList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OpsDatum WHERE (isDeleted=false or isDeleted is null) and opsDatumUid=:opsDatumUid", "opsDatumUid", datumUid, qp);
					if (datumList.size()>0) {
						OpsDatum datum = datumList.get(0);
						if (datum.getReportingDatumOffset()!=null) {
							rtElevation += datum.getReportingDatumOffset();
						}
					}
					thisConverter.setBaseValue(rtElevation);
					attr.addAttribute("rtElevation", thisConverter.getFormattedValue());
				}
				
				String prevreleasequery = "SELECT o.rigOffHireDate FROM Well w, Wellbore wb, Operation o " +
							  "WHERE (o.isDeleted=false or o.isDeleted is null) " +
							  "AND (wb.isDeleted=false or wb.isDeleted is null) " +
							  "AND (w.isDeleted=false or w.isDeleted is null) " +
							  "AND w.wellUid=wb.wellUid " +
							  "AND wb.wellboreUid=o.wellboreUid " +
							  "AND w.nextWell = :wellname ";
				List <Date> prevrelease = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(prevreleasequery, new String[] {"wellname"}, new Object[] {well.getWellName()},qp);						
				for (Date prevrelobj:prevrelease){
					if (prevrelobj != null) {
						attr.addAttribute("prevRigReleaseDate", df.format(prevrelobj));
						break;
					}
				}
				
				String depthquery = "SELECT d.operationUid, MAX(rd.depthMdMsl) FROM ReportDaily rd, Daily d " +
								"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND d.dailyUid=rd.dailyUid " +
								"AND d.operationUid = :opsid " +
								"AND rd.reportType=:reportType " +
								"GROUP BY d.operationUid";
				List <Object[]> depthList =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(depthquery, new String[]{"opsid","reportType"}, new Object[]{ops.getOperationUid(), reportType}, qp);			 
				if (depthList.size()>0){
					Double depth = (Double) depthList.get(0)[1];
					if (depth!=null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl", operationUomContext.getUomTemplateUid());
						thisConverter.setBaseValue(depth);
						attr.addAttribute("totalDepth", thisConverter.getFormattedValue());
					}
				}

				Map<String, String> repnames = new HashMap<String, String>();
				String repquery = "SELECT rd.sentby FROM ReportDaily rd, Daily d " +
								  "WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
								  "AND (d.isDeleted=false or d.isDeleted is null) " +
								  "AND d.dailyUid=rd.dailyUid " +
								  "AND rd.reportType=:reportType " +
								  "AND d.operationUid = :opsid " +
								  "GROUP BY rd.sentby ";
				List <String> replist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(repquery, new String[]{"opsid", "reportType"}, new Object[]{ops.getOperationUid(), reportType}, qp);
				for (String sentby : replist) {
					if (StringUtils.isNotBlank(sentby)) {
						String[] arr_sentby = sentby.split("/");
						for (String name : arr_sentby) {
							name = name.trim();
							if (!repnames.containsKey(name.toUpperCase())) repnames.put(name.toUpperCase(), name);
						}
					}
				}
				String allNames = "";
				for (Map.Entry<String, String> entry : repnames.entrySet()) {
					allNames += (StringUtils.isNotBlank(allNames)?" / ":"") + entry.getValue();
				}
				attr.addAttribute("representative", allNames);
				
				String rigquery = "SELECT r.rigName, r.rigManager FROM ReportDaily rd, RigInformation r, Daily d " +
								  "WHERE (rd.isDeleted=false or rd.isDeleted is null)  " +
								  "AND (r.isDeleted=false or r.isDeleted is null) " +
								  "AND (d.isDeleted=false or d.isDeleted is null) " +
								  "AND d.dailyUid=rd.dailyUid " +
								  "AND rd.rigInformationUid = r.rigInformationUid " +
								  "AND rd.reportType=:reportType " +
								  "AND rd.operationUid = :opsid " +
								  "ORDER BY d.dayDate DESC";
				List <Object[]> riglist =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(rigquery, new String[]{"opsid", "reportType"}, new Object[]{ops.getOperationUid(), reportType}, qp);
				if (riglist.size()>0) {
					Object[] rigobj = riglist.get(0);
					if (rigobj[0] != null) attr.addAttribute("rigName", nullToEmptyString(rigobj[0]));
					if (rigobj[1] != null) {
						String rigManager = nullToEmptyString(rigobj[1]);
						LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rigManager);
						if (company!=null) rigManager = company.getCompanyName();
						attr.addAttribute("rigManager", rigManager);
					}
				}
				
				String dpSize = null;
				String dcSize = null;
				queryString = "SELECT d.operationUid, b FROM Bharun b, BharunDailySummary bd, Daily d " +
						"WHERE (b.isDeleted=false or b.isDeleted is null) " +
						"AND (bd.isDeleted=false or bd.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND b.bharunUid=bd.bharunUid " +
						"AND d.dailyUid=bd.dailyUid " +
						"AND d.operationUid=:opsid " +
						"ORDER BY d.dayDate DESC";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "opsid", ops.getOperationUid(), qp);
				for (Object[] rec : list) {
					Bharun bharun = (Bharun) rec[1];
					
					if (bharun.getDpOd()!=null && dpSize==null) {
						thisConverter.setReferenceMappingField(Bharun.class, "dpOd", operationUomContext.getUomTemplateUid());
						thisConverter.setBaseValue(bharun.getDpOd());
						attr.addAttribute("dpSize", thisConverter.getFormattedValue());
					}
					
					if (bharun.getDc1Od()!=null && dcSize==null) {
						thisConverter.setReferenceMappingField(Bharun.class, "dc1Od", operationUomContext.getUomTemplateUid());
						thisConverter.setBaseValue(bharun.getDc1Od());
						attr.addAttribute("dcSize", thisConverter.getFormattedValue());
					}
					
					if (dpSize!=null && dcSize!=null) break; 
				}
				
				writer.startElement("well",attr);	
				
				//rig equipment - OK
				attr = new SimpleAttributes();
				writer.startElement("rigEquipment",attr);				
				String rigpumpquery = "SELECT d.operationUid, rp.liner, rp.unitName,rp.unitNumber FROM RigPumpParam rp, Daily d " +
									  "WHERE (rp.isDeleted=false or rp.isDeleted is null) " +
									  "AND (d.isDeleted=false or d.isDeleted is null) " +
									  "AND rp.dailyUid=d.dailyUid " +
									  "AND d.operationUid=:operationUid " +
									  "GROUP BY rp.unitNumber,rp.unitName,rp.liner";
				List <Object[]> rigpumplist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(rigpumpquery, new String[]{"operationUid"}, new Object[]{ops.getOperationUid()}, qp); 
				for (Object[] rigpumpobj:rigpumplist){
					attr = new SimpleAttributes();
					if (rigpumpobj[1]!=null) {
						thisConverter.setReferenceMappingField(RigPumpParam.class, "liner", operationUomContext.getUomTemplateUid());
						thisConverter.setBaseValue((Double) rigpumpobj[1]);
						attr.addAttribute("pumpLinerId", thisConverter.getFormattedValue());
					}
					attr.addAttribute("pumpMake", nullToEmptyString(rigpumpobj[2]));
					attr.addAttribute("pumpNumber", nullToEmptyString(rigpumpobj[3]));
					writer.addElement("pumpDetails", null, attr);
				}
				writer.endElement();
				
				//drilling fluid - OK
				attr = new SimpleAttributes();
				writer.startElement("fluid",attr);	
				String fluidquery = "SELECT d.operationUid, mp.mudMedium, MIN(mp.depthMdMsl), MAX(mp.depthMdMsl) " +
						"FROM MudProperties mp, Daily d " +
						"WHERE (mp.isDeleted=false OR mp.isDeleted is null) " +
						"AND (d.isDeleted=false OR d.isDeleted is null) " +
						"AND d.operationUid =:opsid " +
						"AND d.dailyUid=mp.dailyUid " +
						"AND (mp.mudMedium!='' and mp.mudMedium is not null) " +
						"GROUP BY d.operationUid, mp.mudMedium " +
						"ORDER BY d.dayDate "; 
				List <Object[]> fluidlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(fluidquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
				for (Object[] fluidobj:fluidlist){
					attr = new SimpleAttributes();
					attr.addAttribute("fluidType", nullToEmptyString(fluidobj[1]));
					this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "depthMdMsl", (Double) fluidobj[2], "minDepth");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "depthMdMsl", (Double) fluidobj[3], "maxDepth");
					writer.startElement("fluidDetails", attr);
					
					//mud properties - OK
					String mudquery = "SELECT d.operationUid, mp FROM MudProperties mp, Daily d " +
									  "WHERE (mp.isDeleted=false OR mp.isDeleted is null) " +
									  "AND (d.isDeleted=false OR d.isDeleted is null) " +
									  "AND d.dailyUid=mp.dailyUid " +
									  "AND d.operationUid =:opsid " +
									  "AND mp.mudMedium=:mudMedium " +
									  "ORDER BY d.dayDate ";
					List <Object[]> mudlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(mudquery, new String[]{"opsid", "mudMedium"}, new Object[]{ops.getOperationUid(), nullToEmptyString(fluidobj[1])},qp);
					for (Object[] mudobj:mudlist){
						attr = new SimpleAttributes();
						MudProperties mp = (MudProperties) mudobj[1];
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "depthMdMsl", mp.getDepthMdMsl(), "mudDepth");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudWeight", mp.getMudWeight(), "mudDensity");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudFv", mp.getMudFv(), "mudViscosity");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "fluidLossApiVolume", mp.getFluidLossApiVolume(), "mudWL");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudPh", mp.getMudPh(), "mudPH");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudChlorideIonDensity", mp.getMudChlorideIonDensity(), "mudCL");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudPv", mp.getMudPv(), "mudPV");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudYp", mp.getMudYp(), "mudYP");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudGel10s", mp.getMudGel10s(), "mudGel10s");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudGel10m", mp.getMudGel10m(), "mudGel10m");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudGel30m", mp.getMudGel30m(), "mudGel30m");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "mudDissolvedSolidsConcentration", mp.getMudDissolvedSolidsConcentration(), "mudSolids");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, MudProperties.class, "drilledSolids", mp.getDrilledSolids(), "mudDrilledSolids");
						writer.addElement("mudDetails", null, attr);
					}	
									
					writer.endElement();
					
					
					
				}	
				writer.endElement();
				
				//casing and cementing
				writer.startElement("casingcement");	
				String csgcmtquery = "SELECT cs.operationUid,cs FROM CasingSection cs " +
								     "WHERE (cs.isDeleted=false OR cs.isDeleted is null) " +
								     "AND cs.wellboreUid =:wellboreUid " +
								     "ORDER BY installStartDate ";
				List <Object[]> csgcmtlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(csgcmtquery, new String[]{"wellboreUid"}, new Object[]{ops.getWellboreUid()},qp);
				for (Object[] csgcmtobj:csgcmtlist){
					attr = new SimpleAttributes();
					CasingSection cs = (CasingSection) csgcmtobj[1];
					if (cs.getCasingOd()!=null) {
						String casingSize = dcf.format(cs.getCasingOd() / 0.0254);
						attr.addAttribute("size", casingSize);
					}

					Double weight = null;
					Double jointintervalTo = null;
					String csgThread = null;
					String grade = null;
					String csgdetailquery ="SELECT o.operationUid, t FROM CasingTally t, CasingSection cs, Operation o " +
										   "WHERE (o.isDeleted=false OR o.isDeleted is null) " +
										   "AND (t.isDeleted=false OR t.isDeleted is null) " +
										   "AND (cs.isDeleted=false OR cs.isDeleted is null) " +
										   "AND cs.casingSectionUid=t.casingSectionUid " +
										   "AND cs.wellboreUid=o.wellboreUid " +
										   "AND o.operationUid=:operationdUid " +
										   "AND cs.casingSectionUid =:csgid " +
										   "ORDER BY t.jointintervalTo ";
					List <Object[]> csgdetaillist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(csgdetailquery, new String[]{"csgid", "operationdUid"}, new Object[]{cs.getCasingSectionUid(), ops.getOperationUid()},qp);
					for (Object[] csgobj:csgdetaillist){
						CasingTally csgTally = (CasingTally) csgobj[1];
						if (csgTally.getWeight()!=null) {
							if (weight==null) {
								weight = csgTally.getWeight();
							} else {
								if (weight < csgTally.getWeight()) weight = csgTally.getWeight(); 
							}
						}
						if (csgTally.getCsgThread()!=null) csgThread = csgTally.getCsgThread();
						if (csgTally.getGrade()!=null) grade = csgTally.getGrade();
						jointintervalTo = csgTally.getJointintervalTo();
					}
					attr.addAttribute("coupling", nullToEmptyString(csgThread));
					attr.addAttribute("grade", nullToEmptyString(grade));
					this.setXmlAttributes(attr, thisConverter, operationUomContext, CasingTally.class, "weight", weight, "weight");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, CasingTally.class, "jointintervalTo", jointintervalTo, "depth");
					
					//writer.startElement("casingcementDetails",attr);	
					String cementquery = "SELECT cj.operationUid, cf.fluidName, sum(coalesce(cf.amountMass,0.0)), MAX(cf.estimateTopMdMsl) FROM CementJob cj, CementStage cs, CementFluid cf " +
										 "WHERE (cj.isDeleted=false OR cj.isDeleted is null) " +
										 "AND (cs.isDeleted=false OR cs.isDeleted is null) " +
										 "AND (cf.isDeleted=false OR cf.isDeleted is null) " +
										 "AND cj.cementJobUid = cs.cementJobUid " +
										 "AND cs.cementStageUid = cf.cementStageUid " +
										 "AND cj.operationUid = :opsid " +
										 "AND cj.casingSectionUid = :casingid " +
										 "GROUP BY cj.operationUid, cf.fluidName";
					List <Object[]> cementlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(cementquery, new String[]{"opsid","casingid"}, new Object[]{cs.getOperationUid(),cs.getCasingSectionUid()},qp);
					for (Object[] cementobj:cementlist){
						String fluidName = nullToEmptyString(cementobj[1]);
						Double amount = (Double) cementobj[2];
						Double cementTop = (Double) cementobj[3];
						if ("Tail".equals(fluidName)) {
							this.setXmlAttributes(attr, thisConverter, operationUomContext, CementFluid.class, "amountMass", amount, "cementAmtTail");
							this.setXmlAttributes(attr, thisConverter, operationUomContext, CementFluid.class, "estimateTopMdMsl", cementTop, "cementTopTail");
						} else if ("Lead".equalsIgnoreCase(fluidName)) {
							this.setXmlAttributes(attr, thisConverter, operationUomContext, CementFluid.class, "amountMass", amount, "cementAmtLead");
							this.setXmlAttributes(attr, thisConverter, operationUomContext, CementFluid.class, "estimateTopMdMsl", cementTop, "cementTopLead");
						}
					}
					writer.addElement("casingcementDetails", null, attr);
				}
				writer.endElement();

				//survey - OK
				attr = new SimpleAttributes();
				writer.startElement("survey",attr);	
				String surveyquery = "SELECT sr.operationUid, ss.depthMdMsl, ss.azimuthAngle, ss.inclinationAngle FROM SurveyStation ss, SurveyReference sr " +
								      "WHERE (ss.isDeleted=false OR ss.isDeleted is null) " +
								      "AND (sr.isDeleted=false OR sr.isDeleted is null) " +
								      "AND sr.surveyReferenceUid = ss.surveyReferenceUid " +
								      "AND sr.wellboreUid = :wellboreUid " +
								      "ORDER BY ss.depthMdMsl";
				List <Object[]> surveylist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(surveyquery, new String[]{"wellboreUid"}, new Object[]{wellbore.getWellboreUid()},qp);
				for (Object[] surveyobj:surveylist){
					this.setXmlAttributes(attr, thisConverter, operationUomContext, SurveyStation.class, "depthMdMsl", (Double) surveyobj[1], "surveyDepth");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, SurveyStation.class, "azimuthAngle", (Double) surveyobj[2], "surveyAzimuthAngle");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, SurveyStation.class, "inclinationAngle", (Double) surveyobj[3], "surveyAngle");
					writer.addElement("surveyDetails", null, attr);
				}
				writer.endElement();
				
				//bit - OK
				double sumduration = 0.0;
				writer.startElement("bit");	
				String bitquery = "SELECT bha.operationUid,br,bha FROM Bitrun br,Bharun bha, BharunDailySummary bds, Daily d " +
								  "WHERE (br.isDeleted=false OR br.isDeleted is null) " +
								  "AND (bha.isDeleted=false OR bha.isDeleted is null) " +
								  "AND (bds.isDeleted=false OR bds.isDeleted is null) " +
								  "AND (d.isDeleted=false OR d.isDeleted is null) " +
								  "AND d.operationUid=:opsid " +
								  "AND bha.bharunUid=br.bharunUid " +
								  "AND bds.bharunUid=bha.bharunUid " +
								  "AND bds.dailyUid=d.dailyUid " +
								  "GROUP BY bha.operationUid,br,bha " +
								  "ORDER BY d.dayDate ";
				List <Object[]> bitlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bitquery, new String[]{"opsid"}, new Object[]{ops.getOperationUid()},qp);
				for (Object[] bitobj:bitlist){
					attr = new SimpleAttributes();
					Bitrun bit = (Bitrun)bitobj[1];
					Bharun bha = (Bharun)bitobj[2];
					
					attr.addAttribute("bitNo", nullToEmptyString(bit.getBitrunNumber()));
					attr.addAttribute("bitType", nullToEmptyString(bit.getModel()));
					this.setXmlAttributes(attr, thisConverter, operationUomContext, Bharun.class, "depthOutMdMsl", bit.getDepthOutMdMsl(), "bitDepthOut");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, Bitrun.class, "tfa", bit.getTfa(), "bitTfa");
					
					if (bit.getBitDiameter()!=null) {
						String bitSize = dcf.format(bit.getBitDiameter() / 0.0254);
						attr.addAttribute("bitSize", bitSize);
					}

					String nozzlequery = "SELECT nozzleQty,nozzleSize  FROM BitNozzle " +
									 "WHERE (isDeleted = false OR isDeleted is null) " +
									 "AND bitrunUid = :bitid ";
					List <Object[]> nozzlelist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(nozzlequery, new String[]{"bitid"}, new Object[]{bit.getBitrunUid()},qp);
					
		
					String nozzle = "";
					for (Object[] nozzleObj:nozzlelist){
						String tempnozzelqty = nullToEmptyString(nozzleObj[0]);
						thisConverter.setReferenceMappingField(BitNozzle.class, "nozzleSize", operationUomContext.getUomTemplateUid());
						thisConverter.setBaseValue(nullToZero((Double) nozzleObj[1]));
						String tempnozzelsize = nullToEmptyString(thisConverter.formatOutputPrecision());
						nozzle += (StringUtils.isNotBlank(nozzle)?", ":"") + tempnozzelqty + "x" + tempnozzelsize;
					}
					attr.addAttribute("bitJets", nullToEmptyString(nozzle));
					
					String bhadailyequery = "SELECT bha.operationUid, bds  FROM Bharun bha,BharunDailySummary bds, Daily d " +
					 						"WHERE (bha.isDeleted = false OR bha.isDeleted is null) " +
					 						"AND (bds.isDeleted = false OR bds.isDeleted is null) " +
					 						"AND (d.isDeleted = false OR d.isDeleted is null) " +
					 						"AND d.dailyUid=bds.dailyUid " +
					 						"AND bha.bharunUid = bds.bharunUid " +
					 						"AND bha.bharunUid = :bhaid " +
					 						"ORDER BY d.dayDate ";
					List <Object[]> bhadailylist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bhadailyequery, new String[]{"bhaid"}, new Object[]{bha.getBharunUid()},qp);
					Double sumprogress =0.0;
					Double sumiadcduration = 0.0;
					Double jetvelocity = null; 
					Double bitAvDP = null;
					Double bitAvDC = null;
					for (Object[] bhaobj:bhadailylist){
						BharunDailySummary bds = (BharunDailySummary) bhaobj[1];
						sumprogress += nullToZero(bds.getProgress());
						sumiadcduration += nullToZero(bds.getIADCDuration());
						sumduration += nullToZero(bds.getIADCDuration());
						jetvelocity = bds.getJetvelocity();
						bitAvDP = bds.getAnnvelDp();
						bitAvDC = bds.getAnnvelDc1();
					}
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "annvelDp", bitAvDP, "bitAvDP");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "annvelDc1", bitAvDC, "bitAvDC");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "jetvelocity", jetvelocity, "bitJetVel");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "progress", sumprogress, "bitProgress");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "IADCDuration", sumiadcduration, "bitHrs");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "IADCDuration", sumduration, "bitCumHrs");
					
					Double rop = 0.0;
					if (sumiadcduration>0.0) rop = sumprogress / sumiadcduration;
					this.setXmlAttributes(attr, thisConverter, operationUomContext, BharunDailySummary.class, "rop", rop, "bitROP");

					String drillParamQuery = "select d.operationUid, dp FROM DrillingParameters dp, Daily d " +
							"WHERE (dp.isDeleted=false or dp.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND d.dailyUid=dp.dailyUid " +
							"AND dp.bharunUid=:bhaid " +
							"ORDER BY d.dayDate DESC";
					List<Object[]> dpList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(drillParamQuery, "bhaid", bha.getBharunUid(), qp);
					if (dpList.size()>0) {
						DrillingParameters dp = (DrillingParameters) dpList.get(0)[1];
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "wobMin", dp.getWobMin(), "bitWOBmin");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "wobMax", dp.getWobMax(), "bitWOBmax");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "surfaceRpmMax", dp.getSurfaceRpmMax(), "bitRPMmax");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "surfaceRpmMin", dp.getSurfaceRpmMin(), "bitRPMmin");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "pressureAvg", dp.getPressureAvg(), "bitPressure");
						this.setXmlAttributes(attr, thisConverter, operationUomContext, DrillingParameters.class, "flowAvg", dp.getFlowAvg(), "bitFlow");
					}
					
					attr.addAttribute("I", nullToEmptyString(bit.getCondFinalInner()));
					attr.addAttribute("O", nullToEmptyString(bit.getCondFinalOuter()));
					attr.addAttribute("D", nullToEmptyString(bit.getCondFinalDull()));
					attr.addAttribute("L", nullToEmptyString(bit.getCondFinalLocation()));
					attr.addAttribute("B", nullToEmptyString(bit.getCondFinalBearing()));
					attr.addAttribute("G", nullToEmptyString(bit.getCondFinalGauge()));
					attr.addAttribute("fO", nullToEmptyString(bit.getCondFinalOther()).replace("\t", " "));
					attr.addAttribute("R", nullToEmptyString(bit.getCondFinalReason()));
					attr.addAttribute("bitRemark", nullToEmptyString(bit.getComment()));
					
					writer.addElement("bitDetails", null, attr);
				}
				writer.endElement();
				
				
				//formation - OK
				attr = new SimpleAttributes();
				writer.startElement("formation",attr);	
				String formationquery = "SELECT f.operationUid,f.formationName,f.sampleTopMdMsl,f.sampleTopTvdMsl FROM Formation f " +
										"WHERE (f.isDeleted = false OR f.isDeleted is null) " +
										"AND f.wellUid = :wellUid " +
										"ORDER BY f.sampleTopMdMsl";
				List <Object[]> formationlist  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(formationquery, new String[]{"wellUid"}, new Object[]{well.getWellUid()},qp);
				for (Object[] formationobj:formationlist){
					attr = new SimpleAttributes();
					attr.addAttribute("name", nullToEmptyString(formationobj[1]));
					this.setXmlAttributes(attr, thisConverter, operationUomContext, Formation.class, "sampleTopMdMsl", (Double) formationobj[2], "mdDepth");
					this.setXmlAttributes(attr, thisConverter, operationUomContext, Formation.class, "sampleTopTvdMsl", (Double) formationobj[3], "tvdDepth");
					writer.addElement("formationDetails", null, attr);
				}
				writer.endElement();
				
				writer.endElement();
			}
			writer.endElement();
			
			writer.close();
			result = bytes.toString("utf-8");
		}catch (Exception e){
			 e.printStackTrace();
			 result = "<root/>";
		}	
			return result;
	}
	
	private void setXmlAttributes(SimpleAttributes attr, CustomFieldUom thisConverter, OperationUomContext operationUomContext, Class referenceClass, String referenceProperty, Double baseValue, String attrName) throws Exception {
		thisConverter.setReferenceMappingField(referenceClass, referenceProperty, operationUomContext.getUomTemplateUid());
		if (thisConverter.isUOMMappingAvailable()) attr.addAttribute(attrName+"UOM", thisConverter.getUomSymbol());
		if (baseValue!=null) {
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValue(baseValue);
				attr.addAttribute(attrName, thisConverter.formatOutputPrecision());
			} else {
				attr.addAttribute(attrName, baseValue.toString());
			}
		}
	}
	
	public String querySTOPUpdate(String[] operations) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			SimpleAttributes attributes;		
			writer.startElement("root");
			SimpleAttributes items;
			String operationsuidlist = "''";
			double totalTicket=0.0;
			double safeTicket=0.0;
			double unsafeTicket=0.0;
			double totalClassificationCon=0.0;
			double totalClassificationBhv=0.0;
			CustomFieldUom thisformat = new CustomFieldUom(this.getLocale(this.getCurrentHttpRequest()));
			attributes = new SimpleAttributes();
			attributes.addAttribute("StopUpdateTitle","Category Hazard Situation::Classification::STOP Update");
			writer.startElement("datarow", null, attributes);
			
			for (String operationUid : operations) {		
				operationsuidlist+=",'" + operationUid + "'";}
	
			for (String operationUid : operations){
				
				// get hazard situation 4 safe and unsafe ticket count
				List safetyTicketDetailList;						
				String operationUidformat="'"+operationUid+"'";
				String safetyTicketDetailQuery = "SELECT stdet.numSafeTicket, stdet.numUnsafeTicket FROM SafetyTicket st, SafetyTicketDetail stdet, SafetyTicketCategory stc " +
						"WHERE (st.isDeleted = FALSE OR st.isDeleted IS NULL) AND (stc.isDeleted=FALSE OR stc.isDeleted IS NULL) AND (stdet.isDeleted=FALSE OR stdet.isDeleted IS NULL) " +
						"AND st.operationUid IN("+operationUidformat+") AND stc.safetyTicketUid = st.safetyTicketUid AND stdet.category = stc.safetyTicketCategoryUid " +
						"AND (stdet.numUnsafeTicket IS NOT NULL OR stdet.numSafeTicket IS NOT NULL)";
				safetyTicketDetailList   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(safetyTicketDetailQuery);				
				
				if(safetyTicketDetailList != null && !safetyTicketDetailList.isEmpty()){							
					items = new SimpleAttributes();	
					
					for(Object objResultsafetyTicketDet: safetyTicketDetailList){							
						Object[] objRstsafetyTicket = (Object[]) objResultsafetyTicketDet;
						totalTicket = totalTicket + (objRstsafetyTicket[0]==null?0.0:Double.parseDouble(objRstsafetyTicket[0].toString())) + (objRstsafetyTicket[1]==null?0.0:Double.parseDouble(objRstsafetyTicket[1].toString()));
						safeTicket = safeTicket + (objRstsafetyTicket[0]==null?0.0:Double.parseDouble(objRstsafetyTicket[0].toString()));
						unsafeTicket = unsafeTicket + (objRstsafetyTicket[1]==null?0.0:Double.parseDouble(objRstsafetyTicket[1].toString()));
					}							
				}
				
				List classificationCon;
				List classificationBhv;				
				
				String safetyTicketConQuery = "SELECT stdet.numSafeTicket, stdet.numUnsafeTicket FROM SafetyTicket st, SafetyTicketDetail stdet, SafetyTicketCategory stc " +
						"WHERE (st.isDeleted = FALSE OR st.isDeleted IS NULL) AND (stc.isDeleted=FALSE OR stc.isDeleted IS NULL) AND (stdet.isDeleted=FALSE OR stdet.isDeleted IS NULL) " +
						"AND st.operationUid IN ("+operationUidformat+") AND stc.safetyTicketUid = st.safetyTicketUid AND stdet.category = stc.safetyTicketCategoryUid " +
						"AND (stdet.numUnsafeTicket IS NOT NULL OR stdet.numSafeTicket IS NOT NULL) AND st.safetyClassification='condition'";
				classificationCon   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(safetyTicketConQuery);				
				
				if(classificationCon != null && !classificationCon.isEmpty()){							
					for(Object objclassificationCon: classificationCon){							
						Object[] objRobjclassificationCon = (Object[]) objclassificationCon;
						totalClassificationCon = totalClassificationCon + (objRobjclassificationCon[0]==null?0.0:Double.parseDouble(objRobjclassificationCon[0].toString())) + (objRobjclassificationCon[1]==null?0.0:Double.parseDouble(objRobjclassificationCon[1].toString()));
					}							
				}
								
				String safetyTicketBhvQuery = "SELECT stdet.numSafeTicket, stdet.numUnsafeTicket FROM SafetyTicket st, SafetyTicketDetail stdet, SafetyTicketCategory stc " +
						"WHERE (st.isDeleted = FALSE OR st.isDeleted IS NULL) AND (stc.isDeleted=FALSE OR stc.isDeleted IS NULL) AND (stdet.isDeleted=FALSE OR stdet.isDeleted IS NULL) " +
						"AND st.operationUid IN("+operationUidformat+") AND stc.safetyTicketUid = st.safetyTicketUid AND stdet.category = stc.safetyTicketCategoryUid " +
						"AND (stdet.numUnsafeTicket IS NOT NULL OR stdet.numSafeTicket IS NOT NULL)AND st.safetyClassification='behaviour'";
				classificationBhv   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(safetyTicketBhvQuery);				
				
				if(classificationBhv != null && !classificationBhv.isEmpty()){							
					for(Object objclassificationBhv: classificationBhv){							
						Object[] objRobjclassificationBhv = (Object[]) objclassificationBhv;
						totalClassificationBhv = totalClassificationBhv + (objRobjclassificationBhv[0]==null?0.0:Double.parseDouble(objRobjclassificationBhv[0].toString())) + (objRobjclassificationBhv[1]==null?0.0:Double.parseDouble(objRobjclassificationBhv[1].toString()));
					}							
				}	
						

			}
			
			List getIncidentNumLst;
			List getSafetyTicketNumNonClassLst;	
			List getLookupSafetyCatLst;
			
			//get # of incident for selected category
			/*
			String getIncidentNumSql = "SELECT incidentCategory,hseEventdatetime,numberOfIncidents FROM HseIncident WHERE operationUid IN ("+operationsuidlist+") " +
					"AND (isDeleted IS NULL OR isDeleted=FALSE) AND incidentCategory IN ('STOP Safe','STOP Unsafe') ORDER BY hseEventdatetime";
			
			getIncidentNumLst   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(getIncidentNumSql);	
			writer.startElement("IncidentNum");
			if(getIncidentNumLst != null && !getIncidentNumLst.isEmpty()){							
				for(Object objgetIncidentNumLst: getIncidentNumLst){							
					Object[] objRobjclassificationBhv = (Object[]) objgetIncidentNumLst;						
					writer.startElement("IncidentNum");
					writer.addElement("incidentCategory", objRobjclassificationBhv[0].toString());
					writer.addElement("month", objRobjclassificationBhv[1].toString());
					writer.addElement("numOfIncident", objRobjclassificationBhv[2].toString());
					writer.endElement();
				}						
			}	
			writer.endElement();
			*/
			String getIncidentNumSql = "SELECT st.safetyTicketDatetime,SUM(stdet.numSafeTicket),SUM(stdet.numUnsafeTicket) FROM SafetyTicket st, SafetyTicketDetail stdet, SafetyTicketCategory stc " + 
			"WHERE st.operationUid IN ("+operationsuidlist+") AND stc.safetyTicketUid = st.safetyTicketUid AND stdet.category = stc.safetyTicketCategoryUid AND (st.isDeleted IS NULL OR st.isDeleted=FALSE) " +
			"AND (stdet.isDeleted IS NULL OR stdet.isDeleted=FALSE) AND (stc.isDeleted IS NULL OR stc.isDeleted=FALSE) AND (stdet.numUnsafeTicket IS NOT NULL OR stdet.numSafeTicket IS NOT NULL) " +
			"GROUP BY MONTHNAME(st.safetyTicketDatetime) ORDER BY st.safetyTicketDatetime";
	
			getIncidentNumLst   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(getIncidentNumSql);	
			writer.startElement("IncidentNum");
			if(getIncidentNumLst != null && !getIncidentNumLst.isEmpty()){							
				for(Object objgetIncidentNumLst: getIncidentNumLst){							
					Object[] objRobjclassificationBhv = (Object[]) objgetIncidentNumLst;						
					writer.startElement("IncidentNum");
					writer.addElement("month", objRobjclassificationBhv[0].toString());
					writer.addElement("numOfSafe", objRobjclassificationBhv[1].toString());
					writer.addElement("numOfUnsafe", objRobjclassificationBhv[2].toString());
					writer.endElement();
				}						
			}	
			writer.endElement();
			
			//get all lookup safety category
			String getLookupSafetyCatSql = "SELECT categoryDescr FROM LookupSafetyCategory WHERE (isDeleted = FALSE OR isDeleted IS NULL) ORDER BY categoryDescr";
			getLookupSafetyCatLst   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(getLookupSafetyCatSql);
			writer.startElement("LookupSafetyCategory");
			if(getLookupSafetyCatLst != null && !getLookupSafetyCatLst.isEmpty()){
				for(Object objgetLookupSafetyCatLst: getLookupSafetyCatLst){
					writer.addElement("category",objgetLookupSafetyCatLst.toString());
				}
				
			}
			writer.endElement();		  
			
			//get # safety ticket which is not classification
			String getSafetyTicketNumNonClassSql ="SELECT lsc.categoryDescr, st.safetyTicketDatetime,SUM(stdet.numSafeTicket), SUM(stdet.numUnsafeTicket) FROM SafetyTicket st, SafetyTicketDetail stdet, SafetyTicketCategory stc, LookupSafetyCategory lsc " +
					"WHERE (st.isDeleted = FALSE OR st.isDeleted IS NULL) AND (stc.isDeleted=FALSE OR stc.isDeleted IS NULL) AND (stdet.isDeleted=FALSE OR stdet.isDeleted IS NULL) AND (lsc.isDeleted =FALSE OR lsc.isDeleted IS NULL)" +
					"AND st.operationUid IN("+operationsuidlist+") AND stc.safetyTicketUid = st.safetyTicketUid AND stdet.category = stc.safetyTicketCategoryUid " +
					"AND (stdet.numUnsafeTicket IS NOT NULL OR stdet.numSafeTicket IS NOT NULL) AND stc.lookupCategoryUid=lsc.lookupSafetyCategoryUid " +
					"GROUP BY stc.lookupCategoryUid,MONTHNAME(st.safetyTicketDatetime) ORDER BY st.safetyTicketDatetime";
			
			getSafetyTicketNumNonClassLst   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(getSafetyTicketNumNonClassSql);
			writer.startElement("NonClassifiedSafetyTicket");
			if(getSafetyTicketNumNonClassLst != null && !getSafetyTicketNumNonClassLst.isEmpty()){
				for(Object objgetSafetyTicketNumNonClassLst: getSafetyTicketNumNonClassLst){
					double totalTicketCount=0.0;
					Object[] objRobjgetSafetyTicketNumNonClassLst = (Object[]) objgetSafetyTicketNumNonClassLst;						
					writer.startElement("SafetyTicketNum");
					writer.addElement("safetyCategory", objRobjgetSafetyTicketNumNonClassLst[0].toString());
					writer.addElement("month", objRobjgetSafetyTicketNumNonClassLst[1]==null?"":objRobjgetSafetyTicketNumNonClassLst[1].toString());
					writer.addElement("numOfSafeTicket", objRobjgetSafetyTicketNumNonClassLst[2].toString());
					writer.addElement("numOfUnsafeTicket", objRobjgetSafetyTicketNumNonClassLst[3].toString());
					totalTicketCount = totalTicketCount + (objRobjgetSafetyTicketNumNonClassLst[2]==null?0.0:Double.parseDouble(objRobjgetSafetyTicketNumNonClassLst[2].toString())) + (objRobjgetSafetyTicketNumNonClassLst[3]==null?0.0:Double.parseDouble(objRobjgetSafetyTicketNumNonClassLst[3].toString()));
					writer.addElement("totalTicketCount", thisformat.formatOutputPrecision(totalTicketCount));
					writer.endElement();
				}						
			}
			writer.endElement();
			
			
			items = new SimpleAttributes();
			items.addAttribute("Safe", String.valueOf(safeTicket));
			items.addAttribute("Unsafe", String.valueOf(unsafeTicket));	
			writer.addElement("safetyTicketNum", "", items);
			
			items = new SimpleAttributes();
			items.addAttribute("Behaviour", String.valueOf(totalClassificationBhv));
			items.addAttribute("Condition", String.valueOf(totalClassificationCon));	
			writer.addElement("safetyTicketClass", "", items);	
			writer.endElement();

		
			writer.endAllElements();
			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;		
					
	}
	
	
	public String queryActivityDowntimeSummary(String operations, String params) {
		String result = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
			SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			
			DataDefinitionHQLQuery query = new DataDefinitionHQLQuery();
			String conditions = "";
			Calendar dateFrom = null;
			Calendar dateTo = null;
			
			for (String param : params.split("[|]")) {
				String[] paramValues = param.split("[:]");
				if (paramValues.length==2) {
					String key = paramValues[0];
					String value = paramValues[1];
					
					if ("classCode".equalsIgnoreCase(key)) {
						conditions += " and a.classCode in ('"+ StringUtils.join(value.split("[,]"), "','") +"') ";
					} else if ("phaseCode".equalsIgnoreCase(key)) {
						conditions += " and a.phaseCode in ('"+ StringUtils.join(value.split("[,]"), "','") +"') ";
					} else if ("taskCode".equalsIgnoreCase(key)) {
						conditions += " and a.taskCode in ('"+ StringUtils.join(value.split("[,]"), "','") +"') ";
					} else if ("rootCauseCode".equalsIgnoreCase(key)) {
						conditions += " and a.rootCauseCode in ('"+ StringUtils.join(value.split("[,]"), "','") +"') ";
					} else if ("country".equalsIgnoreCase(key)) {
						conditions += " and w.country=:country ";
						query.addParam("country", value);
					} else if ("rigInformationUid".equalsIgnoreCase(key)) {
						conditions += " and o.rigInformationUid=:rigInformationUid ";
						query.addParam("rigInformationUid", value);
					} else if ("operationUid".equalsIgnoreCase(key)) {
						conditions += " and operationUid=:operationUid ";
						query.addParam("operationUid", value);
					} else if ("year".equalsIgnoreCase(key)) {
						if (!"".equals(value)) {
							Integer year = Integer.parseInt(value);
							if (dateFrom==null) {
								dateFrom = Calendar.getInstance();
								dateFrom.set(year, 0, 1, 0, 0, 0);
							} else {
								dateFrom.set(year, dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DATE));
							}
							if (dateTo==null) {
								dateTo = Calendar.getInstance();
								dateTo.set(year+1, 0, 1, 0, 0, 0);
							} else {
								dateTo.set(year, dateTo.get(Calendar.MONTH), dateTo.get(Calendar.DATE));
							}
						}
					} else if ("month".equalsIgnoreCase(key)) {
						if ("Q1".equals(value) || "Q2".equals(value) || "Q3".equals(value) || "Q4".equals(value)) {
							value = value.replaceAll("[Q]","");
							Integer quarter = Integer.parseInt(value);
							Integer monthFrom = (quarter - 1) * 3;
							Integer monthTo = (quarter * 3);
							
							if (dateFrom==null) {
								dateFrom = Calendar.getInstance();
								dateFrom.set(1970, monthFrom, 1, 0, 0, 0);
							} else {
								dateFrom.set(dateFrom.get(Calendar.YEAR), monthFrom, dateFrom.get(Calendar.DATE));
							}
							
							if (dateTo==null) {
								dateTo = Calendar.getInstance();
							}
							dateTo.set(dateFrom.get(Calendar.YEAR), monthTo, 1, 0, 0, 0);
						} else {
							Integer month = null;
							for (int i=0 ; i < monthName.length; i++) {
								if (value.equalsIgnoreCase(monthName[i])) {
									month = i;
									break;
								}
							}
							
							if (month!=null) {
								if (dateFrom==null) {
									dateFrom = Calendar.getInstance();
									dateFrom.set(1970, month, 1, 0, 0, 0);
								} else {
									dateFrom.set(dateFrom.get(Calendar.YEAR), month, dateFrom.get(Calendar.DATE), 0,0,0);
								}
								
								if (dateTo==null) {
									dateTo = Calendar.getInstance();
								}
								dateTo.set(dateFrom.get(Calendar.YEAR), month+1, 1, 0, 0, 0);
							}
						}
					}
				}
			}
			if (dateFrom!=null) {
				conditions += "AND d.dayDate>=:fromDate ";
				query.addParam("fromDate", dateFrom.getTime());
				writer.addElement("startDate", df.format(dateFrom.getTime()));
			}
			if (dateTo!=null) {
				conditions += "AND d.dayDate<:toDate ";
				query.addParam("toDate", dateTo.getTime());
				writer.addElement("endDate", df.format(dateTo.getTime()));
			}
			conditions += "AND o.operationUid in (" + operations + ")";

			Date resultDateFrom = null;
			Date resultDateTo = null;
			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getCurrentUserSession().getUserLocale());
			String currentOperationUid = "";
			query.queryString = "SELECT o.operationUid, w, wb, o, d, a FROM Well w, Wellbore wb, Operation o, Daily d, Activity a " +
					"WHERE (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=a.dailyUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " + conditions +
					"ORDER BY w.wellName, wb.wellboreName, o.operationName, d.dayDate, a.startDatetime ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query.queryString, query.paramNames, query.paramValues, qp);
			for (Object[] rec : list) {
				String operationUid = (String) rec[0];
				//Well well = (Well) rec[1];
				//Wellbore wellbore = (Wellbore) rec[2];
				Operation operation = (Operation) rec[3];
				Daily daily = (Daily) rec[4];
				Activity activity = (Activity) rec[5];
				
				if (resultDateFrom==null) resultDateFrom = daily.getDayDate();
				if (resultDateTo==null) resultDateTo = daily.getDayDate();
				
				if (resultDateFrom.compareTo(daily.getDayDate())>0) resultDateFrom = daily.getDayDate(); 
				if (resultDateFrom.compareTo(daily.getDayDate())<0) resultDateTo = daily.getDayDate(); 
				
				if (!operationUid.equals(currentOperationUid)) {
					if (!"".equals(currentOperationUid)) writer.endElement();
					operationUomContext.setOperationUid(operationUid);
					currentOperationUid = operationUid;
					
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("operationUid", operationUid);
					attr.addAttribute("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(operation.getGroupUid(), operationUid));
					//TODO : calc NPT
					
					String queryString = "select d.operationUid, sum(a.activityDuration) FROM Activity a, Daily d " +
							"WHERE (d.isDeleted=false or d.isDeleted is null) " +
							"AND (a.isDeleted=false or a.isDeleted is null) " +
							"AND a.dailyUid=d.dailyUid " +
							"AND d.operationUid=:operationUid " +
							"AND a.internalClassCode in ('TP','TU') " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " + 
							"GROUP BY d.operationUid ";
					List<Object[]> nptList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, qp);
					if (nptList.size()>0) {
						thisConverter.setReferenceMappingField(Activity.class, "activityDuration", operationUomContext.getUomTemplateUid());
						if (nptList.get(0)[1]!=null) {
							thisConverter.setBaseValue((Double) nptList.get(0)[1]);
							attr.addAttribute("npt", thisConverter.formatOutputPrecision());
							attr.addAttribute("npt_uomSymbol", thisConverter.getUomSymbol());
						}

					}
					
					writer.startElement("Operation", attr);
				}
				
				SimpleAttributes attr = new SimpleAttributes();		
				attr.addAttribute("dayDate", df.format(daily.getDayDate()));
				attr.addAttribute("startTime", tf.format(activity.getStartDatetime()));
				attr.addAttribute("endTime", tf.format(activity.getEndDatetime()));
				attr.addAttribute("classCode", activity.getClassCode());
				attr.addAttribute("phaseCode", activity.getPhaseCode());
				attr.addAttribute("taskCode", activity.getTaskCode());
				attr.addAttribute("rootCauseCode", activity.getRootCauseCode());
				attr.addAttribute("descr", activity.getActivityDescription());
				attr.addAttribute("trouble", "TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())?"1":"0");
				
				//duration
				thisConverter.setReferenceMappingField(Activity.class, "activityDuration", operationUomContext.getUomTemplateUid());
				if (activity.getActivityDuration()!=null) {
					thisConverter.setBaseValue(activity.getActivityDuration());
					attr.addAttribute("duration", thisConverter.formatOutputPrecision());
					attr.addAttribute("duration_uomSymbol", thisConverter.getUomSymbol());
				}
				//depth
				thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl", operationUomContext.getUomTemplateUid());
				if (activity.getDepthMdMsl()!=null) {
					thisConverter.setBaseValue(activity.getDepthMdMsl());
					attr.addAttribute("depth", thisConverter.formatOutputPrecision());
					attr.addAttribute("depth_uomSymbol", thisConverter.getUomSymbol());
				}
				
				writer.addElement("Activity", "", attr);
			}
			if (!"".equals(currentOperationUid)) writer.endElement();
			if (dateFrom==null && resultDateFrom!=null) writer.addElement("startDate", df.format(resultDateFrom));
			if (dateTo==null && resultDateTo!=null) writer.addElement("endDate", df.format(resultDateTo));

			writer.close();
			result = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
}
