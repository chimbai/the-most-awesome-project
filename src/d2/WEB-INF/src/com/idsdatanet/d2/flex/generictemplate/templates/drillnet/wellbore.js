(function(window){
	
	D3.inherits(WellboreNameCustomField,D3.TextInputField);
	
	function WellboreNameCustomField(){
		D3.TextInputField.call(this);
	}
	
	WellboreNameCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			fieldEditor = WellboreNameCustomField.uber.getFieldEditor.call(this);
			fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			fieldEditor = WellboreNameCustomField.uber.getFieldEditor.call(this);
		}			
		return fieldEditor;
	}
	WellboreNameCustomField.prototype.onKeyUp = function(event){
		this.checkWellboreNameExist();
	}
	WellboreNameCustomField.prototype.checkWellboreNameExist = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	WellboreNameCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "checkingWellboreNameExist";
		params.name = this.getFieldEditor().value;
		params.isNewRecord = this._node.atts.isNewRecord? "1":"";
		params.parentWellUid = this._node.getFieldValue("wellUid");

		return params;
	}
	
	WellboreNameCustomField.prototype.loaded = function(response) {
		var self = this;
		this.fieldContainer.clearFieldUserMsgContainer();
		var isDuplicate = false;
		$(response).find('item').each(function(){
			if ($(this).attr('name')=='duplicate'){
				if ($(this).attr('value')=='true'){
					isDuplicate = true;
				}
			}
		});
		if (isDuplicate){
			var msg  = D3.LanguageManager.getLabel("warning.wellboreNameExist");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);

		}
			
	}
	
	D3.inherits(WellboreNodeListener,D3.EmptyNodeListener);
	
	function WellboreNodeListener()
	{
		
	}
	WellboreNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "wellboreNameInWWO") {
			return new WellboreNameCustomField();
		} else {
			return null;
		}
	}
	
	WellboreNodeListener.prototype.getConfirmationMessage = function(commandBeanProxy) {
		
		var wellbores = commandBeanProxy.rootNode.children["Wellbore"].getItems();
		var msg = null;
		D3.forEach(wellbores,function(node){
			var isLastWellbore = node.getFieldValue("@isLastWellbore");
			if (node.atts.action == D3.CommandBeanDataNode.STANDARD_ACTION_DELETE){
				if (isLastWellbore == "true" && isLastWellbore !=""){
					msg = D3.LanguageManager.getLabel("warning.lastWellbore");
				}
			}
		},this);
		return msg;
	}
		
	D3.CommandBeanProxy.nodeListener = WellboreNodeListener;
	
})(window);