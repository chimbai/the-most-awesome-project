(function(window){

	D3.inherits(IdsTableComboBox,D3.ComboBoxField);
	
	function IdsTableComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
	}

	IdsTableComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		IdsTableComboBox.uber.dispose.call(this);
	}
	
	IdsTableComboBox.prototype.retrieveLinkedToDepotTable = function() {
		var params = {};
		params._invokeCustomFilter = "get_linked_depot_table_name";
		params.depotType = this._node.getFieldValue("depotType");
		params.idsTableName = this._node.getFieldValue("idsTableName");
		if(params.depotType != "" && params.idsTableName != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.depotTableNameLoadCompleted, this);
		}
	}
	
	IdsTableComboBox.prototype.depotTableNameLoadCompleted = function(response) {
		var val = $(response).find("depotTableName").text();
		this._node.setFieldValue("depotTableName", val);
		this._schedulerEventDispatcher.fireIdsTableChangedEvent();
	};
	
	IdsTableComboBox.prototype.afterDataChanged = function(event) {
		this._node.setFieldValue("depotTableName","");
		this.retrieveLinkedToDepotTable();
		this._onChanged(event);
	}

	IdsTableComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireIdsTableChangedEvent(this._node.uid);
	}
	
	D3.inherits(DepotTableRecord,D3.AbstractFieldComponent);
	
	function DepotTableRecord(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
	}

	DepotTableRecord.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		IdsTableComboBox.uber.dispose.call(this);
	}
	
	DepotTableRecord.prototype.onIdsTableChanged = function() {
		this.refreshFieldRenderer();
	}
	
	DepotTableRecord.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotTableName");
		this.setFieldRendererValue(renderedText);
	}

	D3.inherits(IdsFieldComboBox,D3.ComboBoxField);

	function IdsFieldComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
	}
	
	IdsFieldComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		IdsFieldComboBox.uber.dispose.call(this);
	}

	IdsFieldComboBox.prototype.onIdsTableChanged = function(event) {
		this._refreshField(event);
	}

	IdsFieldComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveIdsFieldList();
		}		
	}

	IdsFieldComboBox.prototype.clearList = function() {
		this._idsFieldListLookup = null;
		this._node.setFieldValue("idsId","");
		this._node.setFieldValue("idsValue","");
		this.refreshFieldEditor();
	};

	IdsFieldComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("idsValue");
		this.setFieldRendererValue(renderedText);
	}

	IdsFieldComboBox.prototype.retrieveIdsFieldList = function() {
		var params = {};
		params._invokeCustomFilter = "get_ids_field_value_list";
		params.depotType = this._node.getFieldValue("depotType");
		params.idsTableName = this._node.getFieldValue("idsTableName");
		if(params.depotType != "" && params.idsTableName != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.idsFieldLoadCompleted, this);
		}
	}

	IdsFieldComboBox.prototype.getLookupRef = function() {
		return this._idsFieldListLookup;
	};

	IdsFieldComboBox.prototype.idsFieldLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.idsField") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find("idsFieldValue");
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item){
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					lookupReference.addLookupItem(newItem);
				});
				this._idsFieldListLookup = lookupReference;
				this._node.setFieldValue("idsId","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.idsField"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	IdsFieldComboBox.prototype._hasError = function(response) {
		var e = $(response).find("error");
		if (e.length > 0)
			return e.text();
		else
			return null;
	}

	IdsFieldComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._idsFieldListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("idsId", lookupItem.data);
		this._node.setFieldValue("idsValue", lookupItem.label);
		this._onChanged(event);
	}

	IdsFieldComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireIdsFieldChangedEvent(this._node.uid);
	}
	
	D3.inherits(DepotFieldComboBox,D3.ComboBoxField);

	function DepotFieldComboBox(dispatcher) {
		this._schedulerEventDispatcher = dispatcher;
		this._commandBean = dispatcher._commandBean;
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
		this._schedulerEventDispatcher.addEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_FIELD_CHANGED, this.onIdsFieldChanged, this);
	}

	DepotFieldComboBox.prototype.dispose = function() {
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_TABLE_CHANGED, this.onIdsTableChanged, this);
		this._schedulerEventDispatcher.removeEventListener(D3.DepotSchedulerEvent.DEPOT_IDS_FIELD_CHANGED, this.onIdsFieldChanged, this);
		this._schedulerEventDispatcher = null;
		this._commandBean = null;
		DepotFieldComboBox.uber.dispose.call(this);
	}

	DepotFieldComboBox.prototype.onIdsTableChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotFieldComboBox.prototype.onIdsFieldChanged = function(event) {
		this._refreshField(event);
	}
	
	DepotFieldComboBox.prototype.clearList = function() {
		this._depotFieldListLookup = null;
		this._node.setFieldValue("depotId","");
		this._node.setFieldValue("depotValue","");
		this._node.setFieldValue("depotCompositeId","");
		this.refreshFieldEditor();
	};

	DepotFieldComboBox.prototype._refreshField = function(event) {
		this._uid = this._node.uid;
		if (this._uid == event.nodeUid) {
			this.clearList();
			this.retrieveDepotFieldList();
		}
	}

	DepotFieldComboBox.prototype.getLookupRef = function() {
		return this._depotFieldListLookup;
	};

	DepotFieldComboBox.prototype.refreshFieldRenderer = function() {
		var renderedText = this._node.getFieldValue("depotValue");
		this.setFieldRendererValue(renderedText);
	}

	DepotFieldComboBox.prototype.retrieveDepotFieldList = function() {
		var params = {};
		params._invokeCustomFilter = "get_depot_value_list";
		params.depotType = this._node.getFieldValue("depotType");
		params.idsTableName = this._node.getFieldValue("idsTableName");
		params.idsId = this._node.getFieldValue("idsId");
		params.idsValue = this._node.getFieldValue("idsValue");
		if(params.depotType != "" && params.idsTableName != "" && params.idsId != "" && params.idsValue != "") {
			this._schedulerEventDispatcher.fireLoadStartedEvent();
			this._node.commandBeanProxy.customInvoke(params, this.depotFieldLoadCompleted, this);
		}
	}

	DepotFieldComboBox.prototype.depotFieldLoadCompleted = function(response) {
		var error = this._hasError(response);
		if(error) {
			var errorText = D3.LanguageManager.getLabel("error.retrievelist.depotField") + " \n" + error;
			D3.UIManager.popup("Error", errorText);
		} else {
			var r = $(response).find("depotValue");
			if(r.length > 0) {
				var lookupReference = new D3.LookupReference();
				var newItem = new D3.LookupItem();
				newItem.data = "";
				newItem.label = "";
				lookupReference.addLookupItem(newItem);

				r.each(function(index, item) {
					var newItem = new D3.LookupItem();
					var item = $(item);
					newItem.data = item.find("uid").text();
					newItem.label = item.find("name").text();
					newItem.composite = item.find("compositeId").text();
					lookupReference.addLookupItem(newItem);
				});

				this._depotFieldListLookup = lookupReference;
				this._node.setFieldValue("depotId","");
				this.refreshFieldEditor();
			} else {
				D3.UIManager.popup("Error", D3.LanguageManager.getLabel("empty.retrievelist.depotField"));
			}
		}
		this._schedulerEventDispatcher.fireLoadCompletedEvent();
	};

	DepotFieldComboBox.prototype.afterDataChanged = function(event) {
		var lookupItem = this._depotFieldListLookup.getLookupItem(this._combobox.value);
		this._node.setFieldValue("depotId", lookupItem.data);
		this._node.setFieldValue("depotValue", lookupItem.label);
		this._node.setFieldValue("depotCompositeId", lookupItem.composite);
		this._onChanged(event);
	}

	DepotFieldComboBox.prototype._onChanged = function(event) {
		this._schedulerEventDispatcher.fireDepotValueChangedEvent(this._node.uid);
	}

	DepotFieldComboBox.prototype._hasError = function(response) {
		var e = $(response).find("error");
		if (e.length > 0)
			return e.text();
		else
			return null;
	}
	
	D3.inherits(DepotWellOperationMappingDataNodeListener,D3.EmptyNodeListener);

	function DepotWellOperationMappingDataNodeListener() { 
		if (!this.dispatcher) {
			this._dispatcher = new D3.SchedulerEventDispatcher();
		}
	}

	DepotWellOperationMappingDataNodeListener.prototype.getCustomFieldComponent = function(id, node){
		var commandBean = node.commandBeanProxy;
		this._dispatcher._commandBean = commandBean;

		if(id == "idsTable"){
			return new IdsTableComboBox(this._dispatcher);
		} else if(id == "idsField"){
			return new IdsFieldComboBox(this._dispatcher);	
		}  else if(id == "depotTable"){
			return new DepotTableRecord(this._dispatcher);	
		} else if(id == "depotField"){
			return new DepotFieldComboBox(this._dispatcher);	
		} else {
			return null;
		}
	}

	DepotWellOperationMappingDataNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		node.setFieldValue("schedulerStatus","stop");
	}

	D3.CommandBeanProxy.nodeListener = DepotWellOperationMappingDataNodeListener;

})(window);