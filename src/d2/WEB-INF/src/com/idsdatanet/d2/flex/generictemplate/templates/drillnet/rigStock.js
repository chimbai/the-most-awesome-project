(function(window){
	D3.inherits(RigStockNodeListener,D3.ExcelImportTemplateNodeListener);
	
	function RigStockNodeListener() {}
	
	RigStockNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
		var importBtn = this.constructor.uber.getCustomRootButton(id,commandBeanProxy,btnDefinition);
	    if (importBtn != null) {
	    	return importBtn;
	    }else if (id == "wellIT-import-rigstock") {
	        var importBtn = document.createElement("button");
	        importBtn.className = "rootButtons";
	        importBtn.appendChild(document.createTextNode("WELS Import"));
	        importBtn.onclick = this.onclickWELSImport.bindAsEventListener(this);
	        return importBtn;
		}
        return null;
	};
	
	RigStockNodeListener.prototype.onclickWELSImport = function() {
		this.commandBeanProxy.addAdditionalFormRequestParams("action", "importWellIT");
		this.commandBeanProxy.submitForServerSideProcess();
    };
	
	D3.CommandBeanProxy.nodeListener = RigStockNodeListener;
})(window);