(function(window){
	D3.inherits(ShiftOperationDayListener,D3.EmptyNodeListener);
	
	function ShiftOperationDayListener()
	{
		
	}
	
	ShiftOperationDayListener.prototype.getConfirmationMessage = function(commandBeanProxy) {
		var msg = "Shift Operation Day will create/overwrite the following data at destination if required: \n" + 
		"- BHA and Bit Run \n" + 
		"- Survey \n" + 
		"Click [OK] to start shift selected day(s).";
		return msg;
	}

	D3.CommandBeanProxy.nodeListener = ShiftOperationDayListener;
	
})(window);