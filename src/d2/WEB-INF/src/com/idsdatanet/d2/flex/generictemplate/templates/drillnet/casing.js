(function(window){
	
	D3.inherits(CasingCementLinkField,D3.HyperLinkField);
	
	function CasingCementLinkField() {
		D3.HyperLinkField.call(this);
		this._linkButton = null;
	}
	
	CasingCementLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var casingSectionUid = this._node.getFieldValue("casingSectionUid");
			var openHoleId = this._node.getFieldValue("openHoleId");
			
			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				css:"casingSection-content div",
				
				context:this
			});
			this._linkButton = D3.UI.createElement({
				tag:"a",
				text:D3.LanguageManager.getLabel("link.casingCementLinkField"),
				css:"casingSection-content cementLink",
				attributes:{
					title:D3.LanguageManager.getLabel("tooltip.casingCementLinkField")
				},
				data:{
					casingSectionUid:casingSectionUid,
					openHoleId:openHoleId
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = "cementjob.html?casingSectionUid=" + data.casingSectionUid;
						var openHoleIdState = data.openHoleId;
						if(openHoleIdState)
							url += "&openHoleId=" + data.openHoleId;
						var msg = D3.LanguageManager.getLabel("message.casingCementLinkField");
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
			this._fieldRenderer.appendChild(this._linkButton);
		}
		return this._fieldRenderer;
	}
	
	CasingCementLinkField.prototype.refreshFieldRenderer = function (){}

	D3.inherits(CasingCementRecordLink,D3.AbstractFieldComponent);
	
	function CasingCementRecordLink(){
		D3.AbstractFieldComponent.call(this);
	}
	
	CasingCementRecordLink.prototype.getFieldRenderer = function() {
		if(!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
			});
			
			var paramURL = this._node.getFieldValue("@cementjob");
			var records = paramURL.split(",");
			
			for (i=0; i<records.length; i++) {
				var jobNumber = this.getParam("cementjobnumber", records[i]);
				var jobUid = this.getParam("cementjobuid", records[i]);
				var jobDailyUid = this.getParam("cementdailyuid", records[i]);
				var recordLink = D3.UI.createElement({
					tag:"a",
					text:D3.LanguageManager.getLabel(jobNumber),
					css:"casingSection-content recordLink",
					attributes:{
						title:D3.LanguageManager.getLabel("tooltip.casingCementRecordLink")
					},
					data:{
						jobUid:jobUid,
						jobDailyUid:jobDailyUid
					},
					callbacks:{
						onclick:function(event) {
							var link = event.target;
							var data = $(link).data();
							var url = "cementjob.html?gotoFieldName=cementJobUid&gotoFieldUid=" + data.jobUid;
							var jobDailyUidState = data.jobDailyUid;
							if(jobDailyUidState)
								url += "&gotoday=" + data.jobDailyUid;
							var msg = D3.LanguageManager.getLabel("message.casingCementRecordLink");
							this._node.commandBeanProxy.gotoUrl(url,msg);
						}
					},
					context:this
				});
				this._fieldRenderer.appendChild(recordLink);
			}
		}
		return this._fieldRenderer;
	}	
	
	CasingCementRecordLink.prototype.getParam = function (name, url) {
	    var match = RegExp(name + '=([^&]*)').exec(url);
	    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}
	
	CasingCementRecordLink.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(CasingSectionNodeListener,D3.EmptyNodeListener);
	
	function CasingSectionNodeListener() {}
	
	CasingSectionNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id=="CasingCementLink"){
			if (!node.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "CasingSection") && !node.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_EDIT)) {
				return null;
			}
			return new CasingCementLinkField();
		}else if (id=="CementRecordLink") {
			return new CasingCementRecordLink();
		}else {
			return null;
		}
	}
	D3.CommandBeanProxy.nodeListener = CasingSectionNodeListener;
	
})(window);