package com.idsdatanet.d2.flex.dat.customObject;

public class DVDCostTimeBreakDownObject {
	private String plannedDepth;
	private String plannedBudget;
	private String actualDepth;
	private String actualCumCost;
	private String durationP;
	private String durationTP;
	private String durationTU;
	private String durationU;
	
	public void setActualCumCost(String actualCumCost) {
		this.actualCumCost = actualCumCost;
	}
	public String getActualCumCost() {
		return actualCumCost;
	}
	public void setActualDepth(String actualDepth) {
		this.actualDepth = actualDepth;
	}
	public String getActualDepth() {
		return actualDepth;
	}
	public void setPlannedBudget(String plannedBudget) {
		this.plannedBudget = plannedBudget;
	}
	public String getPlannedBudget() {
		return plannedBudget;
	}
	public void setPlannedDepth(String plannedDepth) {
		this.plannedDepth = plannedDepth;
	}
	public String getPlannedDepth() {
		return plannedDepth;
	}	
	public void setDurationP(String durationP) {
		this.durationP = durationP;
	}
	public String getDurationP() {
		return durationP;
	}
	public void setDurationTP(String durationTP) {
		this.durationTP = durationTP;
	}
	public String getDurationTP() {
		return durationTP;
	}
	public void setDurationTU(String durationTU) {
		this.durationTU = durationTU;
	}
	public String getDurationTU() {
		return durationTU;
	}
	public void setDurationU(String durationU) {
		this.durationU = durationU;
	}
	public String getDurationU() {
		return durationU;
	}
	public DVDCostTimeBreakDownObject() {
		
	}
}
