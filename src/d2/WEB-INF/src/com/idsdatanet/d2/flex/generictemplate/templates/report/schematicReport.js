(function(window){
	
	D3.inherits(SchematicReportChooserField,D3.CheckboxField);
	SchematicReportChooserField.FIELD_SELECTED_DATE = "@schSelectedDate";
	SchematicReportChooserField.LATEST_REPORT_FLAG = "latestReport";
	function SchematicReportChooserField(){
		D3.CheckboxField.call(this);
	}
	
	SchematicReportChooserField.prototype.config = function(config) {
		if (!this._config){
			this._fieldName = SchematicReportChooserField.FIELD_SELECTED_DATE;
			config.label = D3.LanguageManager.getLabel("label.schematicReportChooser");
		}
		SchematicReportChooserField.uber.config.call(this,config);
		
		
	};
	SchematicReportChooserField.prototype.refreshFieldEditor = function() {
	}
	SchematicReportChooserField.prototype.getFieldEditor = function() {
		
		var fieldEditor = null;
		if (!this._fieldEditor) {
			
			var value = this._node.getFieldValue(SchematicReportChooserField.FIELD_SELECTED_DATE);
			if(! value) value = SchematicReportChooserField.LATEST_REPORT_FLAG;
			
			fieldEditor = SchematicReportChooserField.uber.getFieldEditor.call(this);
			
			$(this._checkbox).data("_node",this._node);
			$(this._checkbox).change(function(){
				var node = $(this).data("_node");
		        if (this.checked) {
		        	node.setFieldValue(SchematicReportChooserField.FIELD_SELECTED_DATE, SchematicReportChooserField.LATEST_REPORT_FLAG);
		        }else{
		        	var dt = new Date(value);
					if(dt == null) dt = new Date();
					value = formatDate(dt);
		        }
		        node.setFieldValue(SchematicReportChooserField.FIELD_SELECTED_DATE,value);

			});	
			if(value == SchematicReportChooserField.LATEST_REPORT_FLAG){
				$(this._checkbox).attr("checked",true);
	        	this._node.setFieldValue(SchematicReportChooserField.FIELD_SELECTED_DATE, SchematicReportChooserField.LATEST_REPORT_FLAG);
			}else{
				var dt = new Date(value);
				if(dt == null) dt = new Date();
				value = formatDate(dt);
				$(this._checkbox).attr("checked",false);			
			}
	        this._node.setFieldValue(SchematicReportChooserField.FIELD_SELECTED_DATE,value);

		}
		
		return fieldEditor;
	};
	
	function formatDate(date) {
		return date.format("DD MMM YYYY");
	}
	
	D3.inherits(SchematicReportListener,D3.DefaultReportListener);
	
	function SchematicReportListener()
	{
		D3.DefaultReportListener.call(this);
	}
	SchematicReportListener.prototype.getCustomFieldComponent = function(id, node){
		if (id=="sch_report_chooser"){
			return new SchematicReportChooserField();
		}else{
			return SchematicReportListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	
	D3.SchematicReportListener = SchematicReportListener;
	D3.CommandBeanProxy.nodeListener = SchematicReportListener;
	
})(window);