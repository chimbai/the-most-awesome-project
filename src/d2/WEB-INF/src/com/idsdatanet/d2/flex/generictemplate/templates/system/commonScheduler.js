(function(window) {
	
	
	D3.inherits(CommonSchedulerNodeListener,D3.EmptyNodeListener);
	
	function CommonSchedulerNodeListener()
	{
		
	}
	
	CommonSchedulerNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		if (node.simpleClassName() == "SchedulerJobDetail")
		{
			var serviceKeySelection = parent.getFieldValue("serviceKey");
			node.setFieldValue("serviceKey", serviceKeySelection);
			var typeSelection = parent.getFieldValue("type");
			node.setFieldValue("@type", typeSelection);
		}
	}
	D3.CommandBeanProxy.nodeListener = CommonSchedulerNodeListener;
	
})(window);