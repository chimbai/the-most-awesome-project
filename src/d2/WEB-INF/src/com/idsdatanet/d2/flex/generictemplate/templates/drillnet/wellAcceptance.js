(function(window){
	D3.inherits(RequirementEvidenceMatrix,D3.AbstractFieldComponent);
	
	function RequirementEvidenceMatrix() {
		D3.AbstractFieldComponent.call(this);
	}
	
	RequirementEvidenceMatrix.prototype.getFieldEditor = function() {
		if (!this._fieldEditor)
			this._fieldEditor = this.createRmIcon();
		return this._fieldEditor;
	}
	
	RequirementEvidenceMatrix.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer)
			this._fieldRenderer = this.createRmIcon();
		return this._fieldRenderer;
	}
	
	RequirementEvidenceMatrix.prototype.createRmIcon = function() {
		var img = document.createElement("img");
		img.setAttribute("class", "requirementEvidenceIcon");
		img.setAttribute("src", "./images/schematics/Annotation/Annotation.png");
		img.onclick = this.onClickRequirementEvidence.bindAsEventListener(this);
		return img;
	}
	
	RequirementEvidenceMatrix.prototype.onClickRequirementEvidence = function() {
		var _popUpDiv = "<div><img class='requirementEvidence' style='max-width: 70%; height: auto;' src='./images/wellacceptance/OP-9521_Requirement_Evidence.png'/></div>";
		this._popUpDialog = $(_popUpDiv);
		$(this._popUpDialog).dialog({
			title:D3.LanguageManager.getLabel("WellAcceptance.requirementEvidence"),
			dialogClass:"requirementEvidencePopUp",
			modal:true,
			width:($(window).width()*0.5),
			height:"auto",
			maxHeight:($(window).height()*0.95),
			resizable:true,
			draggable:false
		});
	}
	
	D3.inherits(WellAcceptanceNodeListener,D3.EmptyNodeListener);
	
	function WellAcceptanceNodeListener()
	{
		
	}
	
	WellAcceptanceNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "requirementEvidenceMatrix"){
			return new RequirementEvidenceMatrix();
		} else{
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = WellAcceptanceNodeListener;
})(window);