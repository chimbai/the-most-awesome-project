(function(window){
	D3.inherits(CustomRCMessage,D3.ComboBoxField);
	function CustomRCMessage(){ 
		D3.ComboBoxField.call(this);
	}
	
	CustomRCMessage.prototype.refreshFieldRenderer = function (){
		CustomRCMessage.uber.refreshFieldRenderer.call(this); 
		this.setDropDown();
	}

	CustomRCMessage.prototype.setDropDown = function (){
		var code = this._config.disableOnInternalClassCode;
		var _disableWhenInternalClassCode = code.split(",");
		
		var currentCode = this._node.getFieldValue("@classCode_internalCode");
		if (_disableWhenInternalClassCode.indexOf(currentCode)!=-1){
			this.fieldContainer._editable = false;
		}else{
			this.fieldContainer._editable = true;
		}
	}
	CustomRCMessage.prototype.data = function(data) {
		CustomRCMessage.uber.data.call(this, data);
		this.setDropDown();
	
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.dataChanged,this);
	}
	CustomRCMessage.prototype.dataChanged = function (event){
		if ((event.fieldName=="classCode" || event.fieldName=="rootCauseCode") && (this._config!=null))
		{
			var code = this._config.disableOnInternalClassCode;
			var _disableWhenInternalClassCode = code.split(",");
			var classCode = this._node.getFieldValue("classCode");
			var rootCauseCode = this._node.getFieldValue("rootCauseCode");
			var currentCode = this._node.getFieldValue("@classCode_internalCode");
			if (classCode != null && classCode != "") {
				if ( _disableWhenInternalClassCode.indexOf(currentCode)!=-1) {
					this.fieldContainer._editable = false;
					this._node.setFieldValue("rootCauseCode","");
				} else {
					this.fieldContainer._editable = true;
				}
			} else {
				this.fieldContainer._editable = false;
				this._node.setFieldValue("rootCauseCode","");
			}
		}	
	}
	
	D3.inherits(CustomActivityCodeField,D3.ComboBoxField);
	function CustomActivityCodeField(){
		D3.ComboBoxField.call(this);
	}
	CustomActivityCodeField.prototype.refreshFieldRenderer = function (){
		CustomActivityCodeField.uber.refreshFieldRenderer.call(this);
		this.setDropDown();
	}
	
	CustomActivityCodeField.prototype.setDropDown = function (){
		var code = this._config.disableOnInternalClassCode;
		var _disableWhenInternalClassCode = code.split(",");
		
		var currentCode = this._node.getFieldValue("@classCode_internalCode");
		if (_disableWhenInternalClassCode.indexOf(currentCode)!=-1){
			this.fieldContainer._editable = false;
		}else{
			this.fieldContainer._editable = true;
		}
	}
	CustomActivityCodeField.prototype.data = function(data) {
		CustomActivityCodeField.uber.data.call(this, data);
		this.setDropDown();
		
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.dataChanged,this);
	}
	CustomActivityCodeField.prototype.dataChanged = function (event){
		if (event.fieldName=="classCode" && this._config!=null)
		{
			var code = this._config.disableOnInternalClassCode;
			_disableWhenInternalClassCode = code.split(",");
			var currentCode = this._node.getFieldValue("@classCode_internalCode")
			var classCode = this._node.getFieldValue("classCode");
			if (classCode != null && classCode != "") {
				if ( _disableWhenInternalClassCode.indexOf(currentCode)!=-1) {
					this.fieldContainer._editable = false;
					this._node.setFieldValue("rootCauseCode","");
					this._node.setFieldValue("lookupCompanyUid","");
				} else {
					this.fieldContainer._editable = true;
				}
			} else {
				this.fieldContainer._editable = false;
				this._node.setFieldValue("rootCauseCode","");
				this._node.setFieldValue("lookupCompanyUid","");
			}	
		}
	}
	

	D3.inherits(BatchCodingButton,D3.AbstractFieldComponent);
	
	function BatchCodingButton() {
		
	}
	BatchCodingButton.prototype.listenTtoRootButton = function(e) {
		if(e.reason == "3")
			this.editMode=true;
		else this.editMode=false;
	}
	BatchCodingButton.prototype.getFieldRenderer = function() {
		this._node.commandBeanProxy.addEventListener(D3.CommandBeanProxyEvent.REDRAW_ROOT_BUTTONS,this.listenTtoRootButton,this);
		if (!this._fieldRenderer) {
			var label = D3.LanguageManager.getLabel("label.batchCoding");
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("label.batchCoding"),
				},
				callbacks:{
					onclick:this.showPopup
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	BatchCodingButton.prototype.showPopup = function(){
		if (this.editMode) alert(D3.LanguageManager.getLabel("warning.InEditMode"));
		else if (this.countSelectedNodes()==0) alert(D3.LanguageManager.getLabel("warning.NodataSelected"));
		else{
			if (!this._container){
				this.initiateContainer();
			}
			this.loadData();
			var self = this;
			var btnConfirm = {
					text:D3.LanguageManager.getLabel("button.confirm"),
					click:function(){
						self.btnConfirm_onClick();
						$(this).dialog('close');
					}
			};
			var btnCancel = {
					text:D3.LanguageManager.getLabel("button.cancel"),
					click:function(){
						self.updateCompleted();
		                $(this).dialog('close');
					}
			};
			
			$(this._container).dialog({
				title:D3.LanguageManager.getLabel("label.batchCoding"),
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, width:"900", 
				height:"500", 
				resizable:false,
				closable:false,
				buttons: [btnConfirm,btnCancel],
				open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        }
			});
		}
	}
	BatchCodingButton.prototype.countSelectedNodes = function(){
		var count = 0;
		var selectedUID = "";

		D3.forEach(this._node.children.Activity._items, function(items) {
			if (items.atts.selected) {
				count = count + 1;
				if (selectedUID) selectedUID = selectedUID + "," + items.primaryKeyValue;
				else selectedUID = items.primaryKeyValue;
			}
		});
		this._selectedUID = selectedUID;
		return count;
	}
	BatchCodingButton.prototype.btnConfirm_onClick = function(){
		var params = {};
		params._invokeCustomFilter = "updateBatchCoding";
		var fields = "";
		for (var key in this._fieldList) {
			if (key!=null){
				var value = this._fieldList[key];
				if (value && value!=null){
					params[key]=value;
					if (fields) fields = fields +  "," + key;
					else fields = key;
				}
			}
	    }
		params["fields"] = fields;
		params["primaryKey"] = this._selectedUID;
		this._node.commandBeanProxy.customInvoke(params,this.updateCompleted,this);
	}
	BatchCodingButton.prototype.updateCompleted = function(){
		var currentPage = this._node.commandBeanProxy.getPagination().currentPage;
		this._node.commandBeanProxy.load(currentPage,"","");
	}
	BatchCodingButton.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div>" +
					"<label style='fontWeight:bold; color:#FD0136'>" + D3.LanguageManager.getLabel("warning.batchCoding") + "</label>" +
	 				"<table>"+
	 				"<tbody class='batchCodeItem-TBODY'></tbody></table>"+
				"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
	
			this._tbody = container.find(".batchCodeItem-TBODY").get(0);
	
		}
	}
	BatchCodingButton.prototype.loadData = function(){
		var params = {};
		
		params._invokeCustomFilter = "collectBatchCodingDefinition";
		this._node.commandBeanProxy.customInvoke(params,this.batchCodingListLoaded,this);
	}
	BatchCodingButton.prototype.batchCodingListLoaded = function(response){
		var doc = $(response);
		this._fieldList = {};
		D3.UI.removeAllChildNodes(this._tbody);
		var tbody = this._tbody;
		var fieldlist = {};
		var self = this;
		doc.find("codingDefinition").each(function(index,item){
			var data = $(this);
			var fieldName = data.find("fieldName").text();
			var title = data.find("title").text();
			var lookupItem = data.find("lookupItem");
			fieldlist[fieldName] = "";
			var tr = document.createElement("tr");
			tbody.appendChild(tr);
			var td = document.createElement("td");
			tr.appendChild(td);
			var label = document.createElement("label");
			label.appendChild(document.createTextNode(title));
			td.appendChild(label);
			var td2 = document.createElement("td");
			tr.appendChild(td2);
			var select = "<select id='"+fieldName+"' uuid='"+fieldName+"' class='codingSelect' >";
			var length = lookupItem.length;
			if (length > 0){
				select = select + "<option value=''></option>";
				select = select + "<option value='-'>-</option>";
			}
			for (i=0; i<length; i++) {
				var lookup = lookupItem[i];
				var code = $(lookup).find("code").text();
				var label = $(lookup).find("label").text();
				select = select +
				"<option value='"+code+"' >"+label+"</option>";
			}
			select = select + "</select>";
			var selection = $(select);
		//	var selection = $(select).get(0);
			selection.data("fieldComponent",self);
			td2.appendChild(selection.get(0));
			//selection.onchange = self.onDataChanged.bindAsEventListener(self);

		},this);
		this._fieldList = fieldlist;
		$(".codingSelect").change(function(){
			var me = $(this);
			var fc = me.data("fieldComponent");
			fc._fieldList[me.attr("uuid")] = me.val();
		});
		
	}
	
	BatchCodingButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(ActivityNptEventLink,D3.ComboBoxField);
	
	
	function ActivityNptEventLink(){
		D3.ComboBoxField.call(this,false);
	}
	ActivityNptEventLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = document.createElement("a");
			this._fieldRenderer.className = "fieldRenderer hyperlinkField";
			this._fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}
		return this._fieldRenderer;
	};
	
	ActivityNptEventLink.prototype.onclick = function(event){
		var redirectUrl = "activitynptevent.html?gotoFieldName=unwantedEventUid&gotoFieldUid=" + escape(this._node.getFieldValue("nptEventUid"));
		this._node.commandBeanProxy.gotoUrl(redirectUrl, D3.LanguageManager.getLabel("Activity.nptEventLink.redirect"));
	}
	
	D3.inherits(ActivityCustomTimeField,D3.TimeField);
	
	function ActivityCustomTimeField(){
		D3.TimeField.call(this);
	}
	
	ActivityCustomTimeField.prototype.onTimeChanged = function(hour,min){
		var fieldName = this._config.name;
		var timeZoneOffset = new Date().getTimezoneOffset();
		var start = parseInt(this._node.getFieldValue("startDatetime"));
		var end = parseInt(this._node.getFieldValue("endDatetime"));
		
		if(!isNaN(start) && !isNaN(end)) {
			var test = new Date(start).toTimeString();
			var test2 = Date.parse(test);
			start = new Date(start);
			start = ((start.getUTCHours()*60*60*1000) + (start.getUTCMinutes()*60*1000)) + (start.getUTCSeconds() * 1000)+(start.getUTCMilliseconds()==999?999:0);
			end = new Date(end);
			end = ((end.getUTCHours()*60*60*1000) + (end.getUTCMinutes()*60*1000)) + (end.getUTCSeconds() * 1000)+(end.getUTCMilliseconds()==999?999:0);
			
			
			if (start == 86399999) start = 86400000;
			if (end == 86399999) end = 86400000;
	
			var duration = (end - start)/60/60/1000;
			if (duration <0){
				this._node.setFieldValue("activityDuration","");
			}else{
				this._node.setFieldValue("activityDuration",duration.toString());
			}
		}
	}
	
	
	
	D3.inherits(ActivityDurationCustomField,D3.TextInputField);
	
	function ActivityDurationCustomField(){
		D3.TextInputField.call(this);
	}
	
	ActivityDurationCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			this._fieldEditor = ActivityDurationCustomField.uber.getFieldEditor.call(this);
			this._fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			this._fieldEditor = ActivityDurationCustomField.uber.getFieldEditor.call(this);
		}			
		return this._fieldEditor;
	}
	ActivityDurationCustomField.prototype.onKeyUp = function(event){
		var duration = parseFloat(this.getFieldEditorValue());
		var timeZoneOffset = new Date().getTimezoneOffset();
		this.fieldContainer.clearFieldUserMsgContainer();
		if (duration >-1){
			var startTime = parseInt(this._node.getFieldValue("startDatetime"));
			startTime = new Date(startTime +(timeZoneOffset*60000));
			var endTime = ((startTime.getHours()*60*60*1000) + (startTime.getMinutes()*60*1000)) + (duration * 3600000);
			if (endTime == 86400000)
				endTime = 86399999;
			
			if (endTime>86400000)
				this.fieldContainer.addFieldUserMessage(D3.LanguageManager.getLabel("Activity.activityDuration.warning"), "error", true);
			
			this._node.setFieldValue("endDatetime",endTime);
		}
		
		
		
		
	}
	
	
	D3.inherits(ActivityLinkField,D3.HyperLinkField);
	
		
	function ActivityLinkField(config){
		this._linkConfig = config;
		D3.HyperLinkField.call(this);
	}
	
	ActivityLinkField.prototype.getFieldRenderer = function(){
		if (!this._fieldRenderer){
			if (this._linkConfig.rendererCSS=="activityLink"){
				var dayDate = this.getFieldValue(); 
				var d = new Date(parseInt(dayDate)); 
				var ddate = $.datepick.formatDate("dd MM yyyy", d);
				this._linkConfig.rendererLabel = ddate;
			}
			var fieldRenderer = ActivityLinkField.uber.getFieldRenderer.call(this);
			var label = this._linkConfig.rendererLabel;
			if (this._config.label) label = this._config.label;
			fieldRenderer.appendChild(document.createTextNode(label));
			fieldRenderer.removeAttribute("target");
			fieldRenderer.removeAttribute("href");
			$(fieldRenderer).addClass(this._linkConfig.rendererCSS);
			
			//fieldRenderer.style="color: red;";
			fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
			this._fieldRenderer = fieldRenderer;
		}else{
			this._fieldRenderer = ActivityLinkField.uber.getFieldRenderer.call(this);
		}			
		return this._fieldRenderer;
	}
	ActivityLinkField.prototype.getFieldEditor = function(){
		if (this._fieldEditor == null && !this._linkConfig.noEditor) {
			this._fieldEditor = $("<span style='color:red' class='"+this._linkConfig.editorCSS+"'>"+this._linkConfig.editorLabel+"</span>").get(0);
			ActivityLinkField.uber.setTextAlign.call(this,this._fieldEditor);
		}
		return this._fieldEditor;
	}
	ActivityLinkField.prototype.onclick = function(){
		this._node.commandBeanProxy.gotoUrl(this._linkConfig.url,this._linkConfig.redirectMsg?this._linkConfig.redirectMsg:"Redirecting to ");
	}
	
	
	ActivityLinkField.prototype.refreshFieldRenderer = function() {
		
	};
	
	
	D3.inherits(ActivityRemarksInline,D3.AbstractFieldComponent);
	function ActivityRemarksInline() {
		D3.AbstractFieldComponent.call(this);
		this.numberFormatter = new D3.NumberBase();
	}
	
	ActivityRemarksInline.prototype.destroyFieldEditor = function(event) {
		this._fieldEditor = null;
	}
	
	ActivityRemarksInline.prototype.getFieldRenderer = function() {
		this._fieldRenderer = this.showFTRText();
		return this._fieldRenderer;
	}

	ActivityRemarksInline.prototype.showFTRText = function() {
		if (this._node.getFieldValue("additionalRemarks")) {
			var remark = this._node.getFieldValue("additionalRemarks");
			remark = remark.replace(/;/g, "\n")
			 
			var tableConfig = "<table style='white-space:pre-wrap;'><tr><td style='vertical-align:top;'>Fixed Text Remark:</td><td>" + remark + "</td></tr></table>";
			
			var renderer = $(tableConfig);
			this._renderer = renderer.get(0);
			return this._renderer;
		}
		return null;
	}

	ActivityRemarksInline.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) { 
			this._fieldEditor = this.createFTRButtonPanel();
			if (this._fieldEditor) {
				this._fieldEditor.setAttribute("style", "white-space:pre-wrap;");
			}
			
			this.triggerFTREditMode();
			
			var autoTrigger = false; 
			if (this._CustomInputList) {
				for (var i = 0; i < this._CustomInputList.length; i++) {
					var customCodeObj = this._CustomInputList[i];
					if (customCodeObj.originalValue) {
						autoTrigger = true;
					}
				}
				if (autoTrigger) {
					if (this._btnRetrieve) {
						this._btnRetrieve.click();
					}
				}
			}
		}
		return this._fieldEditor;
	}
	
	ActivityRemarksInline.prototype.guidGenerator = function() {
	    var S4 = function() {
	        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	     };
	     return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}

	ActivityRemarksInline.prototype.triggerFTREditMode = function() {
		if(this.allowFieldShow()) {
			if (this._container) {
				$(this._container).click();
			}
		}
	}
	
	ActivityRemarksInline.prototype.createFTRButtonPanel = function() {
		var fieldEditor = null;
		
		var labelFTR = D3.LanguageManager.getLabel("button.FTR");
		var labelClear = D3.LanguageManager.getLabel("button.clear");
		
		var divConfig = "<div class='inlineFTRTable'>" +
			"<table style='white-space:pre-wrap;'><tr><td style='vertical-align:top;'>Fixed Text Remark:</td><td>" + this._node.getFieldValue("additionalRemarks") + "</td></tr>" +		
				"<tr>" +
					"<td colspan='2' style='padding-left:3px;'><button class='DefaultButton buttonClear'>"+labelClear+"</button></td>" +
				"</tr>" +
			"</table>" + "</div>";
		
		if (this.allowFieldShow()) {
			var container = $(divConfig);
			this._container = container.get(0);
			var that = this;
			
			container.click(function() {
				that.populateRemarksEventHandler();	
			});
			
			this._btnClear = container.find(".buttonClear");
			this._btnClear.click(function() {
				that.clearEventHandler();
			});
			
			var outerDiv;
			if (this._divId) {
				var findId = '#' + this._divId;
				$(findId.toString()).empty();
				outerDiv = $(findId.toString()).get(0);
				if (!outerDiv) {		
					outerDiv = document.createElement("div");
					outerDiv.setAttribute("id", this._divId);
				}
			} else {
				outerDiv = this.initiateCreateFieldEditor();
			}
			outerDiv.appendChild(this._container);	
			fieldEditor = outerDiv;
		} else if(this._node.getFieldValue("additionalRemarks")!=null && this._node.getFieldValue("additionalRemarks")!=""){
			if(!this.allowFieldShow())
			{
				var container = $(divConfig);
				this._container = container.get(0);
				var that = this;
				container.click(function() {
					D3.UIManager.popup("Info", "This fixed text remark combination no longer exists for editing.");
				});
				this._btnClear = container.find(".buttonClear");
				this._btnClear.click(function() {
					that.clearEventHandler();
				});
				
				var outerDiv;
				if (this._divId) {
					var findId = '#' + this._divId;
					$(findId.toString()).empty();
					outerDiv = $(findId.toString()).get(0);
					if (!outerDiv) {		
						outerDiv = document.createElement("div");
						outerDiv.setAttribute("id", this._divId);
					}
				} else {
					outerDiv = this.initiateCreateFieldEditor();
				}
				outerDiv.appendChild(this._container);	
				fieldEditor = outerDiv;
			}
		}
		return fieldEditor;
	}
	
	ActivityRemarksInline.prototype.dispose = function() {
		this._FreeTextRemarksList = null;
		this._lookupModuleParams = null;
		this._lookupModuleConstant = null;
		this._lookupParamsUOM = null;
		this._lookupList = null;
		this._userInputComponentList = [];
		this._CustomInputList = null;
		this._customInputLookupList = null;
		this._customInputComponentList = null;
		this._customInputParams = null;
		this._customCodeParamKey = null;
		this._customCodeParamValue = null;
		this._customCodeParamUid = null;
		if (this._node.commandBeanProxy) {
			this._node.commandBeanProxy.removeEventListener(D3.CommandBeanProxyEvent.REDRAW_ROOT_BUTTONS);	
		}
		
		ActivityRemarksInline.uber.dispose.call(this);
	}
	
	ActivityRemarksInline.prototype.populateRemarksEventHandler = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksInline.prototype.clearEventHandler = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
		this._node.setFieldValue("@activityFreeTextRemark", "");
		
		var editor = this.cleanAndGetFieldEditor();
		editor = this.createFTRButtonPanel();
		
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnReset_onClick = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
		this._node.setFieldValue("@activityFreeTextRemark", "");
		
		this.clearInputFields();
	}
	
	ActivityRemarksInline.prototype.loadXMLCustomParameters = function(response) {
		//collect custom parameters
		this.loadWitsmlSettings(response);
		this.populateCustomInputList(response);
		if (this._customInputSize == 0) {
			this.populateRemarkList(response);
			this.generateInlineEditor();
		} else {
			this.generateCustomInlineEditor();
		}
		
	}
	
	ActivityRemarksInline.prototype.loadXMLFreeTextRemarks = function(response) {
		//collect free text remarks
		this.populateCustomInputParameterFromUser(response);
		this.populateRemarkList(response);
		this.generateInlineEditor();
	}
	
	ActivityRemarksInline.prototype.loadWitsmlSettings = function(response) {
		var elmIsWitsmlFtr = "";
		var doc = $(response);
		doc.find("witsmlSettings").each(function(index, element){
			var elm = $(this);
			elmIsWitsmlFtr = elm.find("isWitsmlFtr").first().text();
		});
		this._isWitsmlFtr = (elmIsWitsmlFtr == "true" ? true : false);
	}
	
	ActivityRemarksInline.prototype.populateRemarkList = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("freeTextRemarks").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("activityDescriptionMatrixUid").first().text();
			newItem.label = elm.find("remarks").first().text();
			
			collection.push(newItem);
		});

		this._FreeTextRemarksList = collection;
		if (this._FreeTextRemarksList.length == 1) {
			this.populateParamList(response);
		}
	}
	
	ActivityRemarksInline.prototype.populateCustomInputParameterFromUser = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("outputParameterArray").each(function(index, element){
			var elm = $(this);
			var skip = elm.find("skip").text();
			if (skip) {
				elm.find("item").each(function(index2, element2){
					var newItem = {};
					var childElm = $(this);
					newItem.index = childElm.find("index").first().text();
					newItem.label = childElm.find("label").first().text(); //parameter
					newItem.value = childElm.find("value").first().text(); //value
					newItem.customCodeUid = childElm.find("customCodeUid").first().text(); //customcodeuid
					collection.push(newItem);
				});
			}
		});
		this._customInputParams = collection;
	}
	
	ActivityRemarksInline.prototype.populateCustomInputList = function(response) {
		var collection = [];
		this._customInputLookupList = [];
		var that = this;
		
		var doc = $(response);
		var customParameter = doc.find("customParameter").get(0);
		this._customInputSize = $(customParameter).attr("size");
		if (this._customInputSize > 0) {
			doc.find("customItem").each(function(index,customItem){
				var newItem = {};
				var elm = $(this);
				newItem.sequence = elm.find("sequence").first().text();
				newItem.parameterKey = elm.find("parameterKey").first().text();
				newItem.label = elm.find("label").first().text();
				newItem.paramType = elm.find("paramType").first().text();
				newItem.dataType = elm.find("dataType").first().text();
				newItem.customCodeUid = elm.find("customCodeUid").first().text();
				newItem.precision = elm.find("precision").first().text();
				newItem.uomSymbol = elm.find("uomSymbol").first().text();
				newItem.lookupKey = elm.find("lookupKey").first().text();
				newItem.originalValue = elm.find("originalValue").first().text(); //if editing existing
				collection.push(newItem);
				
				var custItemLookup = elm.find("custItemLookup");
				if (custItemLookup != null) {
					var newLookupRef = new D3.LookupReference();
					var newLookup = new D3.LookupItem();
					newLookup.data = "";
					newLookup.label = "";
					newLookupRef.addLookupItem(newLookup);
					
					var idx = 0;
					
					elm.find("item").each(function(index2, element2){
						newLookup = new D3.LookupItem();
						var childElm = $(this);
						
						newLookup.data = childElm.find("key").first().text();
						newLookup.label = childElm.find("label").first().text();
						newLookupRef.addLookupItem(newLookup);
						idx++;
					});
					if (idx > 0) {
						var parameterKey = elm.find("parameterKey").first().text();
						that._customInputLookupList[parameterKey] = newLookupRef;
					}
				}

			});
		}
		
		this._CustomInputList = collection;
	}
	
	ActivityRemarksInline.prototype.populateParamList = function(response) {
		this._lookupModuleParams = [];
		this._lookupParamsUOM = [];
		this._lookupParamsPrecision = [];
		this._placeholder = [];
		this._prefix = [];
		this._suffix = [];
		
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		doc.find("paramDetail").each(function(index, element){
			var elm = $(this);
			idx = elm.find("sequence").first().text();
			
			var newItem = {};
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("dataType").first().text();
			newItem.originalValue = elm.find("paramValue").first().text();
			newItem.precision = elm.find("precision").first().text();
			
			that._lookupModuleParams[idx] = newItem;
			that._lookupParamsPrecision[idx] = elm.find("precision").first().text();
			that._lookupParamsUOM[idx] = elm.find("uomSymbol").first().text();
			that._placeholder[idx] = elm.find("placeholder").first().text();
			that._prefix[idx] = elm.find("prefix").first().text();
			that._suffix[idx] = elm.find("suffix").first().text();
		});
		
		this.populateConstantList(response);
		this.populateLookupList(response);
	}
	
	ActivityRemarksInline.prototype.populateConstantList = function(response) {
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		this._lookupModuleConstant = [];
		
		doc.find("constantDetail").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("paramValue").first().text();
			
			that._lookupModuleConstant[idx] = newItem;
			idx++;
		});
	}
	
	ActivityRemarksInline.prototype.populateLookupList = function(response) {
		var idx = 0;
		var lookupID = "";
		var that = this;
		this._lookupList = [];
		
		var doc = $(response);
		doc.find("lookup").each(function(index, element){
			var elm = $(this);
			lookupID = elm.attr("id");
			
			idx = 0;
			
			var lookupItem = new D3.LookupItem();
			lookupItem.data = "";
			lookupItem.label = "";
			var lookupRef = new D3.LookupReference();
			lookupRef.addLookupItem(lookupItem);
			
			elm.find("item").each(function(index2, element2){
				var childElm = $(this);
				lookupItem = new D3.LookupItem();
				lookupItem.data = childElm.find("key").first().text();
				lookupItem.label = childElm.find("label").first().text();
				lookupRef.addLookupItem(lookupItem);
				idx++;
			});
			
			if (idx > 0) {
				that._lookupList[lookupID] = lookupRef;
			}
		});
	}
	
	ActivityRemarksInline.prototype.generateInlineEditor = function() {
		var that = this;
		
		var btnReset = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.reset"),
				callbacks:{
					onclick:function(event) {
						this.btnReset_onClick(event);
					}
				},
				context:this
		}
		
		var btnWitsml = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("WITSML Import"),
				callbacks:{
					onclick:function(event) {
						this.btnWitsml_onClick(event);
					}
				},
				context:this
		}
		
		this.btnBackId = this.guidGenerator();
		var btnBack = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.back"),
				callbacks:{
					onclick:function(event) {
						this.btnBack_onClick(event);
					}
				},
				attributes:{"id": this.btnBackId},
				context:this
		}
		
		if (this._FreeTextRemarksList.length > 0) {
			var obj = this._FreeTextRemarksList[0];
			
			if (obj.label != null) {
				this._selectedRemark = obj.label;
			}
			this._panel = document.createElement("div");
			
			var dynamicObj = this.generateDynamicObject(this._selectedRemark);
			var buttons = null;
			
			if (this._customInputSize > 0) {
				var buttonContainer = document.createElement("div");
				buttonContainer.appendChild(D3.UI.createElement(btnBack));
				buttonContainer.appendChild(D3.UI.createElement(btnReset));
				if(this._isWitsmlFtr) buttonContainer.appendChild(D3.UI.createElement(btnWitsml));
				buttons = buttonContainer;
			}
			else {
				var buttonContainer = document.createElement("div");
				buttonContainer.appendChild(D3.UI.createElement(btnReset));
				if(this._isWitsmlFtr) buttonContainer.appendChild(D3.UI.createElement(btnWitsml));
				buttons = buttonContainer;
			}
			if (dynamicObj) {
				this._panel.appendChild(dynamicObj);
				var pnl = $(this._panel);
				this._inlineSecPanel = document.createElement("div");
				
				var editor = this.cleanAndGetFieldEditor();
				
				this._inlineSecPanel.appendChild(pnl.get(0));
				this._inlineSecPanel.appendChild(buttons);
				if (editor) editor.appendChild(this._inlineSecPanel);

				this._fieldEditor = editor;
			} else {
				var that = this;
				if (this.checkIfFormHasError()) {
					this.createCustomInlineEditor();
				} else {
					this._panel = document.createElement("div");
					this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
					var pnl = $(this._panel);
					var ok = {
						tag:"button",
						css:"DefaultButton",
						text:D3.LanguageManager.getLabel("button.ok"),
						callbacks:{
							onclick:function(event) {
								this.createCustomInlineEditor(event);
							}
						},
						context:this
						
					};
					var button = document.createElement("div");
					button.appendChild(D3.UI.createElement(ok));
					
					this._inlineSecPanel = document.createElement("div");
					
					var editor = this.cleanAndGetFieldEditor();
					
					this._inlineSecPanel.appendChild(pnl.get(0));
					this._inlineSecPanel.appendChild(button);
					if (editor) editor.appendChild(this._inlineSecPanel);
					
					this._fieldEditor= editor;

				}
			}
		} else {
			this._panel = document.createElement("div");
			this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
			var pnl = $(this._panel);
			var ok = {
					tag:"button",
					css:"DefaultButton",
					text:D3.LanguageManager.getLabel("button.ok"),
					callbacks:{
						onclick:function(event) {
							this.createCustomInlineEditor(event);
						}
					},
					context:this
				};
			var button = document.createElement("div");
			button.appendChild(D3.UI.createElement(ok));
			this._inlineSecPanel = document.createElement("div");
			var editor = this.cleanAndGetFieldEditor();
			this._inlineSecPanel.appendChild(pnl.get(0));
			this._inlineSecPanel.appendChild(button);
			if (editor) editor.appendChild(this._inlineSecPanel);
			
			this._fieldEditor = editor;
		}
	}
	
	ActivityRemarksInline.prototype.createCustomInlineEditor = function() {
		this.generateCustomInlineEditor();
	}
	
	ActivityRemarksInline.prototype.cleanAndGetFieldEditor = function() {
		var editor;
		if (this._divId) {
			var findId = '#' + this._divId.toString();
			$(findId.toString()).empty();
			editor = $(findId.toString()).get(0);
			if (!editor) {
				this.removeFTRPanel();
				editor = this._fieldEditor;
			}
		} else {
			this.removeFTRPanel();
			editor = this._fieldEditor;
		}
		return editor;
	}
	
	ActivityRemarksInline.prototype.removeFTRPanel = function() {
		$(this._fieldEditor).empty();
	}
	
	ActivityRemarksInline.prototype.initiateCreateFieldEditor = function() {
		this._divId = this.guidGenerator();	
		var outerDiv = document.createElement("div");
		outerDiv.setAttribute("id", this._divId);
		this._fieldEditor = outerDiv;
		return outerDiv;
	}
	
	ActivityRemarksInline.prototype.generateCustomInlineEditor = function() {
		var that = this;
		var btnRetrieve = {
				tag:"button",
				css:"DefaultButton",
				text:D3.LanguageManager.getLabel("button.retrieveFTR"),
				callbacks:{
					onclick:function(event) {
						that.btnRetrieve_onClick(event);
					}
				},
				context:this
		}
		
		var div = document.createElement("div");
		
		if (this._customInputSize > 0) {
				this._panelInitial = document.createElement("div");
				this._btnRetrieve = D3.UI.createElement(btnRetrieve);
				this._btnRetrieveId = this.guidGenerator();
				this._btnRetrieve.id = this._btnRetrieveId;
				var result = this.generateCustomInputFields();
				if (result) {
					this._panelInitial.appendChild(result);
					
					var pnl = $(this._panelInitial);
					this._inlinePanel = document.createElement("div");
					
					var editor = this.cleanAndGetFieldEditor();
					
					this._inlinePanel.appendChild(pnl.get(0));
					var buttons = document.createElement("div");
					buttons.appendChild(this._btnRetrieve);
					
					this._inlinePanel.appendChild(buttons);
					if (editor) editor.appendChild(this._inlinePanel);
					this._fieldEditor = editor;
				}
		}
		else {
			this.generateInlineEditor();
		}
		
		
	}
	
	ActivityRemarksInline.prototype.generateCustomInputFields = function() {
		var label = null;
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.setAttribute("style", "padding:1px;");
		
		table.appendChild(tr);
		tr.appendChild(td);
		var span = document.createElement("span");
		
		this._customInputComponentList = [];
		
		if (this._customInputSize > 0) {
			for (var i = 0; i < this._CustomInputList.length; i++) {
				var customCodeObj = this._CustomInputList[i];
				var value = "";
				label = document.createTextNode(customCodeObj.label.toString());
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px;");
				tr.appendChild(td);
				var embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width: auto;");
				span = document.createElement("span");
				span.setAttribute("style", "margin-left:2px; margin-right:2px;");
				span.appendChild(label);
				embedDiv.appendChild(span);
				td.appendChild(embedDiv);
				this.createCustomInputObject(customCodeObj.parameterKey, i, embedDiv);
			}
		}
		return table;
	}
	
	ActivityRemarksInline.prototype.generateDynamicObject = function(buffer) {

		this._userInputComponentList = [];
		
		if (buffer.length > 0) {
			var label = null;
			var textbox = null;
			var table = document.createElement("table");
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			var span = document.createElement("span");
			td.setAttribute("style", "padding:1px");
			
			var embedDiv = document.createElement("div");
			embedDiv.setAttribute("style", "width:auto;");
			
			var boxIndex = 0;
			if (buffer.indexOf(";") >= 0) {
				var temp = [];
				temp = buffer.split(";");
				for(var index = 0; index < temp.length; index++) {
					tr = document.createElement("tr");
					table.appendChild(tr);
					td = document.createElement("td");
					td.setAttribute("style", "padding:1px");
					tr.appendChild(td);
					
					embedDiv = document.createElement("div");
					embedDiv.setAttribute("style", "width:auto;");
						
					if ($.trim(temp[index].toString()).length > 0) {
						if (temp[index].indexOf("#") >= 0) {
							var temp1 = [];
							temp1 = temp[index].split(/(#)/g);
							
							for(var index1 = 0; index1 < temp1.length; index1++) {
								
								var toProcess = $.trim(temp1[index1].toString());
								var nonEmptyString = toProcess.length > 0;
								
								if (nonEmptyString) {
									if (toProcess == "#") {
										this.createUserInputObject(boxIndex, embedDiv);	
										if (this.triggerChangeDynaAttr == true) {
											this.setNodeDynaAttrFTR(this);
										}
										boxIndex++;
									} else {
										label = document.createTextNode(toProcess);
										span = document.createElement("span");
										span.setAttribute("style", "margin-left:2px; margin-right:2px;");
										span.appendChild(label);
										embedDiv.appendChild(span);
									}
								}
							}
						} else {
							this.setNodeDynaAttrFTR(this);
							label = document.createTextNode(temp[index].toString());
							span = document.createElement("span");
							span.setAttribute("style", "margin-left:2px; margin-right:2px;");
							span.appendChild(label);
							embedDiv.appendChild(span);
						}
					}
					td.appendChild(embedDiv);
				}
				
			} else {
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px");
				tr.appendChild(td);
				
				embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width:auto;");
				
				if ($.trim(buffer.toString()).length > 0) {
						
					if (buffer.indexOf("#") >= 0) {
						var temp1 = [];
						temp1 = buffer.split(/(#)/g);
							
						for(var index1 = 0; index1 < temp1.length; index1++) {
								
							var toProcess = $.trim(temp1[index1].toString());
							var nonEmptyString = toProcess.length > 0;
								
							if (nonEmptyString) {
								if (toProcess == "#") {
									this.createUserInputObject(boxIndex, embedDiv);	
									if (this.triggerChangeDynaAttr == true) {
										this.setNodeDynaAttrFTR(this);
									}
									boxIndex++;
								} else {
									label = document.createTextNode(toProcess);
									span = document.createElement("span");
									span.setAttribute("style", "margin-left:2px; margin-right:2px;");
									span.appendChild(label);
									embedDiv.appendChild(span);
								}
							}
						}
					} else {
						this.setNodeDynaAttrFTR(this);
						label = document.createTextNode(buffer.toString());
						span = document.createElement("span");
						span.setAttribute("style", "margin-left:2px; margin-right:2px;");
						span.appendChild(label);
						embedDiv.appendChild(span);
					}

					td.appendChild(embedDiv);
				}
			}
			
			return table;
		}
		return null;
	}
	
	ActivityRemarksInline.prototype.createCustomInputObject = function(parameterKey, i, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		var customCodeObj = this._CustomInputList[i];
		
		if (this._customInputLookupList.hasOwnProperty(parameterKey)) {
			var comboObject = document.createElement("select");
			comboObject.id = parameterKey;
			comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			
			lookupRef = this._customInputLookupList[parameterKey];
			
			if (lookupRef != null) {
				var option = document.createElement("option");

				D3.forEach(lookupRef.getActiveLookup(), function(item) {
					option = document.createElement("option");
					option.setAttribute("value", item.data);
					option.appendChild(document.createTextNode(item.label));
					comboObject.options.add(option);
				});
				
				if (customCodeObj.originalValue) {
					this.setSelectedValue(comboObject, customCodeObj.originalValue); //if edit existing	
				}
			}
			this._customInputComponentList.push(comboObject);
			embedDiv.appendChild(comboObject);
		} else {
			var textObject = document.createElement("input");
			textObject.type = "text";
			textObject.id = parameterKey;
			textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			if (customCodeObj.originalValue) {
				//if edit existing
				textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(customCodeObj.originalValue);
			}
			this._customInputComponentList.push(textObject);
			embedDiv.appendChild(textObject);
		}
		uom = customCodeObj.uomSymbol;
		if (uom.length > 0) {
			label = " " + uom;
			span = document.createElement("span");
			span.setAttribute("style", "margin-left:2px; margin-right:2px;");
			span.appendChild(document.createTextNode(label));
			embedDiv.appendChild(span);
		}
	}
	
	
	ActivityRemarksInline.prototype.checkIfFormHasError = function() {
		//brute force?
		var hasError = false;
		if (this._node) {
			if (this._node.getFieldErrors("activityDescription") != null) {
				hasError = true;
			}
		}
		return hasError;
	}
	
	ActivityRemarksInline.prototype.createUserInputObject = function(index, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		
		if (this._lookupModuleParams.length >= index) {
			var obj = this._lookupModuleParams[index];
			if (obj != null) {
				if (this._lookupList.hasOwnProperty(obj.value)) {
					var comboObject = document.createElement("select");
					comboObject.id = obj.value;
					comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					lookupRef = this._lookupList[obj.value];
					
					if (lookupRef != null) {
						var option = document.createElement("option");

						D3.forEach(lookupRef.getActiveLookup(), function(item) {
							option = document.createElement("option");
							option.setAttribute("value", item.data);
							option.appendChild(document.createTextNode(item.label));
							comboObject.options.add(option);
						});
						var that = this;
						comboObject.onchange = function(){that.setNodeDynaAttrFTR(that);};
						if (this.checkIfFormHasError()) {
							
						} else {
							if (obj.originalValue) {
								this.setSelectedValue(comboObject, obj.originalValue);
								this.triggerChangeDynaAttr = true;
							}
						}
					}
					this._userInputComponentList.push(comboObject);
					embedDiv.appendChild(comboObject);
				} else {
					var textObject = document.createElement("input");
					textObject.type = "text";
					textObject.id = obj.value;
					textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					textObject.setAttribute("placeholder", this._placeholder[index]);
					var textObjSize = 4;
					if (this._placeholder[index]) {
						textObjSize = this._placeholder[index].length;
					}
					textObject.setAttribute("size", textObjSize);
					if (this.checkIfFormHasError()) {
						
					} else {
						if (obj.originalValue) {
							//if edit existing
							textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(obj.originalValue);
							this.triggerChangeDynaAttr = true;
						}
					}
					var that = this;
					textObject.onkeyup = function() {that.setNodeDynaAttrFTR(that);};
					
					this._userInputComponentList.push(textObject);
					embedDiv.appendChild(textObject);
				}
				
				uom = this._lookupParamsUOM[index];
				if (uom.length > 0) {
					label = " " + uom;
					span = document.createElement("span");
					span.setAttribute("style", "margin-left:2px; margin-right:2px;");
					span.appendChild(document.createTextNode(label));
					embedDiv.appendChild(span);
				}
				
				return;
			}
		}
		
		label = "#";
		return;
	}
	
	ActivityRemarksInline.prototype.btnCancel_onClick = function(event){
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnCancelInitial_onClick = function(event){
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
	}
	
	ActivityRemarksInline.prototype.btnBack_onClick = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var editor = this.createFTRButtonPanel();
		this._fieldEditor = editor;
		this.loadXMLCustomParameters(response);
	}

	ActivityRemarksInline.prototype.checkInputEntered = function(that) {
		var hasEntered = false;
		
		for (var i=0; i < that._userInputComponentList.length; i++) {
			var uiObj = that._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			if (value) {
				hasEntered = true;
			}
		}
		return hasEntered;
	}
	
	ActivityRemarksInline.prototype.clearInputFields = function() {
		for (var i=0; i < this._userInputComponentList.length; i++) {
			var uiObj = this._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				uiObj.selectedIndex = 0;
				this._lookupModuleParams[i].originalValue = 0;
			} else {
				uiObj.value = "";
				this._lookupModuleParams[i].originalValue = "";
			}
		}
	}
	
	ActivityRemarksInline.prototype.setNodeDynaAttrFTR = function(that) {
		var outputList = "";
		var result = "";
		var errList = "";
		
		var hasEnteredSomething = that.checkInputEntered(that);

		if (hasEnteredSomething) {
			for (var i=0; i < that._userInputComponentList.length; i++) {
				var uiObj = that._userInputComponentList[i];
				var value = "";
				if (uiObj.type == "select-one") {
					value = uiObj.options[uiObj.selectedIndex].value;
				} else {
					value = uiObj.value;
				}
				
				result = that.validate(i, value);
				if (result.length > 0) {
					if (errList.length > 0) {
						errList += "\n";
					} 
					errList += result;
				}
				else {
					var obj = that._lookupModuleParams[i];
					if (obj != null) {
						if (outputList.length > 0) {
							outputList += "::";
						}
						var dataType = obj.label;
						var precision = that._lookupParamsPrecision[i];
						if (dataType == "double" && value.length != 0) {
							// Convert to raw number
							value = that.numberFormatter.parseNumberString(value.toString(), that._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, that._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
							
							// Process
							value = that.numberFormatter.formatRoundingWithPrecision(value, D3.NumberBase.NEAREST, precision);
							value = that.numberFormatter.formatThousands(value);
							value = that.numberFormatter.formatDecimal(value);
							value = that.numberFormatter.formatPrecision(value, precision);
							
							// Convert back to locale specific format
							value = that.numberFormatter.localizeNumberString(value.toString(), that._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, that._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
						}
						outputList = outputList + obj.value + ":=" + value;
					}
				}
			}
		}
		
		that.getCustomCodeInputEntered();
		outputList = that.appendConstantValue(outputList);

		if (errList.length == 0) {
			that.setFieldValue(outputList); //important to set @activityFreeTextRemark

			if (hasEnteredSomething) {
				var remark = that.populateFreeTextRemark();
				that._node.setFieldValue("additionalRemarks", remark.replace(/;/g, "\n"));
			} else {
				if ((this._selectedRemark.length > 0) && (this._selectedRemark.indexOf("#") <= 0)) {
					that._node.setFieldValue("additionalRemarks", this._selectedRemark.replace(/;/g, "\n"));
				}else{
					that._node.setFieldValue("additionalRemarks", "");
				}
			}
			that._node.setFieldValue("@customCodeParamKey", that._customCodeParamKey);
			that._node.setFieldValue("@customCodeParamValue", that._customCodeParamValue);
			that._node.setFieldValue("@customCodeParamUid", that._customCodeParamUid);
			
		} else {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		
	}
	
	ActivityRemarksInline.prototype.getCustomCodeInputEntered = function() {
		this._customCodeParamKey = "";
		this._customCodeParamValue = "";
		this._customCodeParamUid = "";
		
		if (this._customInputParams) {
			for (var j=0; j < this._customInputParams.length; j++) {
				var obj2 = this._customInputParams[j];
				var value2 = obj2.value;
				
				if (value2 != null && value2.length == 0) {
					value2 = "null";
				}
				if (this._customCodeParamKey.length > 0) {
					this._customCodeParamKey += ":.:.:";
				}
				if (this._customCodeParamValue.length > 0) {
					this._customCodeParamValue += ":.:.:";
				}
				if (this._customCodeParamUid.length > 0) {
					this._customCodeParamUid += ":.:.:";
				}
				this._customCodeParamKey += obj2.label;
				this._customCodeParamValue += value2;
				this._customCodeParamUid += obj2.customCodeUid;
			}
		}
	} 
	
	ActivityRemarksInline.prototype.btnRetrieve_onClick = function(event) {
		this.customCodeParams = this.getParamsFTR();
		
		var result = "";
		var errList = "";
		var parameterKeyArray = [];
		var valueArray = [];
		var customCodeUidArray = [];
		for (var i=0; i < this._customInputComponentList.length; i++) {
			var uiObj = this._customInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			result = this.validateCustomInput(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				}
				errList += result;
			}
			else {
				var obj = this._CustomInputList[i];
				if (obj != null) {
					parameterKeyArray[i] = obj.parameterKey;
					valueArray[i] = value;
					customCodeUidArray[i] = obj.customCodeUid;
				}
			}
		}
		
		if (errList.length != 0) {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		this.customCodeParams.outputParameterArray = parameterKeyArray;
		this.customCodeParams.outputValueArray = valueArray;
		this.customCodeParams.outputCustomCodeUidArray = customCodeUidArray;
		
		var response = this._node.commandBeanProxy.customInvokeSynchronous(this.customCodeParams);
		this.loadXMLFreeTextRemarks(response);

	}
	
	ActivityRemarksInline.prototype.validate = function(index, value) {
		var obj = this._lookupModuleParams[index];
		
		if (obj != null) {
			var dataType = obj.label;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.validateCustomInput = function(index, value) {
		var obj = this._CustomInputList[index];
		if (obj != null) {
			var dataType = obj.dataType;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.setSelectedValue = function(comboBox, selectedValue) {
		for (var i=0; i < comboBox.length; i++) {
			if (comboBox.options[i].value == selectedValue) {
				comboBox.selectedIndex = i;
				break;
			}
		}
	}
	
	ActivityRemarksInline.prototype.appendConstantValue = function(bufferList) {
		if (this._lookupModuleConstant != null) {
			var obj;
			
			for (var index = 0;index < this._lookupModuleConstant.length; index++) {
				obj = this._lookupModuleConstant[index];
				if (obj != null) {
					if (bufferList.length > 0) {
						bufferList += "::";
					} 
					bufferList += obj.value + ":=" + obj.label; 
				}
			}
		}
		return bufferList;
	}
	
	ActivityRemarksInline.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "collectCustomParameters";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksInline.prototype.getParamsFTR = function() {
		var params = {};
		params._invokeCustomFilter = "collectFreeTextRemarks";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksInline.prototype.populateFreeTextRemark = function(){
		var buffer = this._selectedRemark;
		if ((buffer.length>0) && (buffer.indexOf("#") >= 0)) {
			var uomSymbol = "";
			var strOutput = "";
			var componentIndex = 0;
			var temp = buffer.split("#");
			var uiObj = null;
			
			var lastIndex = temp.length - 1;
			for (var i=0; i<=lastIndex; i++) {
				if (i != lastIndex) {
					if (this._userInputComponentList.length > componentIndex) {
						uiObj = this._userInputComponentList[componentIndex];
						if (uiObj == null) {
							strOutput += temp[i].toString() + "#";
						} else {
							var value = "";
							if (uiObj.type == "select-one") {
								value = uiObj.options[uiObj.selectedIndex].label;
							} else {
								value = uiObj.value;
							}
							if (value) {
								strOutput += temp[i].toString();
								var obj = this._lookupModuleParams[componentIndex];
								var dataType = obj.label;
								var precision = this._lookupParamsPrecision[componentIndex];
								if (dataType == "double" && value.length != 0 && uiObj.type != "select-one") {
									var strValue = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strValue = this.numberFormatter.formatRoundingWithPrecision(strValue, D3.NumberBase.NEAREST, precision);
									strValue = this.numberFormatter.formatThousands(strValue);
									strValue = this.numberFormatter.formatDecimal(strValue);
									strValue = this.numberFormatter.formatPrecision(strValue, precision);
									strValue = this.numberFormatter.localizeNumberString(strValue.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strOutput += strValue;
								} else {
									strOutput += value;
								}
								if (this._lookupParamsUOM.length > componentIndex) {
									uomSymbol = this._lookupParamsUOM[componentIndex];
									if (uomSymbol !=""){
										strOutput += " " + uomSymbol;
									}
								}
							} else {
								//remove prefix and suffix of parameter if empty
								var processedWord = "";
								if (this._prefix) {
									processedWord = temp[i].toString().replace(this._prefix[componentIndex], "");
									strOutput += processedWord;
								}
								if (this._suffix) {
									processedWord = temp[i+1].toString().replace(this._suffix[componentIndex], "");
									temp[i+1] = processedWord;
								}
							}
						}
						componentIndex++;
					} else {
						strOutput += "#";
					}
				} else {
					strOutput += temp[i].toString();
				}
			}
			buffer = strOutput;
		}
		return buffer;
	}
	
	ActivityRemarksInline.prototype.allowFieldShow = function(){
		var showButton = this._node.getFieldValue("@showFTRButton") == "1" ? true : false;
		return showButton;
	}
	
	ActivityRemarksInline.prototype.refreshFieldEditor = function() {
	};
	
	ActivityRemarksInline.prototype.refreshFieldRenderer = function() {
		
	};
	
	D3.inherits(ActivityRemarksPopulation,D3.AbstractFieldComponent);
	
	function ActivityRemarksPopulation(){
		D3.AbstractFieldComponent.call(this);
		this.numberFormatter = new D3.NumberBase();
		
	}
	
	ActivityRemarksPopulation.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		
		var labelFTR = D3.LanguageManager.getLabel("button.FTR");
		var labelClear = D3.LanguageManager.getLabel("button.clear");
		var divConfig = "<div>" +
				"<table>" +
					"<tr>" +
						"<td style='padding-left:40px;'><button class='DefaultButton buttonFTR'>"+labelFTR+"</button></td>" +
						"<td style='padding-left:3px;'><button class='DefaultButton buttonClear'>"+labelClear+"</button></td>" +
					"</tr>" +
				"</table>" + "</div>";
		
		if (!this._fieldEditor && this.allowFieldShow()) {
			var container = $(divConfig);
			this._container = container.get(0);
			var that = this;
			this._btnFTR = container.find(".buttonFTR");
			this._btnFTR.click(function() {
				that.populateRemarksEventHandler();
			});
			
			this._btnClear = container.find(".buttonClear");
			this._btnClear.click(function() {
				that.clearEventHandler();
			});
			
			fieldEditor = this._container;
			this._fieldEditor = fieldEditor;
			
		} else if(this._node.getFieldValue("additionalRemarks")!=null && this._node.getFieldValue("additionalRemarks")!=""){
			//if(!this._fieldEditor == null && !this.allowFieldShow())
			if(!this._fieldEditor && !this.allowFieldShow())
			{
				var container = $(divConfig);
				this._container = container.get(0);
				var that = this;
				this._btnFTR = container.find(".buttonFTR");
				this._btnFTR.addClass("hide");
				
				this._btnClear = container.find(".buttonClear");
				this._btnClear.click(function() {
					that.clearEventHandler();
				});
				
				fieldEditor = this._container;
				//this._editor.addChild(this._editorClear);
				this._fieldEditor = fieldEditor;
			}
		}else {
			ActivityRemarksPopulation.uber.getFieldEditor.call(this);
		}
		return this._fieldEditor;
	}
	
	ActivityRemarksPopulation.prototype.dispose = function() {
		ActivityRemarksPopulation.uber.dispose.call(this);
		
		this._FreeTextRemarksList = null;
		this._lookupModuleParams = null;
		this._lookupModuleConstant = null;
		this._lookupParamsUOM = null;
		this._lookupList = null;
		this._userInputComponentList = null;
		this._CustomInputList = null;
		this._customInputLookupList = null;
		this._customInputComponentList = null;
		this._customInputParams = null;
		this._customCodeParamKey = null;
		this._customCodeParamValue = null;
		this._customCodeParamUid = null;
	}
	
	ActivityRemarksPopulation.prototype.populateRemarksEventHandler = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksPopulation.prototype.clearEventHandler = function(event) {
		this._node.setFieldValue("additionalRemarks", "");
	}
	
	ActivityRemarksPopulation.prototype.loadXMLCustomParameters = function(response) {
		//collect custom parameters
		this.populateCustomInputList(response);
		if (this._customInputSize == 0) {
			this.populateRemarkList(response);
			this.generatePopUpWindow();
		} else {
			this.generateCustomInputPopUpWindow();
		}
	}
	
	ActivityRemarksPopulation.prototype.loadXMLFreeTextRemarks = function(response) {
		//collect free text remarks
		this.populateCustomInputParameterFromUser(response);
		this.populateRemarkList(response);
		this.generatePopUpWindow();
	}
	
	ActivityRemarksPopulation.prototype.populateRemarkList = function(response) {
		var collection = [];
		
		var doc = $(response);
		doc.find("freeTextRemarks").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("activityDescriptionMatrixUid").first().text();
			newItem.label = elm.find("remarks").first().text();
			
			collection.push(newItem);
		});
		this._FreeTextRemarksList = collection;
		if (this._FreeTextRemarksList.length == 1) {
			this.populateParamList(response);
		}
	}
	
	ActivityRemarksPopulation.prototype.populateCustomInputParameterFromUser = function(response) {
		var collection = [];
		var doc = $(response);
		doc.find("outputParameterArray").each(function(index, element){
			var elm = $(this);
			var skip = elm.find("skip").text();
			if (skip) {
				elm.find("item").each(function(index2, element2){
					var newItem = {};
					var childElm = $(this);
					newItem.index = childElm.find("index").first().text();
					newItem.label = childElm.find("label").first().text(); //parameter
					newItem.value = childElm.find("value").first().text(); //value
					newItem.customCodeUid = childElm.find("customCodeUid").first().text(); //customcodeuid
					collection.push(newItem);
				});
			}
		});
		this._customInputParams = collection;
	}
	
	ActivityRemarksPopulation.prototype.populateCustomInputList = function(response) {
		var collection = [];
		this._customInputLookupList = [];
		var that = this;
		
		var doc = $(response);
		var customParameter = doc.find("customParameter").get(0);
		this._customInputSize = $(customParameter).attr("size");
		

			if (this._customInputSize > 0) {
				doc.find("customItem").each(function(index,customItem){
					var newItem = {};
					var elm = $(this);
					newItem.sequence = elm.find("sequence").first().text();
					newItem.parameterKey = elm.find("parameterKey").first().text();
					newItem.label = elm.find("label").first().text();
					newItem.paramType = elm.find("paramType").first().text();
					newItem.dataType = elm.find("dataType").first().text();
					newItem.customCodeUid = elm.find("customCodeUid").first().text();
					newItem.precision = elm.find("precision").first().text();
					newItem.uomSymbol = elm.find("uomSymbol").first().text();
					newItem.lookupKey = elm.find("lookupKey").first().text();
					newItem.originalValue = elm.find("originalValue").first().text(); //if editing existing
					collection.push(newItem);
					
					var custItemLookup = elm.find("custItemLookup");
					if (custItemLookup != null) {
						var newLookupRef = new D3.LookupReference();
						var newLookup = new D3.LookupItem();
						newLookup.data = "";
						newLookup.label = "";
						newLookupRef.addLookupItem(newLookup);
						
						var idx = 0;
						
						elm.find("item").each(function(index2, element2){
							newLookup = new D3.LookupItem();
							var childElm = $(this);
							
							newLookup.data = childElm.find("key").first().text();
							newLookup.label = childElm.find("label").first().text();
							newLookupRef.addLookupItem(newLookup);
							idx++;
						});
						if (idx > 0) {
							var parameterKey = elm.find("parameterKey").first().text();
							//var obj = {};
							//obj[parameterKey] = newLookupRef;
							//that._customInputLookupList.push(obj);
							that._customInputLookupList[parameterKey] = newLookupRef;
						}
					}

				});
			}
			
			this._CustomInputList = collection;
	}
	
	ActivityRemarksPopulation.prototype.populateParamList = function(response) {
		this._lookupModuleParams = [];
		this._lookupParamsUOM = [];
		this._lookupParamsPrecision = [];
		this._placeholder = [];
		this._prefix = [];
		this._suffix = [];
		
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		doc.find("paramDetail").each(function(index, element){
			var elm = $(this);
			idx = elm.find("sequence").first().text();
			
			var newItem = {};
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("dataType").first().text();
			newItem.originalValue = elm.find("paramValue").first().text();
			newItem.precision = elm.find("precision").first().text();
			
			that._lookupModuleParams[idx] = newItem;
			that._lookupParamsPrecision[idx] = elm.find("precision").first().text();
			that._lookupParamsUOM[idx] = elm.find("uomSymbol").first().text();
			that._placeholder[idx] = elm.find("placeholder").first().text();
			that._prefix[idx] = elm.find("prefix").first().text();
			that._suffix[idx] = elm.find("suffix").first().text();
		});
		
		this.populateConstantList(response);
		this.populateLookupList(response);
	}
	
	ActivityRemarksPopulation.prototype.populateConstantList = function(response) {
		var idx = 0;
		var that = this;
		
		var doc = $(response);
		that._lookupModuleConstant = [];
		doc.find("constantDetail").each(function(index, element){
			var newItem = {};
			var elm = $(this);
			newItem.value = elm.find("parameterDetailUid").first().text();
			newItem.label = elm.find("paramValue").first().text();
			
			that._lookupModuleConstant[idx] = newItem;
			idx++;
		});
	}
	
	ActivityRemarksPopulation.prototype.populateLookupList = function(response) {
		var idx = 0;
		var lookupID = "";
		var that = this;
		this._lookupList = [];
		
		var doc = $(response);
		doc.find("lookup").each(function(index, element){
			var elm = $(this);
			lookupID = elm.attr("id");
			
			idx = 0;
			
			var lookupItem = new D3.LookupItem();
			lookupItem.data = "";
			lookupItem.label = "";
			var lookupRef = new D3.LookupReference();
			lookupRef.addLookupItem(lookupItem);
			
			elm.find("item").each(function(index2, element2){
				var childElm = $(this);
				lookupItem = new D3.LookupItem();
				lookupItem.data = childElm.find("key").first().text();
				lookupItem.label = childElm.find("label").first().text();
				lookupRef.addLookupItem(lookupItem);
				idx++;
			});
			
			if (idx > 0) {
				that._lookupList[lookupID] = lookupRef;
			}
		});
	}
	
	ActivityRemarksPopulation.prototype.generatePopUpWindow = function() {
		var that = this;
		var btnOK = {
				text:D3.LanguageManager.getLabel("button.ok"),
				click:function(event){
					that.btnOK_onClick(event)
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				click:function(event) {
					that.btnCancel_onClick(event)
				}
		};
		var btnBack = {
				text:D3.LanguageManager.getLabel("button.back"),
				click:function(event) {
					that.btnBack_onClick(event)
				}
		}
		
		if (this._FreeTextRemarksList.length > 0) {
			var obj = this._FreeTextRemarksList[0];
			if (obj.label != null) {
				this._selectedRemark = obj.label;
			}
			if ((this._selectedRemark.length > 0) && (this._selectedRemark.indexOf("#") < 0)) {
				this.setFieldValue("");
				this._node.commandBeanProxy.addAdditionalFormRequestParams("auto_populate_description", this._selectedRemark);
				this.getCustomCodeInputEntered();
				this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_key", this._customCodeParamKey);
				this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_value", this._customCodeParamValue);
				this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);
			} else {
				
				this._panel = document.createElement("div");
				
				var dynamicObj = this.generateDynamicObject(this._selectedRemark);
				var buttons = null;
				
				if (this._customInputSize > 0) {
					buttons = [btnOK, btnBack, btnCancel];
				}
				else {
					buttons = [btnOK, btnCancel];
				}
				if (dynamicObj) {
					this._panel.appendChild(dynamicObj);
					var pnl = $(this._panel);
					
					pnl.dialog({
								resizable: true,
						        modal: true,
						        buttons: buttons,
						        dialogClass:"ftrPopup",
						        open: function(event, ui) { 
						            //hide close button.
						        	//$(".ui-dialog-titlebar-close", ui.dialog).hide();
						        	$(".ui-dialog-titlebar", ui.dialog).hide();
						        },
							});
				} else {
					var that = this;
					this._panel = document.createElement("div");
					this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
					var pnl = $(this._panel);
					var ok = {
						text:D3.LanguageManager.getLabel("button.ok"),
						click:function(){
							that.createCustomInputPopup();
							pnl.dialog('close');
						}
					};
					var button = [ok];
					pnl.dialog({
						modal:true,
						dialogClass:"ftrPopup",
						buttons: button
					});
				}
			}
		} else {
			this._panel = document.createElement("div");
			this._panel.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
			var pnl = $(this._panel);
			pnl.dialog({modal:true,dialogClass:"ftrPopup"});
		}
	}
	
	ActivityRemarksPopulation.prototype.createCustomInputPopup = function() {
		this.generateCustomInputPopUpWindow();
	}
	
	ActivityRemarksPopulation.prototype.generateCustomInputPopUpWindow = function() {
		var that = this;
		var btnRetrieve = {
				text:D3.LanguageManager.getLabel("button.retrieveFTR"),
				click:function(event){
					that.btnRetrieve_onClick(event)
				}
		}
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				click:function(event){
					that.btnCancelInitial_onClick(event)
				}
		};
		
		var div = document.createElement("div");
		
		if (this._customInputSize > 0) {
				this._panelInitial = document.createElement("div");
				
				var result = this.generateCustomInputFields();
				if (result) {
					this._panelInitial.appendChild(result);
					
					var pnl = $(this._panelInitial);
					pnl.dialog({
				        resizable: true,
				        modal: true,
				        buttons: [btnRetrieve,btnCancel],
				        dialogClass:"ftrPopup",
				        open: function(event, ui) { 
				            //hide close button.
				        	//$(".ui-dialog-titlebar-close", ui.dialog).hide();
							$(".ui-dialog-titlebar", ui.dialog).hide();
				        },
				    });
				}
		}
		else {
			this.generatePopUpWindow();
		}
		
		
	}
	
	ActivityRemarksPopulation.prototype.generateCustomInputFields = function() {
		var label = null;
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.setAttribute("style", "padding:1px;");
		
		table.appendChild(tr);
		tr.appendChild(td);
		var span = document.createElement("span");
		
		this._customInputComponentList = [];
		
		if (this._customInputSize > 0) {
			for (var i = 0; i < this._CustomInputList.length; i++) {
				var customCodeObj = this._CustomInputList[i];
				var value = "";
				label = document.createTextNode(customCodeObj.label.toString());
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px;");
				tr.appendChild(td);
				var embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width: auto;");
				span = document.createElement("span");
				span.setAttribute("style", "margin-left:2px; margin-right:2px;");
				span.appendChild(label);
				embedDiv.appendChild(span);
				td.appendChild(embedDiv);
				this.createCustomInputObject(customCodeObj.parameterKey, i, embedDiv);
			}
		}
		return table;
	}
	
	ActivityRemarksPopulation.prototype.generateDynamicObject = function(buffer) {

		this._userInputComponentList = [];
		
		if (buffer.length > 0) {
			var label = null;
			var textbox = null;
			var table = document.createElement("table");
			var tr = document.createElement("tr");
			var td = document.createElement("td");
			var span = document.createElement("span");
			td.setAttribute("style", "padding:1px");
			
			var embedDiv = document.createElement("div");
			embedDiv.setAttribute("style", "width:auto;");
			
			var boxIndex = 0;
			if (buffer.indexOf(";") >= 0) {
				var temp = [];
				temp = buffer.split(";");
				for(var index = 0; index < temp.length; index++) {
					tr = document.createElement("tr");
					table.appendChild(tr);
					td = document.createElement("td");
					td.setAttribute("style", "padding:1px");
					tr.appendChild(td);
					
					embedDiv = document.createElement("div");
					embedDiv.setAttribute("style", "width:auto;");
						
					if ($.trim(temp[index].toString()).length > 0) {
						if (temp[index].indexOf("#") >= 0) {
							var temp1 = [];
							temp1 = temp[index].split(/(#)/g);
							
							for(var index1 = 0; index1 < temp1.length; index1++) {
								
								var toProcess = $.trim(temp1[index1].toString());
								var nonEmptyString = toProcess.length > 0;
								
								if (nonEmptyString) {
									if (toProcess == "#") {
										this.createUserInputObject(boxIndex, embedDiv);	 
										boxIndex++;
									} else {
										label = document.createTextNode(toProcess);
										span = document.createElement("span");
										span.setAttribute("style", "margin-left:2px; margin-right:2px;");
										span.appendChild(label);
										embedDiv.appendChild(span);
									}
								}
							}
						} else {
							label = document.createTextNode(temp[index].toString());
							span = document.createElement("span");
							span.setAttribute("style", "margin-left:2px; margin-right:2px;");
							span.appendChild(label);
							embedDiv.appendChild(span);
						}
					}
					td.appendChild(embedDiv);
				}
				
			} else {
				tr = document.createElement("tr");
				table.appendChild(tr);
				td = document.createElement("td");
				td.setAttribute("style", "padding:1px");
				tr.appendChild(td);
				
				embedDiv = document.createElement("div");
				embedDiv.setAttribute("style", "width:auto;");
				
				if ($.trim(buffer.toString()).length > 0) {
					//label = document.createTextNode(buffer.toString());
					//embedDiv.appendChild(label);
					//td.appendChild(embedDiv);
						
					if (buffer.indexOf("#") >= 0) {
						var temp1 = [];
						temp1 = buffer.split(/(#)/g);
							
						for(var index1 = 0; index1 < temp1.length; index1++) {
								
							var toProcess = $.trim(temp1[index1].toString());
							var nonEmptyString = toProcess.length > 0;
								
							if (nonEmptyString) {
								if (toProcess == "#") {
									this.createUserInputObject(boxIndex, embedDiv);	 
									boxIndex++;
								} else {
									label = document.createTextNode(toProcess);
									span = document.createElement("span");
									span.setAttribute("style", "margin-left:2px; margin-right:2px;");
									span.appendChild(label);
									embedDiv.appendChild(span);
								}
							}
						}
					} else {
						label = document.createTextNode(buffer.toString());
						span = document.createElement("span");
						span.setAttribute("style", "margin-left:2px; margin-right:2px;");
						span.appendChild(label);
						embedDiv.appendChild(span);
					}

					td.appendChild(embedDiv);
				}
			}
			return table;
		}
		return null;
	}
	
	ActivityRemarksPopulation.prototype.createCustomInputObject = function(parameterKey, i, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		var customCodeObj = this._CustomInputList[i];
		
		if (this._customInputLookupList.hasOwnProperty(parameterKey)) {
			var comboObject = document.createElement("select");
			comboObject.id = parameterKey;
			comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			
			lookupRef = this._customInputLookupList[parameterKey];
			
			if (lookupRef != null) {
				var option = document.createElement("option");

				D3.forEach(lookupRef.getActiveLookup(), function(item) {
					option = document.createElement("option");
					option.setAttribute("value", item.data);
					option.appendChild(document.createTextNode(item.label));
					comboObject.options.add(option);
				});
				
				this.setSelectedValue(comboObject, customCodeObj.originalValue); //if edit existing
			}
			this._customInputComponentList.push(comboObject);
			embedDiv.appendChild(comboObject);
		} else {
			var textObject = document.createElement("input");
			textObject.type = "text";
			textObject.id = parameterKey;
			textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
			if (customCodeObj.originalValue) {
				//if edit existing
				textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(customCodeObj.originalValue);
			}
			this._customInputComponentList.push(textObject);
			embedDiv.appendChild(textObject);
		}
		uom = customCodeObj.uomSymbol;
		if (uom.length > 0) {
			label = " " + uom;
			span = document.createElement("span");
			span.setAttribute("style", "margin-left:2px; margin-right:2px;");
			span.appendChild(document.createTextNode(label));
			embedDiv.appendChild(span);
		}
	}
	
	ActivityRemarksPopulation.prototype.createUserInputObject = function(index, embedDiv) {
		var uom = "";
		var lookupRef = null;
		var label = null;
		var span = document.createElement("span");
		
		if (this._lookupModuleParams.length >= index) {
			var obj = this._lookupModuleParams[index];
			if (obj != null) {
				if (this._lookupList.hasOwnProperty(obj.value)) {
					var comboObject = document.createElement("select");
					comboObject.id = obj.value;
					comboObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					lookupRef = this._lookupList[obj.value];
					
					if (lookupRef != null) {
						var option = document.createElement("option");

						D3.forEach(lookupRef.getActiveLookup(), function(item) {
							option = document.createElement("option");
							option.setAttribute("value", item.data);
							option.appendChild(document.createTextNode(item.label));
							comboObject.options.add(option);
						});
						
						this.setSelectedValue(comboObject, obj.originalValue);
					}
					this._userInputComponentList.push(comboObject);
					embedDiv.appendChild(comboObject);
				} else {
					var textObject = document.createElement("input");
					textObject.type = "text";
					textObject.id = obj.value;
					textObject.setAttribute("style", "margin-left:2px; margin-right:2px;");
					textObject.setAttribute("placeholder", this._placeholder[index]);
					var textObjSize = 4;
					if (this._placeholder[index]) {
						textObjSize = this._placeholder[index].length;
					}
					textObject.setAttribute("size", textObjSize);
					if (obj.originalValue) {
						//if edit existing
						textObject.value = this._node.commandBeanProxy.getServerLocaleInfo().removeThousandSeparators(obj.originalValue);
					}
					this._userInputComponentList.push(textObject);
					embedDiv.appendChild(textObject);
				}
				
				uom = this._lookupParamsUOM[index];
				if (uom.length > 0) {
					label = " " + uom;
					span = document.createElement("span");
					span.setAttribute("style", "margin-left:2px; margin-right:2px;");
					span.appendChild(document.createTextNode(label));
					embedDiv.appendChild(span);
				}
				
				return;
			}
		}
		
		label = "#";
		return;
	}
	
	ActivityRemarksPopulation.prototype.btnCancel_onClick = function(event){
		$(this._panel).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.btnCancelInitial_onClick = function(event){
		$(this._panelInitial).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.btnBack_onClick = function(event) {
		var params = this.getParams();
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		$(this._panel).dialog('close');
		this.loadXMLCustomParameters(response);
	}
	
	ActivityRemarksPopulation.prototype.btnOK_onClick = function(event) {
		var outputList = "";
		var result = "";
		var errList = "";
		
		for (var i=0; i < this._userInputComponentList.length; i++) {
			var uiObj = this._userInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			
			result = this.validate(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				} 
				errList += result;
			}
			else {
				var obj = this._lookupModuleParams[i];
				if (obj != null) {
					if (outputList.length > 0) {
						outputList += "::";
					}
					var dataType = obj.label;
					var precision = this._lookupParamsPrecision[i];
					if (dataType == "double" && value.length != 0) {
						// Convert to raw number
						value = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
						
						// Process
						value = this.numberFormatter.formatRoundingWithPrecision(value, D3.NumberBase.NEAREST, precision);
						value = this.numberFormatter.formatThousands(value);
						value = this.numberFormatter.formatDecimal(value);
						value = this.numberFormatter.formatPrecision(value, precision);
						
						// Convert back to locale specific format
						value = this.numberFormatter.localizeNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
					}
					
					outputList = outputList + obj.value + ":=" + value;
				}
			}
		}
		
		this.getCustomCodeInputEntered();
		outputList = this.appendConstantValue(outputList);
		
		if (errList.length == 0) {
			this.setFieldValue(outputList); //important to set @activityFreeTextRemark
			
			$(this._panel).dialog('close');
			
			this._node.commandBeanProxy.addAdditionalFormRequestParams("auto_populate_description", this.populateFreeTextRemark());
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_key", this._customCodeParamKey);
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_value", this._customCodeParamValue);
			this._node.commandBeanProxy.addAdditionalFormRequestParams("custom_code_param_uid", this._customCodeParamUid);
			this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);			
		} else {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		
	}
	
	ActivityRemarksPopulation.prototype.getCustomCodeInputEntered = function() {
		this._customCodeParamKey = "";
		this._customCodeParamValue = "";
		this._customCodeParamUid = "";
		
		if (this._customInputParams) {
			for (var j=0; j < this._customInputParams.length; j++) {
				var obj2 = this._customInputParams[j];
				var value2 = obj2.value;
				
				if (value2 != null && value2.length == 0) {
					value2 = "null";
				}
				if (this._customCodeParamKey.length > 0) {
					this._customCodeParamKey += ":.:.:";
				}
				if (this._customCodeParamValue.length > 0) {
					this._customCodeParamValue += ":.:.:";
				}
				if (this._customCodeParamUid.length > 0) {
					this._customCodeParamUid += ":.:.:";
				}
				this._customCodeParamKey += obj2.label;
				this._customCodeParamValue += value2;
				this._customCodeParamUid += obj2.customCodeUid;
			}
		}
	} 
	
	ActivityRemarksPopulation.prototype.btnRetrieve_onClick = function(event) {
		var params = this.getParamsFTR();
		
		var result = "";
		var errList = "";
		var parameterKeyArray = [];
		var valueArray = [];
		var customCodeUidArray = [];
		for (var i=0; i < this._customInputComponentList.length; i++) {
			var uiObj = this._customInputComponentList[i];
			var value = "";
			if (uiObj.type == "select-one") {
				value = uiObj.options[uiObj.selectedIndex].value;
			} else {
				value = uiObj.value;
			}
			result = this.validateCustomInput(i, value);
			if (result.length > 0) {
				if (errList.length > 0) {
					errList += "\n";
				}
				errList += result;
			}
			else {
				var obj = this._CustomInputList[i];
				if (obj != null) {
					parameterKeyArray[i] = obj.parameterKey;
					valueArray[i] = value;
					customCodeUidArray[i] = obj.customCodeUid;
				}
			}
		}
		
		if (errList.length != 0) {
			if (errList.length > 0) {
				errList = D3.LanguageManager.getLabel("Activity.invalidDataFormat") + "\n" + errList;
				alert(errList);
			}
		}
		params.outputParameterArray = parameterKeyArray;
		params.outputValueArray = valueArray;
		params.outputCustomCodeUidArray = customCodeUidArray;
		
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		this.loadXMLFreeTextRemarks(response);

		$(this._panelInitial).dialog('close');
	}
	
	ActivityRemarksPopulation.prototype.validate = function(index, value) {
		var obj = this._lookupModuleParams[index];
		
		if (obj != null) {
			var dataType = obj.label;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksPopulation.prototype.validateCustomInput = function(index, value) {
		var obj = this._CustomInputList[index];
		if (obj != null) {
			var dataType = obj.dataType;
			dataType = dataType.toLowerCase();
			if ((dataType == "double" || dataType == "integer") && value.length != 0) {
				var rawValue = this.numberFormatter.parseNumberString(value,this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator,this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
				if (rawValue != null && rawValue.length != 0 && $.isNumeric(rawValue) == false) {
					return D3.LanguageManager.getLabel("Activity.textBox") + " [" + (index + 1).toString() + "] : " + dataType;
				}
			}
		}
		return "";
	}
	
	ActivityRemarksInline.prototype.btnWitsml_onClick = function() {
		this.queryBox = new JSEventsMessageBox (this._node, this);
		var startDate = new Date(parseInt(this._node.getFieldValue("startDatetime")));
		var startDateInMiliseconds = (startDate.getUTCHours() * 60 * 60 * 1000) + (startDate.getUTCMinutes() * 60 * 1000);
		var endDate = new Date(parseInt(this._node.getFieldValue("endDatetime")));
		var endDateInMiliseconds = (endDate.getUTCHours() * 60 * 60 * 1000) + (endDate.getUTCMinutes() * 60 * 1000);
		
		if((isNaN(startDateInMiliseconds) || isNaN(endDateInMiliseconds)) || (startDateInMiliseconds > endDateInMiliseconds)) alert("Activity start/end time is not valid.");
		else {
			this.queryBox.startQuery(startDateInMiliseconds, endDateInMiliseconds);
		}
	}
	
	ActivityRemarksPopulation.prototype.setSelectedValue = function(comboBox, selectedValue) {
		for (var i=0; i < comboBox.length; i++) {
			if (comboBox.options[i].value == selectedValue) {
				comboBox.selectedIndex = i;
				break;
			}
		}
	}
	
	ActivityRemarksPopulation.prototype.appendConstantValue = function(bufferList) {
		if (this._lookupModuleConstant != null) {
			var obj;
			
			for (var index = 0;index < this._lookupModuleConstant.length; index++) {
				obj = this._lookupModuleConstant[index];
				if (obj != null) {
					if (bufferList.length > 0) {
						bufferList += "::";
					} 
					bufferList += obj.value + ":=" + obj.label; 
				}
			}
		}
		return bufferList;
	}
	
	ActivityRemarksPopulation.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "collectCustomParameters";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksPopulation.prototype.getParamsFTR = function() {
		var params = {};
		params._invokeCustomFilter = "collectFreeTextRemarks";
		params.activityUid = this._node.primaryKeyValue;
		params.activityFreeTextRemark = this._node.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._node.getFieldValue("classCode");
		params.phaseCode = this._node.getFieldValue("phaseCode");
		params.jobTypeCode = this._node.getFieldValue("jobTypeCode");
		params.taskCode = this._node.getFieldValue("taskCode");
		params.userCode = this._node.getFieldValue("userCode");
		params.sourceActivityUid = this._node.getFieldValue("@sourceActivityUid");
		params.customDescriptionMatrixUid = this._node.getFieldValue("@customDescriptionMatrixUid");
		return params;
	}
	
	ActivityRemarksPopulation.prototype.populateFreeTextRemark = function(){
		var buffer = this._selectedRemark;
		if ((buffer.length>0) && (buffer.indexOf("#") >= 0)) {
			var uomSymbol = "";
			var strOutput = "";
			var componentIndex = 0;
			var temp = buffer.split("#");
			var uiObj = null;
			
			var lastIndex = temp.length - 1;
			for (var i=0; i<=lastIndex; i++) {
				if (i != lastIndex) {
					if (this._userInputComponentList.length > componentIndex) {
						uiObj = this._userInputComponentList[componentIndex];
						if (uiObj == null) {
							strOutput += temp[i].toString() + "#";
						} else {
							var value = "";
							if (uiObj.type == "select-one") {
								value = uiObj.options[uiObj.selectedIndex].label;
							} else {
								value = uiObj.value;
							}
							if (value) {
								strOutput += temp[i].toString();
								var obj = this._lookupModuleParams[componentIndex];
								var dataType = obj.label;
								var precision = this._lookupParamsPrecision[componentIndex];
								if (dataType == "double" && value.length != 0 && uiObj.type != "select-one") {
									var strValue = this.numberFormatter.parseNumberString(value.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strValue = this.numberFormatter.formatRoundingWithPrecision(strValue, D3.NumberBase.NEAREST, precision);
									strValue = this.numberFormatter.formatThousands(strValue);
									strValue = this.numberFormatter.formatDecimal(strValue);
									strValue = this.numberFormatter.formatPrecision(strValue, precision);
									strValue = this.numberFormatter.localizeNumberString(strValue.toString(), this._node.commandBeanProxy.getServerLocaleInfo().thousandSeparator, this._node.commandBeanProxy.getServerLocaleInfo().decimalSeparator);
									strOutput += strValue;
								} else {
									strOutput += value;
								}
								if (this._lookupParamsUOM.length > componentIndex) {
									uomSymbol = this._lookupParamsUOM[componentIndex];
									strOutput += " " + uomSymbol;
								}
							} else {
								//remove prefix and suffix of parameter if empty
								var processedWord = "";
								if (this._prefix) {
									processedWord = temp[i].toString().replace(this._prefix[componentIndex], "");
									strOutput += processedWord;
								}
								if (this._suffix) {
									processedWord = temp[i+1].toString().replace(this._suffix[componentIndex], "");
									temp[i+1] = processedWord;
								}
							}
						}
						componentIndex++;
					} else {
						strOutput += "#";
					}
				} else {
					strOutput += temp[i].toString();
				}
			}
			buffer = strOutput;
		}
		return buffer;
	}
	
	ActivityRemarksPopulation.prototype.allowFieldShow = function(){
		var showButton = this._node.getFieldValue("@showFTRButton") == "1" ? true : false;
		return showButton;
	}
	
	ActivityRemarksPopulation.prototype.refreshFieldRenderer = function() {
		
	};
	
	
	D3.inherits(LessonLearnedRecords,D3.AbstractFieldComponent);
	
	function LessonLearnedRecords(config){
		this._linkConfig = config;
		D3.AbstractFieldComponent.call(this);
	}
	
	LessonLearnedRecords.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var container = "<div class='fieldRenderer'>Lesson Ticket(s) available:</div>";
			this._fieldRenderer = $(container).get(0);
			this.loadData();
		}
		return this._fieldRenderer;
	}
	
	LessonLearnedRecords.prototype.getFieldEditor = function(){
		if (this._fieldEditor == null && !this._linkConfig.noEditor) {
			this._fieldEditor = $("<span style='color:red' class='"+this._linkConfig.editorCSS+"'>"+this._linkConfig.editorLabel+"</span>").get(0);
			LessonLearnedRecords.uber.setTextAlign.call(this,this._fieldEditor);
		}
		return this._fieldEditor;
	}
	
	LessonLearnedRecords.prototype.refreshFieldRenderer = function() {
	}
	
	LessonLearnedRecords.prototype.loadData = function(){
		this.setLessonLearnedLink();
	}
	
	LessonLearnedRecords.prototype.setLessonLearnedLink = function(){
		var self = this;
		var lessonTicketList = this._node.getFieldValue("@lessonTicketList");
		
		var lessonTicket = null;
		if (lessonTicketList){
			lessonTicket = lessonTicketList.split(",");
		}
		
		if (lessonTicket && lessonTicket.length>0){
			for (var i=0;i<lessonTicket.length;i++){
				var strParams = lessonTicket[i];
				if (!strParams[0] != "&" && !strParams[0] != "?")
					strParams = "&"+strParams;
				strParams = decodeURIComponent(strParams.replace(/\+/g,  " "));
				var params = {};
			    var parts = strParams.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			    	params[key] = value;
			    });
			    
				var id = params.lessonticketuid;
				var label = params.lessontitle;
				
				var newLink = D3.UI.createElement({
					tag:"a",
					id:id,
					text:label,
					css:self._linkConfig.rendererCSS,
					data:{
						lessonticketuid:params.lessonticketuid,
					},
					callbacks:{
						onclick:function(event) {
							var link = event.target;
							var data = $(link).data();
							var url = self._linkConfig.url + data.lessonticketuid;
							var msg = self._linkConfig.redirectMsg ? self._linkConfig.redirectMsg : "Redirecting to ";
							//this._node.commandBeanProxy.gotoUrl(url, msg);
							// open new window instead redirecting
							window.open(url, "_blank", "", true);
						}
					},
					context:self
				});
				self._fieldRenderer.appendChild(newLink);
			}
			return self._fieldRenderer;
		}
		return null;
	}
	
	
	D3.inherits(PlannedLessonLearnedRecords,D3.AbstractFieldComponent);
	
	function PlannedLessonLearnedRecords(config){
		this._linkConfig = config;
		D3.AbstractFieldComponent.call(this);
	}
	
	PlannedLessonLearnedRecords.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var container = "<div class='fieldRenderer'>Lesson Ticket(s) available:</div>";
			this._fieldRenderer = $(container).get(0);
			this.loadData();
		}
		return this._fieldRenderer;
	}
	
	PlannedLessonLearnedRecords.prototype.getFieldEditor = function(){
		if (this._fieldEditor == null && !this._linkConfig.noEditor) {
			this._fieldEditor = $("<span style='color:red' class='"+this._linkConfig.editorCSS+"'>"+this._linkConfig.editorLabel+"</span>").get(0);
			LessonLearnedRecords.uber.setTextAlign.call(this,this._fieldEditor);
		}
		return this._fieldEditor;
	}
	
	PlannedLessonLearnedRecords.prototype.refreshFieldRenderer = function() {
	}
	
	PlannedLessonLearnedRecords.prototype.loadData = function(){
		this.setPlannedLessonLearnedLink();
	}
	
	PlannedLessonLearnedRecords.prototype.setPlannedLessonLearnedLink = function(){
		var self = this;
		var plannedLessonTicketList = this._node.getFieldValue("@plannedLessonTicketList");
		
		var plannedLessonTicket = null;
		if (plannedLessonTicketList){
			plannedLessonTicket = plannedLessonTicketList.split(",");
		}
		
		if (plannedLessonTicket && plannedLessonTicket.length>0){
			for (var i=0;i<plannedLessonTicket.length;i++){
				var strParams = plannedLessonTicket[i];
				if (!strParams[0] != "&" && !strParams[0] != "?")
					strParams = "&"+strParams;
				strParams = decodeURIComponent(strParams.replace(/\+/g,  " "));
				var params = {};
			    var parts = strParams.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			    	params[key] = value;
			    });
			    
				var id = params.lessonticketuid;
				var label = params.lessontitle;
				
				var newLink = D3.UI.createElement({
					tag:"a",
					id:id,
					text:label,
					css:self._linkConfig.rendererCSS,
					data:{
						lessonticketuid:params.lessonticketuid,
					},
					callbacks:{
						onclick:function(event) {
							var link = event.target;
							var data = $(link).data();
							var url = self._linkConfig.url + data.lessonticketuid;
							var msg = self._linkConfig.redirectMsg ? self._linkConfig.redirectMsg : "Redirecting to ";
							//this._node.commandBeanProxy.gotoUrl(url, msg);
							// open new window instead redirecting
							window.open(url, "_blank", "", true);
						}
					},
					context:self
				});
				self._fieldRenderer.appendChild(newLink);
			}
			return self._fieldRenderer;
		}
		return null;
	}

	D3.inherits(FtrReloadButton,D3.AbstractFieldComponent);
	
	function FtrReloadButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	FtrReloadButton.prototype.getFieldEditor = function(){
		
		if (!this._fieldEditor){
			this._fieldEditor = D3.UI.createElement({
				tag:"button",
				text:this._config.label,
				css:"DefaultButton",
				callbacks:{
					onclick:this.reClick
				},
				context:this
			}) ;
		}			
		return this._fieldEditor;
	}
	
	FtrReloadButton.prototype.reClick = function(){
		this._node.setFieldValue("@showReloadFTRButton", null);
		this._node.setFieldValue("@showNoFtrSetup", 1);
		this._node.setFieldValue("@importTemplate", 1);
		this._node.commandBeanProxy.submitForServerSideProcess(this._node,"phaseCode");
	}
	
	D3.inherits(ReloadNoFtrMessage,D3.AbstractFieldComponent);
	
	function ReloadNoFtrMessage(){
		D3.AbstractFieldComponent.call(this);
	}
	
	ReloadNoFtrMessage.prototype.getFieldEditor = function(){
		
		if (!this._fieldEditor){
			var ok = {
					tag:"button",
					css:"DefaultButton",
					text:D3.LanguageManager.getLabel("button.ok"),
					callbacks:{},
					context:this
				};
			
			this._fieldEditor = document.createElement("div");
			this._fieldEditor.appendChild(document.createTextNode(D3.LanguageManager.getLabel("Activity.noFixedTextRemarkFound")));
			var button = document.createElement("div");
			button.appendChild(D3.UI.createElement(ok));
			this._fieldEditor.appendChild(button);
		}			
		return this._fieldEditor;
	}
	
	D3.inherits(ActivityNodeListener,D3.EmptyNodeListener);
	
	function ActivityNodeListener()
	{
		this._copyFields = ["planReference","planTaskReference","planDetailReference","classCode","phaseCode","taskCode","rootCauseCode","depthMdMsl","holeSize","jobTypeCode","businessSegment",
		                    "sections","incidentNumber","incidentLevel","ofInterest","userCode","equipmentCode","subEquipmentCode",
		      				"lookupCompanyUid","contractRateCode","tripNumber","failureType","failureCause",
		      				"@showFTRButton","conveyanceMethod","stringToDepthMdMsl"];
	}
	
	
	ActivityNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "unwantedEventLink"){
			var config = {
					rendererLabel:D3.LanguageManager.getLabel("label.unwantedEventLink"),
					rendererCSS:"unwantedEventLink",
					editorLabel:D3.LanguageManager.getLabel("warning.unwantedEventLink"),
					editorCSS:"unwantedEventLink",
					redirectMsg:D3.LanguageManager.getLabel("label.unwantedEventLink.redirect"),
					url: "unwantedevent.html?activityId=" + escape(node.getFieldValue("activityUid")),
			};
			return new ActivityLinkField(config);
		}else if (id == "lessonTicketLink") {
			var config = {
					rendererLabel:D3.LanguageManager.getLabel("label.lessonTicketLink"),
					rendererCSS:"lessonTicketLink",
					editorLabel:D3.LanguageManager.getLabel("warning.lessonTicketLink"),
					editorCSS:"lessonTicketLink",
					redirectMsg:D3.LanguageManager.getLabel("label.lessonTicketLink.redirect"),
					url: node.getGWP("specificLessonTicketLinkfromActivty")+"?newRecordInputMode=1&activityId=" + escape(node.getFieldValue("activityUid")),
			};
			return new ActivityLinkField(config);
		}else if (id == "lessonTicketRecord") {
			var config = {
					rendererLabel:node.getFieldValue("@lessonTicketRecordFound"),
					rendererCSS:"lessonTicketRecord",
					redirectMsg:D3.LanguageManager.getLabel("label.lessonTicketLink.redirect"),
					url: node.getGWP("specificLessonTicketLinkfromActivty") + "?gotoFieldName=lessonTicketUid&gotoFieldUid=" + escape(node.getFieldValue("@lessonTicketUid")),
					noEditor : true
			};
			return new ActivityLinkField(config);
		}else if (id == "lessonLearnedRecords") {
			var config = {
					rendererCSS:"fieldRenderer hyperlinkField lessonLearnedLinks",
					redirectMsg:D3.LanguageManager.getLabel("label.lessonTicketRecord.redirect"),
					url: node.getGWP("specificLessonTicketLinkfromActivty") + "?gotoFieldName=lessonTicketUid&gotoFieldUid=",
					noEditor : true
			};
			return new LessonLearnedRecords(config);
		}else if (id == "plannedlessonLearnedRecords") {
			var config = {
					rendererCSS:"fieldRenderer hyperlinkField lessonLearnedLinks",
					redirectMsg:D3.LanguageManager.getLabel("label.lessonTicketRecord.redirect"),
					url: "plannedlessonlearned.html?gotoFieldName=lessonTicketUid&gotoFieldUid=",
					noEditor : true
			};
			return new PlannedLessonLearnedRecords(config);
		}else if (id == "activityLink") {
			var config = {
					rendererLabel:"-",
					rendererCSS:"activityLink",
					redirectMsg:D3.LanguageManager.getLabel("label.activityScreen.redirect"),
					url: "activity.html?gotoday=" + escape(node.getFieldValue("dailyUid")),
					noEditor : true
			};
			return new ActivityLinkField(config);
		}else if (id == "BatchCoding") {
			return new BatchCodingButton();
		}else if (id == "activityDurationCustomField") {
			return new ActivityDurationCustomField();
		}else if (id=="ActivityCustomTimeField") {
			return new ActivityCustomTimeField();
		}else if (id=="ActivityNptEventLink") {
			return new ActivityNptEventLink(); 
		}else if (id=="ActivityCodeField") {
			return new CustomActivityCodeField(); 
		}else if (id=="ActivityRCMessage") {
			return new CustomRCMessage();
		}else if (id=="ActivityRemarksPopulation"){
			return new ActivityRemarksPopulation();
		}else if (id=="ActivityRemarksInline") {
			return new ActivityRemarksInline();
		}else if (id == "reloadFTR") {
			return new FtrReloadButton();
		}else if (id == "reloadNoFTRFound") {
			return new ReloadNoFtrMessage();
		}else {
			return null;
		}
	}
	
	ActivityNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "add_next_day_activity" ) {

			var nextDayDailyUid = node.commandBeanProxy.rootNode.getFieldValue("@nextDayDailyUid");
			var nextDayAvailable = node.commandBeanProxy.rootNode.getFieldValue("@nextDayAvailable");
			
			if (nextDayAvailable == "0" && nextDayDailyUid == "") {
				node.addNewChildNodeOfClass("NextDayActivity",true,true);
				
			} else if (nextDayAvailable == "0" && nextDayDailyUid != "") {
				var url =  "activity.html?gotoday="+nextDayDailyUid+"&newActivityRecordInputMode=1";
				node.commandBeanProxy.gotoUrl(url,"Redirecting to ");

			}
			
		}else if (id == "reprocess_data" ) {
			var params ={};
			params._invokeCustomFilter = "reprocessData";
			node.commandBeanProxy.submitForm();
			node.commandBeanProxy.customInvoke(params,this.loaded,this);
		}else if (id == "clear_data" ) {
			var params ={};
			params._invokeCustomFilter = "clearData";
			node.commandBeanProxy.submitForm();
			node.commandBeanProxy.customInvoke(params,this.loaded,this);
		}
	}
	
	ActivityNodeListener.prototype.loaded = function(response) {
		location.reload(); 
	}

	ActivityNodeListener.prototype.getCustomRecordActionButton = function(id, node, commandBean, buttonDeclaration, buttonData) {
		if (id == "createRecord") {
			var self = this;
			var container = $("<span class='option-button dropdown'>");
			var button = $("<button type='button' data-toggle='dropdown' class='dropdown-toggle RecordActionsButton CreateRecord' aria-expanded='false'><i class='fa fa-angle-down'></i></button>");
			container.append(button);
			var selection = $("<ul class='dropdown-menu'><li><a id='addAbove'>Insert above</a></li><li><a id='addBelow'>Insert below</a></li></ul>");
			container.append(selection);
			
			var $addAbove = container.find("#addAbove");
			$addAbove.on("click", function() {
				var targetIndex = node.getCurrentNodeIndex();
				node.parent.addNewChildNodeOfClass(node.simpleClassName(), true, true, {targetNode:node, insert:"before", insertAtIndexOf:targetIndex, triggerNodeCreatedEvent:false, afterChildNodeCreated:function(newNode) {
					self.afterInsertAboveOrBelow("before", newNode, node);
				}});
			});
			
			var $addBelow = container.find("#addBelow");
			$addBelow.on("click", function() {
				var targetIndex = node.getCurrentNodeIndex();
				node.parent.addNewChildNodeOfClass(node.simpleClassName(), true, true, {targetNode:node, insert:"after", insertAtIndexOf:++targetIndex, triggerNodeCreatedEvent:false, afterChildNodeCreated:function(newNode) {
					self.afterInsertAboveOrBelow("after", newNode, node);
				}});
			});
			return container.get(0);
		}
		return null;
	}
	ActivityNodeListener.prototype.afterInsertAboveOrBelow = function(insertTo, newNode, node) {
		var siblingNode = (insertTo == "before") ? node.getPreviousNode() : node.getNextNode();
		var timeZoneOffset = new Date().getTimezoneOffset();
		var startTime = null;
		var endTime = null;
		if (insertTo == "before") {
			if (siblingNode != null) {
				startTime = parseInt(siblingNode.getFieldValue("endDatetime"));
			} else {
				var timeZoneOffset = new Date().getTimezoneOffset();
				startTime = parseInt(node.getFieldValue("startDatetime"));
				var zero = new Date(startTime);
				zero.setHours(00);
				zero.setMinutes(00);
				zero.setSeconds(00);
				startTime = parseInt(zero.getTime()) - (timeZoneOffset * 60000);
			}
			endTime = parseInt(node.getFieldValue("startDatetime"));
		} else {
			if (siblingNode != null) {
				endTime = parseInt(siblingNode.getFieldValue("startDatetime"));
			} else {
				var timeZoneOffset = new Date().getTimezoneOffset();
				var now = new Date();
				now.setHours(23);
				now.setMinutes(59);
				now.setSeconds(59);
				endTime = parseInt(now.getTime()) - (timeZoneOffset * 60000);
			}
			startTime = parseInt(node.getFieldValue("endDatetime"));
		}
		newNode.setFieldValue("startDatetime", startTime);
		newNode.setFieldValue("endDatetime", endTime);
		newNode.setFieldValue("depthFromMdMsl", node.getFieldValue("depthMdMsl"));
		newNode.setFieldValue("stringFromDepthMdMsl", node.getFieldValue("stringToDepthMdMsl"));
		this.copyFieldValue(node, newNode);
		newNode.setFieldValue("@sourceActivityUid", node.primaryKeyValue);
	};
	
	ActivityNodeListener.prototype.afterRecordActionCreated = function(node, view) {
		if (node.isClientSideNewlyAdded()) return;
		var previousNode = node.getPreviousNode();
		if (previousNode != null) {
			var startTime = new Date(parseInt(node.getFieldValue("startDatetime")));
			var startMinutes = startTime.getMinutes() + (startTime.getHours() * 60);
			var previousEndTime = new Date(parseInt(previousNode.getFieldValue("endDatetime")));
			var previousMinutes = previousEndTime.getMinutes() + (previousEndTime.getHours() * 60);
			
			if (previousMinutes < startMinutes) {
				$(view).addClass("gap-row");
			} else if (previousMinutes > startMinutes) {
				$(view).addClass("overlap-row");
			}
		}
	};

	ActivityNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		
		var btn = null;
		if (id == "add_next_day_activity" && parentNode.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "NextDayActivity")) {
			btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>")
			
			return btn.get(0);
		}else if (id == "reprocess_data") {
			btn = $("<button class='DefaultButton' style='margin:3px'>Reprocess Data</button>");
			return btn.get(0);
		}else if (id == "clear_data") {
			btn = $("<button class='DefaultButton' style='margin:3px'>Clear Data</button>");
			return btn.get(0);
		}
		 
		return null;
	};
	
	ActivityNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration) {
		this._commandBeanProxy = commandBeanProxy;
		if (id == "refresh") {
			var refreshDataBtn = document.createElement("button");
			refreshDataBtn.className = "rootButtons";
			var label = D3.LanguageManager.getLabel("button.refreshExternalActivity");
			var refreshLabel = document.createTextNode(label);
			refreshDataBtn.appendChild(refreshLabel);
			refreshDataBtn.onclick = this.onClickRefreshExternalActivityBtn.bindAsEventListener(this);
			this.startStatusCheck();
			return refreshDataBtn;
		}
	};
	
	ActivityNodeListener.prototype.onClickRefreshExternalActivityBtn = function(event) {
		var self = this;
		D3.UIManager.confirm("Refresh Data", "Are you sure you want to refresh? This will also cause External Activities to be reprocessed.", function() {
			var params = {};
			params._invokeCustomFilter = "refresh_data";			
			self._commandBeanProxy.customInvoke(params, self.submitRefreshExternalActivityCompleted, self, "json");
		}, null, this);
	};
	
	ActivityNodeListener.prototype.submitRefreshExternalActivityCompleted = function(data) {
		var response = data.response;
		if (response.status == "success") {
			this.startStatusCheck();
		} else {
			D3.UIManager.popup("Error", "Failed to check status with error: " + response.errorMessages);
		}
	};
	
	ActivityNodeListener.prototype.startStatusCheck = function() {
		this.scheduleNextStatusCheck();
	};
	
	ActivityNodeListener.prototype.scheduleNextStatusCheck = function() {
		var self = this;
		setTimeout(function() {
			self.checkStatus();
		}, 1000);
	};
	
	ActivityNodeListener.prototype.checkStatus = function() {
		this.setStatusMessage("Checking status...");
		var params = {};
		params._invokeCustomFilter = 'check_status';
		this._commandBeanProxy.customInvoke(params, this.statusCheckCallback, this, "json");
	};
	
	ActivityNodeListener.prototype.statusCheckCallback = function(data) {
		this.prepareDialog();
		var response = data.response;
		if (response.status == "success") {
			if (response.result == "queue" || response.result == "pending") {
				this.setStatusMessage("Fetching external activity is in progress...");
				this.startStatusCheck();
			} else {
				this.statusDialog.dialog("close");
			}
		} else {
			var messages = response.messages;
			if (messages != undefined && messages != null) {
				D3.UIManager.popup("Error", response.messages[0]);
			} else {
				this.statusDialog.dialog("close");
			}
		}
	};
	
	ActivityNodeListener.prototype.prepareDialog = function() {
		if (this.dialogContent != null) return;
		
		var dialog = $("<div id='statusDialog'></div>");
		this.dialogContent = $("<div class='dialogContent' style='white-space:pre;'></div>");
		dialog.append(this.dialogContent);
		
		var self = this;
		
		this.statusDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
	
	ActivityNodeListener.prototype.setStatusMessage = function(message) {
		this.prepareDialog();
		this.dialogContent.empty();
		this.dialogContent.append(message);
		this.statusDialog.dialog("open");
	};
	
	ActivityNodeListener.prototype.copyFieldValue = function(source,destination){
		for (var i=0;i< this._copyFields.length;i++) {
			var fieldName = this._copyFields[i];
			destination.setFieldValue(fieldName, source.getFieldValue(fieldName));
		}
	}
	ActivityNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		var actMaxEndTime = null;
		var nActMaxEndTime = null;

		var fieldName;
		var fieldValues = [];
		for (var fieldName in this.copyFields) {
			fieldValues[fieldName] = null;
		}

		var noOfNextDayActivity = 0;
		if(parent.children["NextDayActivity"]!= undefined) noOfNextDayActivity = parent.children["NextDayActivity"].getLength();
		var simpleClassName = node.simpleClassName();
		var timeZoneOffset = new Date().getTimezoneOffset();
		
		
		if(simpleClassName == "Activity")
		{
			var acts = parent.children["Activity"].getItems();
			D3.forEach(acts,function(child){
				var endTime = child.getFieldValue("endDatetime");
				var endTimeCon = parseInt(endTime) + (timeZoneOffset/60*3600*1000);
				if (endTime) {
					if (!actMaxEndTime || (new Date(parseInt(endTimeCon)).format("hh:nn")> new Date(parseInt(actMaxEndTime)).format("hh:nn"))) {
						actMaxEndTime = endTimeCon;
					}
				}
				node.setFieldValue("depthFromMdMsl", child.getFieldValue("depthMdMsl"));
				node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
				this.copyFieldValue(child,node);
				node.setFieldValue("@sourceActivityUid", child.primaryKeyValue);
			},this);	
		}
		else if(simpleClassName == "ActivityRsd")
		{
			if(noOfNextDayActivity < 1)
			{
				var acts = parent.children["ActivityRsd"].getItems();
				D3.forEach(acts,function(child){
					node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
					for (var i=0;i< this._copyFields.length;i++) {
						var fieldName = this._copyFields[i];
						fieldValues[fieldName] = child.getFieldValue(fieldName);
					}
				},this);
			}
		} else if(simpleClassName == "ActivityExt")
			{
				var acts = parent.children["ActivityExt"].getItems();
				D3.forEach(acts,function(child){
					var endTime = child.getFieldValue("endDatetime");
					var endTimeCon = parseInt(endTime) + (timeZoneOffset/60*3600*1000);
					if (endTime) {
						if (!actMaxEndTime || (new Date(parseInt(endTimeCon)).format("hh:nn")> new Date(parseInt(actMaxEndTime)).format("hh:nn"))) {
							actMaxEndTime = endTimeCon;
						}
					}
					node.setFieldValue("depthFromMdMsl", child.getFieldValue("depthMdMsl"));
					node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
					this.copyFieldValue(child,node);
					node.setFieldValue("@sourceActivityUid", child.primaryKeyValue);
				},this);
		} else if(simpleClassName == "ActivityMar")
			{
				if(noOfNextDayActivity < 1)
				{
					var acts = parent.children["ActivityMar"].getItems();
					D3.forEach(acts,function(child){
						node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
						for (var i=0;i< this._copyFields.length;i++) {
							var fieldName = this._copyFields[i];
							fieldValues[fieldName] = child.getFieldValue(fieldName);
						}
					},this);
				}
		} else {
			if(noOfNextDayActivity < 1)
			{
				var acts = parent.children["Activity"].getItems();
				D3.forEach(acts,function(child){
					node.setFieldValue("depthFromMdMsl", child.getFieldValue("depthMdMsl"));
					node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
					for (var i=0;i< this._copyFields.length;i++) {
						var fieldName = this._copyFields[i];
						fieldValues[fieldName] = child.getFieldValue(fieldName);
					}
				},this);
			}
		}	
		
		if(simpleClassName == "NextDayActivity"){
			var acts = parent.children["NextDayActivity"].getItems();
			D3.forEach(acts,function(child){
				var endTime = child.getFieldValue("endDatetime");
				var endTimeCon = parseInt(endTime) +(timeZoneOffset/60*3600*1000);
				if (endTime) {
					if (!nActMaxEndTime || (new Date(parseInt(endTimeCon)).format("hh:nn")> new Date(parseInt(nActMaxEndTime)).format("hh:nn"))) {
						nActMaxEndTime = endTimeCon;
					}
				}
				node.setFieldValue("depthFromMdMsl", child.getFieldValue("depthMdMsl"));
				node.setFieldValue("stringFromDepthMdMsl", child.getFieldValue("stringToDepthMdMsl"));
				this.copyFieldValue(child,node);
				node.setFieldValue("@sourceActivityUid", child.primaryKeyValue);
			},this);	

			if (noOfNextDayActivity < 1){
				for (var i=0;i< this._copyFields.length;i++) {
					var fieldName = this._copyFields[i];
					node.setFieldValue(fieldName, fieldValues[fieldName]);
				}
			}
		}
		
		
		if (simpleClassName == "Activity" && actMaxEndTime != null) {
			node.setFieldValue("startDatetime",parseInt(actMaxEndTime)- (timeZoneOffset/60*3600*1000));
			node.setFieldValue("endDatetime", parseInt(actMaxEndTime)- (timeZoneOffset/60*3600*1000));
		}
		
		if (simpleClassName == "NextDayActivity" && nActMaxEndTime != null) {
			node.setFieldValue("startDatetime", parseInt(nActMaxEndTime)- (timeZoneOffset/60*3600*1000));
			node.setFieldValue("endDatetime", parseInt(nActMaxEndTime)- (timeZoneOffset/60*3600*1000));
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ActivityNodeListener;
	
	function JSEventsMessageBox(node, context) {
		this._processingNode = node;
		this.context = context;
		this.isCancelled = false;
		this.title = "Query WITSML data";
		this.init();
	}
	
	JSEventsMessageBox.prototype.dispose = function() {
		this._processingNode = null;
		this._commandBeanProxy = null;
		this.box = null;
	};
	
	JSEventsMessageBox.prototype.init = function() {
		if(!this.box) {
			var _boxDiv = "<div class='popUpContainer'></div>";
			this.box = $(_boxDiv);
			this.build(this.box);
		}
	};
	
	JSEventsMessageBox.prototype.build = function(parent) {
		var tableCfg = "<div class='popUpLineOne-label'/>" +
						"<div class='popUpLineTwo-button'/>"
		parent.append(tableCfg);

		var _popUpLineOne = parent.find(".popUpLineOne-label").get(0);
		//future refinement: add warning/error/info icons before message
//		this.image = document.createElement("img");
//		this.image.setAttribute('src', "images/warning.png");
//		this.image.setAttribute('height', '20px');
//		this.image.setAttribute('width', '20px');
//		this.image.setAttribute('style', 'padding:0 0 10px 0');
//		_popUpLineOne.appendChild(this.image);
		
		var labelText = "Loading ... ";
		this._messageLabel = document.createElement("label");
		this._messageLabel.appendChild(document.createTextNode(labelText));
		_popUpLineOne.appendChild(this._messageLabel);
		
		
		
		var _popUpLineTwo = parent.find(".popUpLineTwo-button").get(0);
		_popUpLineTwo.align = "right";
		this.buildButton(_popUpLineTwo);
	};
	
	JSEventsMessageBox.prototype.updateLabel = function(status, message){
		this._messageLabel.appendChild(document.createElement("br"));
		this._messageLabel.appendChild(document.createTextNode(message));
		this._messageLabel.appendChild(document.createElement("br"));
		this._hasMessage = true;
	};
	
	JSEventsMessageBox.prototype.clearLabel = function(){
		while (this._messageLabel.hasChildNodes()) {
			this._messageLabel.removeChild(this._messageLabel.lastChild);
		}
	};
	
	JSEventsMessageBox.prototype.buildButton = function(container) {
		var buttonConfig = {
				tag:"button",
				text:"Cancel",
				css:"DefaultButton",
				callbacks:{
					onclick:this._button_onClick
				},
				context:this
		}
		this._button = D3.UI.createElement(buttonConfig);
		container.appendChild(this._button);
	};
	
	JSEventsMessageBox.prototype._button_onClick = function(){
		this.isCancelled = true;
		this.close();
	};
	
	JSEventsMessageBox.prototype.close = function(){
		if(this.box!=null && this.box.hasClass("ui-dialog-content") && this.box.dialog('isOpen')) this.box.dialog("close");
		this.dispose();
	};
	
	JSEventsMessageBox.prototype.show = function() {
		this.showBox();
	};
	
	JSEventsMessageBox.prototype.showBox = function() {
		this.box.dialog({
			width: "300",
			minHeight: "130",
			title: this.title,
			resizable: false,
			modal: true,
			open: function(event, ui) {
		        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
		    }
		});
	};
	
	JSEventsMessageBox.prototype.startQuery = function(startDateInMiliseconds, endDateInMiliseconds) {
		this.show();
		var simpleClassName = this._processingNode.simpleClassName();
		var self = this;
		var response = null;
		
		var params ={};
		params.activityUid = this._processingNode.primaryKeyValue;
		params.isDayPlus = (simpleClassName=="NextDayActivity" ? true : false);
		params.startDatetime = startDateInMiliseconds;
		params.endDatetime = endDateInMiliseconds;
		
		params.activityFreeTextRemark = this._processingNode.getFieldValue("@activityFreeTextRemark");
		params.classCode = this._processingNode.getFieldValue("classCode");
		params.phaseCode = this._processingNode.getFieldValue("phaseCode");
		params.jobTypeCode = this._processingNode.getFieldValue("jobTypeCode");
		params.taskCode = this._processingNode.getFieldValue("taskCode");
		params.userCode = this._processingNode.getFieldValue("userCode");
		params.sourceActivityUid = this._processingNode.getFieldValue("@sourceActivityUid");
		
		if(this.context.customCodeParams != null){
			this.context.customCodeParams._invokeCustomFilter = "queryWitsmlForFtrCustom";
			this.context.customCodeParams.isDayPlus = params.isDayPlus;
			this.context.customCodeParams.startDatetime = startDateInMiliseconds;
			this.context.customCodeParams.endDatetime = endDateInMiliseconds;

			this._processingNode.commandBeanProxy.customInvoke(this.context.customCodeParams, this.queryCompleted, this, null, false);
		} else {
			params._invokeCustomFilter = "queryWitsmlForFtr";
			this._processingNode.commandBeanProxy.customInvoke(params, this.queryCompleted, this, null, false);
		}
	};
	
	JSEventsMessageBox.prototype.queryCompleted = function(response){
		self = this;
		var doc = $(response);
		var isResponseSuccess = "";
		var responseMessage = "";
		var x = doc.find("response");
		self.clearLabel();
		x.find("message").each(function(index, element){
			var elm = $(this);
			status = elm.find("status").first().text();
			responseMessage = elm.find("responseMessage").first().text();
			self.updateLabel(status, responseMessage);
		});

		if(!this.isCancelled){
			if(this.context.customCodeParams != null){
				this.context.loadXMLFreeTextRemarks(response);
			} else {
				this.context.loadXMLCustomParameters(response);
			}
		}

		if(this._hasMessage) this._button.innerText = "Close";
		else this.close();
	};
	
	D3.JSEventsMessageBox = JSEventsMessageBox;
	
})(window);