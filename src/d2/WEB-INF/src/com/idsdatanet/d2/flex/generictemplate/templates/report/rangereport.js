(function(window){
	D3.inherits(RangeReportCustomField,D3.AbstractFieldComponent);
	function RangeReportCustomField(){
		D3.AbstractFieldComponent.call(this);
		this._checkboxes = {};
	}
	
	RangeReportCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			var tableConfig = "<table cellspacing='0' cellpadding='0' width='100%'>" +
			"<tr><td width='25%' valign='top'>" +
			"<input id='distinctRangeReport' type='checkbox' class='distinctRangeReport'>" +
			"<label for='distinctRangeReport' class='textBold'>"+D3.LanguageManager.getLabel("label.distinctReport")+"</label> <p>" +
			"<div style='border: 1px solid #eeeeee; padding: 1px;'>" +
			"<input type='checkbox' id='latestRangeReportCB' class='latestRangeReportCB'>" +
			"<label for='latestRangeReportCB' class='textBold'>"+D3.LanguageManager.getLabel("label.latestReport")+"</label><br>" +
			"<label class='textBold'>or </label><br>" +
			"<input type='checkbox' id='rangeReportDateCB' class='rangeReportDateCB'>" +
			"<label for='rangeReportDateCB' class='textBold'>"+D3.LanguageManager.getLabel("label.selectDateRange")+"</label><br>" +
			"<br>" +
			"<label class='textBold'>&nbsp;&nbsp;"+D3.LanguageManager.getLabel("label.startDate")+"&nbsp;&nbsp;</label>" +
			"<input type='text' id='rangeReportDate' class='fieldEditor rangeReportDate' readonly>" +
			"<label class='textBold'>&nbsp;&nbsp;"+D3.LanguageManager.getLabel("label.endDate")+"&nbsp;&nbsp;</label>"+		
			"<input type='text' id='rangeReportDateTo' class='fieldEditor rangeReportDateTo' readonly>" +
			"</div>" +
			"</td>" +
			"<td width='5%'></td>" +
			"<td>" +
			"<span class='fieldContainer'>" +
			"<div class='multiselect dailySelector'>" +
			"<div class='multiselect-MainContainer floatItem-Left'>" +
			"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='fieldEditor RangeReport-operationList'>" +
			"</ul>" +
			"<div class='multiselect-ButtonContainer'>" +
			"<button id='rangeSelectAllButton' class='DefaultButton'>" +
			"<label for='rangeSelectAllButton'>"+D3.LanguageManager.getLabel("button.selectAll")+"</label></button>" +
			"<button id='rangeUnselectAllButton' class='DefaultButton'>" +
			"<label for='rangeUnselectAllButton'>"+D3.LanguageManager.getLabel("button.unselectAll")+"</label></button>" +
			"</div></div></div>" +
			"</span>" +
			"</td></tr></table>";
			var table = $(tableConfig);
			
			var distinctReport = table.find(".distinctRangeReport");
			this._distinctReport = distinctReport.get(0);
			this._distinctReport.onclick = this.btnDistinctReport_onClick.bindAsEventListener(this);
			
			var latestReportCB = table.find(".latestRangeReportCB");
			this._latestReport = latestReportCB.get(0);
			this._latestReport.onclick = this.btnlatestReportCB_onClick.bindAsEventListener(this);
			
			var reportDateCB = table.find(".rangeReportDateCB");
			this._selectReport = reportDateCB.get(0);
			this._selectReport.onclick = this.btnreportDateCB_onClick.bindAsEventListener(this);
			
			var reportDate = table.find(".rangeReportDate");
			this._reportDate = reportDate.get(0);
			$(this._reportDate).datepick({dateFormat:'dd M yyyy', showTrigger:'<img style="padding-left:3px;padding-right:3px;" src="images/calendar-blue.gif"/>', onSelect:this.reportDate_onChange.bindAsEventListener(this)});
			this._reportDate.onclick = this.btnreportDate_onClick.bindAsEventListener(this);
			
			var reportDateTo = table.find(".rangeReportDateTo");
			this._reportDateTo = reportDateTo.get(0);
			$(this._reportDateTo).datepick({dateFormat:'dd M yyyy', showTrigger:'<img style="padding-left:3px;padding-right:3px;" src="images/calendar-blue.gif"/>', onSelect:this.reportDate_onChange.bindAsEventListener(this)});
			this._reportDateTo.onclick = this.btnreportDate_onClick.bindAsEventListener(this);
			
			var that = this;
			$(".mainContentContainer").scroll(function() {
				$(that._reportDate).datepick('hide');
				$(that._reportDate).blur();
				$(that._reportDateTo).datepick('hide');
				$(that._reportDateTo).blur();
			});
			$(window).resize(function() {
				$(that._reportDate).datepick('hide');
				$(that._reportDate).blur();
				$(that._reportDateTo).datepick('hide');
				$(that._reportDateTo).blur();
			});
			
			var operationList = table.find(".RangeReport-operationList");
			this._rptOperationList = operationList.get(0);
			
			var checkSelectAll = table.find("#rangeSelectAllButton");
			this._checkSelectAll = checkSelectAll.get(0);
			this._checkSelectAll.onclick = this.checkSelectAll_onClick.bindAsEventListener(this);

			var checkUnselectAll = table.find("#rangeUnselectAllButton");
			this._checkUnselectAll = checkUnselectAll.get(0);
			this._checkUnselectAll.onclick = this.checkUnselectAll_onClick.bindAsEventListener(this);

			this._fieldEditor = table.get(0);
		}
		return this._fieldEditor;
	}
	
	RangeReportCustomField.prototype.refreshFieldEditor = function() {
		if (this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT)) {
			//note: setting .checked = "true" or .checked = "false" does nothing, need to be .checked = true / false
			if (this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT) == "true") {
				this._distinctReport.checked = true;
			} else if (this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT) == "false") {
				this._distinctReport.checked = false;
			} else if (this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT) != "") {
				//fallback
				this._distinctReport.checked = this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT);
			}
		}
		if (this._node.getFieldValue(RangeReportListener.FIELD_LATEST)){
			//note: setting .checked = "true" or .checked = "false" does nothing, need to be .checked = true / false
			if (this._node.getFieldValue(RangeReportListener.FIELD_LATEST) == "true") {
				this._latestReport.checked = true;
			} else if (this._node.getFieldValue(RangeReportListener.FIELD_LATEST) == "false") {
				this._latestReport.checked = false;	
			} else if (this._node.getFieldValue(RangeReportListener.FIELD_LATEST) != ""){
				//fallback
				this._latestReport.checked = this._node.getFieldValue(RangeReportListener.FIELD_LATEST); 
			}
		}
		if (this._node.getFieldValue(RangeReportListener.FIELD_START_DATE) || this._node.getFieldValue(RangeReportListener.FIELD_END_DATE)) {
			if (this._node.getFieldValue(RangeReportListener.FIELD_START_DATE)) {
				this._reportDate.value = this._node.getFieldValue(RangeReportListener.FIELD_START_DATE);	
				this._selectedStartDate = this._reportDate.value;
			}
			if (this._node.getFieldValue(RangeReportListener.FIELD_END_DATE)) {
				this._reportDateTo.value = this._node.getFieldValue(RangeReportListener.FIELD_END_DATE);
				this._selectedEndDate = this._reportDateTo.value;
			}
			this._selectReport.checked = true;
		}
		if (this._node.getFieldValue(RangeReportListener.FIELD_LATEST) || this._node.getFieldValue(RangeReportListener.FIELD_START_DATE) || this._node.getFieldValue(RangeReportListener.FIELD_END_DATE)){
			this.retrieveOperationList(true);

			var that = this;
			if (this._node.getFieldValue(RangeReportListener.FIELD_OPERATION)) {
				var uidArr = this._node.getFieldValue(RangeReportListener.FIELD_OPERATION).split(',');
				if (uidArr) {
					D3.forEach(uidArr, function(item) {
						if (that._checkboxes[item]) {
							that._checkboxes[item].checked = true;	
						}
					});	
				}
			}
		}
	}
	
	RangeReportCustomField.prototype.btnDistinctReport_onClick = function(){
		if (this._distinctReport.checked){
			this._node.setFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT, true);
		}else{
			this._node.setFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT, false);
		}
	}
	
	RangeReportCustomField.prototype.btnreportDate_onClick = function(){
		this._latestReport.checked = false;
		this._node.setFieldValue(RangeReportListener.FIELD_LATEST, false);
		this._selectReport.checked = true;
	}
	RangeReportCustomField.prototype.reportDate_onChange = function(){
		this._checkboxes = [];
		this.setDate();
		this.retrieveOperationList(false);
	}
	RangeReportCustomField.prototype.setDate = function(){
		var selected_dateFrom = this._reportDate.value;
		var selected_dateTo = this._reportDateTo.value;
		this._node.setFieldValue(RangeReportListener.FIELD_LATEST, false);
		this._node.setFieldValue(RangeReportListener.FIELD_START_DATE, selected_dateFrom);
		this._node.setFieldValue(RangeReportListener.FIELD_END_DATE, selected_dateTo);
		this._selectedStartDate = selected_dateFrom;
		this._selectedEndDate = selected_dateTo;
	}
	RangeReportCustomField.prototype.btnlatestReportCB_onClick = function(){
		this._selectReport.checked = false;
		this._reportDate.value=null;
		this._reportDateTo.value=null;
		this._latestReport.checked = true;
		
		this._node.setFieldValue(RangeReportListener.FIELD_START_DATE, "");
		this._node.setFieldValue(RangeReportListener.FIELD_END_DATE, "");
		this._node.setFieldValue(RangeReportListener.FIELD_OPERATION, null);
		if (this._latestReport.checked){
			this._node.setFieldValue(RangeReportListener.FIELD_LATEST, true);
		}else{
			this._node.setFieldValue(RangeReportListener.FIELD_LATEST, false);
		}
		this._checkboxes = [];
		this.retrieveOperationList(false);
	}
	
	RangeReportCustomField.prototype.btnreportDateCB_onClick = function(){
		this._latestReport.checked = false;
		this._selectReport.checked = true;
		this._node.setFieldValue(RangeReportListener.FIELD_LATEST, false);
		
		this.setDate();
		this.retrieveOperationList(false);
	}
	
	RangeReportCustomField.prototype.checkSelectAll_onClick = function(){
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = true;
		}
		this.operationSelected();
	}
	RangeReportCustomField.prototype.checkUnselectAll_onClick = function(){
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = false;
		}
		this.operationSelected();
	}
	
	RangeReportCustomField.prototype.btnOperation_onClick = function(){
		this.operationSelected();
	}
	RangeReportCustomField.prototype.operationSelected = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue(RangeReportListener.FIELD_OPERATION, id_list);
	}
	
	RangeReportCustomField.prototype.retrieveOperationList = function(refresh){
		this._lookupItems = this.getMultiSelectLookupReference();

		var container = this._rptOperationList;
		D3.UI.removeAllChildNodes(container);
		D3.forEach(this._lookupItems.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.checked = false;
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this);
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		if (refresh == false) {
			this.operationSelected();	
		}
	}
	
	RangeReportCustomField.prototype.getMultiSelectLookupReference = function(){
		
		var params = {};
		
		params._invokeCustomFilter = "flexDateRangeReportLoadOperationList";
		params.startDate = this._selectedStartDate;
		params.endDate = this._selectedEndDate;
		params.isLatest = this._latestReport.checked;
		params.operations = this._node.getFieldValue(RangeReportListener.FIELD_OPERATION);
		params.isDistinct = this._node.getFieldValue(RangeReportListener.FIELD_IS_DISTINCT_REPORT);
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		var doc = $(response);
		
		doc.find("op").each(function(item,index){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("code").text();
			newItem.label = item.find("label").text();
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}

	D3.inherits(RangeReportListener,D3.DefaultReportListener);
	function RangeReportListener()
	{
		D3.DefaultReportListener.call(this);
	}
	RangeReportListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "extdr_report_chooser"){
			return new RangeReportCustomField();
		}else{
			return RangeReportListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	RangeReportListener.FIELD_START_DATE = "@startDate";
	RangeReportListener.FIELD_END_DATE = "@endDate";
	RangeReportListener.FIELD_OPERATION = "@operations";
	RangeReportListener.FIELD_IS_DISTINCT_REPORT = "@isDistinctReport";
	RangeReportListener.FIELD_LATEST = "@isLatest";
	
	RangeReportListener.prototype.generateReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report","1");
		this._commandBeanProxy.submitForServerSideProcess(null, null, this._showAllDays);
		
		this._report_job_started = true;
	}
	
	D3.CommandBeanProxy.nodeListener = RangeReportListener;
})(window);