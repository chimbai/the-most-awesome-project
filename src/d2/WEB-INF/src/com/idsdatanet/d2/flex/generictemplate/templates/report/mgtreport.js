(function(window){
	D3.inherits(MgtReportCustomField,D3.AbstractFieldComponent);
	function MgtReportCustomField(){
		D3.AbstractFieldComponent.call(this);
		this._checkboxes = {};
		this._checkboxesOpsType = {};
		this._operationTypeLookupCount = 0;
		this._operationTypeLookup = null;
	}
	
	MgtReportCustomField.prototype.config = function(config) {
		this._useDateRange = false;
		this._selectedDate = MGTReportListener.LATEST_REPORT_FLAG;
		this._useDistinctReport = (config.useDistinctReport && config.useDistinctReport=="false" ? false: true);
		// operationTypesRestriction - use to restrict to view specific operation type of wells.
		this._operationTypesRestriction = (config.operationTypesRestriction ? config.operationTypesRestriction.split(",") : []);
		
		D3.forEach(this._operationTypesRestriction, function(item, i){
			this._operationTypesRestriction[i] = item.trim();
		}, this);
		
		this._selectedOperationTypes = this._operationTypesRestriction.join();
		if (config.useDateRange=="true"){
			this._useDateRange = true;
		}
	}
	
	MgtReportCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._operationTypeLookup = this._node.getLookup(MGTReportListener.FIELD_SELECTED_OPERATION_TYPE);
			this._selectedDate = this._node.getFieldValue(MGTReportListener.FIELD_SELECTED_DATE);
			if (this._operationTypeLookup!=null) this._operationTypeLookupCount = this._operationTypeLookup._items.length;
			var tableConfig = "<table cellspacing='0' cellpadding='0' width='100%'>" +
			"<tr><td width='25%' valign='top'>" +
			(this._useDistinctReport ?
			"<input id='distinctReport' type='checkbox' class='distinctReport'>" +
			"<label for='distinctReport' class='textBold'>"+D3.LanguageManager.getLabel("label.distinctReport")+"</label> <p>" +
			"<div style='border: 1px solid #eeeeee; padding: 1px;'>": "") +	
			"<input type='checkbox' id='latestReportCB' class='latestReportCB'>" +
			"<label for='latestReportCB' class='textBold'>"+D3.LanguageManager.getLabel("label.genLatestReport")+"</label><br>" +
			"<label class='textBold'>or </label><br>" +
			"<input type='checkbox' id='reportDateCB' class='reportDateCB'>" +
			"<label for='reportDateCB' class='textBold'>"+D3.LanguageManager.getLabel("label.selectReport")+"</label><br>" +
			"<input type='text' id='reportDate' class='reportDate' " +
			(this._useDateRange?"title='" + this.getToolTips() +"' ":"") +
			"readonly>" +
			(this._useDateRange?
			"<label class='textBold'>&nbsp;&nbsp;"+D3.LanguageManager.getLabel("label.to")+"&nbsp;&nbsp;</label>"+		
			"<input type='text' id='reportDateTo' class='reportDateTo' title='" + this.getToolTips() + "' readonly>" 	
			:"")+
			"<br>&nbsp;"+
			"</div>" +
			"</td>" +
			"<td width='5%'></td>" +
			(this._operationTypeLookupCount>0?
			"<td width='25%'>" +
			"<span class='fieldContainer'>" +
			"<div class=' multiselect dailySelector'>" +
			"<div class='multiselect-Title floatItem-Left'>"+D3.LanguageManager.getLabel("label.operationType")+"</div>" +
			"<div class='multiselect-ItemContainer floatItem-Left'>" +
			"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='MgtReport-operationTypeList'>" +
			"<li style='white-space: nowrap;'>" +
			"<li style='white-space: nowrap;'>" +
			"</ul>" +
			"<div class='multiselect-ButtonContainer'>" +
			"<input id='selectAllOpTypeCheckBox' type='checkbox' class='selectAllOpsType'>" +
			"<label for='selectAllOpTypeCheckBox'>"+D3.LanguageManager.getLabel("label.selectAllOpsType")+"</label>" +
			"</div></div></div>" +
			"</span></td>" 
			:"")+
			"<td width='25%'>" +
			"<span class='fieldContainer'>" +
			"<div class='multiselect dailySelector'>" +
			"<div class='multiselect-Title floatItem-Left'>"+D3.LanguageManager.getLabel("label.selectedWells")+"</div>" +
			"<div class='multiselect-MainContainer floatItem-Left'>" +
			"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='fieldEditor MgtReport-operationList'>" +
			"</ul>" +
			"<div class='multiselect-ButtonContainer'>" +
			"<input id='selectAllCheckBox' type='checkbox' class='selectAllOps'>" +
			"<label for='selectAllCheckBox'>"+D3.LanguageManager.getLabel("label.selectAllWells")+"</label>" +
			"</div></div></div>" +
			"</span></td>" +
			"<td width='5%'></td>" +
			"<td width='25%'>" +
			"<span class='fieldContainer'>" +
			"<div class='mgmtRptComment'>" +
			"<label for='mgmtRptComment' class='textBold floatItem-Left'>"+D3.LanguageManager.getLabel("label.mgmtRptComment")+"</label>" +
			"<textarea style='margin: 0px; padding: 0px;height:120px; width:300px; resize:none;' id='mgmtRptComment' class='fieldEditor mgmtRptComment'>" 
			"</div></span>" +
			"</td>" +
			"<td width='5%'></td>" +
			"</tr></table>";
			
			var table = $(tableConfig);
			
			if(this._useDistinctReport){
				var distinctReport = table.find(".distinctReport");
				this._distinctReport = distinctReport.get(0);
				this._distinctReport.onclick = this.btnDistinctReport_onClick.bindAsEventListener(this);
			}

			var latestReportCB = table.find(".latestReportCB");
			this._latestReport = latestReportCB.get(0);
			this._latestReport.onclick = this.btnlatestReportCB_onClick.bindAsEventListener(this);
			
			var reportDateCB = table.find(".reportDateCB");
			this._selectReport = reportDateCB.get(0);
			this._selectReport.onclick = this.btnreportDateCB_onClick.bindAsEventListener(this);
			
			var mgmtRptComment = table.find(".mgmtRptComment");
			this._mgmtRptComment = mgmtRptComment.get(0);
			this._mgmtRptComment.onchange = this.mgmtRptComment_onChange.bindAsEventListener(this);
			
			var reportDate = table.find(".reportDate");
			this._reportDate = reportDate.get(0);
			$(this._reportDate).datepick({dateFormat:'dd M yyyy', showTrigger:'<img style="padding-left:3px;padding-right:3px;" src="images/calendar-blue.gif"/>', onSelect:this.reportDate_onChange.bindAsEventListener(this)});
			this._reportDate.onclick = this.btnreportDate_onClick.bindAsEventListener(this);
			
			if (this._useDateRange){
				var reportDateTo = table.find(".reportDateTo");
				this._reportDateTo = reportDateTo.get(0);
				$(this._reportDateTo).datepick({dateFormat:'dd M yyyy', showTrigger:'<img style="padding-left:3px;padding-right:3px;" src="images/calendar-blue.gif"/>', onSelect:this.reportDate_onChange.bindAsEventListener(this)});
				this._reportDateTo.onclick = this.btnreportDate_onClick.bindAsEventListener(this);
			}
			
			var that = this;
			$(".mainContentContainer").scroll(function() {
				$(that._reportDate).datepick('hide');
				$(that._reportDate).blur();
				if (that._useDateRange) {
					$(that._reportDateTo).datepick('hide');
					$(that._reportDateTo).blur();
				}
			});
			$(window).resize(function() {
				$(that._reportDate).datepick('hide');
				$(that._reportDate).blur();
				if (that._useDateRange) {
					$(that._reportDateTo).datepick('hide');
					$(that._reportDateTo).blur();
				}
			});

			var operationList = table.find(".MgtReport-operationList");
			this._mgtOperationList = operationList.get(0);
			
			var checkSelectAll = table.find(".selectAllOps");
			this._checkSelectAll = checkSelectAll.get(0);
			this._checkSelectAll.onclick = this.checkSelectAll_onClick.bindAsEventListener(this);
						
			if (this._operationTypeLookupCount>0){
				var operationTypeList = table.find(".MgtReport-operationTypeList");
				this._mgtOperationTypeList = operationTypeList.get(0);
				this.retrieveOperationTypeList();
				
				var checkSelectAllOpsType = table.find(".selectAllOpsType");
				this._selectAllOpTypeCheckBox = checkSelectAllOpsType.get(0);
				this._selectAllOpTypeCheckBox.onclick = this.checkSelectAllOpsType_onClick.bindAsEventListener(this)

			}
			this._fieldEditor = table.get(0);
			this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_DATE, MGTReportListener.LATEST_REPORT_FLAG);
			this._node.setFieldValue(MGTReportListener.FIELD_DISTINCT_REPORT, false);
			
			if (this._selectedDate == MGTReportListener.LATEST_REPORT_FLAG){
				this.btnlatestReportCB_onClick();
			}else if (this._selectedDate == ""){
				this.btnlatestReportCB_onClick();
			}else{
				this._reportDate.value = this._selectedDate;
				this.btnreportDateCB_onClick();
			}
		}
		return this._fieldEditor;
	}
	
	MgtReportCustomField.prototype.btnDistinctReport_onClick = function(){
		if (this._distinctReport.checked){
			this._node.setFieldValue(MGTReportListener.FIELD_DISTINCT_REPORT, true);
		}else{
			this._node.setFieldValue(MGTReportListener.FIELD_DISTINCT_REPORT, false);
		}
	}
	MgtReportCustomField.prototype.btnreportDate_onClick = function(){
		this._latestReport.checked = false;
		this._selectReport.checked = true;
	}
	MgtReportCustomField.prototype.reportDate_onChange = function(){
		this.setDate();
		this.retrieveOperationList();
	}
	MgtReportCustomField.prototype.mgmtRptComment_onChange = function(){
		var comment = $.trim($("#mgmtRptComment").val());
		var nullComment = "";
		if(comment != null) {
			this._node.setFieldValue(MGTReportListener.FIELD_COMMENT, comment);
		}else {
			this._node.setFieldValue(MGTReportListener.FIELD_COMMENT, nullComment);
		}
		
	}
	MgtReportCustomField.prototype.setDate = function(){
		if (this._useDateRange){
			var selected_dateFrom = this._reportDate.value;
			var selected_dateTo = this._reportDateTo.value;
			this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_DATE, selected_dateFrom + "," + selected_dateTo);
			this._selectedDate = selected_dateFrom + "," + selected_dateTo;
		}else{
			this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_DATE, this._reportDate.value);
			this._selectedDate = this._reportDate.value;
		}
	}
	MgtReportCustomField.prototype.btnlatestReportCB_onClick = function(){
		this._selectReport.checked = false;
		this._reportDate.value=null;
		this._latestReport.checked = true;
		this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_DATE, MGTReportListener.LATEST_REPORT_FLAG);

		this._selectedDate = MGTReportListener.LATEST_REPORT_FLAG;
		this.retrieveOperationList();
		
		var comment = $.trim($("#mgmtRptComment").val());
		this._node.setFieldValue(MGTReportListener.FIELD_COMMENT, comment);
	}
	
	MgtReportCustomField.prototype.btnreportDateCB_onClick = function(){
		this._latestReport.checked = false;
		this._selectReport.checked = true;	
		
		this.setDate();
		this.retrieveOperationList();
		
		var comment = $.trim($("#mgmtRptComment").val());
		this._node.setFieldValue(MGTReportListener.FIELD_COMMENT, comment);
	}
	
	MgtReportCustomField.prototype.btnselectOpsTAll_onClick = function(check){
		for (var key in this._checkboxesOpsType) {
			this._checkboxesOpsType[key].checked = check;
		}
		this.operationTypeChanged();
	}
	MgtReportCustomField.prototype.checkSelectAllOpsType_onClick = function(){
		var check = false;
		if (this._selectAllOpTypeCheckBox.checked) {
			check = true;
		}
		for (var key in this._checkboxesOpsType) {
			this._checkboxesOpsType[key].checked = check;
		}
		this.operationTypeChanged();
	}
	MgtReportCustomField.prototype.checkSelectAll_onClick = function(){
		var check = false;
		if (this._checkSelectAll.checked) {
			check = true;
		}
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = check;
		}
		this.operationSelected();
	}
	MgtReportCustomField.prototype.btnOperation_onClick = function(){
		this.operationSelected();
	}
	MgtReportCustomField.prototype.btnOperationType_onClick = function(){
		this.operationTypeChanged();
	}
	MgtReportCustomField.prototype.operationSelected = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_OPERATIONS, id_list);
	}
	MgtReportCustomField.prototype.operationTypeChanged = function(){
		var selectedOperationTypes = "";
		for (var key in this._checkboxesOpsType) {
			if (this._checkboxesOpsType[key].checked){
				if(selectedOperationTypes == ""){
					selectedOperationTypes = key;
				}else{
					selectedOperationTypes += "," + key;
				}
			}
		}
		this._node.setFieldValue(MGTReportListener.FIELD_SELECTED_OPERATION_TYPE, selectedOperationTypes);
		this._selectedOperationTypes = (selectedOperationTypes == "" ? this._operationTypesRestriction.join() : selectedOperationTypes);
		this.retrieveOperationList();
	}
	
	MgtReportCustomField.prototype.getToolTips = function(){
		var maxAllowsDays = this._node.getFieldValue("@daysLimit");
		var msg = D3.LanguageManager.getLabel("label.maxDayMsg");
		return msg.replace("$(maxAllowsDays)", maxAllowsDays); 			
	}
	
	MgtReportCustomField.prototype.retrieveOperationTypeList = function(){
		var dxx = this._node.getFieldValue(MGTReportListener.FIELD_SELECTED_OPERATION_TYPE);
		var containerType = this._mgtOperationTypeList;
		var len = this._operationTypesRestriction.length;
		D3.UI.removeAllChildNodes(containerType);
		this._checkboxesOpsType = [];
		D3.forEach(this._operationTypeLookup._items, function(item) {
			if (item && !item.firstBlankItem) {	
				if(len > 0 && this._operationTypesRestriction.indexOf(item.data) == -1){
					return;
				}

				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxesOpsType[item.data] = cb;
				this._checkboxesOpsType[item.data].onclick = this.btnOperationType_onClick.bindAsEventListener(this);
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				li.appendChild(label);
				containerType.appendChild(li);
			}
		}, this);
		this.operationSelected();
	}
	
	MgtReportCustomField.prototype.retrieveOperationList = function(){
		this._lookupItems = this.getMultiSelectLookupReference();

		var container = this._mgtOperationList;
		D3.UI.removeAllChildNodes(container);
		this._checkboxes = [];
		D3.forEach(this._lookupItems.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				if (this._node.getGWP("mgtReportAutoSelectAllOperation")=="1"){
					cb.setAttribute("checked", true);
				}else{
					cb.setAttribute("checked", false);
				}
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this);
				li.appendChild(cb);

				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		this.operationSelected();
	}
	
	MgtReportCustomField.prototype.getMultiSelectLookupReference = function(){
		
		var params = {};
		
		params._invokeCustomFilter = "flexMgtReportLoadOperationList";
		params.reportDate = this._selectedDate;
		params.operationType = this._selectedOperationTypes;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		var doc = $(response);
		
		var currentSelected = this._node.getFieldValue(MGTReportListener.FIELD_SELECTED_OPERATIONS);
		var currentList = null;
		
		doc.find("Operation").each(function(item,index){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("operationUid").text();
			newItem.label = item.find("operationName").text();
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}

	D3.inherits(MGTReportListener,D3.DefaultReportListener);
	function MGTReportListener()
	{
		D3.DefaultReportListener.call(this);
	}
	MGTReportListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "mgt_report_chooser"){
			return new MgtReportCustomField();
			//return new OperationSelectorField();
		}else{
			return MGTReportListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	MGTReportListener.FIELD_SELECTED_DATE = "@mgtSelectedDate";
	MGTReportListener.FIELD_SELECTED_OPERATIONS = "@mgtSelectedOperations";
	MGTReportListener.FIELD_DISTINCT_REPORT = "@mgtDistinctReport";
	MGTReportListener.LATEST_REPORT_FLAG = "latestReport";
	MGTReportListener.FIELD_SELECTED_OPERATION_TYPE = "@mgtSelectedOperationType";
	MGTReportListener.FIELD_COMMENT = "@mgmtRptComment";
	
	MGTReportListener.prototype.generateReport = function(){
		
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report","1");
		this._commandBeanProxy.submitForServerSideProcess(null, null, this._showAllDays);
		this._report_job_started = true;
	}
	
	D3.MGTReportListener = MGTReportListener;
	D3.CommandBeanProxy.nodeListener = MGTReportListener;
})(window);