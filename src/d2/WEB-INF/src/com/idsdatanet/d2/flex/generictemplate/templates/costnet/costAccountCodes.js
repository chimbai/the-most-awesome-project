(function(window){

	D3.inherits(ReportSummarySubmitField, D3.AbstractFieldComponent);
	
	function ReportSummarySubmitField(id) {
		D3.AbstractFieldComponent.call(this);
	}
	
	ReportSummarySubmitField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {

			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Search",
				css:"DefaultButton",
				attributes:{
					title:"Search"
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var accountCodeFilter = this._node.getFieldValue("@accountCodeFilter");

							this._node.setFieldValue("@accountCodeFilter",accountCodeFilter);
							this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node);
								
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReportSummarySubmitField.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(CostAccountCodesNodeListener,D3.EmptyNodeListener);
	
	function CostAccountCodesNodeListener() {
	}
	
	CostAccountCodesNodeListener.prototype.customButtonTriggered = function(node, id, button){
		this._node = node;
		if (id == "search_code") {
			this.searchCodePopup(node, id, button);
		}
	};
	
	CostAccountCodesNodeListener.prototype.searchCodePopup = function(node, id, button){
		this._node = node;
		var code = prompt("Search Code :", "");
		this._node.setFieldValue("@accountCodeFilter",code);
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node, "@accountCodeFilter");
	}

	CostAccountCodesNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (id == "exportCode") {
			this._commandBeanProxy = commandBeanProxy;
			
			this._btneExportData = document.createElement("button");
			this._btneExportData.className = "rootButtons";
			var label = "Export Data";
			var addNew = document.createTextNode(label);
			this._btneExportData.appendChild(addNew);
			this._btneExportData.onclick = this.exportData.bindAsEventListener(this);
			
			return this._btneExportData;
		}else if (id == "searchCode") {
			this._commandBeanProxy = commandBeanProxy;
			
			this._btnSearchData = document.createElement("button");
			this._btnSearchData.className = "rootButtons";
			var label = "Search Code";
			var addNew = document.createTextNode(label);
			this._btnSearchData.appendChild(addNew);
			this._btnSearchData.onclick = this.searchDatas.bindAsEventListener(this);
			
			if (commandBeanProxy.rootNode.getFieldValue("@username")=="jwong"){
				return this._btnSearchData;
			}
		}
		
	}
	
	CostAccountCodesNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "search_code") {
			btn = $("<button class='DefaultButton' style='margin:3px'>Search</button>");
			return btn.get(0);
		} else {
			return null;
		}
	};
	
	CostAccountCodesNodeListener.prototype.searchDatas = function(event) {
		var code = prompt("Search Code :", "");
		this._node = this._commandBeanProxy.rootNode;
		this._node.setFieldValue("test",code);
		this._node.setFieldValue("@accountCodeFilter",code);
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node, "@accountCodeFilter");
	}
	
	CostAccountCodesNodeListener.prototype.exportData = function(event) {
		var costAccountCodes = this._commandBeanProxy.rootNode.children["CostAccountCodes"].getItems();
		alert("XX");
		var xx = "";
	}
	
	CostAccountCodesNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id == "filterbutton"){
			return new ReportSummarySubmitField();
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = CostAccountCodesNodeListener;
	
})(window);