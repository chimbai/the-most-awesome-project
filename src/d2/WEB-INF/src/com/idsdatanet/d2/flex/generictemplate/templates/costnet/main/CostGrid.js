
(function(window){
	
	function CostGrid(){
		this.costAfeMaster = [];
		this.costAfeDetails = [];

		this.modifiedRecords = [];
		this.deletedRecords = [];
		this.baseUrl = "";
		this.rootBox = "";
		this.afeNoBox = "";
		this.currentAction = "";
		this.primaryCurrency = "";
		this.currentUser = "";
	}
	
	CostGrid.prototype.init = function (options){

	}
	
	CostGrid.prototype.freezeChanged = function(){
		if ($("#freezeCheckBox")[0].checked==true) {
			this.afeGrid.jqGrid('setFrozenColumns');
		}else{
			this.afeGrid.jqGrid('destroyFrozenColumns');
		}
	}
	
	CostGrid.prototype.getCurrentGridWidth = function(){
		var n = this.afeGrid.get(0).parentNode;
		while(n){
			if(n.className && n.className.indexOf("ui-jqgrid-bdiv") != -1){
				return n.offsetWidth;
			}
			n = n.parentNode;
		}
		return 0;
	}
	
	CostGrid.prototype.groupingChanged = function(){
		var currentWidth = this.getCurrentGridWidth();
		var grouppingType = this.groupCat.value;
		if (grouppingType=="Category"){
			var checkf = this.afeGrid.jqGrid('getGridParam').colModel.find(x => x.name=== 'subcategory');
			if (checkf == undefined){
			
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','category'], {
		           // groupOrder : ['desc'],
					groupText : ['<b>{0}</b>','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
		            groupColumnShow: [false, false],
		            groupCollapse: false, 
		     //       groupSummary: [false, true],
		     //       groupSummaryPos: ['header', 'header'],
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });
			}else{
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','category','subcategory'], {
			           // groupOrder : ['desc'],
						groupText : ['<b>{0}</b>','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
			            groupColumnShow: [false, false, false],
			            groupCollapse: false, 
			     //       groupSummary: [false, true],
			     //       groupSummaryPos: ['header', 'header'],
			            formatDisplayField: [
			                function (value) {
			                	if (value=="true") return "Tangible";
			                	if (value=="false") return "Intangible";
			                	return value;
			                }
			             ]
			        });
			}
		}else if (grouppingType=="AccCode"){
			this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','accountCode'], {
	            // groupOrder : ['desc'],
				groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
	            groupColumnShow: [false, true],
	            groupCollapse: false,
	            formatDisplayField: [
	                function (value) {
	                	if (value=="true") return "Tangible";
	                	if (value=="false") return "Intangible";
	                	return value;
	                }
	             ]
	        });
		}else if (grouppingType=="AccGroup"){
			var checkf = this.afeGrid.jqGrid('getGridParam').colModel.find(x => x.name=== 'afeGroupName');
			var checkdaily = this.afeGrid.jqGrid('getGridParam').colModel.find(x => x.name=== 'companyName');

			if (checkf == undefined){
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible'], {
		            // groupOrder : ['desc'],
					groupText : ['<b>{0} </b> '],
		            groupColumnShow: [false],
		            groupCollapse: false,
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });
			}else if (this.afeType=="PRICELIST" || this.afeType=="MASTER" || this.afeType=="SUPPLEMENTARY"){
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','afeGroupName'], {
		            // groupOrder : ['desc'],
					groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
		            groupColumnShow: [false, true],
		            groupCollapse: false,
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });
			}else if (checkdaily!=undefined){
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','afeGroupName'], {
		            // groupOrder : ['desc'],
					groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total: <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>  Used: <b>'+this.primaryCurrency +' {afeUsed}</b>  Balance: <b>'+this.primaryCurrency+' {afeBalance}</b>'],
		            groupColumnShow: [false, true],
		            groupCollapse: false,
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });	
			}else{
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','afeGroupName'], {
		            // groupOrder : ['desc'],
					groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
		            groupColumnShow: [false, true],
		            groupCollapse: false,
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });
			};
		}else if (grouppingType=="PlanPhase"){
			this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','planReferenceName'], {
	            // groupOrder : ['desc'],
				groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
	            groupColumnShow: [false, true],
	            groupCollapse: false,
	            formatDisplayField: [
	                function (value) {
	                	if (value=="true") return "Tangible";
	                	if (value=="false") return "Intangible";
	                	return value;
	                }
	             ]
	        });
		}else if (grouppingType=="Vendor"){
			var checkf = this.afeGrid.jqGrid('getGridParam').colModel.find(x => x.name=== 'lookupCompanyName');
			if (checkf != undefined){
			this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','lookupCompanyName'], {
	            // groupOrder : ['desc'],
				groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
	            groupColumnShow: [false, true],
	            groupCollapse: false,
	            formatDisplayField: [
	                function (value) {
	                	if (value=="true") return "Tangible";
	                	if (value=="false") return "Intangible";
	                	return value;
	                }
	             ]
	        });
			}else{
			checkf = this.afeGrid.jqGrid('getGridParam').colModel.find(x => x.name=== 'companyName');
			if (checkf != undefined){
			this.afeGrid.jqGrid('groupingGroupBy', ['isTangible','companyName'], {
	            // groupOrder : ['desc'],
				groupText : ['<b>{0} </b> ','<b>{0} - </b>  Total : <b>' + this.primaryCurrency +' {itemTotalInPrimaryCurrency}</b>'],
	            groupColumnShow: [false, true],
	            groupCollapse: false,
	            formatDisplayField: [
	                function (value) {
	                	if (value=="true") return "Tangible";
	                	if (value=="false") return "Intangible";
	                	return value;
	                }
	             ]
	        });
			}else{
				this.afeGrid.jqGrid('groupingGroupBy', ['isTangible'], {
		            // groupOrder : ['desc'],
					groupText : ['<b>{0} </b> '],
		            groupColumnShow: [false],
		            groupCollapse: false,
		            formatDisplayField: [
		                function (value) {
		                	if (value=="true") return "Tangible";
		                	if (value=="false") return "Intangible";
		                	return value;
		                }
		             ]
		        });
				
			}
		}
			
		}else{
			this.afeGrid.jqGrid('groupingRemove');
		}
		if(currentWidth > 0){
			this.afeGrid.setGridWidth(currentWidth);
		}else if (this.option.width!=undefined) {
			this.afeGrid.setGridWidth( this.option.width);
			this.afeGrid.jqGrid({
                autowidth: true,
                shrinkToFit: true
            });
		}
	}
	
	CostGrid.prototype.fieldOnChange = function(){
		this.show(this.rootBox);
		this.disableItems(true);
	}
	
	CostGrid.prototype.getHtmlEncode = function(value){
		if (value==null) return "";
		value = value.replace(/&/g, "&amp;")
		value = value.replace(/"/g, "&quot;")
		value = value.replace(/'/g, "&#39;")
		value = value.replace(/\//g, "&#47;")
		value = value.replace(/</g, "&lt;")
		value = value.replace(/>/g, "&gt;");
		return value;
	}
		
	CostGrid.prototype.yesNoFormatter = function(cellValue, options, rowObject){
		return (cellValue=="true" ? "Yes" : "No");
	}
	
	CostGrid.prototype.yesNoUnformatter = function(cellValue, options, cell){
		return (cellValue == "Yes" ? true : false);
	}
	
	CostGrid.prototype.getCodeName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;
		var n = unformatValue.indexOf("::");
		if (n<=0){
			unformatValue = unformatValue + " :: "+ rowObject["shortDescription"];
		}

	    return unformatValue;
	}
	
	CostGrid.prototype.getVendorName = function(cellValue, options, rowObject){
		if (cellValue==undefined) return "";
		var unformatValue = cellValue;

	    $.each(options.colModel.editoptions.value, function (k, value)
	    {
	        if (cellValue == k)
	        {
	            unformatValue = value;
	        }
	    });

	    return unformatValue;
	}
	
	CostGrid.prototype.calTotal = function(value, name, record){
		var rate = record.currencyRate;
		if (isNaN(rate)) rate = 1.0;
		var total = record.itemCost * record.estimatedDays * record.quantity;
		total = total / rate; 
		const  fixedNumber = Number.parseFloat(total).toFixed(2);
	    return String(fixedNumber).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	CostGrid.prototype.sumGroupTotal = function(value, name, record){
		return value += record[name];
	}
	
	CostGrid.prototype.show = function(value) {
		if(value) value.style.display = "";
	};

	CostGrid.prototype.hide = function(value) {
		if(value) value.style.display = "none";
	};
	
	CostGrid.prototype.closeDialog = function(){
		$(this.dialog).dialog( "close" );
	};
	
	CostGrid.prototype.closeCSVDialog = function(){
		$(this.cvsDialog).dialog( "close" );
	};
	
	CostGrid.prototype.numberWithCommas = function (num) {
		var options = { 
				  minimumFractionDigits: 2,
				  maximumFractionDigits: 2 
				};
		// https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_tolocalestring_num_all
		var formatted = Number(num).toLocaleString(this.option.lang, options);
		return formatted;
	};
	
	CostGrid.prototype.getformattednumber = function(cellValue, options, rowObject){
		return this.numberWithCommas(cellValue);
	}
	
	CostGrid.prototype.getunformattednumber = function(cellValue, options, rowObject){
		return this.unformatString(cellValue);
	}
	
	CostGrid.prototype.unformatString = function (values) {
		if (typeof values=="number") return values;
		if (values=="") return 0.0;
	    var parts = (1234.5).toLocaleString(this.option.lang).match(/(\D+)/g);
	    var unformatted = values;

	    unformatted = unformatted.split(parts[0]).join("");
	    unformatted = unformatted.split(parts[1]).join(".");

	    return parseFloat(unformatted);

	}
	
	CostGrid.prototype.sumfunc = function(val, name, record)
	{
		var vals = this.unformatString(val);
		return this.numberWithCommas(parseFloat(vals||0) + parseFloat((record[name]||0)));
	}
	
	CostGrid.prototype.disableItems = function(value){
		var n = (this.jqGridMain ? $(this.jqGridMain).find(".ui-search-toolbar") : $(".ui-search-toolbar")); 
		if (value==true){
			n.hide();
		}else{
			n.show();
		}
		this.enableEditFields(value);
	}
	
	CostGrid.prototype.enableEditFields = function(value){
		
	}
	
	CostGrid.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	CostGrid.prototype.exportToExcel = function(fileName){
		this.afeGrid.jqGrid("exportToExcel",{
			includeLabels : true,
			includeGroupHeader : true,
			includeFooter: true,
			fileName : fileName + ".xlsx",
			maxlength : 40 // maxlength for visible string data 
		});
	}
	
	CostGrid.prototype.exportToPdf = function(fileName, titles){
		this.afeGrid.jqGrid("exportToPdf",{
			title: titles,
			orientation: 'landscape',
			pageSize: 'A4',
			description: '',
			customSettings: null,
			download: 'download',
			includeLabels : true,
			includeGroupHeader : true,
			includeFooter: true,
			fileName : fileName + ".pdf"
	});
	}
	
	CostGrid.prototype.checkField = function(dataField, headerText){
		var model = {};
		model.name = dataField;
		
		switch(dataField){
			case "sequence":
				if (headerText=="") headerText = "Seq #"; //text
				model.label = headerText;
				model.width = 60;
				model.sortable = true;
				model.sorttype = "number";
				model.editable = true; 
				model.editoptions = {autocomplete: "off",maxlength:4};
				model.edittype = 'text';
				model.align = "center";
				model.frozen = true;
				break;
			case "isTangible": //label, not editable
				if (headerText=="") headerText = "Tangible?";
				model.label = headerText;
				model.width = 60;
				model.sortable = true;
				model.editable = false; 
				model.align = "center";
				model.frozen = true;
				break;
			case "category": //text, not editable
				if (headerText=="") headerText = "Category";
				model.label = headerText;
				model.width = 50;
				model.sortable = true;
				model.editable = false;
				model.frozen = true;
				break;
			case "subcategory": //text, not editable
				if (headerText=="") headerText = "Sub Category";
				model.label = headerText;
				model.width = 50;
				model.sortable = true;
				model.editable = false;
				model.frozen = true;
				break;
			case "accountCode": //lookup
				if (headerText=="") headerText = "Account Code";
				model.width = 300;
				model.sortable = true;
				model.editable = true;
	//			model.edittype = "select";
				model.frozen = true;
				model.searchoptions = {
					sopt:['cn','eq','bw','bn','nc','ew','en']
				};
				model.edittype = "select";
				model.editoptions = {
					value: this.accountCodeList,
					style: "width:100%" 
//					dataInit: function (element) {
//                   	 	$(element).select2();
//                    }
				};
//				model.editoptions=  {
//					 value: this.accountCodeList,	
//                     dataInit: function (element) {
//                    	 $(element).select2();
//                     }
//                 };
//				model.formatter = this.getCodeName;
				break;
			case "afeItemDescription":
			case "itemDescription": 
				if (headerText=="") headerText = "Description";
				model.width = 200;
				model.sortable = true;
				model.editable = true;
				model.editoptions = {autocomplete: "off"};
				model.edittype = 'text';
				break;
			case "longDescription": 
				if (headerText=="") headerText = "Comment";
				model.width = 200;
				model.sortable = true;
				model.editable = true;
				model.editoptions = {autocomplete: "off"};
				model.edittype = 'text';
				break;
			case "companyName": //lookup
			case "lookupCompanyName":
				if (headerText=="") headerText = "Vendor";
		//		model.name = "lookupCompanyUid";		
				model.width = 250;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.vendorList,
					style: "width:100%" 
				};
		//		model.formatter = this.getVendorName;
				break;
			case "afeGroupName":
				if (headerText=="") headerText = "AFE Group";	
				model.width = 150;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.afeGroupList,
					style: "width:100%" 
				};
				break;	
			case "planReference":
				model.name = "planReferenceName";
				if (headerText=="") headerText = "Plan Phase";	
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.planRefereList,
					style: "width:100%" 
				};
				break;	
			case "referenceNumber": //text
				if (headerText=="") headerText = "Reference Number";
				model.width = 200;
				model.sortable = true;
				model.editable = true;
				model.edittype = 'text';
				break;
			case "recurringItem": //lookup
				if (headerText=="") headerText = "Recurring?";
				model.width = 50;
				model.sortable = true;
				model.editable = true;
				model.edittype = 'select';
				model.align = "center";
				model.editoptions= {
					value:{"true": "Yes", "false": "No"}
					};
			//	model.formatter = "select";
			//	model.formatter = this.yesNoFormatter;
				break;
			case "quantity": //text
				if (headerText=="") headerText = "Qty";
				model.width = 70;
				model.sortable = true;
				model.sorttype = "float";
				model.editable = true;
				model.editoptions = {autocomplete: "off"};
				model.align = "center";
		//		model.editoptions.defaultValue = 1; // default value to set to editor in editoptions
		//		model.editrules={number:true};
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
			
				break;
			case "itemUnit": //lookup
				if (headerText=="") headerText = "UOM";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.align = "center";
				model.editoptions = {
					value: this.lookupList,
					style: "width:100%" 
				};
				model.formatter = this.getBasicLookName;
				break;
			case "estimatedDays": //text
				if (headerText=="") headerText = "Qty/Day";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.editoptions = {autocomplete: "off"};
				model.editrules={number:true};
				model.align = "center";
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;
			case "className": //lookup
				if (headerText=="") headerText = "cls";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.clsCodeList,
					style: "width:100%" 
				};
				break;
			case "phaseName": //lookup
				if (headerText=="") headerText = "Phs";
				model.width = 100;
				model.sortable = true;
				model.editable = false;
				model.edittype = "select";
				model.editoptions = {
					value: this.phsCodeList,
					style: "width:100%" 
				};
				break;
			case "taskName": //lookup
				if (headerText=="") headerText = "Ops";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.opsCodeList,
					style: "width:100%" 
				};
				break;
			case "rootCauseName": //lookup
				if (headerText=="") headerText = "NPT";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.editoptions = {
					value: this.rcCodeList,
					style: "width:100%" 
				};
				break;
			case "holeSizeName": //lookup
				if (headerText=="") headerText = "Hole Size";
				model.width = 100;
				model.sortable = true;
				model.editable = true;
				break;
			case "currency": //lookup
				if (headerText=="") headerText = "Curr";
				model.width = 50;
				model.sortable = true;
				model.editable = true;
				model.edittype = "select";
				model.align = "center";
				model.editoptions = {
					value: this.currencyList,
					style: "width:100%" 
				};
//				model.stype = "select";
//				model.searchoptions = {
//	                "sopt": [ "eq", "ne" ],
//	                "value": this.currencyList,
//	            }
		//		model.formatter = this.getBasicLookName;
				break;
			case "itemCost": //text
				if (headerText=="") headerText = "Unit Price";
				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.editable = true;
				model.editoptions = {autocomplete: "off"};
				model.align = "right";
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
//				model.editrules={
//						number:true,
//						};
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;
			case "itemTotal": //text, not editable
				if (headerText=="") headerText = "Total";
				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.align = "right";
			//	model.formatter = "number";
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
		//		model.summaryType = 'sum';
		//		model.summaryRoun= 2; numberWithCommas
		//		model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;
			case "itemTotalInPrimaryCurrency": //text, not editable
				if (this.afeType=="PRICELIST") headerText = "Unit Price";
				if (headerText=="") headerText = "Total (" + this.primaryCurrency + ")";
				else headerText = headerText + " (" + this.primaryCurrency + ")";
				if (this.afeType=="SO") headerText = "SO Total" + " (" + this.primaryCurrency + ")";

				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.align = "right";
			//	model.formatter = this.calTotal;
				model.summaryTpl = "Total : {0}";
			//	model.summaryType = 'sum';
				model.summaryType = this.sumfunc.bind(this);
				model.summaryRound = 2;
				model.summaryRoundType = 'round';            // 'fixed'
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
//				model.formatter = function (cellval, opts, rwdat, act) {
//				    if (opts.rowId === "") {
//				        if (cellval > 1000) {
//				            return '<span style="color:green">' +
//				                $.fn.fmatter('number', cellval, opts, rwdat, act) +
//				                '</span>';
//				        } else {
//				            return '<span style="color:red">' +
//				                $.fn.fmatter('number', cellval, opts, rwdat, act) +
//				                '</span>';
//				        }
//				    } else {
//				        return $.fn.fmatter('number', cellval, opts, rwdat, act);
//				    }
//				},
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;
			case "quantityTotal":
				if (headerText=="") headerText = "Qty Total";
				model.label = headerText;
				model.width = 60;
				model.sortable = true;
				model.editable = false; 
				model.align = "center";
				model.frozen = true;
				break;
			case "quantityUsed":
				if (headerText=="") headerText = "Qty Used";
				model.label = headerText;
				model.width = 60;
				model.sortable = true;
				model.editable = false; 
				model.align = "center";
				model.frozen = true;
				break;
			case "quantityBalance":
				if (headerText=="") headerText = "Qty Balance";
				model.label = headerText;
				model.width = 60;
				model.sortable = true;
				model.editable = false; 
				model.align = "center";
				model.frozen = true;
				break;	
			case "afeUsed":
				
				if (headerText=="") headerText = "AFE Used (" + this.primaryCurrency + ")";
				else headerText = headerText + " (" + this.primaryCurrency + ")";
				if (this.afeType=="SO") headerText = "SO Used"+ " (" + this.primaryCurrency + ")";
				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.align = "right";
			//	model.formatter = this.calTotal;
				model.summaryTpl = "Total : {0}";
			//	model.summaryType = 'sum';
				model.summaryType = this.sumfunc.bind(this);
				model.summaryRound = 2;
				model.summaryRoundType = 'round';            // 'fixed'
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};

				break;
			case "afeBalance":
				if (headerText=="") headerText = "AFE Balance (" + this.primaryCurrency + ")";
				else headerText = headerText + " (" + this.primaryCurrency + ")";
				if (this.afeType=="SO") headerText = "SO Balance"+ " (" + this.primaryCurrency + ")";

				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.align = "right";
			//	model.formatter = this.calTotal;
				model.summaryTpl = "Total : {0}";
			//	model.summaryType = 'sum';
				model.summaryType = this.sumfunc.bind(this);
				model.summaryRound = 2;
				model.summaryRoundType = 'round';            // 'fixed'
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;
			case "itemCostInPrimaryCurrency":
				if (headerText=="") headerText = "Item Cost (" + this.primaryCurrency + ")";
				else headerText = headerText + " (" + this.primaryCurrency + ")";
//				if (this.afeType="SO") headerText = "SO Total";

				model.width = 100;
				model.sortable = true;
				model.sorttype = "float";
				model.align = "right";
			//	model.formatter = this.calTotal;
				model.summaryTpl = "Total : {0}";
			//	model.summaryType = 'sum';
				model.summaryType = this.sumfunc.bind(this);
				model.summaryRound = 2;
				model.summaryRoundType = 'round';            // 'fixed'
				model.formatter = this.getformattednumber.bind(this);
				model.unformat = this.getunformattednumber.bind(this);
				model.formatoptions = {decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'};
				break;		
			default: 
				if (headerText=="") headerText = dataField;
				model.width = 100;
		}
		model.label = headerText;
		
		if(model.editable /*&& model.edittype == 'text'*/){
		//	model.editoptions = model.editoptions || {};
		//	model.editoptions.dataInit = function(){/*console.log("enter eventoptions.dataInit")*/};
			model.editoptions.dataEvents = [{ //not trigger change event when edit and hit enter (exit from edit mode, with jqGrid editRow key= true)
				type: 'change',
				fn: this.onDataFieldChange.bind(this)
			},{
				type: 'keydown', //to cater case where edit and hit enter to exit from edit mode, only keydown listen to Enter key, keyup + keypress cannot trigger Enter key
				fn: this.onDataFieldChange.bind(this)
			}];
		}
		
		return model;
	}

	CostGrid.prototype.getProgressStatus = function(enable, progress, text){  
		var divTemplate = "<div id='loadingDialog' title='Loading'>" +
			"<div id='progressbar'><div class='progress-label'></div></div>" +
		"</div>" ;
		if (this.laodbar==undefined) {
			this.laodbar = $(divTemplate).get(0);
		}
		var progressbar = $(this.laodbar).find("#progressbar").get(0);
		var progressLabel = $(this.laodbar).find(".progress-label").get(0);
		
		progressLabel.textContent = text;
		
		if (enable){
			$( this.laodbar ).dialog({
				dialogClass: 'no-close' ,
				closeonEscape:false,
		        autoOpen: false,
		        closeText: "",
		        modal: true,
		        height: 80,
		        width: 500, 
			 });
			$(this.laodbar).dialog( "open" );
			this.loadbar_open = true;
			
			$(progressbar).progressbar({
		        value: false,
		        change: function() {
		        	var progressLabel = $(this).find(".progress-label").get(0);
		        	if (progressLabel.textContent!=""){
		        		progressLabel.textContent = text + " - "  + $(this).progressbar("value") + "%";
		        	}else{
		        		progressLabel.textContent = $(this).progressbar("value") + "%";
		        	}
		          },
		          complete: function() {
		        	  var progressLabel = $(this).find(".progress-label").get(0)
			          progressLabel.textContent =  "Complete!";
		            }
		      }, this);
			
			$(progressbar).progressbar( "value", progress );
		}else{
			if(this.loadbar_open){
				$(this.laodbar).dialog("destroy");
				delete this.loadbar_open;
			}
		}
		

	}
	window.CostGrid = CostGrid;	
})(window);