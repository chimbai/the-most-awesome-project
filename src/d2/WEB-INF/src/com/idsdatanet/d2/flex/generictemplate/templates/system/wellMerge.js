(function(window){
	
	D3.inherits(MergeOptionsDropdown,D3.ComboBoxField);
	
	function MergeOptionsDropdown() {
		D3.ComboBoxField.call(this);
		// Maybe move to beanConfig later
		this._mergeOptions = 
			[{
				value : "wellMerge",
				label : "Merge Wells (Shift All Wellbores)"
			},{
				value : "wellboreMerge",
				label : "Merge Wellbores (Shift All Operations)"
			},{
				value : "operationMerge",
				label : "Merge Operation (Shift All Daily)"
			}];
	}
	
	MergeOptionsDropdown.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			var divConfig = "<div id='mergeOptionsContainer'/>";
			this._mergeOptionsContainer = $(divConfig);
			this._init();
			this._fieldEditor = this._mergeOptionsContainer.get(0);
		}
		return this._fieldEditor;
	};
	
	MergeOptionsDropdown.prototype._init = function() {
		this._createDropdown(this._mergeOptionsContainer, this._mergeOptions);
	};
	
	MergeOptionsDropdown.prototype._createDropdown = function(container, list) {
		var select = document.createElement("select");
		$(select).on("change", {context: this}, this._onChangeDropdown);
		
		var emptyOpt = document.createElement("option");
		emptyOpt.value = "";
		emptyOpt.textContent = "";
		select.appendChild(emptyOpt);
		
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var opt = document.createElement("option");
			opt.value = item.value;
			opt.textContent = item.label;
			select.appendChild(opt);
		}
		container.append(select);
	}
	
	MergeOptionsDropdown.prototype._onChangeDropdown = function(e) {
		var context = null;
		if (e!= null)
			context = e.data.context;
		else
			context = this;
		var value = context._mergeOptionsContainer.find('select').val();
		context._node.setFieldValue("@mergeOption", value);
	};
	
	D3.inherits(ExecuteButton, D3.AbstractFieldComponent);
	
	function ExecuteButton() {
		D3.AbstractFieldComponent.call(this);
	}
	
	ExecuteButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag: "button",
				text: "Execute",
				css: "DefaultButton",
				attributes:{
					title: "Execute!"
				},
				callbacks:{
					onclick:this.onClickExecuteButton
				},
				context:this
			}) ;
		}
		return this._fieldRenderer;
	}
	
	ExecuteButton.prototype.onClickExecuteButton = function() {
		this.execute();
	};
	
	ExecuteButton.prototype.execute = function() {
		var params = {};
		var valid = this.getParams(params);

		if(valid) {
			this._node.commandBeanProxy.customInvoke(params, this.completed, this);
		}
	};
	
	ExecuteButton.prototype.getParams = function(params) {
		var mergeOption = this._node.getFieldValue("@mergeOption");
		if (mergeOption == null || mergeOption == "") {
			return false;
		} else {
			params._invokeCustomFilter = this._node.getFieldValue("@mergeOption");
		}
		params.sourceWell = this._node.getFieldValue("@well") || "";
		params.sourceWellbore = this._node.getFieldValue("@wellbore") || "";
		params.sourceOperation = this._node.getFieldValue("@operation") || "";
		params.targetWell = this._node.getFieldValue("@targetWell") || "";
		params.targetWellbore = this._node.getFieldValue("@targetWellbore") || "";
		params.targetOperation = this._node.getFieldValue("@targetOperation") || "";
		return true;
	};
	
	ExecuteButton.prototype.completed = function() {
		alert("Done!");
		var url = window.location.href;
		window.location.href = url;
	};
	
	ExecuteButton.prototype.refreshFieldRenderer = function(){}
	
	D3.inherits(WellMergeListener,D3.EmptyNodeListener);
	
	function WellMergeListener(){}
	
	WellMergeListener.prototype.getCustomFieldComponent = function(id, node) {
		if(id == "mergeOptions") {
			return new MergeOptionsDropdown();
		} else if (id == "executeButton" ) {
			return new ExecuteButton();
		} else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = WellMergeListener;
	
})(window);