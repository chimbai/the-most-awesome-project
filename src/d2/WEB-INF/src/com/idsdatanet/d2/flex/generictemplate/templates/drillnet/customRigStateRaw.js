(function(window){
	
	D3.inherits(SearchButtonField, D3.AbstractFieldComponent);
	
	function SearchButtonField() {
		D3.AbstractFieldComponent.call(this);
	}
	
	SearchButtonField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {

			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Submit",
				css:"DefaultButton",
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var dayDate = this._node.getFieldValue("@daydate");
						var dayTime = this._node.getFieldValue("@dayTime");
						var rigState = this._node.getFieldValue("@rigState");
						var hideRec = this._node.getFieldValue("@hideRec");
						if (hideRec=="") hideRec = "0";
						if ((dayDate!=null && dayDate!="") && (dayTime!=null && dayTime!="")) {
							var filter = dayDate + "," + dayTime + ","+ rigState + "," + hideRec;
							this._node.setFieldValue("@filter",filter);
							this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@filter");
						} else {
							alert("Date & Time Need to be filled");
						}
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	SearchButtonField.prototype.refreshFieldRenderer = function () {}

	D3.inherits(ResetButtonField, D3.AbstractFieldComponent);
	
	function ResetButtonField() {
		D3.AbstractFieldComponent.call(this);
	}
	
	ResetButtonField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {

			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Reset",
				css:"DefaultButton",
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						this._node.setFieldValue("@filter","reset");
						this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@filter");
						
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ResetButtonField.prototype.refreshFieldRenderer = function () {}

	D3.inherits(HourfieldButtonField, D3.AbstractFieldComponent);
	
	function HourfieldButtonField() {
		D3.AbstractFieldComponent.call(this);
		this.hourDropdown = document.createElement("select");
		this.hourDropdown.setAttribute("class", "fieldEditor comboBox");
		this.hourDropdown.onchange = this.hour_onchange.bindAsEventListener(this);
	}
	
	HourfieldButtonField.prototype.hour_onchange = function(e){
		this._node.setFieldValue("@dayTime",this.hourDropdown.value);
	};
	
	HourfieldButtonField.prototype.createOption = function(value) {
		var option = document.createElement("option");
		option.value = value;
		option.appendChild(document.createTextNode(value));
		return option;
	};
	
	HourfieldButtonField.prototype.generateHourSelection = function() {
		var hourSelection = new Array("", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23");
		D3.forEach(hourSelection, function(hour) {
			this.hourDropdown.appendChild(this.createOption(hour));
		}, this);
	};
	
	HourfieldButtonField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this.generateHourSelection();
			
			this._fieldEditor = document.createElement("span");
			this._fieldEditor.style.fontFamily = "Arial";
			this._fieldEditor.style.fontSize = "9pt";
			this._fieldEditor.appendChild(this.hourDropdown);
			this._fieldEditor.appendChild(document.createTextNode(" h "));
			this.hourDropdown.value = this._node.getFieldValue("@dayTime");
		}
		return this._fieldEditor;
	};
	
	HourfieldButtonField.prototype.refreshFieldEditor = function () {}
	HourfieldButtonField.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(HidefieldButtonField, D3.AbstractFieldComponent);
	
	function HidefieldButtonField() {
		D3.AbstractFieldComponent.call(this);
		this.hidefield = document.createElement("input");
		this.hidefield.setAttribute("type", "checkbox");
		this.hidefield.onclick = this.checkbox_onClick.bindAsEventListener(this);
	}
	
	HidefieldButtonField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor = this.hidefield;
			if (this._node.getFieldValue("@hideRec")=="1") this.hidefield.checked=true;
			else this.hidefield.checked=false;
		}
		return this._fieldEditor;
	};
	
	HidefieldButtonField.prototype.checkbox_onClick = function(e){
		if (this.hidefield.checked){
			this._node.setFieldValue("@hideRec","1");
		}else{
			this._node.setFieldValue("@hideRec","0");
		}
	};
	
	HidefieldButtonField.prototype.refreshFieldEditor = function () {};
	
	D3.inherits(CustomRigStateRawNodeListener,D3.EmptyNodeListener);
	
	function CustomRigStateRawNodeListener() {}
	
	CustomRigStateRawNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id == "searchButton"){
			return new SearchButtonField();
		} else if(id == "resetButton"){
			return new ResetButtonField();
		} else if(id == "hourfield"){
			return new HourfieldButtonField();
		} else if(id == "hidefield"){
			return new HidefieldButtonField();
		} else {
			return null;
		}
	}
	D3.CommandBeanProxy.nodeListener = CustomRigStateRawNodeListener;
	
})(window);