(function(window){
	D3.inherits(DvdPlanDetailLink,D3.AbstractFieldComponent);
	
	function DvdPlanDetailLink(){
		D3.AbstractFieldComponent.call(this);
	}
	
	DvdPlanDetailLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = DvdPlanDetailLink.uber.getFieldRenderer.call(this);
		}
		
		this._fieldRenderer = D3.UI.createElement({
			tag:"a",
			text:"Click here to view DVD Plan Detail",
			callbacks:{
				onclick:function(event) {
					if (window.parent){
						window.parent.tabsManager.select("dvdplandetail");
					}
				}
			},
			context:this
		});

		return this._fieldRenderer;
	}
	
	DvdPlanDetailLink.prototype.refreshFieldRenderer = function() {
	}
	
	D3.inherits(DVDPlanMasterNodeListener, D3.EmptyNodeListener);
	
	
	function DVDPlanMasterNodeListener(){

	}
	
	//add import from edm button
	DVDPlanMasterNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration) {

	        if (id == "importFromEDM") {
	        	this.commandBeanProxy = commandBeanProxy;

	            this.btnImportFromEdm = document.createElement("button");
	        	this.btnImportFromEdm.className = "rootButtons";
	            this.btnImportFromEdm.appendChild(document.createTextNode("Import From EDM"));
	            this.btnImportFromEdm.onclick = this.onImportFromEdm.bindAsEventListener(this);;
	            this.btnImportFromEdm.style.display = "";
	            this.btnImportFromEdm.style.visibility = "visible";
	            return this.btnImportFromEdm;
	        }
	        return DVDPlanMasterNodeListener.uber.getCustomRootButton(id, commandBeanProxy, buttonDeclaration);
    }
	
	//on click button function
	DVDPlanMasterNodeListener.prototype.onImportFromEdm = function() {
		this.commandBeanProxy.customInvoke({_invokeCustomFilter: "edm_dvdplan_import"}, this.onImportEdmResponseCallback, this, "json");
		this.startImport();
		location.reload();
    };
    
    DVDPlanMasterNodeListener.prototype.onImportEdmResponseCallback = function(response){
		
    	if (response.success) {
    		//  import completed
    		loadDialog.close();

    	} else{
    		// response.error
    		loadDialog.close();
    		
    	}
 
    };

	DVDPlanMasterNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "DvdPlanDetailLink"){
			return new DvdPlanDetailLink();
		}else{
			return null;
		}
	}
	
	DVDPlanMasterNodeListener.prototype.startImport = function(event) {
		this.prepareLoadingDialog();
		this.loadContent.text("Importing data from EDM. Please wait momentarily.");
		this.loadDialog.dialog("open");
	};
	
	DVDPlanMasterNodeListener.prototype.prepareLoadingDialog = function() {
		var dialogDiv = "<div id='loadDialog'>" +
			"<div class='loadContent'></div>" +
		"</div>";
		var dialog = $(dialogDiv);
		this.loadContent = $(dialog.find(".loadContent").get(0));
		
		this.loadDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
	
	D3.CommandBeanProxy.nodeListener = DVDPlanMasterNodeListener;
	
})(window);