(function(window){

	D3.inherits(SetPrimaryButton,D3.AbstractFieldComponent);
	
	function SetPrimaryButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	SetPrimaryButton.prototype.getFieldRenderer = function(){
		var isPrimaryCurrency = this._node.getFieldValue("isPrimaryCurrency");
		if (isPrimaryCurrency=="0"){
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Set Primary",
				css:"RootButton",
				callbacks:{
					onclick:this.setPrimary
				},
				context:this
			}) ;
			this._fieldRenderer.setAttribute('style', 'width:150px');
		}
		return this._fieldRenderer;
		
	}
	
	SetPrimaryButton.prototype.setPrimary = function(){
		this._node.commandBeanProxy.addAdditionalFormRequestParams("_action","_associate_afe");
		this._node.commandBeanProxy.addAdditionalFormRequestParams("costCurrencyLookupUid", this._node.getFieldValue("costCurrencyLookupUid"));	
		this._node.commandBeanProxy.addAdditionalFormRequestParams("operationUid", this._node.getFieldValue("@operationUid"));	
		this._node.commandBeanProxy.submitForServerSideProcess();
		this._node.commandBeanProxy.reloadParentHtmlPage(false);
	}
	
	SetPrimaryButton.prototype.refreshFieldRenderer = function(){
	}
	
	
	D3.inherits(CostCurrencyNodeListener,D3.EmptyNodeListener);
	
	function CostCurrencyNodeListener() {
	}	

	CostCurrencyNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "setPrimary"){
			return new SetPrimaryButton();
		}
	}
	

	D3.CommandBeanProxy.nodeListener = CostCurrencyNodeListener;
	
})(window);