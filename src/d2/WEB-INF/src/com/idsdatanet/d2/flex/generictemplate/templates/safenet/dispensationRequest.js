(function(window){

	D3.inherits(SendDiespensationButton,D3.AbstractFieldComponent);
	function SendDiespensationButton() {
		D3.AbstractFieldComponent.call(this);
	}
	SendDiespensationButton.prototype.getFieldRenderer = function() {
		var label = D3.LanguageManager.getLabel("Send");
		if (!this._fieldRenderer) {
			this._fieldRenderer = document.createElement("button");
			this._fieldRenderer.setAttribute("type", "button");
			this._fieldRenderer.setAttribute("title", label);
			this._fieldRenderer.className = "DefaultButton";
			this._fieldRenderer.appendChild(document.createTextNode(label));
			this._fieldRenderer.onclick = this.sendProfile.bindAsEventListener(this);
		}
		
		if(!this._node.commandBeanProxy.nodeListener.isSearchViewMode(this._node.commandBeanProxy)){
			return this._fieldRenderer;
		}else if(!this._node.commandBeanProxy.nodeListener.isDifferentGroup(this._node)){
			return this._fieldRenderer;
		}
	}
	
	SendDiespensationButton.prototype.sendProfile = function(event){
		var url = window.location.origin + window.location.pathname;
		this._node.commandBeanProxy.addAdditionalFormRequestParams("baseUrl", url);
		this._node.commandBeanProxy.submitForServerSideProcess(this._node, "sendEmailNotification");
	}
	SendDiespensationButton.prototype.refreshFieldRenderer = function (){}
	

	
	D3.inherits(DispensationNodeListener,D3.EmptyNodeListener);
	
	function DispensationNodeListener()
	{
		
	}
	
	DispensationNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "sendEmailNotification"){
			return new SendDiespensationButton();
		} else if(id == "reportLink"){
			return new ReportLinkField();
		}else {
			return null;
		}
	}
	
	DispensationNodeListener.prototype.showRootButton = function(commandBeanProxy, btn){
		if(btn.textContent == D3.RootButtons.SELECT_ALL){
			return !this.isSearchViewMode(commandBeanProxy);
		}
	}
	
	DispensationNodeListener.prototype.isSearchViewMode = function(commandBeanProxy){
		return (commandBeanProxy.getLayoutFilterFlag("searchViewMode") != null && commandBeanProxy.getLayoutFilterFlag("searchViewMode") == "true");
	}
	
	DispensationNodeListener.prototype.isDifferentGroup = function(node){
		return (node.commandBeanProxy.getLayoutFilterFlag("differentGroup") != null && node.commandBeanProxy.getLayoutFilterFlag("differentGroup") == "true");
			   
	}
	
	DispensationNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if(this.isSearchViewMode(commandBeanProxy)){
			commandBeanProxy.rootNode.setAdditionalUserRequest("gotoFieldUid", commandBeanProxy.gotoFieldUid);
		}
	}
	
	DispensationNodeListener.prototype.beforeDataLoad = function(requestParams, commandBeanProxy) {
		if(this.isSearchViewMode(commandBeanProxy)){
			commandBeanProxy.rootNode.collectRequestParameters(requestParams, true, false, true);
		}
	}
	D3.inherits(ReportLinkField,D3.AbstractFieldComponent);
	function ReportLinkField(){
		D3.AbstractFieldComponent.call(this);
		this.downloadLabel = D3.LanguageManager.getLabel("CallOutList.download");
	}
	ReportLinkField.prototype.download = function(){
		var id = this.getFieldValue();
		if(id){
			this._node.commandBeanProxy.customInvoke({_invokeCustomFilter: "getReportDownloadFilename", id: id},this.startDownload,this,"json");
		}
	}
	ReportLinkField.prototype.startDownload = function(response){
		var url = "abaccess/calloutlist/" + encodeURIComponent(response.filename) + "?_invokeCustomFilter=downloadReport&reportId=" + encodeURIComponent(this.getFieldValue());
		this._node.commandBeanProxy.openDownloadWindow(url);
	}
	ReportLinkField.prototype.getFieldRenderer = function(){
		if (!this._renderer){
			this._renderer = document.createElement("a");
			this._renderer.className = "reportLink";
			this._renderer.style.cursor = "pointer";
			var self = this;
			this._renderer.addEventListener("click", function(e){e.stopPropagation(); self.download();});
		}
		return this._renderer;
	}
	ReportLinkField.prototype.refreshFieldRenderer = function(){
		while(this._renderer.firstChild) this._renderer.removeChild(this._renderer.firstChild);
		if(this.getFieldValue()){
			this._renderer.appendChild(document.createTextNode(this.downloadLabel));
		}
	}
	DispensationNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (!this._commandBeanProxy){
			this._commandBeanProxy = commandBeanProxy;
			this.LABEL_GENERATE_REPORT = D3.LanguageManager.getLabel("button.generateReport");
			this.LABEL_GENERATE_REPORT_AND_SEND = D3.LanguageManager.getLabel("button.generateReportAndSend");
			this.LABEL_GENERATE_REPORT_PLEASE_WAIT = D3.LanguageManager.getLabel("button.generateReportPleaseWait");
		}
		if (id == "generateReport") {
				
			this._btnGenerateReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:this.LABEL_GENERATE_REPORT,
				callbacks:{
					onclick:this.generateReport
				},
				context:this
			});
			return this._btnGenerateReport;
		}else if (id == "generateAndSent") {
			this._btnGenerateAndSendReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:this.LABEL_GENERATE_REPORT_AND_SEND,
				callbacks:{
					onclick:this.generateAndSendReport
				},
				context:this
			});
			return this._btnGenerateAndSendReport;
		}
	}
	DispensationNodeListener.prototype.getSelectedRecords = function(){
		var result = new Array();
		var list = this._commandBeanProxy.rootNode.children["MocDisp"].getItems();
		for(var i=0; i < list.length; ++i){
			if(list[i].getSelectedMode()){
				result.push({key: list[i].primaryKeyValue});
			}
		}
		if(result.length == 0){
			var selected = D3.Viewer.pageListingListView.getChildClassUIComponents()["MocDisp"].currentSelectedRowRef;
			if(selected){
				result.push({key: selected.node.primaryKeyValue});
			}
		}
		return result;
	}
	DispensationNodeListener.prototype.submitReport = function(sendAfterGenerate, btn){
		if(btn.disabled) return;
		var selected = this.getSelectedRecords();
		if(selected.length == 0){
			alert("Please add Daily Observation before generate report.");
			return;
		}
		var params = {};
		params._invokeCustomFilter = "generate_report";
		if(sendAfterGenerate) params.sendAfterGenerate = "1";
		params.selected = JSON.stringify(selected);
		this._commandBeanProxy.customInvoke(params,this.statusCheckCompleted,this,"json");
	
		this.disableBtnAndShowGeneratingReport([btn]);
	}
	DispensationNodeListener.prototype.generateReport = function(){
		this.submitReport(false, this._btnGenerateReport);
	}
	DispensationNodeListener.prototype.generateAndSendReport = function(){
		this.submitReport(true, this._btnGenerateAndSendReport);
	}
	DispensationNodeListener.prototype.loadCompleted = function(commandBean) {
		this.checkingCurrentStatus();
	}
	DispensationNodeListener.prototype.checkingCurrentStatus = function() {
		this._commandBeanProxy.customInvoke({_invokeCustomFilter: "check_report_status"},this.statusCheckCompleted,this,"json");
	}
	DispensationNodeListener.prototype.updateLabel = function(btn, label){
		while(btn.firstChild) btn.removeChild(btn.firstChild);
		btn.appendChild(document.createTextNode(label));
	}
	DispensationNodeListener.prototype.disableBtnAndShowGeneratingReport = function(btns){
		for(var i=0; i < btns.length; ++i){
			if(!btns[i].disabled){
				this.updateLabel(btns[i], this.LABEL_GENERATE_REPORT_PLEASE_WAIT);
			}
		}
		this.disableAllReportButtons(true);
	}
	DispensationNodeListener.prototype.disableAllReportButtons = function(disable){
		this._btnGenerateReport.disabled = disable;
//		this._btnGenerateAndSendReport.disabled = disable; 
	}
	DispensationNodeListener.prototype.setGeneratingReportInProgressAndCheckAgainLater = function(btns){
		this.disableBtnAndShowGeneratingReport(btns);
		var self = this;
		setTimeout(function(){self.checkingCurrentStatus()},1000);
	}
	DispensationNodeListener.prototype.updateCompletedJob = function(list, errorList){
		if(!list) return;
		var i, item, node;
		for(i=0; i < list.length; ++i){
			item = list[i];
			if(item.status == "completed"){
				node = this.findNode("MocDisp", item.key)
				if(node) this.updateOriginalFieldValue(node, "@reportDownload", item.key);
				var selected = this.getSelectedRecords();
				this._commandBeanProxy.addAdditionalFormRequestParams("reportId", encodeURIComponent(selected[0].key));
				this._commandBeanProxy.submitForServerSideProcess();
			}else if(item.status == "error"){
				errorList.push("Error generating report: " + item.msg);
			}
		}
	}
	DispensationNodeListener.prototype.updateOriginalFieldValue = function(node, field, value){
		var f = node.fieldData[field];
		if(f){
			f.value = value;
			node.fireFieldValueChangedEvent.call(node, field);
		}
	}
	DispensationNodeListener.prototype.findNode = function(className, key){
		var list = this._commandBeanProxy.rootNode.children[className].getItems();
		for(var i=0; i < list.length; ++i){
			if(list[i].primaryKeyValue == key) return list[i];
		}
	}
	DispensationNodeListener.prototype.statusCheckCompleted = function(response) {
		var errorList = [];
		this.updateCompletedJob(response.gen_report.completed, errorList);
		this.updateCompletedJob(response.gen_and_send.completed, errorList);
		var running = [];
		var status = response.gen_report.status;
		if (status == "no_job") {
			this.updateLabel(this._btnGenerateReport, this.LABEL_GENERATE_REPORT);
		} else if (status == "job_running") {
			running.push(this._btnGenerateReport);
		}
		status = response.gen_and_send.status;
//		if (status == "no_job") {
//			this.updateLabel(this._btnGenerateAndSendReport, this.LABEL_GENERATE_REPORT_AND_SEND);
//		} else if (status == "job_running") {
//			running.push(this._btnGenerateAndSendReport);
//		}
		if(running.length > 0){
			this.setGeneratingReportInProgressAndCheckAgainLater(running);
		}else{
			this.disableAllReportButtons(false);
		}
		if(response.error){
			errorList.push(response.error);
		}
		if(errorList.length > 0){
			if(!this._commandBeanProxy.systemMessage) this._commandBeanProxy.systemMessage = [];
			for(var i=0; i < errorList.length; ++i){
				this._commandBeanProxy.systemMessage.push({value: errorList[i], type:"3"});
			}
			D3.Viewer.showInlineSystemMessage();
		}
	}
	D3.CommandBeanProxy.nodeListener = DispensationNodeListener;
})(window);