(function(window){
	D3.inherits(DepotObjectCustomFieldMappingDataNodeListener, D3.EmptyNodeListener);

	function DepotObjectCustomFieldMappingDataNodeListener() { }

	DepotObjectCustomFieldMappingDataNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		if (node.meta._simpleClassName == "DepotObjectCustomFieldMapping") {
			node.setFieldValue("idsTableName", parent.getFieldValue("edmObject"));
		}
	}

	D3.CommandBeanProxy.nodeListener = DepotObjectCustomFieldMappingDataNodeListener;

})(window);