(function(window){
	/** Reset Button for the customField "resetButton" */
	D3.inherits(ResetButtonField, D3.AbstractFieldComponent);
	function ResetButtonField() {
		D3.AbstractFieldComponent.call(this);
	}
	/** Rendewr the Reset button at the "resetButton" customField */
	ResetButtonField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"Reset",
				css:"DefaultButton",
				callbacks:{
					onclick:function(event) {
						// reset all the dynaAttr of the filters
						this._node.setFieldValue("@server", "");
						this._node.setFieldValue("@wellName", "");
						this._node.setFieldValue("@uwi", "");
						this._node.setFieldValue("@country", "");
						this._node.setFieldValue("@status", "");
						this._node.setFieldValue("@onOffShore", "");
						this._node.setFieldValue("@uwbi", "");
						this._node.setFieldValue("@field", "");
						this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,
							"@server,@wellName,@uwi,@country,@status,@onOffShore,@uwbi,@field");
						
					}
				}, context: this
			});
		}
		return this._fieldRenderer;
	}
	ResetButtonField.prototype.refreshFieldRenderer = function () {}
	
	/** Import Actual Button for the customField "importActualButton" */
	D3.inherits(ImportActualButtonField, D3.AbstractFieldComponent);
	function ImportActualButtonField() {
		D3.AbstractFieldComponent.call(this);
	}
	/** Helps to render at the "importActualButton" customFields */
	ImportActualButtonField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			// get the status value which is hiddeningly set to this import button
			var importedStatus = this._node.getFieldValue("status");
			// if not yet imported (NOT_IMPORTED or null), render the import button 
			// Goes through ImportActualButtonField.prototype .checkServerSetup() -> .checkDepotVersionCompleted() -> .showImportPopup()
			if(importedStatus == "NOT_IMPORTED" || !importedStatus) {
				var self = this;
				this._fieldRenderer = D3.UI.createElement({
					tag:"button",
					text:"Import as New",
					css:"DefaultButton",
					callbacks:{
						onclick:function(event) {
							this.checkServerSetup();
						}
					}, context: this
				});
			// Display "Imported"
			} else if(importedStatus == "IMPORTED") {
				var container = "<div class='fieldRenderer'>Imported All From Endpoint</div>";
				this._fieldRenderer = $(container).get(0);
			// Display "Imported Well/Wellbore"
			} else if(importedStatus == "IMPORTED_WW") {
				var container = "<div class='fieldRenderer'>Imported Well/Wellbore</div>";
				this._fieldRenderer = $(container).get(0);
			// Display "Preparing Import Job"
			} else if(importedStatus == "PENDING_IMPORT") {
				var container = "<div class='fieldRenderer'>Preparing import job</div>";
				this._fieldRenderer = $(container).get(0);
			// Display "Importing"
			} else if(importedStatus == "IMPORTING") {
				var container = "<div class='fieldRenderer'>Currently importing</div>";
				this._fieldRenderer = $(container).get(0);
			}
		}
		return this._fieldRenderer;
	}
	/** 1. Check first if the server properties have been setup properly */
	ImportActualButtonField.prototype.checkServerSetup = function () {
		var params = {};
		params._invokeCustomFilter = "checkDepotVersion";
		params.serverUid = this._node.getFieldValue("importExportServerPropertiesUid");
		this._node.commandBeanProxy.customInvoke(params, this.checkDepotVersionCompleted, this, "json");
	}
	/** 2. Callback for checkServerSetup's customInvoke. Will throw a system message popup if there is an issue. 
	 * Otherwise it will display the import popup as usual. */
	ImportActualButtonField.prototype.checkDepotVersionCompleted = function(response) {
		if(!response.success) {
			var errorContainer = $("<div><div class='ErrorSystemMessage SystemMessageItem'>"+response.msg+"</div></div>");
			$(errorContainer).dialog({ 
				title: "System Message: Import error",
				draggable: false,
				closeonEscape:false, modal:true, width:"auto", minHeight:0, resizable:false, closeable: true,
		        close: function(event, ui) { 
    				$(this).empty().remove();
			    } 
			});
			return;
		}
		var isIdsAdmin = response.isIdsAdmin;
		// proceed to the Import pop up generation if all is good
		this.showImportPopup(isIdsAdmin);
	}
	/** 3. Custom function for Import As New button. 
	* It will render a JQuery dialog with the table for the selected WEC record, and to ask to confirm the import action.
	* Can be Import All From X Server or Import Well/Wellbore Only, with different onclick callbacks.*/
	ImportActualButtonField.prototype.showImportPopup = function(serverName, isIdsAdmin) {
		// grab the server name and endpoint url of the selected WEC via the lookup in its Server field
		var serverUid = this._node.getFieldValue("importExportServerPropertiesUid");
		var serverNameLookup = this._node.getLookup("importExportServerPropertiesUid").getLookupItem(serverUid);
		var serverEndPointLookup = this._node.commandBeanProxy.rootNode.getLookup("@serverEndPoint").getLookupItem(serverUid);
		var serverName = (serverNameLookup == null ? serverUid : serverNameLookup.label);
		var serverEndPoint = (serverEndPointLookup == null ? "No endpoint found!" : serverEndPointLookup.label);
		// create the pop up container
		var containerPopup = "<div><span class='fieldContainer'><div class='fieldRenderer'>Please confirm the type of import action for this external well record.</div></span>"+
		"<table class='SimpleGridTable'><tbody>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Well Name</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("wellName")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Wellbore Name</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("wellboreName")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>UWI</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("uwi")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>UWBI</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("uwbi")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Country</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("country")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Field</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("field")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>On/Offshore</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+this._node.getFieldValue("onOffShore")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>"+serverName+"</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer' id='endPointField'>"+serverEndPoint+"</div></span></td></tr>"+
		"</tbody></table></div>";
		containerPopup = $(containerPopup).get(0); // use jquery to get this prepare this container
		$(".SimpleGridHeaderCell", containerPopup).css('width', '14%'); // make the headers inside this dialog table smaller width
		if(serverEndPointLookup == null) {
			$("#endPointField", containerPopup).css({"color" : "red", "font-weight":"bold"}); // bold and red the "Something went wrong" text
		}
		
		// Create Import All button with ImportType=importAll request.params
		var importFromServerBtn = D3.UI.createElement({
			tag:"button",
			text:"Import All From "+serverName+" Server",
			css:"DefaultButton",
			attributes:{ 
				style:"margin-right: 10px;cursor:pointer !important", 
				title:"Query all relevant data associated with this Well/Wellbore (e.g., Operation, Daily, ReportDaily, etc.)."
			},
			callbacks:{
				onclick: async function(event) {
					this._node.commandBeanProxy.addAdditionalFormRequestParams("ImportType", "importAll");
					await this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true); // submit and create jobs
					$(containerPopup).dialog('close'); // close the popup. You need to reference the container in this context
					this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node);
				}
			}, context: this
		});
		// Create Import WW Only button with ImportType=importWWOnly request.params
		var importWWOnlyBtn = D3.UI.createElement({
			tag:"button",
			text:"Import Well/Wellbore Only",
			css:"DefaultButton",
			attributes:{ 
				style:"margin-left: 10px;cursor:pointer !important",
				title:"Import only Well/Wellbore from the server endpoint."
			},
			callbacks:{
				onclick: async function(event) {
					this._node.commandBeanProxy.addAdditionalFormRequestParams("ImportType", "importWWOnly");
					await this._node.commandBeanProxy.submitForServerSideProcess(this._node,null,true);
					$(containerPopup).dialog('close'); // close the popup. You need to reference the container in this context
				}
			}, context: this
		});
		// put the two button options above in a formatted div
		var buttonDiv = "<div style='text-align: center; margin-top: 10px'></div>"
		buttonDiv = $(buttonDiv).get(0);
		buttonDiv.appendChild(importFromServerBtn);
		buttonDiv.appendChild(importWWOnlyBtn);
		
		// check if the user is an IDS user, which will add on this file upload option
		if(isIdsAdmin)
			this.buildTestUploadFileForm(containerPopup);
		
		containerPopup.appendChild(buttonDiv);
		// open the dialog box that is closeable (default to true) allowing a X button to show
		$(containerPopup).dialog({ 
			title: "Confirm Import Source",
			draggable: false,
			closeonEscape:false, modal:true, width:($(window).width()*0.4), height:"auto", resizable:false, closeable: true,
			position: {my: 'center', at: 'center top'},
	        close: function(event, ui) { 
				$(this).empty().remove();
		    } 
		});
	}
	/** Build a file upload form only when the user is a IDS admin (fetched from customInvoke in checkServerSetup() previously) */
	ImportActualButtonField.prototype.buildTestUploadFileForm = function(containerPopup) {
		// create file upload btn
		var uploadFileBtn = D3.UI.createElement({
			tag:"button", text:"Upload Test JSON file", css:"DefaultButton",
			attributes:{ title:"Use a test JSON file to test the import process of DEPOT." },
			callbacks:{
				onclick:function(event) {
				}
			}
		});
		$(uploadFileBtn).attr("disabled","disabled");
		// create the form first
		var formDiv = "<div style='text-align: center; margin-top: 10px'></div>"
		var uploadTestFileForm = "<form action='upload' method='post' enctype='multipart/form-data'>"+
		"<span>IDS ADMIN DEBUG FILE UPLOAD: </span></form>";
		// create the file input and do some validate for JSON files only
		var fileInput = $("<input type='file' id='testJsonData' name='testJsonData'/>").get(0);
		$(fileInput).on("change", {context : this, uploadFileBtn: uploadFileBtn}, function(event) {
			var jsonFile = event.target.files[0];
			// check if the uploaded fileToBeUploaded.type is 'application/json'
			if((jsonFile !== undefined || jsonFile !== null) && jsonFile.type !== 'application/json') {
	        	alert("Wrong file type! Expecting a JSON file but given something else. "+
	        		"Please upload a compitable JSON file for import testing.");
	        	$(event.data.uploadFileBtn).attr("disabled","disabled");
	        	$(event.currentTarget).val("");
	        } else {
				$(event.data.uploadFileBtn).removeAttr("disabled");
	        }
		});
		// append stuff
		uploadTestFileForm = $(uploadTestFileForm).get(0);
		uploadTestFileForm.append(fileInput);
		uploadTestFileForm.append(uploadFileBtn);
		formDiv = $(formDiv).get(0);
		formDiv.append(uploadTestFileForm);
		// append the file form/input into the containerPopup after 
		containerPopup.appendChild(formDiv);
	}
	ImportActualButtonField.prototype.refreshFieldRenderer = function () {}


	/** View Raw Data Button with the customField "viewRawData" */
	D3.inherits(ViewRawDataButtonField, D3.AbstractFieldComponent);
	function ViewRawDataButtonField(){
		D3.AbstractFieldComponent.call(this);
	}
	/** Render the View Raw Data button at the "viewRawData" customField */
	ViewRawDataButtonField.prototype.getFieldRenderer = function() {		
		if (!this._fieldRenderer) {
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:"View Raw Data",
				css:"DefaultButton",
				callbacks:{
					// on clicking this button it will generate a popup via ViewRawDataButtonField.prototype.showRawDataPopup()
					onclick:function(event) {
						var self = this;
						var wecUid = this._node.getFieldValue("wellExternalCatalogUid");
						var url =  "webservice/wellexternalcatalogservice.html?method=viewRaw&wecUid="+wecUid;
						$.ajax({
							url: url,
							dataType: "json"
						}).done(function(response) {
							self.showRawDataPopup(response, self._node);
						});
					}
				}, context: this
			});
		}
		return this._fieldRenderer;
	}
	/** Upon clicking the View Raw Data button, it will generate a popup to display the WEC.sourceData. 
	* First it will need to create a suitable html container and then use the dialog function.*/
	ViewRawDataButtonField.prototype.showRawDataPopup = function(response, node) {
		var serverUid = node.getFieldValue("importExportServerPropertiesUid");
		var serverLookup = node.getLookup("importExportServerPropertiesUid").getLookupItem(serverUid);
		serverLookup = (serverLookup == null ? serverUid : serverLookup.label);
		var containerPopup = "<div><span class='fieldContainer'><div class='fieldRenderer' style='font-size: 13px'>Raw data of "+serverLookup+" External Well/Wellbore</div></span>"+
		"<table class='SimpleGridTable'><tbody>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Well Name</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+node.getFieldValue("wellName")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Wellbore Name</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+node.getFieldValue("wellboreName")+"</div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Source Data</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'>"+
			"<textarea class='textarea' style='width:98%;height:450px' disabled ></textarea></div></span></td></tr>"+
		"</tbody></table></div>";
		containerPopup = $(containerPopup).get(0); // use jquery to get this prepare this container
		$(".SimpleGridHeaderCell", containerPopup).css('width', '14%'); // make the headers inside this dialog table smaller width

		// check if something is wrong with the sourceData and output the correct message (if response is null, or success is fail meaning sourceData is empty/null)
		var sourceData = (response == null ? 
			"Something went wrong, cannot retreive source data!" : !response.success ? response.errorMsg : JSON.stringify(response.sourceData, null, 2));
		if(response == null || !response.success) {
			$("textarea", containerPopup).css({"color" : "red", "font-weight":"bold"}); // bold and red the "Something went wrong" text
		}
		$("textarea", containerPopup).val(sourceData); // add the data in and use 2 spacing indent
		$(containerPopup).dialog({ 
			title: "View Raw Data",
			draggable: false,
			closeonEscape:false, modal:true, width:($(window).width()*0.4), height:"auto", resizable:true, closeable: true,
			position: {my: 'center', at: 'center top'},
	        close: function(event, ui) { 
				$(this).empty().remove();
		    } 
		});
	}
	ViewRawDataButtonField.prototype.refreshFieldRenderer = function () {}


	/** Checkbox for the customField "selectWECCheckbox" */
	D3.inherits(SelectWECCheckbox, D3.AbstractFieldComponent);
	function SelectWECCheckbox() {
		D3.AbstractFieldComponent.call(this);
	}
	/** Here to create a custom record action button (the check box on the left) to behave in a custom way */
	SelectWECCheckbox.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var checkbox = document.createElement("input");
			checkbox.className = "RecordActionsCheckbox SelectWECCheckbox";
			checkbox.type = "checkbox";
			var self = this;
			checkbox.onchange = function (event) {
				if(checkbox.checked) 
					self._node.atts.selected = true;
				if (!checkbox.checked)
					self._node.atts.selected = false;
				// update the attributes and text depending if you check something
				var checkedWECs = self._node.commandBeanProxy.rootNode.collectSelectedChildNodes().WellExternalCatalog.length;
				$('#importSelectedButtonWEC').attr("disabled", checkedWECs > 0 ? false : true);
				$('#importSelectedButtonWEC').css({display: checkedWECs > 0 ? '' : 'none' });
				$('#importSelectedButtonWEC').text("Import Selected ("+checkedWECs+")");
			}
			return checkbox;
		}
		return this._fieldRenderer;
	}
	
	
	/** Main listener to help render customFields */
	D3.inherits(WellExternalCatalogNodeListener,D3.EmptyNodeListener);
	function WellExternalCatalogNodeListener() {}
	/** Fetch the customField ids and return a corresponding rendered object */
	WellExternalCatalogNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id == "resetButton") {
			return new ResetButtonField();
		} else if(id == "importActualButton" && node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)) {
			return new ImportActualButtonField();
		} else if(id == "viewRawData"){
			return new ViewRawDataButtonField();
		} else if(id == "selectWECCheckbox" && node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
			var importedStatus = node.getFieldValue("status");
			var serverUid = node.getFieldValue("importExportServerPropertiesUid");
			var serverLookup = node.getLookup("importExportServerPropertiesUid").getLookupItem(serverUid);
			if(!(importedStatus == "NOT_IMPORTED" || !importedStatus) || serverLookup == null) return null;
			return new SelectWECCheckbox();
		} else {
			return null;
		}
	}
	/** Add a select all checkbox at the header left of the Server column */
	WellExternalCatalogNodeListener.prototype.getCustomButtonForHeaderColumn = function(node, id) {
		if (id != "selectAllNonImported") return null;
		var checkbox = document.createElement("input");
		checkbox.className = "RecordActionsCheckbox";
		checkbox.type = "checkbox";
		checkbox.onclick = function(event) {
			var checked = $(this).prop('checked');
		    $('.SelectWECCheckbox').prop('checked', checked).change();
		}
		return checkbox;
	}
	/** Import Selected Heady Button for the entryHeader button createFromListener="importSelected" */
	WellExternalCatalogNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var entryHeaderButtonIds = ["importSelected","indexExternalWell"];
		if(!parentNode.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)) return null;
		if(!entryHeaderButtonIds.includes(id)) return null;
		var entryHeaderButton = null;
		// generate a Imported Selected button
		if(id==="importSelected") {
			entryHeaderButton = D3.UI.createElement({
				tag:"button",
				text:"Import Selected ("+parentNode.collectSelectedChildNodes().WellExternalCatalog.length+")",
				css:"DefaultButton",
				attributes:{ 
					style:"margin-right: 10px; display:none;", 
					id:"importSelectedButtonWEC"
				}, context:this
			});
		}
		// generate a Index External Wells button
		if(id==="indexExternalWell") {
			entryHeaderButton = D3.UI.createElement({
				tag:"button",
				text:"Index External Wells",
				css:"DefaultButton",
				attributes:{ 
					style:"margin-right: 10px;", 
					id:"indexExternalWellButtonWEC"
				}, context:this
			});
		}
		// return the button generated by either above
		return entryHeaderButton;
	}
	/** Triggered when you click the customHeaderButton we generated above, use id to differentiate between header buttons */
	WellExternalCatalogNodeListener.prototype.customButtonTriggered = function(node, id, button){
		if (id == "importSelected") this.showImportSelectedPopup(node, id, button); 
		if (id == "indexExternalWell") this.showIndexWECPopup(node, id, button); 
	}
	/** Generate the Import Selected popup, asking for confirmation to import those selected by the checkboxes */
	WellExternalCatalogNodeListener.prototype.showImportSelectedPopup = function (node, id, button) {
		// grab the selectedNodes, then set up the container
		var selectedNodes = node.commandBeanProxy.rootNode.collectSelectedChildNodes().WellExternalCatalog;
		var containerPopup = "<div><span class='fieldContainer'><div class='fieldRenderer' style='font-size: 13px'>Please confirm the following selection to import</div></span>"+
		"<table class='SimpleGridTable'><tbody>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'>"+
				"<div class='fieldRenderer'>"+selectedNodes.length+" Well/Wellbores to Import</div>"+
			"</span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridDataCell'><span class='fieldContainer'>"+
				"<div class='fieldRenderer'><textarea class='textarea' style='width:98%;height:300px' disabled></textarea></div>"+
			"</span></td></tr>"+
		"</tbody></table></div>";
		containerPopup = $(containerPopup).get(0); // use jquery to get this prepare this container
		
		// set up the [Server] Well >> Wellbore names to display on the textarea, including the WECUids to submit for server side process 
		var wellWellboreNames = ""; var WECUids = ""; var delimeter = "";
		for (var i = 0; i < selectedNodes.length; i++) {
			WECUids += delimeter + selectedNodes[i].getFieldValue("wellExternalCatalogUid");
			var serverUid = selectedNodes[i].getFieldValue("importExportServerPropertiesUid");
			var serverLookup = selectedNodes[i].getLookup("importExportServerPropertiesUid").getLookupItem(serverUid);
			wellWellboreNames += "["+serverLookup.label+"] " + selectedNodes[i].getFieldValue("wellName") + 
				" >> " + selectedNodes[i].getFieldValue("wellboreName") + "\n";
			delimeter = ",";
		}
		$("textarea", containerPopup).val(wellWellboreNames); // add the names of the well/wellbores being selected for import
		
		// Create Import All button with ImportType=importSelected request.params
		var importFromServerBtn = D3.UI.createElement({
			tag:"button",
			text:"Import All "+selectedNodes.length+" From Servers",
			css:"DefaultButton",
			attributes:{ 
				style:"margin-right: 10px;cursor:pointer !important", 
				title:"Query all relevant data associated with this Well/Wellbore (e.g., Operation, Daily, ReportDaily, etc.)."
			},
			callbacks:{
				onclick: async function(event) {
					node.commandBeanProxy.addAdditionalFormRequestParams("ImportType", "importSelected");
					node.commandBeanProxy.addAdditionalFormRequestParams("WECUids", WECUids);
					await node.commandBeanProxy.submitForServerSideProcess(node,null,true);
					$(containerPopup).dialog('close'); // close the popup. You need to reference the container in "this" context
					node.commandBeanProxy.submitCurrentNodeAndReload(node);
				}
			}, context: this
		});
		// Create Import WW Only button. This is always disabled with a hover title to explain why.
		var importWWOnlyBtn = D3.UI.createElement({
			tag:"button",
			text:"Import Well/Wellbore Only",
			css:"DefaultButton",
			attributes:{ 
				style:"margin-left: 10px;cursor:not-allowed !important",
				title:"This feature only works when importing one external well/wellbore at a time.",
				disabled: "disabled"
			},
		});
		// put the two button options above in a formatted div
		var buttonDiv = "<div style='text-align: center; margin-top: 10px'></div>"
		buttonDiv = $(buttonDiv).get(0);
		buttonDiv.appendChild(importFromServerBtn);
		buttonDiv.appendChild(importWWOnlyBtn);
		containerPopup.appendChild(buttonDiv);
		$(containerPopup).dialog({ 
			title: "Selected "+selectedNodes.length+" Well/Wellbores for import",
			draggable: false,
			closeonEscape:false, modal:true, width:($(window).width()*0.4), height:"auto", resizable:true, closeable: true,
			position: {my: 'center', at: 'center top'},
	        close: function(event, ui) { 
				$(this).empty().remove();
		    } 
		});
	}
	WellExternalCatalogNodeListener.prototype.showIndexWECPopup = function (node, id, button) {
		var self = this;
		// setup the popup container and the Server select2 field
		var containerPopup = "<div><span class='fieldContainer'><div class='fieldRenderer' style='font-size: 13px'>Enter the following details for a indexing background job: </div></span>"+
		"<table class='SimpleGridTable'><tbody>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Server</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer' id='serverSelectedForIndexing'></div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Server End Point</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer' id='serverEndPointSelected'></div></span></td></tr>"+
			"<tr class='SimpleGridDataRow'><td class='SimpleGridHeaderCell'><span class='fieldContainer'><div class='fieldRenderer'>Extra Configurations</div></span></td>"+
			"<td class='SimpleGridDataCell'><span class='fieldContainer'><div class='fieldRenderer'><textarea class='textarea' id='extraConfigForIndexing' style='width:98%;height:200px'></textarea></div></span></td></tr>"+
		"</tbody></table></div>";
		var select2 = '<span class="fieldContainer"><span class="fieldEditor comboBox autoCompleteComboBox">'+
			'<select tabindex="-1" id="serverLookupForIndexing" class="select2-hidden-accessible" aria-hidden="true"></select></span></span';
		select2 = $(select2).get(0);
		containerPopup = $(containerPopup).get(0);
		// make the headers inside this dialog table smaller width
		$(".SimpleGridHeaderCell", containerPopup).css('width', '14%'); 
		// Create Index Well button. This is disabled if the serverLookup is null
		var indexWellBtn = D3.UI.createElement({
			tag:"button",
			text:"Index Wells From External Source",
			css:"DefaultButton",
			attributes:{ 
				title:"Index external wells from the following server and configurations",
				disabled: "disabled", // disable initially
				id: "indexFromExternalBtnWEC"
			}, 
			callbacks:{
				onclick:function(event) {
					var serverUid = $('#serverLookupForIndexing').select2('data')[0].id;
					var extraConfig = $("#extraConfigForIndexing").val();
					this.createIndexJob(node, serverUid, extraConfig);
					$(containerPopup).dialog('close'); // close the popup. You need to reference the container in this context
				}
			}, context: this
		});
		// put the button in a formatted div
		var buttonDiv = "<div style='text-align: center; margin-top: 10px'></div>"
		buttonDiv = $(buttonDiv).get(0);
		buttonDiv.appendChild(indexWellBtn);
		// grab the root node's @server lookup, to populate the select2 we created
		var serverLookup = node.getLookup("@server");
		var optionsHtml = "";
		// if null, just create a default selected saying what's wrong and disable
		if(serverLookup == null) { 
			optionsHtml = "<option value='' title='No Well External Catalog Server setup!' selected disabled hidden>No Well External Catalog Server setup!</option>"; 
			$("select", select2).attr("disabled","disabled");
			$(indexWellBtn).attr("disabled","disabled");
		} else {
			var lookupItems = serverLookup._items;
			optionsHtml += "<option value='' title='' selected disabled hidden/>";
			for(var i=1; i < lookupItems.length; i++) {
				optionsHtml += "<option value='"+lookupItems[i].data+"' title='"+lookupItems[i].label+"'>"+lookupItems[i].label+"</option>"; 
			}
		}
		// append the options to the select and the select/button to the main container, and activate the select as a select2
		$("#serverLookupForIndexing", select2).append(optionsHtml);
		$("#serverSelectedForIndexing", containerPopup).append(select2);
		$("#serverLookupForIndexing", containerPopup).select2({dropdownAutoWidth: true, width: 'auto', 
			allowClear: true, placeholder: "Select a Well External Catalog Server to index from..."});
		$(containerPopup).append(buttonDiv);
		
		// add a event callback to the changes (select2:select and unselect) of the select2
		$("#serverLookupForIndexing", containerPopup).on("select2:select", function (event){
			var serverUid = event.params.data.id;
			var serverEndPointLookup = node.getLookup("@serverEndPoint").getLookupItem(serverUid);
			var serverEndPoint = (serverEndPointLookup == null ? "No endpoint found!" : serverEndPointLookup.label);
			$("#serverEndPointSelected", self.containerPopup).empty(); // empty out text
			$("#serverEndPointSelected", self.containerPopup).append(serverEndPoint); // add the end point url
			if(serverEndPointLookup == null) {
				$("#serverEndPointSelected", self.containerPopup).css({"color" : "red", "font-weight":"bold"});
			} else {
				$("#serverEndPointSelected", self.containerPopup).css({"color" : "", "font-weight":""});
			}
			$("#indexFromExternalBtnWEC", self.containerPopup).attr("disabled",false);
		});
		// if unselect anything, disable the btn and empty out the end point text
		$("#serverLookupForIndexing", containerPopup).on("select2:unselect", function (event){
			$("#serverEndPointSelected", self.containerPopup).empty(); // empty out text
			$("#indexFromExternalBtnWEC", self.containerPopup).attr("disabled",true);
		});
		
		// open the popup
		$(containerPopup).dialog({ 
			title: "Index Wells from External Source",
			draggable: false,
			closeonEscape:false, modal:true, width:($(window).width()*0.4), height:"auto", resizable:true, closeable: true,
			position: {my: 'center', at: 'center top'},
	        close: function(event, ui) { 
				$(this).empty().remove();
		    } 
		});
		
	}
	/** Once you click the "Index Wells From External Source" button with the given selected Server and extra config, 
	* make the call to the data node listener to create a index job */
	WellExternalCatalogNodeListener.prototype.createIndexJob = function(node, serverUid, extraConfig) {
		console.log(serverUid); console.log(extraConfig);
		node.commandBeanProxy.addAdditionalFormRequestParams("ImportType", "indexExternalWells");
		node.commandBeanProxy.addAdditionalFormRequestParams("serverUid", serverUid);
		node.commandBeanProxy.addAdditionalFormRequestParams("extraConfig", extraConfig);
		node.commandBeanProxy.submitForServerSideProcess(node,null,true); // submit and create indexing job
	}
	D3.CommandBeanProxy.nodeListener = WellExternalCatalogNodeListener;
	
})(window);