(function(window){
	
	D3.inherits(DatumReferenceCombobox,D3.ComboBoxField);
	
	function DatumReferenceCombobox(){
		D3.ComboBoxField.call(this);
		this.onOffShoreField = null;
		this.countryField = null;
	};
	
	DatumReferenceCombobox.prototype.filteredLookupReference = function(lookupRef){
		var result = new D3.LookupReference();
		var blank = new D3.LookupItem();
		blank.data = "";
		blank.label = ""
		blank.inactive = false;
		blank.firstBlankItem = true;
		result.addLookupItem(blank);
		var item = null;
		if(this._node && this._node.getFieldValue(this.onOffShoreField) == "ON"){
			if((this._node.getFieldValue(this.countryField) == "Australia" 
				|| this._node.getFieldValue(this.countryField) == "AU")
				&& this._node){
				item = lookupRef.getLookupItem("AHD");
				if(item){
					result.addLookupItem(item);
				}
			}
			
			item = lookupRef.getLookupItem("CHF");
			if(item){
				result.addLookupItem(item);
			}
			
			item = lookupRef.getLookupItem("GL");
			if(item){
				result.addLookupItem(item);
			}
			
			item = lookupRef.getLookupItem("MSL");
			if(item){
				result.addLookupItem(item);
			}
			
			item = lookupRef.getLookupItem("WH");
			if(item){
				result.addLookupItem(item);
			}
			
			if(this._node.getFieldValue("operationCode") == "SVC" 
				|| this._node.getFieldValue("operationCode") == "INTV" 
				|| this._node.getFieldValue("operationCode") == "WKO"){
				item = lookupRef.getLookupItem("TH");
				if(item){
					result.addLookupItem(item);
				}
			}
			
		}else{
			var items = lookupRef.getLookup();
			for(var i=0;i< items.length;i++){
				item = items[i];
				if(item.data != "GL"){
					if(item.data == "AHD"){
						if(this._node && this._node.getFieldValue(this.countryField) == "Australia" || 
							this._node && this._node.getFieldValue(this.countryField) == "AU"){
							result.addLookupItem(item);
						}
					}else if(item.data == "TH"){
						if(this._node.getFieldValue("operationCode") == "SVC" 
							|| this._node.getFieldValue("operationCode") == "INTV" 
							|| this._node.getFieldValue("operationCode") == "WKO"){
							result.addLookupItem(item);
						}
					}else{
						result.addLookupItem(item);
					}
				}
			}
			
		}
		this._filteredLookupReference = result;
		return result;
	}
	
	DatumReferenceCombobox.prototype.afterPropertiesSet = function(){
		DatumReferenceCombobox.uber.afterPropertiesSet.call(this);
		
		this.onOffShoreField = this._config.onOffShoreField;
		if(this.onOffShoreField == ""){
			this.onOffShoreField = "@well.onOffShore";
		}
		
		this.countryField = this._config.countryField;
		if(this.countryField == ""){
			this.countryField = "@well.country";
		}
	}
	D3.DatumReferenceCombobox = DatumReferenceCombobox;
	
	
	D3.inherits(DatumTypeCombobox,D3.ComboBoxField);
	
	function DatumTypeCombobox(){
		D3.ComboBoxField.call(this);
	};
	
	DatumTypeCombobox.prototype.filteredLookupReference = function(lookupRef){
		var operationCode = this._node.getFieldValue("operationCode");
		var result = new D3.LookupReference();
		var items = lookupRef.getLookup();
		var len = items.length;
		
		//when no operation type is selected, show as defined in bean config
		if(operationCode == null || operationCode == ""){
			return lookupRef;
		}
		
		for(var i=0; i<len; i++){
			var item = items[i];
			var datumType = item.data;
			if(datumType != null && datumType != "" && datumType == "TH"){
				if(operationCode == "SVC" || operationCode == "INTV" || operationCode == "WKO"){
					result.addLookupItem(item);
				}
			}else{
				result.addLookupItem(item);
			}
		}

		this._filteredLookupReference = result;
		return result;
	}
	
	D3.DatumTypeCombobox = DatumTypeCombobox;
	
})(window);