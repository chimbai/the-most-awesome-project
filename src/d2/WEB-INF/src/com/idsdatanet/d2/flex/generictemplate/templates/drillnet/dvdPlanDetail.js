(function(window){

	D3.inherits(DvdPlanDetailNodeListener,D3.EmptyNodeListener);
	
	function DvdPlanDetailNodeListener()	{}
	
	DvdPlanDetailNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		if(!this.commandBeanProxy){
			this.commandBeanProxy = parentNode.commandBeanProxy;
		}
		if(id == "calculatePlanPhaseDetailStartAndEndDateTime"){
			var config = {
					tag:"button",
					text:D3.LanguageManager.getLabel("OperationPlanPhase.calculatePlanStartAndEndDateTimeButton.label"),
					css:"DefaultButton",
					context:this
			};
			if(this.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)
				&& parentNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "OperationPlanPhase")){
				return this._calculatePlanPhaseDetailStartAndEndDateTimeBtn = D3.UI.createElement(config);
			}
		} else{
			return null;
		}
	}
	
	DvdPlanDetailNodeListener.prototype.customButtonTriggered = function(node, action, button) {
		if(action == "calculatePlanPhaseDetailStartAndEndDateTime"){
			var operationPlanPhases = node.children["OperationPlanPhase"].getItems();
			for(var p in operationPlanPhases){
				//operationPlanPhases[p].setEditMode(true, false);
				//operationPlanPhases[p].setSubmitFlag(true);
				var operationPlanTasks = operationPlanPhases[p].children["OperationPlanTask"].getItems();
				for(var t in operationPlanTasks) {
					//operationPlanTasks[t].setEditMode(true, false);
					//operationPlanTasks[t].setSubmitFlag(true);
					var operationPlanDetails = operationPlanTasks[t].children["OperationPlanDetail"].getItems();
					for(var d in operationPlanDetails) {
						operationPlanDetails[d].setEditMode(true, false);
						operationPlanDetails[d].setSubmitFlag(true);
					}
				}
				//break;
			}
			node.commandBeanProxy.addAdditionalFormRequestParams("action", "calculatePlanPhaseDetailStartAndEndDateTime");
			node.commandBeanProxy.submitForServerSideProcess(node);
		} 
	}
	
	DvdPlanDetailNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent){
		if(node.simpleClassName() == "OperationPlanDetail"){
			var isBatchDrilling = getIsBatchDrilling(node);
			if(isBatchDrilling == "1" && isFirstDetail(node, parent)){
				node.setFieldValue("@isBatchDrilling", "1");
				node.setFieldValue("@isFirstNode", "1");
			} else if(isVeryFirstNode(node, parent)) {
				node.setFieldValue("@isVeryFirstNode", "1");
			}
		}
	};
	
	getIsBatchDrilling = function(node) {
		var value = "";
		var params = {};
		params._invokeCustomFilter = "getIsBatchDrilling";
		var response = node.commandBeanProxy.customInvokeSynchronous(params);
		var r = $(response).find('isBatchDrilling');
		if(r.length > 0) {
			value = r[0].textContent;
		}
		return value;		
	};
	
	isFirstDetail = function (node, parent){
		var list = parent.children[node.simpleClassName()];
		var count = list.getLength();
		if(count == 0) { //this is first node
			return true;
		} else return false;
	};
	
	//determine if this node is first and veryfirst
	isVeryFirstNode = function(node, parent) {
		return isFirstDetail(node, parent) && checkParentsIfFirst(parent);
	};
	
	checkParentsIfFirst = function(node){
		if(node.simpleClassName() != "OperationPlanTask" && node.simpleClassName() != "OperationPlanPhase") return true;
		var siblings = node.parent.children[node.simpleClassName()];
		if(siblings._items[0] == node) {
			return checkParentsIfFirst(node.parent);
		} else return false;
	};
	
	DvdPlanDetailNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id=="CustomTextInputField"){
			return new CustomTextInputField();
		} else if (id=="CustomDateTimeField"){
			return new CustomDateTimeField();
		}
	};
	
	DvdPlanDetailNodeListener.prototype.nodeStateChanged = function(node, event){
		if(event.fieldName == "p50Duration" || event.fieldName == "plannedStartDatetime"){
			var p50Dur = node.getFieldValue("p50Duration");
			var planStart = new Date(node.getFieldValue("plannedStartDatetime"));
			if(p50Dur && planStart)
				event.currentTarget.commandBeanProxy.submitForServerSideProcess(node);
		}
	};
	
	D3.CommandBeanProxy.nodeListener = DvdPlanDetailNodeListener;
	
	/**
	 * CustomTextInputField
	 */
	D3.inherits(CustomTextInputField,D3.TextInputField);
	
	function CustomTextInputField(){
		D3.TextInputField.call(this);
	}
	
	CustomTextInputField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._fieldEditor =  D3.TextInputField.prototype.getFieldEditor.call(this);
			this._fieldEditor.onblur = this.dataChangeListener.bindAsEventListener(this);
		} 
		return this._fieldEditor;
	};
	
	CustomTextInputField.prototype.dataChangeListener = function(event) {
		D3.TextInputField.prototype.dataChangeListener.call(this);
		var p50Dur = this._node.getFieldValue("p50Duration");
		var planStart = new Date(this._node.getFieldValue("plannedStartDatetime"));
		if(p50Dur && planStart)
			this._node.commandBeanProxy.submitForServerSideProcess(this._node);
	};
	
	/**
	 * CustomDateTimeField
	 */
	D3.inherits(CustomDateTimeField,D3.DateTimeField);
	
	function CustomDateTimeField(){
		D3.DateTimeField.call(this);
	}
	/**
	 * 1. there is loop going on, cannot select date sometimes
	 * 2. doesnt work when select hour and minute
	 */
	/*CustomDateTimeField.prototype.data = function(data) {
		D3.DateTimeField.prototype.data.call(this, data);
		//this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange, this);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.dataChangeListener, this);
		
	}*/
	
	CustomDateTimeField.prototype.dataChangeListener = function(event) {
		if(event.fieldName == "p50Duration" || event.fieldName == "plannedStartDatetime"){
			var p50Dur = event.currentTarget.getFieldValue("p50Duration");
			var planStart = new Date(event.currentTarget.getFieldValue("plannedStartDatetime"));
			if(p50Dur && planStart)
				event.currentTarget.commandBeanProxy.submitForServerSideProcess(event.currentTarget);
		}
	};
		
	CustomDateTimeField.prototype.afterFieldEditorAppended = function() {
		this._dateField.afterFieldEditorAppended();
		//this._dateField._$datePicker.onclick = this.dataChangeListener.bindAsEventListener(this._dateField._$datePicker);
		$(this._dateField._$datePicker).onblur = this.dataChangeListener.bindAsEventListener($(this._dateField._$datePicker));
		/*$(this._dateField._$datePicker).datepick({
			onSelect: this.onTextChangedDateField.bindAsEventListener(this._dateField)
		});*/
		//this._fieldEditor.appendChild(this._dateField.getFieldEditor());
		this._timeField.afterFieldEditorAppended();
		this._timeField.onchange = this.dataChangeListener.bindAsEventListener(this._timeField);
		this._fieldEditor.appendChild(this._timeField.getFieldEditor());
		//D3.DateTimeField.prototype.afterFieldEditorAppended.call(this);
		//this._fieldEditor.onchange = this.dataChangeListener.bindAsEventListener(this);
	};
	
	//CustomDateTimeField.prototype.getFieldEditor = function() {
//		if (!this._fieldEditor) {
//			this._fieldEditor =  D3.DateTimeField.prototype.getFieldEditor.call(this);
//			//this._dateField._$datePicker.onblur = this.onTextChangedDateField.bindAsEventListener(this._dateField);
//			this._dateField.onTextChanged = this.onTextChangedDateField.bindAsEventListener(this);
//		} 
//		return this._fieldEditor;
		/*if (!this._fieldEditor) {
			this._$datePicker = document.createElement("input");
			this._$datePicker.setAttribute("type", "text");
			this._$datePicker.className = "fieldEditor textInput autoWidth";
			this._$datePicker.onblur = this.onTextChangedDateField.bindAsEventListener(this);
			this.applyFieldLayoutAttributes(this._combobox);
			if (this.inDataGrid) {
				this._$datePicker.width="100%";
				this.applyBoxSizing(this._$datePicker);
			}
			this._fieldEditor = document.createElement("span");
			this._fieldEditor.appendChild(this._$datePicker);
		}
		return this._fieldEditor;*/
	//};
	
	
	
	/*CustomDateTimeField.prototype.onTextChangedDateField = function(node, event) {
		D3.DateField.prototype.onTextChanged.call(this);
		if(event.fieldName == "p50Duration" || event.fieldName == "plannedStartDatetime"){
			var p50Dur = event.currentTarget.getFieldValue("p50Duration");
			var planStart = new Date(event.currentTarget.getFieldValue("plannedStartDatetime"));
			if(p50Dur && planStart)
				event.currentTarget.commandBeanProxy.submitForServerSideProcess(event.currentTarget);
		}
	};*/
	
})(window);