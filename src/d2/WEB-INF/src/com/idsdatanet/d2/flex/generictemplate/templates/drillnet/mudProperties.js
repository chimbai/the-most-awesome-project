(function(window){
	
	D3.inherits(WITSMLUpdateImport, D3.AbstractFieldComponent);

	function WITSMLUpdateImport(id) {
	    D3.AbstractFieldComponent.call(this);
	}

	WITSMLUpdateImport.prototype.getFieldRenderer = function() {
		if(!this._node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
        	return null;
        }
		if (!this._fieldRenderer) {
	        var mudPropertiesUid = this._node.getFieldValue("mudPropertiesUid");
	        var importParameters = this._node.getFieldValue("importParameters");
	        var cssDisplay = importParameters ? "" : "none";
	        var type = "";
	        
	        var buttonDeclaration = { 
					uid: mudPropertiesUid, 
					witsml: "MudProperties",	
					refFields: "@schedulerTemplate"
				}; 	
			this.WITSMLImportButton = new D3.WITSMLImportButton(this._node.commandBeanProxy, buttonDeclaration, this._node);
	    
	        if(this._config.customField)
	            type = this._config.customField;
	        var label = D3.LanguageManager.getLabel("WITSML Update");
	        if(this._config.label)
	            label = this._config.label;
	        
	        this._fieldRenderer = D3.UI.createElement({
	            tag:"div",
	            css:"div",
	            context:this
	        });
	        
	        this._linkButton = D3.UI.createElement({
	            tag:"button",
	            text:label,
	            css:"DefaultButton",
	            attributes:{
	                title:D3.LanguageManager.getLabel("Get updates from WITSML server")
	            },
	            data:{
	                mudPropertiesUid:mudPropertiesUid,
	                type:type
	            },
	            callbacks:{
	                onclick:function(event) {
	                	this.WITSMLImportButton.doUpdateWitsml (mudPropertiesUid, importParameters);
	                }
	            },
	            context:this
	        });
            this._linkButton.style.display = cssDisplay;
	        this._fieldRenderer.appendChild(this._linkButton);
	    }
	    return this._fieldRenderer
	}

	WITSMLUpdateImport.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(MudPropertiesNodeListener, D3.EmptyNodeListener);
	
	function MudPropertiesNodeListener() {}
	
//	MudPropertiesNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
//		if (id == "re-calculate-survey-detail") {
//			MudPropertiesNodeListener.RECALCULATE_SURVEY_DETAIL = buttonDeclaration.label;
//			var config = {
//					tag:"button",
//					text:MudPropertiesNodeListener.RECALCULATE_SURVEY_DETAIL,
//					css:"DefaultButton",
//					context:this
//			};
//			this.btnSnapshot = D3.UI.createElement(config);
//			return this.btnSnapshot;
//		} else {
//			return null;
//		}
//	}
	
	MudPropertiesNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if (commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_NEW" || commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_UPDATE") {
			D3.UIManager.addCalllater(this.archive, this, [commandBeanProxy, commandBeanProxy.formSubmissionReason]);
		}
	};
	
	MudPropertiesNodeListener.prototype.archive = function(commandBeanProxy, formSubmissionReason){
		var mudproperties = commandBeanProxy.rootNode.children["MudProperties"];
		var nodes = mudproperties._items.slice(0, mudproperties._items.length);
		var editObjPrimaryKeys = [];
		for (var i = 0; i < nodes.length; i++){
			if(nodes[i].atts.editMode){
				editObjPrimaryKeys.push(nodes[i].primaryKeyValue);
			}
		}
		for (var i = 0; i < nodes.length; i++)
		{
			if(!nodes[i].atts.editMode ){
				if ((formSubmissionReason == "WITSML_IMPORT_NEW" && editObjPrimaryKeys.length > 0 && editObjPrimaryKeys.indexOf(nodes[i].primaryKeyValue) != -1) || formSubmissionReason == "WITSML_IMPORT_UPDATE")
					mudproperties.archive(nodes[i]);
			}
		}
		if (mudproperties._items.length == 0) mudproperties.restore();
	};
	
//	MudPropertiesNodeListener.prototype.customButtonTriggered = function(node, id, button) {
//		if (id == "re-calculate-survey-detail") {
//			for (var i=0; i<node.children["SurveyStation"].getLength(); i++) {
//				var child = node.children["SurveyStation"].getItems()[i];
//				var depthMdMsl = child.getFieldValue("depthMdMsl");
//				var inclinationAngle = child.getFieldValue("inclinationAngle");
//				var azimuthAngle = child.getFieldValue("azimuthAngle");
//				if ((depthMdMsl && depthMdMsl != "") || (inclinationAngle && inclinationAngle != "") || (azimuthAngle && azimuthAngle != "")) {
//					if (depthMdMsl == "" || inclinationAngle == "" || azimuthAngle == "") {
//						alert("You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
//						return;
//					}
//				}
//				child.setEditMode(true,false);
//				child.setSubmitFlag(true);
//			}
//			node.commandBeanProxy.submitForServerSideProcess(node);
//		}
//	}
	
//	MudPropertiesNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
//		if (node.simpleClassName() == "SurveyStation" && parent.children["SurveyStation"].getLength() < 1) {
//			node.setFieldValue("depthMdMsl", "0.0");
//			node.setFieldValue("inclinationAngle", "0.0");
//			node.setFieldValue("azimuthAngle", "0.0");
//		}
//	}
	
	MudPropertiesNodeListener.prototype.getCustomFieldComponent = function(id, node) {
		if (id == "witsmlUpdateTrajectory" ) {
			return new WITSMLUpdateImport(id);
		} else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = MudPropertiesNodeListener;
	
})(window);