(function(window){

	D3.inherits(FwrReportCustomField,D3.AbstractFieldComponent);
	
	function FwrReportCustomField() {
		D3.AbstractFieldComponent.call(this);
		this._selectionList = null;
		this._sectionCount = 0;
		this._textboxCount = 0;
	}
	
	FwrReportCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			
			var tableConfig = "<table class='sectionSelectContainer'>" +
			"<tr>" +
				"<td valign='bottom'><div class='sectionSelectLabel'><div class='label-left'>"+D3.LanguageManager.getLabel("label.selectSection")+"</div></div></td>" +
			"</tr>" +
			"<tr>" +
				"<td valign='middle'><div class='sectionSelectContent'/></td>" +
			"</tr>" +
			"<tr>" +
				"<td valign='top'><div class='selectAll-ButtonContainer'/></td>" +
			"</tr>" +
			"</table>";
			
			this._table = $(tableConfig);
			this._sectionSelectContainer = this._table.find(".sectionSelectContainer").get(0);
			this._sectionSelectContent = this._table.find(".sectionSelectContent").get(0);
			this._selectAllBox = this._table.find(".selectAll-ButtonContainer").get(0);
			
			this._init();
			this._fieldEditor = this._table.get(0);
		}
		return this._fieldEditor;
	}
	
	FwrReportCustomField.prototype._init = function() {
		this._columnTotal = this._config.columns; // retrieve from custom field
		if (!this._columnTotal) {
			this._columnTotal = 1; // default column count
		}
		this._columnRows = this._config.rows; // retrieve from custom field
		if (!this._columnRows) {
			this._columnRows = 8; // default row count
		}
		var lineHeight = 20; // if change need change 'height' in .sectionSelectContainer li in fwrreport.css
		
		$(this._sectionSelectContent).height((lineHeight * this._columnRows) + 3);
		this._generateSectionList();
		this._generateButtons(this._selectAllBox, this._btnSelectAll_onClick, this._btnUnselectAll_onClick);
	}
	
	FwrReportCustomField.prototype._generateSectionList = function(){	
		
		var tableConfig = "<table class='innerSectionSelectContent'></table>";
		this._innerContent = $(tableConfig);
		this._buildContent();
		$(this._sectionSelectContent).append(this._innerContent);
	}
	
	FwrReportCustomField.prototype._buildContent = function(){
		var columnSize = 250;

		this._columnSize = columnSize * this._columnTotal;
		
		var container = this._innerContent;
		this._countRowsNeeded();
		var totalLines = this._sectionCount + this._textboxCount;
		var rowsPerColumn = Math.ceil(totalLines / this._columnTotal);
		if ((rowsPerColumn % 2) == 1) // to prevent separation of textInput from its respective checkboxes
			rowsPerColumn++;
		
		container.attr('width', columnSize * this._columnTotal +"px");
		
		container.append($("<tr id='innerTableRow'>"));
		var tableRow = container.find('tr').get(0);
		var tableColumns = [];
		
		for (var columnCount = 0; columnCount < this._columnTotal; columnCount++) {
			$(tableRow).append($("<td id='innerTableColumn" + columnCount + "' width='"+ columnSize +"px'>"));
			tableColumns[columnCount] = $(tableRow).find('td').get(columnCount);
		}
		this._populateSectionList(tableColumns, rowsPerColumn, this._columnTotal);
	}
	
	FwrReportCustomField.prototype._countRowsNeeded = function () {
		if (this._selectionList == null)
			this._selectionList = this._retrieveSectionList();
		D3.forEach(this._selectionList, function(item) {
			if (item && !item.firstBlankItem) {
				if (item.customProperty == "TextInput") {
					this._textboxCount++;
				}
				this._sectionCount++;
			}
		}, this);
	}
	
	FwrReportCustomField.prototype._generateButtons = function(container, selectAllHandler, unselectAllHandler){	
		var btnSelectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.selectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:selectAllHandler
				},
				context:this
		}
		var _btnSelectAll = D3.UI.createElement(btnSelectAllConfig);
		
		var btnUnselectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:unselectAllHandler
				},
				context:this
		}
		var _btnUnselectAll = D3.UI.createElement(btnUnselectAllConfig);
		
		container.appendChild(_btnSelectAll);
		container.appendChild(_btnUnselectAll);
	}
	
	FwrReportCustomField.prototype._updateSelectedSections = function() {
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue("@reportSelectedSections", id_list);
	}
	
	FwrReportCustomField.prototype._updateTextInput = function(e) {
		if (e != null)
			context = e.data.context;
		else
			context = this;
		var id_list = "";
		for (var key in context._textboxes) {
			value = "";
			if (context._textboxes[key].value != "" && context._textboxes[key].value != null){
				if(id_list == ""){
					value = (context._textboxes[key].id).replace("txtInput", context._textboxes[key].value);
					id_list = value;
				}else{
					value = (context._textboxes[key].id).replace("txtInput", context._textboxes[key].value);
					id_list += "," + value;
				}
			}
		}
		context._node.setFieldValue("@reportAdditionalInputParam", id_list);
	}

	FwrReportCustomField.prototype._populateSectionList = function(tableColumns, rowsPerColumn, columnTotal) {
		if (this._selectionList == null)
			this._selectionList = this._retrieveSectionList();
		var container = tableColumns;
		
		for(var i=0; i < columnTotal; i++)
			D3.UI.removeAllChildNodes(container[i]);
		
		this._checkboxes = [];
		this._textboxes = [];
		
		var currentRow = 0;
		var currentColumn = 0;
		D3.forEach(this._selectionList, function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				var uid = item.sectionBeanId;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.value = item.sectionBeanId;
				cb.onclick = this._sectionClicked.bindAsEventListener(this);
				this._checkboxes[item.sectionBeanId] = cb;
				this._checkboxes[item.sectionBeanId].onclick = this._sectionClicked.bindAsEventListener(this);
				li.appendChild(cb);
				var sectionLabel = document.createElement("label");
				sectionLabel.setAttribute("for", uid);
				sectionLabel.appendChild(document.createTextNode(item.sectionLabel));
				$(li).attr('title', item.sectionLabel);
				li.appendChild(sectionLabel);
				container[currentColumn].appendChild(li);
				currentRow++;
				
				if (item.customProperty == "TextInput") {
					xtraBox = document.createElement("li");
					xtraBox.setAttribute("class", "textInputLine");
					var label = document.createElement("label");
					label.setAttribute("for", item.sectionBeanId + ":txtInput");
					label.appendChild(document.createTextNode(item.label));
					var input = document.createElement("input");
					input.setAttribute("type", "text");
					input.id = item.sectionBeanId + ":txtInput";
					if (item.isNumeric == "true") {
					    $(input).bind("keydown", this._numericCheck);
					}
					$(input).on("change", {context: this}, this._updateTextInput);
					xtraBox.appendChild(label);
					xtraBox.appendChild(input);
					this._textboxes[item.sectionBeanId] = input;
					container[currentColumn].appendChild(xtraBox);
					currentRow++;
				}
			}
			if (currentRow == rowsPerColumn) {
				currentColumn++;
				currentRow = 0;
			}
		}, this);
		this._updateSelectedSections();
	}
	
	FwrReportCustomField.prototype._retrieveSectionList = function() {
		var params = {};
		params._invokeCustomFilter = "flexFwrReportSectionList";
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var sectionList = [];
		
		$(response).find('FWRSection').each(function(){
			var newItem = {}
			var item = $(this);
			newItem.sectionBeanId = item.find("sectionBeanId").text();
			newItem.sectionLabel = item.find("sectionLabel").text();
			newItem.customProperty = item.find("customProperty").text();
			newItem.isNumeric = item.find("isNumeric").text();
			newItem.label = item.find("label").text();
			sectionList.push(newItem);
		});
		return sectionList;
	}
	
	FwrReportCustomField.prototype._numericCheck = function(e){
		// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
	}

	FwrReportCustomField.prototype._sectionClicked = function(){
		this._updateSelectedSections();
	}
	
	FwrReportCustomField.prototype._btnSelectAll_onClick = function() {
		for (var key in this._checkboxes)
			this._checkboxes[key].checked = true;
		this._updateSelectedSections();
	}
	
	FwrReportCustomField.prototype._btnUnselectAll_onClick = function() {
		for (var key in this._checkboxes)
			this._checkboxes[key].checked = false;
		this._updateSelectedSections();
	}
	
	D3.inherits(FwrReportOtherDocCustomField,D3.AbstractFieldComponent);
	
	function FwrReportOtherDocCustomField() {
		D3.AbstractFieldComponent.call(this);
		this._documentList = null;
	}
	
	FwrReportOtherDocCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			var tableConfig = "<table class='documentSelectContainer'>" +
			"<tr>" +
				"<td valign='bottom'><div class='documentSelectLabel'>"+D3.LanguageManager.getLabel("label.selectOtherDocument")+"</div></td>" +
			"</tr>" +
			"<tr>" +
				"<td valign='top'>" +
					"<div class='documentSelectContent'>" +
						"<input class='documentSelectCheckbox' type='checkbox'>"+D3.LanguageManager.getLabel("label.includeOtherDocumentPDF")+"</input>" +
					"</div></td>" +
			"</tr>" +
			"</table>";
			
			var table = $(tableConfig);
			this._documentSelectContent = table.find(".documentSelectContent").get(0);
			this._documentSelectCheckbox = table.find(".documentSelectCheckbox").get(0);
			this._documentSelectCheckbox.onclick = this._documentSelectCheckbox_onClick.bindAsEventListener(this);
			
			this._init();
			this._fieldEditor = table.get(0);
		}
		return this._fieldEditor;
	}
	
	FwrReportOtherDocCustomField.prototype._init = function () {
		this._documentSelectCheckbox.checked = this._node.getFieldValue("@includeOtherDocument");
	}
	
	FwrReportOtherDocCustomField.prototype._documentSelectCheckbox_onClick = function () {
		if (this._documentSelectCheckbox.checked) {
			this._node.setFieldValue("@includeOtherDocument", true);
		} else {
			this._node.setFieldValue("@includeOtherDocument", false);
		}
	}
	
	D3.inherits(FwrReportParentChildCustomField,D3.AbstractFieldComponent);
	
	function FwrReportParentChildCustomField() {
		D3.AbstractFieldComponent.call(this);
	}
	
	FwrReportParentChildCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._showChildWellList = this._node.getFieldValue("@showChildWellList");
			this._parentChildFwr = this._node.getFieldValue("@parentChildFwr");
			
			var tableConfig = "<table class='parentChildContainer'>" +
			"<tr>" +
				"<td valign='bottom'><div class='parentChildLabel'>"+D3.LanguageManager.getLabel("label.selectParentChildOperations")+"</div></td>" +
			"</tr>" +
			"<tr>" +
				"<td valign='top'><div class='parentChildContent'/></td>" +
			"</tr>" +
			"<tr>" +
				"<td valign='top'><div class='parentChildSelectAllOperationButton'/></td>" +
			"</tr>" +
			"</table>";
			
			var table = $(tableConfig);
			this._parentChildContent = table.find(".parentChildContent").get(0);
			this._parentChildSelectAllOperationButton = table.find(".parentChildSelectAllOperationButton").get(0);
			
			this._init();
			if(this._showChildWellList) {
				this._fieldEditor = table.get(0);
			}
		}
		return this._fieldEditor;
	}
	
	FwrReportParentChildCustomField.prototype._init = function () {
		this._populateOperationList();
		this._generateButtons();
	}
	
	FwrReportParentChildCustomField.prototype._populateOperationList = function () {
		this._operationList = this._retrieveOperationList();
		
		var container = this._parentChildContent;
		D3.UI.removeAllChildNodes(container);
		this._checkboxes = [];
		D3.forEach(this._operationList.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this);
				li.appendChild(cb);
				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				$(li).attr('title', item.label);
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		this._updateSelectedOperations();
	}
	
	FwrReportParentChildCustomField.prototype.btnOperation_onClick = function(){
		this._updateSelectedOperations();
	}
	
	FwrReportParentChildCustomField.prototype._retrieveOperationList = function(){
		var params = {};
		params._invokeCustomFilter = "flexFwrChildWellList";
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		$(response).find('Operation').each(function(){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("operationUid").text();
			newItem.label = item.find("operationName").text();
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	
	FwrReportParentChildCustomField.prototype._updateSelectedOperations = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue("@parentChildFwr", id_list);
	}
	
	FwrReportParentChildCustomField.prototype._generateButtons = function(){	
		var btnSelectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.selectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnSelectAll_onClick
				},
				context:this
		}
		this._btnSelectAll = D3.UI.createElement(btnSelectAllConfig);
		
		var btnUnselectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnUnselectAll_onClick
				},
				context:this
		}
		this._btnUnselectAll = D3.UI.createElement(btnUnselectAllConfig);
		
		this._parentChildSelectAllOperationButton.appendChild(this._btnSelectAll);
		this._parentChildSelectAllOperationButton.appendChild(this._btnUnselectAll);
	}
	
	FwrReportParentChildCustomField.prototype._btnSelectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = true;
		}
		this._updateSelectedOperations();
	}
	
	FwrReportParentChildCustomField.prototype._btnUnselectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = false;
		}
		this._updateSelectedOperations();
	}
	
	D3.inherits(FwrReportNodeListener, D3.DefaultReportListener);
	
	function FwrReportNodeListener() {}
	
	FwrReportNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if (id == "fwr_report_chooser") {
			return new FwrReportCustomField();
		}else if(id == "fwr_other_document_option"){
			return new FwrReportOtherDocCustomField();			
		}else if(id == "fwr_parent_child_option"){				
			return new FwrReportParentChildCustomField();
		}else{
			return FwrReportNodeListener.uber.getCustomFieldComponent.call(this,id,node);
		}
	}
	
	FwrReportNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration) {
		var b = FwrReportNodeListener.uber.getCustomRootButton.call(this, id, commandBeanProxy, buttonDeclaration);
		if (b != null) {
			if (id == "generateReport") {
				b.onclick = null; // removing old event handler
				b.onclick = this.overwriteFWRMessage.bindAsEventListener(this);
			}
		} else {
			return null;
		}
		return b;
	}
	
	FwrReportNodeListener.prototype.overwriteFWRMessage = function(event) {
		if(this._overwriteFWRMessage && this._FWRReportFilesUid != "") {
			var r = confirm(D3.LanguageManager.getLabel("message.overwriteReport"));
			if (r == true) {
			    this.generateReport(event);
			}
		} else {
			this.generateReport(event);
		}
	}

	D3.CommandBeanProxy.nodeListener = FwrReportNodeListener;
})(window);