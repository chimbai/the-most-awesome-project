/**
 * 
 */

(function(window){
	
	function CostAfeDetail(){
		
		this.costAfeDetailUid = null;
		this.costAfeMasterUid = null;
		this.parentAccountCode = "";
		this.accountCode = "";
		this.pureAccountCode = null;
		this.shortDescription = "";
		this.itemDescription = "";
		this.longDescription = "";
		this.afeGroupName = "";
		this.planReference = "";
		this.planReferenceName = "";
		//this.longDescription = null;
		this.referenceNumber = null;
		this.selected = false;
		this.currency = null;
		this.itemCost = 0.0;
		this.itemUnit = null;
		this.quantity = 1.0;
		this.itemTotal = 0.0;
		this.quantityTotal=0.0;
		this.quantityUsed=0.0;
		this.quantityBalance=0.0;
		this.itemTotalInPrimaryCurrency = 0.0;
		this.itemCostInPrimaryCurrency = 0.0;
		this.recurringItem= "No";
		this.afeUsed = 0.0;
		this.afeBalance = 0.0;
		this.duplicateAccountCode = false;
		this.addAccountCodeDescription = true;
		this.isnew = false;
		this.insearch = false;
		this.accountCodesLookup = null;
		this.accountCodeSelectedIndex = 0;
		this.estimatedDays = 1.0;
		this.sequence = 0;
		this.currencyLookup = null;
		this.currencySelectedIndex = 0;
		this.currencyRate = 1.0;
		this.category = "";
		this.subcategory = "";
		this.isTangible = "No";
		this.isActive = true;
		
		this.action = "";
		
		this.phaseCode = "";
		this.phaseName = null;
		this.lookupPhaseCode = null;
		this.lookupPhaseCodeSelectedIndex = 0;		
		
		this.holeSize = null;
		this.holeSizeName = null;
		this.lookupHoleSize = null;
		this.lookupHoleSizeSelectedIndex = 0;
		
		this.lookupCompanyUid = null;
		this.companyName = null;
		this.lookupCompany = null;
		this.lookupCompanySelectedIndex = 0;
	}
	
	CostAfeDetail.prototype.run = function(){
		if (this.isTangible== "false") this.isTangible= "No";
		if (this.isTangible== "true") this.isTangible= "Yes";
		if (this.recurringItem== "false") this.recurringItem= "No";
		if (this.recurringItem== "true") this.recurringItem= "Yes";
		if (this.category == "") this.category = "-";
		if (this.subcategory == "") this.subcategory = "-";
		this.itemTotalInPrimaryCurrency = this.itemTotal;
		if (this.currencyRate==undefined) this.currencyRate = 1.0;

		this.itemCostInPrimaryCurrency = this.itemCost/this.currencyRate;
		this.itemTotal = this.itemCost * this.quantity * this.estimatedDays;
		this.accountCode = (this.parentAccountCode==""?"-":this.parentAccountCode) + " :: " + (this.accountCode==""?"-":this.accountCode) + " :: " + this.shortDescription;
	}
	
	CostAfeDetail.prototype.runBeforeSave = function(){
		this.isTangible = (this.isTangible=="Yes"?true:false);
		this.recurringItem = (this.recurringItem=="Yes"?true:false);
		if (isNaN(this.itemCost)) this.itemCost = 0.0;
		if (isNaN(this.quantity)) this.quantity = 1.0;
		if (isNaN(this.estimatedDays)) this.estimatedDays = 1.0;
		if (this.currencyRate==undefined) this.currencyRate = 1.0;
		this.itemTotal = (this.itemCost * this.quantity * this.estimatedDays);
		this.itemTotalUsed=this.itemTotal;

		this.itemTotalInPrimaryCurrency = (this.itemCost * this.quantity * this.estimatedDays) / this.currencyRate;
		var res = this.accountCode.split(" :: ");
		this.accountCode = res[1] + " :: " + res[2];
	}
	
	window.CostAfeDetail = CostAfeDetail;
})(window);