package com.idsdatanet.d2.flex.dat;

import java.text.DecimalFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.core.web.security.BaseAclPermission;

public class DatFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	
	private final static String ADAT_FLASH_CONTROLLER_NAME = "adatController";
	
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = CommonUtils.urlPathConcat(userSession.getClientBaseUrl(), "webservice/datservice.html");
		String baseUrl = userSession.getClientBaseUrl();
		String showMap = GroupWidePreference.getValue(userSession.getCurrentGroupUid(),"showMapInDat");
		showMap = ("1".equals(showMap)?"true":"false");
		String hideAllWells = GroupWidePreference.getValue(userSession.getCurrentGroupUid(),"datHideAllWellsSelection");
		hideAllWells = ("1".equals(hideAllWells)?"true":"false");
		
		String activateADAT = GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "activateADAT");
		activateADAT = ("1".equals(activateADAT)?"true":"false");
		
		String preloadQR = GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "preloadQueryRunnerInDAT");
		preloadQR = ("1".equals(preloadQR)?"true":"false");
		
		String preloadQS = GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "preloadQuerySmithInDAT");
		preloadQS = ("1".equals(preloadQS)?"true":"false");
		
		
		
		Locale locale = userSession.getUserLocale();
		DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getInstance(locale);
		String decimalSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getDecimalSeparator());
		String thousandSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getGroupingSeparator());
		
		Datum datum = DatumManager.getDatum(userSession.getCurrentUOMDatumUid(), userSession.getUserLocale(), userSession.getCurrentGroupUid());
		String datumLabel = "";
		String datumReferencePoint = "";
		if (datum!=null) {
			datumLabel = " " + datum.getSimplifiedDatumCode();
			datumReferencePoint = " " + datum.getDatumReferencePoint();
		}
		
		boolean editEnabled = AclManager.getConfiguredInstance().getAccess(BaseAclPermission.ACL_KEY_WRITE, ADAT_FLASH_CONTROLLER_NAME , userSession, null);
		boolean createEnabled = AclManager.getConfiguredInstance().getAccess(BaseAclPermission.ACL_KEY_CREATE, ADAT_FLASH_CONTROLLER_NAME , userSession, null);
		boolean deleteEnabled = AclManager.getConfiguredInstance().getAccess(BaseAclPermission.ACL_KEY_DELETE, ADAT_FLASH_CONTROLLER_NAME , userSession, null);
	
		return "d2Url="+urlEncode(serviceUrl)
			+ "&baseUrl=" + urlEncode(baseUrl)
			+ "&showMap=" + urlEncode(showMap)
			+ "&hideAllWells=" + urlEncode(hideAllWells)
			+ "&activateADAT=" + urlEncode(activateADAT)
			+ "&preloadQR=" + urlEncode(preloadQR)
			+ "&preloadQS=" + urlEncode(preloadQS)
			+ "&decimalSeperator=" + urlEncode(decimalSeperator)
			+ "&thousandSeperator=" + urlEncode(thousandSeperator)
			+ "&datumLabel=" + urlEncode(datumLabel)
			+ "&datumReferencePoint=" + urlEncode(datumReferencePoint)
			+ "&adatEditEnabled=" + editEnabled
			+ "&adatCreateEnabled=" + createEnabled
			+ "&adatDeleteEnabled=" + deleteEnabled;
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "VisNet";
	}

	@Override
	public String getAdditionalHtmlHeaderContent(String targetSubModule, UserSession userSession, HttpServletRequest request){
		return "<script language=\"JavaScript\" type=\"text/javascript\">" +
		"var datForm;" +
		"function createDatForm(path){" +
		"	datForm = document.createElement('form');"+
		"	datForm.setAttribute('method', 'post');" +
		"	datForm.setAttribute('action', path);" +
		"	datForm.setAttribute('target', '_blank');" +
		"}"+
		"function postToUrl(){" +
		"	document.body.appendChild(datForm);" +
		"	datForm.submit();" +
		"}"+
		"function setDatFormAttr(fieldName, fieldValue){" +
		"		var hiddenField = document.createElement('input');" +
		"		hiddenField.setAttribute('type', 'hidden');" +
		"		hiddenField.setAttribute('name', fieldName);" +
		"		hiddenField.setAttribute('value', fieldValue);" +
		"		datForm.appendChild(hiddenField);" +
		"}"+
		"function isInternetExplorer(){" +
			"var agt=navigator.userAgent.toLowerCase();" +
			"return ((agt.indexOf(\"msie\") != -1) && (agt.indexOf(\"opera\") == -1));" +
		"}" +
		"function openDownloadWindow(helperUrl,fileUrl){" +
		"  if(isInternetExplorer()){" +
		"    window.open(helperUrl,\"_blank\");" +
		"  }else{" +
		"    window.open(fileUrl);" +
		"  }" +
		"}" +
		"function popupAttachmentStateChanged(eventData){" +
		"	if(window.opener && ! window.opener.closed){" +
		"		window.opener.callbackEvent(\"popupFilesAttachmentStateChanged\",eventData);" +
		"	}" +
		"}" +
		"</script>";
	}
}
