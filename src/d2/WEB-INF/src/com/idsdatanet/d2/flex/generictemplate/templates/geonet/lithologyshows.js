(function(window){
	D3.inherits(LithologyShowLinkField,D3.HyperLinkField);
	
	function LithologyShowLinkField(isNew){
		this._isNew = isNew;
		D3.HyperLinkField.call(this);
	}
	
	LithologyShowLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			if (this._isNew) this._label = D3.LanguageManager.getLabel("label.lithologynew");
			else this._label = D3.LanguageManager.getLabel("label.lithologyedit");
		
			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				css:"",
				context:this
			});
			this._linkButton = D3.UI.createElement({
				tag:"a",
				text:this._label,
				css:"lLink",
				callbacks:{
					onclick:function(event) {
						var url = "";
						if (this._isNew) url = "geolshows.html?lithologyShowsUid=" + escape(this._node.getFieldValue("lithologyShowsUid")) + "&newRecordInputMode=1";
						else url = "geolshows.html?lithologyShowsUid=" + escape(this._node.getFieldValue("lithologyShowsUid"));
							
						this._node.commandBeanProxy.gotoUrl(url,D3.LanguageManager.getLabel("label.redirect"));
					}
				},
				context:this
			});
			this._fieldRenderer.appendChild(this._linkButton);
		}
		return this._fieldRenderer;
	}
	
	LithologyShowLinkField.prototype.refreshFieldRenderer = function() {	
	};
	
	D3.inherits(LithologyShowNodeListener,D3.EmptyNodeListener);
	
	function LithologyShowNodeListener()
	{
		
	}
	
	LithologyShowNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "add_new_oilshows"){
			return new LithologyShowLinkField(true);
		} else if(id == "go_to_oilshows"){
			return new LithologyShowLinkField(false);
		} else {
			return null;
		}
	}
	LithologyShowNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		this.maxBottomDepth = null;
		var simpleClassName = node.simpleClassName();
		var acts = parent.children[simpleClassName].getItems();
		D3.forEach(acts,function(child){
			var bottomMdMsl = child.getFieldValue("bottomMdMsl")
			if (this.maxBottomDepth){
				var mx = parseInt(this.maxBottomDepth.replace(",", ""));
				var btt = bottomMdMsl.replace(",", ""); 
				var bt = parseInt(btt);
				if (mx < bt){
					this.maxBottomDepth = bottomMdMsl;
				}
			}else{
				this.maxBottomDepth = bottomMdMsl;
			}
		},this);
		if (this.maxBottomDepth){
			node.setFieldValue("topMdMsl", this.maxBottomDepth.toString());
			node.setFieldValue("bottomMdMsl", this.maxBottomDepth.toString());
		}
	}
	D3.CommandBeanProxy.nodeListener = LithologyShowNodeListener;
})(window);