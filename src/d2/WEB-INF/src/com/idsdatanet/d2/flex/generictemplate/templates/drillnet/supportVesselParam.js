(function(window) {
	
D3.inherits(SupportVesselParamWellITImportBox,D3.AbstractFieldComponent);
	
	function SupportVesselParamWellITImportBox(commandBeanProxy, lookupReference, response) {
		this._commandBeanProxy = commandBeanProxy;
		this._lookupReference = lookupReference;
		this._response = response;
		this._init();
	}
	
	SupportVesselParamWellITImportBox.prototype._init = function() {
		if(!this.box) {
			var _boxDiv = "<div class='popUpWellITImportContainer'></div>";
			this.box = $(_boxDiv);
			this.buildImportBox(this.box);
		}
	}
	
	SupportVesselParamWellITImportBox.prototype.show = function() {
		if(!this.box) {
			this._init();
		}
		var that = this;
		this.box.dialog({
			width: "250",
			height: "375",
			title: D3.LanguageManager.getLabel('label.wellitimportbutton.supportvesselparam'),
			resizable: false,
			modal: true,
			buttons: [{
				text: D3.LanguageManager.getLabel("button.import"),
				"id" : "btnImport",
				disabled : true,
				click: function() {
					that.wellitimport();
				}
			},{
				text: D3.LanguageManager.getLabel("button.cancel"),
				"id" : "btnCancel",
				click: function() {
					$(this).dialog('close');
					$(this).dialog('destroy');
					that.box = null;
				}
			}]
		});
	}
	
	SupportVesselParamWellITImportBox.prototype.buildImportBox = function(parent) {
		var tableCfg = "<table class='importBox'>" +
			"<tr>" +
				"<td><label class='floatItem-Right'>" + D3.LanguageManager.getLabel('label.vesselSelect') + "</label></td>" +
			"</tr>" +
			"<tr>" +
				"<td class='popUpColumnTwo-VesselList'/>" +
			"</tr>" +
		"</table>";
		parent.append(tableCfg);
		var _importPopupVessel = parent.find(".popUpColumnTwo-VesselList").get(0);
		this.buildVesselList(_importPopupVessel);
		this.validateImportButton();
	}
	
	SupportVesselParamWellITImportBox.prototype.wellitimport = function() {
		var params = {};
		params._vesselIds = this._vesselIds; // need?
		this._commandBeanProxy.addAdditionalFormRequestParams("action", "importWellIT");
		this._commandBeanProxy.addAdditionalFormRequestParams("vesselIds", this._vesselIds);
		this._commandBeanProxy.addAdditionalFormRequestParams("json", JSON.stringify(this._response));
		this._commandBeanProxy.submitForServerSideProcess();
		
		this.importBoxClose();
	}
	
	SupportVesselParamWellITImportBox.prototype.importBoxClose = function() {
        this.box.dialog("close");
        this.box.dialog("destroy");
        this.box = null;
    };
	
	SupportVesselParamWellITImportBox.prototype.validateImportButton = function() {
		var dataObject = this._vesselIds;
		if (dataObject != null && dataObject != "") {
			this.enableImportButton(true);
		} else {
			this.enableImportButton(false);
		}
	}
	
	SupportVesselParamWellITImportBox.prototype.enableImportButton = function(value) {
		if (value) {
			$("#btnImport").button("enable");
		} else {
			$("#btnImport").button("disable");
		}
	}
	
	SupportVesselParamWellITImportBox.prototype.buildVesselList = function(container) {
		var tableCfg = "<table class='vesselTable'>" +
				"<tr>" +
					"<td valign='middle'><div class='vesselSelectContent'/></td>" +
				"</tr>" +
				"<tr>" +
					"<td valign='top'><div class='selectAllButtonContainer'/></td>" +
				"</tr>" +
				"</table>";
		var table = $(tableCfg);
		this._vesselSelectContent = table.find(".vesselSelectContent").get(0);
		var _selectAllButtonContainer = table.find(".selectAllButtonContainer").get(0);
		this._buildVesselSelection(this._vesselSelectContent);
		this._generateButtons(_selectAllButtonContainer, this._btnSelectAll_onClick, this._btnUnselectAll_onClick);
		$(container).append(table);
	}
	
	SupportVesselParamWellITImportBox.prototype._buildVesselSelection = function(container) {
		D3.UI.removeAllChildNodes(container);
		
		// Here? - Do Synchronous call 'getImportedVesselList' and compare list to see which vessels to disable
		var params = {};
		params._invokeCustomFilter = "getImportedVesselList";
		var importedVesselResponse = this._commandBeanProxy.customInvokeSynchronous(params, "text");
		this._importedVessels = null;
		if (importedVesselResponse != null) {
			this._importedVessels = importedVesselResponse.split(",");
		}
		
		var lookupList = this._lookupReference;
		if (lookupList) {
			this._checkboxes = [];
			D3.forEach(lookupList.getActiveLookup(), function(item) {
				if (item && !item.firstBlankItem && item.data != "") {
					var imported = false;
					var li = document.createElement("li");
					var uid = item.data;
					var cb = document.createElement("input");
					cb.setAttribute("id", uid);
					cb.setAttribute("type", "checkbox");
					if (this._importedVessels != null) {
						if (this._importedVessels.indexOf(uid.toString()) !== -1) {
							cb.setAttribute("disabled", true);
							imported = true;
						}
					}
					cb.value = item.data;
					this._checkboxes[item.data] = cb;
					this._checkboxes[item.data].onclick = this.optionClicked.bindAsEventListener(this);
					li.appendChild(cb);
					var label = document.createElement("label");
					label.setAttribute("for", uid);
					label.appendChild(document.createTextNode(item.label));
					if (!imported) {
						$(li).attr('title', item.label);
					} else {
						$(li).attr('title', D3.LanguageManager.getLabel('tooltip.imported.wellit'));
					}
					li.appendChild(label);
					container.appendChild(li);
				}
			}, this);
		}
		var d = $(container).children("li:odd").addClass("oddItem");
		var e = $(container).children("li:even").addClass("evenItem");
		this.validateImportButton();
	}
	
	SupportVesselParamWellITImportBox.prototype.optionClicked = function() {
		this._updateSelectedDataObjects();
	}
	
	SupportVesselParamWellITImportBox.prototype._generateButtons = function(container, selectAllHandler, unselectAllHandler) {	
		var btnSelectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.selectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:selectAllHandler
				},
				context:this
		}
		var _btnSelectAll = D3.UI.createElement(btnSelectAllConfig);
		
		var btnUnselectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:unselectAllHandler
				},
				context:this
		}
		var _btnUnselectAll = D3.UI.createElement(btnUnselectAllConfig);
		
		container.appendChild(_btnSelectAll);
		container.appendChild(_btnUnselectAll);
	}
	
	SupportVesselParamWellITImportBox.prototype._btnSelectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = true;
		}
		this._updateSelectedDataObjects();
	}
	
	SupportVesselParamWellITImportBox.prototype._btnUnselectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = false;
		}
		this._updateSelectedDataObjects();
	}
	
	SupportVesselParamWellITImportBox.prototype._updateSelectedDataObjects = function() {
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked) {
				if (id_list == "") {
					id_list = key;
				} else {
					id_list += "," + key;
				}
			}
		}
		this._vesselIds = id_list;
		this.validateImportButton();
	}
	
	
	D3.inherits(SupportVesselParamNodeListener, D3.EmptyNodeListener);
	
	function SupportVesselParamNodeListener() {}
	
	SupportVesselParamNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
		if (id == "wellIT-import-supportvesselparam") {
	        var importBtn = document.createElement("button");
	        importBtn.className = "rootButtons";
	        importBtn.appendChild(document.createTextNode("WELS Import"));
	        importBtn.onclick = this.onclickWELSImport.bindAsEventListener(this);
	        return importBtn;
		}
        return null;
	};
	
	SupportVesselParamNodeListener.prototype.onclickWELSImport = function() {
		var params = this.getParams();
		this.wellitStart();
		var response = this.commandBeanProxy.customInvokeSynchronous(params, "json");
		
		if (response == null || response.vessels === undefined) {
    		this.wellitNoVessels();
    	} else {
    		this.wellitLoaded(response);
    	}
    };
    
    SupportVesselParamNodeListener.prototype.getParams = function() {
		var params = {};
		params._invokeCustomFilter = "importWellIT";
		return params;
	}
    
    SupportVesselParamNodeListener.prototype.wellitLoaded = function(response) {
    	this._response = response;
    	this.lookupReference = new D3.LookupReference();
    	
    	for (var i = 0; i < response.vessels.length; i++) {
    		var obj = response.vessels[i];
			var newItem = new D3.LookupItem();
			newItem.data = obj.vesselId;
			newItem.label = obj.vesselName;
			this.lookupReference.addLookupItem(newItem);
    	}
    	this.lookupReference._items.sort(this.sortVessel);
    	this.showWellITImportBox();
    	this.loadDialog.dialog("close");
    	this.loadDialog.dialog("destroy");
	}
    
    SupportVesselParamNodeListener.prototype.showWellITImportBox = function() {
		if (!this._importBox) {
			this._importBox = new SupportVesselParamWellITImportBox(this.commandBeanProxy, this.lookupReference, this._response);
		}
		this._importBox.show();
	}
    
    SupportVesselParamNodeListener.prototype.wellitStart = function() {
		this.prepareLoadingDialog();
		this.loadContent.text("Importing data... Please wait momentarily.");
		this.loadDialog.dialog("open");
	};
	
	SupportVesselParamNodeListener.prototype.prepareLoadingDialog = function() {
		var dialogDiv = "<div id='loadDialog'>" +
			"<div class='loadContent'></div>" +
		"</div>";
		var dialog = $(dialogDiv);
		this.loadContent = $(dialog.find(".loadContent").get(0));
		
		this.loadDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
	
	SupportVesselParamNodeListener.prototype.wellitNoVessels = function() {
		this.prepareNoRigDialog();
		this.noRigContent.text("No vessels to import on current rig.");
		this.noRigDialog.dialog("open");
	};
	
	SupportVesselParamNodeListener.prototype.prepareNoRigDialog = function() {
		var dialogDiv = "<div id='noRigDialog'>" +
			"<div class='noRigContent'></div>" +
		"</div>";
		var dialog = $(dialogDiv);
		
		var self = this;
		this.noRigContent = $(dialog.find(".noRigContent").get(0));
		
		this.noRigDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
		this.noRigDialog.dialog("option", "buttons", [{
			text: "Close",
			click: function() {
				self.closeDialogBoxes();
			}
		}]);
	};
	
	SupportVesselParamNodeListener.prototype.closeDialogBoxes = function() {
		if (this.noRigDialog && this.noRigDialog.hasClass("ui-dialog-content") && this.noRigDialog.dialog("isOpen") === true) {
			this.noRigDialog.dialog("close");
			this.noRigDialog.dialog("destroy");
		}
		if (this.loadDialog && this.loadDialog.hasClass("ui-dialog-content") && this.loadDialog.dialog("isOpen") === true) {
			this.loadDialog.dialog("close");
			this.loadDialog.dialog("destroy");
		}
	};
    
    SupportVesselParamNodeListener.prototype.sortVessel = function(a, b) {
    	if (a.label < b.label) {
    		return -1;
    	}
    	if (a.label > b.label) {
		    return 1;
		}
    	return 0;
    }
	
	D3.CommandBeanProxy.nodeListener = SupportVesselParamNodeListener;
	
})(window);