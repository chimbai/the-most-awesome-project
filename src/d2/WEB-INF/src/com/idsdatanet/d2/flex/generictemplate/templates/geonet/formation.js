(function(window){
	D3.inherits(FormationIconComboBoxField, D3.ComboBoxField);
	
	function FormationIconComboBoxField() {
		D3.ComboBoxField.call(this);
	}
	FormationIconComboBoxField.prototype.config = function(config) {
		if (config) {
			config.autoComplete = "true";
			FormationIconComboBoxField.uber.config(config);
		}
	};
	
	FormationIconComboBoxField.prototype.getAutoCompleteConfig = function() {
		var config = FormationIconComboBoxField.uber.getAutoCompleteConfig();
		config.formatResult = this.formatResult;
		config.formatSelection = this.formatSelection;
		config.width="200px";
		return config;
	};
	
	FormationIconComboBoxField.prototype.formatResult = function(item) {
		if (item.text && item.element) {
			return $('<span><img src="images/formation/' + item.element[0].value.toString() + '" class="image-style" />' + item.text + '</span>');
		} else {
			return item.text;
		}
	}
	
	FormationIconComboBoxField.prototype.formatSelection = function(item) {
		if (item.text && item.element) {
			return $('<span><img src="images/formation/' + item.element[0].value.toString() + '" class="image-style image-selected" />' + item.text + '</span>');	
		} else {
			return item.text;
		}
	}
	FormationIconComboBoxField.prototype.setFieldRendererValue = function(value) {
		if (D3.UI.isDomElement(this._fieldRenderer)) {
			D3.UI.removeAllChildNodes(this._fieldRenderer);
			var fieldValue = this.getFieldValue();
			var img = document.createElement("img");
			img.setAttribute("class", "image-style");
			if (fieldValue != "") {
				img.setAttribute("src", "images/formation/" + fieldValue);
			}
			this._fieldRenderer.appendChild(img);

			
			if (value == null) value = "";
			var t = typeof value;
			if (t === "string" || t === "number") {
				this._fieldRenderer.appendChild(document.createTextNode(this.appendUOMToLabel(value)));
			} else {
				this._fieldRenderer.appendChild(value);
			}
		}
	};
	
	D3.inherits(WITSMLUpdateImport, D3.AbstractFieldComponent);

	function WITSMLUpdateImport(node, buttonDeclaration, context) {
//	    D3.AbstractFieldComponent.call(this);
		if(!node.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
        	return null;
        }
        var type = "";
        
//        var buttonDeclaration = { 
//				//uid: formationUid, 
//				witsml: "Formation",	
//				refFields: "@schedulerTemplate"
//			}; 	
		this.WITSMLImportButton = new D3.WITSMLImportButton(node.commandBeanProxy, buttonDeclaration, node);
    
//        if(this._config.customField)
//            type = this._config.customField;
//        var label = D3.LanguageManager.getLabel("WITSML Update");
//        if(this._config.label)
//            label = this._config.label;

        this._linkButton = D3.UI.createElement({
            tag:"button",
            text:buttonDeclaration.label,
            css:"DefaultButton",
            attributes:{
                title:D3.LanguageManager.getLabel("Get updates from WITSML server")
            },
            
            callbacks:{
                onclick:function(event) {
                	var formations = node.commandBeanProxy.rootNode.children["Formation"];
            		var nodes = formations._items.slice(0, formations._items.length);
            		for(var i=0; i<=nodes.length; i++){
            			var node = nodes[i];
            			var importParameters = node.getFieldValue("importParameters")
            			var formationUid = node.getFieldValue("formationUid");
            			if(importParameters != ""){
            				this.WITSMLImportButton.doUpdateWitsml (formationUid, importParameters);
            			}
            			
            		}
                }
            },
            context:context
        });
	    return this._linkButton;
	}
	
	WITSMLUpdateImport.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(FormationNodeListener,D3.EmptyNodeListener);
	
	function FormationNodeListener() {}
	
	FormationNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "formation_image_dropdown"){
			var ficb = new FormationIconComboBoxField();
			return ficb;
		} else {
			return null;
		}
	}
	
	FormationNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "witsml-import-formation") {
			if(!parentNode.commandBeanProxy.aclManager.isActionAllowed(D3.CommandBeanDataNode.STANDARD_ACTION_ADD)){
	        	return null;
	        }
			var config = {
					tag:"button",
					text:buttonDeclaration.label,
					css:"DefaultButton",
					context:this
			};
			this.btnSnapshot = D3.UI.createElement(config);
//			this.btnSnapshot = new WITSMLUpdateImport(parentNode, buttonDeclaration, this);
			return this.btnSnapshot;
		} else {
			return null;
		}
	};
	
	FormationNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "witsml-import-formation") {
			var formations = node.commandBeanProxy.rootNode.children["Formation"];
    		var nodes = formations._items.slice(0, formations._items.length);
    		var buttonDeclaration = {
					tag:"button",
					text:"WITSML",
					css:"DefaultButton",
					context:this
			};
    		for(var i=0; i<=nodes.length; i++){
    			var node = nodes[i];
    			var importParameters = node.getFieldValue("importParameters")
    			var formationUid = node.getFieldValue("formationUid");
    			if(importParameters != ""){
//    				this.WITSMLImportButton.doUpdateWitsml (formationUid, importParameters);
//    				this.btnSnapshot.doUpdateWitsml (formationUid, importParameters);
    				var WITSMLUpdateImport = new D3.WITSMLImportButton(node.commandBeanProxy, buttonDeclaration, node);
    				WITSMLUpdateImport.doUpdateWitsml (formationUid, importParameters);
    			}
    			
    		}
			
		}
	}

	FormationNodeListener.prototype.loadCompleted = function(commandBeanProxy) {
		if (commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_NEW" || commandBeanProxy.formSubmissionReason == "WITSML_IMPORT_UPDATE") {
			D3.UIManager.addCalllater(this.archive, this, [commandBeanProxy, commandBeanProxy.formSubmissionReason]);
		}
	};
	
	FormationNodeListener.prototype.archive = function(commandBeanProxy, formSubmissionReason){
		var formations = commandBeanProxy.rootNode.children["Formation"];
		var nodes = formations._items.slice(0, formations._items.length);
		var editObjPrimaryKeys = [];
		for (var i = 0; i < nodes.length; i++){
			if(nodes[i].atts.editMode){
				editObjPrimaryKeys.push(nodes[i].primaryKeyValue);
			}
		}
		for (var i = 0; i < nodes.length; i++)
		{
			if(!nodes[i].atts.editMode ){
				if ((formSubmissionReason == "WITSML_IMPORT_NEW" && editObjPrimaryKeys.length > 0 && editObjPrimaryKeys.indexOf(nodes[i].primaryKeyValue) != -1) || formSubmissionReason == "WITSML_IMPORT_UPDATE")
					formations.archive(nodes[i]);
			}
		}
		if (formations._items.length == 0) formations.restore();
	};
	
	D3.CommandBeanProxy.nodeListener = FormationNodeListener;
})(window);