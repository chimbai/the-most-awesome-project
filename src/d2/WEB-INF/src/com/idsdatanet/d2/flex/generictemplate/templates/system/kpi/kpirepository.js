(function(window) {
	
	
	
	D3.inherits(KpiRepositoryNodeListener, D3.EmptyNodeListener);
	function KpiRepositoryNodeListener()
	{
		
	}
	
	KpiRepositoryNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
        this.commandBeanProxy = commandBeanProxy;
        if (id == "sync_kpi_config") {
            var syncBtn = document.createElement("button");
            syncBtn.className = "rootButtons";
            syncBtn.appendChild(document.createTextNode("Sync KPI Config"));
            syncBtn.onclick = this.onclickSync.bindAsEventListener(this);
            return syncBtn;
        }
        return null;
    };
    
    KpiRepositoryNodeListener.prototype.onclickSync = function(event) {
    	var self = this;
    	D3.UIManager.confirm("KPI configuration sync", "Are you sure you want to synchronize all configurations to IDS Bridge?", function() {
			var params = {};
			params._invokeCustomFilter = "sync_kpi_config";
			self.commandBeanProxy.customInvoke(params, self.submitCompleted, self, "json");
			self.setStatusMessage("Synchronizing KPI configurations to IDS Bridge...");
		});
    };
    
    KpiRepositoryNodeListener.prototype.submitCompleted = function(response) {
    	var data = response.SimpleXmlResponse.data.root;
		var success = data.success.text;
		this.statusDialog.dialog("close");
		if (success == "true") {
			if (data.info != undefined) {
				D3.UIManager.popup("Info", data.info.text);
			}
		} else {
			D3.UIManager.popup("Error", data.error);
		}
    };
    
    KpiRepositoryNodeListener.prototype.setStatusMessage = function(message) {
		this.prepareDialog();
		this.dialogContent.empty();
		this.dialogContent.append(message);
		this.statusDialog.dialog("open");
	};
	
	KpiRepositoryNodeListener.prototype.prepareDialog = function() {
		if (this.dialogContent != null) return;
		
		var dialog = $("<div id='statusDialog'></div>");
		this.dialogContent = $("<div class='dialogContent' style='white-space:pre;'></div>");
		dialog.append(this.dialogContent);
		
		var self = this;
		this.statusDialog = dialog.dialog({
			title: "System Message",
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			modal:true,
			minWidth: "auto",
			minHeight: "auto"
		});
	};
    
	D3.CommandBeanProxy.nodeListener = KpiRepositoryNodeListener;
	
})(window);