(function(window){
	D3.inherits(BharunNumberCustomField,D3.TextInputField);
	
	function BharunNumberCustomField(){
		D3.TextInputField.call(this);
	}
	BharunNumberCustomField.prototype.getFieldEditor = function(){
		var fieldEditor = null;
		if (!this._fieldEditor){
			fieldEditor = BharunNumberCustomField.uber.getFieldEditor.call(this);
			fieldEditor.onkeyup = this.onKeyUp.bindAsEventListener(this);
		}else{
			fieldEditor = BharunNumberCustomField.uber.getFieldEditor.call(this);
		}			
		return fieldEditor;
	}
	BharunNumberCustomField.prototype.onKeyUp = function(event){
		this.checkBhaNumExist();
	}
	BharunNumberCustomField.prototype.checkBhaNumExist = function(){
		var params = this.getParams();
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	BharunNumberCustomField.prototype.getParams = function() {
		var params ={};
		params._invokeCustomFilter = "checkBharunNumberExist";
		params.name = this.getFieldEditor().value;
		params.bharunUid = this._node.getFieldValue("bharunUid");
		params.isNewRecord = this._node.atts.isNewRecord? "1":"";
		return params;
	}
	
	BharunNumberCustomField.prototype.loaded = function(response) {
		var self = this;
		this.fieldContainer.clearFieldUserMsgContainer();
		var isDuplicate = false;
		$(response).find('item').each(function(){
			if ($(this).attr('name')=='duplicate'){
				if ($(this).attr('value')=='true'){
					isDuplicate = true;
				}
			}
		});
		if (isDuplicate){
			var msg = D3.LanguageManager.getLabel("warning.bharunNumberExist");
			this.fieldContainer.addFieldUserMessage(msg, "warning", true);

		}	
	}
	
	D3.inherits(BitrunNumberLinkField,D3.HyperLinkField);
	
	function BitrunNumberLinkField() {
		D3.HyperLinkField.call(this);
		this._linkButton = null;
	}
	
	BitrunNumberLinkField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var bitrun = this._node.getFieldValue("@bitrun");
			data = bitrun.split("&");
			var params = {};
			for (var i=0;i<data.length;i++){
				 var parts = data[i].split("=");
				 params[parts[0]] = parts[1];
			}
			params.bitrunnumber = decodeURIComponent((params.bitrunnumber+'').replace(/\+/g, '%20'));

			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				css:"bharun-content",
				context:this
			});
			this._linkButton = D3.UI.createElement({
				tag:"a",
				text:params.bitrunnumber,
				css:"bharunLink",
				data:{
					bitrunUid:params.bitrunuid,
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = "bitrun.html?&gotoFieldName=bitrunUid&gotoFieldUid=" + data.bitrunUid;
						var msg = D3.LanguageManager.getLabel("message.loading");
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
			this._fieldRenderer.appendChild(this._linkButton);
		}
		return this._fieldRenderer;
	}
	
	BitrunNumberLinkField.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(BharunCustomTimeField,D3.TimeField);
	
	function BharunCustomTimeField(){
		D3.TimeField.call(this);
	}
	
	D3.inherits(ServiceLogButton,D3.AbstractFieldComponent);
	
	function ServiceLogButton() {
		D3.AbstractFieldComponent.call(this);
		this._columns = [
			{
				type: "recordAction",
				width:"50px"
			},{
				type:"field",
				field:"equipmentDescription",
				align:"left",
				width:"140px",
				title:D3.LanguageManager.getLabel("ServiceLog.header.Comment")
			},{
				type:"field",
				field:"serviceDateTime",
				align:"center",
				title:D3.LanguageManager.getLabel("ServiceLog.header.ServiceDate")
			},{
				type:"field",
				field:"bharunNumber",
				width:"75px",
				align:"center",
				title:D3.LanguageManager.getLabel("ServiceLog.header.BHARun")
			},{
				type:"field",
				field:"bharunDailyTimeout",
				width:"135px",
				align:"center",
				title:D3.LanguageManager.getLabel("ServiceLog.header.BHATimeOut")
			},{
				type:"field",
				field:"operationName",
				align:"center",
				title:D3.LanguageManager.getLabel("ServiceLog.header.OperationName")
			},{
				type:"field",
				field:"counterReset",
				align:"center",
				title:D3.LanguageManager.getLabel("ServiceLog.header.ResetCumHrs")
			}
		]
	}
	ServiceLogButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var label = D3.LanguageManager.getLabel("ServiceLog.buttonLabel");
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("ServiceLog.buttonLabel"),
				},
				callbacks:{
					onclick:this.showPopup
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	ServiceLogButton.prototype.showPopup = function(event){
		if (!this._container){
			this.initiateContainer();
		}
		this.loadServiceLog();
		
		$(this._container).dialog({title:D3.LanguageManager.getLabel("ServiceLog.title"),dialogClass: 'no-close' ,closeonEscape:false,modal:true, width:"900", height:"500", resizable:false,closable:false});
	}
	ServiceLogButton.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container'>" +
					"<div class='StandardButtonContainer'>" +
					    "<button class='RootButton AddNew'>"+D3.LanguageManager.getLabel("ServiceLog.AddNew")+"</button>" +
						"<button class='RootButton DeleteSelected'>"+D3.LanguageManager.getLabel("button.deleteSelected")+"</button>" +
						"<button class='RootButton Close'>"+D3.LanguageManager.getLabel("button.close")+"</button>" +
					"</div>" +
				 	"<div class='ConfirmCancelButtonContainer hide'>" +
				 		"<button class='PopupConfirmButton Confirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				 		"<button class='PopupCancelButton Cancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
	 				"</div>" +
	 				"<table><tr>" + 
	 				"<td><b>" + D3.LanguageManager.getLabel("ServiceLog.Equipment") + "</td>" +
	 				"<td><label class='titleEquipment'/></td>" +
	 				"<td width='3%'/>" + 
	 				"<td><b>" + D3.LanguageManager.getLabel("ServiceLog.Serial") + "</td>" +
	 				"<td><label class='titleSerialNum'/></td>" +
	 				"</tr></table>" + 
	 				"<table class='SimpleGridTable reportListing'><thead>"+this.getColumnHeader()+"</thead>" +
	 				"<tbody class='dailyHoursUsedLogList-TBODY'></tbody></table>"+
				"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			this._standardButtonContainer = container.find(".StandardButtonContainer").get(0);
			//D3.UI.renderBorder(this._standardButtonContainer);
			
			this._btnAddNew = container.find(".AddNew").get(0);
			this._btnAddNew.onclick = this.btnAddNew_onClick.bindAsEventListener(this);
			this._btnDeleteSelected = container.find(".DeleteSelected").get(0);
			this._btnDeleteSelected.onclick = this.btnDeleteSelected_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
		
			this._confirmCancelButtonContainer = container.find(".ConfirmCancelButtonContainer").get(0);
			//D3.UI.renderBorder(this._confirmCancelButtonContainer);
			
			this._btnConfirm = container.find(".Confirm").get(0);
			this._btnConfirm.onclick = this.btnConfirm_onClick.bindAsEventListener(this);
			this._btnCancel = container.find(".Cancel").get(0);
			this._btnCancel.onclick = this.btnCancel_onClick.bindAsEventListener(this);
			
			this._tbody = container.find(".dailyHoursUsedLogList-TBODY").get(0);
		
			this._titleEquipment = container.find(".titleEquipment").get(0);
			this._titleSerialNum = container.find(".titleSerialNum").get(0);
			this._titleEquipment.textContent = this._node.getFieldValue("@typeLabel");
			this._titleSerialNum.textContent = this._node.getFieldValue("serialNum");
			this._serialNum = this._node.getFieldValue("serialNum");	
			this._bhaComponentUid = this._node.getFieldValue("bhaComponentUid");
			this._bharunUid = this._node.getFieldValue("bharunUid");		
		}
	}
	ServiceLogButton.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}
	ServiceLogButton.prototype.loadServiceLog = function(){
		var params = {};
		
		params._invokeCustomFilter = "getServiceLogList";
		params.serialNum = this._serialNum;
		params.bharunUid = this._bharunUid;
		this._node.commandBeanProxy.customInvoke(params,this.serviceLogListLoaded,this);
	}
	ServiceLogButton.prototype.serviceLogListLoaded = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("InventoryServiceLog").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.inventoryServiceLogUid = data.attr("inventoryServiceLogUid");
			newItem.equipmentDescription = data.attr("equipmentDescription");
			newItem.serialNum = data.attr("serialNum");
			newItem.serviceDateTime = data.attr("serviceDateTime");
			newItem.bharunUid = data.attr("bharunUid");
			newItem.bharunNumber = data.attr("bharunNumber");
			newItem.bharunDailyTimeout = data.attr("bharunDailyTimeout");
			newItem.operationUid = data.attr("operationUid");
			newItem.operationName = data.attr("operationName");
			newItem.counterReset = data.attr("counterReset");
			newItem.dailyUid = data.attr("dailyUid");

			var record = {};
			record.data = newItem;
			record.uid = newItem.inventoryServiceLogUid;
			record.selected = 0;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		this._serviceLogItems = collection;
		this._currTimeOut = doc.find("currTimeOutStatus").get(0).attributes.currTimeOut.value;
		if (this._currTimeOut=="false") $(this._btnAddNew).addClass("hide");
		else $(this._btnAddNew).removeClass("hide");
		D3.UI.removeAllChildNodes(this._tbody);

		this.updateServiceLogList();
		this.updateRootContainer();
	}
	ServiceLogButton.prototype.updateServiceLogList = function(){
		for (var i=0;i<this._serviceLogItems.length;i++){
			var row = this._serviceLogItems[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	ServiceLogButton.prototype.btnAddNew_onClick = function(){
		if (!this._addNewContainer){
			var addNewContainerTemplate = "<div class='addNew-container'>" +
				"<table class='SimpleGridTable'>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("dailyHoursUsed.popAdd.Comment") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<textarea class='textarea commentRecord'/></td>" + 
				"</span></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("ServiceLog.add.ResetCumHrs") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input class='resetCumHrsRecord' type='checkbox'/>" + 
				"</td></span></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("ServiceLog.add.ServiceDate") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input type='text' id='reportDate' class='datepickset serviceDateRecord' readonly>" +
				"</span></td></tr>" +
				"<tr class='SimpleGridDataRow'><td colspan=2 align='center'>" +
				"<span class='fieldRenderer'>" +
				"<div class='ConfirmButtonContainer'>" +
				"<button class='PopupConfirmButton popConfirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				"<button class='PopupCancelButton popCancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
				"</div></span></td></tr>" +
				"</table>"+
				"</div>";
			var addNewContainer = $(addNewContainerTemplate);
			this._addNewContainer = addNewContainer.get(0);
			
			this._commentRecord = addNewContainer.find(".commentRecord").get(0);
			this._resetCumHrsRecord = addNewContainer.find(".resetCumHrsRecord").get(0);
			this._serviceDateRecord  = addNewContainer.find(".serviceDateRecord").get(0);
			$(this._serviceDateRecord).datepick({
				dateFormat:'dd M yyyy', 
				showTrigger:'<img style="padding-left:3px;padding-right:3px;" src="images/calendar-blue.gif"/>',
			});
			
			$(this._serviceDateRecord).datepicker("widget").css({"z-index":100});
			this._btnPopConfirm = addNewContainer.find(".popConfirm").get(0);
			this._btnPopConfirm.onclick = this.btnPopConfirm_onClick.bindAsEventListener(this);
			this._btnPopCancel = addNewContainer.find(".popCancel").get(0);
			this._btnPopCancel.onclick = this.btnPopCancel_onClick.bindAsEventListener(this);
			var btnContainer = addNewContainer.find('.ConfirmButtonContainer').get(0);
			//D3.UI.renderBorder(btnContainer);
		}	
			this._commentRecord.value = "";
			this._resetCumHrsRecord.checked = false;
			this._serviceDateRecord.value = "";
			$(this._addNewContainer).dialog({
				title:D3.LanguageManager.getLabel("ServiceLog.add.Title"),
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"400", 
				height:"auto", 
				resizable:false,
				closable:false
				});
	}
	ServiceLogButton.prototype.btnPopConfirm_onClick = function(){
		if (this._serviceDateRecord.value == ""){
			alert(D3.LanguageManager.getLabel("ServiceLog.add.validate"));
		}else{
			var params = {};
			params._invokeCustomFilter = "saveNewServiceLog";
			params.serialNum = this._serialNum;
			params.descr = this._commentRecord.value;
			params.dtChooser = this._serviceDateRecord.value;
			params.chkBox = (this._resetCumHrsRecord.checked==true?"1":"0")
			params.bharunUid = this._bharunUid;
			params.bhaComponentUid = this._bhaComponentUid;
			this._node.commandBeanProxy.customInvoke(params,this.addNewCompleted,this);
		}
	}
	ServiceLogButton.prototype.addNewCompleted = function(){
		$(this._addNewContainer).dialog("close");
		this.loadServiceLog();
	}
	ServiceLogButton.prototype.btnPopCancel_onClick = function(){
		$(this._addNewContainer).dialog("close");
	}
	ServiceLogButton.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'/>");
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.check_onclick.bindAsEventListener(this);
			var btnDelete = $("<button class='RecordActionsButton RecordActionsDelete'>");
			btnDelete.data({uid:record.uid});
			btnDelete.get(0).onclick = this.btnDelete_onclick.bindAsEventListener(this);
			var btnCancel = $("<button class='RecordActionsButton RecordActionsCancel hide'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>");
			btnCancel.data({uid:record.uid});
			btnCancel.get(0).onclick = this.btnRecordCancel_onclick.bindAsEventListener(this);
			$(columnContainer).append(checkbox);
			$(columnContainer).append(btnDelete);
			$(columnContainer).append(btnCancel);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox,"delete":btnDelete,"cancel":btnCancel};
		}
	}
	ServiceLogButton.prototype.btnDelete_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,"delete");
	}
	ServiceLogButton.prototype.btnRecordCancel_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,null);
		
	}
	ServiceLogButton.prototype.setAction = function(record,action){
		if (action === "delete"){
			record.action = "delete";
		}else if (action === "save"){
			record.action = "save";
			this.setEditMode(record,true);
		}else if (!action){
			record.action = null;
			this.setEditMode(record,false);
			record.modified = {};
		}
		this.updateRecordAction(record);
		this.updateRootContainer();
	}
	ServiceLogButton.prototype.setEditMode = function(row,editMode){
		row.editMode = editMode;
		var fields = row.field;
		
		for (var key in fields){
			var field = fields[key];
			var hasEditor = field._editor != null;
			if (field._renderer){
				if (editMode && hasEditor)
					$(field._renderer).addClass("hide");
				else
					$(field._renderer).removeClass("hide");
			}
			if (field._editor){
				if (editMode && hasEditor){
					$(field._editor).removeClass("hide");
					this.refreshFieldEditor(row,key);
				}else
					$(field._editor).addClass("hide");
			}
		}
	}
	ServiceLogButton.prototype.updateRecordAction = function(row){
		var uiComponents = row.field["_recordAction"]._uiComponents;
		
		if (row.action){
			if (row.action == "delete"){
				$(row.refRow).addClass("DeleteRecordBackground");
				uiComponents["check"].removeAttr("checked");
			}
			uiComponents["check"].addClass("hide");
			uiComponents["delete"].addClass("hide");
			uiComponents["cancel"].removeClass("hide");
			
		}else{
			$(row.refRow).removeClass("DeleteRecordBackground");
			uiComponents["check"].removeClass("hide");
			uiComponents["delete"].removeClass("hide");
			uiComponents["cancel"].addClass("hide");
		}
	}
	ServiceLogButton.prototype.btnDeleteSelected_onClick = function(){
		var selectedItems = [];
		for (var i=0;i<this._serviceLogItems.length;i++){
			var item = this._serviceLogItems[i];
			if (item.selected)
				selectedItems.push(item);
		}
		if (selectedItems.length>0){
			var deleteSelected = false;
			deleteSelected = confirm(D3.LanguageManager.getLabel("alert.delete"));
			if(deleteSelected) {
				for (var key in selectedItems){
					var record = selectedItems[key].action = "delete";
				}
				this.processRecords();
			}
		}else{
			alert(D3.LanguageManager.getLabel("ServiceLog.Delete.Warning"));
		}
	}
	ServiceLogButton.prototype.btnClose_onClick = function(){
		$(this._container).dialog("close");
	}
	ServiceLogButton.prototype.btnConfirm_onClick = function(){
		this.processRecords();
	}
	ServiceLogButton.prototype.processRecords = function(){
		if (!this._processDialog)
			this._processDialog = $("<div>"+D3.LanguageManager.getLabel("ServiceLog.processing")+"</div>");
		this._processDialog.dialog({dialogClass: 'no-close' ,closeonEscape:false,closable:false,modal:true,title:D3.LanguageManager.getLabel("alert.warning.title")});
		
		for (var i=0;i<this._serviceLogItems.length;i++){
			var record = this._serviceLogItems[i];
			this.deleteRecords(record);
		}
		$(this._confirmCancelButtonContainer).addClass("hide");
		this.loadServiceLog();
	}
	
	ServiceLogButton.prototype.deleteRecords = function(record){
		if (record.action == "delete"){
			var params = {};
			params._invokeCustomFilter = "delServiceLog";
			params.inventoryServiceLogUid = record.uid;
			this._node.commandBeanProxy.customInvoke(params,this.completed,this);
		}
	}
	ServiceLogButton.prototype.completed = function(response){
		if (this._processDialog)
			this._processDialog.dialog("close");
	}
	ServiceLogButton.prototype.btnCancel_onClick = function(){
		for (var i=0;i<this._serviceLogItems.length;i++){
			var record = this._serviceLogItems[i];
			this.setAction(record,null);
		}
	}
	ServiceLogButton.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	ServiceLogButton.prototype.getRecord = function(uid){
		for (var i=0;i<this._serviceLogItems.length;i++){
			var record = this._serviceLogItems[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	ServiceLogButton.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		if (fieldName === "bharunNumber"){
			var renderer= D3.UI.createElement({
				tag:"a",
				text:value,
				css:"bharunLink",
				data:{
					dailyUid:record.data.dailyUid,
					bharunUid:record.data.bharunUid,
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = "bharunwithoutbitrun.html?&gotoday=" + data.dailyUid + "&gotoFieldName=bharunUid&gotoFieldUid=" + escape(data.bharunUid);	
						var msg = D3.LanguageManager.getLabel("message.loading");
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
			return renderer;
		}else if (fieldName === "counterReset"){	
			var checkBox= D3.UI.createElement({
				tag:"input",
				attributes:{
					type:"checkbox",
					readonly:"",
					disabled:"",
				},
				context:this
			});
			if (record.data.counterReset=="true") checkBox.checked = true;
			return checkBox;
		}else{
			var span = document.createElement("span");
			span.className = "fieldRenderer";
			$(span).text(value);
		}
		return span;
	}
	ServiceLogButton.prototype.updateRootContainer = function(){
		var hasAnythingToSubmit = false;
		for (var i=0;i<this._serviceLogItems.length;i++){
			var record = this._serviceLogItems[i];
			if (record.action)
				hasAnythingToSubmit = true;
		}
		if (hasAnythingToSubmit){
			$(this._standardButtonContainer).addClass("hide");
			$(this._confirmCancelButtonContainer).removeClass("hide");
		}else{
			$(this._standardButtonContainer).removeClass("hide");
			$(this._confirmCancelButtonContainer).addClass("hide");
		}
	}
	ServiceLogButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(DailyHoursUsedLogButton,D3.AbstractFieldComponent);
	
	function DailyHoursUsedLogButton() {
		D3.AbstractFieldComponent.call(this);
		this._columns = [
		     {
		    	 type: "recordAction",
		    	 width:"50px"
		     },
		     {
		    	 type:"field",
		    	 field:"comment",
		    	 align:"left",
		    	 title:D3.LanguageManager.getLabel("dailyHoursUsed.popComment")
		    	 
		     },    
		     {
		    	 type:"field",
		    	 field:"dayNumDate",
		    	 width:"125px",
		    	 align:"center",
		         title:D3.LanguageManager.getLabel("dailyHoursUsed.popDate")
		     },
		     {
		    	 type:"field",
		    	 field:"hoursUsed",
		    	 width:"103px",
		    	 align:"center",
		    	 title:D3.LanguageManager.getLabel("dailyHoursUsed.popHoursUsed")
		     }
		    ]
	}
	
	DailyHoursUsedLogButton.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var label = D3.LanguageManager.getLabel("dailyHoursUsed.buttonLabel");
			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:label,
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("dailyHoursUsed.buttonLabel"),
				},
				callbacks:{
					onclick:this.showPopup
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	DailyHoursUsedLogButton.prototype.showPopup = function(event){
		if (!this._container){
			this.initiateContainer();
		}
		
		this.loadDailyHoursUsedLog();
		
		$(this._container).dialog({title:D3.LanguageManager.getLabel("dailyHoursUsed.popHeader"),dialogClass: 'no-close' ,closeonEscape:false,modal:true, width:"1024", height:"auto", resizable:false,closable:false});
	}
	
	DailyHoursUsedLogButton.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container'>" +
					"<div class='StandardButtonContainer'>" +
					    "<button class='RootButton AddNew'>"+D3.LanguageManager.getLabel("dailyHoursUsed.popAddNew")+"</button>" +
						"<button class='RootButton DeleteSelected'>"+D3.LanguageManager.getLabel("button.deleteSelected")+"</button>" +
						"<button class='RootButton Close'>"+D3.LanguageManager.getLabel("button.close")+"</button>" +
					"</div>" +
				 	"<div class='ConfirmCancelButtonContainer hide'>" +
				 		"<button class='PopupConfirmButton Confirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				 		"<button class='PopupCancelButton Cancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
	 				"</div>" +
	 				"<table class='SimpleGridTable reportListing'><thead>"+this.getColumnHeader()+"</thead><tbody class='dailyHoursUsedLogList-TBODY'></tbody></table>"+
				"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			this._standardButtonContainer = container.find(".StandardButtonContainer").get(0);
			//D3.UI.renderBorder(this._standardButtonContainer);
			this._btnAddNew = container.find(".AddNew").get(0);
			this._btnAddNew.onclick = this.btnAddNew_onClick.bindAsEventListener(this);
			this._btnDeleteSelected = container.find(".DeleteSelected").get(0);
			this._btnDeleteSelected.onclick = this.btnDeleteSelected_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
		
			this._confirmCancelButtonContainer = container.find(".ConfirmCancelButtonContainer").get(0);
			//D3.UI.renderBorder(this._confirmCancelButtonContainer);
			this._btnConfirm = container.find(".Confirm").get(0);
			this._btnConfirm.onclick = this.btnConfirm_onClick.bindAsEventListener(this);
			this._btnCancel = container.find(".Cancel").get(0);
			this._btnCancel.onclick = this.btnCancel_onClick.bindAsEventListener(this);
			
			this._tbody = container.find(".dailyHoursUsedLogList-TBODY").get(0);
			
			this._bhaComponentUid = this._node.getFieldValue("bhaComponentUid");
			this._bharunUid = this._node.getFieldValue("bharunUid");		
		}
	}
	
	DailyHoursUsedLogButton.prototype.btnAddNew_onClick = function(){
		if (!this._addNewContainer){
			var addNewContainerTemplate = "<div class='addNew-container'>" +
				"<table class='SimpleGridTable'>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("ServiceLog.add.Comment") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<textarea class='textarea commentRecord'/></td>" + 
				"</span></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("dailyHoursUsed.popAdd.Date") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<select class='dateRecord'>" + 
				"</select>" +
				"</td></span></tr>" +
				"<tr class='SimpleGridDataRow'>" +
				"<td><span class='fieldRenderer'>"+D3.LanguageManager.getLabel("dailyHoursUsed.popAdd.HoursUsed") + "</span></td>" +
				"<td>" +
				"<span class='fieldRenderer'>" +
				"<input type='number' class='hoursUsedRecord'/></td>" + 
				"</span></tr>" +
				"<tr class='SimpleGridDataRow'><td colspan=2 align='center'>" +
				"<span class='fieldRenderer'>" +
				"<div class='ConfirmButtonContainer'>" +
				"<button class='PopupConfirmButton popConfirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				"<button class='PopupCancelButton popCancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
				"</div></span></td></tr>" +
				"</table>"+
				"</div>";
			var addNewContainer = $(addNewContainerTemplate);
			this._addNewContainer = addNewContainer.get(0);
			
			this._commentRecord = addNewContainer.find(".commentRecord").get(0);
			this._dateRecord = addNewContainer.find(".dateRecord").get(0);
			this.retrieveDateList();
			this._hoursUsedRecord = addNewContainer.find(".hoursUsedRecord").get(0);
			
			
			this._btnPopConfirm = addNewContainer.find(".popConfirm").get(0);
			this._btnPopConfirm.onclick = this.btnPopConfirm_onClick.bindAsEventListener(this);
			this._btnPopCancel = addNewContainer.find(".popCancel").get(0);
			this._btnPopCancel.onclick = this.btnPopCancel_onClick.bindAsEventListener(this);
			
			var btnContainer = addNewContainer.find('.ConfirmButtonContainer').get(0);
			//D3.UI.renderBorder(btnContainer);
		}	
		this._commentRecord.value = "";
		this._dateRecord.value = "";
		this._hoursUsedRecord.value = "";
			$(this._addNewContainer).dialog({
				title:D3.LanguageManager.getLabel("dailyHoursUsed.popAdd.Title"),
				dialogClass: 'no-close' ,
				closeonEscape:false,
				modal:true, 
				width:"400", 
				height:"auto", 
				resizable:false,
				closable:false
				});
	}
	DailyHoursUsedLogButton.prototype.retrieveDateList = function(){
		var datelist = this._dateRecord
		D3.UI.removeAllChildNodes(datelist);
		D3.forEach(this._dayDateLookupItems._items, function(item) {
			option = document.createElement("option");
			option.setAttribute("value", item.data);
			option.appendChild(document.createTextNode(item.label));
			datelist.appendChild(option);
		});
	}
	DailyHoursUsedLogButton.prototype.btnPopConfirm_onClick = function(){
		if (this._hoursUsedRecord.value == "" || this._dateRecord.value == ""){
			alert(D3.LanguageManager.getLabel("dailyHoursUsed.popAdd.validate"));
		}else{
			var params = {};
			params._invokeCustomFilter = "saveNewDailyHoursUsedLog";
			params.descr = this._commentRecord.value;
			params.txtHours = this._hoursUsedRecord.value;
			params.cbDayDate = this._dateRecord.value;
			params.bharunUid = this._bharunUid;
			params.bhaComponentUid = this._bhaComponentUid;
			this._node.commandBeanProxy.customInvoke(params,this.addNewCompleted,this);
		}
	}
	DailyHoursUsedLogButton.prototype.addNewCompleted = function(){
		$(this._addNewContainer).dialog("close");
		this.loadDailyHoursUsedLog();
	}
	DailyHoursUsedLogButton.prototype.btnPopCancel_onClick = function(){
		$(this._addNewContainer).dialog("close");
	}
	
	DailyHoursUsedLogButton.prototype.btnDeleteSelected_onClick = function(){
		var selectedItems = [];
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var item = this._dailyHoursUsedLogList[i];
			if (item.selected)
				selectedItems.push(item);
		}
		if (selectedItems.length>0){
			var deleteSelected = false;
			deleteSelected = confirm(D3.LanguageManager.getLabel("alert.delete"));
			if(deleteSelected) {
				for (var key in selectedItems){
					var record = selectedItems[key].action = "delete";
				}
				this.processRecords();
			}
		}else{
			alert(D3.LanguageManager.getLabel("dailyHoursUsed.pop.alert.delete.noRecordSelected"));
		}
	}
	DailyHoursUsedLogButton.prototype.updateRecordAction = function(row){
		var uiComponents = row.field["_recordAction"]._uiComponents;
		
		if (row.action){
			if (row.action == "delete"){
				$(row.refRow).addClass("DeleteRecordBackground");
				uiComponents["check"].removeAttr("checked");
			}
			uiComponents["check"].addClass("hide");
			uiComponents["delete"].addClass("hide");
			uiComponents["cancel"].removeClass("hide");
			
		}else{
			$(row.refRow).removeClass("DeleteRecordBackground");
			uiComponents["check"].removeClass("hide");
			uiComponents["delete"].removeClass("hide");
			uiComponents["cancel"].addClass("hide");
		}
	}
	DailyHoursUsedLogButton.prototype.processRecords = function(){
		if (!this._processDialog)
			this._processDialog = $("<div>"+D3.LanguageManager.getLabel("dailyHoursUsed.pop.label.processing")+"</div>");
		this._processDialog.dialog({dialogClass: 'no-close' ,closeonEscape:false,closable:false,modal:true,title:D3.LanguageManager.getLabel("alert.warning.title")});
		
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			this.deleteRecords(record);
		}
		this.loadDailyHoursUsedLog();
	}
	
	DailyHoursUsedLogButton.prototype.deleteRecords = function(record){
		if (record.action == "delete"){
			var params = {};
			params._invokeCustomFilter = "delDailyHoursUsedLog";
			params.bhaComponentHoursUsedUid = record.data.bhaComponentHoursUsedUid;
			params.bharunUid = this._bharunUid;
			params.bhaComponentUid = this._bhaComponentUid;
			this._node.commandBeanProxy.customInvoke(params,this.completed,this);
		}
	}
	DailyHoursUsedLogButton.prototype.completed = function(response){
		if (this._processDialog)
			this._processDialog.dialog("close");
	}
	DailyHoursUsedLogButton.prototype.updateRootContainer = function(){
		var hasAnythingToSubmit = false;
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			if (record.action)
				hasAnythingToSubmit = true;
		}
		if (hasAnythingToSubmit){
			$(this._standardButtonContainer).addClass("hide");
			$(this._confirmCancelButtonContainer).removeClass("hide");
		}else{
			$(this._standardButtonContainer).removeClass("hide");
			$(this._confirmCancelButtonContainer).addClass("hide");
		}
	}
	
	DailyHoursUsedLogButton.prototype.btnClose_onClick = function(){
		$(this._container).dialog("close");
	}
	DailyHoursUsedLogButton.prototype.btnConfirm_onClick = function(){
		this.processRecords();
	}
	DailyHoursUsedLogButton.prototype.btnCancel_onClick = function(){
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			this.setAction(record,null);
		}
	}
	DailyHoursUsedLogButton.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th style='"+(col.width?("width:"+col.width+";"):"")+"' "+(col.align?("align='"+col.align+"'"):"")+">"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}
	DailyHoursUsedLogButton.prototype.loadDailyHoursUsedLog = function(){
		var params = {};
		
		params._invokeCustomFilter = "getDailyHoursUsedLogList";
		params.bhaComponentUid = this._bhaComponentUid;
		params.bharunUid = this._bharunUid;
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	DailyHoursUsedLogButton.prototype.loaded = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("DailyHoursUsedLog").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.bhaComponentHoursUsedUid = data.attr("bhaComponentHoursUsedUid");
			newItem.comment = data.attr("comment");
			newItem.hoursUsed = data.attr("hoursUsed");
			newItem.dailyUid = data.attr("dailyUid");
			newItem.dayNumDate = "Day " + data.attr("dayNumber") + " " + data.attr("daydate");

			var record = {};
			record.data = newItem;
			record.uid = newItem.bhaComponentHoursUsedUid;
			record.selected = 0;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		this._dailyHoursUsedLogList = collection;
		D3.UI.removeAllChildNodes(this._tbody);
		var lookupReference = new D3.LookupReference();
		doc.find("DayDateLookup").each(function(index,item){
			var datedata = $(this);
			var newDayItem = new D3.LookupItem();
			newDayItem.data = datedata.attr("dailyUid");
			newDayItem.label = "Day " + datedata.attr("dayNumber") + " " + datedata.attr("daydate");
			lookupReference.addLookupItem(newDayItem);
		});
		this._dayDateLookupItems = lookupReference;
		this.updateHoursUsedLogList();
		this.updateRootContainer();
	}
	
	DailyHoursUsedLogButton.prototype.updateHoursUsedLogList = function(){
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var row = this._dailyHoursUsedLogList[i];
			var tr = document.createElement("tr");
			if (i%2==1) tr.className="SimpleGridDataRow AlternateDataRowBackground";
			else tr.className="SimpleGridDataRow"; 
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				if (col.align){
					td.align=col.align;
				}
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	DailyHoursUsedLogButton.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'/>");
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.check_onclick.bindAsEventListener(this);
			var btnDelete = $("<button class='RecordActionsDelete'>");
			btnDelete.data({uid:record.uid});
			btnDelete.get(0).onclick = this.btnDelete_onclick.bindAsEventListener(this);
			var btnCancel = $("<button class='RecordActionsCancel hide'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>");
			btnCancel.data({uid:record.uid});
			btnCancel.get(0).onclick = this.btnRecordCancel_onclick.bindAsEventListener(this);
			$(columnContainer).append(checkbox);
			$(columnContainer).append(btnDelete);
			$(columnContainer).append(btnCancel);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox,"delete":btnDelete,"cancel":btnCancel};
		}
	}
	DailyHoursUsedLogButton.prototype.btnDelete_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,"delete");
	}
	DailyHoursUsedLogButton.prototype.btnRecordCancel_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,null);
		
	}
	DailyHoursUsedLogButton.prototype.setAction = function(record,action){
		if (action === "delete"){
			record.action = "delete";
		}else if (action === "save"){
			record.action = "save";
			this.setEditMode(record,true);
		}else if (!action){
			record.action = null;
			this.setEditMode(record,false);
			record.modified = {};
		}
		this.updateRecordAction(record);
		this.updateRootContainer();
	}
	DailyHoursUsedLogButton.prototype.setEditMode = function(row,editMode){
		row.editMode = editMode;
		var fields = row.field;
		
		for (var key in fields){
			var field = fields[key];
			var hasEditor = field._editor != null;
			if (field._renderer){
				if (editMode && hasEditor)
					$(field._renderer).addClass("hide");
				else
					$(field._renderer).removeClass("hide");
			}
			if (field._editor){
				if (editMode && hasEditor){
					$(field._editor).removeClass("hide");
					this.refreshFieldEditor(row,key);
				}else
					$(field._editor).addClass("hide");
			}
		}
	}
	DailyHoursUsedLogButton.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	DailyHoursUsedLogButton.prototype.getRecord = function(uid){
		for (var i=0;i<this._dailyHoursUsedLogList.length;i++){
			var record = this._dailyHoursUsedLogList[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	DailyHoursUsedLogButton.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		var value = record.data[fieldName];
		var span = document.createElement("span");
		span.className = "fieldRenderer";
		$(span).text(value);
		return span;
	}
	DailyHoursUsedLogButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(BharunNodeListener,D3.EmptyNodeListener);
	
	function BharunNodeListener() {
	}
		
	BharunNodeListener.prototype.customButtonTriggered = function(node, id, button){
		this._node = node;
		if (id == "continue_bha_from_previous_run") {
			this.continueFromPreviousRunPopup(node, id, button);
		}else if (id == "continue_component_from_previous_run"){
			this.continueFromPreviousRunPopup(node, id, button);
		} 
	};
	
	BharunNodeListener.prototype.continueFromPreviousRunPopup = function(node, id, button){
		
		this._node = node;
		this._action = id;
		
		this._pnlEditor = this.createElement("div");
		var datumHBox = this.createElement("div",null,"centerdiv");	
		datumHBox.appendChild(this.getMainContain());
		this._pnlEditor.appendChild(datumHBox);
		this._pnlEditor.self = this;
			
		var btnConfirm = {
				text:D3.LanguageManager.getLabel("button.confirm"),
				"class":"PopupConfirmButton",
				click:function(){
					$(this).dialog('close');
					$(this).get(0).self.btnConfirm_onClick(node);
				}
		};
		var btnCancel = {
				text:D3.LanguageManager.getLabel("button.cancel"),
				"class":"PopupCancelButton",
				click:function(){
				//	$(this).get(0).self.btnCancel_onClick(node);
	                $(this).dialog('close');
				}
		};
		var btnClose = {
				text:D3.LanguageManager.getLabel("button.close"),
				"class":"RootButton",
				click:function(){
	                $(this).dialog('close');
				}
		};
		$(this._pnlEditor).dialog({
		        resizable: false,
		        width:300,
		        height:120,
		        modal: true,
		        draggable:false,
		        title: D3.LanguageManager.getLabel("popup.continueFromPrevTitle"),	        
		        open: function(event, ui) { 
		            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		        },
		    });
		if (this._bhaNumber._items.length>0){
			$(this._pnlEditor).dialog('option','buttons',[btnConfirm,btnCancel]);
		}else{
			$(this._pnlEditor).dialog('option','buttons',[btnClose]);
		}
	}
	BharunNodeListener.prototype.btnConfirm_onClick = function(node){
		node.commandBeanProxy.addAdditionalFormRequestParams(this._action, this._combo.value);
		node.commandBeanProxy.addAdditionalFormRequestParams("_action", this._action);
		if (this._action=="continue_bha_from_previous_run"){
			node.commandBeanProxy.submitForm();
		}else{
			node.commandBeanProxy.submitForServerSideProcess(node, null, true);
		}
	}
	BharunNodeListener.prototype.btnCancel_onClick = function(node){
		node.cancelAllActions();
	}
	BharunNodeListener.prototype.getMainContain = function(){
		this._bhaNumber = this.loadBhaNumber();
		var main = this.createElement("div",null,"VBox");
		if (this._bhaNumber._items.length > 0){
			main.appendChild(this.createElement("label",D3.LanguageManager.getLabel("popup.bhaLabel")+"  ","popupText"));
			this._combo = this.createElement("select",null,"fieldEditor comboBox");
			this._bhaNumber.forEachActiveLookup(this, function(item) {
				if (item && !item.firstBlankItem) {
					var option = document.createElement("option");
					option.value = item.data;
					option.appendChild(document.createTextNode(item.label));
					this._combo.appendChild(option);
				}
			});
			main.appendChild(this._combo);
		}else{
			main.appendChild(this.createElement("label",D3.LanguageManager.getLabel("warning.noBharunNumber"),"popupText"));
		}
		return main;
	}
	BharunNodeListener.prototype.loadBhaNumber = function(){
		var params = {};
		
		params._invokeCustomFilter = "flexBhaRunNumber";
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();

		$(response).find('item').each(function(){
			var newItem = new D3.LookupItem();
			newItem.data = $(this).attr('key');
			newItem.label = $(this).attr('value');
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}

	BharunNodeListener.prototype.createElement = function(tag,text,css,attributes, callbacks){
		var element = document.createElement(tag);
		if (text)
			$(element).text(text);
		if (css)
			$(element).addClass(css);
		if (attributes){
			for (var key in attributes)
			{
				element.setAttribute(key,attributes[key]);
			}
		}
		
		if (callbacks){
			for (key in callbacks)
			{
				element[key] = callbacks[key].bindAsEventListener(this);
			}
		}
		return element;
	};
	
	BharunNodeListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		var btn = null;
		if (id == "continue_bha_from_previous_run") {
			btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>");
			return btn.get(0);
		}else if (id == "continue_component_from_previous_run"){
			var count = 0;
			D3.forEach(parentNode.children["BhaComponent"]._items, function(item) {
				count = count + 1;
			}, this);
			if (count==0){
				btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>");
				return btn.get(0);
			}else return null;
		} else if (id == "witsml-import-tubular") {
			var bharunUid = parentNode.getFieldValue("bharunUid");
			if(bharunUid && bharunUid != ""){
				this.WITSMLImportButton = new D3.WITSMLImportButton(parentNode.commandBeanProxy, buttonDeclaration, parentNode);
				this.btnSnapshot = this.WITSMLImportButton.getButton();
				this.btnSnapshot.className = "DefaultButton";
				return this.btnSnapshot;
			}
			return null;
		} else {
			return null;
		}
	};

	BharunNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id=="bitrunNumLink"){
			return new BitrunNumberLinkField();
		}else if (id=="bhaRunNumber") {
			return new BharunNumberCustomField();
		}else if (id=="dailyHoursUsedLog") {
			return new DailyHoursUsedLogButton();
		}else if (id=="BharunCustomTimeField") {
			return new BharunCustomTimeField();
		}else if (id=="checkbox") {
			return new D3.CheckboxField();
		}else if (id=="serviceLog") {
			return new ServiceLogButton();
		}else {
			return null;
		}
	}
	
	D3.CommandBeanProxy.nodeListener = BharunNodeListener;
})(window);