(function(window){
	
	D3.inherits(MultiOperationByYearCustomField,D3.AbstractFieldComponent);
	
	function MultiOperationByYearCustomField() {
		D3.AbstractFieldComponent.call(this);
		this._yearList = null;
		this._operationList = null;
	}
	
	MultiOperationByYearCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._yearSelected = this._node.getFieldValue("@yearSelected");
			this._reportSelectedOperations = this._node.getFieldValue("@reportSelectedOperations");
			this._yearList = this._retrieveYearList();
			this._operationList = this._retrieveOperationList();
			
			var tableConfig = "<table cellspacing='0' cellpadding='0' width='100%'>" +
			"<tr>" +
				"<td valign='top'>" +
					"<div style='overflow: hidden'>" +
						"<div class='textBold floatItem-Left'>"+D3.LanguageManager.getLabel("label.yearSelect")+"</div>" +
						"<div class='floatItem-Left'>" +
							"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='report-yearList'></ul>" +
						"</div>" +
						"<div class='divider'></div>" +
						"<div class='multiselect-Title floatItem-Left'>"+D3.LanguageManager.getLabel("label.selectedOperations")+"</div>" +
						"<div class='multiselect operationSelector operationMultiSelect floatItem-Left'>" +
								"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='report-operationList fieldEditor'></ul>" +
								"<div class='selectAll-ButtonContainer'></div>" +
						"</div>" +
					"</div>" +
				"</td>" +
			"</tr></table>";
			var table = $(tableConfig);
			
			this._yearSelector = table.find(".report-yearList").get(0);
			this._yearSelector.onchange = this._btnYearSelector_onClick.bindAsEventListener(this);
			if(this._yearList.length > 0) {
				this._populateYearList();
			}
			
			this._operationSelector = table.find(".report-operationList").get(0);
			this._selectAllBox = table.find(".selectAll-ButtonContainer").get(0);
			if(this._operationList._items.length > 0) {
				this._populateOperationList();
				this._generateButtons();
			}
			
			this._fieldEditor = table.get(0);
		}
		return this._fieldEditor;
	}
	
	MultiOperationByYearCustomField.prototype._generateButtons = function(){	
		var btnSelectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.selectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnSelectAll_onClick
				},
				context:this
		}
		this._btnSelectAll = D3.UI.createElement(btnSelectAllConfig);
		
		var btnUnselectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnUnselectAll_onClick
				},
				context:this
		}
		this._btnUnselectAll = D3.UI.createElement(btnUnselectAllConfig);
		
		this._selectAllBox.appendChild(this._btnSelectAll);
		this._selectAllBox.appendChild(this._btnUnselectAll);
	}
	
	MultiOperationByYearCustomField.prototype._btnSelectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = true;
		}
		this._updateSelectedOperations();
	}
	
	MultiOperationByYearCustomField.prototype._btnUnselectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = false;
		}
		this._updateSelectedOperations();
	}
	
	MultiOperationByYearCustomField.prototype._btnYearSelector_onClick = function() {
		this._yearSelected = $('#selectedYear').val();
		this._node.setFieldValue("@yearSelected", this._yearSelected);
		this._populateOperationList();
	}
	
	MultiOperationByYearCustomField.prototype._populateYearList = function(){	
		var yearList = this._yearList;
		var select = document.createElement("select");
		select.id = "selectedYear";
		$(select).addClass("fieldEditor");
		for(var i = 0; i < yearList.length; i++) {
			var year = yearList[i];
			var e = document.createElement("option");
			e.textContent = year;
			e.value = year;
			select.appendChild(e);
		}
		this._yearSelector.appendChild(select);
	}
	
	MultiOperationByYearCustomField.prototype._retrieveYearList = function(){		
		var params = {};
		params._invokeCustomFilter = "loadYearList";
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var yearList = [];
		
		$(response).find('Years').each(function(){
			yearList.push($(this).find("year").text());
		});
		return yearList;
	}
	
	MultiOperationByYearCustomField.prototype._populateOperationList = function(){	
		this._operationList = this._retrieveOperationList();
		
		var container = this._operationSelector;
		D3.UI.removeAllChildNodes(container);
		this._checkboxes = [];
		D3.forEach(this._operationList.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this);
				li.appendChild(cb);
				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				$(li).attr('title', item.label);
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		this._updateSelectedOperations();
	}
	
	MultiOperationByYearCustomField.prototype.btnOperation_onClick = function(){
		this._updateSelectedOperations();
	}
	
	MultiOperationByYearCustomField.prototype._retrieveOperationList = function(){
		var params = {};
		params._invokeCustomFilter = "flexMultiWellReportLoadOperationList";
		params.selectedyear = this._yearSelected;
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		$(response).find('Operation').each(function(){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("operationUid").text();
			newItem.label = item.find("operationName").text();
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	
	MultiOperationByYearCustomField.prototype._updateSelectedOperations = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue("@reportSelectedOperations", id_list);
	}
	
	
	D3.inherits(MultiOperationCustomField,D3.AbstractFieldComponent);
	
	function MultiOperationCustomField() {
		D3.AbstractFieldComponent.call(this);
		this._operationList = null;
	}
	
	MultiOperationCustomField.prototype.getFieldEditor = function() {
		if (!this._fieldEditor) {
			this._reportSelectedOperations = this._node.getFieldValue("@reportSelectedOperations");
			this._operationList = this._retrieveOperationList();
			
			var tableConfig = "<table cellspacing='0' cellpadding='0' width='100%'>" +
			"<tr>" +
				"<td valign='top'>" +
					"<div style='overflow: hidden'>" +
						"<div class='divider'></div>" +
						"<div class='multiselect-Title floatItem-Left'>"+D3.LanguageManager.getLabel("label.selectedOperations")+"</div>" +
						"<div class='fieldEditor multiselect operationSelector'>" +
							"<div class='operationMultiSelect floatItem-Left'>" +
								"<ul style='list-style-type: none; margin: 0px; padding: 0px;' class='report-operationList'></ul>" +
								"<div class='selectAll-ButtonContainer'></div>" +
							"</div>" +
						"</div>" +
					"</div>" +
				"</td>" +
			"</tr></table>";
			var table = $(tableConfig);
			
			this._operationSelector = table.find(".report-operationList").get(0);
			this._selectAllBox = table.find(".selectAll-ButtonContainer").get(0);
			if(this._operationList._items.length > 0) {
				this._populateOperationList();
				this._generateButtons();
			}
			
			this._fieldEditor = table.get(0);
		}
		return this._fieldEditor;
	}
	
	MultiOperationCustomField.prototype._generateButtons = function(){	
		var btnSelectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.selectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnSelectAll_onClick
				},
				context:this
		}
		this._btnSelectAll = D3.UI.createElement(btnSelectAllConfig);
		
		var btnUnselectAllConfig = {
				tag:"button",
				text:D3.LanguageManager.getLabel("button.unselectAll"),
				css:"DefaultButton",
				callbacks:{
					onclick:this._btnUnselectAll_onClick
				},
				context:this
		}
		this._btnUnselectAll = D3.UI.createElement(btnUnselectAllConfig);
		
		this._selectAllBox.appendChild(this._btnSelectAll);
		this._selectAllBox.appendChild(this._btnUnselectAll);
	}
	
	MultiOperationCustomField.prototype._btnSelectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = true;
		}
		this._updateSelectedOperations();
	}
	
	MultiOperationCustomField.prototype._btnUnselectAll_onClick = function() {
		for (var key in this._checkboxes) {
			this._checkboxes[key].checked = false;
		}
		this._updateSelectedOperations();
	}
	
	MultiOperationCustomField.prototype._populateOperationList = function(){	
		this._operationList = this._retrieveOperationList();
		
		var container = this._operationSelector;
		D3.UI.removeAllChildNodes(container);
		this._checkboxes = [];
		D3.forEach(this._operationList.getActiveLookup(), function(item) {
			if (item && !item.firstBlankItem) {
				li = document.createElement("li");
				li.setAttribute("style", 'white-space: nowrap;');
				var uid = this._node.uid + "_" + item.data;
				var cb = document.createElement("input");
				cb.setAttribute("id", uid);
				cb.setAttribute("type", "checkbox");
				cb.value = item.data;
				cb.onclick = this.dataChangeListener.bindAsEventListener(this);
				this._checkboxes[item.data] = cb;
				this._checkboxes[item.data].onclick = this.btnOperation_onClick.bindAsEventListener(this);
				li.appendChild(cb);
				var label = document.createElement("label");
				label.setAttribute("for", uid);
				label.appendChild(document.createTextNode(item.label));
				$(li).attr('title', item.label);
				li.appendChild(label);
				container.appendChild(li);
			}
		}, this);
		this._updateSelectedOperations();
	}
	
	MultiOperationCustomField.prototype.btnOperation_onClick = function(){
		this._updateSelectedOperations();
	}
	
	MultiOperationCustomField.prototype._retrieveOperationList = function(){
		var params = {};
		params._invokeCustomFilter = "flexMultiWellReportLoadOperationList";
		params._action = this._action;
		var response = this._node.commandBeanProxy.customInvokeSynchronous(params);
		var lookupReference = new D3.LookupReference();
		
		$(response).find('Operation').each(function(){
			var newItem = new D3.LookupItem();
			var item = $(this);
			newItem.data = item.find("operationUid").text();
			newItem.label = item.find("operationName").text();
			lookupReference.addLookupItem(newItem);
		});
		return lookupReference;
	}
	
	MultiOperationCustomField.prototype._updateSelectedOperations = function(){
		var id_list = "";
		for (var key in this._checkboxes) {
			if (this._checkboxes[key].checked){
				if(id_list == ""){
					id_list = key;
				}else{
					id_list += "," + key;
				}
			}
		}
		this._node.setFieldValue("@reportSelectedOperations", id_list);
	}
	
	D3.inherits(ListAllReportField,D3.AbstractFieldComponent);
	function ListAllReportField(){
		D3.AbstractFieldComponent.call(this);
		this._pendingProcess = [];
		this._columns = [
		     {
		    	 type: "recordAction",
		    	 width:"50px"
		     },
		     {
		    	 type:"field",
		    	 field:"isPrivate",
		    	 labelField:"isPrivateLabel",
		    	 width:"105px",
		    	 title:D3.LanguageManager.getLabel("label.listAllReports.isPrivate")
		    	 
		     },
		     {
		    	 type:"field",
		    	 field:"displayName",
		    	 title:D3.LanguageManager.getLabel("label.listAllReports.displayName")
		     },
		     {
		    	 type:"field",
		    	 field:"timeReportFileCreatedText",
		    	 width:"103px",
		         title:D3.LanguageManager.getLabel("label.listAllReports.timeReportFileCreatedText")
		     },
		     {
		    	 type:"field",
		    	 field:"reportUserUid",
		    	 width:"103px",
		    	 title:D3.LanguageManager.getLabel("label.listAllReports.reportUserUid")
		     }
				
		]
	}
	ListAllReportField.prototype.getFieldRenderer = function(){
		if (!this._renderer){
			this._enableZip = this._node.getGWP("enableZip") == "1"; 
			this._renderer = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel("button.listAllReport"),
				css:"DefaultButton",
				callbacks:{
					onclick:this.btnListAllReport_onClick
				},
				context:this
			});
		}
		return this._renderer;
	}
	ListAllReportField.prototype.btnListAllReport_onClick = function(event){
		if (!this._container){
			this.initiateContainer();
		}
		
		this.loadDDRReportList();
		
		$(this._container).dialog({title:D3.LanguageManager.getLabel("label.multiReports"),dialogClass: 'no-close' ,closeonEscape:false,modal:true, width:"1024", height:"auto", resizable:false,closable:false});
	}
	ListAllReportField.prototype.getColumnHeader = function(){
		var config ="";

		for (var i=0; i<this._columns.length;i++){
			var col = this._columns[i];
			var title = "";
			if(col.title) title = col.title
			var thConfig = "<th style='"+(col.width?("width:"+col.width+";"):"")+"'>"+title+"</th>";
			config+=thConfig;
		}
		return config;
	}
	ListAllReportField.prototype.btnSelectAll_onClick = function(){
		for (var i=0;i<this._reportList.length;i++){
			var item = this._reportList[i];
			item.selected = true;
			var uiComponents = item.field["_recordAction"]._uiComponents;
			uiComponents["check"].get(0).checked = true;
		}
		$(this._btnSelectAll).addClass("hide");
		$(this._btnUnselectAll).removeClass("hide");
	}
	ListAllReportField.prototype.btnUnselectAll_onClick = function(){
		for (var i=0;i<this._reportList.length;i++){
			var item = this._reportList[i];
			item.selected = false;
			var uiComponents = item.field["_recordAction"]._uiComponents;
			uiComponents["check"].get(0).checked = false;
		}
		
		$(this._btnSelectAll).removeClass("hide");
		$(this._btnUnselectAll).addClass("hide");
	}
	ListAllReportField.prototype.deleteReport = function(uid){
		this._pendingProcess.push(uid);
		var params = {};
		params._invokeCustomFilter = "deleteReport";
		params.reportFilesUid = uid;
		this._node.commandBeanProxy.customInvoke(params,this.completed,this);
	
	}
	
	ListAllReportField.prototype.updateVisibility = function(uid,isPrivate){
		this._pendingProcess.push(uid);
		var params = {};
		params._invokeCustomFilter = "updateVisibility";
		params.reportFilesUid = uid;
		params.isPrivate = isPrivate?1:0;
		this._node.commandBeanProxy.customInvoke(params,this.completed,this);
	
	}
	
	ListAllReportField.prototype.completed = function(response){
		var uid = $(response).find("reference").text();
		if (this._pendingProcess.indexOf(uid)!=-1){
			this._pendingProcess.splice(this._pendingProcess.indexOf(uid));
		}
		if (this._pendingProcess.length==0){
			if (this._processDialog)
				this._processDialog.dialog("close");
			this.loadDDRReportList();
			
		}
	}
	ListAllReportField.prototype.processRecord = function(record){
		if (record.action == "delete"){
			this.deleteReport(record.uid);
		}else if (record.action == "save"){
			if (record.modified.hasOwnProperty("isPrivate")){
				this.updateVisibility(record.uid,record.modified.isPrivate);
			}
		}
			
	}
	ListAllReportField.prototype.processRecords = function(){
		if (!this._processDialog)
			this._processDialog = $("<div>"+D3.LanguageManager.getLabel("label.processing")+"</div>");
		this._processDialog.dialog({dialogClass: 'no-close' ,closeonEscape:false,closable:false,modal:true,title:D3.LanguageManager.getLabel("alert.warning.title")});
		
		for (var i=0;i<this._reportList.length;i++){
			var record = this._reportList[i];
			this.processRecord(record);
		}
		if (this._processDialog)
			this._processDialog.dialog("close");
		this.loadDDRReportList();
	}
	
	ListAllReportField.prototype.btnDeleteSelected_onClick = function(){
		var selectedItems = [];
		for (var i=0;i<this._reportList.length;i++){
			var item = this._reportList[i];
			if (item.selected)
				selectedItems.push(item);
		}
		if (selectedItems.length>0){
			var deleteSelected = false;
			deleteSelected = confirm(D3.LanguageManager.getLabel("alert.delete"));
			if(deleteSelected) {
				for (var key in selectedItems){
					var record = selectedItems[key].action = "delete";
				}
				this.processRecords();
			}
		}else{
			alert(D3.LanguageManager.getLabel("alert.delete.noRecordSelected"));
		}
	}
	ListAllReportField.prototype.btnClose_onClick = function(){
		$(this._container).dialog("close");
		this._node.commandBeanProxy.load();
	}
	ListAllReportField.prototype.btnConfirm_onClick = function(){
		var hasDeleted = false;
		for (var i=0;i<this._reportList.length;i++){
			var item = this._reportList[i];
			if (item.action == "delete") hasDeleted = true;
		}
		if (hasDeleted) {
			var deleteSelected = false;
			deleteSelected = confirm(D3.LanguageManager.getLabel("alert.delete"));
			if(deleteSelected) this.processRecords();
		}else this.processRecords();
	}
	ListAllReportField.prototype.btnCancel_onClick = function(){
		for (var i=0;i<this._reportList.length;i++){
			var record = this._reportList[i];
			this.setAction(record,null);
		}
	}
	ListAllReportField.prototype.loaded = function(response){
		var doc = $(response);
		var collection = [];
		doc.find("DDRReportFile").each(function(index,item){
			var newItem = {};
			var data = $(this);
			newItem.reportFilesUid = data.attr("reportFilesUid");
			newItem.isPrivate = data.attr("isPrivate");
			newItem.displayName = data.attr("displayName");
			newItem.fileExtension = data.attr("fileExtension");
			newItem.reportUserUid = data.attr("reportUserUid");
			newItem.timeReportFileCreatedText = data.attr("timeReportFileCreatedText"); //(xml.attr("newlyGeneratedFlag=="true" ? 1 : 0 );
			
			newItem.newlyGeneratedFlag = data.attr("newlyGeneratedFlag");
			newItem.selectedIndex = (data.attr("isPrivate").toString() == "true" ? 1 : 0);
			newItem.isPrivateLabel = (data.attr("isPrivate").toString() == "true" ? "Private" : "Public");
			var record = {};
			record.data = newItem;
			record.uid = newItem.reportFilesUid;
			record.selected = 0;
			record.editMode = false;
			record.action = null;
			record.modified = {};
			collection.push(record);
		});
		this._reportList = collection;
		D3.UI.removeAllChildNodes(this._tbody);
		this.updateReportList();
		this.updateRootContainer();
	}
	
	ListAllReportField.prototype.getRecord = function(uid){
		for (var i=0;i<this._reportList.length;i++){
			var record = this._reportList[i];
			if (record.uid === uid)
				return record;
		}
		return null;
	}
	
	ListAllReportField.prototype.setEditMode = function(row,editMode){
		row.editMode = editMode;
		var fields = row.field;
		
		for (var key in fields){
			var field = fields[key];
			var hasEditor = field._editor != null;
			if (field._renderer){
				if (editMode && hasEditor)
					$(field._renderer).addClass("hide");
				else
					$(field._renderer).removeClass("hide");
			}
			if (field._editor){
				if (editMode && hasEditor){
					$(field._editor).removeClass("hide");
					this.refreshFieldEditor(row,key);
				}else
					$(field._editor).addClass("hide");
			}
		}
	}
	
	ListAllReportField.prototype.updateRootContainer = function(){
		var hasAnythingToSubmit = false;
		for (var i=0;i<this._reportList.length;i++){
			var record = this._reportList[i];
			if (record.action)
				hasAnythingToSubmit = true;
		}
		if (hasAnythingToSubmit){
			$(this._standardButtonContainer).addClass("hide");
			$(this._confirmCancelButtonContainer).removeClass("hide");
		}else{
			$(this._standardButtonContainer).removeClass("hide");
			$(this._confirmCancelButtonContainer).addClass("hide");
		}
	}
	
	ListAllReportField.prototype.updateRecordAction = function(row){
		var uiComponents = row.field["_recordAction"]._uiComponents;
		
		
		if (row.action){
			if (row.action == "delete"){
				$(row.refRow).addClass("DeleteRecordBackground");
				uiComponents["check"].removeAttr("checked");
			}
			uiComponents["check"].addClass("hide");
			uiComponents["delete"].addClass("hide");
			uiComponents["cancel"].removeClass("hide");
			
		}else{
			$(row.refRow).removeClass("DeleteRecordBackground");
			uiComponents["check"].removeClass("hide");
			uiComponents["delete"].removeClass("hide");
			uiComponents["cancel"].addClass("hide");
			
		}
	}
	ListAllReportField.prototype.updateReportList = function(){
		for (var i=0;i<this._reportList.length;i++){
			var row = this._reportList[i];
			var tr = document.createElement("tr");
			tr.className="SimpleGridDataRow";
			this._tbody.appendChild(tr);
			row.refRow = tr;
			
			tr.ondblclick = this.row_onDoubleClick.bindAsEventListener(this);
			row.field = {};
			for (var j=0; j<this._columns.length;j++){
				var col = this._columns[j];
				var td = document.createElement("td");
				td.className="SimpleGridDataCell";
				tr.appendChild(td);
				this.generateDataColumn(tr,row,col,td);
			}
			$(tr).data({uid:row.uid});
		}
	}
	
	ListAllReportField.prototype.refreshFieldEditor = function(record,fieldName){
		var value = record.data[fieldName];
		if (record.modified && record.modified.hasOwnProperty("isPrivate"))
			value = record.modified[fieldName];
		if (fieldName == "isPrivate"){
			$(record.field[fieldName]._editor).val(value=="true"?"1":"0");
		}
	}
	ListAllReportField.prototype.getEditor = function(record,column,columnContainer){
		var fieldName = column.field;
		if (fieldName == "isPrivate"){
			var config = "<select class='fieldEditor combobox hide'>" +
							"<option value='0'>Public</option>" +
							"<option value='1'>Private</option>" +
						"</select>";
			var combo = $(config).get(0);
			$(combo).data({uid:record.uid});
			$(combo).val(record.data["selectedIndex"]);
			combo.onchange = this.isPrivate_onChange.bindAsEventListener(this);
			return combo;
		}	
	}
	ListAllReportField.prototype.isPrivate_onChange = function(event){
		var target =$(event.currentTarget);
		var uid = target.data().uid;
		var record = this.getRecord(uid);
		record.modified.selectedIndex = target.val();
		record.modified.isPrivate = target.val()==1?true:false;
		 
	}
	ListAllReportField.prototype.reportLink_onClick = function(event){
		var comp = event.currentTarget;
		var uid = $(comp).data().uid;
		var data = this.getRecord(uid);
		var fileUrl = "abaccess/download/" + encodeURIComponent(D3.Common.replaceInvalidCharInFileName(data.data["displayName"])) + "." + data.data["fileExtension"] + "?handlerId=ddr&reportId=" + encodeURIComponent(data.data["reportFilesUid"]);
		this._node.commandBeanProxy.openDownloadWindow(fileUrl);
	}
	ListAllReportField.prototype.zipButton_onClick = function(event){
		var comp = event.currentTarget;
		var uid = $(comp).data().uid;
		var data = this.getRecord(uid);
		var reportFilesUid = encodeURIComponent(data.data["reportFilesUid"]);
		var displayName = encodeURIComponent(D3.Common.replaceInvalidCharInFileName(data.data["displayName"]));
		var file_url = "abaccess/download/" + displayName + ".zip" + "?handlerId=ddr&zip=true&reportId=" + reportFilesUid + "&name=" + displayName + "." + data.data["fileExtension"];
		this._node.commandBeanProxy.openDownloadWindow(file_url);
	}
	
	ListAllReportField.prototype.getRenderer = function(record,column,columnContainer){
		var fieldName = column.field;
		if (column.labelField){
			fieldName = column.labelField;
		}
		
		var value = record.data[fieldName];
		
		if (fieldName === "displayName"){
			var layout = "<div class='reportField'><a class='hyperlinkField reportLink'></a>" +
			(this._enableZip?"<img class='zipIcon'  title='"+D3.LanguageManager.getLabel("tooltip.zip")+"'></img>":"")+
			"<button class='DefaultButton newReport'>"+D3.LanguageManager.getLabel("label.newReport")+"</button>" +
			"</div>";
			var renderer = $(layout);
			
			var reportLink = renderer.find(".reportLink").get(0);
			$(reportLink).text(record.data["displayName"]);
			$(reportLink).data({uid:record.uid});
			reportLink.onclick = this.reportLink_onClick.bindAsEventListener(this);
			
			if (this._enableZip){
				var zipButton = renderer.find(".zipIcon").get(0);
				$(zipButton).data({uid:record.uid});
				zipButton.onclick = this.zipButton_onClick.bindAsEventListener(this);
			}
			
			var newIcon = renderer.find(".newReport").get(0);
			
			var renderer = renderer.get(0);
			return renderer;
		}else{
			var span = document.createElement("span");
			span.className = "fieldRenderer";
			$(span).text(value);
			return span;
		}
		
	}
	ListAllReportField.prototype.check_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		data.selected = $(target).val();
	}
	ListAllReportField.prototype.btnDelete_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,"delete");
	}
	ListAllReportField.prototype.row_onDoubleClick= function(event){
		var target = event.currentTarget;
		var uid = $(target).data().uid;
		var row = this.getRecord(uid);
		if (!row.editMode){
			this.setAction(row,"save");
			
		}
	}
	ListAllReportField.prototype.setAction = function(record,action){
		if (action === "delete"){
			record.action = "delete";
		}else if (action === "save"){
			record.action = "save";
			this.setEditMode(record,true);
		}else if (!action){
			record.action = null;
			this.setEditMode(record,false);
			record.modified = {};
		}
		this.updateRecordAction(record);
		this.updateRootContainer();
	}
	
	
	ListAllReportField.prototype.btnRecordCancel_onclick = function(event){
		var target = event.target;
		var uid = $(target).data().uid;
		var data = this.getRecord(uid);
		this.setAction(data,null);
		
	}
	ListAllReportField.prototype.generateDataColumn = function(elemTR, record,column,columnContainer){
		if (column.type == "field"){
			 record.field[column.field] = {};
			 var renderer = this.getRenderer(record,column,columnContainer);
			 if (renderer){
				 record.field[column.field]._renderer = renderer;
				 columnContainer.appendChild(renderer);
			 }
			 var editor = this.getEditor(record,column,columnContainer);
			 if (editor){
				 record.field[column.field]._editor = editor;
				 columnContainer.appendChild(editor);
			 }
		}else{
			var checkbox = $("<input type='checkbox' class='RecordActionsCheckbox'></input>");
			checkbox.data({uid:record.uid});
			checkbox.get(0).onclick = this.check_onclick.bindAsEventListener(this);
			var btnDelete = $("<button class='RecordActionsDelete RecordActionsButton'>");
			btnDelete.data({uid:record.uid});
			btnDelete.get(0).onclick = this.btnDelete_onclick.bindAsEventListener(this);
			var btnCancel = $("<button class='RecordActionsButton RecordActionsCancel hide'></button>");
			btnCancel.data({uid:record.uid});
			btnCancel.get(0).onclick = this.btnRecordCancel_onclick.bindAsEventListener(this);
			$(columnContainer).append(checkbox);
			$(columnContainer).append(btnDelete);
			$(columnContainer).append(btnCancel);
			record.field["_recordAction"] = {};
			record.field["_recordAction"]._uiComponents = {"check":checkbox,"delete":btnDelete,"cancel":btnCancel};
		}
	}
	
	ListAllReportField.prototype.loadDDRReportList = function(){
		var params = {};
		
		params._invokeCustomFilter = "getDDRReportFileList";
		params.dailyUid = this._dailyUid;
		params.operationUid = this._operationUid;
		params.reportType = this._reportType;
		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	ListAllReportField.prototype.initiateContainer = function(){
		if (!this._container){
			var containerTemplate = "<div class='listAllReport-container'>" +
					"<div class='StandardButtonContainer'>" +
						"<button class='RootButton SelectAll'>"+D3.LanguageManager.getLabel("button.selectAll")+"</button>" +
						"<button class='RootButton UnSelectAll hide'>"+D3.LanguageManager.getLabel("button.unselectAll")+"</button>" +
						"<button class='RootButton DeleteSelected'>"+D3.LanguageManager.getLabel("button.deleteSelected")+"</button>" +
						"<button class='RootButton Close'>"+D3.LanguageManager.getLabel("button.close")+"</button>" +
					"</div>" +
				 	"<div class='ConfirmCancelButtonContainer hide'>" +
				 		"<button class='RootButton RootButtonConfirm Confirm'>"+D3.LanguageManager.getLabel("button.confirm")+"</button>" +
				 		"<button class='RootButton RootButtonCancel Cancel'>"+D3.LanguageManager.getLabel("button.cancel")+"</button>" +
	 				"</div>" +
	 				"<table class='SimpleGridTable reportListing'><thead>"+this.getColumnHeader()+"</thead><tbody class='reportListing-TBODY'></tbody></table>"+
				"</div>";
			var container = $(containerTemplate);
			this._container = container.get(0);
			this._standardButtonContainer = container.find(".StandardButtonContainer").get(0);
			//D3.UI.renderBorder(this._standardButtonContainer);
			
			this._btnSelectAll = container.find(".SelectAll").get(0);
			this._btnSelectAll.onclick = this.btnSelectAll_onClick.bindAsEventListener(this);
			this._btnUnselectAll = container.find(".UnSelectAll").get(0);
			this._btnUnselectAll.onclick = this.btnUnselectAll_onClick.bindAsEventListener(this);
			this._btnDeleteSelected = container.find(".DeleteSelected").get(0);
			this._btnDeleteSelected.onclick = this.btnDeleteSelected_onClick.bindAsEventListener(this);
			this._btnClose = container.find(".Close").get(0);
			this._btnClose.onclick = this.btnClose_onClick.bindAsEventListener(this);
		
			this._confirmCancelButtonContainer = container.find(".ConfirmCancelButtonContainer").get(0);
			//D3.UI.renderBorder(this._confirmCancelButtonContainer);
			
			this._btnConfirm = container.find(".Confirm").get(0);
			this._btnConfirm.onclick = this.btnConfirm_onClick.bindAsEventListener(this);
			this._btnCancel = container.find(".Cancel").get(0);
			this._btnCancel.onclick = this.btnCancel_onClick.bindAsEventListener(this);
			
			this._tbody = container.find(".reportListing-TBODY").get(0);
			
			this._dailyUid = this._node.getFieldValue("dailyUid");
			this._operationUid = this._node.getFieldValue("operationUid");
			this._reportType = this._node.getFieldValue("reportType");
			
		}
	}
	
	
	D3.inherits(ReportLinkField,D3.AbstractFieldComponent);
	
	function ReportLinkField(){
		D3.AbstractFieldComponent.call(this);
		this._enableZip = false;
		this._reportLink = null;
		this._zipButton = null;
		this._reportLabel = null;
		this._newIcon = null;
		
	}
	
	ReportLinkField.prototype.getNoOfReports = function(){
		var value =parseInt(this._node.getFieldValue("numberOfReport"));
		if (isNaN(value))
			return 0;
		return value;
	}
	
	ReportLinkField.prototype.reportLink_onClick = function(){
		var fileUrl = "abaccess/download/" + encodeURIComponent(D3.Common.replaceInvalidCharInFileName(this._node.getFieldValue("reportDisplayName"))) + "." + this._node.getFieldValue("fileExtension") + "?handlerId=ddr&reportId=" + encodeURIComponent(this._node.getFieldValue("reportFilesUid"));
		this._node.commandBeanProxy.openDownloadWindow(fileUrl);
	}
	ReportLinkField.prototype.zipButton_onClick = function(){
		var reportFilesUid = encodeURIComponent(this._node.getFieldValue("reportFilesUid"));
		var displayName = encodeURIComponent(D3.Common.replaceInvalidCharInFileName(this._node.getFieldValue("reportDisplayName")));
		var file_url = "abaccess/download/" + displayName + ".zip" + "?handlerId=ddr&zip=true&reportId=" + reportFilesUid + "&name=" + displayName + "." + this._node.getFieldValue("fileExtension");
		this._node.commandBeanProxy.openDownloadWindow(file_url);
	}
	
	ReportLinkField.prototype.refreshFieldRenderer = function(){
		$(this._reportLink).text(this.getFieldValue());
		
		if (this._node.getFieldValue("newlyGeneratedFlag") == "1") {
			$(this._newIcon).removeClass("hide");
		}else{
			$(this._newIcon).addClass("hide");
		}
		var reportFilesUid = this._node.getFieldValue("reportFilesUid");
		var noOfReports = this.getNoOfReports();
		if (!reportFilesUid ||reportFilesUid === "" || !this._enableZip || noOfReports >1){
			$(this._zipButton).addClass("hide");
		}else{
			$(this._zipButton).removeClass("hide");
		}
		
		if (noOfReports>1){
			$(this._reportLink).addClass("hide");
			$(this._reportLabel).removeClass("hide");
			$(this._reportLabel).text(D3.LanguageManager.getLabel("label.noOfReports")+" "+noOfReports);
			$(this._newIcon).addClass("hide");
		}else{
			$(this._reportLabel).addClass("hide");
			$(this._reportLink).removeClass("hide");
		}
	}
	ReportLinkField.prototype.getFieldRenderer = function(){
		if (!this._renderer){
			
			var layout = "<div class='reportField'><a class='hyperlinkField reportLink'></a>" +
				(this._enableZip?"<img class='zipIcon' title='"+D3.LanguageManager.getLabel("tooltip.zip")+"' alt=''></img>":"")+
				"<button class='DefaultButton newReport'>"+D3.LanguageManager.getLabel("label.newReport")+"</button>" +
				"<span class='reportLabel hide'></span></div>";
			var renderer = $(layout);
			
			this._reportLink = renderer.find(".reportLink").get(0);
			if (this.getNoOfReports()<2){
				this._reportLink.onclick = this.reportLink_onClick.bindAsEventListener(this);
			}
			if (this._enableZip){
				this._zipButton = renderer.find(".zipIcon").get(0);
				this._zipButton.onclick = this.zipButton_onClick.bindAsEventListener(this);
			}
			this._reportLabel = renderer.find(".reportLabel").get(0);
			this._newIcon = renderer.find(".newReport").get(0);
			
			this._renderer = renderer.get(0);
		}
		return this._renderer;
	}
	
	ReportLinkField.prototype.data = function(data){
		ReportLinkField.uber.data.call(this,data);
		this._enableZip = this._node.getGWP("enableZip") == "1";
	}
	
	
	D3.inherits(DefaultReportListener,D3.EmptyNodeListener);
	
	function DefaultReportListener()
	{
		
	}
	
	DefaultReportListener.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		if (!this._commandBeanProxy){
			this._commandBeanProxy = commandBeanProxy;
			this._commandBeanProxy.addEventListener(D3.CommandBeanProxyEvent.LOAD_COMPLETED, this.commandBeanLoadCompleted,this);
			DefaultReportListener.GENERATE_REPORT = D3.LanguageManager.getLabel("button.generateReport");
			DefaultReportListener.GENERATE_REPORT_AND_SEND = D3.LanguageManager.getLabel("button.generateReportAndSend");
			DefaultReportListener.GENERATE_REPORT_AND_DOWNLOAD = D3.LanguageManager.getLabel("button.generateReportAndDownload");
			DefaultReportListener.GENERATE_REPORT_PLEASE_WAIT = D3.LanguageManager.getLabel("button.generateReportPleaseWait");
			this.customizeReportBtnLable();
		}
		if (id == "generateReport") {
			if (!commandBeanProxy.rootNode.isAllowedAction("generateReport", "ReportOutputFile")) {
				return null;
			}
			var label = DefaultReportListener.GENERATE_REPORT;
			if (buttonDeclaration.label)
				label = buttonDeclaration.label;
			
			this._btnGenerateReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:label,
				callbacks:{
					onclick:this.generateReport
				},
				context:this
			});
			return this._btnGenerateReport;
		}else if (id == "generateAndSent") {
			if (!commandBeanProxy.rootNode.isAllowedAction("generateReport", "ReportOutputFile")) {
				return null;
			}
			var label = DefaultReportListener.GENERATE_REPORT_AND_SEND;
			if (buttonDeclaration.label)
				label = buttonDeclaration.label;
			
			this._btnGenerateAndSendReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:label,
				callbacks:{
					onclick:this.generateAndSendReport
				},
				context:this
			});
			return this._btnGenerateAndSendReport;
		}else if (id == "generateAndDownload") {
			if (!commandBeanProxy.rootNode.isAllowedAction("generateReport", "ReportOutputFile")) {
				return null;
			}
			var label = DefaultReportListener.GENERATE_REPORT_AND_DOWNLOAD;
			if (buttonDeclaration.label)
				label = buttonDeclaration.label;
			
			this._btnGenerateAndDownloadReport = D3.UI.createElement({
				tag:"button",
				css:"DefaultButton",
				text:label,
				callbacks:{
					onclick:this.generateAndDownloadReport
				},
				context:this
			});
			return this._btnGenerateAndDownloadReport;
		}
		
	}
	
	DefaultReportListener.prototype.customizeReportBtnLable = function(){
		
	}
	
	DefaultReportListener.prototype.generateReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report","1");
		this._commandBeanProxy.submitForServerSideProcess();
		
		this._generateReportStarted = true;
	}
	
	DefaultReportListener.prototype.generateAndSendReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_and_sent","1");
		this._commandBeanProxy.submitForServerSideProcess();
		
		this._generateReportAndSendStarted = true;
	}
	
	DefaultReportListener.prototype.generateAndDownloadReport = function(){
		this._commandBeanProxy.addAdditionalFormRequestParams("custom_button_generate_report_and_download","1");
		this._commandBeanProxy.submitForServerSideProcess();
		
		this._generateReportAndDownloadStarted = true;
	}
	DefaultReportListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "reportLink"){
			return new ReportLinkField();
		} else if (id == "multiOperationOptionChooserByYear") {
			return new MultiOperationByYearCustomField();
		} else if (id == "multiOperationOptionChooser") {
			return new MultiOperationCustomField();	
		}else if (id == "listAllReport"){
			return new ListAllReportField();
		}
	}
	DefaultReportListener.prototype.commandBeanLoadCompleted = function(event) {
		this.checkingCurrentStatus();
		this._overwriteFWRMessage = ((this._commandBeanProxy.rootNode.getFieldValue("@overwriteFWRGlobalMessage")) == "1"? true : false);
		this._FWRReportFilesUid = this._commandBeanProxy.rootNode.getFieldValue("@FWRReportFilesUid");
	}

	DefaultReportListener.prototype.commandBeanSubmitCompleted = function(event) {
		this.checkingCurrentStatus();
		this._overwriteFWRMessage = ((this._commandBeanProxy.rootNode.getFieldValue("@overwriteFWRGlobalMessage")) == "1"? true : false);
		this._FWRReportFilesUid = this._commandBeanProxy.rootNode.getFieldValue("@FWRReportFilesUid");
	}
	
	DefaultReportListener.prototype.checkingCurrentStatus = function() {
		var params = {};
		params._invokeCustomFilter = "check_report_status";
		this._commandBeanProxy.customInvoke(params,this.statusCheckCompleted,this);

		if (this._btnGenerateReport != null) {
			this.enableButton(this._btnGenerateReport,false);
		}
		if (this._btnGenerateAndSendReport != null) {
			this.enableButton(this._btnGenerateAndSendReport,false);
		}
		if (this._btnGenerateAndDownloadReport != null) {
			this.enableButton(this._btnGenerateAndDownloadReport,false);
		}
	}
	
	DefaultReportListener.prototype.enableButton = function(button,value){
		if (value)
			$(button).removeAttr("disabled");
		else
			$(button).attr("disabled","disabled");
	}
	
	DefaultReportListener.prototype.setReadyToGenerateReport = function(){
		$(this._btnGenerateReport).text(D3.DefaultReportListener.GENERATE_REPORT);
		this.enableButton(this._btnGenerateReport,true);	
	}
	
	DefaultReportListener.prototype.setReadyToGenerateAndSent = function(){
		$(this._btnGenerateAndSendReport).text(D3.DefaultReportListener.GENERATE_REPORT_AND_SEND);
		this.enableButton(this._btnGenerateAndSendReport,true);
	}
	
	DefaultReportListener.prototype.setReadyToGenerateAndDownload = function(){
		$(this._btnGenerateAndDownloadReport).text(D3.DefaultReportListener.GENERATE_REPORT_AND_DOWNLOAD);
		this.enableButton(this._btnGenerateAndDownloadReport,true);	
	}
	
	DefaultReportListener.prototype.setGeneratingReportInProgressAndCheckAgainLater = function(){
		$(this._btnGenerateReport).text(D3.DefaultReportListener.GENERATE_REPORT_PLEASE_WAIT);
		this.enableButton(this._btnGenerateReport,false);	

		this.callLaterToCheckStatus();
	}
	
	DefaultReportListener.prototype.setGenerateAndSentInProgressAndCheckAgainLater = function(){
		$(this._btnGenerateAndSendReport).text(D3.DefaultReportListener.GENERATE_REPORT_PLEASE_WAIT);
		this.enableButton(this._btnGenerateAndSendReport,false);	

		this.callLaterToCheckStatus();
	}
	
	DefaultReportListener.prototype.setGeneratingReportAndDownloadInProgressAndCheckAgainLater = function(){
		$(this._btnGenerateAndDownloadReport).text(D3.DefaultReportListener.GENERATE_REPORT_PLEASE_WAIT);
		this.enableButton(this._btnGenerateAndDownloadReport,false);	

		this.callLaterToCheckStatus();
	}
	
	DefaultReportListener.prototype.callLaterToCheckStatus = function(){
		var self = this;
		setTimeout(function(){self.checkingCurrentStatus()},1000);
	}
	
		
	DefaultReportListener.prototype.reloadParentPage = function(){
		this._commandBeanProxy.load();	
	}
	
	
	DefaultReportListener.prototype.statusCheckCompleted = function(response) {
		var doc = $(response);
		var code = doc.find("responseCode").text();
		if (this._generateReportStarted) {
			if (code == "no_job") {
				this._generateReportStarted = false;
				this.setReadyToGenerateReport();
			} else if (code == "job_done") {
				this._generateReportStarted = false;
				this.reloadParentPage();
			} else if (code == "job_error") {
				this._generateReportStarted = false;
				this.setReadyToGenerateReport();
				alert("Error Generating Report");
			} else if (code == "job_running") {
				this.setGeneratingReportInProgressAndCheckAgainLater();
			}
		} else if (this._generateReportAndSendStarted) {
			if (code == "no_job") {
				this._generateReportAndSendStarted = false;
				this.setReadyToGenerateAndSent();
			} else if (code == "job_done") {
				this._generateReportAndSendStarted = false;
				this.reloadParentPage();
			} else if (code == "job_error") {
				this._generateReportAndSendStarted = false;
				this.setReadyToGenerateAndSent();
				alert("Error Generating Report");
			} else if (code == "job_running") {
				this.setGenerateAndSentInProgressAndCheckAgainLater();
			}
		} else if (this._generateReportAndDownloadStarted) {
			if (code == "no_job") {
				this._generateReportAndDownloadStarted = false;
				this.setReadyToGenerateAndDownload();
			} else if (code == "job_done") {
				this._generateReportAndDownloadStarted = false;
				var reportDownloadPath = $(response).find("reference").text();
				if (reportDownloadPath) {
					this.downloadFile(reportDownloadPath);
				}
				this.reloadParentPage();
			} else if (code == "job_error") {
				this._generateReportAndDownloadStarted = false;
				this.setReadyToGenerateAndDownload();
				alert("Error Generating Report");
			} else if (code == "job_running") {
				this.setGeneratingReportAndDownloadInProgressAndCheckAgainLater();
			}
		} else {
			if (code == "no_job" || code == "job_done" || code == "job_error") {
				if (this._btnGenerateReport != null) {
					this.setReadyToGenerateReport();
				}
				if (this._btnGenerateAndSendReport != null) {
					this.setReadyToGenerateAndSent();
				}
				if (this._btnGenerateAndDownloadReport != null) {
					this.setReadyToGenerateAndDownload();
				}

				if (this._archiveReportToFileManagerBtn != null && this._afterArchived) {
					this.reloadParentPage();
					this._afterArchived = false;
				}
			} else if (code == "job_running") {
				this._generateReportStarted = true;
				this.setGeneratingReportInProgressAndCheckAgainLater();
			}
		}
		
	}
	
	DefaultReportListener.prototype.downloadFile = function(reportDownloadPath) {
		var fileUrl = "abaccess/download/" + reportDownloadPath;
		this._commandBeanProxy.openDownloadWindow(fileUrl);
	}
	
	D3.DefaultReportListener = DefaultReportListener;
	D3.CommandBeanProxy.nodeListener = DefaultReportListener;
})(window);