(function(window){
	
	D3.inherits(ReportSummarySubmitField, D3.AbstractFieldComponent);
	
	function ReportSummarySubmitField(id) {
		D3.AbstractFieldComponent.call(this);
	}
	
	ReportSummarySubmitField.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {

			this._fieldRenderer = D3.UI.createElement({
				tag:"button",
				text:D3.LanguageManager.getLabel("label.ReportSummarySubmitField.button"),
				css:"DefaultButton",
				attributes:{
					title:D3.LanguageManager.getLabel("title.ReportSummarySubmitField.button")
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var searchType = this._node.getFieldValue("@searchType");
						var dateFrom = this._node.getFieldValue("@dateFrom");
						var dateTo = this._node.getFieldValue("@dateTo");
						var qcStatus = this._node.getFieldValue("@qcStatus");
						if ((searchType!=null && searchType!="") && (dateFrom!=null && dateFrom!="") && (dateTo!=null && dateTo!="") && (qcStatus!=null && qcStatus!="")) {
							var selectedDateFrom = new Date(parseInt(dateFrom));
							var selectedDateTo = new Date(parseInt(dateTo));
							if(!isNaN(selectedDateFrom.getTime()) && !isNaN(selectedDateTo.getTime())) {
								var strDateFrom = $.datepicker.formatDate("dd MM yy", selectedDateFrom);
								var strDateTo = $.datepicker.formatDate("dd MM yy", selectedDateTo);
								if(selectedDateFrom>selectedDateTo) {
									alert(D3.LanguageManager.getLabel("alert.ReportSummarySubmitField.dateTo") + ": \"" +strDateTo+"\" " + D3.LanguageManager.getLabel("alert.ReportSummarySubmitField.cannotBefore") + " " + D3.LanguageManager.getLabel("alert.ReportSummarySubmitField.dateFrom") + ": \"" +strDateFrom + "\"");
								} else {
									var filter = searchType + "," + strDateFrom + ","+ strDateTo + "," + qcStatus;
									this._node.setFieldValue("@filter",filter);
									this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node,"@filter");
								}
							}
						} else {
							alert(D3.LanguageManager.getLabel("alert.ReportSummarySubmitField.requiredFields"));
						}
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReportSummarySubmitField.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(ReportSummaryReportLink, D3.HyperLinkField);
	
	function ReportSummaryReportLink(id) {
		D3.HyperLinkField.call(this);
	}
	
	ReportSummaryReportLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var ddrList = this._node.getFieldValue("@reportList");
			var strReportFileUid = this.getParam("reportfileuid", ddrList);
			var strReportName = this.getParam("reportname", ddrList);
			var strFileName = this.getParam("filename", ddrList);
			this._fieldRenderer = D3.UI.createElement({
				tag:"a",
				text:strReportName,
				css:"reportSummary link",
				data:{
					strReportFileUid:strReportFileUid,
					strFileName:strFileName
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var url = "abaccess/download/" + encodeURIComponent(D3.Common.replaceInvalidCharInFileName(data.strFileName)) + "?handlerId=ddr&reportId=" + encodeURIComponent(data.strReportFileUid);;
						this._node.commandBeanProxy.openDownloadWindow(url);
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReportSummaryReportLink.prototype.getParam = function (name, url) {
	    var match = RegExp(name + '=([^&]*)').exec(url);
	    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}
	
	ReportSummaryReportLink.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(ReportSummaryReportDailyLink, D3.HyperLinkField);
	
	function ReportSummaryReportDailyLink(id) {
		D3.HyperLinkField.call(this);
	}
	
	ReportSummaryReportDailyLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var daily = this._node.getFieldValue("@dailyUid");
			var dailyUid = this._node.getFieldValue("dailyUid");
			var reportType = this._node.getFieldValue("@reportType");
			
			this._fieldRenderer = D3.UI.createElement({
				tag:"a",
				text:daily,
				css:"reportSummary link",
				data:{
					daily:daily,
					dailyUid:dailyUid,
					reportType:reportType
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var reportType = data.reportType;
						var screen = "reportdaily.html";
						
						if(reportType == "DDR") screen = "reportdaily.html";
						else if(reportType == "DCCR") screen = "cementjob.html";
						if(reportType == "LOTR") screen = "leakofftest.html";
						if(reportType == "PNAR") screen = "plugandabandon.html";
						
						var url = screen + "?gotoday=" + data.dailyUid;
						var msg = D3.LanguageManager.getLabel("message.link") + " " + data.daily;
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReportSummaryReportDailyLink.prototype.refreshFieldRenderer = function () {}
	
	D3.inherits(ReportSummaryReportScreenLink, D3.HyperLinkField);
	
	function ReportSummaryReportScreenLink(id) {
		D3.HyperLinkField.call(this);
	}
	
	ReportSummaryReportScreenLink.prototype.getFieldRenderer = function() {
		if (!this._fieldRenderer) {
			var qcFlag = this._node.getFieldValue("@qcFlag");
			var operationUid = this._node.getFieldValue("operationUid");
			var reportType = this._node.getFieldValue("@reportType");
			var dailyUid = this._node.getFieldValue("dailyUid");
			this._fieldRenderer = D3.UI.createElement({
				tag:"a",
				text:qcFlag,
				css:"reportSummary link",
				data:{
					operationUid:operationUid,
					dailyUid:dailyUid,
					reportType:reportType
				},
				callbacks:{
					onclick:function(event) {
						var link = event.target;
						var data = $(link).data();
						var reportType = data.reportType;
						var screen = "reportdaily.html?tab=report";
						
						if(reportType == "DDR") screen = "reportdaily.html?tab=report";
						else if(reportType == "DCCR") screen = "casingcementreport.html?gotoday=" + data.dailyUid;
						if(reportType == "LOTR") screen = "leakofftestreport.html?gotoday=" + data.dailyUid;
						if(reportType == "PNAR") screen = "plugandabandonreport.html?gotoday=" + data.dailyUid;
						
						var url = screen + "&gotowellop=" + data.operationUid;
						var msg = D3.LanguageManager.getLabel("message.link") + "...";
						this._node.commandBeanProxy.gotoUrl(url,msg);
					}
				},
				context:this
			});
		}
		return this._fieldRenderer;
	}
	
	ReportSummaryReportScreenLink.prototype.refreshFieldRenderer = function () {}

	D3.inherits(ReportSummaryNodeListener,D3.EmptyNodeListener);
	
	function ReportSummaryNodeListener() {}
	
	ReportSummaryNodeListener.prototype.getCustomFieldComponent = function(id, node){			
		if(id == "reportSummary"){
			return new ReportSummarySubmitField();
		}
		else if(id == "reportSummaryReportLink"){
			return new ReportSummaryReportLink();
		}
		else if(id == "reportSummaryReportDailyLink"){
			return new ReportSummaryReportDailyLink();
		}
		else if(id == "reportSummaryReportScreenLink"){
			return new ReportSummaryReportScreenLink();
		}		
		else {
			return null;
		}
	}
	D3.CommandBeanProxy.nodeListener = ReportSummaryNodeListener;
	
})(window);