(function(window) {
	
	D3.inherits(ProductionStringDetailNodeListener,D3.EmptyNodeListener);
	
	function ProductionStringDetailNodeListener()
	{
		
	}
	
	ProductionStringDetailNodeListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "show_component") {
			var buttonLabel = button.textContent;
			var productionStringUid = node.getFieldValue("productionStringUid");
			
			var param = {};
			param.gotoFieldName = "productionStringUid";
			param.gotoFieldUid = escape(productionStringUid);
			
			var viewComponentMode = null;
			if (buttonLabel == "Show All Components") {
				viewComponentMode = "show_all";
			} else if (buttonLabel == "Show Current Installed Components") {
				viewComponentMode = "show_current";
			}
			
			var url =  "productionstring.html?viewComponentMode="+viewComponentMode+ "&gotoFieldName=productionStringUid&gotoFieldUid="+escape(productionStringUid);
			node.commandBeanProxy.gotoUrl(url,buttonLabel);
		}
	}
	
	ProductionStringDetailNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent) {
		var maxServiceDatetime = null;
		var depthMdMsl = null;
		var bottomDepthMdMsl = null;
		var installDateTime = null;
		var uninstallDateTime = null;
		var sizeOd = null;
		var sizeId = null;
		var numOfJoint = null;
		var length =null;
		var description =null;
		var status = null;
		var i=0;
		
		if (node.meta._simpleClassName == "ProductionStringDetailLog") {
			var childNode = parent.children[node.meta._simpleClassName].getItems();
			D3.forEach(childNode,function(child){
				for (var i=0;i< childNode.length;i++) {
					var serviceDatetime = child.getFieldValue("serviceDateTime");
					if (!maxServiceDatetime || ((new Date(serviceDatetime)) > (new Date(maxServiceDatetime)))) {
						maxServiceDatetime = serviceDatetime;
						installDateTime = child.getFieldValue("installDateTime");
						uninstallDateTime = child.getFieldValue("uninstallDateTime");
						depthMdMsl = child.getFieldValue("depthMdMsl");
						bottomDepthMdMsl = child.getFieldValue("bottomDepthMdMsl");
						sizeOd = child.getFieldValue("sizeOd");
						sizeId = child.getFieldValue("sizeId");
						numOfJoint = child.getFieldValue("numJoints");
						length = child.getFieldValue("length");
						description = child.getFieldValue("description");
						status = child.getFieldValue("status");
					}
					
					if (maxServiceDatetime) {
						node.setFieldValue("serviceDateTime", maxServiceDatetime);
						node.setFieldValue("installDateTime", installDateTime);
						node.setFieldValue("uninstallDateTime", uninstallDateTime);
						
						if (node.getGWP("autoCalculateDepthToatComponentServiceLog")=="1"){
							node.setFieldValue("depthMdMsl", bottomDepthMdMsl);	
						}else{
							node.setFieldValue("depthMdMsl", depthMdMsl);
						}					
						node.setFieldValue("sizeOd", sizeOd);
						node.setFieldValue("sizeId", sizeId);
						node.setFieldValue("numJoints", numOfJoint);
						node.setFieldValue("length", length);
						node.setFieldValue("description", description);
						node.setFieldValue("status", status);
						
					}
				}
			},this);
		}
	}
	
	D3.CommandBeanProxy.nodeListener = ProductionStringDetailNodeListener;
	
})(window);