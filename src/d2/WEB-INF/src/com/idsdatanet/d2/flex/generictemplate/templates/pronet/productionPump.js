(function(window){
	D3.inherits(ProductionPumpListener,D3.EmptyNodeListener);
	
	function ProductionPumpListener() {
	}
	
	ProductionPumpListener.prototype.customButtonTriggered = function(node, id, button) {
		if (id == "add_production_pump") {
			node.addNewChildNodeOfClass("ProductionPumpParam",true,true);
		}
	}
	
	ProductionPumpListener.prototype.getCustomHeaderButton = function(id, parentNode, buttonDeclaration) {
		
		var btn = null;
		if (id == "add_production_pump" && parentNode.commandBeanProxy.rootNode.isAllowedAction(D3.CommandBeanDataNode.STANDARD_ACTION_ADD, "ProductionPumpParam")) {
			btn = $("<button class='DefaultButton' style='margin:3px'>"+buttonDeclaration.label+"</button>")
			
			return btn.get(0);
		}
		 
		return null;
	}
	
	D3.CommandBeanProxy.nodeListener = ProductionPumpListener;
})(window);