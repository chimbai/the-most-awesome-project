(function(window){
	D3.inherits(RecoverOperationButton,D3.AbstractFieldComponent);
	
	function RecoverOperationButton(){
		D3.AbstractFieldComponent.call(this);
	}
	
	RecoverOperationButton.prototype.getFieldRenderer = function(){

		this._fieldRenderer = D3.UI.createElement({
			tag:"button",
			text:"Recover this WWO",
			css:"RootButton",
			callbacks:{
				onclick:this.recoverOperation
			},
			context:this
		}) ;
		this._fieldRenderer.setAttribute('style', 'width:150px');
		return this._fieldRenderer;
		
	}
	
	RecoverOperationButton.prototype.recoverOperation = function(){
		var wantDup  = confirm("Are you sure you want recover this WWO?");
		if (wantDup){
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_action","_recover_operation");
			this._node.commandBeanProxy.addAdditionalFormRequestParams("submitForServerSideProcess_operationUid", this._node.getFieldValue("operationUid"));	
			this._node.commandBeanProxy.submitForServerSideProcess();
			this._node.commandBeanProxy.reloadParentHtmlPage(false);
		}
	}
	
	RecoverOperationButton.prototype.refreshFieldRenderer = function(){
	}
	
	D3.inherits(CostAfeListNodeListener,D3.EmptyNodeListener);
	
	function CostAfeListNodeListener() {
	}
	
	CostAfeListNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "recover_operation_button"){
			return new RecoverOperationButton();
		}
	}
	
	D3.CommandBeanProxy.nodeListener = CostAfeListNodeListener;
	
})(window);