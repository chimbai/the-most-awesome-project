(function(window){
	
	D3.inherits(OperationLinkButton,D3.HyperLinkField);
	
	function OperationLinkButton(){
		D3.HyperLinkField.call(this);
	}
	
	OperationLinkButton.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		
		if (!this._fieldRenderer){
		  var linkLabel = this._node.getFieldValue("@completeOperationName");
		  fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		  fieldRenderer.appendChild(document.createTextNode(linkLabel));
		  fieldRenderer.removeAttribute("target");
		  fieldRenderer.removeAttribute("href");
		  $(fieldRenderer).addClass("whitespace");
		  fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}else{
	      fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		}
		
		return fieldRenderer; 
	}
	
	OperationLinkButton.prototype.onclick = function(){
		var url = this._node.getFieldValue("@operationPage") + "?gotowellop=" + this._node.getFieldValue("@operationUid");
		this._node.commandBeanProxy.gotoUrl(url,"Loading " + this.getFieldValue());
	}
	
	OperationLinkButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(DailyLinkButton,D3.HyperLinkField);
	
	function DailyLinkButton(){
		D3.HyperLinkField.call(this);
	}
	
	DailyLinkButton.prototype.getFieldRenderer = function(){
		var fieldRenderer = null;
		
		if (!this._fieldRenderer){
		  var linkLabel = this._node.getFieldValue("@dayName");
		  fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		  fieldRenderer.appendChild(document.createTextNode(linkLabel));
		  fieldRenderer.removeAttribute("target");
		  fieldRenderer.removeAttribute("href");
		  $(fieldRenderer).addClass("whitespace");
		  fieldRenderer.onclick = this.onclick.bindAsEventListener(this);
		}else{
	      fieldRenderer = OperationLinkButton.uber.getFieldRenderer.call(this);
		}
		return fieldRenderer; 
	}
	
	DailyLinkButton.prototype.onclick = function(){
		var url = "filebridge.html?gotoday=" + this._node.getFieldValue("@dailyUid");
		this._node.commandBeanProxy.gotoUrl(url,"Loading " + this.getFieldValue());
	}
	
	DailyLinkButton.prototype.refreshFieldRenderer = function (){}
	
	D3.inherits(StatusLinkButton, D3.AbstractFieldComponent);
	
	function StatusLinkButton(id) {
		D3.AbstractFieldComponent.call(this);
		this.tic = "";
	}
	
	StatusLinkButton.prototype.getFieldRenderer = function() {
		var label = "";
		if (!this._fieldRenderer) {
			var fbfStatusRemark = this._node.getFieldValue("@fbfStatusRemark");
			label = this._node.getFieldValue("@status");
			this._fieldRenderer = D3.UI.createElement({
				tag:"div",
				context:this
			});
			this._linkStatusButton = this.createLinkStatusButton(label, fbfStatusRemark);
			this._fieldRenderer.appendChild(this._linkStatusButton);
		}
		
		var self = this;
		this.tic = (label=="Success" || label=="Fail") ? "" : setInterval(function(){self.checkStatusRemark();}, 10000);
		return this._fieldRenderer
	}
	
	StatusLinkButton.prototype.createLinkStatusButton = function(status, remark){
		return D3.UI.createElement({
			tag:"button",
			text:status,
			css:"DefaultButton",
			attributes:{
				title:remark,
				style:"background: rgba(0,0,0,0); border:none; color:" + this.getStatusColor(status) + ";font-weight:bold;width:90px;",
				width: "30px"
			},
			callbacks:{
				onclick:function(event) {
					//fetch remarks
					var _textStatus = D3.UI.createElement({
						tag:"div",
						css:"fieldRenderer",
						attributes:{
							// w x h from flash equivalent
							width:"auto",
							height:"auto",
							style: "overflow-y:scroll"
						},
						text:this._node.getFieldValue("@fbfStatusRemark"),
						context:this
					});
					var _popUpDiv = "<div></div>";
					this._popUpDialog = $(_popUpDiv);
					this._popUpDialog.append(_textStatus);
					this._popUpDialog.dialog({
						height:"auto",
						width:"auto", 
						resizable:false,
						modal:true,
						title:"Remarks"
					});
				}
			},
			context:this
		});
	}
	
	StatusLinkButton.prototype.getStatusColor = function(status){
		var color = "";
		if (status=="Success") { color="green"; }
		else if (status=="Fail") { color="red"; }
		else if (status=="Processing") { color="orange"; }
		else if (status=="Pending") { color="black"; }
		return color;
	}
	
	StatusLinkButton.prototype.checkStatusRemark = function(){
		if(this._node != null){
			var params = {};
			params._invokeCustomFilter = "statusRemarkCheck";
			params.fileBridgeFilesUid = this._node.getFieldValue("@fileBridgeFilesUid");
			if(params.fileBridgeFilesUid != "") {
				this._node.commandBeanProxy.customInvoke(params, this.completed, this);	
			}
		}
	}
	
	StatusLinkButton.prototype.completed = function(response) {
		var parserKey = $(response).find('parserKey')[0].textContent;
		var status = $(response).find('status')[0].textContent;
		var remark = $(response).find('remark')[0].textContent;
		this._node.setFieldValue("@parserKey", parserKey);
		this._node.setFieldValue("@status", status);
		this._node.setFieldValue("@fbfStatusRemark", remark);
		this._node.refresh
		this._linkStatusButton.textContent = status;
		this._linkStatusButton.setAttribute("style", "background: rgba(0,0,0,0); border:none; color:" + this.getStatusColor(status) + ";font-weight:bold;width:90px;");
		this._linkStatusButton.setAttribute("title", remark);
		if (status=="Success" || status=="Fail") {
			clearInterval(this.tic);
		}
		//this.refreshFieldRenderer();
		this._node.fireRefreshNodeEvent();
	}
	
	StatusLinkButton.prototype.refreshFieldRenderer = function () {}

	D3.inherits(FileBridgeFilesManager,D3.EmptyNodeListener);
	
	function FileBridgeFilesManager()
	{
		this.btnSnapshot = null;
		this.snapshotState = null;
		
	}

	FileBridgeFilesManager.prototype.getCustomFieldComponent = function(id, node){
		if (id == "operationLink") {
			return new OperationLinkButton();
		} if (id == "dailyLink") {
			return new DailyLinkButton();
		} if (id == "statusLink") {
			return new StatusLinkButton();
		}else{
			return null;
		}
	}

	FileBridgeFilesManager.prototype.getCustomRootButton = function(id, commandBeanProxy, buttonDeclaration){
		
	}

	D3.CommandBeanProxy.nodeListener = FileBridgeFilesManager;
	
})(window);