(function(window){

	D3.inherits(LessonLearnedTitleCustomField, D3.ComboBoxField);
	function LessonLearnedTitleCustomField() {
		D3.ComboBoxField.call(this);
	}
	LessonLearnedTitleCustomField.prototype.getAutoCompleteConfig = function() {
		var config = LessonLearnedTitleCustomField.uber.getAutoCompleteConfig();
		config.width="200px";
		return config;
	};
	LessonLearnedTitleCustomField.prototype.data = function(data) {
		LessonLearnedTitleCustomField.uber.data.call(this, data);
		this._node.addEventListener(D3.CommandBeanDataNodeEvent.NODE_FIELD_VALUE_CHANGE, this.fieldValueChange,this);
	};

	LessonLearnedTitleCustomField.prototype.fieldValueChange = function (event){
		if ((event.fieldName=="@country" || event.fieldName=="@lessonStatus")){
			this.load();
		}
	}
	LessonLearnedTitleCustomField.prototype.getLookupRef = function() {
		return this._lookupRef;
	}
	LessonLearnedTitleCustomField.prototype.refreshFieldEditor = function() {
		this.load();
	}
	LessonLearnedTitleCustomField.prototype.load = function (){
		var params = {};
		params._invokeCustomFilter = "lessonLearnedTitleList";
		var country = this.setValue("@country");
		if (country) params._country = country;
		var lessonStatus = this.setValue("@lessonStatus");
		if(lessonStatus) params._lessonStatus = lessonStatus;

		this._node.commandBeanProxy.customInvoke(params,this.loaded,this);
	}
	
	LessonLearnedTitleCustomField.prototype.setValue = function(fieldName){
		var data = this._node.getMultiSelect(fieldName);
		var stringValue = null;
		if (data!=null && data.length>0){
			for (var i=0;i<data.length;i++){
				if(stringValue==null){
					if(data[i] && data[i]!="null")
						stringValue = "'"+data[i]+"'";
					else
						stringValue = "";
				}
				else{
					if(data[i] && data[i]!="null")
						stringValue +=",'"+data[i]+"'";
					else
						stringValue = "";
				}
			}
		}else{
			stringValue = "";
		}
		
		return stringValue;
	};
	LessonLearnedTitleCustomField.prototype.loaded = function(response){
		var lookupReference = new D3.LookupReference();
		var doc = $(response);
		var selected_item = null;
		var rawValue = this.getFieldValue();
		var r = $(response).find('LessonLearnedTitle');
		D3.UI.removeAllChildNodes(this._combobox);	
		var combo = this._combobox;
		if(r.length > 0) {
			var newItem = new D3.LookupItem();
			newItem.data = "";
			newItem.label = "";
			lookupReference.addLookupItem(newItem);
			var self = this;
			var option = document.createElement("option");
			option.value = "";
			option.title = "";
			this._combobox.appendChild(option);
			
			r.each(function(index, item){
				var newItem = new D3.LookupItem();
				var item = $(item);
				newItem.data = item.find("code").text();
				newItem.label = item.find("label").text();
				lookupReference.addLookupItem(newItem);
				if (newItem.data){
					var option = document.createElement("option");
					option.value = newItem.data;
		//			option.selected = (newItem.data == rawValue);
					option.title = newItem.data;
					option.appendChild(document.createTextNode(newItem.data)); 
					self._combobox.appendChild(option);
					if (newItem.data == rawValue) $(self._combobox).select2("val",rawValue);
				}
				if (newItem.data == rawValue)
					selected_item = newItem;
			});
		}
		this._lookupRef = lookupReference;
	}
	
	D3.inherits(LessonTitleFilter, D3.AbstractFieldComponent);
	function LessonTitleFilter() {
		D3.AbstractFieldComponent.call(this);
	}
	LessonTitleFilter.prototype.getFieldRenderer = function() {
		if(this._linkButton == null) {
			this._linkButton = document.createElement("button");
			var label = "Search";
			this._linkButton.appendChild(document.createTextNode(label));
			this._linkButton.onclick = this.refreshButtonClicked.bindAsEventListener(this);
		}
		return this._linkButton;
	}
	
	LessonTitleFilter.prototype.refreshButtonClicked = function() {
		var relate = this._config.relatedFields;
		this._node.commandBeanProxy.submitCurrentNodeAndReload(this._node, relate);
	}
	D3.inherits(LessonTicketAdminListener,D3.EmptyNodeListener);
	
	function LessonTicketAdminListener()
	{
		
	}
	
	LessonTicketAdminListener.prototype.getCustomFieldComponent = function(id, node){
		if(id == "lessonTitleList"){
			return new LessonLearnedTitleCustomField();
		}else if(id == "lessonTitleSearch"){
			return new LessonTitleFilter();
		}else{
			return null;
		}
	}
	D3.CommandBeanProxy.nodeListener = LessonTicketAdminListener;
})(window);