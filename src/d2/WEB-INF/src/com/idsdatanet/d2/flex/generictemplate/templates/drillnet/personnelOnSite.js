(function(window){
	D3.inherits(PersonnelOnSiteNodeListener,D3.ExcelImportTemplateNodeListener);
	
	function PersonnelOnSiteNodeListener() {}
	
	PersonnelOnSiteNodeListener.prototype.getCustomRootButton = function(id, commandBeanProxy, btnDefinition) {
		this.commandBeanProxy = commandBeanProxy;
	    var importBtn = this.constructor.uber.getCustomRootButton(id,commandBeanProxy,btnDefinition);
	    if (importBtn != null) {
	    	return importBtn;
	    }else if (id == "wellIT-import-personnelonsite") {
			var importBtn = document.createElement("button");
			importBtn.className = "rootButtons";
		    importBtn.appendChild(document.createTextNode("WELS Import"));
		    importBtn.onclick = this.onclickWELSImport.bindAsEventListener(this);
		    return importBtn;
		}
        return null;
	};
	
	PersonnelOnSiteNodeListener.prototype.onclickWELSImport = function() {
		this.commandBeanProxy.addAdditionalFormRequestParams("action", "importWellIT");
		this.commandBeanProxy.submitForServerSideProcess();
    };
	
	D3.CommandBeanProxy.nodeListener = PersonnelOnSiteNodeListener;
})(window);