(function(window) {
	
	D3.inherits(WirelineToolLinkButton, D3.AbstractFieldComponent);
	function WirelineToolLinkButton() {
		D3.AbstractFieldComponent.call(this);
	}
	
	WirelineToolLinkButton.prototype.dispose = function() {
		WirelineToolLinkButton.uber.dispose.call(this);
		this._linkButton = null;
	};
	
	WirelineToolLinkButton.prototype.getFieldRenderer = function() {
		if(!this._linkButton){
			this._linkButton = D3.UI.createElement({
				tag:"a",
				text:D3.LanguageManager.getLabel("link.moreDetailLink"),
				css:"moreDetailLinkCss",
				callbacks:{
					onclick:this.pageRedirect
				},
				context:this
			});
		}
		return this._linkButton;
	};
	
	WirelineToolLinkButton.prototype.pageRedirect = function(){
		var redirect_url = "equipmentfailurebyoperation.html?toolType=wireline_tool&toolId=" + escape(this._node.getFieldValue("wirelineToolUid")) ;
		var redirect_msg =D3.LanguageManager.getLabel("label.equipmentfailure.redirect");
		this._node.commandBeanProxy.gotoUrl(redirect_url,redirect_msg);
	}
	
	WirelineToolLinkButton.prototype.getFieldEditor = function() {
		if (this._fieldEditor == null) {
			var divConfig = "<div>" +
								"<table>" +
									"<tr>" +
										"<td>" +
											"<font color='red'>" +
												D3.LanguageManager.getLabel("editor.msg") +
											"</font>" +
										"</td>" +
									"</tr>" +
								"</table>" +
							"</div>";
			
			this._div = $(divConfig);
			this._fieldEditor = this._div.get(0);
		}

		return this._fieldEditor;
	}
	
	
	D3.inherits(WirelineSuiteLinkButton, D3.AbstractFieldComponent);
	function WirelineSuiteLinkButton() {
		D3.AbstractFieldComponent.call(this);

	}
	
	WirelineSuiteLinkButton.prototype.dispose = function() {
		WirelineSuiteLinkButton.uber.dispose.call(this);
		this._renderer = null;
	};
	
	WirelineSuiteLinkButton.prototype.getFieldRenderer = function() {
		if(!this._renderer){
			this._renderer = D3.UI.createElement({
				tag:"a",
				text:this.getFieldValue(),
				css:"wirelineSuiteLinkCss",
				callbacks:{
					onclick:this.gotoWirelineSuitePage
				},
				context:this
			});
		}
		
		return this._renderer;	
	}
	
	WirelineSuiteLinkButton.prototype.gotoWirelineSuitePage = function() {
		var url = this._node.getFieldValue("@wirelineSuitePage") + "?gotoFieldName=wirelineSuiteUid&gotoFieldUid="  + this._node.getFieldValue("wirelineSuiteUid");
		var msg = D3.LanguageManager.getLabel("label.wirelineSuiteLink.redirect");
		this._node.commandBeanProxy.gotoUrl(url,msg);
	}
	
	
	D3.inherits(WirelineSuiteNodeListener,D3.EmptyNodeListener);
	
	function WirelineSuiteNodeListener()
	{
		
	}
	
	WirelineSuiteNodeListener.prototype.nodeCreatedForClientSideAddNew = function(node, parent){
		var endTime = null;
		var lastDailyUid = null;
		if(node.meta._simpleClassName == "WirelineLog"){
			var childNode = parent.children["WirelineLog"].getItems();
			D3.forEach(childNode,function(child){
				for (var i=0;i< childNode.length;i++) {
					endTime = childNode[i].getFieldValue("endTime");
					lastDailyUid=childNode[i].getFieldValue("dailyUid");
				}
			},this);
		}
		if (endTime) {
			node.setFieldValue("startTime", endTime);
			node.setFieldValue("endTime", endTime);
		}
		if (lastDailyUid){
			node.setFieldValue("dailyUid",lastDailyUid);
		}
	}
	
	WirelineSuiteNodeListener.prototype.getCustomFieldComponent = function(id, node){
		if (id == "equipmentFailureLink") {
			return new WirelineToolLinkButton();
			return null;
		}else if (id == "wirelineSuiteLink") {
			return new WirelineSuiteLinkButton();
		}else{
			return null;
		}
	},
	D3.CommandBeanProxy.nodeListener = WirelineSuiteNodeListener;
	
})(window);