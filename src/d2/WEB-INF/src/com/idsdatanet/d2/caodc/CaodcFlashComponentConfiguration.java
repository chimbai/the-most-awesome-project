package com.idsdatanet.d2.caodc;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CaodcFlashComponentConfiguration extends AbstractFlashComponentConfiguration{

	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = "../../../webservice/caodcservice.html";
		String baseUrl = "../../../";
		String allowOneDayImportOnly = GroupWidePreference.getValue(userSession.getCurrentGroupUid(),"allowOneDayImportOnly");
		allowOneDayImportOnly = ("1".equals(allowOneDayImportOnly)?"true":"false");
		
		return "d2Url=" + urlEncode(serviceUrl) 
				+ "&baseUrl="+urlEncode(baseUrl)
				+ "&allowOneDayImportOnly="+urlEncode(allowOneDayImportOnly);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return targetSubModule;
	}

}
