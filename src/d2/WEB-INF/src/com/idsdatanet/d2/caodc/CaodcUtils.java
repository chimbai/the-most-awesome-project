package com.idsdatanet.d2.caodc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import javax.xml.datatype.XMLGregorianCalendar;

import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.UOMConverter;
import com.idsdatanet.d2.core.uom.jaxb.unittypes.Unit;
import com.idsdatanet.d2.core.uom.jaxb.unittypes.Unittype;

public class CaodcUtils {
	public static Date XMLGregorianCalendarToDate(XMLGregorianCalendar calendar, String wellUid) {
		if (calendar==null) return null;
		calendar.setTimezone(0);
		return calendar.toGregorianCalendar().getTime();
	}
	
	public static Double offsetDatumDepth(Double value, Double offsetValue) {
		return value - offsetValue;
	}
	
	public static Double convertToBaseValue(Class clazz, String fieldName, Double value, String uom) {
		if (value==null) return null;
		try {
			CustomFieldUom converter = new CustomFieldUom((Locale) null, clazz, fieldName);
			String unitTypeId = converter.getUOMMapping().getUnitTypeID();
			value = UOMConverter.convertToBase(value, CaodcUtils.getUomType(unitTypeId, uom));
		} catch (Exception e) {
			System.out.println("UOM NOT FOUND: " + clazz.getSimpleName() + "." + fieldName + " - " + e.getMessage());
		}
		
		return value;
	}
	
	public static Double convertToBaseValue(Class clazz, String fieldName, BigDecimal value, String uom) {
		if (value==null) return null;
		return CaodcUtils.convertToBaseValue(clazz, fieldName, Double.parseDouble(value.toString()), uom);
	}
	
	public static String getUomType(String unitTypeId, String uomSymbol) {
		Unittype ut = (Unittype) UOMConverter.getAllUnitTypes().get(unitTypeId.toLowerCase());
		String uomName = "";
		for (Unit unit : ut.getUnit()) {
			if (unit.getSymbol().toLowerCase().equals(uomSymbol.toLowerCase()) || unit.getShorthand().toLowerCase().equals(uomSymbol.toLowerCase())) {
				uomName = unit.getName();
				break;
			}
		}
		
		String uomType = uomSymbol;
		if ("".equals(uomName)) {
			if ("TemperatureMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("C")) {
					uomType = "DegreesCelsius";
				} else if (uomSymbol.equals("F")) {
					uomType = "DegreesFahrenheit";
				}
			} else if ("MassMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("1000 daN")) {
					uomType = "KiloDecanewton";
					unitTypeId = "ForceMeasure";
				}
			} else if ("AngleMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("deg")) {
					uomType = "Degree";
				}
			} else if ("FunnelViscosityMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("s/l")) {
					uomType = "SecondsPerLitre";
				} else if (uomSymbol.equals("s/gal")) {
					uomType = "SecondsPerUSGallon";
				}
			} else if ("DensityMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("lb/cuft")) {
					uomType = "PoundsMassPerCubicFoot";
				}
			} else if ("MassPerLengthMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("lb/ft")) {
					uomType = "PoundsPerFoot";
				}
			} else if ("TimeMeasure".equals(unitTypeId)) {
				if (uomSymbol.equals("hours")) {
					uomType = "Hour";
				} else if (uomSymbol.equals("days")) {
					uomType = "Day";
				} else if (uomSymbol.equals("weeks")) {
					uomType = "Week";
				} else if (uomSymbol.equals("months")) {
					uomType = "Month";
				}
			}
		} else {
			uomType = uomName;
		}
		
		return unitTypeId + "." + uomType;
	}
	
}
