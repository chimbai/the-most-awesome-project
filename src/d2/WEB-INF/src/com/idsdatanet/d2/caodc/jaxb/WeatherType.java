//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.1-b02-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.04.07 at 05:21:29 PM SGT 
//


package com.idsdatanet.d2.caodc.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for weatherType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="weatherType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CLEAR"/>
 *     &lt;enumeration value="PARTLY CLOUDY"/>
 *     &lt;enumeration value="OVERCAST"/>
 *     &lt;enumeration value="BLOWING DUST"/>
 *     &lt;enumeration value="HAZE"/>
 *     &lt;enumeration value="SMOKE"/>
 *     &lt;enumeration value="DRIZZLE / MIST"/>
 *     &lt;enumeration value="SHOWER"/>
 *     &lt;enumeration value="RAIN"/>
 *     &lt;enumeration value="THUNDERSTORM"/>
 *     &lt;enumeration value="HAIL"/>
 *     &lt;enumeration value="FOG"/>
 *     &lt;enumeration value="FROST"/>
 *     &lt;enumeration value="SLEET"/>
 *     &lt;enumeration value="ICE PELLETS"/>
 *     &lt;enumeration value="BLOWING SNOW"/>
 *     &lt;enumeration value="LIGHT SNOW"/>
 *     &lt;enumeration value="SNOW"/>
 *     &lt;enumeration value="BLIZZARD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum WeatherType {

    CLEAR("CLEAR"),
    @XmlEnumValue("PARTLY CLOUDY")
    PARTLY_CLOUDY("PARTLY CLOUDY"),
    OVERCAST("OVERCAST"),
    @XmlEnumValue("BLOWING DUST")
    BLOWING_DUST("BLOWING DUST"),
    HAZE("HAZE"),
    SMOKE("SMOKE"),
    @XmlEnumValue("DRIZZLE / MIST")
    DRIZZLE_MIST("DRIZZLE / MIST"),
    SHOWER("SHOWER"),
    RAIN("RAIN"),
    THUNDERSTORM("THUNDERSTORM"),
    HAIL("HAIL"),
    FOG("FOG"),
    FROST("FROST"),
    SLEET("SLEET"),
    @XmlEnumValue("ICE PELLETS")
    ICE_PELLETS("ICE PELLETS"),
    @XmlEnumValue("BLOWING SNOW")
    BLOWING_SNOW("BLOWING SNOW"),
    @XmlEnumValue("LIGHT SNOW")
    LIGHT_SNOW("LIGHT SNOW"),
    SNOW("SNOW"),
    BLIZZARD("BLIZZARD");
    private final String value;

    WeatherType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WeatherType fromValue(String v) {
        for (WeatherType c: WeatherType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
