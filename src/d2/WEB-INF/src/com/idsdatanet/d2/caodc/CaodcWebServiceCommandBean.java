package com.idsdatanet.d2.caodc;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;

public class CaodcWebServiceCommandBean extends AbstractGenericWebServiceCommandBean {
	
	protected static final String PARAM_INVOCATION_METHOD = "method";
	protected static final String METHOD_GET_FILE_CONTENT = "update_single_file";
	protected static final String MAIN_ACTION_GET_SESSION_ID = "get_session_id";
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String method = request.getParameter(PARAM_INVOCATION_METHOD);
		if (METHOD_GET_FILE_CONTENT.equals(method)) {
			this.getFileContent(request, response);
        }else if(MAIN_ACTION_GET_SESSION_ID.equals(method)){
        	this.getCurrentSessionId(request, response);
		}
	}
	
	private void getCurrentSessionId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String session_id = null;
		HttpSession session = request.getSession(false);
		if(session != null) session_id = session.getId();
		
		SimpleXmlResponse xmlResponse = new SimpleXmlResponse(response, true);
		xmlResponse.addElement("sessionid", session_id);
		xmlResponse.close();		
	}

	private void getFileContent(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FileItem file_item = null;
        if(request instanceof MultipartHttpServletRequest){
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			if(! multipartRequest.getFileMap().isEmpty()){
				file_item = ((CommonsMultipartFile) multipartRequest.getFileMap().values().iterator().next()).getFileItem();
			}
		}else{
			FileItemFactory factory = new DiskFileItemFactory();
	        ServletFileUpload upload = new ServletFileUpload(factory);
	        List items = upload.parseRequest(request);        
	        
	        for(Iterator i = items.iterator(); i.hasNext(); ){
	            FileItem item = (FileItem) i.next();
	            if(! item.isFormField()){
	                file_item = item;
	                break;
	            }
	        }		
		}
		
		if (file_item!=null) {
			DataInputStream in = new DataInputStream(file_item.getInputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			String content = "";
			while ((strLine = br.readLine()) != null)   {
				content += strLine+"\n";
			}
			
			try {
				SimpleXMLElement data = SimpleXMLElement.loadXMLString(content); //testing xml only
				response.setContentType("text/xml");
				response.getWriter().write(content);
				in.close();
			} catch (Exception e) {
				SimpleXmlResponse.send(response, false, "Invalid File", e.getMessage());
			}
			file_item.delete();
		} else {
			SimpleXmlResponse.send(response, false, "Invalid File", "No file uploaded");
		}
			
	}
}
