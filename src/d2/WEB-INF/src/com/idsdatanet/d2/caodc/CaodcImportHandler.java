package com.idsdatanet.d2.caodc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.idsdatanet.d2.caodc.jaxb.DirectionType;
import com.idsdatanet.d2.caodc.jaxb.ETS;
import com.idsdatanet.d2.caodc.jaxb.LocationType;
import com.idsdatanet.d2.caodc.jaxb.WindStrengthType;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.Rig;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Weather;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment.Bits;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment.MudPumps;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment.Bits.Bit;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment.Bits.Bit.BitCondition;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Equipment.MudPumps.MudPump;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.CirculationPressure;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DeviationSurveys;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DrillRecords;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DrillingAssemblys;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.ReducedPumpSpeeds;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.SolidsControls;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.TimeLogs;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.TourPayroll;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.CirculationPressure.CirculationPumps.CirculationPump;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DeviationSurveys.DeviationSurvey;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DrillRecords.DrillRecord;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.DrillingAssemblys.DrillingAssembly;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord.MudMaterials;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord.MudSamples;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord.MudMaterials.MudMaterial;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord.MudSamples.MudSample;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.MudRecord.MudSamples.MudSample.OtherTests.OtherTest;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.ReducedPumpSpeeds.ReducedPumpSpeed;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.SolidsControls.SolidsControl;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.TimeLogs.TimeLog;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tours.Tour.TourPayroll.Employees.Employee;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.Casings;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.Coils;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.DrillPipes;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.Casings.Casing;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.Coils.Coil;
import com.idsdatanet.d2.caodc.jaxb.ETS.WellTours.WellTour.DayTours.DayTour.Tubular.DrillPipes.DrillPipe;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.model.RigSlowPumpParam;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.SolidControlEquipment;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.model.WeatherEnvironment;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CaodcImportHandler extends BlazeRemoteClassSupport {
	
	private String currentOperationUid = null;
	private String currentWellboreUid = null;
	private String currentWellUid = null;
	private String currentRigInformationUid = null;
	private String currentOperationCode = null;
	private Double KBHeight = 0.0;
	private Map<String, String> _selectedImportItems = null; 
	private QueryProperties queryProperties = new QueryProperties();
	private List<String> errorMessage = null;
	Map<BigInteger, String> bharunTour = null;
	
	private void disableQueryProperties() {
		queryProperties.setUomConversionEnabled(false);
		queryProperties.setDatumConversionEnabled(false);
	}
	
	public String getWellList() {
		String xml = "<root/>";
		try {
			String groupUid = this.getCurrentUserSession().getCurrentGroupUid();
			
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			String strSql = "FROM Operation where (isDeleted=false or isDeleted is null) and (isCampaignOverviewOperation=false or isCampaignOverviewOperation is null) and operationUid=:operationUid order by operationName";
			List<Operation> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", this.getCurrentUserSession().getCurrentOperationUid());
			Operations accessibleOperations = this.getCurrentUserSession().getCachedAllAccessibleOperations();
			for (Operation operation : list) {
				if (accessibleOperations.containsKey(operation.getOperationUid())) {
					SimpleAttributes atts = new SimpleAttributes();
					String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operation.getOperationUid());
					atts.addAttribute("operationUid", operation.getOperationUid());
					atts.addAttribute("operationName", operationName);
					if (operation.getOperationUid().equals(this.getCurrentUserSession().getCurrentOperationUid())) {
						atts.addAttribute("selected", "1");
					}
					writer.startElement("Operation", "", atts);
					strSql = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid";
					List<ReportDaily> rdl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operation.getOperationUid());
					for (ReportDaily rd : rdl) {
						atts = new SimpleAttributes();
						atts.addAttribute("qcFlag", nullToEmptyString(rd.getQcFlag()));
						atts.addAttribute("reportDatetime", nullToEmptyString(rd.getReportDatetime()));
						writer.addElement("ReportDaily", null, atts);
					}
					writer.endElement();
				}
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	
	public String process(String operationUid, String xmlString, String[] selectedImportItems) {
		try {
			this.disableQueryProperties();
			errorMessage = new ArrayList<String>();

			this._selectedImportItems = new HashMap<String, String>();
			if (selectedImportItems==null) return "No item to be imported.";
			for (String selectedItem : selectedImportItems) {
				this._selectedImportItems.put(selectedItem, selectedItem);
			}
			
			JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.caodc.jaxb");
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			ETS ets = (ETS) unmarshaller.unmarshal(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
			if (ets.getWellTours()!=null) {
				if (!this.importWellTours(ets.getWellTours(), operationUid)) {
					String strErrors = "";
					for (Object error : errorMessage) {
						strErrors += "\n- " + error.toString();
					}
					if (errorMessage.size()==1) strErrors = errorMessage.get(0).toString();
					return strErrors;
				}
			}
			MenuManager.getConfiguredInstance().refreshCachedData();
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			//D2ApplicationEvent.refresh();
		} catch (Exception e) {
			e.printStackTrace();
			return "Unable to parse imported file or invalid format.";
		}
		return "true";
	}
	
	private Boolean importWellTours(WellTours wellTours, String operationUid) throws Exception {
		if (wellTours==null) return true;
		if (wellTours.getWellTour()==null) return true;
		
		Operation operation = null;
		if (this.getCurrentUserSession().getCachedAllAccessibleOperations().containsKey(operationUid)) {
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		}
		if (operation==null) return false;
		this.currentOperationUid = operation.getOperationUid();
		this.currentWellboreUid = operation.getWellboreUid();
		this.currentWellUid = operation.getWellUid();
		this.currentRigInformationUid = operation.getRigInformationUid();
		this.currentOperationCode = operation.getOperationCode();
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(this.currentWellUid);
		
		for (WellTour welltour : wellTours.getWellTour()) {
			
			if (this.currentRigInformationUid==null) this.importRig(welltour.getRig());
			if (!this.importDayTours(welltour.getDayTours())) return false; // try import daily data first 
			
			if (well.getLicenseNumber()==null) well.setLicenseNumber(welltour.getLicenseNo());
			//WellName
			if (well.getUwi()==null) well.setUwi(welltour.getUniqueWellId());
			
			String location = welltour.getSurfaceLocation().getValue();
			LocationType surfaceType = welltour.getSurfaceLocation().getType();
			if ((LocationType.CLSS).equals(surfaceType)) {
				//TODO: CLSS location
			} else if ((LocationType.DLS).equals(surfaceType)) {
				//eg. 05-24-046-10 W4M
				//DLS = well.getLocLsd1() + well.getLocSec1() + well.getLocTwp1() + well.getLocRge1() + well.getLocMer1();
				if (well.getLocLsd1()==null && well.getLocSec1()==null && well.getLocTwp1()==null 
					&& well.getLocRge1()==null && well.getLocMer1()==null) {
					String[] splitLocation = location.split(" ");
					if (splitLocation.length==2) {
						well.setLocMer1(splitLocation[1]);
						splitLocation = splitLocation[0].split("[-]");
						if (splitLocation.length==4) {
							well.setLocLsd1(splitLocation[0]);
							well.setLocSec1(splitLocation[1]);
							well.setLocTwp1(splitLocation[2]);
							well.setLocRge1(splitLocation[3]);
						}
					}
					
				}
			} else if ((LocationType.LAT_LONG).equals(surfaceType)) {
				//TODO: LAT_LONG location
				//well.getLatDeg()
				//well.getLatMinute()
				//well.getLatSecond()
				//well.getLatNs()
				//well.getLongDeg()
				//well.getLongMinute()
				//well.getLongSecond()
				//well.getLongEw()
			} else if ((LocationType.NTS).equals(surfaceType)) {
				//TODO: NTS location
			} else if ((LocationType.UTM).equals(surfaceType)) {
				//TODO: UTM location
				//well.getLocUtmZone()
			}

			if (operation.getSpudDate()==null) operation.setSpudDate(CaodcUtils.XMLGregorianCalendarToDate(welltour.getSpudDateTime(), this.currentWellUid));
			//JobNo
			if (operation.getAfeNumber()==null) operation.setAfeNumber(welltour.getAfeNo());
			if (operation.getTypeTech()==null) operation.setTypeTech(welltour.getWellType().value());
			if (operation.getRigOffHireDate()==null) operation.setRigOffHireDate(CaodcUtils.XMLGregorianCalendarToDate(welltour.getRigReleaseDateTime(), this.currentWellUid));
			
			if (welltour.getKBHeight()!=null && operation.getzKbelevation()==null) {
				Double value = CaodcUtils.convertToBaseValue(Operation.class, "zKbelevation", welltour.getKBHeight().getValue(), welltour.getKBHeight().getUom().value());
				this.KBHeight = value;
				operation.setzKbelevation(value);
			}
			// - Contractor/ContractorId
			if (operation.getContractorName()==null) operation.setContractorName(welltour.getContractor().getName());
			if (operation.getOpCo()==null) operation.setOpCo(welltour.getOperator().getName());
			if (operation.getRigInformationUid()==null) operation.setRigInformationUid(this.currentRigInformationUid);
			if (this._selectedImportItems.containsKey("Operation")) {
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well, queryProperties);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation, queryProperties);
			}
			
			break;
		}
		return true;
	}
	
	private String importRig(Rig rig) {
		try {
			String strSql = "FROM RigInformation where (isDeleted=false or isDeleted is null) AND rigName=:rigName";
			List<RigInformation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigName", rig.getRigId());
			RigInformation rigInformation = null;
			if (rs.size()>0) {
				rigInformation = rs.get(0);
			} else {
				rigInformation = new RigInformation();
				rigInformation.setRigName(rig.getRigId());
				if (this._selectedImportItems.containsKey("Operation")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigInformation, queryProperties);
			}
			this.currentRigInformationUid = rigInformation.getRigInformationUid();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.currentRigInformationUid; 
	}
	
	private Boolean importDayTours(DayTours dayTours) throws Exception {
		if (dayTours==null) return true;
		if (dayTours.getDayTour()==null) return true;
		for (DayTour dayTour : dayTours.getDayTour()) {
			bharunTour = new HashMap<BigInteger, String>();
			
			Date date = CaodcUtils.XMLGregorianCalendarToDate(dayTour.getDate(), this.currentWellUid);
			
			String strSql = "FROM ReportDaily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and reportType='DDR' and reportDatetime>:currentDate";
			List<ReportDaily>rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid","currentDate"}, new Object[] {this.currentOperationUid, date});
			if (rs.size()>0) { // check import date is greater than last day
				Date reportDatetime = rs.get(0).getReportDatetime();
				strSql = "FROM ReportDaily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and reportType='DDR' and reportDatetime=:currentDate";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid","currentDate"}, new Object[] {this.currentOperationUid, date});
				if (rs.size()>0) {
					//do nothing
				} else {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					errorMessage.add("Cannot create new day [" + dateFormat.format(date) + "] which is date before the last day [" + (reportDatetime==null?"":dateFormat.format(reportDatetime)) + "].");
					return false;
				}
			}
			
			Daily daily = CommonUtil.getConfiguredInstance().createNewDaily(date, this.currentOperationUid, null, null);
			
			strSql = "FROM ReportDaily where (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid and reportType='DDR'";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", daily.getDailyUid());
			ReportDaily reportDaily = null;
			if (rs.size()>0) {
				reportDaily = rs.get(0);
				if ("1".equals(reportDaily.getQcFlag())) {
					errorMessage.add("Rig Side QC Done");
					return false;
				} else if ("2".equals(reportDaily.getQcFlag())) {
					errorMessage.add("Town Side QC Done");
					return false;
				}
			} else {
				String reportNumber = "1";
				strSql = "FROM ReportDaily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and reportType='DDR' ORDER BY reportDatetime DESC";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid"}, new Object[] {this.currentOperationUid});
				if (rs.size()>0) {
					reportDaily = rs.get(0);
					reportNumber = reportDaily.getReportNumber();
					try {
						reportNumber = (Integer.parseInt(reportNumber) + 1) + "";
					} catch (Exception e) {
						reportNumber = "1";
					}
				}
				
				reportDaily = new ReportDaily();
				reportDaily.setWellUid(this.currentWellUid);
				reportDaily.setWellboreUid(this.currentWellboreUid);
				reportDaily.setOperationUid(this.currentOperationUid);
				reportDaily.setDailyUid(daily.getDailyUid());
				reportDaily.setReportDatetime(date);
				reportDaily.setReportType("DDR");
				reportDaily.setReportNumber(reportNumber); 
			}
			if (reportDaily.getDrillengineer()==null) reportDaily.setDrillengineer(dayTour.getContractorRep());
			//ContractorSigned
			if (reportDaily.getSupervisor()==null) reportDaily.setSupervisor(dayTour.getOperatorRep());
			//OperatorSigned
			if (reportDaily.getRigInformationUid()==null) reportDaily.setRigInformationUid(this.currentRigInformationUid);
			//if (this._selectedImportItems.containsKey("ReportDaily")) {
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily, queryProperties);
			//}
			
			this.importWeather(daily.getDailyUid(), dayTour.getWeather());
			this.importTubular(daily.getDailyUid(), dayTour.getTubular());
			this.importEquipment(daily.getDailyUid(), dayTour.getEquipment());
			//-X: OperatorChecks
			//-X: ContractorChecks
			//-X: FuelAt800
			//-X: DayPayroll
			this.importTours(daily.getDailyUid(), dayTour.getTours());
		}
		return true;
	}
	
	private Boolean importTubular(String dailyUid, Tubular tubular) throws Exception {
		if (tubular==null) return true;
		if (tubular.getCasings()!=null) this.importCasings(dailyUid, tubular.getCasings());
		if (tubular.getCoils()!=null) this.importCoils(dailyUid, tubular.getCoils());
		if (tubular.getDrillPipes()!=null) this.importDrillPipes(dailyUid, tubular.getDrillPipes());
		return true;
	}
	
	private Boolean importCoils(String dailyUid, Coils coils) throws Exception {
		if (coils.getCoil()==null) return true;
		for (Coil coil : coils.getCoil()) {
			//TODO: import coils
		}
		return true;
	}
	
	private Boolean importDrillPipes(String dailyUid, DrillPipes drillPipes) throws Exception {
		if (drillPipes.getDrillPipe()==null) return true;
		for (DrillPipe drillPipe : drillPipes.getDrillPipe()) {
			//TODO: import drill pipe
		}
		return true;
	}
	
	private Boolean importCasings(String dailyUid, Casings casings) throws Exception {
		if (casings==null) return true;
		if (casings.getCasing()==null) return true;
		String uom;
		for (Casing casing : casings.getCasing()) {
			
			CasingSection casingSection = null;
			String strSql = "FROM CasingSection where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and sectionName=:sectionName";
			List<CasingSection> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"operationUid","sectionName"}, new Object[]{this.currentOperationUid, casing.getCategory().value()});
			if (rs.size()>0) {
				casingSection = rs.get(0);
			} else {
				casingSection = new CasingSection();
				casingSection.setSectionName(casing.getCategory().value());
				casingSection.setWellUid(this.currentWellUid);
				casingSection.setWellboreUid(this.currentWellboreUid);
				casingSection.setOperationUid(this.currentOperationUid);
				casingSection.setGrade(casing.getGrade());
				if (this._selectedImportItems.containsKey("CasingSection")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingSection, queryProperties);
			}
			
			CasingTally casingTally = new CasingTally();
			casingTally.setWellUid(this.currentWellUid);
			casingTally.setWellboreUid(this.currentWellboreUid);
			casingTally.setOperationUid(this.currentOperationUid);
			casingTally.setCasingSectionUid(casingSection.getCasingSectionUid());
			casingTally.setType("CSG");
			
			casingTally.setGrade(casing.getGrade());
			
			if (casing.getOutsideDiameter()!=null) {
				uom = casing.getOutsideDiameter().getUom().value();
				Double casingOd = CaodcUtils.convertToBaseValue(CasingTally.class, "casingOd", casing.getOutsideDiameter().getValue(), uom);
				casingTally.setCasingOd(casingOd);
			}
			
			if (casing.getInsideDiameter()!=null) {
				uom = casing.getInsideDiameter().getUom().value();
				Double casingId = CaodcUtils.convertToBaseValue(CasingTally.class, "casingId", casing.getInsideDiameter().getValue(), uom);
				casingTally.setCasingId(casingId);
			}

			if (casing.getLinearMass()!=null) {
				uom = casing.getLinearMass().getUom().value();
				Double weight = CaodcUtils.convertToBaseValue(CasingTally.class, "weight", casing.getLinearMass().getValue(), uom);
				casingTally.setWeight(weight);
			}
			
			casingTally.setTotaljointsInhole(casing.getJointsCount() * 1.0);
			casingTally.setNumJoints(casing.getJointsCount());
			
			if (casing.getTotalLength()!=null) {
				uom = casing.getTotalLength().getUom().value();
				Double length = CaodcUtils.convertToBaseValue(CasingTally.class, "csgLength", casing.getTotalLength().getValue(), uom);
				casingTally.setCsgLength(length);
			}
			
			if (casing.getKBToCasingBottom()!=null) {
				uom = casing.getKBToCasingBottom().getUom().value();
				Double depth = CaodcUtils.convertToBaseValue(CasingTally.class, "sectionBottomMdMsl", casing.getKBToCasingBottom().getValue(), uom);
				depth = CaodcUtils.offsetDatumDepth(depth, this.KBHeight);
				casingTally.setSectionBottomMdMsl(depth);
			}
			
			if (casing.getKBToCasingHead()!=null) {
				uom = casing.getKBToCasingHead().getUom().value();
				Double depth = CaodcUtils.convertToBaseValue(CasingTally.class, "sectionTopMdMsl", casing.getKBToCasingHead().getValue(), uom);
				depth = CaodcUtils.offsetDatumDepth(depth, this.KBHeight);
				casingTally.setSectionTopMdMsl(depth);
			}
			
			if (this._selectedImportItems.containsKey("CasingSection")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingTally, queryProperties);
		}
		
		return true;
	}
	
	private Boolean importWeather(String dailyUid, Weather weather) throws Exception {
		if (weather==null) return true;
		
		WeatherEnvironment weatherEnv = new WeatherEnvironment();
		weatherEnv.setWellUid(this.currentWellUid);
		weatherEnv.setWellboreUid(this.currentWellboreUid);
		weatherEnv.setOperationUid(this.currentOperationUid);
		weatherEnv.setDailyUid(dailyUid);
		weatherEnv.setReportDatetime(CaodcUtils.XMLGregorianCalendarToDate(weather.getTime(), this.currentWellUid));
		if (weather.getTemperature()!=null) {
			String uom = weather.getTemperature().getUom().value();
			Double value = CaodcUtils.convertToBaseValue(WeatherEnvironment.class, "temperaturemax", weather.getTemperature().getValue(), uom);
			weatherEnv.setTemperaturemax(value);
		}
		
		DirectionType windDir = weather.getWindDirection();
		Double dir = 0.0;
		if ((DirectionType.E).equals(windDir)) {
			dir = 90.0;
		} else if ((DirectionType.N).equals(windDir)) {
			dir = 0.0;
		} else if ((DirectionType.NE).equals(windDir)) {
			dir = 45.0;
		} else if ((DirectionType.NW).equals(windDir)) {
			dir = 315.0;
		} else if ((DirectionType.S).equals(windDir)) {
			dir = 180.0;
		} else if ((DirectionType.SE).equals(windDir)) {
			dir = 135.0;
		} else if ((DirectionType.SW).equals(windDir)) {
			dir = 225.0;
		} else if ((DirectionType.W).equals(windDir)) {
			dir = 270.0;
		}
		dir = CaodcUtils.convertToBaseValue(WeatherEnvironment.class, "windDirection", dir, "deg");
		weatherEnv.setWindDirection(dir);
		
		WindStrengthType strength = weather.getWindStrength();
		Double windspeedmin = null;
		Double windspeedmax = null;
		Double windspeed = null;
		if ((WindStrengthType.UP_TO_19_KM_H).equals(strength)) {
			windspeedmin = 0.0;
			windspeed = 9.5;
			windspeedmax = 19.0;
		} else if ((WindStrengthType.UP_TO_29_KM_H).equals(strength)) {
			windspeedmin = 19.0;
			windspeed = 24.0;
			windspeedmax = 29.0;
		} else if ((WindStrengthType.UP_TO_39_KM_H).equals(strength)) {
			windspeedmin = 29.0;
			windspeed = 34.0;
			windspeedmax = 39.0;
		} else if ((WindStrengthType.UP_TO_50_KM_H).equals(strength)) {
			windspeedmin = 39.0;
			windspeed = 44.5;
			windspeedmax = 50.0;
		} else if ((WindStrengthType.UP_TO_62_KM_H).equals(strength)) {
			windspeedmin = 50.0;
			windspeed = 56.5;
			windspeedmax = 62.0;
		} else if ((WindStrengthType.UP_TO_75_KM_H).equals(strength)) {
			windspeedmin = 62.0;
			windspeed = 68.5;
			windspeedmax = 75.0;
		} else if ((WindStrengthType.UP_TO_87_KM_H).equals(strength)) {
			windspeedmin = 75.0;
			windspeed = 81.0;
			windspeedmax = 87.0;
		} else if ((WindStrengthType.UP_TO_102_KM_H).equals(strength)) {
			windspeedmin = 87.0;
			windspeed = 94.5;
			windspeedmax = 102.0;
		} else if ((WindStrengthType.ABOVE_102_KM_H).equals(strength)) {
			windspeedmin = 102.0;
		}
		if (windspeedmin!=null) windspeedmin = CaodcUtils.convertToBaseValue(WeatherEnvironment.class, "windspeedmin", windspeedmin, "km/h");
		if (windspeedmax!=null) windspeedmax = CaodcUtils.convertToBaseValue(WeatherEnvironment.class, "windspeedmin", windspeedmax, "km/h");
		if (windspeed!=null) windspeed = CaodcUtils.convertToBaseValue(WeatherEnvironment.class, "windspeedmin", windspeed, "km/h");
		weatherEnv.setWindspeedmin(windspeedmin);
		weatherEnv.setWindspeedmax(windspeedmax);
		weatherEnv.setWindSpeed(windspeed);
		//RoadCondition
		//Condition
		if (this._selectedImportItems.containsKey("WeatherEnvironment")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(weatherEnv, queryProperties);
		
		return true;
	}
	
	private Boolean importTours(String dailyUid, Tours allTours) throws Exception {
		if (allTours==null) return true;
		if (allTours.getTour()==null) return true;
		for (Tour tour : allTours.getTour()) {
			this.importMudRecords(dailyUid, tour.getMudRecord());
			this.importSolidsControls(dailyUid, tour.getSolidsControls());
			this.importDrillRecords(dailyUid, tour.getDrillRecords());
			this.importCirculationPressure(dailyUid, tour.getCirculationPressure());
			this.importReducedPumpSpeeds(dailyUid, tour.getReducedPumpSpeeds());
			this.importTimeLog(dailyUid, tour.getTimeLogs());
			this.importPayroll(dailyUid, tour.getTourPayroll());
			this.importBharun(dailyUid, tour.getDrillingAssemblys(), tour.getTourId());
			//TODO -X: HoleCondition
			//TODO -X: Boilers
			this.importDeviationSurveys(dailyUid, tour.getDeviationSurveys());
			//TODO -X: WellEvents
		}
		
		return true;
	}
	
	private Boolean importBharun(String dailyUid, DrillingAssemblys drillingAssemblys, BigInteger tourId) throws Exception {
		if (!this._selectedImportItems.containsKey("BhaBit")) return true;
		if (drillingAssemblys==null) return true;
		for (DrillingAssembly drillingAssembly : drillingAssemblys.getDrillingAssembly()) {
			//Bharun bharun = null;
			if (bharunTour.containsKey(tourId)) {
				String bharunUid = bharunTour.get(tourId);
				BharunDailySummary bharunDailySummary = new BharunDailySummary();
				String queryString = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and bharunUid=:bharunUid";
				List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid", "bharunUid"}, new Object[] {dailyUid, bharunUid});
				if (list.size()>0) {
					bharunDailySummary = list.get(0);
				} else {
					bharunDailySummary.setBharunUid(bharunUid);
					bharunDailySummary.setDailyUid(dailyUid);
				}
				if (drillingAssembly.getWeightOfString()!=null) {
					String uom = drillingAssembly.getWeightOfString().getUom();
					Double weightString = CaodcUtils.convertToBaseValue(SurveyStation.class, "depthMdMsl", drillingAssembly.getWeightOfString().getValue(), uom);
					bharunDailySummary.setWeightString(weightString);
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary, queryProperties);
				
			} else {
				//TODO: Bharun
			}
			//TODO: BhaComponent
		}
		
		return true;
	}
	
	private Boolean importDeviationSurveys(String dailyUid, DeviationSurveys surveys) throws Exception {
		if (surveys==null) return true;
		if (surveys.getDeviationSurvey()==null) return true;
		SurveyReference sr = null;
		String strSql = "FROM SurveyReference WHERE (isDeleted=false or isDeleted is null) and wellboreUid=:wellboreUid and (isPlanned is null or isPlanned=false)";
		List<SurveyReference> srl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", this.currentWellboreUid);
		if (srl.size()>0) {
			sr = srl.get(0);
			
		}
		for (DeviationSurvey survey : surveys.getDeviationSurvey()) {
			if (sr == null) {
				sr = new SurveyReference();
				sr.setWellUid(this.currentWellUid);
				sr.setWellboreUid(this.currentWellboreUid);
				sr.setOperationUid(this.currentOperationUid);
				sr.setVerticalSectionAngle(0.0);
				if (this._selectedImportItems.containsKey("SurveyStation")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sr, queryProperties);
			}
			SurveyStation station = new SurveyStation();
			if (survey.getDepth()!=null) {
				String uom = survey.getDepth().getUom().value();
				Double depthMdMsl = CaodcUtils.convertToBaseValue(SurveyStation.class, "depthMdMsl", survey.getDepth().getValue(), uom);
				strSql = "FROM SurveyStation WHERE (isDeleted=false or isDeleted is null) AND surveyReferenceUid=:surveyReferenceUid and depthMdMsl=:depthMdMsl";
				List<SurveyStation> stl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"surveyReferenceUid", "depthMdMsl"}, new Object[] {sr.getSurveyReferenceUid(), depthMdMsl}, queryProperties);
				if (stl.size()>0) station = stl.get(0);
				station.setDepthMdMsl(depthMdMsl);
			}
			station.setSurveyReferenceUid(sr.getSurveyReferenceUid());
			station.setWellUid(this.currentWellUid);
			station.setWellboreUid(this.currentWellboreUid);
			station.setDailyUid(dailyUid);
			if (survey.getSurveyType()!=null) {
				station.setToolType(survey.getSurveyType().value());
			}
			Double dir = CaodcUtils.convertToBaseValue(SurveyStation.class, "inclinationAngle", survey.getDeviation(), "deg");
			station.setInclinationAngle(dir);
			
			try {
				dir = Double.parseDouble(survey.getDirection());
				dir = CaodcUtils.convertToBaseValue(SurveyStation.class, "azimuthAngle", dir, "deg");
				station.setAzimuthAngle(dir);
			} catch (Exception e) {
				//do nothing
			}
			if (this._selectedImportItems.containsKey("SurveyStation")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(station, queryProperties);
		}
		return true;
	}
	
	private Boolean importPayroll (String dailyUid, TourPayroll payroll) throws Exception {
		if (payroll==null) return true;
		if (payroll.getEmployees()==null) return true;
		if (payroll.getEmployees().getEmployee()==null) return true;
		Integer seq = 1;
		for (Employee employee : payroll.getEmployees().getEmployee()) {
			PersonnelOnSite pob = new PersonnelOnSite();
			pob.setWellUid(this.currentWellUid);
			pob.setWellboreUid(this.currentWellboreUid);
			pob.setOperationUid(this.currentOperationUid);
			pob.setRigInformationUid(this.currentRigInformationUid);
			pob.setDailyUid(dailyUid);
			pob.setPax(1);
			pob.setSequence(seq);
			
			pob.setEmployeeNum(employee.getEmployeeNo());
			long diff = employee.getToTime().toGregorianCalendar().getTimeInMillis() - employee.getFromTime().toGregorianCalendar().getTimeInMillis();
			Double hours = diff / 1000.0;
			pob.setHoursworked(hours / 60 / 60);
			pob.setWorkingHours(hours);
			//employee.getInitial()
			//employee.getLevel()
			pob.setCrewName(employee.getName());
			pob.setCrewPosition(employee.getPosition());
			
			if (employee.getRemarks()!=null) {
				String remarks = "";
				if (employee.getRemarks().getRemark()!=null) {
					for (String remark : employee.getRemarks().getRemark()) {
						remarks += ("".equals(remarks)?"":"\n") + "- " + remark;
					}
				}
				pob.setComment(remarks);
			}
			pob.setCrewGroup(employee.getWorkType().name());
			
			if (this._selectedImportItems.containsKey("PersonnelOnSite")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(pob, queryProperties);
			
			seq++;
		}
		return true;
	}
	
	private Boolean importDrillRecords(String dailyUid, DrillRecords drillRecords) throws Exception {
		if (drillRecords==null) return true;
		if (drillRecords.getDrillRecord()==null) return true;
		String uom;
		for (DrillRecord drillRecord : drillRecords.getDrillRecord()) {
			DrillingParameters drillParam = new DrillingParameters();
			drillParam.setWellUid(this.currentWellUid);
			drillParam.setWellboreUid(this.currentWellboreUid);
			drillParam.setOperationUid(this.currentOperationUid);
			drillParam.setDailyUid(dailyUid);
			drillParam.setBharunUid(""); //TODO: set bharunUid
			
			if (drillRecord.getFromDepth()!=null) {
				uom = drillRecord.getFromDepth().getUom().value();
				Double depthTopMdMsl = CaodcUtils.convertToBaseValue(DrillingParameters.class, "depthTopMdMsl", drillRecord.getFromDepth().getValue(), uom);
				drillParam.setDepthTopMdMsl(depthTopMdMsl);
			}
			
			if (drillRecord.getToDepth()!=null) {
				uom = drillRecord.getToDepth().getUom().value();
				Double depthMdMsl = CaodcUtils.convertToBaseValue(DrillingParameters.class, "depthMdMsl", drillRecord.getToDepth().getValue(), uom);
				drillParam.setDepthMdMsl(depthMdMsl);
			}
			
			drillParam.setDrillMode(drillRecord.getActivity().name());
			drillParam.setDhRpmAvg(drillRecord.getRotaryRpm() * 1.0);
			
			if (drillRecord.getWeightOnBit()!=null) {
				uom = drillRecord.getWeightOnBit().getUom();
				Double wobAvg = CaodcUtils.convertToBaseValue(DrillingParameters.class, "wobAvg", drillRecord.getWeightOnBit().getValue(), uom);
				drillParam.setWobAvg(wobAvg);
			}
			
			if (this._selectedImportItems.containsKey("DrillingParameters")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(drillParam, queryProperties);
		}
		return true;
	}
	
	private Boolean importMudRecords(String dailyUid, MudRecord mudRecord) throws Exception {
		if (mudRecord==null) return true;
		if (mudRecord.getMudMaterials()!=null) this.importMudMaterials(dailyUid, mudRecord.getMudMaterials());
		if (mudRecord.getMudSamples()!=null) this.importMudSamples(dailyUid, mudRecord.getMudSamples(), mudRecord.isIsWaterBased(), mudRecord.isIsOilBased(), mudRecord.getMudSystemType());
		return true;
	}
	
	private Boolean importMudSamples(String dailyUid, MudSamples mudSamples, Boolean isWaterBased, Boolean isOilBased, String mudSystemType) throws Exception {
		if (mudSamples.getMudSample()==null) return true;
		String uom;
		for (MudSample sample:mudSamples.getMudSample()) {
			MudProperties mudProp = new MudProperties();
			mudProp.setWellUid(this.currentWellUid);
			mudProp.setWellboreUid(this.currentWellboreUid);
			mudProp.setOperationUid(this.currentOperationUid);
			mudProp.setDailyUid(dailyUid);
			
			if (isWaterBased) mudProp.setMudType("h2o");
			else if (isOilBased) mudProp.setMudType("oil");
			
			mudProp.setReportTime(CaodcUtils.XMLGregorianCalendarToDate(sample.getTime(), this.currentWellUid));
			mudProp.setMudMedium(mudSystemType);
			mudProp.setSampleTakenFrom(sample.getLocation());
			if (sample.getFluidPh()!=null) mudProp.setMudPh(Double.parseDouble(sample.getFluidPh().toString()));
			
			if (sample.getFunnelViscosity()!=null) {
				uom = sample.getFunnelViscosity().getUom().value();
				Double fv = CaodcUtils.convertToBaseValue(MudProperties.class, "mudFv", sample.getFunnelViscosity().getValue() * 1.0, uom);
				mudProp.setMudFv(fv);
			}
			
			if (sample.getDepth()!=null) {
				uom = sample.getDepth().getUom().value();
				Double depth = CaodcUtils.convertToBaseValue(MudProperties.class, "depthMdMsl", sample.getDepth().getValue(), uom);
				depth = CaodcUtils.offsetDatumDepth(depth, this.KBHeight);
				mudProp.setDepthMdMsl(depth);
			}
			
			if (sample.getPVT()!=null) {
				uom = sample.getPVT().getUom().value();
				Double mudPv = CaodcUtils.convertToBaseValue(MudProperties.class, "mudPv", sample.getPVT().getValue(), uom);
				mudProp.setMudPv(mudPv);
			}
			
			if (sample.getDensity()!=null) {
				uom = sample.getDensity().getUom().value();
				Double mudWeight = CaodcUtils.convertToBaseValue(MudProperties.class, "mudWeight", sample.getDensity().getValue(), uom);
				mudProp.setMudWeight(mudWeight);
			}
			
			//TODO: sample.getWaterLoss()

			if (sample.getRemarks()!=null) {
				String remarks = "";
				if (sample.getRemarks().getRemark()!=null) {
					for (String remark : sample.getRemarks().getRemark()) {
						remarks += ("".equals(remarks)?"":"\n") + "- " + remark;
					}
				}
				mudProp.setMudEngineerSummary(remarks);
			}
			
			if (sample.getOtherTests()!=null) {
				if (sample.getOtherTests().getOtherTest()!=null) {
					int counter = 0;
					for (OtherTest other : sample.getOtherTests().getOtherTest()) {
						if (counter==0) {
							mudProp.setOtherlabel1(other.getTestType());
							mudProp.setOthervalue1(other.getTestValue().getValue() + other.getTestValue().getUom());
						} else if (counter==1) {
							mudProp.setOtherlabel2(other.getTestType());
							mudProp.setOthervalue2(other.getTestValue().getValue() + other.getTestValue().getUom());
						} else if (counter==2) {
							mudProp.setOtherlabel3(other.getTestType());
							mudProp.setOthervalue3(other.getTestValue().getValue() + other.getTestValue().getUom());
						} else if (counter==3) {
							mudProp.setOtherlabel4(other.getTestType());
							mudProp.setOthervalue4(other.getTestValue().getValue() + other.getTestValue().getUom());
						} else if (counter==4) {
							mudProp.setOtherlabel5(other.getTestType());
							mudProp.setOthervalue5(other.getTestValue().getValue() + other.getTestValue().getUom());
						}
						counter++;
						if (counter>=5) break;
					}
				}
			}
			
			if (this._selectedImportItems.containsKey("MudProperties")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mudProp, queryProperties);
	}
		return true;
	}
	
	private Boolean importMudMaterials(String dailyUid, MudMaterials mudMaterials) throws Exception {
		if (mudMaterials==null) return true;
		if (mudMaterials.getMudMaterial()==null) return true;
		for (MudMaterial mudMaterial : mudMaterials.getMudMaterial()) {
			RigStock rigStock = new RigStock();
			rigStock.setWellUid(this.currentWellUid);
			rigStock.setWellboreUid(this.currentWellboreUid);
			rigStock.setOperationUid(this.currentOperationUid);
			rigStock.setDailyUid(dailyUid);
			rigStock.setType("fluids");
			rigStock.setStockBrand(mudMaterial.getProduct());
			rigStock.setStockCode(mudMaterial.getProduct());
			rigStock.setStockShortDescription(mudMaterial.getProduct());
			if (mudMaterial.getUnitQuantity()!=null) {
				rigStock.setStoredUnit(mudMaterial.getUnitQuantity().getValue() + "" + mudMaterial.getUnitQuantity().getUom());
			}
			rigStock.setAmtUsed(mudMaterial.getAmount() * 1.0);
			
			if (this._selectedImportItems.containsKey("RigStock")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigStock, queryProperties);
		}
		return true;
	}
	
	private Boolean importSolidsControls(String dailyUid, SolidsControls solidsControls) throws Exception {
		if (solidsControls==null) return true;
		MudVolumes mudVol = new MudVolumes();
		mudVol.setWellUid(this.currentWellUid);
		mudVol.setWellboreUid(this.currentWellboreUid);
		mudVol.setOperationUid(this.currentOperationUid);
		mudVol.setDailyUid(dailyUid);
		mudVol.setChecknumber("1");
		if (this._selectedImportItems.containsKey("MudVolumes")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mudVol, queryProperties);
		String mudVolumesUid = mudVol.getMudVolumesUid();
		
		if (solidsControls.getSolidsControl()!=null) {
			for (SolidsControl solidsControl : solidsControls.getSolidsControl()) {
				SolidControlEquipment solidControlEquipment = new SolidControlEquipment();
				solidControlEquipment.setOperationUid(this.currentOperationUid);
				solidControlEquipment.setDailyUid(dailyUid);
				solidControlEquipment.setMudVolumesUid(mudVolumesUid);
				solidControlEquipment.setName(solidsControl.getEquipmentName());
				String remarks = "";
				if (solidsControl.getRemarks()!=null) {
					if (solidsControl.getRemarks().getRemark()!=null) {
						for (String remark : solidsControl.getRemarks().getRemark()) {
							remarks += ("".equals(remarks)?"":"\n") + "- " + remark;
						}
					}
				}
				solidControlEquipment.setComments(remarks);
				solidControlEquipment.setHoursUsed(Double.parseDouble(solidsControl.getHoursRun().toString()));
				if (this._selectedImportItems.containsKey("MudVolumes")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(solidControlEquipment, queryProperties);
			}
		}
		return true;
	}
	
	private Boolean importTimeLog(String dailyUid, TimeLogs timeLogs) throws Exception {
		if (timeLogs==null) return true;
		if (timeLogs.getTimeLog()==null) return true;
		for (TimeLog timeLog : timeLogs.getTimeLog()) {
			Activity activity = new Activity();
			activity.setWellUid(this.currentWellUid);
			activity.setWellboreUid(this.currentWellboreUid);
			activity.setOperationUid(this.currentOperationUid);
			activity.setDailyUid(dailyUid);
			activity.setStartDatetime(CaodcUtils.XMLGregorianCalendarToDate(timeLog.getFromTime(), this.currentWellUid));
			activity.setEndDatetime(CaodcUtils.XMLGregorianCalendarToDate(timeLog.getToTime(), this.currentWellUid));
			
			long duration  = timeLog.getToTime().toGregorianCalendar().getTimeInMillis() - timeLog.getFromTime().toGregorianCalendar().getTimeInMillis(); 
			activity.setActivityDuration(duration / 1000.0);
			
			activity.setActivityDescription(timeLog.getDetail());
			activity.setTaskCode(this.getTaskCode(timeLog.getTimeCodeNo()));
			
			if (this._selectedImportItems.containsKey("Activity")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(activity, queryProperties);
		}
		return true;
	}
	
	private String getTaskCode(String pasonCode) {
		String idsCode = pasonCode;
		try {
			String strSql = "FROM LookupTaskCode where (isDeleted=false or isDeleted is null) and pasonCode=:pasonCode and operationCode=:operationCode";
			List<LookupTaskCode> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"pasonCode","operationCode"}, new Object[] {pasonCode, this.currentOperationCode});
			if (rs.size()>0) {
				LookupTaskCode taskCode = rs.get(0);
				if (taskCode.getShortCode()!=null) idsCode = taskCode.getShortCode();
			}
			if (idsCode.equals(pasonCode)) {
				Map<String, LookupItem> lookupResult = LookupManager.getConfiguredInstance().getLookup("xml://pason.timeLogCode?key=code&amp;value=label", new UserSelectionSnapshot(this.getCurrentUserSession()),null);
				if (lookupResult!=null) {
					if (lookupResult.containsKey(pasonCode)) {
						idsCode = lookupResult.get(pasonCode).getValue().toString();
						if(idsCode.length()>19) {
							idsCode = idsCode.substring(0, 16) + "...";
						} 
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idsCode;
	}
	
	private Boolean importEquipment(String dailyUid, Equipment equipments) throws Exception {
		if (equipments==null) return true;
		//-X: EquipmentOfServices
		//-X: Boilers
		//-X: ShaleShakes
		if (equipments.getMudPumps()!=null) this.importMudPumps(dailyUid, equipments.getMudPumps());
		if (equipments.getBits()!=null) this.importBits(dailyUid, equipments.getBits());
		return true;
	}
	
	private String createBharun(String dailyUid, String bhaRunNumber) throws Exception {
		String queryString = "FROM Bharun WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid and bhaRunNumber=:bhaRunNumber";
		List<Bharun> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid", "bhaRunNumber"}, new Object[]{this.currentOperationUid, bhaRunNumber});
		String bharunUid = null; 
		for (Bharun rec : list) {
			bharunUid = rec.getBharunUid();
			queryString = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid and dailyUid=:dailyUid ";
			List<BharunDailySummary> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"bharunUid", "dailyUid"}, new Object[]{bharunUid, dailyUid});
			if (list2.size()>0) return bharunUid;
		}
		if (bharunUid==null) {
			Bharun bharun = new Bharun();
			bharun.setBhaRunNumber(bhaRunNumber);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun, queryProperties);
			bharunUid = bharun.getBharunUid();
		}
		BharunDailySummary dailySummary = new BharunDailySummary();
		dailySummary.setBharunUid(bharunUid);
		dailySummary.setDailyUid(dailyUid);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dailySummary, queryProperties);
		return bharunUid;
	}
	
	private Boolean importBits(String dailyUid, Bits bits) throws Exception {
		if (!this._selectedImportItems.containsKey("BhaBit")) return true; 
		if (bits.getBit()==null) return true;
		for (Bit bit : bits.getBit()) {
			Bitrun bitrun = null;
			String bitrunNumber = bit.getBitNo();
			String queryString = "FROM Bitrun WHERE (isDeleted=false or isDeleted is null) and bitrunNumber=:bitrunNumber and operationUid=:operationUid";
			List<Bitrun> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"bitrunNumber","operationUid"}, new Object[]{bitrunNumber, this.currentOperationUid});
			if (list.size()>0) {
				bitrun = list.get(0);
			} else {
				bitrun = new Bitrun();
				bitrun.setBitrunNumber(bitrunNumber);
			}
			String bharunUid = this.createBharun(dailyUid, bitrunNumber);
			bitrun.setBharunUid(bharunUid);
			bharunTour.put(bit.getTourId(), bharunUid);
			
			if (bit.getSize()!=null) {
				String uom = bit.getSize().getUom().value();
				Double bitSize = CaodcUtils.convertToBaseValue(Bitrun.class, "bitDiameter", bit.getSize().getValue(), uom);
				bitrun.setBitDiameter(bitSize);
			}
			bitrun.setIadcCode(bit.getIADCCode1());
			bitrun.setMake(bit.getManufacturer());
			bitrun.setCutterType(bit.getBitType());
			bitrun.setSerialNumber(bit.getSerialNo());
			
			if (bit.getDepthIn()!=null) {
				String uom = bit.getDepthIn().getUom().value();
				Double depthIn = CaodcUtils.convertToBaseValue(Bitrun.class, "depthInMdMsl", bit.getDepthIn().getValue(), uom);
				bitrun.setDepthInMdMsl(depthIn);
			}
			if (bit.getDepthOut()!=null) {
				String uom = bit.getDepthOut().getUom().value();
				Double depthOut = CaodcUtils.convertToBaseValue(Bitrun.class, "depthOutMdMsl", bit.getDepthOut().getValue(), uom);
				bitrun.setDepthOutMdMsl(depthOut);
			}
			
			if (bit.getRemarks()!=null) {
				String remarks = "";
				if (bit.getRemarks().getRemark()!=null) {
					for (String remark : bit.getRemarks().getRemark()) {
						remarks += ("".equals(remarks)?"":"\n") + "- " + remark;
					}
				}
				bitrun.setComment(remarks);
			}
			
			if (bit.getBitCondition()!=null) {
				BitCondition conditions = bit.getBitCondition();
				bitrun.setCondFinalInner(conditions.getCsTI());
				bitrun.setCondFinalOuter(conditions.getCsTO());
				if (conditions.getCsMdc()!=null) bitrun.setCondFinalDull(conditions.getCsMdc().value());
				bitrun.setCondFinalLocation(conditions.getCsLoc());
				bitrun.setCondFinalBearing(conditions.getBearing());
				if (conditions.getGage()!=null) {
					String uom = conditions.getGage().getUom().value();
					Double gage = CaodcUtils.convertToBaseValue(Bitrun.class, "bitDiameter", conditions.getGage().getValue(), uom);
					gage = Math.round(gage / 0.0254 / 100.0) / 100.0;
					bitrun.setCondFinalGauge(gage + "\"");
				}
				if (conditions.getOdc()!=null) bitrun.setCondFinalOther(conditions.getOdc().value());
				if (conditions.getReasonPulled()!=null) bitrun.setCondFinalReason(conditions.getReasonPulled().name());
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, queryProperties);
			//String bitrunUid = bitrun.getBitrunUid();
		}
		return true;
	}
	
	private RigPump getRigPump(MudPump mudPump, String pumpNo) throws Exception {
		RigPump rigPump = null;
		
		if (mudPump!=null) {
			String strSql = "FROM RigPump where (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid and unitNumber=:unitNumber";
			List<RigPump> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"rigInformationUid","unitNumber"}, new Object[] {this.currentRigInformationUid, mudPump.getPumpNo()+""});
			if (rs.size()>0) {
				rigPump = rs.get(0);
			} else {
				rigPump = new RigPump();
				rigPump.setRigInformationUid(this.currentRigInformationUid);
				rigPump.setUnitNumber(mudPump.getPumpNo()+"");
			}
			rigPump.setUnitName(mudPump.getMake()+"");
			if (rigPump.getUnitModel()==null) rigPump.setUnitModel(mudPump.getModel());
			if (rigPump.getUnitNumber()==null) rigPump.setUnitNumber(mudPump.getSerialNo());
			
			if (rigPump.getStrokeLength()==null) {
				String uom = mudPump.getStrokeLength().getUom().value();
				Double strokeLength = CaodcUtils.convertToBaseValue(RigPump.class, "strokeLength", mudPump.getStrokeLength().getValue(), uom);
				rigPump.setStrokeLength(strokeLength);
			}
			if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPump, queryProperties);
		} else if (pumpNo!=null) {
			String strSql = "FROM RigPump where (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid and unitNumber=:unitNumber";
			List<RigPump> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"rigInformationUid","unitNumber"}, new Object[] {this.currentRigInformationUid, pumpNo});
			if (rs.size()>0) {
				rigPump = rs.get(0);
			} else {
				rigPump = new RigPump();
				rigPump.setRigInformationUid(this.currentRigInformationUid);
				rigPump.setUnitNumber(pumpNo);
				if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPump, queryProperties);
			}
		}
		
		return rigPump;
	}
	
	private RigPumpParam getRigPumpParam(String dailyUid, String rigPumpUid) throws Exception {
		RigPumpParam rigPumpParam = null;
		String strSql = "FROM RigPumpParam where (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and rigPumpUid=:rigPumpUid";
		List<RigPumpParam> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"dailyUid","rigPumpUid"}, new String[] {dailyUid, rigPumpUid});
		if (rs.size()>0) {
			return rs.get(0);
		} else {
			rigPumpParam = new RigPumpParam();
			rigPumpParam.setRigPumpUid(rigPumpUid);
			rigPumpParam.setDailyUid(dailyUid);
			rigPumpParam.setWellUid(this.currentWellUid);
			rigPumpParam.setWellboreUid(this.currentWellboreUid);
			rigPumpParam.setOperationUid(this.currentOperationUid);
			rigPumpParam.setPressureOut(0.0);
			if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPumpParam, queryProperties);
			return rigPumpParam;
		}
	}
	
	private Boolean importMudPumps(String dailyUid, MudPumps mudPumps) throws Exception {
		if (mudPumps==null) return true;
		if (mudPumps.getMudPump()==null) return true;
		String uom;
		for (MudPump mudPump : mudPumps.getMudPump()) {
			
			RigPump rigPump = this.getRigPump(mudPump, null);
			
			if (rigPump!=null) {
				RigPumpParam rigPumpParam = this.getRigPumpParam(dailyUid, rigPump.getRigPumpUid());
				
				if (rigPumpParam!=null) {
					if (mudPump.getPumpStyle()!=null) rigPumpParam.setSampleType(mudPump.getPumpStyle().name());
					
					if (mudPump.getStrokeLength()!=null) {
						uom = mudPump.getStrokeLength().getUom().value();
						Double stroke = CaodcUtils.convertToBaseValue(RigPump.class, "strokeLength", mudPump.getStrokeLength().getValue(), uom);
						rigPumpParam.setStroke(stroke);
					}
					if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPumpParam, queryProperties);
				}
			}
		}
		return true;
	}
	
	private Boolean importCirculationPressure(String dailyUid, CirculationPressure circulationPressure) throws Exception {
		if (circulationPressure==null) return true;
		if (circulationPressure.getCirculationPumps()==null) return true;
		if (circulationPressure.getCirculationPumps().getCirculationPump()==null) return true;
		for (CirculationPump pump : circulationPressure.getCirculationPumps().getCirculationPump()) {
			RigPump rigPump = this.getRigPump(null, pump.getPumpNo()+"");
			if (rigPump!=null) {
				RigPumpParam rigPumpParam = this.getRigPumpParam(dailyUid, rigPump.getRigPumpUid());
				
				if (rigPumpParam!=null) {
					if (pump.getLinerSize()!=null) {
						String uom = pump.getLinerSize().getUom().value();
						Double liner = CaodcUtils.convertToBaseValue(RigPumpParam.class, "liner", pump.getLinerSize().getValue(), uom);
						rigPumpParam.setLiner(liner);
					}
					
					rigPumpParam.setSpm(pump.getStrokesPerMinute() * 1.0);
					if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPumpParam, queryProperties);
				}
			}
		}
		return true;
	}
	private Boolean importReducedPumpSpeeds(String dailyUid, ReducedPumpSpeeds reducedPumpSpeeds) throws Exception {
		if (reducedPumpSpeeds==null) return true;
		if (reducedPumpSpeeds.getReducedPumpSpeed()==null) return true;
		String uom;
		for (ReducedPumpSpeed pump : reducedPumpSpeeds.getReducedPumpSpeed()) {
			RigPump rigPump = this.getRigPump(null, pump.getPumpNo()+"");
			if (rigPump!=null) {
				RigPumpParam rigPumpParam = this.getRigPumpParam(dailyUid, rigPump.getRigPumpUid());
				if (rigPumpParam!=null) {
					
					if (pump.getDepth()!=null) {
						uom = pump.getDepth().getUom().value();
						Double sampleDepth = CaodcUtils.convertToBaseValue(RigPumpParam.class, "sampleDepth", pump.getDepth().getValue(), uom);
						rigPumpParam.setSampleDepth(sampleDepth);
						if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigPumpParam, queryProperties);
					}

					//slow readings
					RigSlowPumpParam rigSlowPumpParam = new RigSlowPumpParam();
					rigSlowPumpParam.setRigPumpParamUid(rigPumpParam.getRigPumpParamUid());
					rigSlowPumpParam.setCheckNumber(1);
					rigSlowPumpParam.setRate(pump.getStrokesPerMinute() * 1.0);
					if (pump.getPressure()!=null) {
						uom = pump.getPressure().getUom().value();	
						Double pressure = CaodcUtils.convertToBaseValue(RigSlowPumpParam.class, "pressure", pump.getPressure().getValue() * 1.0, uom);
						rigSlowPumpParam.setPressure(pressure);
					}
					if (this._selectedImportItems.containsKey("RigPump")) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigSlowPumpParam, queryProperties);
					
				}
			}
		}
		return true;
	}
	
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
}
