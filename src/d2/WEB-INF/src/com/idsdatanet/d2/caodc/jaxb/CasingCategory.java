//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.1-b02-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.04.07 at 05:21:29 PM SGT 
//


package com.idsdatanet.d2.caodc.jaxb;

import javax.xml.bind.annotation.XmlEnum;


/**
 * <p>Java class for casingCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="casingCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SURFACE"/>
 *     &lt;enumeration value="INTERMEDIATE"/>
 *     &lt;enumeration value="PRODUCTION"/>
 *     &lt;enumeration value="LINER"/>
 *     &lt;enumeration value="TUBING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum CasingCategory {

    SURFACE,
    INTERMEDIATE,
    PRODUCTION,
    LINER,
    TUBING;

    public String value() {
        return name();
    }

    public static CasingCategory fromValue(String v) {
        return valueOf(v);
    }

}
