package com.idsdatanet.d2.idsinvoice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.InvReportDaily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class InvReportDailyCommandBeanListener extends EmptyCommandBeanListener {
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		commandBean.getRoot().resetPersistedDynamicAttribute("invoiceNumber");
		//commandBean.getRoot().resetPersistedDynamicAttribute("invoiceDatetime");
		
		// reset selected invReportDailyUid if client group changed 
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		if (StringUtils.isNotBlank(clientGroupUid)) {
			String invReportDailyUid = (String) commandBean.getRoot().getDynaAttr().get("invReportDailyUid");
			if (StringUtils.isNotBlank(invReportDailyUid)) {
				InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
				if (invReportDaily != null) {
					if (!clientGroupUid.equals(invReportDaily.getGroupUid())) {
						commandBean.getRoot().resetPersistedDynamicAttribute("invReportDailyUid");
					}
				}
			}
			
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select invGeneral from InvGroupClientInfo where groupUid =:clientgroup and (isDeleted = false or isDeleted is null)", "clientgroup", clientGroupUid);
			if (result != null && !result.isEmpty()) {
				String invGeneral = result.get(0).toString();
				commandBean.getRoot().getDynaAttr().put("invGeneral", invGeneral);
			}else {
				commandBean.getRoot().resetPersistedDynamicAttribute("invGeneral");
			}

		} else {
			commandBean.getRoot().resetPersistedDynamicAttribute("invReportDailyUid");
		}
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		String invReportDailyUid = (String) commandBean.getRoot().getDynaAttr().get("invReportDailyUid");
		if (StringUtils.isNotBlank(invReportDailyUid)) {
			InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
			if (invReportDaily != null) {
				//commandBean.getRoot().getDynaAttr().put("invoiceDatetime", new SimpleDateFormat("dd MMM yyyy").format(invReportDaily.getInvoiceDatetime()));
				commandBean.getRoot().getDynaAttr().put("invReportDailyLastEditUserUid", invReportDaily.getLastEditUserUid());
				
			}
		}
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		String invReportDailyUid = (String) commandBean.getRoot().getDynaAttr().get("invReportDailyUid");
		String invoiceNumber = (String) commandBean.getRoot().getDynaAttr().get("invoiceNumber");
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		
		Date datecreated = new Date();
		String invoiceNumberPrefix = clientGroupUid + "_" + new SimpleDateFormat("yyyyMMdd").format(datecreated) + "_";
		
		if (StringUtils.isNotBlank(invReportDailyUid)) {
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ReportDaily set idsinvoiceid = null where idsinvoiceid = :idsinvoiceid and (isDeleted = false or isDeleted is null)", new String[] { "idsinvoiceid" }, new Object[] { invReportDailyUid });
			boolean hasReportDailySelected = false;
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ReportDaily").values()) {
				if (node.getAtts().getSelected()) {
					ReportDaily reportDaily = (ReportDaily) node.getData();
					reportDaily.setIdsinvoiceid(invReportDailyUid);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
					hasReportDailySelected = true;
				}
			}
			if (!hasReportDailySelected) {
				// mark invoice as deleted if no report selected
				InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
				if (invReportDaily != null) {
					invReportDaily.setIsDeleted(true);
					commandBean.getSystemMessage().addInfo("Invoice " + invReportDaily.getInvoiceNumber() + " has been deleted." ,true, true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(invReportDaily);
					commandBean.getRoot().resetPersistedDynamicAttribute("invReportDailyUid");
				}
			} else {
				// touch invReportDaily so that last edit user is updated
				InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
				if (invReportDaily != null) {
					String invoiceReference = invReportDaily.getInvoiceNumber().substring(invoiceNumberPrefix.length());
					invReportDaily.setInvoiceNumber(invoiceNumberPrefix + invoiceReference);
					invReportDaily.setInvoiceDatetime(datecreated);
					// date could be updated
					//String invoiceDatetime = (String) commandBean.getRoot().getDynaAttr().get("invoiceDatetime");
					//if (StringUtils.isNotBlank(invoiceDatetime)) {
					//	invReportDaily.setInvoiceDatetime(CommonDateParser.parse(invoiceDatetime));
					//}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(invReportDaily);
					commandBean.getSystemMessage().addInfo("Invoice updated." ,true, true);
				}
			}
		} else if (StringUtils.isNotBlank(invoiceNumber)) {

			List<InvReportDaily> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from InvReportDaily where invoiceNumber like :invoiceNumber and (isDeleted = false or isDeleted is null) and groupUid= :clientGroupUid", new String[] {"invoiceNumber", "clientGroupUid"}, new Object[]{"%_"+invoiceNumber,clientGroupUid});
			if (results != null && !results.isEmpty()) {
				// add reports to an existing invoice
				InvReportDaily invReportDaily = (InvReportDaily) results.get(0);
				for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ReportDaily").values()) {
					if (node.getAtts().getSelected()) {
						ReportDaily reportDaily = (ReportDaily) node.getData();
						reportDaily.setIdsinvoiceid(invReportDaily.getInvReportDailyUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
					}
				}
				invReportDaily.setInvoiceNumber(invoiceNumberPrefix + invoiceNumber);
				invReportDaily.setInvoiceDatetime(datecreated);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(invReportDaily);
				
				commandBean.getRoot().getDynaAttr().put("invReportDailyUid", invReportDaily.getInvReportDailyUid());
				commandBean.getSystemMessage().addInfo("Report(s) added to an existing invoice." ,true, true);
			} else {
				
				
				//String invoiceDatetime = (String) commandBean.getRoot().getDynaAttr().get("invoiceDatetime");
				//if (StringUtils.isBlank(invoiceDatetime)) {
				//	commandBean.getSystemMessage().addError("Date must not be empty.");
				//} else {
					// create new invoice
					boolean hasReportDailySelected = false;
					for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ReportDaily").values()) {
						if (node.getAtts().getSelected()) {
							hasReportDailySelected = true;
							break;
						}
					}
					if (hasReportDailySelected) {
						
						InvReportDaily invReportDaily = new InvReportDaily();
						invReportDaily.setGroupUid((String) commandBean.getRoot().getDynaAttr().get("clientGroupUid"));
						invReportDaily.setInvoiceNumber(invoiceNumberPrefix + invoiceNumber);
						invReportDaily.setInvoiceDatetime(datecreated);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(invReportDaily);
						
						for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ReportDaily").values()) {
							if (node.getAtts().getSelected()) {
								ReportDaily reportDaily = (ReportDaily) node.getData();
								reportDaily.setIdsinvoiceid(invReportDaily.getInvReportDailyUid());
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
							}
						}
						commandBean.getRoot().getDynaAttr().put("invReportDailyUid", invReportDaily.getInvReportDailyUid());
						commandBean.getSystemMessage().addInfo("New invoice created." ,true, true);
					} else {
						commandBean.getSystemMessage().addError("No report selected." ,true, true);
					}
				//}
			}
		} else {
			commandBean.getSystemMessage().addError("Invoice number must not be empty." ,true, true);
		}
		
		commandBean.setRedirectUrl(CommonUtils.urlPathConcat(UserSession.getInstance(request).getApplicationContextPath(), "invreportdaily.html"));
	}
	
}
