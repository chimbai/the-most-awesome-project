package com.idsdatanet.d2.idsinvoice.stt;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.model.SysLogSendToTown;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.stt.AbstractSTTEngine;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTDataDirection;
import com.idsdatanet.d2.core.stt.STTEngine;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.stt.STTStatus;
import com.idsdatanet.d2.core.stt.STTTransactionType;
import com.idsdatanet.d2.core.stt.jaxb.SttStatus;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class InvSTTJob {
	
	private Log log = LogFactory.getLog(this.getClass());
	private boolean enabled = false;
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	private MailEngine mailEngine = null;
	private String systemPlatform = null;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}

	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public void setMailEngine(MailEngine value){
		this.mailEngine = value;
	}
	
	public void setSystemPlatform(String systemPlatform) {
		this.systemPlatform = systemPlatform;
	}
	
	public void doSendToTown() throws Exception {
		if (!"rig".equalsIgnoreCase(systemPlatform)) {
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
			try {
				userSelection.populdateDataAccordingToUserLogin(AbstractSTTEngine.SYSTEM_USER);
			} catch (Exception e) {
				log.error("Cannot find the user 'idsadmin' that is required for scheduled send to town job");
			}
			
			if (userSelection.getUserUid() != null) {
				STTStatus status = new STTStatus(); 
				SysLogSendToTown sysLog = new SysLogSendToTown();
				File f = null;
				String stringResponseBody = null;
				String error = null;
				try {
					final long start = System.currentTimeMillis();
					
					// load and marshal
					f = STTEngine.getConfiguredInstance().marshal(jobConfigs, userSelection, status, STTEngine.MARSHALLING_MODE_STT, false);
					if (f == null) {
						throw new Exception("Failed to marshal data");
					}
					sysLog.setFilename(f.getName());
					
					// extra request parameters, mainly for logging at the other end
					Map<String, String> requestParameters = new HashMap<String, String>();
					requestParameters.put("groupUid", nullToEmptyString(userSelection.getGroupUid()));
					requestParameters.put("wellUid", nullToEmptyString(userSelection.getWellUid()));
					requestParameters.put("wellboreUid", nullToEmptyString(userSelection.getWellboreUid()));
					requestParameters.put("operationUid", nullToEmptyString(userSelection.getOperationUid()));
					requestParameters.put("rigInformationUid", nullToEmptyString(userSelection.getRigInformationUid()));
					requestParameters.put("dailyUid", nullToEmptyString(userSelection.getDailyUid()));
					for (STTJobConfig jobConfig : jobConfigs) {
						requestParameters.put("jobConfigBeanNames", nullToEmptyString(jobConfig.getBeanName()));
					}
					requestParameters.put("destinationUrl", nullToEmptyString(comConfig.getDestinationUrl()));
					requestParameters.put("scheduled", "true");
					requestParameters.put("userName", AbstractSTTEngine.SYSTEM_USER);
					
					// send data
					stringResponseBody = STTEngine.getConfiguredInstance().send(comConfig, "/webservice/sendtotownservice.html", f, requestParameters, status);
					
					// parse response
					JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.core.stt.jaxb");
					Unmarshaller m = jc.createUnmarshaller();
					SttStatus sttStatus = (SttStatus) m.unmarshal(new ByteArrayInputStream(stringResponseBody.getBytes()));
					
					if (sttStatus.getErrors() != null && sttStatus.getErrors().getError().size() > 0) {
						sysLog.setStatus(STTStatus.FAIL);
					} else {
						sysLog.setStatus(STTStatus.OK);
					}
					
					status.log("Elapsed time: " + (System.currentTimeMillis() - start) + "ms");
					
				} catch (javax.xml.bind.UnmarshalException e) {
					status.log("Failed to parse response from town:\n" + stringResponseBody);
					error = stringResponseBody;
					sysLog.setStatus(STTStatus.FAIL);
				} catch (Exception e) {
					status.log("Caught exception:\n" + SystemExceptionHandler.printAsString(e));
					error = SystemExceptionHandler.printAsString(e);
					sysLog.setStatus(STTStatus.FAIL);
				} finally {
					// logging
					sysLog.setGroupUid(userSelection.getGroupUid());
					sysLog.setWellUid(userSelection.getWellUid());
					sysLog.setWellboreUid(userSelection.getWellboreUid());
					sysLog.setOperationUid(userSelection.getOperationUid());
					sysLog.setRigInformationUid(userSelection.getRigInformationUid());
					sysLog.setDailyUid(userSelection.getDailyUid());
					sysLog.setJobconfigs(STTJobConfig.jobConfigsToString(jobConfigs));
					sysLog.setTransactionDatetime(new Date());
					sysLog.setTransactionType(STTTransactionType.SEND_TO_TOWN);
					sysLog.setDataDirection(STTDataDirection.OUTGOING);
					sysLog.setIsScheduled(true);
					sysLog.setRemoteAddr(comConfig.getDestinationUrl());
					sysLog.setUserName(AbstractSTTEngine.SYSTEM_USER);
					sysLog.setTransactionLog(status.getTransactionLog().toString());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysLog);
					
					if (sysLog.getStatus().equalsIgnoreCase("FAIL")) {
						this.logInvoicingSTTFailed(sysLog, error);
					}
					System.gc();
					f.delete();
					status.dispose();
				}
				
			}
		} // if (this.enabled)
	}
	
	private String nullToEmptyString(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}
	
	public void logInvoicingSTTFailed(SysLogSendToTown sysLog, String error) throws Exception{
		if(StringUtils.isBlank(SystemExceptionHandler.getConfiguredInstance().getOnErrorSendMailTo())) return;
		
		try{
			
			String mailSubject = "IDS Invoicing Send To Town Failed (" + sysLog.getGroupUid() + ")";
			String mailContent = "<html><head></head><body>IDS Invoicing Send To Town Failed<br/><br/>" +
				"<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">" +
				"<tr><td>Date/Time:</td><td><b>" + sysLog.getTransactionDatetime() + "</b></td></tr>" +
				"<tr><td>Type:</td><td><b>" + sysLog.getTransactionType() + "</b></td></tr>" +
				"<tr><td>Incoming/Outgoing:</td><td><b>" + sysLog.getDataDirection() + "</b></td></tr>" +
				"<tr><td>Group:</td><td><b>" + sysLog.getGroupUid() + "</b></td></tr>" +
				"<tr><td>Job:</td><td><b>" + sysLog.getJobconfigs() + "</b></td></tr>" +
				"<tr><td>Remote Address:</td><td><b>" + sysLog.getRemoteAddr() + "</b></td></tr>" +
				"<tr><td>User Name:</td><td><b>" + sysLog.getUserName() + "</b></td></tr>" +
				"<tr><td>Status:</td><td><b>" + sysLog.getStatus() + "</b></td></tr>" +
				"<tr><td>Host Name:</td><td><b>" + InetAddress.getLocalHost().getHostName() + "</b></td></tr>" +
				"<tr><td>IP Address:</td><td><b>" + InetAddress.getLocalHost().getHostAddress() + "</b></td></tr>" +
				"</table><br/>" +
				error + "</body></html>";
				
			this.mailEngine.sendMail(SystemExceptionHandler.getConfiguredInstance().getOnErrorSendMailTo(), ApplicationConfig.getConfiguredInstance().getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(), mailSubject, mailContent);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
