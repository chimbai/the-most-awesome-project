package com.idsdatanet.d2.idsinvoice.invbbinfo;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.InvBbMovementLog;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

public class InvBbInfoCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		loopAllNodes(commandBean.getRoot(), request);
		commandBean.getRoot().setDirty(true);
		
	}
	
	private void loopAllNodes(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		for (String className: node.getList().keySet()) {
			if (InvBbMovementLog.class.getSimpleName().equals(className)) {
				populate(node.getCommandBean(), node.getList().get(className), request);
				return;
			}
			
			for (CommandBeanTreeNode child: node.getList().get(className).values()) {
				loopAllNodes(child, request);
			}
		}
	}
	
	private void populate(CommandBean commandBean, Map<String, CommandBeanTreeNode> nodes, HttpServletRequest request) throws Exception {
		String targetNode = request.getParameter("submitForServerSideProcess_targetNode");
		
		for (CommandBeanTreeNode node: nodes.values()) {
			if (node.getInfo().getNestedPropertyNameForField("bbLocation").equals(targetNode) ) {		
				CommandBeanTreeNode parentNode = node.getParent();				
				InvBbMovementLog bbMovementLog = (InvBbMovementLog) parentNode.getData();
				
				if (!"CLIENT".equalsIgnoreCase(bbMovementLog.getBbLocation())) {
					bbMovementLog.setDestinationGroupUid(null);
					bbMovementLog.setDestinationRigInformationUid(null);
				}
			}
		}
			
	}
}
