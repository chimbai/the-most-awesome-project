package com.idsdatanet.d2.idsinvoice;

import java.io.File;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.model.ConsultantMaster;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.ReportOutputFile;

public class ConsultantReportOutputFile extends ReportOutputFile {
	
	private String idsinvoiceid = null;
	private String lastEditUserUid = null;
	private String reportDisplayName = null;

	public ConsultantReportOutputFile(ReportFiles reportFile, File outputFile) throws Exception {
		super(reportFile, null, null, null, outputFile);
		
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		extraProperties.readPropertiesFromXmlString(reportFile.getExtraProperties());
		String consultantMasterUid = extraProperties.getProperty("consultantMasterUid");
		if (StringUtils.isNotBlank(consultantMasterUid)) {
			ConsultantMaster thisConsultantMaster = (ConsultantMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ConsultantMaster.class, consultantMasterUid);
			if (thisConsultantMaster != null) {
				this.idsinvoiceid = thisConsultantMaster.getIdsinvoiceid();
				this.lastEditUserUid = thisConsultantMaster.getLastEditUserUid();
				this.reportDisplayName = thisConsultantMaster.getGroupUid() + " " + thisConsultantMaster.getIdsinvoiceid();
			}
		}
	}

	public String getIdsinvoiceid() {
		return idsinvoiceid;
	}
	
	public String getReportDisplayName() {
		return reportDisplayName;
	}

	public String getLastEditUserUid() {
		return lastEditUserUid;
	}

}
