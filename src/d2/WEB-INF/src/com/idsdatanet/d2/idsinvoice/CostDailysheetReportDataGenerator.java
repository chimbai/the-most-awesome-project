package com.idsdatanet.d2.idsinvoice;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class CostDailysheetReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		
		String groupUid = userContext.getUserSelection().getCustomProperty("clientGroupUid").toString();
		
		
		String strSql = "Select dailyUid from CostDailysheet where groupUid =:groupUid and (isDeleted = false or isDeleted is null) group by dailyUid";
		List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"groupUid"}, new Object[] {groupUid});
	
		if (lstResult.size() > 0){
			for(String thisDailyUid : lstResult)
			{
				ReportDataNode thisReportNode = reportDataNode.addChild("CostDailysheet");
				if (StringUtils.isNotBlank(thisDailyUid)) thisReportNode.addProperty("dailyUid", thisDailyUid);
			}
		}
		
	}
		
}
