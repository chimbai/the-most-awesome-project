package com.idsdatanet.d2.idsinvoice;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ConsultantMaster;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class TimesheetCommandBeanListener extends EmptyCommandBeanListener {
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		if (StringUtils.isNotBlank(clientGroupUid)) {
			String strIdsinvoiceid = (String) commandBean.getRoot().getDynaAttr().get("idsinvoiceid");
			if (StringUtils.isNotBlank(strIdsinvoiceid)) {
				String strSql = "FROM ConsultantMaster WHERE groupUid = :groupUid AND idsinvoiceid = :idsinvoiceid AND (isDeleted IS NULL OR isDeleted = '')";
				String[] paramsFields = {"groupUid","idsinvoiceid"};
				Object[] paramsValues = {clientGroupUid,strIdsinvoiceid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (lstResult.size() > 0){
					Object obj = (Object) lstResult.get(0);
					ConsultantMaster thisConsultantMaster = (ConsultantMaster) obj;
					if (thisConsultantMaster != null) {
						if (!clientGroupUid.equals(thisConsultantMaster.getGroupUid())) {
							commandBean.getRoot().resetPersistedDynamicAttribute("idsinvoiceid");
						}
					}
				}
				else
				{
					commandBean.getRoot().resetPersistedDynamicAttribute("idsinvoiceid");
				}
			}
		} else {
			commandBean.getRoot().resetPersistedDynamicAttribute("idsinvoiceid");
		}
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		String previousIdsinvoiceid = (String) commandBean.getRoot().getDynaAttr().get("idsinvoiceid");
		
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		if (commandBean.getFlexClientAdaptor()!=null) commandBean.getFlexClientAdaptor().setReloadParentHtmlPage();
		
		if (StringUtils.isNotBlank(previousIdsinvoiceid)) {
			String strSql = "FROM ConsultantMaster WHERE groupUid = :groupUid AND idsinvoiceid = :idsinvoiceid AND (isDeleted IS NULL OR isDeleted = '')";
			String[] paramsFields = {"groupUid","idsinvoiceid"};
			Object[] paramsValues = {clientGroupUid,previousIdsinvoiceid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0){
				commandBean.getRoot().getDynaAttr().put("idsinvoiceid", previousIdsinvoiceid);
			}
		}
	}
	
}
