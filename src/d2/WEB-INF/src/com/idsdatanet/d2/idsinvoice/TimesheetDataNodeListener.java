package com.idsdatanet.d2.idsinvoice;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.ConsultantMaster;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class TimesheetDataNodeListener extends EmptyDataNodeListener {
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof ConsultantMaster) {
			ConsultantMaster thisConsultantMaster = (ConsultantMaster) object;
			thisConsultantMaster.setGroupUid(thisConsultantMaster.getInvoiceTo());
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof ConsultantMaster) {
			ConsultantMaster thisConsultantMaster = (ConsultantMaster) object;
			if(commandBean.getRoot().getDynaAttr().get("clientGroupUid") != null)
			{
				thisConsultantMaster.setInvoiceTo(commandBean.getRoot().getDynaAttr().get("clientGroupUid").toString());
			}
		}
	}
}
