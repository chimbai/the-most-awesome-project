package com.idsdatanet.d2.idsinvoice.stt;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.model.CostDailysheet;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class InvSTTDataLoaderInterceptor implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		String strSql = "SELECT MAX(transactionDatetime) FROM SysLogSendToTown where jobconfigs = 'idsInvoiceSTTJobConfig' and status = 'OK'";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		String thisFilter = "";
		Date date = new Date();
		//String strInCondition = "";

		if (lstResult.size()>0) {
			if (lstResult.get(0) != null){
				date = (Date)lstResult.get(0);
			}else {
				date.setTime(0);	
			}
			date = DateUtils.truncate(date,Calendar.DAY_OF_MONTH);
			
		}else {
			date.setTime(0);
			date = DateUtils.truncate(date,Calendar.DAY_OF_MONTH);
		}
		
		if (date !=null){
			
			if (meta.getTableClass().equals(ReportDaily.class) || 
					meta.getTableClass().equals(Daily.class) || 
					meta.getTableClass().equals(Well.class) || 
					meta.getTableClass().equals(Wellbore.class) || 
					meta.getTableClass().equals(Operation.class) || 
					meta.getTableClass().equals(RigInformation.class) || 
					meta.getTableClass().equals(UnwantedEvent.class) || 
					meta.getTableClass().equals(LessonTicket.class) || 
					meta.getTableClass().equals(CostDailysheet.class)) {
				query.addParam("lastSTTDatetime", date);
				thisFilter = "lastEditDatetime >:lastSTTDatetime";
			}
		}
		
		if (StringUtils.isNotBlank(thisFilter)) {
			return conditionClause.replace("{_custom_condition_}", thisFilter);
		}else {
			return conditionClause.replace("{_custom_condition_}", "1=0"); //do not want to send any record
		}

	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}
