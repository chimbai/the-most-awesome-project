package com.idsdatanet.d2.idsinvoice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.Group;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class InvClientSummaryDataNodeListener extends EmptyDataNodeListener{
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object obj = node.getData();
		
		if (obj instanceof Group) {
			Group clientGroup = (Group) obj;
			String[] paramsFields = {"ClientGroupUid"};
			Object[] paramsValues = {clientGroup.getGroupUid()};
			
			String strSql = "SELECT count(*) as totalDays from ReportDaily WHERE (isDeleted = false or isDeleted is null) and groupUid =:ClientGroupUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			Integer totalDays = 0;
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				
				if (thisResult != null) totalDays = Integer.parseInt(thisResult.toString());
				node.getDynaAttr().put("totalDays", totalDays);
			}
			
			String strSql1 = "SELECT count(*) as totalUnbilledDays from ReportDaily WHERE (isDeleted = false or isDeleted is null) and (idsinvoiceid is null or idsinvoiceid = '') and groupUid =:ClientGroupUid";
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);
			Integer totalUnbilledDays = 0;
			if (!lstResult1.isEmpty())
			{
				Object thisResult = (Object) lstResult1.get(0);
				
				if (thisResult != null) totalUnbilledDays = Integer.parseInt(thisResult.toString());
				node.getDynaAttr().put("totalUnbilledDays", totalUnbilledDays);
			}
			
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf = new SimpleDateFormat("MM");
			int month = Integer.parseInt((String) sdf.format(now));
			
			sdf = new SimpleDateFormat("yyyy");
			int year = Integer.parseInt((String) sdf.format(now));
			
			
			Calendar calendar = Calendar.getInstance();
			
			calendar.set(year, month - 2, 1, 0,0,0);
			Date d1 = calendar.getTime();
			
			calendar.set(year, month - 1, 1, 0, 0, 0);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			Date d2 = calendar.getTime();
			
			sdf = new SimpleDateFormat("MMMM yyyy");
			commandBean.getRoot().getDynaAttr().put("usageMonth", (String) sdf.format(d1));
		
			String strSql2 = "SELECT count(*) as currentMonthTotal from ReportDaily WHERE (isDeleted = false or isDeleted is null) and (reportDatetime >= :start_date and reportDatetime <= :end_date) and groupUid =:ClientGroupUid";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"start_date", "end_date", "ClientGroupUid"}, new Object[] {d1, d2, clientGroup.getGroupUid()});
			Integer currentMonthTotal = 0;
			if (!lstResult2.isEmpty())
			{
				Object thisResult = (Object) lstResult2.get(0);
				
				if (thisResult != null) currentMonthTotal = Integer.parseInt(thisResult.toString());
				node.getDynaAttr().put("currentMonthTotal", currentMonthTotal);
			}
			
			String strSql3 = "SELECT count(*) as currentMonthUnbilled from ReportDaily WHERE (isDeleted = false or isDeleted is null) and (idsinvoiceid is null or idsinvoiceid = '') and (reportDatetime >= :start_date and reportDatetime <= :end_date) and groupUid =:ClientGroupUid";
			List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, new String[] {"start_date", "end_date", "ClientGroupUid"}, new Object[] {d1, d2, clientGroup.getGroupUid()});
			Integer currentMonthUnbilled = 0;
			if (!lstResult3.isEmpty())
			{
				Object thisResult = (Object) lstResult3.get(0);
				
				if (thisResult != null) currentMonthUnbilled = Integer.parseInt(thisResult.toString());
				node.getDynaAttr().put("currentMonthUnbilled", currentMonthUnbilled);
			}
			
			String strSql4 = "SELECT count(*) as currentMonthBilled from ReportDaily WHERE (isDeleted = false or isDeleted is null) and (idsinvoiceid is not null or idsinvoiceid != '') and (reportDatetime >= :start_date and reportDatetime <= :end_date) and groupUid =:ClientGroupUid";
			List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, new String[] {"start_date", "end_date", "ClientGroupUid"}, new Object[] {d1, d2, clientGroup.getGroupUid()});
			Integer currentMonthBilled = 0;
			if (!lstResult4.isEmpty())
			{
				Object thisResult = (Object) lstResult4.get(0);
				
				if (thisResult != null) currentMonthBilled = Integer.parseInt(thisResult.toString());
				node.getDynaAttr().put("currentMonthBilled", currentMonthBilled);
			}
			
			
			String sql = "SELECT d.dayDate from Daily d, LessonTicket l where l.groupUid =:ClientGroupUid and " +
						"(d.isDeleted = false or d.isDeleted is null) and (l.isDeleted = false or l.isDeleted is null) and " +
						"l.dailyUid=d.dailyUid order by d.dayDate ASC" ;
			List lstLessonTicket = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "ClientGroupUid", clientGroup.getGroupUid());
			
			if (!lstLessonTicket.isEmpty()){
				if (lstLessonTicket.get(0).toString() !=null) {
					SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
					node.getDynaAttr().put("lessonTicket", "Lesson Ticket : Yes (Start Date: " + df.format(lstLessonTicket.get(0)) + ")");
				}
			}
			
			String sql2 = "SELECT eventDatetime from UnwantedEvent where groupUid =:ClientGroupUid order by eventDatetime ASC" ;
			
			List lstuer = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "ClientGroupUid", clientGroup.getGroupUid());
			
			if (!lstuer.isEmpty()){
				if (lstuer.get(0).toString() !=null) {
					SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
					node.getDynaAttr().put("unwantedEvent", "Unwanted Event : Yes (Start Date: " + df.format(lstuer.get(0)) + ")");
				}
			}

			
		}
	}

}
