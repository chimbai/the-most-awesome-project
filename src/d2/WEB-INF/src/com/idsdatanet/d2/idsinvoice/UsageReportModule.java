package com.idsdatanet.d2.idsinvoice;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.InvReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;

public class UsageReportModule extends DefaultReportModule {
	
	private Log logger = LogFactory.getLog(this.getClass());
	
	@Override
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		userSelection.setCustomProperty("clientGroupUid", commandBean.getRoot().getDynaAttr().get("clientGroupUid"));
		commandBean.getRoot().resetPersistedDynamicAttribute("multiSelectReportOptionChooserSelectedItems");
	}
	
	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexMultiSelectReportOptionChooserLoadItems".equals(invocationKey)) {
			String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
			if (StringUtils.isNotBlank(clientGroupUid)) {
				List<InvReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from InvReportDaily where groupUid = :groupUid and (isDeleted = false or isDeleted is null)", "groupUid", clientGroupUid);
				
				SimpleDateFormat flexDateFormater = new SimpleDateFormat("dd MMM yyyy");
				
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				for (InvReportDaily invReportDaily : list) {
					writer.startElement("Item");
					writer.addElement("data", invReportDaily.getInvReportDailyUid());
					writer.addElement("label", invReportDaily.getInvoiceNumber() + " (" + flexDateFormater.format(invReportDaily.getInvoiceDatetime()) + ")");
					writer.endElement();
				}
				writer.endElement();
				writer.close();
			}
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	
	Set getSelectedItems(CommandBean commandBean) throws Exception {
		String multiSelectReportOptionChooserSelectedItems = (String) commandBean.getRoot().getDynaAttr().get("multiSelectReportOptionChooserSelectedItems");
		if (StringUtils.isNotBlank(multiSelectReportOptionChooserSelectedItems)) {
			String[] selectedItems = multiSelectReportOptionChooserSelectedItems.split(",");
			Set<String> selectedItemsSet = new HashSet<String>();
			for (String selectedItem : selectedItems) {
				selectedItemsSet.add(selectedItem.trim());
			}
			return selectedItemsSet;
		}
		return null;
	}
	
	@Override
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		Set selectedItems = this.getSelectedItems(commandBean);
		if (clientGroupUid != null && selectedItems != null && !selectedItems.isEmpty()) {
			List<UserSelectionSnapshot> userSelections = new ArrayList<UserSelectionSnapshot>();
			for (Object item : selectedItems) {
				UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
				userSelection.setCustomProperty("clientGroupUid", clientGroupUid);
				userSelection.setCustomProperty("invReportDailyUid", item);
				userSelections.add(userSelection);
			}
			return this.submitReportJob(userSelections, this.getParametersForReportSubmission(session, request, commandBean));
		}else{
			return null;
		}
	}
	
	@Override
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception {
		InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, (String) userContext.getUserSelection().getCustomProperty("invReportDailyUid"));
		return invReportDaily.getInvoiceNumber() + " " + invReportDaily.getGroupUid() + "." + this.getOutputFileExtension();
	}
	
	@Override
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception {
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		UserContext userContext = jobContext.getUserContext();
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		report.setGroupUid((String) userSelection.getCustomProperty("clientGroupUid"));
		extraProperties.setProperty("invReportDailyUid", (String) userSelection.getCustomProperty("invReportDailyUid"));
		report.setExtraProperties(extraProperties.serializePropertiesToXmlString());
	}
	
	@Override
	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception { 
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where reportType = :reportType and groupUid = :groupUid and (isDeleted = false or isDeleted is null) order by dateGenerated desc", new String[] {"reportType", "groupUid" }, new Object[] { reportType, userSelection.getCustomProperty("clientGroupUid") });
	}
	
	@Override
	protected List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, String reportType, HttpServletRequest request) throws Exception{
		List<ReportFiles> db_files = this.loadSourceReportFilesFromDB(userSelection, reportType);
		List<Object> output_files = new ArrayList<Object>();
		
		for(ReportFiles db_file: db_files){
			if(StringUtils.isNotBlank(db_file.getReportFile())){
				File outputFile = this.getReportOutputFile(db_file);
				if(outputFile.exists()){
					try{
						UsageReportOutputFile output_file = new UsageReportOutputFile(db_file, outputFile);
						if(output_file != null) output_files.add(output_file);
					}catch(Exception e){
						this.logger.error("Error encountered when creating ReportOutputFile: " + e.getMessage(), e);
					}
				}else{
					this.logger.error("Physical output file not found. [ReportFilesUid: " + db_file.getReportFilesUid() + ", Output file: " + null2EmptyString(db_file.getReportFile()) + "]");
				}
			}
		}
		
		Comparator comparator = new ReportOutputFilesComparator();
		if(comparator != null) Collections.sort(output_files, comparator);
	
		return output_files;
	}
	
	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if(meta.getTableClass().equals(UsageReportOutputFile.class)){
			return this.loadReportOutputFiles(userSelection, request);
		}else{
			return null;
		}
	}
	
	private class ReportOutputFilesComparator implements Comparator<Object> {
		public int compare(Object o2, Object o1) {
			UsageReportOutputFile f1 = (UsageReportOutputFile) o1;
			UsageReportOutputFile f2 = (UsageReportOutputFile) o2;
			
			if(f1.getInvoiceNumber() != null && f2.getInvoiceNumber() != null){
				return f1.getInvoiceNumber().compareTo(f2.getInvoiceNumber());
			}else if(f1.getInvoiceNumber() != null){
				return 1;
			}else if(f2.getInvoiceNumber() != null){
				return -1;
			}else{
				return 0;
			}
		}
	}

}
