package com.idsdatanet.d2.idsinvoice;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class InvReportDailyDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		String invReportDailyUid = (String) commandBean.getRoot().getDynaAttr().get("invReportDailyUid");
		if (StringUtils.isNotBlank(invReportDailyUid)) {
			node.getAtts().setSelected(true);
		}
		
		Object obj = (Object) node.getData();
		
		if (obj instanceof ReportDaily){
			ReportDaily reportdaily = (ReportDaily) obj;
			
			Date reportdate = (Date) reportdaily.getReportDatetime();
			
			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf = new SimpleDateFormat("MMMM yyyy");
			String displayMonth = StringUtils.upperCase((String) sdf.format(reportdate));
			node.getDynaAttr().put("displayMonth", displayMonth);
			
			String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), reportdaily.getOperationUid());

			if (StringUtils.isNotBlank(completeOperationName)){
				node.getDynaAttr().put("completeOperationName", completeOperationName);
			}
			
			if (StringUtils.isNotBlank(reportdaily.getReportPeriodSummary())){
				if (reportdaily.getReportPeriodSummary().length() > 100) {
					String reportPeriodSummary = StringUtils.substring(reportdaily.getReportPeriodSummary(), 0, 100) + " ...";
					node.getDynaAttr().put("reportPeriodSummary", reportPeriodSummary);
				}else {
					node.getDynaAttr().put("reportPeriodSummary", reportdaily.getReportPeriodSummary());
				}
			}
			
			if (StringUtils.isNotBlank(reportdaily.getDailyEndSummary())){
				if (reportdaily.getDailyEndSummary().length() > 100) {
					String dailyEndSummary = StringUtils.substring(reportdaily.getDailyEndSummary(), 0, 100) + " ...";
					node.getDynaAttr().put("dailyEndSummary", dailyEndSummary);
				}else {
					node.getDynaAttr().put("dailyEndSummary", reportdaily.getDailyEndSummary());
				}
			}
			
			if (StringUtils.isNotBlank(reportdaily.getReportCurrentStatus())){
				if (reportdaily.getReportCurrentStatus().length() > 100) {
					String reportCurrentStatus = StringUtils.substring(reportdaily.getReportCurrentStatus(), 0, 100) + " ...";
					node.getDynaAttr().put("reportCurrentStatus", reportCurrentStatus);
				}else {
					node.getDynaAttr().put("reportCurrentStatus", reportdaily.getReportCurrentStatus());
				}
			}
			
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, reportdaily.getOperationUid());
			if (operation != null){
				node.getDynaAttr().put("operationType", operation.getOperationCode());
			}
		}

	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
	}

}
