package com.idsdatanet.d2.idsinvoice;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ConsultantMaster;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;

public class ConsultantReportModule extends UsageReportModule {
	
	private Log logger = LogFactory.getLog(this.getClass());
	
	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexMultiSelectReportOptionChooserLoadItems".equals(invocationKey)) {
			String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
			if (StringUtils.isNotBlank(clientGroupUid)) {
				List<ConsultantMaster> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ConsultantMaster WHERE groupUid = :groupUid AND (isDeleted = FALSE OR isDeleted IS NULL)", "groupUid", clientGroupUid);
				
				SimpleDateFormat flexDateFormater = new SimpleDateFormat("dd MMM yyyy");
				
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				for (ConsultantMaster thisConsultantMaster : list) {
					writer.startElement("Item");
					writer.addElement("data", thisConsultantMaster.getConsultantMasterUid());
					writer.addElement("label", thisConsultantMaster.getIdsinvoiceid() + " (" + flexDateFormater.format(thisConsultantMaster.getPeriodStartDateTime()) + ")");
					writer.endElement();
				}
				writer.endElement();
				writer.close();
			}
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
		
	@Override
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		Set selectedItems = getSelectedItems(commandBean);
		if (clientGroupUid != null && selectedItems != null && !selectedItems.isEmpty()) {
			List<UserSelectionSnapshot> userSelections = new ArrayList<UserSelectionSnapshot>();
			for (Object item : selectedItems) {
				UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
				userSelection.setCustomProperty("clientGroupUid", clientGroupUid);
				userSelection.setCustomProperty("consultantMasterUid", item);
				userSelections.add(userSelection);
			}
			return this.submitReportJob(userSelections, this.getParametersForReportSubmission(session, request, commandBean));
		}else{
			return null;
		}
	}
	
	@Override
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception {
		ConsultantMaster thisConsultantMaster = (ConsultantMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ConsultantMaster.class, (String) userContext.getUserSelection().getCustomProperty("consultantMasterUid"));
		return thisConsultantMaster.getGroupUid() + "_" + thisConsultantMaster.getIdsinvoiceid() + "." + this.getOutputFileExtension();
	}
	
	@Override
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception {
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		UserContext userContext = jobContext.getUserContext();
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		report.setGroupUid((String) userSelection.getCustomProperty("clientGroupUid"));
		extraProperties.setProperty("consultantMasterUid", (String) userSelection.getCustomProperty("consultantMasterUid"));
		report.setExtraProperties(extraProperties.serializePropertiesToXmlString());
	}
	
	/*@Override
	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception { 
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where reportType = :reportType and groupUid = :groupUid and (isDeleted = false or isDeleted is null)", new String[] {"reportType", "groupUid" }, new Object[] { reportType, userSelection.getCustomProperty("clientGroupUid") });
	}*/
	
	@Override
	protected List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, String reportType, HttpServletRequest request) throws Exception{
		List<ReportFiles> db_files = this.loadSourceReportFilesFromDB(userSelection, reportType);
		List<Object> output_files = new ArrayList<Object>();
		
		for(ReportFiles db_file: db_files){
			if(StringUtils.isNotBlank(db_file.getReportFile())){
				File outputFile = this.getReportOutputFile(db_file);
				if(outputFile.exists()){
					try{
						ConsultantReportOutputFile output_file = new ConsultantReportOutputFile(db_file, outputFile);
						if(output_file != null) output_files.add(output_file);
					}catch(Exception e){
						this.logger.error("Error encountered when creating ReportOutputFile: " + e.getMessage(), e);
					}
				}else{
					this.logger.error("Physical output file not found. [ReportFilesUid: " + db_file.getReportFilesUid() + ", Output file: " + null2EmptyString(db_file.getReportFile()) + "]");
				}
			}
		}
		
		Comparator comparator = new ReportOutputFilesComparator();
		if(comparator != null) Collections.sort(output_files, comparator);
	
		return output_files;
	}
	
	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if(meta.getTableClass().equals(ConsultantReportOutputFile.class)){
			return this.loadReportOutputFiles(userSelection, request);
		}else{
			return null;
		}
	}
	
	private class ReportOutputFilesComparator implements Comparator<Object> {
		public int compare(Object o2, Object o1) {
			ConsultantReportOutputFile f1 = (ConsultantReportOutputFile) o1;
			ConsultantReportOutputFile f2 = (ConsultantReportOutputFile) o2;
			
			if(f1.getIdsinvoiceid() != null && f2.getIdsinvoiceid() != null){
				return f1.getIdsinvoiceid().compareTo(f2.getIdsinvoiceid());
			}else if(f1.getIdsinvoiceid() != null){
				return 1;
			}else if(f2.getIdsinvoiceid() != null){
				return -1;
			}else{
				return 0;
			}
		}
	}

}
