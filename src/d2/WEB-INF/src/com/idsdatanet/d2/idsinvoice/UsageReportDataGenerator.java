package com.idsdatanet.d2.idsinvoice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.InvReportDaily;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class UsageReportDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		
		ReportDataNode thisReportNode = reportDataNode.addChild("UsageReport");
		
		String currentUser = userContext.getUserSelection().getUserUid();
		User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, currentUser);
		
		if (StringUtils.isNotBlank(user.getFname())) {
			thisReportNode.addProperty("generatedBy",user.getFname());
		}
		
		String invReportDailyUid = userContext.getUserSelection().getCustomProperty("invReportDailyUid").toString();
		thisReportNode.addProperty("idsinvoiceid", invReportDailyUid);
		
		//InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
		Date invoicedate = null;
		
		String strSql = "Select reportDatetime from ReportDaily where idsinvoiceid =:invoiceNumber and (isDeleted = false or isDeleted is null) order by reportDatetime";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"invoiceNumber"}, new Object[] {invReportDailyUid});
	
		if (lstResult.size() > 0){
			Object objResult = (Object) lstResult.get(0);
			
			if (objResult !=null){
				invoicedate = (Date) objResult;
			}
		}else
		{
			InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
		}
		
		
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf = new SimpleDateFormat("MM");
		int month = Integer.parseInt((String) sdf.format(invoicedate));
		
		sdf = new SimpleDateFormat("yyyy");
		int year = Integer.parseInt((String) sdf.format(invoicedate));
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, 1, 0,0,0);
		Date d1 = calendar.getTime();
		
		calendar.set(year, month, 1, 0, 0, 0);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date d2 = calendar.getTime();
		
		sdf = new SimpleDateFormat("dd MMM yyyy");
		thisReportNode.addProperty("invoicePeriod", "("+ sdf.format(d1) + " - " + sdf.format(d2) + ")");
		
		
		
		/*String strSql = "Select rigInformationUid, operationUid from ReportDaily where reportType='DDR' and idsinvoiceid =:invoiceNumber and (isDeleted = false or isDeleted is null) group by rigInformationUid, operationUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "invoiceNumber", invReportDailyUid);
		
	
		if (lstResult.size() > 0){
			for (Object objResult: lstResult) {
				Object[] objResult2 = (Object[]) objResult;
				ReportDataNode thisRigReportNode = reportDataNode.addChild("usageMonthByRig");
				if (objResult2[0] !=null){					
					RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, objResult2[0].toString());
					thisRigReportNode.addProperty("rigInformationUid", rig.getRigInformationUid());
					thisRigReportNode.addProperty("rigInformationName", rig.getRigName());
				}
				thisRigReportNode.addProperty("operationUid",objResult2[1].toString());
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, objResult2[1].toString());
				thisRigReportNode.addProperty("operationName", operation.getOperationName());

				String strSql2 = "Select reportDatetime from ReportDaily where rigInformationUid=:rigid and operationUid =:operationid and idsinvoiceid =:invoiceNumber order by reportDatetime";
				List <Date>lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,  new String[] {"rigid", "operationid", "invoiceNumber"}, new Object[] {objResult2[0].toString(), objResult2[1].toString(), invReportDailyUid});
				if (lstResult2.size() > 0){
					for (Date objResult3: lstResult2) {
						ReportDataNode reportDateReportNode = thisRigReportNode.addChild("usageMonthByReport");
						
						SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
						
						reportDateReportNode.addProperty("reportDatetime", sdf.format(objResult3));
					}
				}
				
				
				String strSql3 = "Select reportDatetime from ReportDaily where rigInformationUid=:rigid and operationUid =:operationid and idsinvoiceid =:invoiceNumber group by MONTH(reportDatetime)";
				List <Date>lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,  new String[] {"rigid", "operationid", "invoiceNumber"}, new Object[] {objResult2[0].toString(), objResult2[1].toString(), invReportDailyUid});
				if (lstResult3.size() > 0){
					for (Date objResult4: lstResult3) {
	
						SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
						thisRigReportNode.addProperty("monthYear", sdf.format(objResult4));
					}
				}
						
						
				
			}
		}
		*/
		
	}
}
