package com.idsdatanet.d2.idsinvoice;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.InvReportDaily;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class InvReportDailyChangeReferenceDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted = false or isDeleted is null) ";
		String clientGroupUid = (String) commandBean.getRoot().getDynaAttr().get("clientGroupUid");
		if (StringUtils.isNotBlank(clientGroupUid)) {
			if (meta.getTableClass().equals(InvReportDaily.class)) {
				customCondition += " and groupUid = :groupUid ";
				query.addParam("groupUid",clientGroupUid);
			}
		} else {
			customCondition = "1=0"; //show blank if clientgroup not selected
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
}
