package com.idsdatanet.d2.idsinvoice;

import java.io.File;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.model.InvReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.ReportOutputFile;

public class UsageReportOutputFile extends ReportOutputFile {
	
	private String invoiceNumber = null;
	private Date invoiceDatetime = null;
	private String invReportDailyLastEditUserUid = null;
	private String reportDisplayName = null;

	public UsageReportOutputFile(ReportFiles reportFile, File outputFile) throws Exception {
		super(reportFile, null, null, null, outputFile);
		
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		extraProperties.readPropertiesFromXmlString(reportFile.getExtraProperties());
		String invReportDailyUid = extraProperties.getProperty("invReportDailyUid");
		if (StringUtils.isNotBlank(invReportDailyUid)) {
			InvReportDaily invReportDaily = (InvReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InvReportDaily.class, invReportDailyUid);
			if (invReportDaily != null) {
				this.invoiceNumber = invReportDaily.getInvoiceNumber();
				this.invoiceDatetime = invReportDaily.getInvoiceDatetime();
				this.invReportDailyLastEditUserUid = invReportDaily.getLastEditUserUid();
				this.reportDisplayName = invReportDaily.getInvoiceNumber();
			}
		}
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public Date getInvoiceDatetime() {
		return invoiceDatetime;
	}
	
	public String getReportDisplayName() {
		return reportDisplayName;
	}

	public String getInvReportDailyLastEditUserUid() {
		return invReportDailyLastEditUserUid;
	}

}
