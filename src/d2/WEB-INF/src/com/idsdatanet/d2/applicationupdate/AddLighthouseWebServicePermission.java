package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.AclEntry;
import com.idsdatanet.d2.core.model.AclGroup;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.UserAclGroup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.infra.system.security.DefaultPasswordEncoder;

public class AddLighthouseWebServicePermission extends ApplicationUpdateConfig {

	@Override
	public void run() throws Exception {
		AclGroup clientAdminGroup = null;
		// Lighthouse ACL
		String sql = "from AclGroup where (isDeleted = false or isDeleted is null)";
		List<AclGroup> groups = (List<AclGroup>)ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if (groups != null && groups.size() > 0) {
			for (AclGroup group : groups) {
				if (StringUtils.equals("Client Administrator Group", group.getName())) {
					clientAdminGroup = group;
				}
				String entrySql = "from AclEntry where (isDeleted = false or isDeleted is null) and aclGroupUid=:aclGroupUid and beanName='webService.lighthouseController'";
				List<AclEntry> entries = (List<AclEntry>)ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(entrySql, "aclGroupUid", group.getAclGroupUid());
				if (entries != null && entries.size() == 0) {
					AclEntry lighthouseEntry = new AclEntry();
					lighthouseEntry.setAclGroupUid(group.getAclGroupUid());
					lighthouseEntry.setBeanName("webService.lighthouseController");
					lighthouseEntry.setPermissions(2);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lighthouseEntry);
				}
			}
		}
		// Lighthouse User
		sql = "from User where (isDeleted = false or isDeleted is null) and userName='lighthouse_admin'";
		List<User> users = (List<User>)ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if (users != null && users.size() == 0) {
			User lighthouseUser = new User();
			lighthouseUser.setUserName("lighthouse_admin");
			lighthouseUser.setPassword(DefaultPasswordEncoder.encodePassword("nrW=LHR78!"));
			lighthouseUser.setGroupUid(ApplicationUtils.getConfiguredInstance().getIdsadminGroupUid());
	       	lighthouseUser.setFname("Lighthouse Admin");
	       	lighthouseUser.setIsAdmin(true);
	       	lighthouseUser.setIdsUser(true);
	        ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lighthouseUser);
	        if (clientAdminGroup != null) {
	        	UserAclGroup userAcl = new UserAclGroup();
	        	userAcl.setAclGroupUid(clientAdminGroup.getAclGroupUid());
	        	userAcl.setUserUid(lighthouseUser.getUserUid());
	        	userAcl.setAclGroupUid(ApplicationUtils.getConfiguredInstance().getIdsadminGroupUid());
	        	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(userAcl);
	        }
		}
	}
}
