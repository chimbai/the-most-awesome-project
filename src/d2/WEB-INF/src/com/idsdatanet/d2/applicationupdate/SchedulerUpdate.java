package com.idsdatanet.d2.applicationupdate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.util.MethodInvoker;

import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.ScheduledReportGenerationJobParams;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport.DailyReportService;
import com.idsdatanet.d2.safenet.lessonTicketReach.schedule.LessonTicketReachEmailSchedule;

public class SchedulerUpdate extends ApplicationObjectSupport implements ApplicationUpdate {
	private String schedulerBeanId = null;
	private Scheduler scheduler;
	public String getSchedulerBeanId() {
		return schedulerBeanId;
	}

	public void setSchedulerBeanId(String schedulerBeanId) {
		this.schedulerBeanId = schedulerBeanId;
	}
	
	public void run() throws Exception {
		this.scheduler = (Scheduler)this.getApplicationContext().getBean(this.schedulerBeanId);
		int recordcount = 0;
		int recordadded = 0;
		for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.anyJobGroup())) {
			Boolean adding = true;
			String errorMsg = "";
			recordcount ++;
			JobDetail jobDetails = scheduler.getJobDetail(jobKey);
			JobDataMap jobDataMap = (JobDataMap) jobDetails.getJobDataMap();
			String targetObject = null;
			String targetMethod = null;
			Object targetObj = null;
			String serviceKey = null;
			String emailListKey = "";
			String emailsubject = "";
			if (jobDataMap.get("methodInvoker")!=null){
				MethodInvoker methodInvoker = (MethodInvoker) jobDataMap.get("methodInvoker");
				targetObject = methodInvoker.getTargetObject().getClass().getSimpleName();
				targetMethod = methodInvoker.getTargetMethod();
				targetObj = methodInvoker.getTargetObject();
				serviceKey = this.getServicekey(targetObj,targetMethod);
			}else{
				ScheduledReportGenerationJobParams scheduledReportGenerationJobParams = (ScheduledReportGenerationJobParams) jobDataMap.get("com.idsdatanet.d2.drillnet.report.ScheduledReportGenerationJobParams");
				
				if (scheduledReportGenerationJobParams.reportType==1){
					targetObject = "ScheduledManagementReport";
					serviceKey = "MGT";
				}else{
					String reportType = scheduledReportGenerationJobParams.defaultReportType;
					targetObject = "DefaultReport";
					serviceKey = this.getDefaultReportServicekey(reportType);
				}
					
				if (StringUtils.isNotEmpty(scheduledReportGenerationJobParams.reportAutoEmail.getEmailListKey()))
					emailListKey = scheduledReportGenerationJobParams.reportAutoEmail.getEmailListKey();
				if (StringUtils.isNotEmpty(scheduledReportGenerationJobParams.reportAutoEmail.getEmailSubject()))
					emailsubject = scheduledReportGenerationJobParams.reportAutoEmail.getEmailSubject();
			}
			System.out.println("[targetObject] : " +targetObject);
			System.out.println("[targetMethod] : " +targetMethod);
			if ("Not Found".equalsIgnoreCase(serviceKey)){
				adding = false;
				errorMsg = "Service Key not Found.";
			}
		    SchedulerJobDetail schedulerJobDetail = new SchedulerJobDetail();
		    schedulerJobDetail.setServiceKey(serviceKey);
		    schedulerJobDetail.setIsActive(false);
		    if (!emailListKey.isEmpty()) schedulerJobDetail.setEmailListKey(emailListKey);
		    if (!emailsubject.isEmpty()) schedulerJobDetail.setEmailSubject(emailsubject);
		    System.out.println("[serviceKey] : " +serviceKey);
		    List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
		    if (!triggers.isEmpty() && triggers.get(0) instanceof CronTrigger) {
		    	CronTrigger cronTriggerBean = (CronTrigger) triggers.get(0);
		    	schedulerJobDetail.setCronExpression(cronTriggerBean.getCronExpression());
		    	System.out.println("[CronExpression] : " +cronTriggerBean.getCronExpression());
		    	schedulerJobDetail.setCronExpression(cronTriggerBean.getCronExpression());
		    	schedulerJobDetail = this.schedulerJobDetailUpdate(schedulerJobDetail);
		    }
		    
		    if (!triggers.isEmpty() && triggers.get(0) instanceof SimpleTrigger) {
		    	SimpleTrigger simpleTriggerBean = (SimpleTrigger) triggers.get(0);
		    	System.out.println("[RepeatInterval] : " +simpleTriggerBean.getRepeatInterval());
		    	long repeatInterval = simpleTriggerBean.getRepeatInterval();
		    	String cronExpression = "0 m h * * ? ";
		    	int[] times = this.splitToComponentTimes(repeatInterval);
		    	if (times[0]==0){
		    		int mins = times[1];
		    		cronExpression = cronExpression.replace("h", "*");
		    		cronExpression = cronExpression.replace("m", "0/"+mins);
		    		schedulerJobDetail.setEveryValue(mins);
		    		schedulerJobDetail.setServiceFrequency("every_min");
		    		schedulerJobDetail.setCronExpression(cronExpression);
		    	}else{
		    		int hours = times[0];
		    		cronExpression = cronExpression.replace("h", "0/"+hours);
		    		cronExpression = cronExpression.replace("m", "0");
		    		schedulerJobDetail.setEveryValue(hours);
		    		schedulerJobDetail.setServiceFrequency("every_hour");
		    		schedulerJobDetail.setCronExpression(cronExpression);
		    	}
		    }
		    
		    if (adding) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(schedulerJobDetail);
		    if (adding) recordadded++;
		    if (StringUtils.isNotEmpty(errorMsg)) System.out.println("[Error] " + errorMsg);		
		    System.out.println("----------------");		
			
		}
		 System.out.println("---Summary---");
		 System.out.println(recordcount + " schedulers Found~");
		 System.out.println(recordadded + " records added to common schedulers~");
	}
	public int[] splitToComponentTimes(long longVal)
	{
		longVal = longVal / 1000;
	    int hours = (int) longVal / 3600;
	    int remainder = (int) longVal - hours * 3600;
	    int mins = remainder / 60;
	    remainder = remainder - mins * 60;
	    int secs = remainder;

	    int[] ints = {hours , mins , secs};
	    return ints;
	}
	
	public String getDefaultReportServicekey(String reportType){ 
		if ("ULR".equalsIgnoreCase(reportType)){
			return "autoUserLoginRService";
		}else if ("OSR".equalsIgnoreCase(reportType)){
			return "autoOSRService";
		}
		return "Not Found";
	}
	
	public String getServicekey(Object targetObjects, String targetMethod){ 
		String targetObject = targetObjects.getClass().getSimpleName();
		if ("DailyReportService".equalsIgnoreCase(targetObject)){
			String opsType = "";
			if (targetObjects instanceof DailyReportService){
				DailyReportService drs = (DailyReportService) targetObjects;
				opsType = drs.getIncludeSpecificOperationType().get(0);
			}
			if ("DRLLG".equals(opsType)) return "DDR";
			if ("CMPLT".equals(opsType)) return "DCR";
			if ("INTV".equals(opsType)) return "DIR";
		}else if ("DailyReportServiceNoAct".equalsIgnoreCase(targetObject)){
				String opsType1 = "";
				if (targetObjects instanceof DailyReportService){
					DailyReportService drs = (DailyReportService) targetObjects;
					opsType1 = drs.getIncludeSpecificOperationType().get(0);
				}
			if ("DRLLG".equals(opsType1)) return "DDR";
			if ("CMPLT".equals(opsType1)) return "DCR";
			if ("INTV".equals(opsType1)) return "DIR";	
		}else if ("DailyManagementReportService".equalsIgnoreCase(targetObject)) {
			return "MGT";
		}else if ("DefaultReportService".equalsIgnoreCase(targetObject))  {

		}else if ("DBCascadeDelete".equalsIgnoreCase(targetObject)){
			return "DBCascadeDeleteJob";
		}else if ("CheckChangePassword".equalsIgnoreCase(targetObject)) {
			return "CheckChangePassword";
		}else if ("CleanUpDvdSummary".equalsIgnoreCase(targetObject)){
			return "cleanUpDvdSummary";
		}else if ("ActiveSTTWorker".equalsIgnoreCase(targetObject)) {
			if ("sendAndRefreshFromTown".equalsIgnoreCase(targetMethod)) return "activeSTTWorker";
			else if ("sendToTownOnly".equalsIgnoreCase(targetMethod)) return "sendToTownOnly";
		}else if ("InvSTTJob".equalsIgnoreCase(targetObject)) {
			return "invoiceSTTJob";
		}else if ("STTJob".equalsIgnoreCase(targetObject)){
			return "ddrSTTJob";
		}else if ("LessonTicketReachEmailSchedule".equalsIgnoreCase(targetObject)) {
			LessonTicketReachEmailSchedule ltres = (LessonTicketReachEmailSchedule) targetObjects;
			String emailType = ltres.getEmailType(); 
			if ("lessonTicketMatch".equals(emailType)) return "lessonLearnedMatchingMail";
			if ("lessonTicketExpired".equals(emailType)) return "lessonLearnedExpiredMail";
		}else if ("ScheduledManagementReportMailService".equalsIgnoreCase(targetObject)) { 
			return "scheduledCustomManagementReport";
		}else if ("ScheduledCustomMailService".equalsIgnoreCase(targetObject)) { 
			return "scheduledCustomMail";
		}else if ("IndexerWorker".equalsIgnoreCase(targetObject)) {  
			return "indexerWorker";
		}else if ("EDMSyncJob".equalsIgnoreCase(targetObject)){
			if ("syncEdmCatalog".equalsIgnoreCase(targetMethod)) return "syncEdmCatalog";
			else if ("syncWithEdm".equalsIgnoreCase(targetMethod)) return "syncWithEdm";
		}else if ("ActivityCodeSchedulerJob".equalsIgnoreCase(targetObject)) {  
			return "activitySyncJob";
		}else if ("D2CustomWitsmlExportJob".equalsIgnoreCase(targetObject)) { 
			return "autoWITSMLExport_addax";
		}else if ("NextBopTestReportExtractionService".equalsIgnoreCase(targetObject)) { 
			return "nextBopTestEmailNotification";
		}else if ("DailyReportExtractionService".equalsIgnoreCase(targetObject)) { 
			return "addaxCDPDDR";
		}else if ("CombinedDDRReportModule".equalsIgnoreCase(targetObject)) { 
			return "ddrCombMultiWellReportModule";
		}
		return "Not Found";
	}
	
	public int convertToInt(String stringInt){
		if (NumberUtils.isNumber(stringInt)) return Integer.parseInt(stringInt);
		return (Integer) null;
	}
	
	public SchedulerJobDetail schedulerJobDetailUpdate(SchedulerJobDetail schedulerJobDetail){
	    //	String cronExpression = "0 m h d * w ";
		String cronExpression = schedulerJobDetail.getCronExpression();
		if ("0 0 * * * ?".equalsIgnoreCase(cronExpression)){
			schedulerJobDetail.setServiceFrequency("every_hour");
			schedulerJobDetail.setEveryValue(1);
			schedulerJobDetail.setCronExpression("0 0 0/1 * * ?");
			return schedulerJobDetail;
		}
    	String[] cronsExp = schedulerJobDetail.getCronExpression().split(" ");
    	String min = cronsExp[1];
    	String hrs = cronsExp[2];
    	String wd = cronsExp[5];
    	
    	int minint = 0;
    	int hrsint = 0;
    	if (("*".equalsIgnoreCase(hrs)) &&  min.indexOf("/")>0){
    		min = min.replace("0/", "");
    		min = min.replace("*/", "");
    		minint = convertToInt(min);
    		schedulerJobDetail.setServiceFrequency("every_min");
			schedulerJobDetail.setEveryValue(minint);
			return schedulerJobDetail;
    	}else if (hrs.indexOf("/")>0){
    		hrs = hrs.replace("0/", "");
    		hrs = hrs.replace("*/", "");
    		hrsint = convertToInt(hrs);
    		schedulerJobDetail.setServiceFrequency("every_hour");
			schedulerJobDetail.setEveryValue(hrsint);
			return schedulerJobDetail;
    	}else if ((NumberUtils.isNumber(min)) && (NumberUtils.isNumber(hrs))){
    		Calendar triggertime = Calendar.getInstance();
        	triggertime.setTime(new Date(0));
        	triggertime.set(Calendar.HOUR_OF_DAY, convertToInt(hrs));
        	triggertime.set(Calendar.MINUTE, convertToInt(min));
    		
        	if ("?".equalsIgnoreCase(wd)){
	        	schedulerJobDetail.setServiceFrequency("d");
				schedulerJobDetail.setEveryValue(null);
				schedulerJobDetail.setTriggerTime(triggertime.getTime());
				schedulerJobDetail.setServerTime(triggertime.getTime());
        	}else{
        		schedulerJobDetail.setServiceFrequency("w");
				schedulerJobDetail.setEveryValue(null);
				schedulerJobDetail.setDayOfWeek(wd);
				schedulerJobDetail.setTriggerTime(triggertime.getTime());
				schedulerJobDetail.setServerTime(triggertime.getTime());
        	}
    	}
 
		return schedulerJobDetail;
	}

	public Boolean isApplyToTown() {
		return true;
	}

	public Boolean isApplyToRig() {
		return true;
	}
}
