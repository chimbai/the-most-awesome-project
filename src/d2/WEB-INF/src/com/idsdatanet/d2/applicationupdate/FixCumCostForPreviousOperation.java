package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FixCumCostForPreviousOperation extends ApplicationUpdateConfig {

	public void run() throws Exception {
			List<ReportDaily> rptDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM ReportDaily " +
					"WHERE (isDeleted IS NULL OR isDeleted=0)");
			
			for (ReportDaily thisRptDaily : rptDaily)
			{
				String strSql = "select sum(daycost) from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= '"+thisRptDaily.getReportDatetime()+"' and wellUid = '"+thisRptDaily.getWellUid()+"'";
				Double cumCostFromPrevOps = null;
				List sumResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				Object a = (Object) sumResult.get(0);
				
				if (a != null) cumCostFromPrevOps = Double.parseDouble(a.toString());
				
				strSql = "select sum(costadjust) from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= '"+thisRptDaily.getReportDatetime()+"' and wellUid = '"+thisRptDaily.getWellUid()+"'";
				List lstResultAdjust = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				Object adjust = (Object) lstResultAdjust.get(0);
				Double cumcostadjust = null;
				if (adjust != null) cumcostadjust = Double.parseDouble(adjust.toString());
				
				if(cumcostadjust != null){
					if(cumCostFromPrevOps != null) cumCostFromPrevOps = cumCostFromPrevOps + cumcostadjust;
					else cumCostFromPrevOps = 0.0 + cumcostadjust;
				}
				
				strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= '"+thisRptDaily.getReportDatetime()+"' and wellUid = '"+thisRptDaily.getWellUid()+"'";
				List cumTangiCost = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				a = (Object) cumTangiCost.get(0);
				Double cumTangibleFromPrevOps = null;
				if (a != null) cumTangibleFromPrevOps = Double.parseDouble(a.toString());
				
				thisRptDaily.setCumCostFromPrevOperation(cumCostFromPrevOps);
	
				thisRptDaily.setCumTangibleCostFromPrevOperation(cumTangibleFromPrevOps);
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRptDaily);
			
			}

	}
	
}
