package com.idsdatanet.d2.applicationupdate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.core.lookup.xml.DepotLookupEntry;
import com.idsdatanet.d2.core.lookup.xml.XmlLookup;
import com.idsdatanet.d2.core.lookup.xml.XmlLookupEntry;
import com.idsdatanet.d2.core.lookup.xml.XmlLookupEntryGroup;
import com.idsdatanet.d2.core.lookup.xml.XmlLookupRoot;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.CommonLookupDepot;
import com.idsdatanet.d2.core.model.CommonLookupType;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class LookupXML2CommonLookup extends ApplicationUpdateConfig implements ServletContextAware {
	private ServletContext servletContext = null;
	private List<String> bannedList = null;
	
	public void setServletContext(ServletContext servletContext){
		this.servletContext = servletContext;
	}
	public void setBannedList(List<String> bannedList){
		this.bannedList = bannedList;
	}
	
	public void run() throws Exception {
		String xmlPath = "WEB-INF/config/lookup.xml";
		File xml = new File(this.servletContext.getRealPath("/"), xmlPath);
		System.out.println(xml.getPath());
		JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.core.lookup.xml");
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		JAXBElement jaxbElement = (JAXBElement) unmarshaller.unmarshal(xml);
		XmlLookupRoot root = (XmlLookupRoot) jaxbElement.getValue();
		List<String> parentLookup = new ArrayList<String>();
		int seq = 300;
		for(XmlLookup lookup: root.getLookup()){
			String lookupTypeName = lookup.getName();
			seq ++;
			if (allowAdding(lookupTypeName)) {
				System.out.println("[ name ]:" + lookupTypeName);
				CommonLookupType commonLookupType = new CommonLookupType();
				commonLookupType.setSequence(seq);
				commonLookupType.setLookupTypeSelection(lookupTypeName);
				commonLookupType.setLookupName(lookupTypeName);
				commonLookupType.setDescription("Import From Lookup.xml");
				
				if (lookup.getEntry().size()>0){
					
					for(XmlLookupEntry xmlLookupEntry: lookup.getEntry()){
						String shortCode = xmlLookupEntry.getCode();
						String lookupLabel = xmlLookupEntry.getLabel();

						CommonLookup commonLookup = new CommonLookup();
						commonLookup.setLookupTypeSelection(lookupTypeName);
						commonLookup.setShortCode(shortCode);
						commonLookup.setLookupLabel(lookupLabel);
						commonLookup.setIsActive(true);
						
						if ("ON".equals(xmlLookupEntry.getFilter())) commonLookup.setOnOffShore(true);
						if ("OFF".equals(xmlLookupEntry.getFilter())) commonLookup.setOnOffShore(false);
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookupType);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookup);
						
						if (xmlLookupEntry.getDepot() != null){
							for(DepotLookupEntry depotLookupEntry:xmlLookupEntry.getDepot() ){
								String type = depotLookupEntry.getType();
								String value = depotLookupEntry.getContent();
								CommonLookupDepot commonLookupDepot = new CommonLookupDepot();
								commonLookupDepot.setCommonLookupUid(commonLookup.getCommonLookupUid());
								commonLookupDepot.setDepotType(type);
								commonLookupDepot.setDepotValue(value);
								
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookupDepot);
							}
						}
					}
				}
				if (lookup.getGroup().size()>0){
					String parentKey = this.getParentKey(lookupTypeName);
					if ("Not Found".equalsIgnoreCase(parentKey)) parentLookup.add(lookupTypeName);
					else commonLookupType.setParentLookup(parentKey);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookupType);

					for(XmlLookupEntryGroup xmlLookupEntryGroup: lookup.getGroup()){
						String groupName = xmlLookupEntryGroup.getName();
						for(XmlLookupEntry xmlLookupEntry: xmlLookupEntryGroup.getEntry()){
							String shortCode = xmlLookupEntry.getCode();
							String lookupLabel = xmlLookupEntry.getLabel();
							
							CommonLookup commonLookup = new CommonLookup();
							commonLookup.setLookupTypeSelection(lookupTypeName);
							commonLookup.setShortCode(shortCode);
							commonLookup.setLookupLabel(lookupLabel);
							commonLookup.setParentReferenceKey(groupName);
							commonLookup.setIsActive(true);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookup);
						}
					}
				}
			}
		}

		for (String group:parentLookup){
			System.out.println("[ Group Name ]:" + group);
		}
	}

	private Boolean allowAdding(String value) {
		if (bannedList.indexOf(value)>=0) return false;
		if (value.contains("siteOperation")) return false;
		if (value.contains("SiteOperationPlanAction")) return false;
		if (value.contains("siteEntity")) return false;
		if (value.contains("site.")) return false;
		if (value.contains("siteLocation.")) return false;
		if (value.contains("wasteSample.")) return false;
		return true;
	}

	private String getParentKey(String child) {
		if ("Activity.nptSubCategory".equals(child)) return "Activity.nptMainCategory";
		else if ("bhacomponent.equipment_type_lookup".equals(child))return "";
		else if ("BopDetail.equipmentType".equals(child))return "";
		else if ("data_object".equals(child))return "depot_type";
		else if ("depot_version".equals(child))return "depot_type";
		else if ("equipmentFailure.subSystem".equals(child)) return "equipmentFailure.mainSystem";
		else if ("hse_incident.body_part".equals(child)) return "";
		else if ("lesson_ticket_reach.keywords1".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords2".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords3".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords4".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords5".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords6".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords6_5".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords7".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("lesson_ticket_reach.keywords8".equals(child)) return "lesson_ticket_reach.operation_type";
		else if ("mudvolumedetail.label_lookup".equals(child)) return "mudvolumedetail.type_lookup";
		else if ("mudvolumedetail.label_lookup_cmplt".equals(child)) return "";
		else if ("npd_subop".equals(child)) return "npd_mainop";
		else if ("operation.operationalCode".equals(child)) return "operationtype";
		else if ("operationname".equals(child)) return "";
		else if ("visnet_feature_setting.key".equals(child)) return "";
		else if ("well.operationalCode".equals(child)) return "well.operationalUnit";
		
		return "Not Found";
	}
	
	public Boolean isApplyToRig() {
		return false;
	}
}
