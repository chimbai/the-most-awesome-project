package com.idsdatanet.d2.applicationupdate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.AclEntry;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.CommonLookupDepot;
import com.idsdatanet.d2.core.model.MenuItem;
import com.idsdatanet.d2.core.model.MenuItemOperationType;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/** Add "Production" Operation Type to D2 - TOWN VERSION
 *  Actions:
 *  - Add Lookups:
 *      - Add PRD ReportDaily.reportTypes
 *      - Add PRD to OperationType (at the end of the list)
 *  - Add ACL Entry for reportDailyPRDController & wellOverviewController
 *      - Add to all ACL Groups that have access to reportDailyController
 *      - Add with same permissions (so clone reportDailyController and edit)
 *  - Configure menu to support Production Type
 *      - Add reportDailyPRD & formation & WellOverview screen to ProNet (for Production operation type)
 *      - Add anova menus
 *      - Configure remaining menus to hide.
 */
public class SetupProductionOperationTypeTown extends ApplicationUpdateConfig {

    public final String shortCode = "PRD";
    public final String operationType = "Production";
    public final String reportTypeLabel = "Daily Production Report";
    
    private List<String> standardMenuList = null;
    private List<Map<String, String>> proNetMenuList = null;
    private List<Map<String, String>> anovaMenuList = null;
    
    private List<String> newMenuItemUids = new ArrayList<String>();
    private List<String> operationTypeExclusionList = new ArrayList<String>();
    private List<String> secondaryMenuItemUidsToClean = new ArrayList<String>();
    private String setupMenuUid = null;
    
    public void setStandardMenuList(List<String> standardMenuList) {
        this.standardMenuList = standardMenuList;
    }
    
    public void setProNetMenuList(List<Map<String, String>> proNetMenuList){
        this.proNetMenuList = proNetMenuList;
    }
    
    public void setAnovaMenuList(List<Map<String, String>> anovaMenuList){
        this.anovaMenuList = anovaMenuList;
    }
    
	@Override
	public void run() {
	    System.out.println("==========================================");
	    System.out.println("[Setup New Operation Type Production(PRD) for TOWN]");
	    
	    try {
	        
	        this.addLookups();
	        this.addAclEntry();
	        this.addMenus();
	        this.configMenus();
	    
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	    System.out.println("[44956: SetupProductionOperationType Done]");
	    System.out.println("==========================================");
	}
	
    /**
     * Add Common Lookups that are related to PRD
     */
    private void addLookups() throws Exception {
        System.out.println("1. Add Lookups");
        this.addLookupToReportType();
        this.addLookupToOperationType();
        System.out.println("Lookups Done.");
        System.out.println();
    }
    
    private void addLookupToReportType() throws Exception {
        System.out.print("- Adding \"PRD\" to ReportDaily.reportTypes: ");
        String sql = "FROM CommonLookup WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive = true OR isActive IS NULL) AND lookupTypeSelection=:lookupTypeSelection AND shortCode=:shortCode";
        List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupTypeSelection","shortCode"}, new Object[] {"report.reportTypes",this.shortCode});
        if (result != null && result.size() == 0) {
            CommonLookup commonLookup = new CommonLookup();
            commonLookup.setIsActive(true);
            commonLookup.setLookupTypeSelection("report.reportTypes");
            commonLookup.setLookupLabel(reportTypeLabel);
            commonLookup.setShortCode(this.shortCode);
            ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookup);
            System.out.println("DONE [Lookup Added]");
        } else {
            System.out.println("SKIP [Lookup Exists]");
        }
    }
    
    private void addLookupToOperationType() throws Exception {
        String commonLookupUid = "";
        System.out.print("- Adding \"Production (PDR)\" to Operation Type: ");
        String sql = "FROM CommonLookup WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive = true OR isActive IS NULL) AND lookupTypeSelection=:lookupTypeSelection AND shortCode=:shortCode";
        List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupTypeSelection","shortCode"}, new Object[] {"operationType",this.shortCode});
        if (result != null && result.size() == 0) {
            CommonLookup commonLookup = new CommonLookup();
            commonLookup.setIsActive(true);
            commonLookup.setLookupTypeSelection("operationType");
            commonLookup.setLookupLabel(this.operationType);
            commonLookup.setShortCode(this.shortCode);
            commonLookup.setParentReferenceKey("");
            commonLookup.setDepotString("?witsml=unknown");
            sql = "FROM CommonLookup WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive = true OR isActive IS NULL) AND lookupTypeSelection=:lookupTypeSelection ORDER BY sequence DESC";
            List<CommonLookup> rv = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupTypeSelection"}, new Object[] {"operationType"});
            if (rv != null && rv.size() > 0) {
                CommonLookup cl = rv.get(0);
                commonLookup.setSequence(cl.getSequence() + 10);
            } else {
                commonLookup.setSequence(10);
            }
            ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookup);
            commonLookupUid = commonLookup.getCommonLookupUid();
            System.out.println("DONE [Lookup added to end of list]");
        } else {
            CommonLookup cl = result.get(0);
            commonLookupUid = cl.getCommonLookupUid();
            System.out.println("SKIP [Lookup Exists]");
        }
        if (StringUtils.isNotBlank(commonLookupUid)) {
            System.out.print("-- Common Lookup Depot \"Production (PRD)\": ");
            sql = "FROM CommonLookupDepot WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND commonLookupUid=:commonLookupUid AND depotType=:depotType AND depotValue=:depotValue";
            List<CommonLookupDepot> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"commonLookupUid","depotType","depotValue"}, new Object[] {commonLookupUid, "witsml","unknown"});
            if (result2 != null && result2.size() == 0) {
                CommonLookupDepot commonLookupDepot = new CommonLookupDepot();
                commonLookupDepot.setCommonLookupUid(commonLookupUid);
                commonLookupDepot.setDepotType("witsml");
                commonLookupDepot.setDepotValue("unknown");
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookupDepot);
                System.out.println("DONE [Entry added]");
            } else {
                System.out.println("SKIP [Entry Exists]");
            }
        }
    }
    
    /**
     * Add ACL Entries to certain ACL Groups
     */
    private void addAclEntry() throws Exception {
        System.out.println("2. Add ACL Entries to ACL Groups with reportDailyController");
        // Get ACL Groups that have access to reportDailyController
        System.out.print("Looking for ACL Groups with reportDailyController: ");
        String sql = "FROM AclEntry WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName";
        List<AclEntry> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName"}, new Object[] {"reportDailyController"});
        if (result != null && result.size() > 0) {
            System.out.println("Found " + result.size() + " groups!");
            for (AclEntry aclEntry : result) {
                System.out.println("- AclGroupUid[" + aclEntry.getAclGroupUid() + "]");
                // Adding ACL if not exist - Clone ACL including Permissions
                sql = "FROM AclEntry WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName AND aclGroupUid=:aclGroupUid";
                List<AclEntry> prdEntry = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName","aclGroupUid"}, new Object[] {"reportDailyPRDController", aclEntry.getAclGroupUid()});
                if (prdEntry != null && prdEntry.size() == 0) {
                    AclEntry aclEntryPRD = (AclEntry) BeanUtils.cloneBean(aclEntry);
                    aclEntryPRD.setAclEntryUid(null);
                    aclEntryPRD.setBeanName("reportDailyPRDController");
                    aclEntryPRD.setLastEditDatetime(null);
                    aclEntryPRD.setLastEditUserUid(null);
                    aclEntryPRD.setRecordOwnerUid(null);
                    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(aclEntryPRD);
                    System.out.println("-- ADD : Added reportDailyPRDController");
                } else {
                    System.out.println("-- SKIP: Already have reportDailyPRDController");
                }
                List<AclEntry> wovEntry = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName","aclGroupUid"}, new Object[] {"wellOverviewController",aclEntry.getAclGroupUid()});
                if (wovEntry != null && wovEntry.size() == 0) {
                    AclEntry aclEntryWOV = (AclEntry) BeanUtils.cloneBean(aclEntry);
                    aclEntryWOV.setAclEntryUid(null);
                    aclEntryWOV.setBeanName("wellOverviewController");
                    aclEntryWOV.setLastEditDatetime(null);
                    aclEntryWOV.setLastEditUserUid(null);
                    aclEntryWOV.setRecordOwnerUid(null);
                    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(aclEntryWOV);
                    System.out.println("-- ADD : Added wellOverviewController");
                } else {
                    System.out.println("-- SKIP: Already have wellOverviewController");
                }
            }
        } else {
            System.out.println("SKIP [No ACL Group found]");
        }
        System.out.println("ACL Entries Done.");
        System.out.println();
    }
    
    /**
     * Add Menus
     */
    private void addMenus() throws Exception {
        System.out.println("3. Add Menus");
        
        this.addProNetMenus();
        this.addAnovaMenus();
        
        System.out.println("Adding Menus Done.");
        System.out.println();
    }
    
    // Adding reportDailyPRD menu, Formation and WellOverview menu + setting them to PRD Operation Type
    private void addProNetMenus() throws Exception {
        System.out.println("- Adding ProNet Menus: ");
        
        // Locating ProNet Menu Item - Create if not exist
        MenuItem proNetMenuItem = this.getProNetMenuItem();
        if (proNetMenuItem == null) {
            System.out.println("** Something went terribly wrong! [ADD PRONET]");
            return;
        } else {
            System.out.println("-- ProNet Menu Item retrieved.");
        }
        newMenuItemUids.add(proNetMenuItem.getMenuItemUid());
        this.secondaryMenuItemUidsToClean.add(proNetMenuItem.getMenuItemUid());
        
        // Create ProNet Menus
        for (Map<String, String> menuMap : this.proNetMenuList) {
            String label = menuMap.get("label");
            String parentLabel = menuMap.get("parentLabel");
            String beanName = menuMap.get("controller");
            Integer sequence = Integer.parseInt(menuMap.get("sequence"));
            System.out.println("-- Processing Menu [" + label + "]");
            
            MenuItem mi = this.getMenuItemWithBeanNameAndLabel(beanName, label, parentLabel);
            
            if (mi == null) {
                System.out.println("-- " + label + " not found. Creating...");
                mi = new MenuItem();
                mi.setLabel(label);
                mi.setBeanName(beanName);
                mi.setParent(proNetMenuItem.getMenuItemUid());
                mi.setUrl("");
                mi.setOnOffShore("");
                mi.setSpecificTo("");
                mi.setSpecificToTightHole("");
                mi.setDataMatrixSectionName("ProNet");
                mi.setSequence(sequence);
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mi);
                System.out.println("-- " + label + " added to " + parentLabel);
            } else {
                System.out.println("-- Existing " + label + " found.");
            }
            newMenuItemUids.add(mi.getMenuItemUid());
            
            String prdOnly = menuMap.get("prdOnly");
            if (StringUtils.isNotBlank(prdOnly) && StringUtils.equalsIgnoreCase(prdOnly, "yes")) {
                this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
            } else { // Add to PRD if Menu has specific op type defined
                List<MenuItemOperationType> miot = this.getMenuItemOperationTypeList(mi.getMenuItemUid());
                if (miot != null && miot.size() > 0) {
                    this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
                } else {
                    System.out.println("[" + label + " is public, no need to add PRD op type]");
                }
            }
        }
    }
    
    // Locating ProNet Menu Item - Create if not exist
    private MenuItem getProNetMenuItem() throws Exception {
        MenuItem proNetMenu = null;
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (parent = '' OR parent IS NULL) AND label LIKE :label";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {"ProNet"});
        if (result != null && result.size() > 0) {
            proNetMenu = result.get(0);
        } else { // ProNet menu doesn't exist - Create and locate it after the menu that has the Operation screen.
            System.out.println("-- ProNet Menu Doesn't Exist. Creating new one.");
            MenuItem newProNetMenu = new MenuItem();
            newProNetMenu.setLabel("ProNet");
            newProNetMenu.setBeanName("");
            newProNetMenu.setParent("");
            newProNetMenu.setUrl("");
            newProNetMenu.setOnOffShore("");
            newProNetMenu.setOnBgColor("#ffc425");
            newProNetMenu.setOffBgColor("#ffc425");
            newProNetMenu.setOnColor("#ffffff");
            newProNetMenu.setOffColor("#ffffff");
            newProNetMenu.setSelectedBgColor("#ffc425");
            newProNetMenu.setSelectedColor("#ffffff");
            newProNetMenu.setSpecificTo("");
            newProNetMenu.setSpecificToTightHole("");
            newProNetMenu.setSpecificView("Well Management");
            newProNetMenu.setDataMatrixSectionName("");
            // d2point5_demo settings
            
            MenuItem operationMenu = getMenuItem("operationController");
            if (operationMenu != null && operationMenu.getSequence() != null) {
                newProNetMenu.setSequence(operationMenu.getSequence() + 1);
                System.out.println("--- New ProNet Menu located after Menu with Operation screen [SEQ:" + newProNetMenu.getSequence() + "]");
            }
            ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newProNetMenu);
            System.out.println("-- New ProNet Menu Created.");
            proNetMenu = newProNetMenu;
        }
        this.createMenuItemOperationTypeLink(proNetMenu.getMenuItemUid(), this.shortCode);
        return proNetMenu;
    }

    private void addAnovaMenus() throws Exception {
        System.out.println("- Adding ANOVA Menus: ");
        
        // Locating ANOVA Menu Item - Create if not exist
        MenuItem anovaMenuItem = this.getAnovaMenuItem();
        if (anovaMenuItem == null) {
            System.out.println("** Something went terribly wrong! [ADD ANOVA]");
            return;
        } else {
            System.out.println("-- ANOVA Menu Item retrieved.");
        }
        newMenuItemUids.add(anovaMenuItem.getMenuItemUid());
        
        // Create ANOVA Menus
        for (Map<String, String> menuMap : this.anovaMenuList) {
            String label = menuMap.get("label");
            String parentLabel = menuMap.get("parentLabel");
            String beanName = (menuMap.get("controller")!=null) ? menuMap.get("controller") : "";
            String url = (menuMap.get("url")!=null) ? menuMap.get("url") : "";
            Integer sequence = Integer.parseInt(menuMap.get("sequence"));
            System.out.println("-- Processing Menu [" + label + "]");
            
            MenuItem mi = this.getMenuItemWithBeanNameAndLabel(beanName, label, parentLabel);
            
            if (mi == null) {
                System.out.println("-- " + label + " not found. Creating...");
                mi = new MenuItem();
                mi.setLabel(label);
                mi.setBeanName(beanName);
                String parentUid = this.resolveParentMenuUid(parentLabel);
                if(StringUtils.isNotBlank(parentLabel) && StringUtils.isBlank(parentUid)) {
                    throw new Exception("ERROR: Failed to Find any Parent MenuItem:" + parentLabel);
                }
                mi.setParent(parentUid);
                mi.setUrl(url);
                mi.setOnOffShore("");
                mi.setSpecificTo("");
                mi.setSpecificToTightHole("");
                mi.setDataMatrixSectionName("");
                mi.setSequence(sequence);
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mi);
                System.out.println("-- " + label + " added to " + parentLabel);
            } else {
                System.out.println("-- Existing " + label + " found.");
            }
            newMenuItemUids.add(mi.getMenuItemUid());
            
            String prdOnly = menuMap.get("prdOnly");
            if (StringUtils.isNotBlank(prdOnly) && StringUtils.equalsIgnoreCase(prdOnly, "yes")) {
                this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
            } else { // Add to PRD if Menu has specific op type defined
                List<MenuItemOperationType> miot = this.getMenuItemOperationTypeList(mi.getMenuItemUid());
                if (miot != null && miot.size() > 0) {
                    this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
                } else {
                    System.out.println("[" + label + " is public, no need to add PRD op type]");
                }
            }
        }
    }
    
    // Locating ANOVA Menu Item - Create if not exist
    private MenuItem getAnovaMenuItem() throws Exception {
        MenuItem anovaMenu = null;
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (parent = '' OR parent IS NULL) AND label LIKE :label";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {"ANOVA"});
        if (result != null && result.size() > 0) {
            anovaMenu = result.get(0);
        } else { // ANOVA doesn't exist - Create and locate it after the menu that has the Operation screen.
            System.out.println("-- ProNet Menu Doesn't Exist. Creating new one.");
            MenuItem newAnovaMenu = new MenuItem();
            newAnovaMenu.setLabel("ANOVA");
            newAnovaMenu.setBeanName("");
            newAnovaMenu.setParent("");
            newAnovaMenu.setUrl("");
            newAnovaMenu.setOnOffShore("");
            newAnovaMenu.setOffBgColor("#696969");
            newAnovaMenu.setSpecificTo("");
            newAnovaMenu.setSpecificToTightHole("");
            newAnovaMenu.setSpecificView("");
            newAnovaMenu.setDataMatrixSectionName("");
            newAnovaMenu.setSequence(90); // d2point5_demo settings
            
            ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newAnovaMenu);
            System.out.println("-- New ANOVA Created.");
            anovaMenu = newAnovaMenu;
            this.createMenuItemOperationTypeLink(anovaMenu.getMenuItemUid(), this.shortCode); // Apply whitelist only if not existing
        }
        return anovaMenu;
    }
   
    
    
    /**
     * Hides all menus other than pre-defined for PRD operation.
     * Because D2 does not have a blacklist, we'll have to whitelist all other operationTypes OTHER than PRD to effectively 'hide it'. 
     */
    private void configMenus() throws Exception {
        System.out.println("4. Configuring Menus");
        
        this.operationTypeExclusionList = this.initializeOperationTypeExclusionList();
        this.addStandardMenusToList();
        this.hideMainMenus();
        
        for (String menuItemUid : this.secondaryMenuItemUidsToClean) {
            this.hideMenus(menuItemUid);
        }
        
        System.out.println("Configuring Menus Done.");
        System.out.println();
    }
    
    // Handles menus that have PRD menu items (so hide the rest - 2nd pass)
    private void hideMenus(String parent) throws Exception {
        System.out.println("- Hiding Menu Items in: " + parent);
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND parent=:parent AND menuItemUid NOT IN (SELECT DISTINCT menuItemUid FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"parent"}, new Object[] {parent});
        if (result != null && result.size() > 0) {
            for (MenuItem mi : result) {
                String menuItemUid = mi.getMenuItemUid();
                if (this.newMenuItemUids.contains(menuItemUid)) {
                    continue;
                } else {
                    System.out.println("-- Hiding " + mi.getLabel());
                    for (String operationCode : this.operationTypeExclusionList) {
                        this.createMenuItemOperationTypeLink(menuItemUid, operationCode);
                    }
                }
            }
        } else {
            System.out.println("- Nothing to hide!");
        }
    }
    
    // Adds standard menus to exclusion list as well as setup
    private void addStandardMenusToList() throws Exception {
        System.out.print("- Adding Standard Menus to Exclusion List:");
        for (String beanName : this.standardMenuList) {
            MenuItem mi = this.getMenuItem(beanName);
            if (mi != null) {
                String menuItemUid = mi.getMenuItemUid();
                String parentUid = mi.getParent();
                this.newMenuItemUids.add(menuItemUid);
                if (StringUtils.isNotBlank(parentUid) && !this.newMenuItemUids.contains(parentUid)) {
                    this.newMenuItemUids.add(parentUid);
                }
            }
        }
        
        // Add Setup to list
        MenuItem setupMenu = this.getMenuItem("SETUP", true);
        if (setupMenu != null) {
            this.setupMenuUid = setupMenu.getMenuItemUid();
            this.newMenuItemUids.add(this.setupMenuUid);
        }
        
        System.out.println("Done");
    }
    
    // Hide Main Menus (1st Pass Through)
    private void hideMainMenus() throws Exception {
        System.out.println("- Hiding Main Menus");
        String arrayListStr = "(";
        for (String s : this.newMenuItemUids) {
            arrayListStr += "'" + s + "',";
        }
        arrayListStr = arrayListStr.substring(0, arrayListStr.length()-1) + ")";
        
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (parent='' OR parent IS NULL) AND menuItemUid NOT IN (SELECT DISTINCT menuItemUid FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
        if (result != null && result.size() > 0) {
            for (MenuItem mi : result) {
                String menuItemUid = mi.getMenuItemUid();
                if (this.newMenuItemUids.contains(menuItemUid)) {
                    if (!StringUtils.equals(this.setupMenuUid, menuItemUid)){
                        this.secondaryMenuItemUidsToClean.add(menuItemUid);
                        System.out.println("-- Saving menu " + mi.getLabel() + " for detailed menu cleaning");
                    }
                    continue;
                } else {
                    System.out.println("-- Hiding " + mi.getLabel());
                    for (String operationCode : this.operationTypeExclusionList) {
                        this.createMenuItemOperationTypeLink(menuItemUid, operationCode);
                    }
                }
            }
        } else {
            System.out.println("- Nothing to hide!");
        }
        System.out.println("- Hiding Main Menus Done");
    }

    /* UTILITY METHODS */
    
    // Get Menu Item by Bean Name - that isn't a parent itself
    private MenuItem getMenuItem(String beanName) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName and (parent='' OR parent IS NULL OR parent IN (SELECT DISTINCT menuItemUid FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (beanName IS NULL OR beanName = '')))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName"}, new Object[] {beanName});
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    // Get menu item with label. Flag to get a parent or not.
    private MenuItem getMenuItem(String label, boolean parent) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND label LIKE :label";
        if (parent) {
            sql += " AND (beanName IS NULL OR beanName='')";
        }
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {label});
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    // Get Menu Item with given parent label. Null not accepted.
    private MenuItem getMenuItemWithBeanNameAndLabel(String beanName, String label, String parentLabel) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName and (parent='' OR parent IS NULL OR parent IN (SELECT DISTINCT menuItemUid FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (beanName IS NULL OR beanName = '')))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName"}, new Object[] {beanName});
        if (result != null && result.size() > 0) {
            for(MenuItem mi : result) {
                if (StringUtils.equalsIgnoreCase(label, mi.getLabel())) {
                    MenuItem parent = (MenuItem) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(MenuItem.class, mi.getParent());
                    if (parent != null && StringUtils.isNotBlank(parent.getLabel()) && StringUtils.equalsIgnoreCase(parent.getLabel(), parentLabel)) {
                        return mi;
                    }
                }
            }
        }
        return null;
    }
    
    private List<MenuItemOperationType> getMenuItemOperationTypeList(String menuItemUid) throws Exception {
        String sql = "FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND menuItemUid=:menuItemUid";
        List<MenuItemOperationType> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"menuItemUid"}, new Object[] {menuItemUid});
        if (result != null) {
            return result;
        }
        return null;
    }
    
    // Create link/whitelist between operationType and menuItem
    private void createMenuItemOperationTypeLink(String menuItemUid, String operationCode) throws Exception {
        if (StringUtils.isNotBlank(menuItemUid) && StringUtils.isNotBlank(operationCode)) {
            String sql = "FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND menuItemUid=:menuItemUid AND operationCode=:operationCode";
            List<MenuItemOperationType> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"menuItemUid","operationCode"}, new Object[] {menuItemUid, operationCode});
            if (result != null && result.size() == 0) {
                MenuItemOperationType menuOpType = new MenuItemOperationType();
                menuOpType.setMenuItemUid(menuItemUid);
                menuOpType.setOperationCode(operationCode);
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(menuOpType);
                //System.out.println("[MenuItemOperationType Link created between menuItemUid:" + menuItemUid + " and opeartionCode:" + operationCode + "]");
            } else {
                //System.out.println("[MenuItemOperationType Link exists between menuItemUid:" + menuItemUid + " and opeartionCode:" + operationCode + "]");
            }
        }
    }
    
    private String resolveParentMenuUid(String label) throws Exception {
        String arrayListStr = "(";
        for (String s : this.newMenuItemUids) {
            arrayListStr += "'" + s + "',";
        }
        arrayListStr = arrayListStr.substring(0, arrayListStr.length()-1) + ")";
        
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND label LIKE :label AND menuItemUid IN " + arrayListStr;
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {label});
        if (result != null && result.size() > 0) {
            MenuItem rv = result.get(0);
            return rv.getMenuItemUid();
        } else {
            return null;
        }
    }
    
    private List<String> initializeOperationTypeExclusionList() throws Exception {
        String sql = "SELECT DISTINCT shortCode FROM CommonLookup WHERE (isDeleted = false OR isDeleted is null) AND (isActive = true OR isActive is null) AND lookupTypeSelection=:lookupTypeSelection AND shortCode!='PRD'";
        List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupTypeSelection"}, new Object[] {"operationType"});
        if (result != null && result.size() > 0) {
            return result;
        }
        return null;
    }

    public Boolean isApplyToRig() {
        return false;
    }
}