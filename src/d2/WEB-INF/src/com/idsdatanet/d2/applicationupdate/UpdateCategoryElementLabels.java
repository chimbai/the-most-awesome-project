package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class UpdateCategoryElementLabels extends ApplicationUpdateConfig{

	@Override
	public void run() throws Exception {
		
		Manager dao = ApplicationUtils.getConfiguredInstance().getDaoManager();
		String sql = "from LessonTicket where (isDeleted is null or isDeleted = false)";
		List<LessonTicket> lts = dao.find(sql);
		
		if(lts != null){
			for(LessonTicket lt : lts){
				sql = "select ltce.lessonCategory, ltce.lessonElement, " + ""
						+ "(select lc.lessonTicketCategory from LookupLessonTicketCategory lc where (lc.isDeleted = false or lc.isDeleted is null) and lc.lookupLessonTicketCategoryUid = ltce.lessonCategory) as lessonCategoryLabel, " 
						+ "(select le.lessonTicketElements from LookupLessonTicketElements le where (le.isDeleted = false or le.isDeleted is null) and le.lookupLessonTicketElementsUid = ltce.lessonElement) as lessonElementLabel "
						+ "from LessonTicketCategoryElement ltce where (ltce.isDeleted is null or ltce.isDeleted = false) "
						+ "and ltce.lessonTicketUid = :lessonTicketUid";
				List<Object[]> ltces = dao.findByNamedParam(sql, "lessonTicketUid", lt.getLessonTicketUid());
				
				if(ltces != null){
					String contatenatedCategoryElements = null;
					for(Object[] ltce : ltces){
						Object lessonCategoryLabel = ltce[2];
						Object lessonElementLabel = ltce[3];
						if(lessonCategoryLabel != null && lessonElementLabel != null){
							if(contatenatedCategoryElements == null){
								contatenatedCategoryElements = lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}else{
								contatenatedCategoryElements = contatenatedCategoryElements  + "\n" + lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}
						}
					}
					lt.setCategoryElementLabels(contatenatedCategoryElements);
					dao.saveObject(lt);
				}
			}
		}
	}
	
	

}
