package com.idsdatanet.d2.applicationupdate;

public interface ApplicationUpdate {
	
	public void run() throws Exception;
	public Boolean isApplyToTown();
	public Boolean isApplyToRig(); 

}