package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.publishsubscribe.PublishSubscribeManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class LookupPubSub extends ApplicationUpdateConfig {

	public void run() throws Exception {
		String hql = "from CommonLookup where (isDeleted = false or isDeleted is null) and lookupTypeSelection=:lookupTypeSelection and shortCode=:shortCode";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"lookupTypeSelection", "shortCode"}, new Object[] { "depot_type", PublishSubscribeManager.LOOKUP_SUBSCRIBER });
		if (result != null && result.size() == 0) {
			CommonLookup lookupSubscriber = new CommonLookup();
			lookupSubscriber.setLookupTypeSelection("depot_type");
			lookupSubscriber.setShortCode(PublishSubscribeManager.LOOKUP_SUBSCRIBER);
			lookupSubscriber.setLookupLabel("Lookup Subscriber");
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookupSubscriber);
			
			CommonLookup lookupSubscriberVersion = new CommonLookup();
			lookupSubscriberVersion.setLookupTypeSelection("depot_version");
			lookupSubscriberVersion.setParentReferenceKey(PublishSubscribeManager.LOOKUP_SUBSCRIBER);
			lookupSubscriberVersion.setShortCode("1.0");
			lookupSubscriberVersion.setLookupLabel("1.0");
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookupSubscriberVersion);
		}
	}
}
