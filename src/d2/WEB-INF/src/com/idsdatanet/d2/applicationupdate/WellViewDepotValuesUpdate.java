package com.idsdatanet.d2.applicationupdate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.CommonLookupDepot;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class WellViewDepotValuesUpdate extends ApplicationUpdateConfig {

	/**
	 * Update CommonLookupDepot with wellview depot values for operation types and report types to be used in the WellView DEPOT callbacks.
	 * Primarily for WellViewOperationCodeFieldComponent and ReportDailyMappingCallback.
	 * Also checks if the WellView depot_type and depot_version exists, and setups if they are missing.
	 * Add this as a singleton on sys_update.bean-config.xml and check table sys_jsp_update after update to see check if this class ran.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run() throws Exception {
		// setup
		String WELLVIEW = "wellview", DEPOT_TYPE = "EXT_WELL_CATALOG", DEPOT_VERSION = "WellView 1.0.0.0"; 
		// operation mappings of D2 Operation Types and WellView.jobCatagories
		Map<String, String> operationMapping = Arrays.stream(new String[][] {
			{"DRLLG","LAND-ORIG DRLG,Drill"},
			{"CMPLT","LAND-ORIG COMP,LAND-RECOMPLETION,Completion,Drill and Complete"},
			{"WKO","Major Rig Work Over (MRWO)"},
			{"SVC","Well Services"},
			{"PNA","Abandon"},
			{"RM","LAND-RIG MOB"},
			{"WELLTEST","Maintenance"},
		}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
		// report type mappings from D2 operation type to D2 report type
		Map<String, String> reportTypeMapping = Arrays.stream(new String[][] {
			{"DDR","DRLLG"},
			{"CMPLT","CMPLT"},
			{"WKO","WKO"},
			{"DWSR","SVC"},
			{"PNA","PNA"},
			{"RM","RM"},
			{"DWTR","WELLTEST"}
		}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
		
		// fetch the operationType CommonLookups
		String hql = "FROM CommonLookup WHERE lookupTypeSelection = 'operationType' AND shortCode IN ('DRLLG','CMPLT','WKO','SVC','PNA','RM','WELLTEST')";
		List<CommonLookup> lookupOperationTypes = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		// set wellview equivalent operationTypes
		for(CommonLookup operationType : lookupOperationTypes) {
			CommonLookupDepot cld = new CommonLookupDepot();
			cld.setCommonLookupUid(operationType.getCommonLookupUid());
			cld.setDepotType(WELLVIEW);
			cld.setDepotValue(operationMapping.get(operationType.getShortCode()));
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cld);
		}
		
		// fetch the report.reportTypes CommonLookups
		hql = "FROM CommonLookup WHERE lookupTypeSelection = 'report.reportTypes' AND shortCode IN ('DDR','CMPLT','DWSR','WKO','PNA','RM','DWTR')";
		List<CommonLookup> lookupReportTypes = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		// set wellview equivalent reportTypes
		for(CommonLookup reportType : lookupReportTypes) {
			CommonLookupDepot cld = new CommonLookupDepot();
			cld.setCommonLookupUid(reportType.getCommonLookupUid());
			cld.setDepotType(WELLVIEW);
			cld.setDepotValue(reportTypeMapping.get(reportType.getShortCode()));
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cld);
		}
		
		// check if EXT_WELL_CATALOG depotType or WellView 1.0.0.0 depotVersion exists. If it doesn't exist we can create it here.
		hql = "FROM CommonLookup WHERE (isDeleted=0 OR isDeleted IS NULL) AND lookupTypeSelection='depot_type' AND shortCode='EXT_WELL_CATALOG'";
		List<CommonLookup> depotType = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if (!(depotType != null && depotType.size() > 0)) {
			CommonLookup cl = new CommonLookup();
			cl.setLookupTypeSelection("depot_type");
			cl.setShortCode(DEPOT_TYPE);
			cl.setLookupLabel("External Well Catalog");
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cl);
		}
		
		hql = "FROM CommonLookup WHERE (isDeleted=0 OR isDeleted IS NULL) AND lookupTypeSelection='depot_version' AND shortCode='WellView 1.0.0.0'";
		List<CommonLookup> depotVersion = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if (!(depotVersion != null && depotVersion.size() > 0)) {
			CommonLookup cl = new CommonLookup();
			cl.setLookupTypeSelection("depot_version");
			cl.setShortCode(DEPOT_VERSION);
			cl.setLookupLabel(DEPOT_VERSION);
			cl.setParentReferenceKey(DEPOT_TYPE);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cl);
		}
	}
}
