package com.idsdatanet.d2.applicationupdate;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.dao.DaoUserContext;
import com.idsdatanet.d2.core.model.SysJspUpdate;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ApplicationUpdateManager extends DefaultBeanSupport implements InitializingBean {
	private static Log logger = LogFactory.getLog(ApplicationUpdateManager.class);
	private String systemPlatform = null;
	
	public void setSystemPlatform(String systemPlatform) {
		this.systemPlatform = systemPlatform;
	}
	
	public void afterPropertiesSet() throws Exception {
		Map<String, ApplicationUpdate> map = this.getApplicationContext().getBeansOfType(ApplicationUpdate.class, false, true);

		if (!map.isEmpty()) {
			logger.info("===Start===");
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
	        userSelection.populdateDataAccordingToUserLogin("idsadmin");
			for(ApplicationUpdate update : map.values()) {
				String cls = update.getClass().getSimpleName();
				SysJspUpdate sysJspUpdate = new SysJspUpdate();
				sysJspUpdate.setUpdatedJspFile(cls);
				boolean updates = false;
				try {
			        DaoUserContext.getThreadLocalInstance().setUserSelection(userSelection);
					if (StringUtils.isNotEmpty(cls)){
						String hql = "from SysJspUpdate where updatedJspFile='" + cls +"'";
						List result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
						
						if (result.size()==0){
							updates = true;
							logger.info("Update : " + cls + " = Start");
							if ((update.isApplyToTown() && ("town".equalsIgnoreCase(this.systemPlatform))) ||
								(update.isApplyToRig() && ("rig".equalsIgnoreCase(this.systemPlatform)))){
									update.run();  // Run the Update Function~
									logger.info("Update : " + cls + " = Done");
									sysJspUpdate.setStatus("Success");
							}else{
								logger.info("Update : " + cls + " = Not Apply to " + this.systemPlatform);
								sysJspUpdate.setStatus("Not Apply to " + this.systemPlatform);
							}
						}else{
							logger.info("Update : " + cls + " = Skip");
						}
						
					}
				} catch(Exception e) {
					logger.info("Update : " + cls + " = Fail");
					sysJspUpdate.setStatus("Failed");
					if (e.getMessage()!=null)sysJspUpdate.setError(e.getMessage());
					else sysJspUpdate.setError(e.toString());
				} finally {						
					if (updates) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysJspUpdate);
					DaoUserContext.clearThreadLocalInstance();
				}
				
			}
			logger.info("===End===");
		}
	}
	
}
