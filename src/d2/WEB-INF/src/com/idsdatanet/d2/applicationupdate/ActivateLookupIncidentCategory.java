package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.model.Group;
import com.idsdatanet.d2.core.model.LookupIncidentCategory;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ActivateLookupIncidentCategory extends ApplicationUpdateConfig {

	@Override
	public void run() throws Exception {
		String hql = "from Group where (isDeleted is null or isDeleted = 0) and parentGroupUid is null";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		Group group = (Group)result.get(0);
		
		String[] incidentTypes = {"Rig Check", "Rig Inspections", "Inspection Pre-Work Safety", "Pit Drill", "Diverter Drill", "Last Trip Drill", "Test COM."};
		for (String incidentType : incidentTypes) {
			hql = "from LookupIncidentCategory where hseCategory=:hseCategory and (isDeleted is null or isDeleted = 0)";
			List<LookupIncidentCategory> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "hseCategory", incidentType);
			if (results != null && results.size() > 0) {
				for (LookupIncidentCategory lookupIncidentCategory : results) {
					if (BooleanUtils.isFalse(lookupIncidentCategory.getIsActive())) {
						lookupIncidentCategory.setIsActive(true);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookupIncidentCategory);
					}
				}
			} else {
				LookupIncidentCategory newLookupIncidentCategory = new LookupIncidentCategory();
				newLookupIncidentCategory.setHseCategory(incidentType);
				newLookupIncidentCategory.setIsActive(true);
				newLookupIncidentCategory.setShowInDdr(true);
				newLookupIncidentCategory.setGroupUid(group.getGroupUid());
				newLookupIncidentCategory.setRecordOwnerUid("ApplicationUpdate:ActivateLookupIncidentCategory");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newLookupIncidentCategory);
			}
		}
	}
}
