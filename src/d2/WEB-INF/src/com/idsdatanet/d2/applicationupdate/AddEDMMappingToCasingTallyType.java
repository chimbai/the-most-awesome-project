package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.CommonLookupDepot;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.DepotConstants;

public class AddEDMMappingToCasingTallyType extends ApplicationUpdateConfig {
	private final static String RISER = "RSR";
	private final static String OPEN_HOLE = "OH";
	private final static String CASING = "CAS";
	
	@Override
	public void run() throws Exception {
		String hql = "from CommonLookup where lookupTypeSelection=:lookupTypeSelection";
		List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "lookupTypeSelection", "CasingTally.type");
		if (result != null && result.size() > 0) {
			for (CommonLookup commonLookup : result) {
				String depotValue = null;
				if ("RISER".equals(commonLookup.getShortCode())) {
					depotValue = RISER;
				} else if ("OPENHOLE".equals(commonLookup.getShortCode())) {
					depotValue = OPEN_HOLE;
				} else {
					depotValue = CASING;
				}
				hql = "from CommonLookupDepot where (isDeleted = false or isDeleted is null) and commonLookupUid=:commonLookupUid and depotType=:depotType and depotValue=:depotValue";
				List depot_lookup_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"depotType", "depotValue", "commonLookupUid"}, new Object[] {DepotConstants.EDM, depotValue, commonLookup.getCommonLookupUid()});
				
				if (depot_lookup_result.size() == 0) {
					CommonLookupDepot cld = new CommonLookupDepot();
					cld.setCommonLookupUid(commonLookup.getCommonLookupUid());
					cld.setDepotType(DepotConstants.EDM);
					cld.setDepotValue(depotValue);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cld);
				}
			}
		}
	}
}
