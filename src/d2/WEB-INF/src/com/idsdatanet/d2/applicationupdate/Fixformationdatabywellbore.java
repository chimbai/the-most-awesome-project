package com.idsdatanet.d2.applicationupdate;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class Fixformationdatabywellbore extends ApplicationUpdateConfig {

	public void run() throws Exception {
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(true);	
			
			//String newUid = "";
			
			List<Well> wells = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Well where (isDeleted is NULL or isDeleted= FALSE)", queryProperties);
			
				if(wells.size()>0 && wells !=null){
					for (Well well : wells) {
						String wellUid = well.getWellUid();
						List<Wellbore> wellbores = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Wellbore where (isDeleted is null or isDeleted=false) and wellUid='"+wellUid+"'", queryProperties);
						
						if(wellbores.size()> 1 && wellbores!=null){
							
								List<Formation> formations = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Formation Where (isDeleted is null or isDeleted =false) and wellUid='"+wellUid+"'", queryProperties);
								if(formations.size()>0 && formations!=null){
									for(Formation formation : formations){
										//newUid = UUID.randomUUID().toString();
										Boolean firstRecord = true;
										for (Wellbore wellbore : wellbores) {
											if (firstRecord){
												formation.setWellboreUid(wellbore.getWellboreUid());
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(formation);
											}else{
												Formation newFormation = (Formation) BeanUtils.cloneBean(formation);
												newFormation.setWellboreUid(wellbore.getWellboreUid());
												newFormation.setFormationUid(null);
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newFormation, queryProperties);
											}
											firstRecord=false;
											
										}
									}
									
								}
							
						}
					}
					
				}

	}


}
