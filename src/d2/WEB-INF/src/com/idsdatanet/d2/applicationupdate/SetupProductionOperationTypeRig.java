package com.idsdatanet.d2.applicationupdate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.MenuItem;
import com.idsdatanet.d2.core.model.MenuItemOperationType;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;

/** Add "Production" Operation Type to D2 - RIG VERSION
 *  Actions:
 *  - Configure menu to support Production Type
 *      - Add reportDailyPRD & formation & WellOverview screen to ProNet (for Production operation type)
 *      - Add anova menus
 *      - Configure remaining menus to hide.
 */
public class SetupProductionOperationTypeRig extends ApplicationUpdateConfig {

    public final String shortCode = "PRD";
    public final String operationType = "Production";
    public final String reportTypeLabel = "Daily Production Report";
    
    private List<String> standardMenuList = null;
    private List<Map<String, String>> proNetMenuList = null;
    
    private List<String> newMenuItemUids = new ArrayList<String>();
    private List<String> operationTypeExclusionList = new ArrayList<String>();
    private List<String> secondaryMenuItemUidsToClean = new ArrayList<String>();
    private String setupMenuUid = null;
    
    public void setStandardMenuList(List<String> standardMenuList) {
        this.standardMenuList = standardMenuList;
    }
    
    public void setProNetMenuList(List<Map<String, String>> proNetMenuList){
        this.proNetMenuList = proNetMenuList;
    }
    
	@Override
	public void run() {
	    System.out.println("==========================================");
	    System.out.println("[Setup New Operation Type Production(PRD) for RIG]");
	    
	    try {
	        this.addMenus();
	        this.configMenus();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	    System.out.println("[44956: SetupProductionOperationType Done]");
	    System.out.println("==========================================");
	}
    
    /**
     * Add Menus
     */
    private void addMenus() throws Exception {
        System.out.println("3. Add Menus");
        
        this.addProNetMenus();
        
        System.out.println("Adding Menus Done.");
        System.out.println();
    }
    
    // Adding reportDailyPRD menu, Formation and WellOverview menu + setting them to PRD Operation Type
    private void addProNetMenus() throws Exception {
        System.out.println("- Adding ProNet Menus: ");
        
        // Locating ProNet Menu Item - Create if not exist
        MenuItem proNetMenuItem = this.getProNetMenuItem();
        if (proNetMenuItem == null) {
            System.out.println("** Something went terribly wrong! [ADD PRONET]");
            return;
        } else {
            System.out.println("-- ProNet Menu Item retrieved.");
        }
        newMenuItemUids.add(proNetMenuItem.getMenuItemUid());
        this.secondaryMenuItemUidsToClean.add(proNetMenuItem.getMenuItemUid());
        
        // Create ProNet Menus
        for (Map<String, String> menuMap : this.proNetMenuList) {
            String label = menuMap.get("label");
            String parentLabel = menuMap.get("parentLabel");
            String beanName = menuMap.get("controller");
            Integer sequence = Integer.parseInt(menuMap.get("sequence"));
            System.out.println("-- Processing Menu [" + label + "]");
            
            MenuItem mi = this.getMenuItemWithBeanNameAndLabel(beanName, label, parentLabel);
            
            if (mi == null) {
                System.out.println("-- " + label + " not found. Creating...");
                mi = new MenuItem();
                mi.setLabel(label);
                mi.setBeanName(beanName);
                mi.setParent(proNetMenuItem.getMenuItemUid());
                mi.setUrl("");
                mi.setOnOffShore("");
                mi.setSpecificTo("");
                mi.setSpecificToTightHole("");
                mi.setDataMatrixSectionName("ProNet");
                mi.setSequence(sequence);
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mi);
                System.out.println("-- " + label + " added to " + parentLabel);
            } else {
                System.out.println("-- Existing " + label + " found.");
            }
            newMenuItemUids.add(mi.getMenuItemUid());
            
            String prdOnly = menuMap.get("prdOnly");
            if (StringUtils.isNotBlank(prdOnly) && StringUtils.equalsIgnoreCase(prdOnly, "yes")) {
                this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
            } else { // Add to PRD if Menu has specific op type defined
                List<MenuItemOperationType> miot = this.getMenuItemOperationTypeList(mi.getMenuItemUid());
                if (miot != null && miot.size() > 0) {
                    this.createMenuItemOperationTypeLink(mi.getMenuItemUid(), this.shortCode);
                } else {
                    System.out.println("[" + label + " is public, no need to add PRD op type]");
                }
            }
        }
    }
    
    // Locating ProNet Menu Item - Create if not exist
    private MenuItem getProNetMenuItem() throws Exception {
        MenuItem proNetMenu = null;
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (parent = '' OR parent IS NULL) AND label LIKE :label";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {"ProNet"});
        if (result != null && result.size() > 0) {
            proNetMenu = result.get(0);
        } else { // ProNet menu doesn't exist - Create and locate it after the menu that has the Operation screen.
            System.out.println("-- ProNet Menu Doesn't Exist. Creating new one.");
            MenuItem newProNetMenu = new MenuItem();
            newProNetMenu.setLabel("ProNet");
            newProNetMenu.setBeanName("");
            newProNetMenu.setParent("");
            newProNetMenu.setUrl("");
            newProNetMenu.setOnOffShore("");
            newProNetMenu.setOnBgColor("#ffc425");
            newProNetMenu.setOffBgColor("#ffc425");
            newProNetMenu.setOnColor("#ffffff");
            newProNetMenu.setOffColor("#ffffff");
            newProNetMenu.setSelectedBgColor("#ffc425");
            newProNetMenu.setSelectedColor("#ffffff");
            newProNetMenu.setSpecificTo("");
            newProNetMenu.setSpecificToTightHole("");
            newProNetMenu.setSpecificView(ModuleConstants.WELL_MANAGEMENT);
            newProNetMenu.setDataMatrixSectionName("");
            // d2point5_demo settings
            
            MenuItem operationMenu = getMenuItem("operationController");
            if (operationMenu != null && operationMenu.getSequence() != null) {
                newProNetMenu.setSequence(operationMenu.getSequence() + 1);
                System.out.println("--- New ProNet Menu located after Menu with Operation screen [SEQ:" + newProNetMenu.getSequence() + "]");
            }
            ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newProNetMenu);
            System.out.println("-- New ProNet Menu Created.");
            proNetMenu = newProNetMenu;
        }
        this.createMenuItemOperationTypeLink(proNetMenu.getMenuItemUid(), this.shortCode);
        return proNetMenu;
    }
    
    /**
     * Hides all menus other than pre-defined for PRD operation.
     * Because D2 does not have a blacklist, we'll have to whitelist all other operationTypes OTHER than PRD to effectively 'hide it'. 
     */
    private void configMenus() throws Exception {
        System.out.println("4. Configuring Menus");
        
        this.operationTypeExclusionList = this.initializeOperationTypeExclusionList();
        this.addStandardMenusToList();
        this.hideMainMenus();
        
        for (String menuItemUid : this.secondaryMenuItemUidsToClean) {
            this.hideMenus(menuItemUid);
        }
        
        System.out.println("Configuring Menus Done.");
        System.out.println();
    }
    
    // Handles menus that have PRD menu items (so hide the rest - 2nd pass)
    private void hideMenus(String parent) throws Exception {
        System.out.println("- Hiding Menu Items in: " + parent);
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND parent=:parent AND menuItemUid NOT IN (SELECT DISTINCT menuItemUid FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"parent"}, new Object[] {parent});
        if (result != null && result.size() > 0) {
            for (MenuItem mi : result) {
                String menuItemUid = mi.getMenuItemUid();
                if (this.newMenuItemUids.contains(menuItemUid)) {
                    continue;
                } else {
                    System.out.println("-- Hiding " + mi.getLabel());
                    for (String operationCode : this.operationTypeExclusionList) {
                        this.createMenuItemOperationTypeLink(menuItemUid, operationCode);
                    }
                }
            }
        } else {
            System.out.println("- Nothing to hide!");
        }
    }
    
    // Adds standard menus to exclusion list as well as setup
    private void addStandardMenusToList() throws Exception {
        System.out.print("- Adding Standard Menus to Exclusion List:");
        for (String beanName : this.standardMenuList) {
            MenuItem mi = this.getMenuItem(beanName);
            if (mi != null) {
                String menuItemUid = mi.getMenuItemUid();
                String parentUid = mi.getParent();
                this.newMenuItemUids.add(menuItemUid);
                if (StringUtils.isNotBlank(parentUid) && !this.newMenuItemUids.contains(parentUid)) {
                    this.newMenuItemUids.add(parentUid);
                }
            }
        }
        
        // Add Setup to list
        MenuItem setupMenu = this.getMenuItem("SETUP", true);
        if (setupMenu != null) {
            this.setupMenuUid = setupMenu.getMenuItemUid();
            this.newMenuItemUids.add(this.setupMenuUid);
        }
        
        System.out.println("Done");
    }
    
    // Hide Main Menus (1st Pass Through)
    private void hideMainMenus() throws Exception {
        System.out.println("- Hiding Main Menus");
        String arrayListStr = "(";
        for (String s : this.newMenuItemUids) {
            arrayListStr += "'" + s + "',";
        }
        arrayListStr = arrayListStr.substring(0, arrayListStr.length()-1) + ")";
        
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (parent='' OR parent IS NULL) AND menuItemUid NOT IN (SELECT DISTINCT menuItemUid FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
        if (result != null && result.size() > 0) {
            for (MenuItem mi : result) {
                String menuItemUid = mi.getMenuItemUid();
                if (this.newMenuItemUids.contains(menuItemUid)) {
                    if (!StringUtils.equals(this.setupMenuUid, menuItemUid)){
                        this.secondaryMenuItemUidsToClean.add(menuItemUid);
                        System.out.println("-- Saving menu " + mi.getLabel() + " for detailed menu cleaning");
                    }
                    continue;
                } else {
                    System.out.println("-- Hiding " + mi.getLabel());
                    for (String operationCode : this.operationTypeExclusionList) {
                        this.createMenuItemOperationTypeLink(menuItemUid, operationCode);
                    }
                }
            }
        } else {
            System.out.println("- Nothing to hide!");
        }
        System.out.println("- Hiding Main Menus Done");
    }

    /* UTILITY METHODS */
    
    // Get Menu Item by Bean Name - that isn't a parent itself
    private MenuItem getMenuItem(String beanName) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName and (parent='' OR parent IS NULL OR parent IN (SELECT DISTINCT menuItemUid FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (beanName IS NULL OR beanName = '')))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName"}, new Object[] {beanName});
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    // Get menu item with label. Flag to get a parent or not.
    private MenuItem getMenuItem(String label, boolean parent) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND label LIKE :label";
        if (parent) {
            sql += " AND (beanName IS NULL OR beanName='')";
        }
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"label"}, new Object[] {label});
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    // Get Menu Item with given parent label. Null not accepted.
    private MenuItem getMenuItemWithBeanNameAndLabel(String beanName, String label, String parentLabel) throws Exception {
        String sql = "FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND beanName=:beanName and (parent='' OR parent IS NULL OR parent IN (SELECT DISTINCT menuItemUid FROM MenuItem WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (beanName IS NULL OR beanName = '')))";
        List<MenuItem> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"beanName"}, new Object[] {beanName});
        if (result != null && result.size() > 0) {
            for(MenuItem mi : result) {
                if (StringUtils.equalsIgnoreCase(label, mi.getLabel())) {
                    MenuItem parent = (MenuItem) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(MenuItem.class, mi.getParent());
                    if (parent != null && StringUtils.isNotBlank(parent.getLabel()) && StringUtils.equalsIgnoreCase(parent.getLabel(), parentLabel)) {
                        return mi;
                    }
                }
            }
        }
        return null;
    }
    
    private List<MenuItemOperationType> getMenuItemOperationTypeList(String menuItemUid) throws Exception {
        String sql = "FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND menuItemUid=:menuItemUid";
        List<MenuItemOperationType> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"menuItemUid"}, new Object[] {menuItemUid});
        if (result != null) {
            return result;
        }
        return null;
    }
    
    // Create link/whitelist between operationType and menuItem
    private void createMenuItemOperationTypeLink(String menuItemUid, String operationCode) throws Exception {
        if (StringUtils.isNotBlank(menuItemUid) && StringUtils.isNotBlank(operationCode)) {
            String sql = "FROM MenuItemOperationType WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND menuItemUid=:menuItemUid AND operationCode=:operationCode";
            List<MenuItemOperationType> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"menuItemUid","operationCode"}, new Object[] {menuItemUid, operationCode});
            if (result != null && result.size() == 0) {
                MenuItemOperationType menuOpType = new MenuItemOperationType();
                menuOpType.setMenuItemUid(menuItemUid);
                menuOpType.setOperationCode(operationCode);
                ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(menuOpType);
                //System.out.println("[MenuItemOperationType Link created between menuItemUid:" + menuItemUid + " and opeartionCode:" + operationCode + "]");
            } else {
                //System.out.println("[MenuItemOperationType Link exists between menuItemUid:" + menuItemUid + " and opeartionCode:" + operationCode + "]");
            }
        }
    }
    
    private List<String> initializeOperationTypeExclusionList() throws Exception {
        String sql = "SELECT DISTINCT shortCode FROM CommonLookup WHERE (isDeleted = false OR isDeleted is null) AND (isActive = true OR isActive is null) AND lookupTypeSelection=:lookupTypeSelection AND shortCode!='PRD'";
        List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupTypeSelection"}, new Object[] {"operationType"});
        if (result != null && result.size() > 0) {
            return result;
        }
        return null;
    }
    
    public Boolean isApplyToTown() {
        return false;
    }

}