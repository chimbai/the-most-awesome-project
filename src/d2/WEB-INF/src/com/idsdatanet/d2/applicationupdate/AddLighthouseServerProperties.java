package com.idsdatanet.d2.applicationupdate;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class AddLighthouseServerProperties extends ApplicationUpdateConfig {
	@Override
	public void run() throws Exception {
		CommonLookup commonLookup = new CommonLookup();
		commonLookup.setLookupTypeSelection("depot_type");
		commonLookup.setShortCode("lighthouse");
		commonLookup.setLookupLabel("Lighthouse");
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(commonLookup);
		
		CommonLookup lighthouseVersion = new CommonLookup();
		lighthouseVersion.setLookupTypeSelection("depot_version");
		lighthouseVersion.setShortCode("1.0");
		lighthouseVersion.setIsActive(true);
		lighthouseVersion.setParentReferenceKey("lighthouse");
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lighthouseVersion);
		
		ImportExportServerProperties serverProperties = new ImportExportServerProperties();
		serverProperties.setDataTransferType("lighthouse");
		serverProperties.setDepotVersion("1.0");
		serverProperties.setServiceName("Lighthouse API");
		serverProperties.setServiceDescr("Lighthouse - RSD Monitoring Tool");
		serverProperties.setEndPoint("http://kch-test-01.vpn.idsdatanet.com/lighthouse");
		serverProperties.setUsername("lighthouse_api");
		serverProperties.setPassword("lighthouse_api");
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(serverProperties);
	}
}
