package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketReachDataNodeListener extends EmptyDataNodeListener {
	private String lessonReportType =null;
	
	public void setLessonReportType(String value){
		this.lessonReportType = value;
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		if (node.getData() instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) node.getData();
			
			if(thisLessonTicket.getLastPushDateTime() != null) thisLessonTicket.setLastPushDateTime(null);	
		}
		if (node.getData() instanceof LessonTicketCategoryElement) {
			if(node.getParent().getData() instanceof LessonTicket) {
				LessonTicket thisParentLessonTicket = (LessonTicket) node.getParent().getData();
				if(thisParentLessonTicket.getLastPushDateTime() != null) thisParentLessonTicket.setLastPushDateTime(null);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisParentLessonTicket);
			}
				
		}
	}
	
public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + thisLessonTicket.getLessonTicketNumber());
			
		}
	}
	//ticket#27631 Carry forward the auto population logic from LessonTicketDataNodeListener to LessonTicketReachDataNodeListener in afterDataNodeSaveOrUpdate
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				
				//auto populate case no 20100924-0539-slfu 
				//String newreportnumber = null;
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoNewLessonTicketNumber"))){
					
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					String prefixPattern = GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberPrefixPattern");
					
					String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
					
					if (StringUtils.isNotBlank(getPrefixName)){
						thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
					}
					//ticket#26804 gwp lessonTicketNumberByWWO select lesson ticket largest number by well/wellbore/operation based
					List list = null;
					String newreportnumber = null;
					String strCondition = null;
					if (this.lessonReportType == null){
						strCondition = " And (lessonReportType IS NULL OR lessonReportType = '')";
					}
					else{
						strCondition = " And lessonReportType = 'geo_lesson'";
					}
					
					if ("well".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
							list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellUid = :wellUid"+strCondition+" order by lessonTicketNumber DESC", "wellUid", userSelection.getWellUid());
					}
					if ("wellbore".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid"+strCondition+" order by lessonTicketNumber DESC", "wellboreUid", userSelection.getWellboreUid());
					}
					if ("operation".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and operationUid = :operationUid"+strCondition+" order by lessonTicketNumber DESC", "operationUid", userSelection.getOperationUid());
					}
					
					
					if (list.size() > 0) {
						Collections.sort(list, new LessonTicketNumberComparator());
						String lessonTicketNumber = null;
						Object a = list.get(0);
						if (a!=null) lessonTicketNumber = a.toString();
							
						if (StringUtils.isNotBlank(lessonTicketNumber) && StringUtils.isNumeric(lessonTicketNumber)) {
							Integer intEventRef = (Integer)Integer.parseInt(lessonTicketNumber) + 1;
					
							newreportnumber = intEventRef.toString();
							
							if (newreportnumber.length()== 1){
								newreportnumber = "0" + newreportnumber;
							}
						}else {
							newreportnumber = "01";
						}

					}else {
						newreportnumber = "01";
					}
					thisLessonTicket.setLessonTicketNumber(newreportnumber);
					thisLessonTicket.setLessonTicketType(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
					
				}
				
			}
		}
	
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	private class LessonTicketNumberComparator implements Comparator<String>{
		public int compare(String s1, String s2){
			try{
				String f1 = WellNameUtil.getPaddedStr(s1.toString());
				String f2 = WellNameUtil.getPaddedStr(s2.toString());
				
				if (f1 == null || f2 == null) return 0;
				//refine the ticket# sorting order, numeric(descending) first then alphanumeric(descending)
				if (StringUtils.isBlank(f1.toString()) || StringUtils.isBlank(f2.toString())) return 0;
				if (!StringUtils.isNumeric(f1.toString()) && StringUtils.isAlphanumeric(f1.toString()) && StringUtils.isNumeric(f2.toString())) return 1;
				if (!StringUtils.isNumeric(f2.toString()) && StringUtils.isAlphanumeric(f2.toString()) && StringUtils.isNumeric(f1.toString())) return -1;
				
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}

	}
	
}
