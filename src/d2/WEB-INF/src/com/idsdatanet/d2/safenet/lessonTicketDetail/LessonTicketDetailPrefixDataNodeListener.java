package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class LessonTicketDetailPrefixDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	private String lessonReportType = null;
	
	public void setLessonReportType(String lessonReportType) {
		this.lessonReportType = lessonReportType;
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			String lessonTicketPrefix = "";
			
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("plan")) {
				node.getDynaAttr().put("lessonTicketNumber", "PLL - " + lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}else if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("non_op")) {
				node.getDynaAttr().put("lessonTicketNumber", "Non-Op - " + lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}else {
				node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}
		}
	}

	@Override
	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
				
		if (meta.getTableClass().equals(LessonTicket.class)) {
			
			List<String> arrayParamNames = new ArrayList<String>();
			List<Object> arrayParamValues = new ArrayList<Object>();
			
			String strSql = "FROM LessonTicket WHERE operationUid = :operationUid AND (isDeleted IS NULL OR isDeleted = FALSE)";
			if(StringUtils.isNotBlank(lessonReportType)) {
				strSql += " AND lessonReportType = :lessonReportType";
				arrayParamNames.add("lessonReportType");
				arrayParamValues.add(this.lessonReportType);
			}else {
				strSql += " AND (lessonReportType IS NULL OR lessonReportType = '')";
			}
			
			arrayParamNames.add("operationUid");
			arrayParamValues.add(userSelection.getOperationUid());
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<LessonTicket> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new LessonTicketComparator());
			
			for(Object objResult: items){
				LessonTicket thisLessonTicket = (LessonTicket) objResult;
				output_maps.add(thisLessonTicket);
			}
						
			return output_maps;
		}
		
		if(meta.getTableClass().equals(LessonTicketCategoryElement.class)){		
			Object object = node.getData();
			
			if(object instanceof LessonTicket){
				LessonTicket thisLessonTicket = (LessonTicket)object;
				
				String strSql = "FROM LessonTicketCategoryElement ltce, LookupLessonTicketCategory lltc " +
								"WHERE ltce.lessonTicketUid = :lessonTicketUid " +
								"AND (ltce.isDeleted is FALSE OR ltce.isDeleted IS NULL) " +
								//"AND (lltc.isDeleted is FALSE OR lltc.isDeleted IS NULL) " +
								"AND ltce.lessonCategory = lltc.lookupLessonTicketCategoryUid " +
								"ORDER BY lltc.sequence ASC";
				List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lessonTicketUid", thisLessonTicket.getLessonTicketUid());
				
				List<Object> output_maps = new ArrayList<Object>();
				
				for(Object[] objResult: items){
					LessonTicketCategoryElement thisLessonTicketCategoryElement = (LessonTicketCategoryElement) objResult[0];
					output_maps.add(thisLessonTicketCategoryElement);
				}
							
				return output_maps;
			}
		}
		return null;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return true;
	}
	
	private class LessonTicketComparator implements Comparator<LessonTicket>{
		public int compare(LessonTicket o1, LessonTicket o2){
			try {
				String p1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumberPrefix());
				String p2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumberPrefix());
				
				String n1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumber());
				String n2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumber());
				
				String ltNumber1 = p1 + " - " + n1;
				String ltNumber2 = p2 + " - " + n2;

				if(o1.getRp2Date() == null || o2.getRp2Date() == null || o1.getRp2Date().compareTo(o2.getRp2Date()) == 0) {
					if(ltNumber1 == null || ltNumber2 == null) {
						return 0;
					}else if(ltNumber1.compareTo(ltNumber2) == 0) {
						return 0;
					}else if(ltNumber1.compareTo(ltNumber2) > 0) {
						return -1;
					}else if(ltNumber1.compareTo(ltNumber2) < 0) {
						return 1;
					}
				}else if(o1.getRp2Date().compareTo(o2.getRp2Date()) > 0) {
					return -1;
				}else if(o1.getRp2Date().compareTo(o2.getRp2Date()) < 0){
					return 1;
				}
				return 0;

			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}
