package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import com.idsdatanet.d2.core.web.mvc.BaseFormController;

public class LessonTicketDetailPopUpController extends BaseFormController {
	
	public static final String SEARCH_VIEW_MODE = "searchViewMode";
	public static final String DIFFERENT_GROUP = "differentGroup";
	public static final String LESSON_REPORT_TYPE = "lessonReportType";
	private Map<String, BaseFormController> controllersByLessonReportType = null;
	private BaseFormController defaultLessonTicketDetailController = null;
	
	public void setDefaultLessonTicketDetailController(BaseFormController defaultLessonTicketDetailController) {
		this.defaultLessonTicketDetailController = defaultLessonTicketDetailController;
	}

	public void setControllersByLessonReportType(Map<String, BaseFormController> controllersByLessonReportType) {
		this.controllersByLessonReportType = controllersByLessonReportType;
	}

	public LessonTicketDetailPopUpController() {
		super();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if(this.defaultLessonTicketDetailController == null) {
			throw new Exception("defaultLessonTicketDetailController is not set");
		}
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		
		arg0.setAttribute(SEARCH_VIEW_MODE, "true");
		
		String lessonReportType = (arg0.getParameter("lessonReportType") == null ? "" : arg0.getParameter("lessonReportType"));
		if(StringUtils.isNotBlank(lessonReportType) && controllersByLessonReportType.containsKey(lessonReportType)) {
			return controllersByLessonReportType.get(lessonReportType).handleRequest(arg0, arg1);
		}
		
		return defaultLessonTicketDetailController.handleRequest(arg0, arg1);
	}
}
