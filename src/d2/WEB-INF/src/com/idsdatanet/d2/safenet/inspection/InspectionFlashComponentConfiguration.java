package com.idsdatanet.d2.safenet.inspection;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class InspectionFlashComponentConfiguration extends AbstractFlashComponentConfiguration {

	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = "../../";
		String inspectionType = request.getParameter("type");
		String groupCheckType = request.getParameter("groupCheckType");
		if (groupCheckType==null) groupCheckType = "";
		return "d2Url=" + urlEncode(serviceUrl) +"&type="+urlEncode(inspectionType)+"&groupCheckType="+urlEncode(groupCheckType);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "inspection";
	}

	@Override
	public String getAdditionalHtmlHeaderContent(String targetSubModule, UserSession userSession, HttpServletRequest request){
		return "<script language=\"JavaScript\" type=\"text/javascript\">" +
		"function reloadParentHtmlPage(clearQueryString){parent.d2ScreenManager.reloadParentHtmlPage(clearQueryString);}" +
		"function setBlockUI(block){" +
		"  if(block){"+
		"    parent.d2ScreenManager.toggleMainBlockUI();" +
		"  }else{" +
		"    parent.d2ScreenManager.toggleMainUnblockUI();" +
		"  }" +
		"}" +
		"</script>";
	}
}
