package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DDRReportListener implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(LessonTicket.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_custom_condition_}", "");
			}
						
			String thisFilter = "";
			
			//GET TODAY'S DATE
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(daily.getDayDate());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
					
			if (conditionClause.indexOf("{_custom_condition_}") != -1)
			{
				query.addParam("todayDate", calendar.getTime());
				thisFilter = "rp2Date <= :todayDate and (lessonStatus = '' or lessonStatus is null)";
			}
			
			return conditionClause.replace("{_custom_condition_}", thisFilter);
		}else{
			return null;
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}
