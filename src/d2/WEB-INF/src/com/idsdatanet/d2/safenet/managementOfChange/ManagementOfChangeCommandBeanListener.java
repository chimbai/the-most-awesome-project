package com.idsdatanet.d2.safenet.managementOfChange;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.MocDisp;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

public class ManagementOfChangeCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener ,InitializingBean{

	private MailEngine mailEngine = null;
	private String support_email = null;
	//private String sendNewMatchingLessonTicketTemplate = null;
	//private String sendUpdatedMatchingLessonTicketTemplate = null;
	private String sendNotificationToResponsiblePartyEmailTemplate = null;
	//private String lessonReportType = null;
	

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		Map<String,CommandBeanTreeNode> md = commandBean.getRoot().getChild("MocDisp");
		Manager dao = ApplicationUtils.getConfiguredInstance().getDaoManager();
		if(md != null){	
			for(Map.Entry<String,CommandBeanTreeNode> i : md.entrySet()){
				CommandBeanTreeNode mdNode = i.getValue();
				MocDisp mdRec = (MocDisp) mdNode.getData();
				String sql = "select mocDispNumber FROM MocDisp where (isDeleted is NULL and isDeleted = 0) and mocDispUid =: mocDispUid";
				/*
				 * String sql = "select ltce.lessonCategory, ltce.lessonElement, " + "" +
				 * "(select lc.lessonTicketCategory from LookupLessonTicketCategory lc where (lc.isDeleted = false or lc.isDeleted is null) and lc.lookupLessonTicketCategoryUid = ltce.lessonCategory) as lessonCategoryLabel, "
				 * +
				 * "(select le.lessonTicketElements from LookupLessonTicketElements le where (le.isDeleted = false or le.isDeleted is null) and le.lookupLessonTicketElementsUid = ltce.lessonElement) as lessonElementLabel "
				 * +
				 * "from LessonTicketCategoryElement ltce where (ltce.isDeleted is null or ltce.isDeleted = false) "
				 * + "and ltce.lessonTicketUid = :lessonTicketUid";
				 */
				
				List<Object[]> listMocDisp = dao.findByNamedParam(sql, "mocDispUid", mdRec.getMocDispUid());
				if(listMocDisp != null){
					//String contatenatedCategoryElements = null;
					/*
					 * for(Object[] ltce : listMocDisp){ Object lessonCategoryLabel = ltce[2];
					 * Object lessonElementLabel = ltce[3]; if(lessonCategoryLabel != null &&
					 * lessonElementLabel != null){ if(contatenatedCategoryElements == null){
					 * contatenatedCategoryElements = lessonCategoryLabel.toString() + " : " +
					 * lessonElementLabel.toString(); }else{ contatenatedCategoryElements =
					 * contatenatedCategoryElements + "\n" + lessonCategoryLabel.toString() + " : "
					 * + lessonElementLabel.toString(); } } }
					 */
					//mdRec.setCategoryElementLabels(contatenatedCategoryElements);
					dao.saveObject(mdRec);
				}
			}
		}
	}

	public String getSendNotificationToResponsiblePartyEmailTemplate() {
		return sendNotificationToResponsiblePartyEmailTemplate;
	}

	public void setSendNotificationToResponsiblePartyEmailTemplate(
			String sendNotificationToResponsiblePartyEmailTemplate) {
		this.sendNotificationToResponsiblePartyEmailTemplate = sendNotificationToResponsiblePartyEmailTemplate;
	}

	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(MocDisp.class)) {
				MocDisp mocDisp = (MocDisp)targetCommandBeanTreeNode.getData();
				if("sendEmailNotification".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					this.sendEmailNotification(mocDisp.getMocDispUid(), (BaseCommandBean)commandBean, request);
				}
			}
		}
	}
	
	public void afterPropertiesSet() throws Exception
	{
		if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
		this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isBlank(support_email)) this.support_email = "support@idsdatanet.com";
	}
	
	
	/**
	 * Send Email Notification to Responsible Party upon clicking "Send" button
	 * @param lessonTicketUid
	 * @param commandBean
	 * @param request 
	 * @throws Exception
	 */
	private void sendEmailNotification(String mocDispUid, BaseCommandBean commandBean, HttpServletRequest request) throws Exception
	{
		MocDisp thisMocDisp = (MocDisp) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(MocDisp.class, mocDispUid);
		String[] emailList=null;
		String hyperLink = null;
		String content = null;
		
		if(!StringUtils.isBlank(thisMocDisp.getEmailReceipient())){

			String input = thisMocDisp.getEmailReceipient();
			if (!StringUtils.isBlank(input)){
				emailList=input.split(",");
			}
			
			String mocDispNumberString = thisMocDisp.getMocDispNumberPrefix();
			String emailSubject = "System Message - Management of Change (MOC #: MOC-" + mocDispNumberString + ")";
			
			String url = request.getParameter("baseUrl");
			//String url = UserSession.getInstance(request).getClientBaseUrl();
			hyperLink = url + "?gotowellop=" + thisMocDisp.getOperationUid() + "&amp;gotoFieldName=mocDispUid&amp;gotoFieldUid=" + thisMocDisp.getMocDispUid();
		
			//call vars in email template
			Map<String, Object> md = new HashMap<String, Object>();
			md.put("mocDispUid", thisMocDisp.getMocDispUid());
			md.put("mocDispNumber", mocDispNumberString);
			md.put("operationUid", thisMocDisp.getOperationUid());
			md.put("hyperLink", hyperLink);
			
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("mocDisp", md);
			
			if(StringUtils.isBlank(this.sendNotificationToResponsiblePartyEmailTemplate)) {
				commandBean.getSystemMessage().addError("Unable to send");
			} else {
				mailEngine.sendMail(new String[]{thisMocDisp.getEmailReceipient()}, emailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendNotificationToResponsiblePartyEmailTemplate, params));
				commandBean.getSystemMessage().addInfo("MOC " + mocDispNumberString + " has been successfully sent");
			}
		}
	}
	
	/**
	 * Get email list with distribution key
	 * @param groupUid
	 * @param distributionKey
	 * @return
	 * @throws Exception
	 */
	private String[] getEmailList(String groupUid, String distributionKey) throws Exception {
		
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(groupUid, distributionKey);
		return emailList;
	}
	

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}
	
}
