package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LookupLessonTicketCategory;
import com.idsdatanet.d2.core.model.LookupLessonTicketElements;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LessonTicketCategoryElementLookupHandler implements CascadeLookupHandler {

	@Override
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		return getLessonTicketElementLookupItems(null);
	}

	@Override
	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {	
		String sql = "From LookupLessonTicketCategory where (isDeleted is null or isDeleted = false) and (isActive is null or isActive = true)";
		List<LookupLessonTicketCategory> ltCategories = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		CascadeLookupSet[] completeCascadeLookups = null;
		
		if(ltCategories != null){
			List<CascadeLookupSet> cascadeLookupList = new ArrayList();
			for(LookupLessonTicketCategory ltCategory : ltCategories){
				String ltCategorylookupLessonTicketCategoryUid = ltCategory.getLookupLessonTicketCategoryUid();
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(ltCategorylookupLessonTicketCategoryUid);
				LinkedHashMap<String, LookupItem> ltElementLookups = (LinkedHashMap<String, LookupItem>) this.getCascadeLookup(commandBean, null, userSelection, request, ltCategorylookupLessonTicketCategoryUid, null);
				cascadeLookupSet.setLookup(ltElementLookups);
				cascadeLookupList.add(cascadeLookupSet);
			}
			completeCascadeLookups = new CascadeLookupSet[cascadeLookupList.size()];
			completeCascadeLookups = cascadeLookupList.toArray(completeCascadeLookups);
		}
		
		return completeCascadeLookups;
	}

	@Override
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {

		return getLessonTicketElementLookupItems(key);
	}
	
	private LinkedHashMap<String, LookupItem> getLessonTicketElementLookupItems(String key) throws Exception{
		String sql = "From LookupLessonTicketElements where (isDeleted is null or isDeleted = false) ";
		List<String> param = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(key)){
			sql += "and lookupLessonTicketCategoryUid = :lookupLessonTicketCategoryUid";
			param.add("lookupLessonTicketCategoryUid");
			paramValue.add(key);
		}

		List<LookupLessonTicketElements> ltElements = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, param, paramValue);
		LinkedHashMap<String, LookupItem> elementLookups = null;
		
		if(ltElements != null){
			elementLookups = new LinkedHashMap<String, LookupItem>();
			for(LookupLessonTicketElements ltElement : ltElements){
				String lookupLessonTicketElementUid = ltElement.getLookupLessonTicketElementsUid();
				LookupItem ltElementLookup = new LookupItem(lookupLessonTicketElementUid, ltElement.getLessonTicketElements());
				elementLookups.put(ltElement.getLookupLessonTicketElementsUid(), ltElementLookup);
			}
			elementLookups = this.sortLookupMapByLookupValue(elementLookups, new LessonTicketElementsComparator());
		}
		
		return elementLookups;
	}
	
	
	private LinkedHashMap<String, LookupItem> sortLookupMapByLookupValue(LinkedHashMap<String, LookupItem> unsortedMap, Comparator comparator){
		LinkedHashMap<String, LookupItem> sortedMap = null;
		if(unsortedMap != null){
			List<Map.Entry<String, LookupItem>> unsortedList = new LinkedList<Map.Entry<String, LookupItem>>(unsortedMap.entrySet());
			Collections.sort(unsortedList, comparator);
			sortedMap = new LinkedHashMap<String, LookupItem>();
			for(Map.Entry<String, LookupItem> lookup : unsortedList){
				sortedMap.put(lookup.getKey(), lookup.getValue());
			}
		}
		return sortedMap;
	}

	private class LessonTicketElementsComparator implements Comparator<Map.Entry<String, LookupItem>>{
		public int compare(Map.Entry<String, LookupItem> o1, Map.Entry<String, LookupItem> o2){
			try{
				// Need to trim the empty space before compare
				// Example f1: 6 5/8" 
				// Example f2: 6"
				//
				// After Padding:
				// f1: 0000000006 0000000005/0000000008"
				// f2: 0000000006"
				//
				// compareTo will return positive number when compare empty space with " character
				//
				String f1 = WellNameUtil.getPaddedStr(o1.getValue().getValue().toString().toLowerCase()).replaceAll(" ", "");
				String f2 = WellNameUtil.getPaddedStr(o2.getValue().getValue().toString().toLowerCase()).replaceAll(" ", "");
				
				if (f1 == null || f2 == null) return 0;
				// String compareTo - return negative integer for precedence position and positive integer for follow position
				// 1. compareTo compares both strings character by character (charAt(k) of f1 - charAt(k) of f2) up till minimum length from 2 strings. 
				// 2. If both strings are same up till minimum length, compareTo return the difference of string length (f1.length() - f2.length())
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
   


	
	
}
