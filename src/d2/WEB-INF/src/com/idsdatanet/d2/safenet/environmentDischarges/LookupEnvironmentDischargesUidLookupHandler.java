package com.idsdatanet.d2.safenet.environmentDischarges;

import java.net.URLDecoder;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LookupEnvironmentDischargesUidLookupHandler implements LookupHandler {
	protected String table = null;
	protected String key = null;
	protected String value = null;
	protected String format = null;
	protected String cache = null;
	protected String order = null;
	protected String filter = null;
	protected String[] filterProperties = null;
	protected String[] extraProperties = null;
	protected String depotkey = null;
	protected String active = "isActive";
	protected String parent = "operationCode";
	protected String tableUid = null;

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCache() {
		return cache;
	}

	public void setCache(String cache) {
		this.cache = cache;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
	public String getTableUid() {
		return tableUid;
	}

	public void setTableUid(String tableUid) {
		this.tableUid = tableUid;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		if (StringUtils.isBlank(this.table)) throw new Exception("Table name required");
		if (StringUtils.isBlank(this.key)) throw new Exception("Lookup key required");
		
		String queryString = "FROM " + this.table + " " +
				" WHERE (isDeleted=false or isDeleted is null) " +
				" AND (" + this.filter + ")" +
				" ORDER BY " + this.order;
		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
		
		return this.populateLookupMap(list);
	}
	
	private Map<String, LookupItem> populateLookupMap(List list) throws Exception {
		LinkedHashMap<String, Object> tempList = new LinkedHashMap<String, Object>();
		for (Object object : list) {
			Map item = PropertyUtils.describe(object);
			String key = (String) item.get(this.key);
			Boolean isActive = (Boolean) item.get(this.active);
			if (isActive==null) isActive = true;
			
			if (tempList.containsKey(key)) {
				if(isActive){
					tempList.put(key, object);
				}
			} else {
				tempList.put(key, object);
			}
		}
		
		List aList = new LinkedList(); //do sorting if order is set
		for (Map.Entry<String, Object> entry : tempList.entrySet()) aList.add(entry.getValue());
		if (StringUtils.isNotBlank(this.order)) {
			LookupItemComparator comparator = new LookupItemComparator();
			comparator.order = this.order;
			Collections.sort(aList, comparator);
		}		
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		for (Object object : aList) {
			Map item = PropertyUtils.describe(object);
			String key = (String) item.get(this.key);

			String value_data = "";
			if (StringUtils.isBlank(this.value)) this.value = this.key;
			Object[] values = this.value.split(",");
			for (int i=0; i<values.length; i++) {
				String fieldName = (String)values[i];
				if (item.containsKey(fieldName.trim())) {
					values[i] = CommonUtils.null2EmptyString(item.get(values[i]));
					value_data += (StringUtils.isNotBlank(value_data)?" ":"") + values[i];
				} else {
					values[i] = "";
				}
			}
			
			Boolean isActive = (Boolean) item.get(this.active);
			LookupItem lookupItem = new LookupItem(key, value_data);
			lookupItem.setActive(isActive);
			
			if (StringUtils.isNotBlank(tableUid)) {
				lookupItem.setTableUid(BeanUtils.getProperty(object, tableUid));
			}
			
			result.put(lookupItem.getKey(), lookupItem);
		}

		return result;
	}
	
	private class LookupItemComparator implements Comparator<Object> {
		public String order = null;
		public int compare(Object o1, Object o2) {
			try {
				String[] orderby = order.split(",");
				Map object1 = PropertyUtils.describe(o1);
				Map object2 = PropertyUtils.describe(o2);
				String compareString1 = "";
				String compareString2 = "";
				for (String orderField : orderby) {
					orderField = orderField.trim();
					if (object1.containsKey(orderField)) compareString1 += WellNameUtil.getPaddedStr(CommonUtils.null2EmptyString(object1.get(orderField)));
					if (object2.containsKey(orderField)) compareString2 += WellNameUtil.getPaddedStr(CommonUtils.null2EmptyString(object2.get(orderField)));
				}
				return compareString1.compareTo(compareString2);
			} catch (Exception e) {
				return 0;
			}
		}
	}
}