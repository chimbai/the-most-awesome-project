package com.idsdatanet.d2.safenet.hsePlan;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.HsePlan;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HsePlanDataNodeListener extends EmptyDataNodeListener {
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof HsePlan){
			HsePlan hseplan = (HsePlan) object;
			
			if (StringUtils.isNotBlank(userSelection.getRigInformationUid())) {
				hseplan.setRigInformationUid(userSelection.getRigInformationUid());
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof HsePlan){
			HsePlan hseplan = (HsePlan) object;
			if (hseplan.getDateEnd()!= null) { 
				if(hseplan.getDateEnd().before(hseplan.getDateStart())){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "dateEnd", "Effective end date can not be earlier than start time.");
					return;
				}
			}
		}
	}

}
