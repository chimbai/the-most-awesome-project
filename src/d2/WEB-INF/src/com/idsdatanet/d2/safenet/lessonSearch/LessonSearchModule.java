package com.idsdatanet.d2.safenet.lessonSearch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.UserLessonSearch;
import com.idsdatanet.d2.core.model.UserSearchResults;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.query.representation.DatabaseQuery;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportUtils;

public class LessonSearchModule extends DefaultReportModule {
	
	private String dateFormat = "yyyyMMdd";
	private String timeFormat = "kkmmss";
	private String reportDateFormat = "ddMMyy";
	private String reportTimeFormat = "hhmm";
	private List<LessonTicket> filteredResult = new ArrayList<LessonTicket>();
	private String lessonTicketPage = "lessonticketdetail.html";
	
	public void setlessonTicketPage(String lessonTicketPage) {
		this.lessonTicketPage = lessonTicketPage;
	}
	
	/**
	 * Submission handler for the main page
	 * @param request
	 * @param response
	 * @param commandBean
	 * @param invocationKey
	 * @throws Exception
	 */
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexLessonPullReportLessonList".equals(invocationKey)) {
			this.customFilterInvokedLessonList(request, response, commandBean, invocationKey);
		} else if ("lessonTicketCategoryElementList".equals(invocationKey)) {
			this.customFilterInvokedCategoryElementList(request, response, commandBean, invocationKey);
		} else if ("flexLessonPullReportLessonSearchResult".equals(invocationKey)) {
			this.customFilterInvokedUserSearchResult(request, response, commandBean, invocationKey);
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		super.afterDataLoaded(commandBean, root, userSelection, request);
		
		UserSession session = UserSession.getInstance(request);
		if ("lessonSearchResultCommandBean".equalsIgnoreCase(commandBean.getBeanName())) {
			List<UserSearchResults> lstUserSearchResults = listAllSearchedResultsFromSpecificUser(session.getUserUid());
			if (lstUserSearchResults.size() <= 0) {
				commandBean.getSystemMessage().addInfo("No search results available. To begin a search, click on the Search tab.", true);
			}
		}
		String ulsSql = "SELECT searchCriteria FROM UserLessonSearch WHERE (isDeleted is null OR isDeleted=false) and userUid=:lastEditUserUid AND searchType='Simple'";
		List ulsResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ulsSql, 
				new String[] {"lastEditUserUid"}, 
				new Object[] {userSelection.getUserUid()});
		if (ulsResult.size() > 0){
			Object b = (Object) ulsResult.get(0);
			if (b != null) {
				root.getDynaAttr().put("keywordsFilter", b.toString());
			}
		} else {
			root.getDynaAttr().put("keywordsFilter", "");
		}
	}
	
	@Override
	protected String generateReportDownloadFilePath(ReportFiles reportFiles) throws Exception{
		
		String path = null;
		
		if(reportFiles != null) {
			path = "LL_" + this.formatDate(new Date(), this.reportDateFormat) + "_" + this.formatDate(new Date(), this.reportTimeFormat) + "." + this.getOutputFileExtension() + "?handlerId=ddr&reportId=" + reportFiles.getReportFilesUid();
			return path;
		}
		return null;		
	}
	
	private void customFilterInvokedUserSearchResult(HttpServletRequest request, HttpServletResponse response,
			BaseCommandBean commandBean, String invocationKey) throws Exception {
		
		UserSession session = UserSession.getInstance(request);
		SimpleDateFormat dateFormat = ApplicationConfig.getConfiguredInstance().getReportDateFormater();
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		String sql = "SELECT lt FROM UserSearchResults usr, LessonTicket lt WHERE usr.lessonTicketUid = lt.lessonTicketUid " +
				"AND (lt.isDeleted is null or lt.isDeleted=false) AND (usr.isDeleted is null or usr.isDeleted=false) " +
				"AND usr.lastEditUserUid=:lastEditUserUid";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, 
				new String[] {"lastEditUserUid"}, 
				new Object[] {session.getCurrentUser().getUserUid()});
		for (Object objLessonTicket : list) {
			LessonTicket thisLessonTicket = (LessonTicket) objLessonTicket;
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			writer.startElement("lessonTicket");
			writer.addElement("lessonTicketUid", thisLessonTicket.getLessonTicketUid());
			writer.addElement("lessonTicketNumber", lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			writer.addElement("lessonTicketNumberPaddedString", WellNameUtil.getPaddedStr(lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber()));
			writer.addElement("operationUid", thisLessonTicket.getOperationUid());
			writer.addElement("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(session.getCurrentGroupUid(), thisLessonTicket.getOperationUid()));
			writer.addElement("rp2Date", thisLessonTicket.getRp2Date()!=null ? dateFormat.format(thisLessonTicket.getRp2Date()) : "");
			writer.addElement("contactPerson", thisLessonTicket.getReportedbyName());
			writer.addElement("descrLesson", thisLessonTicket.getDescrLesson());
			writer.addElement("investigationFinding", thisLessonTicket.getInvestigationFinding());
			writer.addElement("descrPostEvent", thisLessonTicket.getDescrPostevent());
			writer.addElement("lessonTicketPage", lessonTicketPage);
			if (thisLessonTicket.getEventLocation() != null){
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisLessonTicket.getEventLocation());
				if (well != null){
					writer.addElement("eventLocation", well.getWellName());
				}else{
					writer.addElement("eventLocation", thisLessonTicket.getEventLocation());
				}
			}
			
			if (thisLessonTicket.getCampaignName() != null){
				Campaign campaign = (Campaign) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Campaign.class, thisLessonTicket.getCampaignName());
				if (campaign != null){
					writer.addElement("campaignName", campaign.getCampaignName());
				}else{
					writer.addElement("campaignName", thisLessonTicket.getCampaignName());
				}
			}
			writer.endElement();
		}
		writer.endElement();
		writer.close();		
	}

	/**
	 * Load category and element for advance search filter
	 * @param request
	 * @param response
	 * @param commandBean
	 * @param invocationKey
	 * @throws Exception
	 */
	private void customFilterInvokedCategoryElementList(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception
	{
		UserSession session = UserSession.getInstance(request);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		
		List<String> criteriaList = new ArrayList<String>();
		String ulsSql = "SELECT searchCriteria FROM UserLessonSearch WHERE (isDeleted is null OR isDeleted=false) and userUid=:lastEditUserUid AND searchType='Advanced'";
		List elementListLstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ulsSql, 
				new String[] {"lastEditUserUid"}, 
				new Object[] {session.getCurrentUser().getUserUid()});
		if (elementListLstResult.size() > 0){
			Object b = (Object) elementListLstResult.get(0);
			if (b != null) {
				for (String elementUid : b.toString().split(",")) {
					criteriaList.add(elementUid);
				}
			}
		}
		
		String sql = "SELECT a.lessonTicketCategory, a.lookupLessonTicketCategoryUid FROM LookupLessonTicketCategory a " +
					 "WHERE (a.isDeleted is null OR a.isDeleted=false) AND (isActive IS NULL or isActive = true)" +
					 "ORDER BY a.sequence";
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		
		writer.startElement("root");
		
		for (Object[] rec : lstResult) {
			
			SimpleAttributes att = new SimpleAttributes();
			att.addAttribute("name", rec[0].toString());
			att.addAttribute("uid", rec[1].toString());
			writer.startElement("category", att);
			
			String sql2 = "SELECT b.lessonTicketElements, b.lookupLessonTicketElementsUid FROM LookupLessonTicketElements b " +
						 "WHERE (b.isDeleted is null OR b.isDeleted=false) AND b.lookupLessonTicketCategoryUid ='"+ rec[1].toString() +"' " +
						 "ORDER BY b.sequence";
			List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql2);
			for (Object[] rec2 : lstResult2) {
				SimpleAttributes elementAtt = new SimpleAttributes();
				elementAtt.addAttribute("name", rec2[0].toString());
				elementAtt.addAttribute("uid", rec2[1].toString());
				if(criteriaList.contains(rec2[1].toString())) {
					elementAtt.addAttribute("selected", "1");
				}
				writer.addElement("element", "", elementAtt);
			}
			writer.endElement();
		}
		writer.endElement();
		writer.close();
	}
	
	/**
	 * custom filter to load lesson ticket
	 * @param request
	 * @param response
	 * @param commandBean
	 * @param invocationKey
	 * @throws Exception
	 */
	private void customFilterInvokedLessonList(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception
	{
		UserSession session = UserSession.getInstance(request);
		
		String searchMethod = (String) request.getParameter("searchMethod");
		if(StringUtils.isNotBlank(searchMethod)) {
			searchMethod = searchMethod.substring(0, searchMethod.indexOf('&'));
		}
		
		String keywordsFilter  = (String) request.getParameter("keywordsFilter");
		String keywordsQuery = null;
		String selectedTicket = "";
		String advancedSearchString = (String) request.getParameter("advancedSearchString");
		String selectedElements = (String) request.getParameter("selectedElements");
		String storedCriteria = null;
		filteredResult.clear();
		if (StringUtils.isNotBlank(advancedSearchString) && "Advanced".equals(searchMethod)) {
			run(advancedSearchString);
			storedCriteria = selectedElements;
		} else if (StringUtils.isNotBlank(keywordsFilter) && "Simple".equals(searchMethod)) {
			storedCriteria = keywordsFilter;
			
			String lessonTicketList = this.getElementKeywordSearch(keywordsFilter);
			
			keywordsQuery = "AND (l.lessonTicketNumber like :keywordsFilter OR l.lessonType like :keywordsFilter OR l.rp2Date like :keywordsFilter " +
					"OR l.rp2Closeoutdate like :keywordsFilter OR l.duration like :keywordsFilter OR l.approxValue like :keywordsFilter " +
					"OR l.priority like :keywordsFilter OR l.reportedbyName like :keywordsFilter OR l.descrEvent like :keywordsFilter OR l.eventLocation like :keywordsFilter " +
					"OR (l.companies IN (SELECT a.lookupCompanyUid FROM LookupCompany a WHERE (a.isDeleted IS NULL or a.isDeleted = false) AND a.companyName like :keywordsFilter) OR l.companies like :keywordsFilter)" +
					((StringUtils.isNotBlank(lessonTicketList))? " OR l.lessonTicketUid IN (" +lessonTicketList+")":"") +
					"OR l.descrLesson like :keywordsFilter OR l.investigationFinding like :keywordsFilter OR l.descrPostevent like :keywordsFilter OR l.rp2Actionby like :keywordsFilter)";
		}
		
		if (filteredResult.size() > 0) {
			for (LessonTicket lt : filteredResult) {
				if (StringUtils.isNotBlank(selectedTicket)) selectedTicket += ",";
				selectedTicket += "'" +lt.getLessonTicketUid()+"'";
			}
		}
		
		if (StringUtils.isBlank(keywordsFilter)) {
			keywordsFilter = "";
		} else {
			//search for specify lesson ticket where lesson category and element containing keywords
//			String lessonTicketList = this.getElementKeywordSearch(keywordsFilter);
//			
//			keywordsQuery = "AND (l.lessonTicketNumber like :keywordsFilter OR l.lessonType like :keywordsFilter OR l.rp2Date like :keywordsFilter " +
//					"OR l.rp2Closeoutdate like :keywordsFilter OR l.duration like :keywordsFilter OR l.approxValue like :keywordsFilter " +
//					"OR l.priority like :keywordsFilter OR l.reportedbyName like :keywordsFilter OR l.descrEvent like :keywordsFilter OR l.eventLocation like :keywordsFilter " +
//					"OR (l.companies IN (SELECT a.lookupCompanyUid FROM LookupCompany a WHERE (a.isDeleted IS NULL or a.isDeleted = false) AND a.companyName like :keywordsFilter) OR l.companies like :keywordsFilter)" +
//					((StringUtils.isNotBlank(lessonTicketList))? " OR l.lessonTicketUid IN (" +lessonTicketList+")":"") +
//					"OR l.descrLesson like :keywordsFilter OR l.descrPostevent like :keywordsFilter OR l.rp2Actionby like :keywordsFilter)";
		}
		
		String allOperationsNotAssignedToOpsTeam = "o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE " + 
				"e.opsTeamUid = a.opsTeamUid AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR ";
		if (BooleanUtils.isTrue(session.getCurrentUser().getAccessToOpsTeamOperationsOnly())) {
			allOperationsNotAssignedToOpsTeam = "";
		}
		
		//Clear user search results
		List<UserSearchResults> lstUserSearchResults = listAllSearchedResultsFromSpecificUser(session.getUserUid());
		for(UserSearchResults usr: lstUserSearchResults){
			ApplicationUtils.getConfiguredInstance().getDaoManager().delete(usr);
		}
		//Clear user search criteria
		List<UserLessonSearch> lstUserLessonSearch = listAllSearchedCriteriaFromSpecificUser(session.getUserUid());
		for(UserLessonSearch uls: lstUserLessonSearch){
			ApplicationUtils.getConfiguredInstance().getDaoManager().delete(uls);
		}
		//Store user search criteria		
		if (StringUtils.isNotBlank(storedCriteria)) {
			UserLessonSearch userLessonSearch = new UserLessonSearch();
			userLessonSearch.setUserUid(session.getCurrentUser().getUserUid());
			userLessonSearch.setSearchCriteria(storedCriteria);
			userLessonSearch.setLastEditUserUid(session.getCurrentUser().getUserUid());
			userLessonSearch.setLastEditDatetime(new Date());
			userLessonSearch.setSearchType(searchMethod);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(userLessonSearch);
		}
		if(StringUtils.isNotBlank(keywordsQuery) || filteredResult.size() > 0) {
				
			String queryString = "SELECT l.lessonTicketUid, l.lessonTicketNumber, l.operationUid FROM LessonTicket l, Operation o ";
			
			// query filter
			DatabaseQuery queryFilter = session.getSystemSelectionFilter().getQueryFilter(ModuleConstants.WELL_MANAGEMENT, "LessonTicket", "l");
			
			String conditionClause = queryFilter.getConditionClause(session);
			
			if (StringUtils.isNotBlank(conditionClause)) {
				queryString += conditionClause + " AND ";
			}
			
			queryString += " o.operationUid = l.operationUid AND (o.isDeleted = '0' OR o.isDeleted IS NULL) AND (o.isCampaignOverviewOperation = '0' OR o.isCampaignOverviewOperation IS NULL) AND o.sysOperationSubclass='well' AND " + 
				"(o.tightHole = '0' OR o.tightHole IS NULL OR (o.tightHole = '1' AND o.operationUid IN (SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid))) AND (" + 
				allOperationsNotAssignedToOpsTeam + "o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " + 
				"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " + 
				"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid)) " +
				((StringUtils.isNotBlank(keywordsQuery) && "Simple".equals(searchMethod))? "" +keywordsQuery+"":"") +
				((StringUtils.isNotBlank(selectedTicket) && "Advanced".equals(searchMethod))? " AND l.lessonTicketUid in (" +selectedTicket+") ":"") +
				"AND (l.isDeleted = '0' OR l.isDeleted IS NULL) AND (l.lessonTicketNumber is not null or l.lessonTicketNumber != '') GROUP BY l.lessonTicketUid ORDER BY o.operationName";
			
			String[] paramNames;
			Object[] paramValues;
			
			paramNames = new String[] { "userUid" };
			paramValues = new Object[] { session.getUserUid() };
			
			if(StringUtils.isNotBlank(keywordsFilter) && "Simple".equals(searchMethod)) {
				paramNames = new String[] { "keywordsFilter", "userUid" };
				paramValues = new Object[] { "%"+ keywordsFilter +"%", session.getUserUid() };
			}

			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
			
			Collections.sort(lstResult, new LessonTicketComparator(session));
			
			if (lstResult.size() > 0) {				
				for (Object[] rec : lstResult) {
					UserSearchResults userSearchResults = new UserSearchResults();
					userSearchResults.setLessonTicketUid((String) rec[0]);
					userSearchResults.setLastEditUserUid(session.getCurrentUser().getUserUid());
					userSearchResults.setLastEditDatetime(new Date());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(userSearchResults);
				}
			}	
		}
	}
	
	private static List<UserSearchResults> listAllSearchedResultsFromSpecificUser(String userUid) throws Exception {
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UserSearchResults WHERE (isDeleted = false or isDeleted is null) and lastEditUserUid=:lastEditUserUid ", "lastEditUserUid", userUid);
	}
	
	private static List<UserLessonSearch> listAllSearchedCriteriaFromSpecificUser(String userUid) throws Exception {
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UserLessonSearch WHERE (isDeleted = false or isDeleted is null) and lastEditUserUid=:lastEditUserUid ", "lastEditUserUid", userUid);
	}
	
	private void run(String selectedValue) throws Exception{
		//String selectedValue="C1=a,b,c,d&C2=&C3=v,b,n,d&C4=d&C5=&C6=";
		List<String> categories = new ArrayList<String>();
		
		Map<String,List<String>> selectedMap = new LinkedHashMap<String,List<String>>();
		String[] lines = selectedValue.split("&");
		for(String line : lines)
		{
			String[] str = line.split("=");
			String categoryUid = str[0];
			String data = null;
			if (str.length>1)
				data = str[1];
			List<String> list = new ArrayList<String>();
			if (StringUtils.isNotBlank(data))
			{
				String[] rows = data.split(",");
				list = Arrays.asList(rows);
			}
			categories.add(categoryUid);
			selectedMap.put(categoryUid, list);
		}
		
		List<String> meshResult = new ArrayList<String>(); 
		recursive("",null,selectedMap,categories,meshResult);
		
		// get from db lesson ticket and lesson ticket category element
		String sql = "FROM LessonTicket a, LessonTicketCategoryElement b WHERE a.lessonTicketUid=b.lessonTicketUid " +
		"AND (a.isDeleted is null OR a.isDeleted = false) AND (b.isDeleted is null OR b.isDeleted = false) GROUP BY a.lessonTicketUid";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		
		for (Object[] row : result)
		{
			LessonTicket lt = (LessonTicket)row[0];
			String strSql = "FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted = false) AND lessonTicketUid =:lessonTicketUid";
			String[] paramsFields = {"lessonTicketUid"};
			Object[] paramsValues = {lt.getLessonTicketUid()};
			List<LessonTicketCategoryElement> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (isIncludeLessonTicket(lstResult,meshResult))
				filteredResult.add(lt);
		}
		
		
	}
	
	public Boolean isIncludeLessonTicket(List<LessonTicketCategoryElement> selectedList,List<String> conditions)
	{
		for (String condition : conditions)
		{
			String[] rows = condition.split(",");
			Boolean isConditionValue = true;
			for (String item : rows)
			{
				Boolean itemFound = false;
				for (LessonTicketCategoryElement ltce : selectedList)
				{
					if (StringUtils.isNotBlank(ltce.getLessonCategory()) && StringUtils.isNotBlank(ltce.getLessonElement()))
					{
						String value = ltce.getLessonCategory()+"|"+ltce.getLessonElement();
						if (value.equalsIgnoreCase(item))
						{
							itemFound = true;
							break;
						}
					}
				}
				if (!itemFound)
				{
					isConditionValue = false;
					break;
				}
			}
			if (isConditionValue)
				return true;
		}
		return false;
	}
	
	public static void recursive(String parentString, Integer index, Map<String,List<String>> maps, List<String> categories, List<String> result)
	{
		if (index== null)
			index = 0;
		String categoryUid = categories.get(index);
		List<String> values = maps.get(categoryUid);
		if (values.size()>0)
		{
			for (String value : values)
			{
				String current = parentString+categoryUid+"|"+value+",";
				if (index+1<categories.size())
				{
					recursive(current,index+1,maps,categories,result);
				}else{
					result.add(current);
				}
			}
		}else{
			
			if (index+1<categories.size())
			{
				recursive(parentString,index+1,maps,categories,result);
			}else{
				result.add(parentString);
			}
		}
		
	}
	
	/**
	 * Get lesson ticket number based on selected lesson ticket uid
	 * @param listLessonTicketUid
	 * @return lessonTicketNumber
	 * @throws Exception
	 */
	private String getLessonTicketNumber(List<String> listLessonTicketUid) throws Exception {
		
		String sql = "SELECT lessonTicketNumber FROM LessonTicket WHERE (isDeleted is null or isDeleted = false) AND lessonTicketUid in (:lessonTicketUid) ORDER BY lessonTicketNumber";
		String[] paramsFields = {"lessonTicketUid"};
		Object[] paramsValues = {listLessonTicketUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		String lessonTicketNumber = null;
		if (lstResult.size() > 0){
			for (Object rec : lstResult) {
				if(StringUtils.isBlank(lessonTicketNumber)) {
					lessonTicketNumber = rec.toString();
				} else {
					lessonTicketNumber += ", " + rec.toString();
				}
			}
		}
		return lessonTicketNumber;
	}
	
	/**
	 * Get lessonTicket where lessonCategory and lessonElement contains keyword from simple search
	 * @param keywords
	 * @return
	 * @throws Exception
	 */
	private String getElementKeywordSearch(String keywords) throws Exception {
		
		String sql = "SELECT lessonTicketUid FROM LessonTicketCategoryElement WHERE (isDeleted is null or isDeleted = false) AND lessonCategory IN (SELECT a.lookupLessonTicketCategoryUid FROM LookupLessonTicketCategory a WHERE (a.isDeleted IS NULL or a.isDeleted = false) AND a.lessonTicketCategory like :keywordsFilter) " +
				"OR lessonElement IN (SELECT a.lookupLessonTicketElementsUid FROM LookupLessonTicketElements a WHERE (a.isDeleted IS NULL or a.isDeleted = false) AND a.lessonTicketElements like :keywordsFilter)";
		String[] paramsFields = {"keywordsFilter"};
		Object[] paramsValues = {"%"+ keywords +"%"};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		String lessonTicketList = "";
		if (lstResult.size() > 0){
			for (Object rec : lstResult) {
				if(StringUtils.isBlank(lessonTicketList)) {
					lessonTicketList = "'" + rec.toString() +"'";
				} else {
					lessonTicketList += ", '" + rec.toString() + "'";
				}
			}
		}
		return lessonTicketList;
	}
	
	/**
	 * submit report job and pass list of lessonTicketUid to custom property, use to load selected lesson ticket record
	 * @param session
	 * @param request
	 * @param commandBean
	 * @throws Exception 
	 */
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		
		Object selectedLessonTicket = commandBean.getRoot().getDynaAttr().get("selectedLessonTicket");
		if(selectedLessonTicket == null || selectedLessonTicket == ""){
			commandBean.getSystemMessage().addError("Please select at least one lesson learned to be generated in the report.", false, true);
			return null;
		}
		if(StringUtils.isBlank(selectedLessonTicket.toString())){
			return null;
		}
		String[] id_list = selectedLessonTicket.toString().split(",");
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomOperation(session, session.getCurrentOperationUid());
		userSelection.setCustomProperty("lessonTicketUid", Arrays.asList(id_list));
		userSelection.setCustomProperty("date", this.formatDate(new Date(), this.dateFormat));
		userSelection.setCustomProperty("time", this.formatDate(new Date(), this.timeFormat));
		userSelection.setCustomProperty("lessonTicketNumber", this.getLessonTicketNumber(Arrays.asList(id_list)));
		
		return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
		
	}
	
	/**
	 * submit report job and send report after report generation
	 * @param session
	 * @param request
	 * @param commandBean
	 * @throws Exception 
	 */
	protected ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		Object selectedLessonTicket = commandBean.getRoot().getDynaAttr().get("selectedLessonTicket");
		if(selectedLessonTicket == null || selectedLessonTicket == ""){
			commandBean.getSystemMessage().addError("No data fit the selected criteria therefore unable to generate report.", false, true);
			return null;
		}
		if(StringUtils.isBlank(selectedLessonTicket.toString())){
			return null;
		}
		String[] id_list = selectedLessonTicket.toString().split(",");
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomOperation(session, session.getCurrentOperationUid());
		userSelection.setCustomProperty("lessonTicketUid", Arrays.asList(id_list));
		userSelection.setCustomProperty("date", this.formatDate(new Date(), this.dateFormat));
		userSelection.setCustomProperty("time", this.formatDate(new Date(), this.timeFormat));
		userSelection.setCustomProperty("lessonTicketNumber", this.getLessonTicketNumber(Arrays.asList(id_list)));
		
		return super.submitReportJobWithEmail(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
	}
	
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception {
		
		String extraProperties = null;
		if (jobContext.getUserContext().getUserSelection().getCustomProperty("lessonTicketNumber") != null) 
			extraProperties = jobContext.getUserContext().getUserSelection().getCustomProperty("lessonTicketNumber").toString();
		
		report.setExtraProperties(extraProperties);
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		return this.getReportType(userContext.getUserSelection()) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}
	
	/**
	 * Set custom report output filename
	 * @param userContext
	 * @param paperSize
	 * @throws Exception 
	 */
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
		//return this.formatDate(new Date(), this.dateFormat) + "_" + this.formatDate(new Date(), this.timeFormat) + "" +(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
		return userContext.getUserSelection().getUserUid() + "" +(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}
	
	/**
	 * Setter/Getter for custom dateFormat
	 * @param dateFormat
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	/**
	 * Setter/Getter for custom dateFormat
	 * @return dateFormat
	 */
	public String getDateFormat() {
		return dateFormat;
	}
	
	/**
	 * Setter/Getter for custom timeFormat
	 * @param timeFormat
	 */
	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	/**
	 * Setter/Getter for custom timeFormat
	 * @return timeFormat
	 */
	public String getTimeFormat() {
		return timeFormat;
	}
	
	/**
	 * common method to get simpleDateFormat
	 * @param date
	 * @param format
	 * @return simpleDateFormat
	 */
	protected String formatDate(Date date, String format)
	{
		DateFormat df=new SimpleDateFormat(format);
		return df.format(date);
	}

	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}

	public void setReportTimeFormat(String reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public String getReportTimeFormat() {
		return reportTimeFormat;
	}

	/**
	 * lesson ticket comparator to order lesson ticket result by complete operation name + lesson ticket number
	 * @author Jackson
	 *
	 */
	private class LessonTicketComparator implements Comparator<Object[]>{
		
		private UserSession userSession;
		
		public LessonTicketComparator(UserSession userSession) {
			this.userSession = userSession;
		}

		public int compare(Object[] o1, Object[] o2){
			try{
				Object lessonTicketNumber1 = o1[1];
				Object lessonTicketNumber2 = o2[1];
				
				Object operation1 = o1[2];
				Object operation2 = o2[2];
				
				String l1 = WellNameUtil.getPaddedStr(lessonTicketNumber1.toString());
				String l2 = WellNameUtil.getPaddedStr(lessonTicketNumber2.toString());
				
				String op1 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), operation1.toString()));
				String op2 = WellNameUtil.getPaddedStr(CommonUtil.getConfiguredInstance().getCompleteOperationName(this.userSession.getCurrentGroupUid(), operation2.toString()));
				
				String f1 = op1 + " " + l1;
				String f2 = op2 + " " + l2;
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	public static void flushUserSearchRecord(String currentUserUid) {
		if(StringUtils.isNotBlank(currentUserUid)) {
			
			List<UserSearchResults> lstUserSearchResults;
			try {
				lstUserSearchResults = listAllSearchedResultsFromSpecificUser(currentUserUid);
				for(UserSearchResults usr: lstUserSearchResults){
					ApplicationUtils.getConfiguredInstance().getDaoManager().delete(usr);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Clear user search criteria
			List<UserLessonSearch> lstUserLessonSearch;
			try {
				lstUserLessonSearch = listAllSearchedCriteriaFromSpecificUser(currentUserUid);
				for(UserLessonSearch uls: lstUserLessonSearch){
					ApplicationUtils.getConfiguredInstance().getDaoManager().delete(uls);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
