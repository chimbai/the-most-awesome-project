package com.idsdatanet.d2.safenet.inspection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.InspectionResult;
import com.idsdatanet.d2.core.model.LookupInspectionCategory;
import com.idsdatanet.d2.core.model.LookupInspectionCheckType;
import com.idsdatanet.d2.core.model.LookupInspectionCheckTypeValue;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class InspectionDDRReportDataGenerator implements ReportDataGenerator{

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		String thisDailyUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getDailyUid().toString())) {
			thisDailyUid = userContext.getUserSelection().getDailyUid().toString();
		}
		if (StringUtils.isBlank(thisDailyUid)) return;
		
		String strSql = "FROM InspectionResult WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid";
		List<InspectionResult> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", thisDailyUid);
		for (InspectionResult rec : list) {
			ReportDataNode reportNode = reportDataNode.addChild("InspectionResult", rec);
			if (StringUtils.isNotBlank(rec.getInspectionResult())) {
				SimpleXMLElement data = SimpleXMLElement.loadXMLString(rec.getInspectionResult());
				if (data!=null && data.getChild()!=null) {
					for(Iterator i = data.getChild().iterator(); i.hasNext(); ){
						SimpleXMLElement result = (SimpleXMLElement) i.next();
						ReportDataNode resultNode = reportNode.addChild("LookupInspectionDetail");
						resultNode.addProperty("sequence", result.getAttribute("sequence"));
						resultNode.addProperty("inspectionDetail", result.getAttribute("inspectionDetail"));
						resultNode.addProperty("hasInput", result.getAttribute("hasInput"));
						resultNode.addProperty("comment", result.getAttribute("comment"));
						resultNode.addProperty("actionTaken", result.getAttribute("actionTaken"));
						resultNode.addProperty("uom", result.getAttribute("uom"));
						resultNode.addProperty("quantity", result.getAttribute("quantity"));
						
						String lookupInspectionCategoryUid = result.getAttribute("lookupInspectionCategoryUid");
						resultNode.addProperty("lookupInspectionCategoryUid", lookupInspectionCategoryUid);
						LookupInspectionCategory cetegory = (LookupInspectionCategory) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupInspectionCategory.class, lookupInspectionCategoryUid);
						resultNode.addProperty("inspectionCategoryName", cetegory.getInspectionCategoryName());
						resultNode.addProperty("inspectionCategorySequence", nullToEmptyString(cetegory.getSequence()));
						
						String lookupInspectionCheckTypeUid = result.getAttribute("lookupInspectionCheckTypeUid");
						resultNode.addProperty("lookupInspectionCheckTypeUid", lookupInspectionCheckTypeUid);
						LookupInspectionCheckType checkType = (LookupInspectionCheckType) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupInspectionCheckType.class, lookupInspectionCheckTypeUid);
						resultNode.addProperty("inspectionCheckTypeName", checkType.getInspectionCheckTypeName());
						resultNode.addProperty("inspectionCheckTypeSequence", nullToEmptyString(checkType.getSequence()));

						//format completion date
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String completionDate = result.getAttribute("completionDate");
						try {
							if (StringUtils.isNotBlank(completionDate)) {
								Date date = df.parse(completionDate);
								df = new SimpleDateFormat("dd MMM yyyy");
								resultNode.addProperty("completionDate", df.format(date));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						//selection values
						String inputType = result.getAttribute("inputType");
						resultNode.addProperty("inputType", inputType);
						for(Iterator j = result.getChild().iterator(); j.hasNext(); ){
							SimpleXMLElement selection = (SimpleXMLElement) j.next();
							ReportDataNode selectionNode = resultNode.addChild("Selection");
							String code = selection.getAttribute("value");
							selectionNode.addProperty("code", code);
							String label = code;
							strSql = "FROM LookupInspectionCheckTypeValue WHERE (isDeleted=false or isDeleted is NULL) and lookupInspectionCheckTypeUid=:lookupInspectionCheckTypeUid and lookupCode=:code order by sequence";
							List<LookupInspectionCheckTypeValue> checkValueList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"lookupInspectionCheckTypeUid", "code"}, new Object[] {lookupInspectionCheckTypeUid, code});
							if (checkValueList.size()>0) {
								label = checkValueList.get(0).getLookupValue();
							}
							selectionNode.addProperty("label", label);
						}
						
						
					}
				}
			}
			
		}
	}
	
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}

}
