package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.model.LookupLessonNumber;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketDetailDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private String lessonTicketNumberPrefixPattern = null;
	private Boolean autoPopulateFromActivity = false;
	private Boolean autoPopulateEventLocation = false;
	private Boolean autoPopulateCampaignName = false;
	private String lessonReportType = null;
	private Boolean autoPopulateUsernameToReportedByName = true;
	
	public void setLessonReportType(String lessonReportType) {
		this.lessonReportType = lessonReportType;
	}

	public void setAutoPopulateFromActivity(Boolean value){
		this.autoPopulateFromActivity = value;
	}
	
	public Boolean getAutoPopulateFromActivity(){
		return this.autoPopulateFromActivity;
	}
	
	public void setAutoPopulateEventLocation(Boolean autoPopulateEventLocation){
		this.autoPopulateEventLocation = autoPopulateEventLocation;
	}
	
	public Boolean getAutoPopulateEventLocation(){
		return this.autoPopulateEventLocation;
	}
	
	public void setAutoPopulateCampaignName(Boolean autoPopulateCampaignName){
		this.autoPopulateCampaignName = autoPopulateCampaignName;
	}
	
	public Boolean getAutoPopulateCampaignName(){
		return this.autoPopulateCampaignName;
	}
	
	public Boolean getAutoPopulateUsernameToReportedByName() {
		return this.autoPopulateUsernameToReportedByName;
	}
	
	public void setAutoPopulateUsernameToReportedByName(Boolean value){
		this.autoPopulateUsernameToReportedByName = value;
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			String lessonTicketPrefix = "";
			String lessonTicketUid = thisLessonTicket.getLessonTicketUid();
			
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("plan")) {
				node.getDynaAttr().put("lessonTicketNumber", "PLL - " + lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}else if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("non_op")) {
				node.getDynaAttr().put("lessonTicketNumber", "Non-Op - " + lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}else {
				node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
			}
			String strSql = "FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and lessonTicketUid =:lessonTicketUid";
			String[] paramsFields = {"lessonTicketUid"};
			Object[] paramsValues = {lessonTicketUid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0) {
				node.getDynaAttr().put("editableLessonLink", "1");
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
			
			if(autoPopulateFromActivity){
				if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW || operationPerformed == BaseCommandBean.OPERATING_MODE_DEPOT || operationPerformed == BaseCommandBean.OPERATING_MODE_WITSML) {
					String activityUid = (String) commandBean.getRoot().getDynaAttr().get("activityId");
					if (StringUtils.isNotBlank(activityUid)) {
						ActivityLessonTicketLink thisLink = new ActivityLessonTicketLink();
						thisLink.setActivityUid(activityUid);
						thisLink.setLessonTicketUid(thisLessonTicket.getLessonTicketUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLink);
					}
					
					String prefixPattern = this.getLessonTicketNumberPrefixPattern();
					if(StringUtils.isBlank(prefixPattern)) {
						prefixPattern = GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberPrefixPattern");
					}
					
					String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
					if (StringUtils.isNotBlank(getPrefixName)) {
						thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
					}
					
					String newreportnumber = null;
					List<String> paramsFields = new ArrayList<String>();
					List<Object> paramsValues = new ArrayList<Object>();
					paramsFields.add("wellUid");
					paramsValues.add(userSelection.getWellUid());
					
					String sql = "select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellUid = :wellUid";
					
					if(StringUtils.isNotBlank(lessonReportType)) {
						sql += " and lessonReportType = :lessonReportType";
						paramsFields.add("lessonReportType");
						paramsValues.add(this.lessonReportType);
					}else {
						sql += " and (lessonReportType is null or lessonReportType = '')";
					}
					
					sql += " order by lessonTicketNumber DESC";
					
					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
					
					if (list.size() > 0) {
						Collections.sort(list, new LessonTicketNumberComparator());
						String lessonTicketNumber = null;
						Object a = list.get(0);
						if (a!=null) {
							lessonTicketNumber = a.toString();
						}
							
						if (StringUtils.isNotBlank(lessonTicketNumber) && StringUtils.isNumeric(lessonTicketNumber)) {
							Integer intEventRef = (Integer)Integer.parseInt(lessonTicketNumber) + 1;
					
							newreportnumber = intEventRef.toString();
							
							if (newreportnumber.length()== 1){
								newreportnumber = "0" + newreportnumber;
							}
						}else {
							newreportnumber = "01";
						}
					}else {
						newreportnumber = "01";
					}
					thisLessonTicket.setLessonTicketNumber(newreportnumber);
					thisLessonTicket.setLessonTicketType(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
				}
			}else{
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoNewLessonTicketNumber"))){
					if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
						if (StringUtils.isNotBlank(thisLessonTicket.getCountry())) {
							process(thisLessonTicket,userSelection,session);
							commandBean.getSystemMessage().addInfo("Please Add Lesson Ticket Category Element for Newly Created Lesson Ticket.", true);
						}
					}
				}
			}	
		}
	}

	@Override
	public void beforeDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ActivityLessonTicketLink set isDeleted = TRUE WHERE lessonTicketUid=:thisLessonTicketUid", new String[] {"thisLessonTicketUid"}, new String[] {thisLessonTicket.getLessonTicketUid()});
		}
	}
	
	@Override
	public void beforeDataNodeSaveOrUpdate (CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			thisLessonTicket.setLessonReportType(lessonReportType);
			
			if (StringUtils.isBlank(thisLessonTicket.getLessonTicketNumber())) {
				thisLessonTicket.setLessonTicketNumber("");
			}
			
			Map<String, LookupItem> emailRecipient = commandBean.getLookupMap(node, "LessonTicket", "emailRecipient");
			if(emailRecipient != null){
				LookupItem selectedEmail = emailRecipient.get(thisLessonTicket.getEmailRecipient());
				if(selectedEmail != null){
					String label = (String) selectedEmail.getValue();
					if(StringUtils.isNotBlank(label)){
						label = label.replace("(", "").replace(")", "");
						String [] str = label.split(selectedEmail.getKey());
						thisLessonTicket.setResponsibleparty(str[0].trim());
					}
				}
			}
		}
	}

	public void onDataNodePasteAsNew(CommandBean commandBean,CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,UserSession userSession) throws Exception {
		Object object = targetNode.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			thisLessonTicket.setLessonTicketNumber(null);
			thisLessonTicket.setLessonTicketNumberPrefix(null);
			
			targetNode.getDynaAttr().put("lessonTicketNumber",null);
		}
	}
	
	synchronized private void process(LessonTicket thisLessonTicket, UserSelectionSnapshot userSelection, UserSession session) throws Exception
	{
		LookupLessonNumber thisLookupLessonNumber = this.getLookupLessonNumber(thisLessonTicket.getCountry());
		
		String prefixPattern = thisLookupLessonNumber.getPrefix();
		
		String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
		
		if (StringUtils.isNotBlank(getPrefixName)){
			thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
		}
		
		String newreportnumber = this.generatedLessonTicketNumber(thisLookupLessonNumber.getLastNumberGenerated(), thisLookupLessonNumber, userSelection);

		thisLessonTicket.setLessonTicketNumber(newreportnumber);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
	}
	
	private String generatedLessonTicketNumber(Integer lessonTicketNumber, LookupLessonNumber thisLookupLessonNumber, UserSelectionSnapshot userSelection) throws Exception {
		
		if(lessonTicketNumber != null) {
			
			Integer newNumber = lessonTicketNumber + 1;
			String strNewNumber = newNumber.toString();
			
			if(thisLookupLessonNumber.getMinimumLength() > 0) {
				Integer minLength = thisLookupLessonNumber.getMinimumLength();
				for (int i=strNewNumber.length(); i < minLength; i++) {
					strNewNumber = "0" + strNewNumber;
				}
			}
			String strSql = "FROM LessonTicket WHERE wellUid = :wellUid and lessonTicketNumberPrefix =:lessonTicketNumberPrefix and country=:country and lessonTicketNumber =:lessonTicketNumber";
			String[] paramsFields = {"wellUid", "lessonTicketNumberPrefix", "country", "lessonTicketNumber"};
			String[] paramsValues = {userSelection.getWellUid(), thisLookupLessonNumber.getPrefix(), thisLookupLessonNumber.getLookupLessonNumberUid(), strNewNumber};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0) {
				this.generatedLessonTicketNumber(newNumber, thisLookupLessonNumber, userSelection);
			} else {
				thisLookupLessonNumber.setLastNumberGenerated(newNumber);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLookupLessonNumber);
				return strNewNumber;
			}
		}
		return null;
		
	}
	
	private LookupLessonNumber getLookupLessonNumber(String lookupLesssonNumberUid) throws Exception {
		
		LookupLessonNumber lookupLessonNumber = (LookupLessonNumber) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupLessonNumber.class, lookupLesssonNumberUid);
		if(lookupLessonNumber != null) { 
			return lookupLessonNumber;
		}
		return null;
	}
	
	public void setLessonTicketNumberPrefixPattern(
			String lessonTicketNumberPrefixPattern) {
		if(StringUtils.isNotBlank(lessonTicketNumberPrefixPattern)) {
			//use $P for paramter and escape it, rather to configure the pattern ${value} directly which conflicts spring propertyholder syntax
			this.lessonTicketNumberPrefixPattern = StringUtils.replace(lessonTicketNumberPrefixPattern, "$P", "$");
		}
	}

	public String getLessonTicketNumberPrefixPattern() {
		return lessonTicketNumberPrefixPattern;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		LessonTicketDetailSearchObjectMapper recMapper = new LessonTicketDetailSearchObjectMapper((BaseCommandBean) commandBean); 
		
		if (meta.getTableClass().equals(LessonTicket.class)) {
			
			List<String> arrayParamNames = new ArrayList<String>();
			List<Object> arrayParamValues = new ArrayList<Object>();
			
			String strSql = "FROM LessonTicket WHERE wellUid = :wellUid AND (isDeleted IS NULL OR isDeleted = FALSE)";
			if(StringUtils.isNotBlank(lessonReportType)) {
				strSql += " AND lessonReportType = :lessonReportType";
				arrayParamNames.add("lessonReportType");
				arrayParamValues.add(this.lessonReportType);
			}else {
				strSql += " AND (lessonReportType IS NULL OR lessonReportType = '')";
			}
			
			arrayParamNames.add("wellUid");
			arrayParamValues.add(userSelection.getWellUid());
			
			
			//Ticket: 42411 - LL and PLL report to be filtered by lessonStatus
			//Add here because 
			//1. custom data loader not allow will not enter dataLoaderInterceptor to allow modify conditionClause
			//2. lesson ticket # logic need to present in report
			//lessonStatus customProperty in userSelection will available when user select from lookup in report generation screen
			String lessonStatusLookupValue = (String) userSelection.getCustomProperty("lessonStatus");
			
			if(StringUtils.isNotBlank(lessonStatusLookupValue)){
				strSql += " AND (lessonStatus = :lessonStatus) ";
				arrayParamNames.add("lessonStatus");
				arrayParamValues.add(lessonStatusLookupValue);
			}
			
			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN && Boolean.valueOf((String) request.getAttribute(LessonTicketDetailPopUpController.SEARCH_VIEW_MODE))) {
				Object currentLessonTicketUid = "";
				if(Boolean.valueOf(request.getParameter(LessonTicketDetailPopUpController.DIFFERENT_GROUP))) {
					return recMapper.mapLessonTicket(request.getParameter("data"), node);
				}else if(StringUtils.isNotBlank(request.getParameter("gotoFieldUid"))) {
					//front end edit a record at one time, gotoFieldUid from post tells current working lesson ticket record
					currentLessonTicketUid = request.getParameter("gotoFieldUid");
				}

				strSql += " AND lessonTicketUid = :lessonTicketUid ";
				arrayParamNames.add("lessonTicketUid");
				arrayParamValues.add(currentLessonTicketUid);
			}
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<LessonTicket> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new LessonTicketComparator());
			
			for(Object objResult: items){
				LessonTicket thisLessonTicket = (LessonTicket) objResult;
				output_maps.add(thisLessonTicket);
			}
						
			return output_maps;
		}
		
		if(meta.getTableClass().equals(LessonTicketCategoryElement.class)){

			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN && Boolean.valueOf((String) request.getAttribute(LessonTicketDetailPopUpController.SEARCH_VIEW_MODE))) {
				if(Boolean.valueOf(request.getParameter(LessonTicketDetailPopUpController.DIFFERENT_GROUP))) {
					return recMapper.mapLessonTicketCategoryElement(request.getParameter("data"), node);
				}
			}
			
			Object object = node.getData();
			
			if(object instanceof LessonTicket){
				LessonTicket thisLessonTicket = (LessonTicket)object;
				
				String strSql = "FROM LessonTicketCategoryElement ltce, LookupLessonTicketCategory lltc " +
								"WHERE ltce.lessonTicketUid = :lessonTicketUid " +
								"AND (ltce.isDeleted is FALSE OR ltce.isDeleted IS NULL) " +
								//"AND (lltc.isDeleted is FALSE OR lltc.isDeleted IS NULL) " +
								"AND ltce.lessonCategory = lltc.lookupLessonTicketCategoryUid " +
								"ORDER BY lltc.sequence ASC";
				List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lessonTicketUid", thisLessonTicket.getLessonTicketUid());
				
				List<Object> output_maps = new ArrayList<Object>();
				
				for(Object[] objResult: items){
					LessonTicketCategoryElement thisLessonTicketCategoryElement = (LessonTicketCategoryElement) objResult[0];
					output_maps.add(thisLessonTicketCategoryElement);
				}
							
				return output_maps;
			}
		}
		return null;
	}
	
	private class LessonTicketComparator implements Comparator<LessonTicket>{
		public int compare(LessonTicket o1, LessonTicket o2){
			try {
				String p1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumberPrefix());
				String p2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumberPrefix());
				
				String n1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumber());
				String n2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumber());
				
				String ltNumber1 = p1 + " - " + n1;
				String ltNumber2 = p2 + " - " + n2;

				if(o1.getRp2Date() == null || o2.getRp2Date() == null || o1.getRp2Date().compareTo(o2.getRp2Date()) == 0) {
					if(ltNumber1 == null || ltNumber2 == null) {
						return 0;
					}else if(ltNumber1.compareTo(ltNumber2) == 0) {
						return 0;
					}else if(ltNumber1.compareTo(ltNumber2) > 0) {
						return -1;
					}else if(ltNumber1.compareTo(ltNumber2) < 0) {
						return 1;
					}
				}else if(o1.getRp2Date().compareTo(o2.getRp2Date()) > 0) {
					return -1;
				}else if(o1.getRp2Date().compareTo(o2.getRp2Date()) < 0){
					return 1;
				}
				return 0;

			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private class LessonTicketNumberComparator implements Comparator<String>{
		public int compare(String s1, String s2){
			try{
				String f1 = WellNameUtil.getPaddedStr(s1.toString());
				String f2 = WellNameUtil.getPaddedStr(s2.toString());
				
				if (f1 == null || f2 == null) {
					return 0;
				}
				//refine the ticket# sorting order, numeric(descending) first then alphanumeric(descending)
				if (StringUtils.isBlank(f1.toString()) || StringUtils.isBlank(f2.toString())) {
					return 0;
				}
				if (!StringUtils.isNumeric(f1.toString()) && StringUtils.isAlphanumeric(f1.toString()) && StringUtils.isNumeric(f2.toString())) {
					return 1;
				}
				if (!StringUtils.isNumeric(f2.toString()) && StringUtils.isAlphanumeric(f2.toString()) && StringUtils.isNumeric(f1.toString())) {
					return -1;
				}
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if("1".equals(request.getParameter("newRecordInputMode"))){
			String activityUid = null;
			if (request !=null){
				activityUid = request.getParameter("activityId");
			}

			if (obj instanceof LessonTicket) {
				LessonTicket thisLessonTicket = (LessonTicket) obj;
				
				if(meta.getTableClass() == LessonTicket.class && StringUtils.isNotBlank(activityUid)){
					commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
					
					String strSql = "SELECT phaseCode, taskCode, rootCauseCode, dailyUid, jobTypeCode, lookupCompanyUid, holeSize FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
					String[] paramsFields = {"activityUid"};
					Object[] paramsValues = {activityUid};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					
					if (lstResult.size() > 0) {
						Object[] a = (Object[]) lstResult.get(0);
						
						if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
							thisLessonTicket.setPhaseCode(a[0].toString());
						}
						
						if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
							thisLessonTicket.setTaskCode(a[1].toString());
						}
						
						if (a[2] != null && StringUtils.isNotBlank(a[2].toString())){
							thisLessonTicket.setRootcauseCode(a[2].toString());
						}
						
						if (a[3] != null && StringUtils.isNotBlank(a[3].toString())){
							Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(a[3].toString());
							thisLessonTicket.setRp2Date(daily.getDayDate());
						}
						if (a[4] != null && StringUtils.isNotBlank(a[4].toString())){
							thisLessonTicket.setJobtypeCode(a[4].toString());
						}
						if (a[5] != null && StringUtils.isNotBlank(a[5].toString())){
							thisLessonTicket.setLookupCompanyUid(a[5].toString());
						}
						
						if (a[6] != null && StringUtils.isNotBlank(a[6].toString())){
							thisLessonTicket.setHoleSize((Double) a[6]);
							node.getDynaAttr().put("editableLessonLink", "1");
						} else if (a[6] == null){
							node.getDynaAttr().put("editableLessonLink", "1");
						}
						
						
					}else {
						if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
							Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
							thisLessonTicket.setRp2Date(daily.getDayDate());
						}
					}
					if (autoPopulateUsernameToReportedByName){
						if (userSelection != null && StringUtils.isNotBlank(userSelection.getUserUid())) {
							User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userSelection.getUserUid());
							thisLessonTicket.setReportedbyName(user.getFname());
						}
					}
					commandBean.getRoot().getDynaAttr().put("activityId", activityUid);	
				}
			}
			
		}
	}

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		Object obj = node.getData();
		
		if(!"1".equals(request.getParameter("newRecordInputMode"))){
			if (obj instanceof LessonTicket) {
				LessonTicket thisLessonTicket = (LessonTicket) obj;
				
				if (StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("plan")) {
					Calendar cal = Calendar.getInstance();
				    cal.setTime(new Date());
				    cal.set(Calendar.HOUR_OF_DAY, 0);
				    cal.set(Calendar.MINUTE, 0);
				    cal.set(Calendar.SECOND, 0);
				    cal.set(Calendar.MILLISECOND, 0);
					thisLessonTicket.setRp2Date(cal.getTime());
				} else {
					if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
						thisLessonTicket.setRp2Date(daily.getDayDate());
					}
				}
				if (autoPopulateEventLocation){
					thisLessonTicket.setEventLocation(userSelection.getWellUid());
				}
				if (autoPopulateCampaignName){
					thisLessonTicket.setCampaignName(userSelection.getCampaignUid());
				}
				if (autoPopulateUsernameToReportedByName){
					if (StringUtils.isNotBlank(userSelection.getUserUid())) {
						User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userSelection.getUserUid());
						thisLessonTicket.setReportedbyName(user.getFname());
					}
				}
			}
		}
	}
}
