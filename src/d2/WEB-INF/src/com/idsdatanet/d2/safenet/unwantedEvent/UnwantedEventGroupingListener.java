package com.idsdatanet.d2.safenet.unwantedEvent;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class UnwantedEventGroupingListener extends EmptyDataNodeListener implements CommandBeanListener, DataNodeLoadHandler{

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(UnwantedEvent.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			String strSql = "FROM UnwantedEvent " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"and operationUid=:operationUid " +
					"and type='UE' " +
					"and (parentUnwantedEventUid='' or parentUnwantedEventUid='root' or parentUnwantedEventUid is null) " +
					"order by eventRef";
			List<UnwantedEvent> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", userSelection.getOperationUid());
			for (UnwantedEvent unwantedEvent : list) {
				String unwantedEventUid = unwantedEvent.getUnwantedEventUid();
				strSql = "FROM ActivityUnwantedEventLink WHERE (isDeleted=false or isDeleted is null) and unwantedEventUid=:unwantedEventUid";
				List<ActivityUnwantedEventLink> link = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "unwantedEventUid", unwantedEventUid);
				if (link.size() > 0) output_maps.add(unwantedEvent);
			}
			return output_maps;
		}
		
		return null;
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof UnwantedEvent) {
			UnwantedEvent unwantedEvent = (UnwantedEvent) node.getData();
			
			Double[] values = this.recursiveDurationAndCost(unwantedEvent.getUnwantedEventUid());
			
			
			CustomFieldUom durationConverter = new CustomFieldUom(UserSession.getInstance(request).getUserLocale(), Activity.class, "activityDuration");
			durationConverter.setBaseValue(values[0]);
			node.getDynaAttr().put("duration", durationConverter.getFormattedValue());

			CustomFieldUom costConverter = new CustomFieldUom(UserSession.getInstance(request).getUserLocale(), ReportDaily.class, "daycost");
			costConverter.setBaseValue(values[1]);
			node.getDynaAttr().put("estimatedCost", costConverter.getFormattedValue(costConverter.getConvertedValue(), false));
			
			Double totalDuration = (Double) commandBean.getRoot().getDynaAttr().get("totalDuration_raw");
			Double totalCost = (Double) commandBean.getRoot().getDynaAttr().get("totalCost_raw");
			if (totalDuration==null) totalDuration = 0.0;
			if (totalCost==null) totalCost = 0.0;
			
			totalDuration += values[0];
			totalCost += values[1];
			
			commandBean.getRoot().getDynaAttr().put("totalDuration_raw", totalDuration);
			commandBean.getRoot().getDynaAttr().put("totalCost_raw", totalCost);
			
			durationConverter.setBaseValue(totalDuration);
			costConverter.setBaseValue(totalCost);
			commandBean.getRoot().getDynaAttr().put("totalDuration", durationConverter.getFormattedValue());
			commandBean.getRoot().getDynaAttr().put("totalCost", costConverter.getFormattedValue(costConverter.getConvertedValue(), false));
			
		}
	}
	
	private Double[] recursiveDurationAndCost(String unwantedEventUid) throws Exception {
		Double[] values = new Double[2];
		QueryProperties qp = new QueryProperties(); 
		qp.setUomConversionEnabled(false);
		
		values[0] = 0.0;
		values[1] = 0.0;
		
		String strSql = "FROM ActivityUnwantedEventLink u, Activity a " +
				"WHERE (u.isDeleted=false or u.isDeleted is null) " +
				"AND (a.isDeleted=false or a.isDeleted is null) " +
				"AND a.activityUid=u.activityUid " +
				"AND u.unwantedEventUid=:unwantedEventUid";
		List<Object[]> links = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "unwantedEventUid", unwantedEventUid, qp);
		for (Object[] link : links) {
			Activity activity = (Activity) link[1];
			if (activity.getActivityDuration()!=null) values[0] += activity.getActivityDuration();
			
			Double dailyCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(activity.getDailyUid());
			if (dailyCost!=null) {
				Double totalDuration = 0.0;
				strSql = "SELECT SUM(activityDuration) AS activityDuration FROM Activity " +
						"WHERE (isDeleted = false OR isDeleted IS NULL) " +
						"AND dailyUid = :dailyUid " +
						"AND (isSimop=false or isSimop is null) " +
						"AND (isOffline=false or isOffline is null)" +
						"AND (dayPlus IS NULL OR dayPlus=0) ";
				List<Double> listDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", activity.getDailyUid(), qp);
				if (listDuration.size()>0) {
					if (listDuration.get(0)!=null) totalDuration = listDuration.get(0); 
				}
				if (totalDuration>0.0 && activity.getActivityDuration()!=null) {
					values[1] += (activity.getActivityDuration() * dailyCost / totalDuration);
				}
			}
		}
		
		strSql = "FROM UnwantedEvent WHERE (isDeleted=false or isDeleted is null) and type='UE' AND parentUnwantedEventUid=:unwantedEventUid";
		List<UnwantedEvent> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "unwantedEventUid", unwantedEventUid);
		for (UnwantedEvent unwantedEvent:list) {
			Double[] childValues = this.recursiveDurationAndCost(unwantedEvent.getUnwantedEventUid());
			values[0] += childValues[0];
			values[1] += childValues[1];
		}
		
		
		return values;
	}

}
