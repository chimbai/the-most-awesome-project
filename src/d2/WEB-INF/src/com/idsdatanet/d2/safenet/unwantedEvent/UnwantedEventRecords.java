package com.idsdatanet.d2.safenet.unwantedEvent;

import java.util.ArrayList;
import java.util.List;

public class UnwantedEventRecords {
	private List<UnwantedEventRecord> records = new ArrayList();
	
	public UnwantedEventRecords() {}
	
	public void setRecords(List<UnwantedEventRecord> records) {
		this.records = records;
	}
	
	public List<UnwantedEventRecord> getRecords() {
		return this.records;
	}
}
