package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.search.elastic.core.ElasticSearchClient;
import com.idsdatanet.d2.core.search.elastic.indexer.IndexDocument;
import com.idsdatanet.d2.core.search.elastic.indexer.IndexField;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class LessonTicketDetailSearchObjectMapper {
	
	private BaseCommandBean commandBean = null;
	private ElasticSearchClient esClient = null;

	public LessonTicketDetailSearchObjectMapper(BaseCommandBean commandBean) {
		this.commandBean = commandBean;
		try {
			this.esClient = ElasticSearchClient.getConfiguredInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public List<Object> mapLessonTicket(String json,  CommandBeanTreeNode node) throws Exception{ 
		if(StringUtils.isBlank(json)) {
			return null;
		}
		Object record = this.mapData(LessonTicket.class.getSimpleName(), JSONObject.fromObject(json).optJSONObject("resource"), new LessonTicket(), node);
		List<Object> result = new ArrayList<Object>();
		result.add(record);
		return result;
	}
	
	public List<Object> mapLessonTicketCategoryElement(String json,  CommandBeanTreeNode node) throws Exception{ 
		if(StringUtils.isBlank(json)) {
			return null;
		}
		JSONArray elements = JSONObject.fromObject(json).optJSONArray("ll_category_element");
		List<Object> result = new ArrayList<Object>();
		
		if(elements != null) {
			for(Object element : elements) {
				Object record = this.mapData(LessonTicketCategoryElement.class.getSimpleName(), JSONObject.fromObject(element), new LessonTicketCategoryElement(), node);
				result.add(record);
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private Object mapData(String className, JSONObject obj, Object data, CommandBeanTreeNode node) throws Exception {
		if(obj == null) {
			return data;
		}
		
		IndexDocument doc = esClient.getIndexerMap().get(className.toLowerCase());
		List<IndexField> indexFields = doc.getField();
		
		for (Iterator i = obj.keys(); i.hasNext();) {
			String key = i.next().toString();
			for(IndexField indexField : indexFields) {
				if(obj.get(key) != null && !obj.get(key).equals("null") && key.equals(indexField.getIndexFieldName()) && indexField.isDataField()) {
					if(indexField.getFieldType().equals(Double.class.getSimpleName())) {
						PropertyUtils.setProperty(data, indexField.getFieldName(), obj.optDouble(key, 0));
					}else if(indexField.getFieldType().equals(Date.class.getSimpleName())) {
						Date d = new SimpleDateFormat(IndexField.INDEX_DATE_FORMAT).parse(obj.getString(key));
						PropertyUtils.setProperty(data, indexField.getFieldName(), d);
					}else if(indexField.getFieldType().equals(String.class.getSimpleName())) {
						if(this.commandBean.isLookup(className, indexField.getFieldName())) {
							Map<String, LookupItem> map = this.commandBean.getLookupMap(node, className, indexField.getFieldName());
							if(!map.containsKey(obj.get(key))) {
								PropertyUtils.setProperty(data, indexField.getFieldName(), obj.opt(indexField.getLinkedLookupName()));
							}else {
								PropertyUtils.setProperty(data, indexField.getFieldName(), obj.get(key));
							}
						}else if(this.commandBean.isCascadeLookup(className, indexField.getFieldName())){
							Map<String, LookupItem> map = this.commandBean.getCascadeLookupMap(node, className, indexField.getFieldName());
							if(!map.containsKey(obj.get(key))) {
								PropertyUtils.setProperty(data, indexField.getFieldName(), obj.opt(indexField.getLinkedLookupName()));
							}else {
								PropertyUtils.setProperty(data, indexField.getFieldName(), obj.get(key));
							}
						}else if(this.commandBean.isMultiSelect(className, indexField.getFieldName())){
							Map<String, LookupItem> map = this.commandBean.getMultiSelectLookupMap(className, indexField.getFieldName());
							JSONArray splitKey = obj.optJSONArray(key);
							if(splitKey != null) {
								String multiKey = "";
								String[] splitLabel = (obj.opt(indexField.getLinkedLookupName()) == null || obj.opt(indexField.getLinkedLookupName()).equals("null") ? new String[] {} : obj.optString(indexField.getLinkedLookupName()).split(","));
								for(int j=0; j<splitKey.size(); j++) {
									if(StringUtils.isNotBlank(splitKey.getString(j))) {
										if(!map.containsKey(splitKey.getString(j))) {
											multiKey += splitLabel[j] + "\t";
										}else {
											multiKey += splitKey.getString(j) + "\t";
										}
									}
								}
								PropertyUtils.setProperty(data, indexField.getFieldName(), multiKey);
							}
						}else {
							PropertyUtils.setProperty(data, indexField.getFieldName(), obj.get(key));
						}
					}
				}
			}
		}
		return data;
	}
	

}


