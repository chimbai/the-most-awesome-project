package com.idsdatanet.d2.safenet.lessonTicketReach.schedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketProfile;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.schedule.ProfileConfiguration;
import com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal.BaseDataTransferService;

public class LessonTicketReachEmailSchedule extends BaseDataTransferService{

	private MailEngine mailEngine = null;
	private String support_email = null;
	private String emailType = null;
	private String siteURL = null;
	private Map<String,ProfileConfiguration> profileConfigurations;
	public Map<String, ProfileConfiguration> getProfileConfigurations() {
		return profileConfigurations;
	}
	public void setProfileConfigurations(
			Map<String, ProfileConfiguration> profileConfigurations) {
		this.profileConfigurations = profileConfigurations;
	}
	
	private List<LessonTicketProfile> validProfile;
	private boolean isRunning=false;
	
	public static final int SUCCESS=1;
	public static final int ERROR=-1;
	
	
	public static LessonTicketReachEmailSchedule getConfiguredInstance() throws Exception {
		return getSingletonInstance(LessonTicketReachEmailSchedule.class);
	}
	
	public synchronized void run() throws Exception
	{
		
		this.setRunning(true);
		this.existingProfile();
		for(LessonTicketProfile ltp:this.validProfile)
		{
			this.runExtractionAndSendEmail(ltp);
		}
		this.setRunning(false);
		
	}
	
	public synchronized void existingProfile() throws Exception
	{
		
		String query = "FROM LessonTicketProfile WHERE (isDeleted = false or isDeleted is null) AND distributionKey is not null ORDER BY profileName";
		List<LessonTicketProfile> lessonTicketProfileList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(query);
		
		validProfile = new ArrayList<LessonTicketProfile>();
		
		for (LessonTicketProfile ltp:lessonTicketProfileList)
		{
			validProfile.add(ltp);
		}
	}
	
	public synchronized Integer runExtractionAndSendEmail(LessonTicketProfile ltp) throws Exception
	{
		try{
			
			if ("lessonTicketMatch".equals(this.emailType)) {
				this.sendLessonTicketProfile(ltp);
			} else if ("lessonTicketExpired".equals(this.emailType)){
				this.sendExpiredLessonTicket(ltp);
			}
			return SUCCESS;
			
		}catch(Exception ex)
		{
			//this.sendErrorEmail(day);
			//this.getLogger().error(ex);
			return ERROR;
		}
	}
	
	private void sendLessonTicketProfile(LessonTicketProfile ltp) throws Exception
	{
		String hyperLink = null;
		String distributionKey = null;
		String sqlCondition = this.constructLessonProfileCondition(ltp);
		ProfileConfiguration profileConfig = new ProfileConfiguration(this.getValidProfileConfiguration());
		
		profileConfig.clearContentMapping();
		profileConfig.setSubject(this.constructEmailSubject(ltp, profileConfig.getSubject()));
		
		//System.out.println(sqlCondition);
		distributionKey = ltp.getDistributionKey();
		if (StringUtils.isNotBlank(sqlCondition))
		{
			String query = "FROM LessonTicket WHERE (isDeleted = false or isDeleted is null) AND (rp2Status ='APPROVED') AND (lastPushDateTime is null) AND profileRegionUid=:lessonProfileUid AND " + sqlCondition;
			List<LessonTicket> lessonTicketList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, "lessonProfileUid", ltp.getLessonTicketProfileUid());
			
			Map<String, Object> lessonLearnedItems = new HashMap<String, Object>();
			if (lessonTicketList != null && lessonTicketList.size() > 0) {
				
				for (LessonTicket lessonTicket : lessonTicketList) {
					
					hyperLink = this.siteURL + "lessonticketreach.html?gotowellop=" + lessonTicket.getOperationUid() + "&amp;gotoFieldName=lessonTicketUid&amp;gotoFieldUid=" +lessonTicket.getLessonTicketUid();
					Map<String, Object> lessonLearnedItem = new HashMap<String, Object>();
					lessonLearnedItem.put("lessonTicketUid", lessonTicket.getLessonTicketUid());
					lessonLearnedItem.put("lessonTicketNumber", lessonTicket.getLessonTicketNumber());
					lessonLearnedItem.put("operationUid", lessonTicket.getOperationUid());
					lessonLearnedItem.put("dailyUid", lessonTicket.getDailyUid());
					lessonLearnedItem.put("hyperLink", hyperLink);
					lessonLearnedItems.put(lessonTicket.getLessonTicketUid(), lessonLearnedItem);
				}
			}
			if(lessonLearnedItems.size() > 0 && lessonLearnedItems != null) {
				profileConfig.addContentMapping("lessonLearned", lessonLearnedItems);
				if(StringUtils.isNotBlank(distributionKey)) {
					
					String[] emailList = this.getEmailList(ltp.getGroupUid(), distributionKey);
					profileConfig.setEmailList(emailList);
					this.sendEmail(profileConfig);
					this.updateLastPushLessonTicket(lessonLearnedItems);
					//System.out.println(hyperLink);
				}
				
			} else {
				//System.out.println("Not in");
			}
		}
	}
	
	private void sendExpiredLessonTicket(LessonTicketProfile ltp) throws Exception
	{
		
		String hyperLink = null;
		String distributionKey = null;
		String numbersOfDaysBeforeCompletion = GroupWidePreference.getValue(ltp.getGroupUid(), "numbersOfDaysBeforeCompletion");
		ProfileConfiguration profileConfig = new ProfileConfiguration(this.getValidProfileConfiguration());
		
		profileConfig.clearContentMapping();
		profileConfig.setSubject(this.constructEmailSubject(ltp, profileConfig.getSubject()));
		
		distributionKey = ltp.getDistributionKey();
		
		String query = "FROM LessonTicket WHERE (isDeleted = false or isDeleted is null) AND (rp2Status !='CLOSED' and rp2Status !='REJ') AND rp2Closeoutdate is not null AND profileRegionUid=:lessonProfileUid";
		List<LessonTicket> lessonTicketList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, "lessonProfileUid", ltp.getLessonTicketProfileUid());
		
		Map<String, Object> lessonLearnedItems = new HashMap<String, Object>();
		if (lessonTicketList != null && lessonTicketList.size() > 0) {
			
			for (LessonTicket lessonTicket : lessonTicketList) {
				Date currentDate = CommonUtil.getConfiguredInstance().currentDateTime(lessonTicket.getGroupUid(), lessonTicket.getWellUid());
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(currentDate);
				thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
				thisCalendar.set(Calendar.MINUTE, 0);
				thisCalendar.set(Calendar.SECOND , 0);
				thisCalendar.set(Calendar.MILLISECOND , 0);
				currentDate = thisCalendar.getTime();
				Date lessonTicketClosureDate = lessonTicket.getRp2Closeoutdate();
				
				if(lessonTicketClosureDate != null && currentDate != null) {
					if(currentDate.compareTo(lessonTicketClosureDate) <= 0) {
						if(((lessonTicketClosureDate.getTime() - currentDate.getTime()) / 86400000) <= Double.parseDouble(numbersOfDaysBeforeCompletion)) {
							hyperLink = this.siteURL + "lessonticketreach.html?gotowellop=" + lessonTicket.getOperationUid() + "&amp;gotoFieldName=lessonTicketUid&amp;gotoFieldUid=" +lessonTicket.getLessonTicketUid();
							Map<String, Object> lessonLearnedItem = new HashMap<String, Object>();
							lessonLearnedItem.put("lessonTicketUid", lessonTicket.getLessonTicketUid());
							lessonLearnedItem.put("lessonTicketNumber", lessonTicket.getLessonTicketNumber());
							lessonLearnedItem.put("operationUid", lessonTicket.getOperationUid());
							lessonLearnedItem.put("dailyUid", lessonTicket.getDailyUid());
							lessonLearnedItem.put("hyperLink", hyperLink);
							lessonLearnedItems.put(lessonTicket.getLessonTicketUid(), lessonLearnedItem);
						}
					}
				}
				
			}
		}
		if(lessonLearnedItems.size() > 0 && lessonLearnedItems != null) {
			profileConfig.addContentMapping("lessonLearned", lessonLearnedItems);
			if(StringUtils.isNotBlank(distributionKey)) {
				
				String[] emailList = this.getEmailList(ltp.getGroupUid(), distributionKey);
				profileConfig.setEmailList(emailList);
				this.sendEmail(profileConfig);
			}
			
		}
	}
	
	private void updateLastPushLessonTicket(Map<String, Object> lessonLearnedItems) throws Exception
	{
		for (Map.Entry<String, Object> attr : lessonLearnedItems.entrySet()) {
			if (attr.getValue()!=null) {
				//System.out.println(attr.getValue());
				String strSql = "UPDATE LessonTicket SET lastPushDateTime=:lastPushDateTime WHERE lessonTicketUid =:lessonTicketUid AND (isDeleted is null OR isDeleted = false)";
				String[] paramsFields = {"lastPushDateTime", "lessonTicketUid"};
				Object[] paramsValues = {new Date(), attr.getKey()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			}
		}
	}
	
	private String constructLessonProfileCondition(LessonTicketProfile ltp) throws Exception
	{	
		String sqlCondition = "";
		
		String sql = "SELECT lessonTicketProfileGroupUid, elementContentLink FROM LessonTicketProfileGroup WHERE (isDeleted = false or isDeleted is null) AND lessonTicketProfileUid=:lessonProfileUid";
		List<Object[]> lstLessonProfileGroup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "lessonProfileUid", ltp.getLessonTicketProfileUid());
		
		if (lstLessonProfileGroup.size() > 0){
			Integer i = lstLessonProfileGroup.size();
			for(Object[] thisLessonProfileGroupResult: lstLessonProfileGroup){
				i = i - 1;
				sqlCondition = sqlCondition + "(";
				String profileGroupUid = null;
				String elementContentLink = null;
				if(thisLessonProfileGroupResult[0] != null) profileGroupUid = thisLessonProfileGroupResult[0].toString();
				if(thisLessonProfileGroupResult[1] != null) elementContentLink = thisLessonProfileGroupResult[1].toString();
				
				String sql2 = "SELECT lessonTicketCategoryUid, lessonTicketElementsUid, lessonTicketElementsContentsUid FROM LessonTicketProfileDetail WHERE (isDeleted = false or isDeleted is null) AND lessonTicketProfileGroupUid=:lessonTicketProfileUid";
				List<Object[]> lstLessonProfileDetail = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "lessonTicketProfileUid", profileGroupUid);
				if (lstLessonProfileDetail.size() > 0){
					Integer j = lstLessonProfileDetail.size();
					for(Object[] thisLessonProfileGroupDetailResult: lstLessonProfileDetail){
						j = j - 1;
						String lessonCategory = "";
						String lessonElements = "";
						String lessonContents = "";
						
						if(thisLessonProfileGroupDetailResult[0] != null) lessonCategory = thisLessonProfileGroupDetailResult[0].toString();
						if(thisLessonProfileGroupDetailResult[1] != null) lessonElements = thisLessonProfileGroupDetailResult[1].toString();
						if(thisLessonProfileGroupDetailResult[2] != null) lessonContents = thisLessonProfileGroupDetailResult[2].toString();
						
						String sql3 = "SELECT lltec.lookupLessonTicketElementsContentsUid FROM " +
								"LookupLessonTicketCategory lltc, LookupLessonTicketElements llte, LookupLessonTicketElementsContents lltec " +
								"WHERE lltc.lookupLessonTicketCategoryUid=llte.lookupLessonTicketCategoryUid " +
								"AND lltc.lookupLessonTicketCategoryUid=lltec.lookupLessonTicketCategoryUid " +
								"AND llte.lookupLessonTicketElementsUid=lltec.lookupLessonTicketElementsUid " +
								"AND (lltc.isDeleted is null OR lltc.isDeleted = false) AND (llte.isDeleted is null OR llte.isDeleted = false) AND (lltec.isDeleted is null OR lltec.isDeleted = false) " +
								"AND lltec.lookupLessonTicketElementsContentsUid=:lessonContents";
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql3, "lessonContents", lessonContents);
						
						//CHECK if lookup have been deleted.
						if(lstResult.size() > 0) {
							sqlCondition = sqlCondition + "lessonTicketUid in (SELECT lessonTicketUid FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted=false) AND lessonElementContent='" + lessonContents + "')";							
						} else {
							sqlCondition = sqlCondition + "lessonTicketUid in (SELECT lessonTicketUid FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted=false) AND lessonElementContent='')";
						}
						if(j!= 0) sqlCondition = sqlCondition + " " + elementContentLink + " ";
					}
				}
				sqlCondition = sqlCondition + ")";
				if(i!=0) sqlCondition = sqlCondition + " AND ";
			}
		}
		
		return sqlCondition;
	}
	
	private String constructEmailSubject(LessonTicketProfile ltp, String subject) throws Exception
	{
		String numbersOfDaysBeforeCompletion = GroupWidePreference.getValue(ltp.getGroupUid(), "numbersOfDaysBeforeCompletion");
		
		if("lessonTicketMatch".equals(this.emailType)) {
			subject = "Lessons Learned - Match Found For Region (" +ltp.getProfileName()+ ")";
		} else if("lessonTicketExpired".equals(this.emailType)) {
			subject = "[Reminder] Lessons Learned - " + numbersOfDaysBeforeCompletion + " Days before Action Completion For Region (" +ltp.getProfileName()+ ")";
		}

		return subject;
	}
	
	public void sendEmail(ProfileConfiguration profileConfig) throws Exception
	{
		if(profileConfig == null) return;
		
		if(profileConfig.getEmailList()==null || (profileConfig.getEmailList()!=null && profileConfig.getEmailList().length==0)){
			throw new Exception("Unable to send mail because email list is empty");
		}
		if(this.mailEngine == null){
			throw new Exception("Unable to send mail because MailEngine is null");
		}
		
		this.mailEngine.sendMail(new String[] {}, profileConfig.getEmailList(), this.support_email, null, profileConfig.getSubject(), this.mailEngine.generateContentFromTemplate(profileConfig.getTemplateFile(), profileConfig.getContentMapping()));
		
	}
	
	protected ProfileConfiguration getValidProfileConfiguration(){
		return this.getProfileConfigurationByType(false, true);
	}
	
	protected ProfileConfiguration getErrorProfileConfiguration(){
		return this.getProfileConfigurationByType(true, false);
	}
	
	private ProfileConfiguration getProfileConfigurationByType(boolean isError, boolean isValid)
	{
		for (Iterator iter = this.getProfileConfigurations().entrySet().iterator(); iter.hasNext();) 
		{
			Map.Entry entry = (Map.Entry) iter.next();
			ProfileConfiguration profileConfiguration = (ProfileConfiguration) entry.getValue();
			if (profileConfiguration.getValidEmail().equals(isValid))
			{
				return profileConfiguration;
			}
		}
		return null;
	}
	
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isRunning() {
		return isRunning;
	}
	
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getEmailType() {
		return emailType;
	}
	
	public void afterPropertiesSet() throws Exception
	{	
		try{
			if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
			this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
			if (StringUtils.isBlank(support_email)) this.support_email = "clong@idsdatanet.com";
			this.siteURL = ApplicationConfig.getConfiguredInstance().getReportLinkServerUrl();
		}catch(Exception ex)
		{
			this.getLogger().error(ex);
		}
	}
	
	private String[] getEmailList(String groupUid, String distributionKey) throws Exception {
		
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(groupUid, distributionKey);
		
		return emailList;
		
	}
	
}
