package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LessonTicketUtils {
	
	/**
	 * Method for checking is report type included for the lesson learned (reportedByname, responsibleparty)
	 * @param operationType
	 * @param reporttype 
	 * @throws Exception
	 * @return Boolean
	 */
	public static Boolean checkReportTypeIncluded(List<String> operationType, String reporttype) throws Exception{
		for(String type:operationType)
		{
			if (type.equals(reporttype)){
				return true;
			}
		}
		
		return false;
	}
	
	public static void deleteActivityLessonTicketLink(String lessonTicketUid) throws Exception{
		String sql = "FROM ActivityLessonTicketLink WHERE lessonTicketUid =:thisLessonTicketUid";
		String[] paramsFields = {"thisLessonTicketUid"};
		Object[] paramsValues = {lessonTicketUid};
		List<ActivityLessonTicketLink> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if(result.size()>0){
			for(ActivityLessonTicketLink thisActivityLessonTicketLink : result){
				thisActivityLessonTicketLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisActivityLessonTicketLink);
			}
		}
	}
	
	/**
	 * Get User Full Name from user table by using user selection's user uid
	 * @param userSelection
	 * @return
	 * @throws Exception
	 */
	public static String getUserFullName(UserSelectionSnapshot userSelection) throws Exception{
		String userUid="";
		userUid=userSelection.getUserUid();
		if (StringUtils.isNotBlank(userUid)) {
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select fname from User where (isDeleted = false or isDeleted is null) and userUid = :userUid", "userUid", userUid);
			if (list.size() > 0 && list!=null) {
				return list.get(0).toString();
			}
		}
		return null;
	}
	
}
