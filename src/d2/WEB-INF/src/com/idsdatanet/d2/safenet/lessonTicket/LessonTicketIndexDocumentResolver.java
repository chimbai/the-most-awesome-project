package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.HashMap;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.search.elastic.indexer.IndexDocument;
import com.idsdatanet.d2.core.search.elastic.indexer.IndexDocumentResolver;

public class LessonTicketIndexDocumentResolver implements IndexDocumentResolver{
	
	HashMap<String, String> commandBeanByLessonReportType = null;
	HashMap<String, String> controllersByLessonReportType = null; 
	
	public void setCommandBeanByLessonReportType(HashMap<String, String> commandBeanByLessonReportType) {
		this.commandBeanByLessonReportType = commandBeanByLessonReportType;
	}
	

	public void setControllersByLessonReportType(HashMap<String, String> controllersByLessonReportType) {
		this.controllersByLessonReportType = controllersByLessonReportType;
	}

	@Override
	public String getCommandBeanId(IndexDocument indexDoc, Object record) throws Exception{
		if(record != null) {
			if(PropertyUtils.isReadable(record, "lessonReportType")) {
				String lessonReportType = (String) PropertyUtils.getProperty(record, "lessonReportType");
				if(StringUtils.isNotBlank(lessonReportType) && commandBeanByLessonReportType.get(lessonReportType) != null) {
					return commandBeanByLessonReportType.get(lessonReportType);
				}else {
					return indexDoc.getCommandBeanID();
				}
			}
			
		}
		
		return indexDoc.getCommandBeanID();
	}

	@Override
	public String getControllerId(IndexDocument indexDoc, Object record) throws Exception{
		if(record != null) {
			if(PropertyUtils.isReadable(record, "lessonReportType")) {
				String lessonReportType = (String) PropertyUtils.getProperty(record, "lessonReportType");
				if(StringUtils.isNotBlank(lessonReportType) && controllersByLessonReportType.get(lessonReportType) != null) {
					return controllersByLessonReportType.get(lessonReportType);
				}else {
					return indexDoc.getDocAccess();
				}
			}
		}
		
		return indexDoc.getDocAccess();
	}
	
	
}
