package com.idsdatanet.d2.safenet.nonConformance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
//import com.idsdatanet.d2.core.web.mvc.UserSession;
//import com.idsdatanet.d2.drillnet.operation.OperationUtils;

//NCR Information Center - jhwong
public class ncrInformationCenterDataNodeHandler implements DataNodeLoadHandler {

	private static String DATESORTORDER = "DESC";
	private static String NCRSORTORDER = "ASC";
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(NonconformanceEvent.class)) {
			String filter = (String) commandBean.getRoot().getDynaAttr().get("filter");
			
			boolean valid = true;
			String selectedDateFromString = null;
			Date selectedDateFrom = null;
			String selectedDateToString = null;
			Date selectedDateTo = null;
			String selectedServiceProvider = null;
			String selectedMOCStatus = null;
			String selectedWellName = null;
			String selectedNCRDescriptionKeyword = null;
			
			List<Object> output_maps = new ArrayList<Object>();
			
			//UserSession userSession = UserSession.getInstance(request);
			
			//String strSql = "FROM NonconformanceEvent WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND operationUid =:operationUid";
			String strSql = "FROM NonconformanceEvent WHERE (isDeleted IS NULL OR isDeleted = FALSE)";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			if(StringUtils.isNotBlank(filter)) {
				String[] filterField = filter.split(",",-1);
				selectedDateFromString = filterField[0];
				selectedDateToString = filterField[1];
				selectedServiceProvider = filterField[2];
				selectedMOCStatus = filterField[3];
				selectedWellName = filterField[4];
				selectedNCRDescriptionKeyword = filterField[5];

				if(StringUtils.isNotBlank(selectedDateFromString))
					selectedDateFrom = _parseDateString(selectedDateFromString);
				if(StringUtils.isNotBlank(selectedDateToString))
					selectedDateTo = _parseDateString(selectedDateToString);
				
				commandBean.getRoot().getDynaAttr().put("dateFrom", selectedDateFrom);
				commandBean.getRoot().getDynaAttr().put("dateTo", selectedDateTo);
				commandBean.getRoot().getDynaAttr().put("serviceProviderFilter", selectedServiceProvider);
				commandBean.getRoot().getDynaAttr().put("mocStatusFilter", selectedMOCStatus);
				commandBean.getRoot().getDynaAttr().put("wellNameFilter", selectedWellName);
				commandBean.getRoot().getDynaAttr().put("ncrDescriptionKeywordFilter", selectedNCRDescriptionKeyword);
			}
			
			// Date Range Check
			if(!(selectedDateFrom==null && selectedDateTo==null)) {
				if((selectedDateFrom == null && selectedDateTo != null) || (selectedDateFrom != null && selectedDateTo == null)) {
					valid = false;
					commandBean.getSystemMessage().addError("Date Range: Please enter both 'From' and 'To' dates", true);
				}
				else if(selectedDateTo.compareTo(selectedDateFrom)<0) {
					valid = false;
					commandBean.getSystemMessage().addError("Date Range: 'To' Date must be after 'From' Date", true);
				}
			}
			
			//arrayParamNames.add("operationUid");
			//arrayParamValues.add(userSession.getCurrentOperationUid());
			
			if(!valid) {
				commandBean.getRoot().getDynaAttr().put("totalRecordsFound", "Total Records Found: 0");
				return output_maps;
			}
			
			if(selectedDateFrom != null) {
				strSql = strSql + " AND eventDateTime >= :dateFrom ";
				arrayParamNames.add("dateFrom");
				arrayParamValues.add(selectedDateFrom);
			}
			if(selectedDateTo != null) {
				strSql = strSql + " AND eventDateTime <= :dateTo ";
				arrayParamNames.add("dateTo");
				arrayParamValues.add(selectedDateTo);
			}
			if(StringUtils.isNotBlank(selectedServiceProvider)) {
				strSql = strSql + " AND serviceProvider = :serviceProvider ";
				arrayParamNames.add("serviceProvider");
				arrayParamValues.add(selectedServiceProvider);
			}
			if(StringUtils.isNotBlank(selectedMOCStatus)) {
				strSql = strSql + " AND eventStatus = :eventStatus ";
				arrayParamNames.add("eventStatus");
				arrayParamValues.add(selectedMOCStatus);
			}
			if(StringUtils.isNotBlank(selectedWellName)) {
				strSql = strSql + " AND eventLocation LIKE :eventLocation ";
				arrayParamNames.add("eventLocation");
				arrayParamValues.add("%" + selectedWellName + "%");
			}
			if(StringUtils.isNotBlank(selectedNCRDescriptionKeyword)) {
				strSql = strSql + " AND eventDescription LIKE :eventDescription ";
				arrayParamNames.add("eventDescription");
				arrayParamValues.add("%" + selectedNCRDescriptionKeyword + "%");
			}
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<NonconformanceEvent> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			commandBean.getRoot().getDynaAttr().put("totalRecordsFound", "Total Records Found: " + items.size());
			
			Collections.sort(items, new NonconformanceEventComparator());
			for(Object objResult: items) {
				NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) objResult;
				output_maps.add(thisNonconformanceEvent);
			}
			return output_maps;
		}
		return null;
	}
	
	// Parses Date String given in format "DD MMM YYYY GMT+X"
	private Date _parseDateString(String dateString) {
		String[] dateField = dateString.split(" ",-1);
		String filteredDateString = dateField[0] + "/" + dateField[1] + "/" + dateField[2];
		DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
		try {
			Date date = df.parse(filteredDateString);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private class NonconformanceEventComparator implements Comparator<NonconformanceEvent> {
		public int compare(NonconformanceEvent o1, NonconformanceEvent o2) {
			int i;
			try {
				Date d1 = o1.getEventDateTime();
				Date d2 = o2.getEventDateTime();
				if (d1 == null || d2 == null) return 0;
				if ("DESC".equalsIgnoreCase(DATESORTORDER)) {
					i = d2.compareTo(d1);
					if (i != 0)
						return i;
				} else {
					i = d1.compareTo(d2);
					if (i != 0)
						return i;
				}
				
				// Date the same, do NCR # comparator
				String f1 = WellNameUtil.getPaddedStr(o1.getEventNumber());
				String f2 = WellNameUtil.getPaddedStr(o2.getEventNumber());
				if (f1 == null || f2 == null) return 0;
				if ("DESC".equalsIgnoreCase(NCRSORTORDER)) {
					return f2.compareTo(f1);
				} else {
					return f1.compareTo(f2);
				}				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}