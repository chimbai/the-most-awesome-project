package com.idsdatanet.d2.safenet.nonConformance;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.validation.BindException;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;


public class NonConformanceListDownloadCommandBean extends AbstractGenericWebServiceCommandBean implements ServletContextAware {
	
	private ServletContext servletContext = null;
	private String reportOutputFormat = "pdf";
	
	public void setServletContext(ServletContext servletContext){
		this.servletContext = servletContext;
	}
	
	public void setReportOutputFormat(String value) {
		this.reportOutputFormat = value;
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String filename = request.getParameter("filename");
		if("1".equals(request.getParameter("base64"))){
			filename = new String(Base64.decodeBase64(filename.getBytes("utf-8")),"utf-8");
		}
		if (filename != null && filename.indexOf("../") == -1 && filename.indexOf("..\\") == -1 && filename.endsWith("." + reportOutputFormat)) {
			File f = new File(servletContext.getRealPath("/") + "WEB-INF/report/pdf/NCR/" + filename);
			if("1".equals(request.getParameter("abaccess"))){
				FileDownloadWriter.writeFileOutput(request, response, f, true);
			}else{
				FileDownloadWriter.writeFileOutput(request, response, f, true, f.getName());
			}
		}
	}

}
