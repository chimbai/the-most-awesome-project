package com.idsdatanet.d2.safenet.nonConformance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


//Nonconformance Add/Edit - jhwong
public class nonConformanceDataNodeHandler implements DataNodeLoadHandler {

	private static String DATESORTORDER = "DESC";
	private static String NCRSORTORDER = "ASC";
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		if (meta.getTableClass().equals(NonconformanceEvent.class)) {
			return true;
		}
		else {
			return false;
		}	
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(NonconformanceEvent.class)) {
			
			List<Object> output_maps = new ArrayList<Object>();
			UserSession userSession = UserSession.getInstance(request);
			
			String strSql = "FROM NonconformanceEvent WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND operationUid = :operationUid";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			arrayParamNames.add("operationUid");
			arrayParamValues.add(userSession.getCurrentOperationUid());
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<NonconformanceEvent> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
			
			Collections.sort(items, new NonconformanceEventComparator());
			for(Object objResult: items) {
				NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) objResult;
				output_maps.add(thisNonconformanceEvent);
			}
			return output_maps;
		}
		return null;
	}
	
	private class NonconformanceEventComparator implements Comparator<NonconformanceEvent> {
		public int compare(NonconformanceEvent o1, NonconformanceEvent o2) {
			int i;
			try {
				Date d1 = o1.getEventDateTime();
				Date d2 = o2.getEventDateTime();
				if (d1 == null || d2 == null) return 0;
				if ("DESC".equalsIgnoreCase(DATESORTORDER)) {
					i = d2.compareTo(d1);
					if (i != 0)
						return i;
				} else {
					i = d1.compareTo(d2);
					if (i != 0)
						return i;
				}
				
				// Date the same, do NCR # comparator
				String f1 = WellNameUtil.getPaddedStr(o1.getEventNumber());
				String f2 = WellNameUtil.getPaddedStr(o2.getEventNumber());
				if (f1 == null || f2 == null) return 0;
				if ("DESC".equalsIgnoreCase(NCRSORTORDER)) {
					return f2.compareTo(f1);
				} else {
					return f1.compareTo(f2);
				}				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}