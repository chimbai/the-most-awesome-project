package com.idsdatanet.d2.safenet.dispensation;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.MocDisp;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;


public class DispensationCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener,InitializingBean{
	
	private MailEngine mailEngine = null;
	private String support_email = null;
	private String sendNotificationToResponsiblePartyEmailTemplate = null;

	
	public String getSendNotificationToResponsiblePartyEmailTemplate() {
		return sendNotificationToResponsiblePartyEmailTemplate;
	}

	public void setSendNotificationToResponsiblePartyEmailTemplate(String sendNotificationToResponsiblePartyEmailTemplate) {
		this.sendNotificationToResponsiblePartyEmailTemplate = sendNotificationToResponsiblePartyEmailTemplate;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

		if (this.mailEngine==null) this.setMailEngine(ApplicationUtils.getConfiguredInstance().getMailEngine());
		this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isBlank(support_email)) this.support_email = "support@idsdatanet.com";
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(MocDisp.class)) {
				MocDisp mocDisp = (MocDisp)targetCommandBeanTreeNode.getData();
				if("sendEmailNotification".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					this.sendEmailNotification(mocDisp.getMocDispUid(), (BaseCommandBean)commandBean, request);
				}
			}
		}
	}
	
	/**
	 * Send Email Notification to Responsible Party upon clicking "Send" button
	 * @param mocDispUid
	 * @param commandBean
	 * @param request 
	 * @throws Exception
	 */
	private void sendEmailNotification(String mocDispUid, BaseCommandBean commandBean, HttpServletRequest request) throws Exception
	{
		MocDisp mocDisp = (MocDisp) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(MocDisp.class, mocDispUid);
		String[] emailList=null;
		String hyperLink = null;
		String content = null;
		
		if(!StringUtils.isBlank(mocDisp.getEmailReceipient())){

			String input = mocDisp.getEmailReceipient();
			if (!StringUtils.isBlank(input)){
				emailList=input.split(",");
			}
			
			String drDispNumber = mocDisp.getMocDispNumberPrefix();
			String emailSubject = "System Message - Dispensation Request (DR #: DR-" + drDispNumber + ")";
			
			String url = request.getParameter("baseUrl");
			//String url = UserSession.getInstance(request).getClientBaseUrl();
			hyperLink = url + "?gotowellop=" + mocDisp.getOperationUid() + "&amp;gotoFieldName=mocDispUid&amp;gotoFieldUid=" + mocDisp.getMocDispUid();
		
			//call vars in email template
			Map<String, Object> md = new HashMap<String, Object>();
			md.put("mocDispUid", mocDisp.getMocDispUid());
			md.put("mocDispNumber", drDispNumber);
			md.put("operationUid", mocDisp.getOperationUid());
			md.put("hyperLink", hyperLink);
			
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("mocDisp", md);
			
			if(StringUtils.isBlank(this.sendNotificationToResponsiblePartyEmailTemplate)) {
				commandBean.getSystemMessage().addError("Unable to send");
			} else {
				mailEngine.sendMail(new String[]{mocDisp.getEmailReceipient()}, emailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendNotificationToResponsiblePartyEmailTemplate, params));
				commandBean.getSystemMessage().addInfo("DR " + drDispNumber + " has been successfully sent");
			}
		}
	}
}
