package com.idsdatanet.d2.safenet.hsePlan;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * A DataLoaderInterceptor for selecting rig based data by providing an operationUid
 * 
 * @author zjong
 *
 */
public class HsePlanSTTDataLoaderInterceptor implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		if (StringUtils.isNotBlank(userSelection.getOperationUid())) {
			List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select rigInformationUid from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid group by rigInformationUid", "operationUid", userSelection.getOperationUid());
			if (rs != null && !rs.isEmpty()) {
				String delimiter = "";
				for (int i = 0; i < rs.size(); i++) {
					customCondition += delimiter;
					customCondition += "rigInformationUid = :rigInformationUid_" + i;
					delimiter = " or ";
					query.addParam("rigInformationUid_" + i, rs.get(i));
				}
			}
		}
		if (StringUtils.isBlank(customCondition)) {
			customCondition = "1 = 2";
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
