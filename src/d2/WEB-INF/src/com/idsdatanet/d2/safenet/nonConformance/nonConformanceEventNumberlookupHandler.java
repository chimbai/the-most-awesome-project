package com.idsdatanet.d2.safenet.nonConformance;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class nonConformanceEventNumberlookupHandler implements LookupHandler {	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String sql = "select a.nonconformanceEventUid, a.eventNumber from NonconformanceEvent a where (a.isDeleted is null or a.isDeleted = 0) and wellUid = :wellUid order by a.eventNumber";
		
		List<Object[]> userResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "wellUid", userSelection.getWellUid());
		if (userResults.size() > 0) {
			for (Object[] userResult : userResults) {
				if (userResult[0] != null && userResult[1] != null) {
					LookupItem lookupItem = null;
					
					lookupItem = new LookupItem(userResult[0].toString(), userResult[1].toString());
					
					result.put(userResult[0].toString(), lookupItem);
				}
			}
		}
		return result;
	}
}