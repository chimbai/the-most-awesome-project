package com.idsdatanet.d2.safenet.environmentDischarges;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.EnvironmentDischargesComment;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

import dev.util.StringUtils;

public class EnvironmentDischargesActionManager implements ActionManager {
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if (Action.SAVE.equals(action)) {
			Object object = node.getData();
			if (object instanceof Discharges) {
				return new SaveDischargesActionHandler();
			} else if (object instanceof Emissions) {
				return new SaveEmissionsActionHandler();
			} else if (object instanceof Wastes) {
				return new SaveWastesActionHandler();
			} else if (object instanceof EnvironmentDischarges && StringUtils.equals("EnvironmentDischarges", node.getDataDefinition().getTableClass().getSimpleName())) {
				return new DummySaveEnvironmentDischargesActionHandler();
			} else if (object instanceof DischargesComment) {
				return new SaveDischargesCommentActionHandler();
			} else if (object instanceof EmissionsComment) {
				return new SaveEmissionsCommentActionHandler();
			} else if (object instanceof WastesComment) {
				return new SaveWastesCommentActionHandler();
			}
		}
		return null;
	}
	
	private class DummySaveEnvironmentDischargesActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			// Don't save anything
			return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveDischargesActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				Discharges discharges = (Discharges) node.getData();
			
				if (discharges !=null) {
					
						EnvironmentDischarges environmentDischarges = new EnvironmentDischarges();
						
						PropertyUtils.copyProperties(environmentDischarges, discharges);
						environmentDischarges.setCategory("discharges");
						
						if (discharges.getLookupEnvironmentDischargesUid() == null) {
							environmentDischarges.setLookupEnvironmentDischargesUid(discharges.getLookupEnvironmentDischargesUid());;
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischarges);
				}
		return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveEmissionsActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				Emissions emissions = (Emissions) node.getData();
			
				if (emissions !=null) {
					
						EnvironmentDischarges environmentDischarges = new EnvironmentDischarges();
						
						PropertyUtils.copyProperties(environmentDischarges, emissions);
						environmentDischarges.setCategory("emissions");
						
						if (emissions.getLookupEnvironmentDischargesUid() == null) {
							environmentDischarges.setLookupEnvironmentDischargesUid(emissions.getLookupEnvironmentDischargesUid());;
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischarges);
				}
		return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveWastesActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				Wastes wastes = (Wastes) node.getData();
			
				if (wastes !=null) {
					
						EnvironmentDischarges environmentDischarges = new EnvironmentDischarges();
						
						PropertyUtils.copyProperties(environmentDischarges, wastes);
						environmentDischarges.setCategory("wastes");
						
						if (wastes.getLookupEnvironmentDischargesUid() == null) {
							environmentDischarges.setLookupEnvironmentDischargesUid(wastes.getLookupEnvironmentDischargesUid());;
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischarges);
				}
		return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveDischargesCommentActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				DischargesComment dischargesComment = (DischargesComment) node.getData();
			
				if (dischargesComment !=null) {
					
						EnvironmentDischargesComment environmentDischargesComment = new EnvironmentDischargesComment();
						
						PropertyUtils.copyProperties(environmentDischargesComment, dischargesComment);
						environmentDischargesComment.setCategory("discharges");
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischargesComment);
				}
		return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveEmissionsCommentActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				EmissionsComment emissionComment = (EmissionsComment) node.getData();
			
				if (emissionComment !=null) {
					
						EnvironmentDischargesComment environmentDischargesComment = new EnvironmentDischargesComment();
						
						PropertyUtils.copyProperties(environmentDischargesComment, emissionComment);
						environmentDischargesComment.setCategory("emissions");
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischargesComment);
				}
		return new ActionHandlerResponse(true);
		}		
	}
	
	private class SaveWastesCommentActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				WastesComment wastesComment = (WastesComment) node.getData();
			
				if (wastesComment !=null) {
					
						EnvironmentDischargesComment environmentDischargesComment = new EnvironmentDischargesComment();
						
						PropertyUtils.copyProperties(environmentDischargesComment, wastesComment);
						environmentDischargesComment.setCategory("wastes");
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(environmentDischargesComment);
				}
		return new ActionHandlerResponse(true);
		}		
	}
}
