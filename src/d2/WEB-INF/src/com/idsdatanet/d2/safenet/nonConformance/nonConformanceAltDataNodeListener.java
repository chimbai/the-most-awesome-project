package com.idsdatanet.d2.safenet.nonConformance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.model.NonconformanceEventLog;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

// nonConformance - jhwong
public class nonConformanceAltDataNodeListener extends EmptyDataNodeListener {

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {	
		Object object = node.getData();
		
		if (object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
			
			//get date
			DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			thisNonconformanceEvent.setEventDateTime(daily.getDayDate());
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		//20028
		if (object instanceof NonconformanceEvent) {	
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
		
			node.getDynaAttr().put("eventNumber",  thisNonconformanceEvent.getEventNumberPrefix() + " - " + thisNonconformanceEvent.getEventNumber());
			
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
			thisNonconformanceEvent.setLastEditUserFullname(session.getCurrentUser().getFname());
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		if(object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;			
				
			// Only for new records
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				// ncr number
				String strSql1 = "Select eventNumber From NonconformanceEvent WHERE (isDeleted is false or isDeleted is null) AND wellUid = :wellUid AND eventNumber IS NOT NULL";
				String[] paramsFields1 = {"wellUid"};
				Object[] paramsValues1 = {session.getCurrentWellUid()};
				String newEventnumber = null;
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
				if (lstResult.size() > 0 && lstResult != null) {
					Collections.sort(lstResult, new EventNumberComparator());
					String ncrEventNumber = null;
					
					Object a = lstResult.get(0);
					if (a!=null) {
						ncrEventNumber = a.toString();
					}
						
					if (StringUtils.isNotBlank(ncrEventNumber) && StringUtils.isNumeric(ncrEventNumber)) {
						Integer intEventRef = (Integer)Integer.parseInt(ncrEventNumber) + 1;
				
						newEventnumber = intEventRef.toString();
						
						if (newEventnumber.length()== 1){
							newEventnumber = "0" + newEventnumber;
						}
					}else {
						newEventnumber = "01";
					}
				}else {
					newEventnumber = "01";
				}
				thisNonconformanceEvent.setEventNumber(newEventnumber);
		
				//get well name
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				
				String wellUid="";
				String wellName = "";
				
				String strSql = "SELECT wellName from Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thiswellUid";
				String[] paramsFields = {"thiswellUid"};
				Object[] paramsValues = {session.getCurrentWellUid()};
				
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				if (lstResult1.size() > 0){
					Object wellname = (Object) lstResult1.get(0);
					
					if (wellname != null && StringUtils.isNotBlank(wellname.toString()))
					{
						wellName = wellname.toString();
					}
				}
				
				//get date
				DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
				thisNonconformanceEvent.setEventDateTime(daily.getDayDate());
				
				//ncreventprefix
				thisNonconformanceEvent.setEventNumberPrefix(wellName + "-" +  dateFormatter.format(daily.getDayDate()));
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisNonconformanceEvent);

			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean,CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,UserSession userSession) throws Exception {
		Object object = targetNode.getData();
		if (object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
			thisNonconformanceEvent.setEventNumber(null);
			thisNonconformanceEvent.setEventDateTime(null);
			
//			targetNode.getDynaAttr().put("thisNonconformanceEvent",null);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj = node.getData();
		
		if (obj instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) obj;
			String strSql = "UPDATE NonconformanceEventLog SET isDeleted = true WHERE nonconformanceEventUid =:thisNonconformanceEventUid";
			String[] paramsFields = {"thisNonconformanceEventUid"};
			Object[] paramsValues = {thisNonconformanceEvent.getNonconformanceEventUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		}
	}
	
	private class EventNumberComparator implements Comparator<String>{
		public int compare(String s1, String s2){
			try{
				String f1 = WellNameUtil.getPaddedStr(s1.toString());
				String f2 = WellNameUtil.getPaddedStr(s2.toString());
				
				if (f1 == null || f2 == null) return 0;
				//refine the ticket# sorting order, numeric first then alphanumeric
				if (StringUtils.isBlank(f1.toString()) || StringUtils.isBlank(f2.toString())) return 0;
				if (!StringUtils.isNumeric(f1.toString()) && StringUtils.isAlphanumeric(f1.toString()) && StringUtils.isNumeric(f2.toString())) return 1;
				if (!StringUtils.isNumeric(f2.toString()) && StringUtils.isAlphanumeric(f2.toString()) && StringUtils.isNumeric(f1.toString())) return -1;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}

	}
}