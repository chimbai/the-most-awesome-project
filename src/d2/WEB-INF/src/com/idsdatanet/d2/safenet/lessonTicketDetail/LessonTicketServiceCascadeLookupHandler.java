package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;



public class LessonTicketServiceCascadeLookupHandler implements CascadeLookupHandler{
	private String lookupType=null;

	
	public String getLookupType() {
		return lookupType;
	}

	public void setLookupType(String lookupType) {
		this.lookupType = lookupType;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		return getServiceTypeLookupItems();
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
			
		return getServiceTypeLookupItems();
	}
	
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Map<String, LookupItem> lookupMap = new LinkedHashMap<String, LookupItem>();
								lookupMap = this.getServiceTypeLookupItems();
		
		String companySql = "SELECT distinct lcs.lookupCompanyUid, lcs.code "
							+ "FROM LookupCompany lc, LookupCompanyService lcs "
							+ "WHERE (lc.isDeleted = false OR lc.isDeleted is null) "
							+ "AND (lcs.isDeleted = false OR lcs.isDeleted is null) "
							+ "AND lc.lookupCompanyUid = lcs.lookupCompanyUid "
							+ "ORDER BY 1, 2";
		
		List<?> lsResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(companySql);
		
		String tempLookupCompanyUid = "";
		Map<String, LookupItem> resultMap = new LinkedHashMap<String, LookupItem>();
		List<CascadeLookupSet> cascadeLookupList = new ArrayList<CascadeLookupSet>();
		String companyKey = "";
				
		if (lsResults.size() > 0) {
			for (Object lsResult : lsResults) {
				if (lsResult != null) {
					Object[] code = (Object[]) lsResult;
					String lookupCompanyUid = (String) code[0];
					String serviceCode = (String) code[1];

					boolean diffCompanyUid = tempLookupCompanyUid.equals("") || !tempLookupCompanyUid.equals(lookupCompanyUid);
					
					if (diffCompanyUid) {
							resultMap = this.sortLookupMapByLookupValue(resultMap, new ServiceTypeComparator());
							for(Entry<String, LookupItem> lookup : resultMap.entrySet()){
								CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(companyKey);
								cascadeLookupSet.setLookup((LinkedHashMap<String, LookupItem>) resultMap);
								cascadeLookupList.add(cascadeLookupSet);
							}
		
						tempLookupCompanyUid = lookupCompanyUid;
						resultMap = new LinkedHashMap<String, LookupItem>();
					}
					
					companyKey = mapCurrentLookupItem(serviceCode, lookupCompanyUid, resultMap, lookupMap);
				}
			}
		}
		CascadeLookupSet[] completeCascadeResults = new CascadeLookupSet[cascadeLookupList.size()];
		completeCascadeResults = cascadeLookupList.toArray(completeCascadeResults);		
		return completeCascadeResults;
	}

	private String mapCurrentLookupItem(String serviceCode, String lookupCompanyUid, Map<String, LookupItem> resultMap, Map<String, LookupItem> lookupMap) throws Exception{
		String companyKey = lookupCompanyUid;
		if (lookupMap.get(serviceCode) != null) {
			resultMap.put(serviceCode, lookupMap.get(serviceCode));
		}
		return companyKey;
	}
	
	private LinkedHashMap<String, LookupItem> getServiceTypeLookupItems() throws Exception{
		String sql = "FROM CommonLookup "
					+ "WHERE (isDeleted IS NULL OR isDeleted=FALSE) "
					+ "AND (isActive IS NULL OR isActive=TRUE) "
					+ "AND lookupTypeSelection=:lookupType "
					+ "ORDER BY lookupLabel";
		
		List<String> param = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		
		param.add("lookupType");
		paramValue.add(this.lookupType);
		
		List<CommonLookup> serviceTypes = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, param, paramValue);
		LinkedHashMap<String, LookupItem> serviceTypeLookups = null;
		
		if(serviceTypes != null && serviceTypes.size()>0){
			serviceTypeLookups = new LinkedHashMap<String, LookupItem>();
			for(CommonLookup serviceType : serviceTypes){
				String serviceCode = serviceType.getShortCode();
				LookupItem serviceTypeLookup = new LookupItem(serviceCode, serviceType.getLookupLabel());
				serviceTypeLookups.put(serviceCode, serviceTypeLookup);
			}
			serviceTypeLookups = this.sortLookupMapByLookupValue(serviceTypeLookups, new ServiceTypeComparator());
		}
		
		return serviceTypeLookups;
	}
	
	private LinkedHashMap<String, LookupItem> sortLookupMapByLookupValue(Map<String, LookupItem> resultMap, Comparator comparator){
		LinkedHashMap<String, LookupItem> sortedMap = null;
		if(resultMap != null){
			List<Map.Entry<String, LookupItem>> unsortedList = new LinkedList<Map.Entry<String, LookupItem>>(resultMap.entrySet());
			Collections.sort(unsortedList, comparator);
			sortedMap = new LinkedHashMap<String, LookupItem>();
			for(Map.Entry<String, LookupItem> lookup : unsortedList){
				sortedMap.put(lookup.getKey(), lookup.getValue());
			}
		}
		return sortedMap;
	}
	
	private class ServiceTypeComparator implements Comparator<Map.Entry<String, LookupItem>>{
		public int compare(Map.Entry<String, LookupItem> o1, Map.Entry<String, LookupItem> o2){
			try{
				
				String f1 = WellNameUtil.getPaddedStr(o1.getValue().getValue().toString().toLowerCase()).replaceAll(" ", "");
				String f2 = WellNameUtil.getPaddedStr(o2.getValue().getValue().toString().toLowerCase()).replaceAll(" ", "");
				
				if (f1 == null || f2 == null) return 0;
				
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
}
