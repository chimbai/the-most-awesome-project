package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LessonTicketProfile;
import com.idsdatanet.d2.core.model.LessonTicketProfileDetail;
import com.idsdatanet.d2.core.model.LessonTicketProfileGroup;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketReachCommandBean extends DefaultBeanSupport implements CommandBeanListener ,InitializingBean, DataNodeListener {
	private MailEngine mailEngine = null;
	private String sendProfileTemplate = null;
	private String sendErrorProfileTemplate = null;
	private String support_email = null;
	
	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	public void setSendProfileTemplate(String sendProfileTemplate) {
		this.sendProfileTemplate = sendProfileTemplate;
	}
	
	public String getSendProfileTemplate() {
		return sendProfileTemplate;
	}
	
	public void setSendErrorProfileTemplate(String sendErrorProfileTemplate) {
		this.sendErrorProfileTemplate = sendErrorProfileTemplate;
	}
	
	public String getSendErrorProfileTemplate() {
		return sendErrorProfileTemplate;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(LessonTicketProfile.class)) {
				if("sendProfile".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					sendProfile(request, commandBean, targetCommandBeanTreeNode);
				}
			}
		}
	}
	
	private void sendProfile(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		
		BaseCommandBean baseCommandBean = (BaseCommandBean) commandBean;
		
		UserSession userSession = UserSession.getInstance(request);
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(userSession);
		Map<String, Object> params = new LinkedHashMap<String, Object>();

		String mailSubject = null;  
		String distributionKey = null;
		String profileName = null;
		Object objProfile = node.getData();
		CommandBeanTreeNode childNode = (CommandBeanTreeNode) node;
		CommandBeanTreeNode childNode2 = (CommandBeanTreeNode) node;
		
		CascadeLookupMeta categoryMeta = baseCommandBean.getCascadeLookupMeta("LessonTicketProfileDetail", "lessonTicketCategoryUid");
		String categoryURI = categoryMeta.getLookupUriOrHandler().toString();
		
		CascadeLookupMeta elementsMeta = baseCommandBean.getCascadeLookupMeta("LessonTicketProfileDetail", "lessonTicketElementsUid");
		String elementsURI = elementsMeta.getLookupUriOrHandler().toString();
		
		if (objProfile instanceof LessonTicketProfile)		
		{	
			LessonTicketProfile LessonTicketProfile = (LessonTicketProfile) objProfile;
			
			profileName = LessonTicketProfile.getProfileName();
			distributionKey = LessonTicketProfile.getDistributionKey();
			mailSubject = "Lesson Profile " + profileName;
			Map<String, Object> profile_groups = new LinkedHashMap<String, Object>();
			
			Map<String, CommandBeanTreeNode> mapList = node.getChild("LessonTicketProfileGroup");
			for (Iterator iter = mapList.entrySet().iterator(); iter.hasNext();) 
			{
				Map.Entry entry = (Map.Entry) iter.next();
				childNode = (CommandBeanTreeNode)entry.getValue();
				
				Object objGroup = childNode.getData();
				if (objGroup instanceof LessonTicketProfileGroup)		
				{
					LessonTicketProfileGroup lessonTicketProfileGroup = (LessonTicketProfileGroup) objGroup;
					
					Map<String, Object> profile_group = new LinkedHashMap<String, Object>();
					String matchingCriteria = this.getLookupValue(commandBean, childNode, LessonTicketProfileGroup.class.getSimpleName(), "elementContentLink", lessonTicketProfileGroup.getElementContentLink(), false, "");
					profile_group.put("groupNumber", lessonTicketProfileGroup.getLessonGroupNumber());
					profile_group.put("match", lessonTicketProfileGroup.getElementContentLink());
					profile_group.put("matchCriteria", (StringUtils.isNotBlank(matchingCriteria))?matchingCriteria + " (" + lessonTicketProfileGroup.getElementContentLink() + ")":"(No Matching Criteria)");
					
					Map<String, CommandBeanTreeNode> mapList2 = childNode.getChild("LessonTicketProfileDetail");
					
					Map<String, Object> group_details = new LinkedHashMap<String, Object>();
					for (Iterator iter2 = mapList2.entrySet().iterator(); iter2.hasNext();) 
					{
						Map.Entry entry2 = (Map.Entry) iter2.next();
						childNode2 = (CommandBeanTreeNode)entry2.getValue();
						
						Object objDetail = childNode2.getData();
						if (objDetail instanceof LessonTicketProfileDetail)		
						{
							LessonTicketProfileDetail lessonTicketProfileDetail = (LessonTicketProfileDetail) objDetail;
							
							Map<String, Object> group_detail = new LinkedHashMap<String, Object>();

							Map<String, LookupItem>categoryLookup = getLookupList(request, userSelection, categoryURI, null, true);
							Map<String, LookupItem>elementsLookup = getLookupList(request, userSelection, elementsURI, lessonTicketProfileDetail.getLessonTicketCategoryUid(), false);
							
							group_detail.put("category", (categoryLookup.get(lessonTicketProfileDetail.getLessonTicketCategoryUid()) != null)?categoryLookup.get(lessonTicketProfileDetail.getLessonTicketCategoryUid()).getValue().toString():"");
							group_detail.put("categoryUid", lessonTicketProfileDetail.getLessonTicketCategoryUid());
							group_detail.put("element", (elementsLookup.get(lessonTicketProfileDetail.getLessonTicketElementsUid()) != null)?elementsLookup.get(lessonTicketProfileDetail.getLessonTicketElementsUid()).getValue().toString():"");
							group_detail.put("elementUid", lessonTicketProfileDetail.getLessonTicketElementsUid());
							group_details.put(lessonTicketProfileDetail.getLessonTicketElementsUid(), group_detail);
						}
					}
					profile_group.put("details", group_details);
					profile_groups.put(lessonTicketProfileGroup.getLessonTicketProfileGroupUid(), profile_group);
				}	
			}
			params.put("profileName", profileName);
			params.put("groupDetails", profile_groups);
		}
		
		if(StringUtils.isNotBlank(distributionKey)) {
			
			String[] emailList = this.getEmailList(userSelection.getGroupUid(), distributionKey);
			
			try{
				if(StringUtils.isBlank(this.sendProfileTemplate)) {
					commandBean.getSystemMessage().addError("Unable to send profile because email template not assigned");
				} else {
					mailEngine.sendMail(new String[]{}, emailList, this.support_email, null, mailSubject, mailEngine.generateContentFromTemplate(this.sendProfileTemplate, params));
					commandBean.getSystemMessage().addInfo("Profile " + profileName + " have been successfully sent");
				}
			}catch (Exception e) {
				String errorMessage = StringUtils.replace(StringUtils.replace(e.toString(), ">", ""), "<", "");
				this.sendErrorEmail(commandBean, profileName, errorMessage);
				this.getLogger().error(e);
				commandBean.getSystemMessage().addError("Failed to send Profile " + profileName + "\n\n" + errorMessage);
			}
		}
	}
	
	private void sendErrorEmail(CommandBean commandBean, String profileName, String error) throws Exception
	{
		String[] errorReceipient={this.support_email};
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		
		//String errorMessage = error.toString();
		params.put("profileName", profileName);
		params.put("ErrorTrace", error);
		String errorMailSubject = "Error Sending Profile " + profileName;
		
		if(StringUtils.isBlank(this.sendErrorProfileTemplate)) {
			commandBean.getSystemMessage().addError("Unable to send profile because email template not assigned. Please contact support@idsdatanet.com");
		} else {
			mailEngine.sendMail(errorReceipient, this.support_email, this.support_email, 
				errorMailSubject, mailEngine.generateContentFromTemplate(this.sendErrorProfileTemplate, params));
		}
	}
	

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		
		
	}

	public void init(CommandBean commandBean) throws Exception {
		
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		
		
	}
	
	public void afterPropertiesSet() throws Exception
	{
		if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
		this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isBlank(support_email)) this.support_email = "support@idsdatanet.com";
		
	}

	public void afterChildClassNodesProcessed(String className,
			ChildClassNodesProcessStatus status, UserSession session,
			CommandBeanTreeNode parent) throws Exception {
		
		
	}

	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		
		
	}

	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		
		Object object = node.getData();
		if (object instanceof LessonTicketProfile) {
			
			LessonTicketProfile thisLessonTicketProfile = (LessonTicketProfile) object;
			
			String withDistributionKey = "0";
			if (StringUtils.isNotBlank(thisLessonTicketProfile.getDistributionKey())) {
				String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(userSelection.getGroupUid(), thisLessonTicketProfile.getDistributionKey());
				if(emailList.length > 0)  {
					withDistributionKey = "1";
				}
			}
			node.getDynaAttr().put("withDistributionKey", withDistributionKey);
		}
	}

	public void afterDataNodeProcessed(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, String action, int operationPerformed,
			boolean errorOccurred) throws Exception {
		
		
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		
		
	}

	public void afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		
	}

	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		
	}

	public void beforeDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		
	}

	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session) throws Exception {
		
		
	}

	public void onDataNodePasteAsNew(CommandBean commandBean,
			CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,
			UserSession session) throws Exception {
		
		
	}
	
	public void onDataNodePasteLastAsNew(CommandBean commandBean,
			CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,
			UserSession session) throws Exception {
		
		
	}

	public List<Object> preFilterLoadedRawChildRecords(List<Object> data,
			CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		return null;
	}
	
	private String[] getEmailList(String groupUid, String distributionKey) throws Exception {
		
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(groupUid, distributionKey);
		
		return emailList;
		
	}
	
	private Map<String, LookupItem> getLookupList(HttpServletRequest request, UserSelectionSnapshot userSelection, String uri, String key, Boolean isParent) {
		Map<String, LookupItem> result = null;
		try {
			if(isParent) {
				result = LookupManager.getConfiguredInstance().getLookup(uri, userSelection, null);
			} else {
				result = LookupManager.getConfiguredInstance().getCascadeLookup(uri, userSelection, key, null);	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	private String getLookupValue(CommandBean commandBean, CommandBeanTreeNode node, String className, String field, String value, Boolean isCascaded, String key) throws Exception
	{
		Map<String, LookupItem> thisLookupMap = commandBean.getLookupMap(node, className, field);
		if(isCascaded) 
		{	
			thisLookupMap = commandBean.getCascadeLookupMap(node, className, field, key);
		}
		
		if(thisLookupMap != null) 
		{
			LookupItem thisLookupItem = thisLookupMap.get(value);
			if(thisLookupItem != null)
			{
				return thisLookupItem.getValue().toString();
			}
		}
						
		return "";
	}
}
