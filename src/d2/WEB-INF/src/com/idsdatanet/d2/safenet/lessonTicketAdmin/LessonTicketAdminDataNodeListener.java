package com.idsdatanet.d2.safenet.lessonTicketAdmin;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.safenet.lessonTicket.LessonTicketUtils;

public class LessonTicketAdminDataNodeListener extends EmptyDataNodeListener {
	
	private Boolean autoPopulateUsernameToReportedByName = false;
	private String lessonStage = null;
	private String lessonReportType =null;
	private List<String> operationType;
	
	public void setOperationType(List<String> operationType)
	{
		if(operationType != null){
			this.operationType = new ArrayList<String>();
			for(String value: operationType){
				this.operationType.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getOperationType() {
		return this.operationType;
	}
	
	public void setLessonStage(String value){
		this.lessonStage = value;
	}
	
	public void setLessonReportType(String value){
		this.lessonReportType = value;
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			Well thisWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisLessonTicket.getWellUid());
			node.getDynaAttr().put("country", thisWell.getCountry());
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + thisLessonTicket.getLessonTicketNumber());
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				
				//auto populate case no 20100924-0539-slfu 
				//String newreportnumber = null;
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoNewLessonTicketNumber"))){
					
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					String prefixPattern = GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberPrefixPattern");
					
					String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
					
					if (StringUtils.isNotBlank(getPrefixName)){
						thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
					}
					//ticket#26804 gwp lessonTicketNumberByWWO select lesson ticket largest number by well/wellbore/operation based
					List list = null;
					String newreportnumber = null;
					String strCondition = null;
					if (this.lessonReportType == null){
						strCondition = " And (lessonReportType IS NULL OR lessonReportType = '')";
					}
					else{
						strCondition = " And lessonReportType = 'geo_lesson'";
					}
					
					if ("well".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
							list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellUid = :wellUid"+strCondition+" order by lessonTicketNumber DESC", "wellUid", userSelection.getWellUid());
					}
					if ("wellbore".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid"+strCondition+" order by lessonTicketNumber DESC", "wellboreUid", userSelection.getWellboreUid());
					}
					if ("operation".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and operationUid = :operationUid"+strCondition+" order by lessonTicketNumber DESC", "operationUid", userSelection.getOperationUid());
					}
					
					
					if (list.size() > 0) {
						Collections.sort(list, new LessonTicketNumberComparator());
						String lessonTicketNumber = null;
						Object a = list.get(0);
						if (a!=null) lessonTicketNumber = a.toString();
							
						if (StringUtils.isNotBlank(lessonTicketNumber) && StringUtils.isNumeric(lessonTicketNumber)) {
							Integer intEventRef = (Integer)Integer.parseInt(lessonTicketNumber) + 1;
					
							newreportnumber = intEventRef.toString();
							
							if (newreportnumber.length()== 1){
								newreportnumber = "0" + newreportnumber;
							}
						}else {
							newreportnumber = "01";
						}

					}else {
						newreportnumber = "01";
					}
					thisLessonTicket.setLessonTicketNumber(newreportnumber);
					thisLessonTicket.setLessonTicketType(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
					
				}
				
			}
		}
	
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
	}

	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			
			if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				thisLessonTicket.setRp2Date(daily.getDayDate());
			}
			
			if(StringUtils.isNotBlank(userSelection.getWellUid())){             
                thisLessonTicket.setWellUid(userSelection.getWellUid());                          
            }
			
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoNewLessonTicketNumber"))){
				String wwo = GroupWidePreference.getValue(userSelection.getGroupUid(), "lessonTicketNumberByWWO");
				//wwo = wwo.toUpperCase().substring(0, 1)+wwo.substring(1);
				//commandBean.getSystemMessage().addInfo("Current Lesson Learned Ticket Number Is Generated by "+wwo, true);
				commandBean.getSystemMessage().addInfo("Please select correct "+wwo+" when creating a lesson learned ticket.", true);
			}
			
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoPopulateUsernameToOriginator"))){
				//save the current user name who created the lesson learned records 
				String fullname="";
				fullname = LessonTicketUtils.getUserFullName(userSelection);
				String rp2Originator="";
				rp2Originator = thisLessonTicket.getRp2Originator();
				if(rp2Originator==null || rp2Originator.equalsIgnoreCase(""))
				{
					if(fullname!=null || StringUtils.isNotBlank(fullname)){
						thisLessonTicket.setRp2Originator(fullname);
					}
				}
			}
			
			if (autoPopulateUsernameToReportedByName){
				//save the current user name who created the lesson learned records 
				String fullname="";
				fullname = LessonTicketUtils.getUserFullName(userSelection);
				String reportedbyName="";
				reportedbyName = thisLessonTicket.getReportedbyName();
				if(reportedbyName==null || reportedbyName.equalsIgnoreCase("")){
					if(fullname!=null || StringUtils.isNotBlank(fullname)){
						thisLessonTicket.setReportedbyName(fullname);
					}
				}
			}
			
		}
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			LessonTicketUtils.deleteActivityLessonTicketLink(thisLessonTicket.getLessonTicketUid());

		}
	}
	
	private class LessonTicketNumberComparator implements Comparator<String>{
		public int compare(String s1, String s2){
			try{
				String f1 = WellNameUtil.getPaddedStr(s1.toString());
				String f2 = WellNameUtil.getPaddedStr(s2.toString());
				
				if (f1 == null || f2 == null) return 0;
				//refine the ticket# sorting order, numeric(descending) first then alphanumeric(descending)
				if (StringUtils.isBlank(f1.toString()) || StringUtils.isBlank(f2.toString())) return 0;
				if (!StringUtils.isNumeric(f1.toString()) && StringUtils.isAlphanumeric(f1.toString()) && StringUtils.isNumeric(f2.toString())) return 1;
				if (!StringUtils.isNumeric(f2.toString()) && StringUtils.isAlphanumeric(f2.toString()) && StringUtils.isNumeric(f1.toString())) return -1;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}

	}

}


