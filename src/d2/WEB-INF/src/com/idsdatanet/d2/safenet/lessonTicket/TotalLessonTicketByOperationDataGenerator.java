package com.idsdatanet.d2.safenet.lessonTicket;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class TotalLessonTicketByOperationDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
			// TODO Auto-generated method stub
		
				//get the current date from the user selection
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				
				if (userContext.getUserSelection().getOperationUid() != null){
		
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
							
					String strSql = "SELECT lt.operationUid, COUNT(*) FROM LessonTicket lt, Daily d WHERE lt.dailyUid = d.dailyUid " +
							"AND (lt.isDeleted = false or lt.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND d.dayDate <= :currentDate " +
							"AND lt.operationUid = :operationUid GROUP BY lt.operationUid";
					String[] paramsFields2 = {"currentDate","operationUid"};
					Object[] paramsValues2 = {thisCalendar.getTime(),userContext.getUserSelection().getOperationUid()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);										
					
					String totalTicket = null;
					if (lstResult.size() > 0) {
						
						for(Object objResult: lstResult){
							Object[] obj = (Object[]) objResult;
							totalTicket = (obj[1] != null) ? obj[1].toString() : "";
							
							ReportDataNode thisReportNode = reportDataNode.addChild("TotalLessonTicketByOperation");
							
							thisReportNode.addProperty("operationUid", obj[0].toString());
							thisReportNode.addProperty("totalLessonTicket", totalTicket);
						}	
					}
				}
			
		}

	
}

