package com.idsdatanet.d2.safenet.safetyTicket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.SafetyTicket;
import com.idsdatanet.d2.core.model.SafetyTicketDetail;
import com.idsdatanet.d2.core.model.StopCardParticipation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SafetyTicketDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener{
	
	private Boolean allowZeroValues = false;

	public void onSubmitForServerSideProcess(CommandBean commandBean,HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// Temporary disable feature to auto populate category and detail when add new category button is click.
		/*
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			if (request!=null){
				if(StringUtils.isNotBlank(request.getParameter("target_class"))){
					if ("SafetyTicketCategory".equals(request.getParameter("target_class"))) {
						
						if (targetCommandBeanTreeNode !=null) {
							
							if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(SafetyTicket.class)){
								
								UserSession session = UserSession.getInstance(request);
								String safetyCategoryLookup="db://LookupSafetyCategory?key=lookupSafetyCategoryUid&value=sequence&orderBy=sequence";
								Map<String,LookupItem> safetyCategoryLookupList = LookupManager.getConfiguredInstance().getLookup(safetyCategoryLookup, new UserSelectionSnapshot(session), null);
								String safetyDetailsLookup="db://LookupSafetySubCategory?key=lookupSafetySubCategoryUid&value=sequence&parent=category&orderBy=sequence";
								
								Map<String, CommandBeanTreeNode> safetyTicketCategory = targetCommandBeanTreeNode.getChild("SafetyTicketCategory");
								Map<String, CommandBeanTreeNode> categoryUidList = new LinkedHashMap<String,CommandBeanTreeNode>();
								for (Map.Entry<String, CommandBeanTreeNode> entry3 : safetyTicketCategory.entrySet()) {
									CommandBeanTreeNode categoryNode = entry3.getValue();
									SafetyTicketCategory data = (SafetyTicketCategory) categoryNode.getData();
									categoryUidList.put(data.getLookupCategoryUid(),categoryNode);
								}
								
								for (Map.Entry<String, LookupItem> entry : safetyCategoryLookupList.entrySet()) {
									String categoryUid = entry.getKey();
									Integer sequenceCat = null;
									Integer sequenceDet = null;
									if (entry.getValue().getValue() != null) sequenceCat = Integer.parseInt(entry.getValue().getValue().toString());
									if (StringUtils.isNotBlank(categoryUid)) {
										//IF category is not found
										if(categoryUidList.get(categoryUid)==null) {
											SafetyTicketCategory newSafetyTicketCategory = new SafetyTicketCategory();
											newSafetyTicketCategory.setLookupCategoryUid(categoryUid);
											newSafetyTicketCategory.setSequence(sequenceCat);
											CommandBeanTreeNode nodeCat = targetCommandBeanTreeNode.addCustomNewChildNodeForInput(newSafetyTicketCategory);
											
											Map<String,LookupItem> safetyDetailsLookupList = LookupManager.getConfiguredInstance().getCascadeLookup(safetyDetailsLookup, new UserSelectionSnapshot(session), categoryUid, null);
											for (Map.Entry<String, LookupItem> entry2 : safetyDetailsLookupList.entrySet()) {
												String detailUid = entry2.getKey();
												if (entry2.getValue().getValue() != null) sequenceDet = Integer.parseInt(entry2.getValue().getValue().toString());
												SafetyTicketDetail newSafetyTicketDetail = new SafetyTicketDetail();
												newSafetyTicketDetail.setCategory(categoryUid);
												newSafetyTicketDetail.setSubCategory(detailUid);
												newSafetyTicketDetail.setSequence(sequenceDet);
												nodeCat.addCustomNewChildNodeForInput(newSafetyTicketDetail);
													
											}
										} 
										//IF category found
										else {
											CommandBeanTreeNode nodeCat = categoryUidList.get(categoryUid);
											Map<String, CommandBeanTreeNode> safetyTicketDetail = nodeCat.getChild("SafetyTicketDetail");
											Map<String, CommandBeanTreeNode> detailUidList = new LinkedHashMap<String,CommandBeanTreeNode>();
											for (Map.Entry<String, CommandBeanTreeNode> detailEntry : safetyTicketDetail.entrySet()) {
												CommandBeanTreeNode detailNode = detailEntry.getValue();
												SafetyTicketDetail data = (SafetyTicketDetail) detailNode.getData();
												detailUidList.put(data.getSubCategory(),detailNode);
											}
											
											Map<String,LookupItem> safetyDetailsLookupList = LookupManager.getConfiguredInstance().getCascadeLookup(safetyDetailsLookup, new UserSelectionSnapshot(session), categoryUid, null);
											for (Map.Entry<String, LookupItem> entry2 : safetyDetailsLookupList.entrySet()) {
												String detailUid = entry2.getKey();
												if (entry2.getValue().getValue() != null) sequenceDet = Integer.parseInt(entry2.getValue().getValue().toString());
												//IF detail not found
												if (detailUidList.get(detailUid)==null) {
													SafetyTicketDetail newSafetyTicketDetail = new SafetyTicketDetail();
													newSafetyTicketDetail.setCategory(categoryUid);
													newSafetyTicketDetail.setSubCategory(detailUid);
													newSafetyTicketDetail.setSequence(sequenceDet);
													nodeCat.addCustomNewChildNodeForInput(newSafetyTicketDetail);
												}
													
											}
										}
									}
								}
								commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
							}
							
						}
					}
				}
			}
		}*/
	}

	public Boolean getAllowZeroValues(){
		return allowZeroValues;
	}
	
	public void setAllowZeroValues(Boolean value){
		this.allowZeroValues = value;
	}
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		String rigInformationUid = userSelection.getRigInformationUid();
		RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
		String rigType = rig.getRigSubType();
		commandBean.getRoot().getDynaAttr().put("rigType", rigType);
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof StopCardParticipation) {
			StopCardParticipation stopCardParticipation = (StopCardParticipation) object;
			if (stopCardParticipation.getNoOfCards() != null && stopCardParticipation.getNoOfCards() < 0){
				status.setFieldError(node, "noOfCards", "The record is not saved if No. Of Cards is less than 0. ");
				status.setContinueProcess(false, true);
				return;
			}
		}
			
		if (this.allowZeroValues){
			if (object instanceof SafetyTicketDetail) {
				SafetyTicketDetail safetyticketdetail = (SafetyTicketDetail) object;
				if((safetyticketdetail.getNumSafeTicket()==null) || (safetyticketdetail.getNumSafeTicket() < 0)){
					status.setFieldError(node, "numSafeTicket", "The record is not saved if Safe or Unsafe Events is less than 0 or blank.");
					status.setContinueProcess(false, true);
				}
				if((safetyticketdetail.getNumUnsafeTicket()==null) || (safetyticketdetail.getNumUnsafeTicket() < 0)){
					status.setFieldError(node, "numUnsafeTicket", "The record is not saved if Safe or Unsafe Events is less than 0 or blank.");
					status.setContinueProcess(false, true);
					return;
				}
				else if (((safetyticketdetail.getNumUnsafeTicket()==null) && (safetyticketdetail.getNumUnsafeTicket() < 0)) && ((safetyticketdetail.getNumSafeTicket()==null) && (safetyticketdetail.getNumSafeTicket() < 0))){
						status.setFieldError(node, "numSafeTicket", "The record is not saved if Safe or Unsafe Events is less than 0 or blank.");
						status.setFieldError(node, "numUnsafeTicket", "The record is not saved if Safe or Unsafe Events is less than 0 or blank.");
						status.setContinueProcess(false, true);
						return;
				}
				
			}
		}
		else {	
			if (object instanceof SafetyTicketDetail) {
				SafetyTicketDetail safetyticketdetail = (SafetyTicketDetail) object;
				if((safetyticketdetail.getNumSafeTicket()!=null && safetyticketdetail.getNumSafeTicket() <= 0) || (safetyticketdetail.getNumUnsafeTicket()!=null && safetyticketdetail.getNumUnsafeTicket() <= 0)) {
					status.setFieldError(node, "numSafeTicket", "The record is not saved if Safe and Unsafe Events is less than 0 or 0 or blank. ");
					status.setFieldError(node, "numUnsafeTicket", "The record is not saved if Safe and Unsafe Events is less than 0 or 0 or blank. ");
					status.setContinueProcess(false, true);
					return;	
				}else if((safetyticketdetail.getNumSafeTicket()==null || safetyticketdetail.getNumSafeTicket() <= 0) && (safetyticketdetail.getNumUnsafeTicket()==null || safetyticketdetail.getNumUnsafeTicket() <= 0)) {
					status.setFieldError(node, "numSafeTicket", "The record is not saved if Safe and Unsafe Events is less than 0 or 0 or blank. ");
					status.setFieldError(node, "numUnsafeTicket", "The record is not saved if Safe and Unsafe Events is less than 0 or 0 or blank. ");
					status.setContinueProcess(false, true);
					return;				
				}
			
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof SafetyTicket) {
			String rigInformationUid = userSelection.getRigInformationUid();
			RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
			String rigType = rig.getRigSubType();
			node.getDynaAttr().put("rigType", rigType);
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof SafetyTicket) {
			String rigInformationUid = userSelection.getRigInformationUid();
			RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
			String rigType = rig.getRigSubType();
			node.getDynaAttr().put("rigType", rigType);
		}
	}
}
