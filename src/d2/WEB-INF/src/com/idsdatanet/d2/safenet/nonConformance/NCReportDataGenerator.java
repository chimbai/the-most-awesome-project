package com.idsdatanet.d2.safenet.nonConformance;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class NCReportDataGenerator implements ReportDataGenerator {

	private static final String[] imageExtensions = new String[]{"jpeg","jpg","gif","png","bmp"};
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = userContext.getUserSelection();

		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, daily.getWellUid());
		
		if (well!=null){
			 
			//FlexNodeFilesAttachment
			if (well.getWellUid()!=null){
				ReportDataNode child = reportDataNode.addChild("NCR");
				
				String strSql = "FROM NonconformanceEvent WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND wellUid = :wellUid ";
				List arrayParamNames = new ArrayList();
				List arrayParamValues = new ArrayList();
				
				arrayParamNames.add("wellUid");
				arrayParamValues.add(well.getWellUid());
				
				if (userSelection.getCustomProperties()!=null){
					String nonconformanceEventUid = (String) userSelection.getCustomProperties().get("nonconformanceEventUid");
					
					if(StringUtils.isNotBlank(nonconformanceEventUid)){
						strSql += "AND (nonconformanceEventUid = :nonconformanceEventUid) "; 
						arrayParamNames.add("nonconformanceEventUid");
						arrayParamValues.add(nonconformanceEventUid);
					}
				}
				
				strSql += "ORDER BY eventNumberPrefix DESC, eventNumber DESC"; 
				
				String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
				Object[] paramValues = arrayParamValues.toArray();
				
				List<NonconformanceEvent> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				
				if (result.size() > 0){
					for (NonconformanceEvent event : result){
						
						int reportColumn = 2;
						String key_value = "NonconformanceEvent" + ":" + event.getNonconformanceEventUid();
						
						StringWriter queryWriter = new StringWriter();		
						queryWriter.append("from FileManagerFiles where (isDeleted = false or isDeleted is null) and (popupAttachment = true) and externalModule = :externalModule and externalSubModule = :externalSubModule and externalKey1 = :key AND (");
						for (int i=0;i < imageExtensions.length;i++){
							if (i==0){
								queryWriter.append("fileName like '%." + imageExtensions[i] + "' ");
							} else {
								queryWriter.append("OR fileName like '%." + imageExtensions[i] +  "' ");
							}
						}
						queryWriter.append(") ORDER BY uploadedDatetime");
						
						List<FileManagerFiles> list = (List<FileManagerFiles>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryWriter.toString(), new String[] {"externalModule","externalSubModule","key"}, new Object[] {"FlexNodeFilesAttachment","nonConformance_alt2_Controller",key_value});
						
						if (list.size() > 0){
							Integer column = 0;
							Boolean newRow = true;
							
							for (FileManagerFiles fm : list){					
								if (newRow) child = reportDataNode.addChild("FlexNodeFilesAttachment");
								
								child.addProperty("nonconformanceEventUid", event.getNonconformanceEventUid());					
								child.addProperty("eventNumberPrefix", event.getEventNumberPrefix());	
								child.addProperty("eventNumber", event.getEventNumber());	
								
								child.addProperty("fileFullPathName_"+column,fm.getFileFullPathName());					
								child.addProperty("fileManagerFilesUid_"+column,fm.getFileManagerFilesUid());	
								child.addProperty("fileName_"+column,fm.getFileName());	
								
								column ++;
								if (column>reportColumn){
									column=0;
									newRow= true;
								}else{
									newRow= false;
								}
								
								child.addProperty("column", column.toString());			
							}				
						}else{
							/*
							for (Integer i=1;i<=1;i++){
								child = reportDataNode.addChild("FlexNodeFilesAttachment");
								child.addProperty("column", i.toString());
							}*/	
						}
					}
				}								
			}
		}
	}

}
