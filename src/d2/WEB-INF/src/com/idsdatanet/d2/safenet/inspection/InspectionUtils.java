package com.idsdatanet.d2.safenet.inspection;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.log.CommandLoggingContext;
import com.idsdatanet.d2.core.log.CommandLoggingControl;
import com.idsdatanet.d2.core.model.InspectionResult;
import com.idsdatanet.d2.core.model.LookupInspectionCategory;
import com.idsdatanet.d2.core.model.LookupInspectionCheckType;
import com.idsdatanet.d2.core.model.LookupInspectionCheckTypeValue;
import com.idsdatanet.d2.core.model.LookupInspectionDetail;
import com.idsdatanet.d2.core.model.LookupInspectionType;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class InspectionUtils extends BlazeRemoteClassSupport {
	
	public String getInspectionType() {
		String xml = "";
		try {
			List<LookupInspectionType> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().getObjects(LookupInspectionType.class);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			for (LookupInspectionType rec:rs) {
				if (rec.getIsDeleted() == null) rec.setIsDeleted(false);
				if (rec.getIsDeleted()) continue;
				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("inspectionTypeName", rec.getInspectionTypeName());
				atts.addAttribute("inspectionTypeCode", rec.getInspectionTypeCode());
				atts.addAttribute("lookupInspectionTypeUid", rec.getLookupInspectionTypeUid());
				writer.addElement("LookupInspectionType", null, atts);
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			xml = "<root/>";
		}
		//System.out.println(xml);
		return xml;
	}
	
	public String getInspectionCategory(String lookupInspectionTypeUid) {
		String xml = "";
		try {
			String strSql = "FROM LookupInspectionCategory WHERE (isDeleted = false or isDeleted is null) AND lookupInspectionTypeUid=:lookupInspectionTypeUid ORDER BY sequence";
			List<LookupInspectionCategory> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lookupInspectionTypeUid", lookupInspectionTypeUid);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			for (LookupInspectionCategory rec:rs) {
				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("sequence", rec.getSequence().toString());
				atts.addAttribute("inspectionCategoryName", rec.getInspectionCategoryName());
				atts.addAttribute("lookupInspectionCategoryUid", rec.getLookupInspectionCategoryUid());
				writer.addElement("LookupInspectionCategory", null, atts);
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			xml = "<root/>";
		}
		//System.out.println(xml);
		return xml;
	}
	
	public String getInspectionCheckType(String lookupInspectionTypeUid) {
		String xml = "";
		try {
			String strSql = "FROM LookupInspectionCheckType WHERE (isDeleted = false or isDeleted is null) AND lookupInspectionTypeUid=:lookupInspectionTypeUid ORDER BY sequence";
			List<LookupInspectionCheckType> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lookupInspectionTypeUid", lookupInspectionTypeUid);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			for (LookupInspectionCheckType rec:rs) {
				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("sequence", rec.getSequence().toString());
				atts.addAttribute("inspectionCheckTypeName", rec.getInspectionCheckTypeName());
				atts.addAttribute("lookupInspectionCheckTypeUid", rec.getLookupInspectionCheckTypeUid());
				atts.addAttribute("inputType", rec.getInputType());
				writer.startElement("LookupInspectionCheckType", atts);

				strSql = "FROM LookupInspectionCheckTypeValue WHERE (isDeleted = false or isDeleted is null) AND lookupInspectionCheckTypeUid=:lookupInspectionCheckTypeUid ORDER BY sequence";
				List<LookupInspectionCheckTypeValue> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lookupInspectionCheckTypeUid", rec.getLookupInspectionCheckTypeUid());
				for (LookupInspectionCheckTypeValue rec2:rs2) {
					SimpleAttributes atts2 = new SimpleAttributes();
					atts2.addAttribute("sequence", rec2.getSequence().toString());
					atts2.addAttribute("lookupCode", rec2.getLookupCode());
					atts2.addAttribute("lookupValue", rec2.getLookupValue());
					writer.addElement("LookupInspectionCategoryValue", null, atts2);
				}
				writer.endElement();
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			xml = "<root/>";
		}
		//System.out.println(xml);
		return xml;
	}
	
	public String getInspectionDetail(String lookupInspectionTypeUid) {
		String xml = "";
		try {
			String strSql = "FROM LookupInspectionDetail WHERE (isDeleted = false or isDeleted is null) AND lookupInspectionTypeUid=:lookupInspectionTypeUid ORDER BY sequence";
			List<LookupInspectionDetail> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lookupInspectionTypeUid", lookupInspectionTypeUid);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			for (LookupInspectionDetail rec:rs) {
				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("sequence", rec.getSequence().toString());
				atts.addAttribute("lookupInspectionDetailUid", rec.getLookupInspectionDetailUid());
				atts.addAttribute("inspectionDetail", rec.getInspectionDetail());
				atts.addAttribute("lookupInspectionCategoryUid", rec.getLookupInspectionCategoryUid());
				atts.addAttribute("lookupInspectionTypeUid", rec.getLookupInspectionTypeUid());
				atts.addAttribute("lookupInspectionCheckTypeUid", rec.getLookupInspectionCheckTypeUid());
				atts.addAttribute("hasInput", rec.getHasInput().toString());
				writer.addElement("LookupInspectionDetail", null, atts);
			}
			writer.close();
			xml = bytes.toString();
		} catch (Exception e) {
			xml = "<root/>";
		}
		//System.out.println(xml);
		return xml;
	}
	
	public String getInspectionResult(String inspectionResultUid) {
		String xml = "";
		try {
			InspectionResult rec = (InspectionResult) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InspectionResult.class, inspectionResultUid);
			if (rec!=null) {
				xml = rec.getInspectionResult();
			}
		} catch (Exception e) {
			xml = "<root/>";
		}
		//System.out.println(xml);
		return xml;
	}
	
	public String saveInspectionResult(String inspectionTypeUid, String xmlData, String inspectionResultUid) {
		String resultUid = "";
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {
				UserSession session = this.getCurrentUserSession();
				String dailyUid = session.getCurrentDailyUid();
				if (dailyUid==null) return "";
				if ("".equals(dailyUid)) return "";
				
				InspectionResult rec = (InspectionResult) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InspectionResult.class, inspectionResultUid);
				if (rec==null) rec = new InspectionResult();
				rec.setInspectionResult(xmlData);
				rec.setDailyUid(dailyUid);
				rec.setLookupInspectionTypeUid(inspectionTypeUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);			
				
				resultUid = rec.getInspectionResultUid();
			} catch (Exception e) {
			
				return "";
			}finally{
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			
			return "";
		}
		return resultUid;
	}
	
	public Boolean deleteInspectionDocument(String inspectionResultUid)  {
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {
				if (inspectionResultUid==null) inspectionResultUid = "";
				InspectionResult rec = (InspectionResult) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(InspectionResult.class, inspectionResultUid);
				if (rec!=null) {
					rec.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}
	
	public String getInspectionResultUid(String inspectionTypeCode) {
		try {
			UserSession session = this.getCurrentUserSession();
			String dailyUid = session.getCurrentDailyUid();
			String[] paramNames = {"dailyUid","inspectionTypeCode"};
			String[] paramValues = {dailyUid,inspectionTypeCode};
			String strSql ="from InspectionResult r, LookupInspectionType l where (l.isDeleted=false or l.isDeleted is null) and (r.isDeleted=false or r.isDeleted is null) and r.dailyUid=:dailyUid and l.lookupInspectionTypeUid=r.lookupInspectionTypeUid and l.inspectionTypeCode=:inspectionTypeCode";
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
			if (rs.size()>0) {
				Object[] result = (Object[])rs.get(0);
				InspectionResult ir = (InspectionResult) result[0];
				if (ir.getInspectionResultUid()!=null) {
					return ir.getInspectionResultUid(); 
				} 
			}
			return "";
		} catch (Exception e) {
			return "";
		}
		
	}
	
	public Boolean getOperationIsLocked(){
		try{
			UserSession session = this.getCurrentUserSession();
			Operation operation = session.getCurrentOperation();
	
			if (BooleanUtils.isTrue(operation.getIsLocked())){
				return true;
			}else{
				return false;
			}
		}catch (Exception e){
			return false;
		}
		
	}
}
