package com.idsdatanet.d2.safenet.environmentDischarges;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.LookupEnvironmentDischarges;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;



public class EnvironmentDischargesUtil {

	/**
	 * Method for calculate sum of target and contingent
	 * @param commandBean
	 * @param node
	 * @throws Exception
	 */
	
	public static void calculateSumTargetContingent(CommandBean commandBean, CommandBeanTreeNode node) throws Exception{
		
		Object object = node.getData();
		
		if (object instanceof EnvironmentDischarges) {
			
			EnvironmentDischarges environmentDischarges = (EnvironmentDischarges)object;
			Double targetAmount = 0.0;
			Double baselineIncidentalAmount = 0.0;
			Double sumTargetAndBaselineIncidentalAmount = 0.0;
			
			if (environmentDischarges.getTargetAmount() != null && StringUtils.isNotBlank(environmentDischarges.getTargetAmount().toString())) {
				targetAmount = environmentDischarges.getTargetAmount();
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "targetAmount");
				thisConverter.setBaseValueFromUserValue(targetAmount);
				targetAmount = thisConverter.getBasevalue();
			}
			if (environmentDischarges.getBaselineIncidentalAmount() != null && StringUtils.isNotBlank(environmentDischarges.getBaselineIncidentalAmount().toString())) {
				baselineIncidentalAmount = environmentDischarges.getBaselineIncidentalAmount();
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "baselineIncidentalAmount");
				thisConverter.setBaseValueFromUserValue(baselineIncidentalAmount);
				baselineIncidentalAmount = thisConverter.getBasevalue();
			}
			
			sumTargetAndBaselineIncidentalAmount = targetAmount + baselineIncidentalAmount;
			
			if (sumTargetAndBaselineIncidentalAmount!=null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "targetAndBaselineIncidentalAmount");
				thisConverter.setBaseValue(sumTargetAndBaselineIncidentalAmount);
				sumTargetAndBaselineIncidentalAmount = thisConverter.getConvertedValue();
				environmentDischarges.setTargetAndBaselineIncidentalAmount(sumTargetAndBaselineIncidentalAmount);
			}
		}
	}
	
	
	/**
	 * Method for calculate Total For Well for each Environment Discharges
	 * @param commandBean
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	
	public static void calculateSumTotalForWell(CommandBean commandBean, CommandBeanTreeNode node) throws Exception{
		
		Object object = node.getData();
		if(object instanceof EnvironmentDischarges) {
			EnvironmentDischarges environmentDischarges = (EnvironmentDischarges)object;
			String currentClass = environmentDischarges.getCategory();
			if(currentClass != null) {
				if(!currentClass.equals("wastes")) {
					LookupEnvironmentDischarges lookupEnvironmentDischarges = (LookupEnvironmentDischarges) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupEnvironmentDischarges.class, environmentDischarges.getLookupEnvironmentDischargesUid());
					if(lookupEnvironmentDischarges != null && !StringUtils.equalsIgnoreCase(lookupEnvironmentDischarges.getName(), "OOC (wet) (Average)") && !StringUtils.equalsIgnoreCase(lookupEnvironmentDischarges.getName(), "Well OOC (wet average)") ) {
						String lookupEnvironmentDischargesUid = environmentDischarges.getLookupEnvironmentDischargesUid();
						String thisWellUid = environmentDischarges.getWellUid();
						String thisDailyUid = environmentDischarges.getDailyUid();
						Date currentReportDateTime = ApplicationUtils.getConfiguredInstance().getCachedDaily(environmentDischarges.getDailyUid()).getDayDate();
			 
						Double sumTotalActualDayPrior =  calculateTotalWellUpToDate (thisWellUid, lookupEnvironmentDischargesUid, currentReportDateTime, thisDailyUid);
						Double totalWellCurrentNode = calculateTotalWellCurrentNode (commandBean, node, false, false);
						
						Double sumTotalWellPriorToToday = sumTotalActualDayPrior + totalWellCurrentNode;
						if(!Action.DELETE.equals(node.getAtts().getAction())){
							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualTotalWellAmount");
							thisConverter.setBaseValue(sumTotalWellPriorToToday);
							environmentDischarges.setActualTotalWellAmount(thisConverter.getConvertedValue());
						}
					
						updateTotalWellSameDayDate (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
						updateTotalWellAfterToday (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
					}else {
//						Double actualIncidentalAmount = 0.0;
//						Double actualAmount = 0.0;
//						Double actualUnplannedAmount = 0.0;	
//						
//						if (environmentDischarges.getActualIncidentalAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualIncidentalAmount().toString())) {
//							actualIncidentalAmount = environmentDischarges.getActualIncidentalAmount();
//						}
//						if (environmentDischarges.getActualAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualAmount().toString())) {
//							actualAmount = environmentDischarges.getActualAmount();
//						}
//						if (environmentDischarges.getActualUnplannedAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualUnplannedAmount().toString())) {
//							actualUnplannedAmount = environmentDischarges.getActualUnplannedAmount();
//						}
//						Double actualTotalWellAmount = actualIncidentalAmount + actualAmount + actualUnplannedAmount;
//						environmentDischarges.setActualTotalWellAmount(actualTotalWellAmount);
						
						String lookupEnvironmentDischargesUid = environmentDischarges.getLookupEnvironmentDischargesUid();
						String thisWellUid = environmentDischarges.getWellUid();
						String thisDailyUid = environmentDischarges.getDailyUid();
						Date currentReportDateTime = ApplicationUtils.getConfiguredInstance().getCachedDaily(environmentDischarges.getDailyUid()).getDayDate();
						
						//ticket number #15883 update Total well for "Well OOC Average"
						Double sumTotalActualDayPrior = 0.0;
						Double totalWellCurrentNode = 0.0;
						Double sumTotalWellPriorToToday = 0.0;
						
						if(lookupEnvironmentDischarges != null && StringUtils.equalsIgnoreCase(lookupEnvironmentDischarges.getName(), "OOC (wet) (Average)")) {
							 sumTotalActualDayPrior =  calculateTotalWellUpToDate (thisWellUid, lookupEnvironmentDischargesUid, currentReportDateTime, thisDailyUid);
							 totalWellCurrentNode = calculateTotalWellCurrentNode (commandBean, node, true, false);
							
							 sumTotalWellPriorToToday =  totalWellCurrentNode;
						}
						else {
							sumTotalActualDayPrior =  calculateTotalWellUpToDate (thisWellUid, lookupEnvironmentDischargesUid, currentReportDateTime, thisDailyUid);
							 totalWellCurrentNode = calculateTotalWellCurrentNode (commandBean, node, false, true);
							
							 sumTotalWellPriorToToday =  totalWellCurrentNode;
						}
						
						if(!Action.DELETE.equals(node.getAtts().getAction())){
							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualTotalWellAmount");
							thisConverter.setBaseValue(sumTotalWellPriorToToday);
							environmentDischarges.setActualTotalWellAmount(thisConverter.getConvertedValue());
						}
					
						updateTotalWellSameDayDate (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
						updateTotalWellAfterToday (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
					}
				} else if(currentClass.equals("wastes")) {
				//	environmentDischarges.setActualTotalWellAmount(environmentDischarges.getActualAmount());
					String lookupEnvironmentDischargesUid = environmentDischarges.getLookupEnvironmentDischargesUid();
					String thisWellUid = environmentDischarges.getWellUid();
					String thisDailyUid = environmentDischarges.getDailyUid();
					Date currentReportDateTime = ApplicationUtils.getConfiguredInstance().getCachedDaily(environmentDischarges.getDailyUid()).getDayDate();
		 
					Double sumTotalActualDayPrior =  calculateTotalWellUpToDate (thisWellUid, lookupEnvironmentDischargesUid, currentReportDateTime, thisDailyUid);
					Double totalWellCurrentNode = calculateTotalWellCurrentNode (commandBean, node, false, false);
					
					Double sumTotalWellPriorToToday = sumTotalActualDayPrior + totalWellCurrentNode;
					if(!Action.DELETE.equals(node.getAtts().getAction())){
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualTotalWellAmount");
						thisConverter.setBaseValue(sumTotalWellPriorToToday);
						environmentDischarges.setActualTotalWellAmount(thisConverter.getConvertedValue());
					}
				
					updateTotalWellSameDayDate (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
					updateTotalWellAfterToday (thisWellUid, currentReportDateTime, thisDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
				}
			}
		}
	}
	
	
	/**
	 * Method for calculate Total for Well for each affected Environment Discharges in case report daily day date is changed or deleted
	 * @param commandBean
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	
	public static void calculateTotalForWellAfterDayChanged(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		
		Object object = node.getData();
		
		if(object instanceof ReportDaily) {
			ReportDaily thisReport = (ReportDaily) object;
			String affectedDailyUid = thisReport.getDailyUid();
			Date afectedReportDateTime = thisReport.getReportDatetime();
			String thisWellUid = session.getCurrentWellUid();
			
			String strSql = "SELECT lookupEnvironmentDischargesUid FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND dailyUid = :affectedDailyUid " ;
			List affectedDischargesList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,"affectedDailyUid",affectedDailyUid);
			
			if(affectedDischargesList.size()>0){
				for(Object item : affectedDischargesList){
					String affectedEnvironmentDischargesUid = item.toString();

					Double sumTotalWellPriorToToday = 0.0;
					sumTotalWellPriorToToday = calculateTotalWellUpToDate (thisWellUid, affectedEnvironmentDischargesUid, afectedReportDateTime, affectedDailyUid);
					
					updateTotalWellSameDayDate (thisWellUid, afectedReportDateTime, affectedDailyUid, affectedEnvironmentDischargesUid, sumTotalWellPriorToToday);
					updateTotalWellAfterToday (thisWellUid, afectedReportDateTime, affectedDailyUid, affectedEnvironmentDischargesUid, sumTotalWellPriorToToday);
					
				}
			}
		}
	}
	
	
	/**
	 * Method for calculate Total for Well for each affected Environment Discharges in case well-bore or operation is deleted
	 * @param commandBean
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	
	public static void calculateTotalWellAfterWellOpsDeleted (CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		
		Object object = node.getData();
		String thisWellUid = session.getCurrentWellUid();
		
		String conditionField = "operationUid";
		String conditionValue = "";
		
		if(object instanceof Wellbore) {
			Wellbore thisWellbore = (Wellbore) object;
			 conditionField = "wellboreUid";
			 conditionValue = thisWellbore.getWellboreUid();
		}
		if(object instanceof Operation) {
			Operation thisOperation = (Operation) object;
			 conditionField = "operationUid";
			 conditionValue = thisOperation.getOperationUid();
		}
		List<ReportDaily> recalculateDayList = getRecalculateDayList (conditionField, conditionValue, thisWellUid);
		if(recalculateDayList!=null && recalculateDayList.size()>0){
			List<String> dischargesList = null;
			dischargesList =  getDischargesListFromDayList (recalculateDayList);
			if(dischargesList!=null && dischargesList.size()>0){
				updateTotalWellAfterWellOpsDeleted (dischargesList, recalculateDayList, thisWellUid, conditionField, conditionValue);
			}
		}
	}
	
	
	/**
	 * Sum Total For Well from first day of current well up to yesterday or today (if there exists overlapping day)
	 * For same environment discharges type
	 */
	
	private static Double calculateTotalWellUpToDate (String wellUid, String lookupEnvironmentDischargesUid, Date currentReportDateTime, String dailyUid) throws Exception{
	
		String strSql = "SELECT SUM(actualAmount), SUM(actualIncidentalAmount), SUM(actualUnplannedAmount) FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) " + 
					"AND wellUid = :thisWellUid AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid " +
					"AND dailyUid IN (SELECT dailyUid FROM ReportDaily WHERE wellUid = :wellUid AND reportDatetime <= :currentReportDateTime AND dailyUid != :currentDailyUid AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY reportDatetime DESC)";
		String[] paramsFields = {"thisWellUid", "lookupEnvironmentDischargesUid", "wellUid", "currentReportDateTime", "currentDailyUid"};
		Object[] paramsValues = {wellUid, lookupEnvironmentDischargesUid, wellUid, currentReportDateTime, dailyUid};

		List priorTodayList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields,paramsValues);
		Double actualAmountPrior = 0.0;
		Double actualIncidentalAmountPrior = 0.0;
		Double actualUnplannedAmountPrior = 0.0;
		Double sumTotalActualDayPrior = 0.0;

		if(priorTodayList.size()>0){
			for (int prior=0; prior<priorTodayList.size(); prior++){
				Object[] priorTodayObj = (Object[]) priorTodayList.get(prior);
				actualAmountPrior = (priorTodayObj[0]!=null)?Double.parseDouble(priorTodayObj[0].toString()):0.0;
				actualIncidentalAmountPrior = (priorTodayObj[1]!=null)?Double.parseDouble(priorTodayObj[1].toString()):0.0;
				actualUnplannedAmountPrior = (priorTodayObj[2]!=null)?Double.parseDouble(priorTodayObj[2].toString()):0.0;
				sumTotalActualDayPrior = sumTotalActualDayPrior + actualAmountPrior + actualIncidentalAmountPrior + actualUnplannedAmountPrior;
			}
		}
		
		return sumTotalActualDayPrior;
	}
	
	
	/**
	 * Sum Total For Well when current environment discharges node is newly added or edited
	 * If action is delete, not require to sum
	 */
	
	private static Double calculateTotalWellCurrentNode (CommandBean commandBean, CommandBeanTreeNode node, Boolean OocAverage, Boolean Welloocaverage) throws Exception{

		Double actualAmount = 0.0;
		Double actualIncidentalAmount = 0.0;
		Double actualUnplannedAmount = 0.0;
		Double totalWellCurrentNode = 0.0;
	
		Object object = node.getData();
	
		if (object instanceof EnvironmentDischarges) {
			EnvironmentDischarges environmentDischarges = (EnvironmentDischarges)object;
			
			if(!Action.DELETE.equals(node.getAtts().getAction())){
				if (environmentDischarges.getActualAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualAmount().toString())) {
					actualAmount = environmentDischarges.getActualAmount();
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualAmount");
					thisConverter.setBaseValueFromUserValue(actualAmount);
					actualAmount = thisConverter.getBasevalue();
				}
				if (environmentDischarges.getActualIncidentalAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualIncidentalAmount().toString())) {
					actualIncidentalAmount = environmentDischarges.getActualIncidentalAmount();
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualIncidentalAmount");
					thisConverter.setBaseValueFromUserValue(actualIncidentalAmount);
					actualIncidentalAmount = thisConverter.getBasevalue();
				}
				if (environmentDischarges.getActualUnplannedAmount() != null && StringUtils.isNotBlank(environmentDischarges.getActualUnplannedAmount().toString())) {
					actualUnplannedAmount = environmentDischarges.getActualUnplannedAmount();
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EnvironmentDischarges.class, "actualUnplannedAmount");
					thisConverter.setBaseValueFromUserValue(actualUnplannedAmount);
					actualUnplannedAmount = thisConverter.getBasevalue();
				}
				//ticket number #15883 update Total well for "Well OOC Average"
				if (OocAverage == true && Welloocaverage == false){
					totalWellCurrentNode = actualIncidentalAmount;
				}
				else if (OocAverage == false && Welloocaverage == true) {
					totalWellCurrentNode = actualAmount;
				}
				else {
					totalWellCurrentNode = actualAmount + actualIncidentalAmount + actualUnplannedAmount;
				}
			}
		}
		return totalWellCurrentNode;
	}
	
	
	/**
	 * Find and Update Total For Well to other daily with the same day date
	 */
	
	private static void updateTotalWellSameDayDate (String wellUid, Date currentReportDateTime, String dailyUid, String lookupEnvironmentDischargesUid, Double sumTotalWellPriorToToday) throws Exception{
		
		String sql = "SELECT dailyUid, reportDatetime FROM ReportDaily WHERE wellUid = :wellUid AND reportDatetime = :currentReportDateTime AND dailyUid != :currentDailyUid AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY reportDatetime ASC";
		String[] paramsfield = {"wellUid", "currentReportDateTime", "currentDailyUid"};
		Object[] paramsvalue = {wellUid, currentReportDateTime, dailyUid};

		List sameDayDateList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsfield,paramsvalue);
		
		if(sameDayDateList.size()>0){
			for (int daily=0; daily<sameDayDateList.size(); daily++){
				Object[] obj = (Object[]) sameDayDateList.get(daily);
				String sameDayDateDailyUid = (obj[0]!=null)?obj[0].toString():"";
				
				updateTotalWell (wellUid, sameDayDateDailyUid, lookupEnvironmentDischargesUid, sumTotalWellPriorToToday);
			}
		}
	}
	
	
	/**
	 * Update Total For Well to the assigned daily
	 */
	
	private static void updateTotalWell (String wellUid, String dailyUid, String lookupEnvironmentDischargesUid, Double actualTotalWellAmount) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String updateTotalWellSql = "UPDATE EnvironmentDischarges SET actualTotalWellAmount =:actualTotalWellAmount, lastEditDatetime =:lastEditDatetime WHERE (isDeleted is null or isDeleted = false) AND dailyUid = :dailyUid AND wellUid = :wellUid AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid";
		String[] fields = {"actualTotalWellAmount", "lastEditDatetime", "dailyUid",  "wellUid", "lookupEnvironmentDischargesUid"};
		Object[] values = {actualTotalWellAmount, new Date(), dailyUid, wellUid, lookupEnvironmentDischargesUid};

		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(updateTotalWellSql, fields, values, qp);
	}
	
	
	/**
	 * Find days after today 
	 * Sum and Update Total For Well for all affected days after today (for each affected environment discharges type)
	 */
	
	private static void updateTotalWellAfterToday (String wellUid, Date currentReportDateTime, String dailyUid, String lookupEnvironmentDischargesUid, Double sumTotalWellPriorToToday) throws Exception{
	
		String sql = "SELECT dailyUid, reportDatetime FROM ReportDaily WHERE wellUid = :wellUid AND reportDatetime > :currentReportDateTime AND dailyUid != :currentDailyUid AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY reportDatetime ASC";
		String[] paramsfield = {"wellUid", "currentReportDateTime", "currentDailyUid"};
		Object[] paramsvalue = {wellUid, currentReportDateTime, dailyUid};

		List dailyAfterToday = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsfield,paramsvalue);
		String dailyAfterTodayUid = null;
		Date targetReportDateTime = null;

		if (dailyAfterToday.size()>0){
			for (int daily=0; daily<dailyAfterToday.size(); daily++){
				Object[] obj = (Object[]) dailyAfterToday.get(daily);
				dailyAfterTodayUid = (obj[0]!=null)?obj[0].toString():"";
				targetReportDateTime = (Date) ((obj[1]!=null)?obj[1]:"");
			
				Double actualAmountAfter = 0.0;
				Double actualIncidentalAmountAfter = 0.0;
				Double actualUnplannedAmountAfter = 0.0;
				Double sumTotalActualDayAfter = 0.0;
				Double sumTotalWellAmountDayAfter = 0.0;
			
				String strSql2 = "SELECT SUM(actualAmount), SUM(actualIncidentalAmount), SUM(actualUnplannedAmount) FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) " + 
								"AND wellUid = :thisWellUid AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid " +
								"AND dailyUid IN (SELECT dailyUid FROM ReportDaily WHERE wellUid = :wellUid AND " +
								"reportDatetime > :currentReportDateTime AND reportDatetime <= :targetReportDateTime AND dailyUid != :currentDailyUid " +
								"AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY reportDatetime ASC)";
				String[] paramsFields2 = {"thisWellUid", "lookupEnvironmentDischargesUid", "wellUid", "currentReportDateTime", "targetReportDateTime", "currentDailyUid"};
				Object[] paramsValues2 = {wellUid, lookupEnvironmentDischargesUid, wellUid, currentReportDateTime, targetReportDateTime, dailyUid};

				List afterTodayList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,paramsFields2,paramsValues2);
			
				if(afterTodayList.size()>0){
					for (int after=0; after<afterTodayList.size(); after++){
						Object[] afterTodayObj = (Object[]) afterTodayList.get(after);
						actualAmountAfter = (afterTodayObj[0]!=null)?Double.parseDouble(afterTodayObj[0].toString()):0.0;
						actualIncidentalAmountAfter = (afterTodayObj[1]!=null)?Double.parseDouble(afterTodayObj[1].toString()):0.0;
						actualUnplannedAmountAfter = (afterTodayObj[2]!=null)?Double.parseDouble(afterTodayObj[2].toString()):0.0;
						sumTotalActualDayAfter = sumTotalActualDayAfter + actualAmountAfter + actualIncidentalAmountAfter + actualUnplannedAmountAfter;
					}
				}

				sumTotalWellAmountDayAfter =  sumTotalWellPriorToToday + sumTotalActualDayAfter;
				updateTotalWell (wellUid, dailyAfterTodayUid, lookupEnvironmentDischargesUid, sumTotalWellAmountDayAfter);
			}
		}
	}
	
	
	/**
	 * Get deleted days from deleted well-bore or operation (to indicate affected min day with environment discharges record)
	 * Get days from non-deleted well-bore/operation with day date >= minimum day of deleted well-bore/operation
	 */
	
	private static List<ReportDaily> getRecalculateDayList (String conditionField, String conditionValue, String wellUid) throws Exception{
	
		String sql = "SELECT dailyUid, reportDatetime FROM ReportDaily WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND " 
					 + conditionField + " = '" + conditionValue + "' AND dailyUid IN "
					 + "(SELECT dailyUid FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND "
					 + conditionField + " = '" + conditionValue + "') ORDER BY reportDatetime ASC";
		List deletedDayList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		
		List<ReportDaily> recalculateDayList = null;
		
		if (deletedDayList.size()>0){
			Object[] deletedDayObj = (Object[]) deletedDayList.get(0);
			Date minReportDate = (Date) ((deletedDayObj[1]!=null)?deletedDayObj[1]:"");
				for (int i=0; i<deletedDayList.size(); i++){
					Object[] obj = (Object[]) deletedDayList.get(i);
					String dailyUid = (obj[0]!=null)?obj[0].toString():"";
					setEnvironmentDischargeAsDeleted (dailyUid, wellUid);
				}
			String strsql = "FROM ReportDaily WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND wellUid = :wellUid AND reportDatetime >= :reportDatetime AND " + conditionField + " != '" + conditionValue + "' ORDER BY reportDatetime ASC";
			String[] paramsFields = {"wellUid","reportDatetime"};
			Object[] paramsValues = {wellUid, minReportDate};
			recalculateDayList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strsql,paramsFields,paramsValues);
		}
		
		return recalculateDayList;
	}
	
	
	/**
	 * Mark environment discharges record as deleted with dailyUid given
	 */
	
	private static void setEnvironmentDischargeAsDeleted (String dailyUid, String wellUid) throws Exception {
		
		String sql = "UPDATE EnvironmentDischarges SET isDeleted = true, lastEditDatetime =:lastEditDatetime WHERE (isDeleted is null or isDeleted = false) AND dailyUid = :dailyUid AND wellUid = :wellUid";
		String[] fields = {"lastEditDatetime", "dailyUid",  "wellUid"};
		Object[] values = {new Date(), dailyUid, wellUid};

		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, fields, values);
	}
	
	
	/**
	 * Return list of environment discharges from daily list
	 */
	
	private static List<String> getDischargesListFromDayList (List<ReportDaily> recalculateDayList) throws Exception{
		
		String dailyCondition = "";
		for(ReportDaily reportDaily: recalculateDayList){
			String dailyUid = (reportDaily!=null)?reportDaily.getDailyUid():"";
			dailyCondition = dailyCondition + "'" + dailyUid + "',";
		}
		if (dailyCondition!=null && StringUtils.isNotBlank(dailyCondition)){
			dailyCondition = StringUtils.substring(dailyCondition, 0, -1);
		}else{
			dailyCondition = "''";
		}

		String strSql = "SELECT DISTINCT lookupEnvironmentDischargesUid FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND dailyUid IN ("+dailyCondition+") ";
		List<String> dischargesList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				
		return dischargesList;
	}
	
	
	/**
	 * 
	 * Loop through each discharges types and each affected days, recalculate Total For Well for all affected days in recalculateDayList 
	 */
	
	private static void updateTotalWellAfterWellOpsDeleted (List<String> dischargesList, List<ReportDaily> recalculateDayList, String wellUid, String conditionField, String conditionValue) throws Exception{
		
		for(String dischargeType :dischargesList){
			for(ReportDaily reportDaily: recalculateDayList){
				String lookupEnvironmentDischargesUid = (dischargeType!=null)?dischargeType:"";
				String dailyUid = (reportDaily!=null)?reportDaily.getDailyUid():"";
				Date reportDatetime = (Date) ((reportDaily!=null)?reportDaily.getReportDatetime():"");
					
				String strSql1 = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid AND dailyUid = :dailyUid";
				String[] paramsFields1 = {"lookupEnvironmentDischargesUid","dailyUid"};
				Object[] paramsValues1 = {lookupEnvironmentDischargesUid, dailyUid};
				List resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1,paramsFields1,paramsValues1);
				
				if (resultList.size()>0){
					String strSql2 = "SELECT SUM(actualAmount), SUM(actualIncidentalAmount), SUM(actualUnplannedAmount) FROM EnvironmentDischarges " + 
									"WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND wellUid = :thisWellUid AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid " +
									"AND dailyUid IN (SELECT dailyUid FROM ReportDaily WHERE wellUid = :wellUid AND reportDatetime <= :reportDatetime AND " + conditionField + " != '" + conditionValue + "' " +
									"AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY reportDatetime ASC)";
					String[] paramsFields2 = {"thisWellUid", "wellUid","lookupEnvironmentDischargesUid","reportDatetime"};
					Object[] paramsValues2 = {wellUid, wellUid, lookupEnvironmentDischargesUid, reportDatetime};
					List amountList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,paramsFields2,paramsValues2);
				
					Double actualAmount = 0.0;
					Double actualIncidentalAmount = 0.0;
					Double actualUnplannedAmount = 0.0;
					Double sumTotalActualDay = 0.0;

					if(amountList.size()>0){
						for (int i=0; i<amountList.size(); i++){
							Object[] amountListObj = (Object[]) amountList.get(i);
							actualAmount = (amountListObj[0]!=null)?Double.parseDouble(amountListObj[0].toString()):0.0;
							actualIncidentalAmount = (amountListObj[1]!=null)?Double.parseDouble(amountListObj[1].toString()):0.0;
							actualUnplannedAmount = (amountListObj[2]!=null)?Double.parseDouble(amountListObj[2].toString()):0.0;
							sumTotalActualDay = sumTotalActualDay + actualAmount + actualIncidentalAmount + actualUnplannedAmount;
						}
						
						updateTotalWell (wellUid, dailyUid, lookupEnvironmentDischargesUid, sumTotalActualDay);
					}
				}
			}
		}
	}
	
	public static double calculateUsedDieselFromSV (CommandBean commandBean, CommandBeanTreeNode node, String dailyUid) throws Exception{			
		Double sumAmount = 0.00;
		String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NOT NULL " 
		+ "AND dailyUid = :dailyUid " 
		+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand = 'DIESEL')";
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {dailyUid};
		List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
		if(res.size()>0){
			if(res.get(0) != null) {
				sumAmount = Double.parseDouble(res.get(0).toString());
				return sumAmount;
			}else {
				return sumAmount;
			}
		}else {
			return sumAmount;
		}
	}
	
	public static double calculateUsedDieselFromRigStock (CommandBean commandBean, CommandBeanTreeNode node, String dailyUid) throws Exception{			
		Double sumAmount = 0.00;
		String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NULL " 
		+ "AND dailyUid = :dailyUid " 
		+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand IN ('DIESEL' , 'FUEL', 'DIESEL OIL'))";
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {dailyUid};
		List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
		if(res.size()>0){
			if(res.get(0) != null) {
				sumAmount = Double.parseDouble(res.get(0).toString());
				return sumAmount;
			}else {
				return sumAmount;
			}
		}else {
			return sumAmount;
		}
	}
	
	public static String[] getStockDetail(CommandBeanTreeNode node) throws Exception{
		Object object = node.getData();
		String[] detail = new String[1];
		if (object instanceof Discharges) {
			Discharges discharges = (Discharges) object;
			detail[0] = (discharges.getLookupEnvironmentDischargesUid()!=null ? discharges.getLookupEnvironmentDischargesUid():"");
		}else if(object instanceof Emissions) {
			Emissions emissions = (Emissions) object;
			detail[0] = (emissions.getLookupEnvironmentDischargesUid()!=null ? emissions.getLookupEnvironmentDischargesUid():"");
		}else if(object instanceof Wastes) {
			Wastes wastes = (Wastes) object;
			detail[0] = (wastes.getLookupEnvironmentDischargesUid()!=null ? wastes.getLookupEnvironmentDischargesUid():"");
		}
		return detail;
	}
	
	public static Boolean checkDischargesStockExist(String dailyUid, String lookupEnvironmentDischargesUid) throws Exception{
		
		if (StringUtils.isNotBlank(lookupEnvironmentDischargesUid)) {
			String[] paramsFields = {"dailyUid", "lookupEnvironmentDischargesUid"};
			Object[] paramsValues = {dailyUid, lookupEnvironmentDischargesUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM EnvironmentDischarges WHERE (isDeleted = false or isDeleted is null) AND category ='discharges' AND dailyUid =:dailyUid AND lookupEnvironmentDischargesUid =:lookupEnvironmentDischargesUid", paramsFields, paramsValues);
			if (lstResult.size() > 0) {			
				return true;		
			}
		}
		
		return false;
	}
	
	public static Boolean checkEmissionsStockExist(String dailyUid, String lookupEnvironmentDischargesUid) throws Exception{
		
		if (StringUtils.isNotBlank(lookupEnvironmentDischargesUid)) {
			String[] paramsFields = {"dailyUid", "lookupEnvironmentDischargesUid"};
			Object[] paramsValues = {dailyUid, lookupEnvironmentDischargesUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM EnvironmentDischarges WHERE (isDeleted = false or isDeleted is null) AND category ='emissions' AND dailyUid =:dailyUid AND lookupEnvironmentDischargesUid =:lookupEnvironmentDischargesUid", paramsFields, paramsValues);
			if (lstResult.size() > 0) {			
				return true;		
			}
		}
		
		return false;
	}
	
	public static Boolean checkWastesStockExist(String dailyUid, String lookupEnvironmentDischargesUid) throws Exception{
		
		if (StringUtils.isNotBlank(lookupEnvironmentDischargesUid)) {
			String[] paramsFields = {"dailyUid", "lookupEnvironmentDischargesUid"};
			Object[] paramsValues = {dailyUid, lookupEnvironmentDischargesUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM EnvironmentDischarges WHERE (isDeleted = false or isDeleted is null) AND category ='wastes' AND dailyUid =:dailyUid AND lookupEnvironmentDischargesUid =:lookupEnvironmentDischargesUid", paramsFields, paramsValues);
			if (lstResult.size() > 0) {			
				return true;		
			}
		}
		
		return false;
	}
}
