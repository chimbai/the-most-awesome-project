package com.idsdatanet.d2.safenet.lessonTicketAdmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;



public class LessonTicketAdminCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener ,InitializingBean{
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		
		if("lessonLearnedTitleList".equals(invocationKey)){
			String country = request.getParameter("_country");
			String lessonStatus = request.getParameter("_lessonStatus");
			
			if(country!=null || lessonStatus!=null){
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				String relatedOperations = null;
				String sqlCondition = "";
				if(country!=null && StringUtils.isNotBlank(country)){
					List<String> operationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT o.operationUid " +
							"FROM Well w, Operation o " +
							"WHERE (w.isDeleted IS NULL OR w.isDeleted=FALSE) " +
							"AND (o.isDeleted IS NULL OR o.isDeleted=FALSE) " +
							"AND w.wellUid=o.wellUid " +
							"AND w.country IN ("+country+")");
					if(operationResult!=null && operationResult.size()>0){
						for(String operation : operationResult){
							if(relatedOperations==null)
								relatedOperations = "'"+operation+"'";
							else
								relatedOperations += ",'"+operation+"'";
						}
					}
				}
				
				if(relatedOperations!=null && StringUtils.isNotBlank(relatedOperations)){
					sqlCondition += "AND operationUid IN ("+relatedOperations+") ";
				}
				if(lessonStatus!=null && StringUtils.isNotBlank(lessonStatus)){
					sqlCondition += "AND lessonStatus IN ("+lessonStatus+")";
				}
				
				List<String> lessonTicketList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT DISTINCT lessonTitle " +
						"FROM LessonTicket " +
						"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
						sqlCondition);
				if(lessonTicketList!=null && lessonTicketList.size()>0){
					for(String lessonLearnedTitle : lessonTicketList){
						writer.startElement("LessonLearnedTitle");
						writer.addElement("code", lessonLearnedTitle);
						writer.addElement("label", lessonLearnedTitle);
						writer.endElement();
					}
				}
				writer.endElement();
				writer.close();
			}
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
