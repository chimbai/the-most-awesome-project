package com.idsdatanet.d2.safenet.nonConformance;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobListener;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.CallOutList;
import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.SystemReportUtils;
import com.idsdatanet.d2.core.report.jasper.JasperJob;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxParameter;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.drillnet.dailyobservation.DailyObservationReportModule;
import com.idsdatanet.d2.drillnet.report.AbstractReportModule;
import com.idsdatanet.d2.drillnet.report.ReportUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class NonConformanceReportModule implements CommandBeanListener, SimpleAjaxActionHandler, JobListener{
	private static Pattern PATTERN_SYSTEM_PROPERTY = Pattern.compile("\\[.+?\\]");
	private static Log logger = LogFactory.getLog(DailyObservationReportModule.class);
	
	private List<ReportJob> generateReportJobs = null;
	private List<ReportJob> generateAndSendJobs = null;
	private String reportType = null;
	private String reportOutputFormat = null;
	private boolean selectXslAccordingToCurrentWellType = false;
	private String paperSize = "A4";
	private ReportDataGenerator dataGenerator = null;
	private String reportNameDateFormat = "dd MMM yyyy HHmm";
	private String emailSubject = null;
	private String emailContentTemplate = null;
	private String emailContentReportDateFormat = "dd MMM yyyy @ HH:mm";
	private MailEngine mailEngine = null;
	private Manager daoManager = null;
	
	private class ReportJob{
//		final DailyObservation dailyObservation;
		final UserSelectionSnapshot userSelectionSnapshot;
		final boolean sendReport;
		JobContext jobContext = null;
		String outputFile = null;
		String error = null;
		final NonconformanceEvent nonconformanceEvent;
		
		ReportJob(NonconformanceEvent nonconformanceEvent, UserSelectionSnapshot userSelectionSnapshot, boolean sendReport){
			this.nonconformanceEvent = nonconformanceEvent;
			this.userSelectionSnapshot = userSelectionSnapshot;
			this.sendReport = sendReport;
		}
	}
	
	public void setMailEngine(MailEngine value) {
		this.mailEngine = value;
	}
	
	public void setDaoManager(Manager value) {
		this.daoManager = value;
	}
	
	public void setEmailContentReportDateFormat(String value) {
		this.emailContentReportDateFormat = value;
	}
	
	public void setEmailSubject(String value) {
		this.emailSubject = value;
	}
	
	public void setEmailContentTemplate(String value) {
		this.emailContentTemplate = value;
	}
	
	public void setReportNameDateFormat(String value) {
		this.reportNameDateFormat = value;
	}
	
	public void setReportType(String value) {
		this.reportType = value;
	}
	
	public void setReportOutputFormat(String value) {
		this.reportOutputFormat = value;
	}
	
	private String getOutputFileExtension() {
		return this.reportOutputFormat;
	}
	
	public void setPaperSize(String value) {
		this.paperSize = value;
	}

	public void setDataGenerator(ReportDataGenerator value){
		this.dataGenerator = value;
	}

	@SuppressWarnings("rawtypes")
	private static void outputJson(Map data, HttpServletResponse response) throws IOException{
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		response.getWriter().write(JSONObject.fromObject(data).toString());
	}
	
	private static boolean isJobCompleted(ReportJob job) {
		if(job.jobContext == null) return false;
		return job.jobContext.isCompleted() || job.jobContext.isErrorOccurred() || job.error != null;
	}
	
	private static List<ReportJob> getCompletedJobs(List<ReportJob> jobs){
		List<ReportJob> result = new ArrayList<ReportJob>();
		for(ReportJob item: jobs) {
			if(isJobCompleted(item)) {
				result.add(item);
			}
		}
		return result;
	}
	
	private static Map<String,String> collectCompletedJobStatus(ReportJob job){
		Map<String,String> result = new HashMap<String,String>();
		result.put("key", job.nonconformanceEvent.getNonconformanceEventUid());
		result.put("status", (job.jobContext.isErrorOccurred() || job.error != null ? "error" : "completed"));
		String msg = job.error;
		if(msg == null && job.jobContext.isErrorOccurred()) {
			msg = job.jobContext.getJobResponse().getResponseMessage();
		}
		if(StringUtils.isNotBlank(msg)) result.put("msg", msg);
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private static Map<String,Object> collectStatus(List<ReportJob> jobs) {
		Map<String,Object> result = new HashMap<String,Object>(); 
		if(jobs != null) {
			List<Map> completedJobStatus = new ArrayList<Map>();
			List<ReportJob> completedJobs = getCompletedJobs(jobs);
			for(ReportJob item: completedJobs) {
				jobs.remove(item);
				completedJobStatus.add(collectCompletedJobStatus(item));
			}
			result.put("completed", completedJobStatus);
		}
		result.put("status", (jobs != null && jobs.size() > 0 ? "job_running" : "no_job"));
		return result;
	}
	
	private synchronized void showJobStatus(HttpServletResponse response, String error) throws IOException {
		Map<String,Object> result = new HashMap<String,Object>();
		if(error != null) result.put("error", error);
		result.put("gen_report", collectStatus(this.generateReportJobs));
		result.put("gen_and_send", collectStatus(this.generateAndSendJobs));
		outputJson(result, response);
	}

	private String getOutputFileInLocalPath(NonconformanceEvent nonconformanceEvent) throws Exception{
		if(StringUtils.isBlank(this.reportType)) {
			throw new Exception("ReportType must not be blank");
		}
		return this.reportType + 
			(StringUtils.isBlank(nonconformanceEvent.getOperationUid()) ? "" : "/" + nonconformanceEvent.getOperationUid()) + 
			(StringUtils.isBlank(nonconformanceEvent.getOperationUid()) ? "" : "/" + nonconformanceEvent.getOperationUid()) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(nonconformanceEvent));
	}
	
	private String getOutputFileName(NonconformanceEvent nonconformanceEvent) throws Exception{
		String wellUid=nonconformanceEvent.getWellUid();
		String operationUid=nonconformanceEvent.getOperationUid();
		//Get Well Name
		String strSql = "SELECT wellName "
				+ "FROM Well "
				+ "WHERE wellUid=:wellUid";
		List<String> wellName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid",wellUid );
		
		//Get Operation Name
		strSql = "SELECT operationName "
				+ "FROM Operation "
				+ "WHERE operationUid=:operationUid";
		List<String> operationName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid",operationUid );
		String num = nonconformanceEvent.getEventNumber();
		return this.reportType + " " + wellName.get(0)+" "+operationName.get(0)+" "+String.format("%02d", Integer.parseInt(num)) +"."+ this.getOutputFileExtension();
	}
	
	public void setSelectXslAccordingToCurrentWellType(boolean value){
		this.selectXslAccordingToCurrentWellType = value;
	}

	@SuppressWarnings("rawtypes")
	private String getReportDesignFile(NonconformanceEvent nonconformanceEvent) throws Exception{
		int well_op_type = ReportUtils.WELL_OPERATION_TYPE_DEFAULT;
		if(this.selectXslAccordingToCurrentWellType){
			List rs = this.daoManager.findByNamedParam("from Well where (isDeleted = false or isDeleted is null) and wellUid = :wellUid", "wellUid", nonconformanceEvent.getWellUid());
			if(rs.size() > 0) {
				Well well = (Well) rs.get(0);
				if(com.idsdatanet.d2.drillnet.constant.Well.ONSHORE.equals(well.getOnOffShore())){
					well_op_type = ReportUtils.WELL_OPERATION_TYPE_ONSHORE;
				}else if(com.idsdatanet.d2.drillnet.constant.Well.OFFSHORE.equals(well.getOnOffShore())){
					well_op_type = ReportUtils.WELL_OPERATION_TYPE_OFFSHORE;
				}
			}
		}			
		String file = ReportUtils.getConfiguredInstance().getXsl(nonconformanceEvent.getGroupUid(), this.reportType, well_op_type, this.paperSize, "");
		if(StringUtils.isBlank(file)) throw new Exception("Report design file not found");
		return file;
	}

	private ReportDataGenerator[] collectDataGenerators() throws Exception {
		if(this.dataGenerator == null) {
			throw new Exception("DataGenerator must be configured");
		}
		return new ReportDataGenerator[] {this.dataGenerator};
	}

	private D2Job createReportJob(String designFile, String output, ReportDataGenerator[] generators, String reportType, UserContext userContext) throws Exception{
		return JasperJob.createJasperReportJob(designFile, output, generators, reportType, this.reportOutputFormat, userContext); 
	}

	private void setJobQueue(List<ReportJob> jobs, boolean generateAndSend) {
		if(generateAndSend) {
			this.generateAndSendJobs = jobs;
		}else {
			this.generateReportJobs = jobs;
		}
	}
	
	private synchronized String generateReport(HttpServletRequest request) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		List<ReportJob> jobs = new ArrayList<ReportJob>();
		boolean sendAfterGenerate = "1".equals(request.getParameter("sendAfterGenerate"));
		JSONArray selected = JSONArray.fromObject(request.getParameter("selected"));
		for(int i=0; i < selected.size(); ++i) {
			JSONObject data = selected.getJSONObject(i);
			jobs.add(prepareReportJob(userSession, data.getString("key"), sendAfterGenerate));
		}
		setJobQueue(jobs, sendAfterGenerate);
		if(!this.submitNextReportJob(jobs)) {
			setJobQueue(null, sendAfterGenerate);
			return "Error generate report";
		}else {
			return null;
		}
	}
	
	private boolean submitNextReportJob(List<ReportJob> list) {
		for(ReportJob item: list) {
			if(!isJobCompleted(item)) {
				try {
					submitReportJob(item);
					return true;
				}catch(Exception ex) {
					item.error = "Error generate report";
					logger.error("Error generate report", ex);
				}
			}
		}
		return false;
	}
	
	private void submitReportJob(ReportJob reportJob) throws Exception {
		UserContext userContext = UserContext.getUserContext(reportJob.userSelectionSnapshot);
		SystemReportUtils.setDefaultReportLocale(reportJob.userSelectionSnapshot, ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject());
		String localOutputFile = this.getOutputFileInLocalPath(reportJob.nonconformanceEvent);
		String outputFile = ReportUtils.getFullOutputFilePath(localOutputFile);
		String designFile = ReportUtils.getFullXslFilePath(this.getReportDesignFile(reportJob.nonconformanceEvent));
		D2Job job = this.createReportJob(designFile, outputFile, this.collectDataGenerators(), this.reportType, userContext);
		JobContext currentJob = JobServer.getConfiguredInstance().createJob(job, userContext);
		currentJob.setJobGroup(AbstractReportModule.REPORT_JOB_POOL_NAME);
		currentJob.setJobListener(this);
		reportJob.jobContext = currentJob;
		reportJob.outputFile = localOutputFile;
		JobServer.getConfiguredInstance().submitJob(currentJob);
	}
	
	private static String printTimePart(int value) {
		return (value < 10 ? "0" : "") + String.valueOf(value);
	}

	private static TimeZone getTimeZone(Double value) {
		if(value == null) {
			return TimeZone.getDefault();
		}
		String sign = value < 0 ? "-" : "+";
		int hour = value.intValue();
		int min = Math.round((float)(60 * Math.abs(value - hour)));
		String id = "GMT" + sign + printTimePart(Math.abs(hour)) + ":" + printTimePart(min);
		return TimeZone.getTimeZone(id);
	}

	private TimeZone getWellTimeZone(String wellUid) throws Exception {
		Well well = (Well) this.daoManager.getObject(Well.class, wellUid);
		return getTimeZone(well.getTzGmtOffset());
	}
	
	private static String fillSystemProperties(String template, Map<String,String> properties){
		 Matcher m = PATTERN_SYSTEM_PROPERTY.matcher(template);
		 StringBuffer sb = new StringBuffer();
		 while (m.find()) {
			 String key = m.group().substring(1, m.group().length() - 1).trim();
			 if(properties.containsKey(key)) {
				 m.appendReplacement(sb, properties.get(key));	 
			 }
		 }
		 m.appendTail(sb);
		 return sb.toString();
	}

	private ReportJob prepareReportJob(UserSession userSession, String key, boolean sendAfterGenerate) throws Exception {
		NonconformanceEvent nonconformanceEvent = (NonconformanceEvent) this.daoManager.getObject(NonconformanceEvent.class, key);
		UserSelectionSnapshot userSelectionSnapshot = UserSelectionSnapshot.getInstanceForCustomDaily(userSession, userSession.getCurrentDailyUid());
		userSelectionSnapshot.setCustomProperty("nonconformanceEventUid", nonconformanceEvent.getNonconformanceEventUid());
		return new ReportJob(nonconformanceEvent, userSelectionSnapshot, sendAfterGenerate);
	}
	
	private List<ReportJob> findJobQueue(JobContext jobContext) {
		if(findReportJob(this.generateReportJobs, jobContext) != null) return this.generateReportJobs;
		if(findReportJob(this.generateAndSendJobs, jobContext) != null) return this.generateAndSendJobs;
		return null;
	}
	
	private static ReportJob findReportJob(List<ReportJob> list, JobContext jobContext) {
		if(list != null) {
			for(ReportJob item: list) {
				if(item.jobContext == jobContext) return item;
			}
		}
		return null;
	}

	private void downloadReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		NonconformanceEvent nonconformanceEvent = (NonconformanceEvent) this.daoManager.getObject(NonconformanceEvent.class, request.getParameter("reportId"));
		if(nonconformanceEvent == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		String localPath=this.getOutputFileInLocalPath(nonconformanceEvent);
		File file = new File(ReportUtils.getFullOutputFilePath(localPath));
		if(!file.exists()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		FileDownloadWriter.writeFileOutput(request, response, file, true);
	}
	
	private String getReportFilename(NonconformanceEvent nonconformanceEvent) throws Exception {
		Operation operation = (Operation) this.daoManager.getObject(Operation.class, nonconformanceEvent.getOperationUid());
		SimpleDateFormat df = new SimpleDateFormat(this.reportNameDateFormat);
		df.setTimeZone(getWellTimeZone(nonconformanceEvent.getWellUid()));
		String name = this.reportType + " " + operation.getOperationName() + " " + "." + this.reportOutputFormat;

		return ReportUtils.replaceInvalidCharactersInFileName(name);
	}
	
	private void getReportDownloadFilename(HttpServletRequest request, HttpServletResponse response) throws Exception {
		NonconformanceEvent nonconformanceEvent = (NonconformanceEvent) this.daoManager.getObject(NonconformanceEvent.class, request.getParameter("id"));
		Map<String,String> result = new HashMap<String,String>();
		result.put("filename", getReportFilename(nonconformanceEvent));
		outputJson(result, response);
	}
	
	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Map<String,CommandBeanTreeNode> list = root.getChild(CallOutList.class.getSimpleName());
		if(list != null) {
			//FormatCreatedAt formatCreatedAt = new FormatCreatedAt();
			for(CommandBeanTreeNode node: list.values()) {
//				CallOutList callOutList = (CallOutList) node.getData();
//				if(StringUtils.isNotBlank(callOutList.getReportFile())) {
		
//				String localOutputFile = this.getOutputFileInLocalPath(callOutList);
//				String outputFile = ReportUtils.getFullOutputFilePath(localOutputFile);
//				node.getDynaAttr().put("reportDownload",callOutList.getCallOutListUid());
//				commandBean.getRoot().getDynaAttr().put("reportDownload", "webservice/sttexportfiledownload.html?filename=" + URLEncoder.encode(outputFile));
//				if(commandBean!=null){
//					commandBean.getSystemMessage().addInfo("Please click <a href=\"abaccess/webservice/sttexportfiledownload/" + URLEncoder.encode(f.getName(),"utf-8") + "?filename=" + URLEncoder.encode(new String(Base64.encodeBase64(outputFile.getBytes("utf-8")),"utf-8"),"utf-8") + "&base64=1&abaccess=1\">here</a> to retrieve the exported file");
//				}
//				}
//				node.getDynaAttr().put("createdAt", formatCreatedAt.format(callOutList));
			}
		}
	}

	@Override
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}

	@Override
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
	}

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
	}

	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null && targetCommandBeanTreeNode.getDataDefinition() != null){
			Object obj = targetCommandBeanTreeNode.getData();
			String operationUid = UserSession.getInstance(request).getCurrentOperationUid();
			String wellUid= UserSession.getInstance(request).getCurrentWellUid();
			if (obj instanceof NonconformanceEvent) {
				NonconformanceEvent col =(NonconformanceEvent) obj;
			}

		}else {
			
			NonconformanceEvent nonconformanceEvent = (NonconformanceEvent) this.daoManager.getObject(NonconformanceEvent.class, request.getParameter("reportId"));
			String filename=nonconformanceEvent.getOperationUid()+"/"+nonconformanceEvent.getOperationUid()+"/"+this.getOutputFileName(nonconformanceEvent);
			File f =null;
			f=new File(request.getSession().getServletContext().getRealPath("/WEB-INF/report/pdf/NCR/")+filename);
			if(commandBean!=null){
				commandBean.getSystemMessage().addInfo("Please click <a href=\"abaccess/webservice/nonconformancedownload/" + URLEncoder.encode(f.getName(),"utf-8") + "?filename=" + URLEncoder.encode(new String(Base64.encodeBase64(filename.getBytes("utf-8")),"utf-8"),"utf-8") + "&base64=1&abaccess=0\">here</a> to retrieve the exported file",true,true);
			}
		}
	}

	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		if("check_report_status".equals(invocationKey)){
			this.showJobStatus(response, null);
		}else if("generate_report".equals(invocationKey)) {
			this.showJobStatus(response, this.generateReport(request));	
		}else if("getReportDownloadFilename".equals(invocationKey)) {
			this.getReportDownloadFilename(request, response);
		}else if("downloadReport".equals(invocationKey)) {
			this.downloadReport(request, response);
		}
	}

	@Override
	public void init(CommandBean commandBean) throws Exception {
	}

	@Override
	public void submitAction(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception {
	}

	@Override
	public void collectRefreshInstruction(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception {
	}

	@Override
	public boolean isHandlerForSimpleAjaxAction(String action) {
		return false;
	}

	@Override
	public boolean isContinueRefreshChecking() {
		return false;
	}

	@Override
	public void restartRefreshChecking(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception {
	}

	@Override
	public synchronized void jobEnded(JobContext job) throws Exception {
		List<ReportJob> list = findJobQueue(job);
		if(list == null) return;
		ReportJob reportJob = findReportJob(list, job);
		if(reportJob != null) {
			//load new instance to make sure it is the latest instance
			NonconformanceEvent nonconformanceEvent  = (NonconformanceEvent) this.daoManager.getObject(NonconformanceEvent.class, reportJob.nonconformanceEvent.getNonconformanceEventUid());
//			callOutList.setReportFile(reportJob.outputFile);
			this.daoManager.saveObject(nonconformanceEvent);
			
//			if(reportJob.sendReport) {
//				try {
//					reportJob.error = this.sendReport(dailyObservation);
//				}catch(Exception ex) {
//					reportJob.error = "Error occurred while sending email";
//					logger.error("Error sending " + this.reportType + " report via email", ex);
//				}
//			}
		}
		this.submitNextReportJob(list);
	}

	@Override
	public void jobStarted(JobContext job) throws Exception {
	}

	@Override
	public void jobTimedOut(JobContext job) throws Exception {
	}

	@Override
	public synchronized void jobErrorOccured(JobContext job) {
		List<ReportJob> list = findJobQueue(job);
		if(list == null) return;
		this.submitNextReportJob(list);
	}

}
