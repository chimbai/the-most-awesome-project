package com.idsdatanet.d2.safenet.nonConformance;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

//NCR Information Center - jhwong
public class ncrInformationCenterDataNodeListener extends EmptyDataNodeListener {

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof NonconformanceEvent) {
			node.getDynaAttr().put("ncrPage", "nonconformance.html");
		}
	}
}