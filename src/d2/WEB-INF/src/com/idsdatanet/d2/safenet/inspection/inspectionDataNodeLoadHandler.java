package com.idsdatanet.d2.safenet.inspection;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.InspectionResult;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class inspectionDataNodeLoadHandler implements DataNodeLoadHandler{

	private String _inspectionType = null;
	
	public void setInspectionType(String value) {
		_inspectionType = value;
	}
	
	public String getInspectionType() {
		return this._inspectionType;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(InspectionResult.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			
			String strSql ="from InspectionResult r, LookupInspectionType l " +
					"where (l.isDeleted=false or l.isDeleted is null) " +
					"and (r.isDeleted=false or r.isDeleted is null) " +
					"and r.dailyUid=:dailyUid " +
					"and l.lookupInspectionTypeUid=r.lookupInspectionTypeUid " +
					"and l.inspectionTypeCode=:inspectionTypeCode";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"dailyUid","inspectionTypeCode"}, new Object[]{userSelection.getDailyUid(), this._inspectionType});
			for (Object[] rec : list) {
				InspectionResult data = (InspectionResult) rec[0];
				output_maps.add(data);
			}
			return output_maps;
		}
		return null;
	}

}
