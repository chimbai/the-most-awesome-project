package com.idsdatanet.d2.safenet.nonConformance;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class nonConformanceAttachmentsDataNodeListener extends nonConformanceAltDataNodeListener{


	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		
		Object object = node.getData(); 
		
		if (object instanceof NonconformanceEvent) {	
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
		

			if (thisNonconformanceEvent.getAttachments()!=null){
				String[] attachment = thisNonconformanceEvent.getAttachments().split("\t");
				
				if (attachment!=null && attachment.length>0){
				 
					for(int i=0;i<attachment.length;i++){  
						node.getDynaAttr().put("attachments_" + attachment[i].toString(), attachment[i].toString() );
					}
				}
			}
			
			if (thisNonconformanceEvent.getNonConformanceBy()!=null){
				String[] nonConformanceBy = thisNonconformanceEvent.getNonConformanceBy().split("\t");
				
				if (nonConformanceBy!=null && nonConformanceBy.length>0){
					 
					for(int i=0;i<nonConformanceBy.length;i++){  
						node.getDynaAttr().put("nonConformanceBy_" + nonConformanceBy[i].toString(), nonConformanceBy[i].toString() );
					}
				}
				
			}
			
			if (thisNonconformanceEvent.getNonConformanceType()!=null){
				String[] nonConformanceType = thisNonconformanceEvent.getNonConformanceType().split("\t");
				
				if (nonConformanceType!=null && nonConformanceType.length>0){
					 
					for(int i=0;i<nonConformanceType.length;i++){  
						node.getDynaAttr().put("nonConformanceType_" + nonConformanceType[i].toString(), nonConformanceType[i].toString() );
					}
				}
				
			}
		}
	}
}