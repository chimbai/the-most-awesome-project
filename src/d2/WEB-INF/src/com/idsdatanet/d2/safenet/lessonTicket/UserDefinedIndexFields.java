package com.idsdatanet.d2.safenet.lessonTicket;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.search.elastic.indexer.UserDefinedField;
import com.idsdatanet.d2.core.search.elastic.indexer.UserDefinedFieldBuilder;
import com.idsdatanet.d2.core.search.elastic.indexer.UserDefinedFieldDataLoader;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class UserDefinedIndexFields implements com.idsdatanet.d2.core.search.elastic.indexer.UserDefinedIndexFields{
	private class LessonTicketIndexFieldLoader implements UserDefinedFieldDataLoader{
		private final Manager daoManager;
		private WeakReference<Map<String,LookupItem>> countryLookupRef = null;

		LessonTicketIndexFieldLoader() throws Exception{
			this.daoManager = ApplicationUtils.getConfiguredInstance().getDaoManager();
		}
		
		private synchronized Map<String,LookupItem> getCountryLookup() throws Exception{
			Map<String,LookupItem> lookup = (this.countryLookupRef != null ? this.countryLookupRef.get() : null);
			if(lookup != null){
				return lookup;
			}
			lookup = LookupManager.getConfiguredInstance().getLookup(UserDefinedIndexFields.this.countryLookup, null, null);
			this.countryLookupRef = new WeakReference(lookup);
			return lookup;
		}
		
		@Override
		public Map<String, Object> loadUserDefinedFields(Object data, String indexType) throws Exception {
			if(!LESSON_TICKET_TYPE.equals(indexType)){
				return null;
			}
			Map<String,Object> result = null;
			LessonTicket lessonTicket = (LessonTicket) data;
			String operationUid = lessonTicket.getOperationUid();
			List<Well> rs = this.daoManager.findByNamedParam("select w from Well w, Operation o where (w.isDeleted is null or w.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false) and w.wellUid = o.wellUid and o.operationUid = :operationUid", new String[] {"operationUid"}, new Object[]{operationUid});
			if(rs.size() > 0){
				Well well = rs.get(0);
				result = new HashMap<String,Object>();
				result.put(USER_DEFINED_FIELD_WELL_NAME, well.getWellName());
				result.put(USER_DEFINED_FIELD_COUNTRY, well.getCountry());
				result.put(USER_DEFINED_FIELD_WELL_FIELD, well.getField());
				
				LookupItem lookupItem = this.getCountryLookup().get(well.getCountry());
				if(lookupItem != null){
					result.put(USER_DEFINED_FIELD_COUNTRY_LOOKUPLABEL, lookupItem.getValue());
				}
			}
			
			String hql = "select c.campaignName from Campaign c, Operation o where (c.isDeleted is null or c.isDeleted = false) "
					+ "and (o.isDeleted is null or o.isDeleted = false) and o.operationCampaignUid = c.campaignUid and o.operationUid = :operationUid";
			List<String> campaigns = this.daoManager.findByNamedParam(hql, new String[] {"operationUid"}, new Object[]{operationUid});
			if(campaigns.size() > 0){
				if(result == null){
					result = new HashMap<String,Object>();
				}
				result.put(USER_DEFINED_CAMPAIGN_NAME, campaigns.get(0));
			}
			
			hql = "select fileName from FileManagerFiles where (isDeleted = false or isDeleted is null) and (popupAttachment = true) and externalModule = :externalModule and externalSubModule = :externalSubModule and externalKey1 = :key";
			String key_value = LessonTicket.class.getSimpleName() + ":" + lessonTicket.getLessonTicketUid();
			String controller = "lessonTicketDetailController";
			
			if(UserDefinedIndexFields.this.controllersByLessonReportType != null && StringUtils.isNotBlank(lessonTicket.getLessonReportType())) {
				controller = UserDefinedIndexFields.this.controllersByLessonReportType.get(lessonTicket.getLessonReportType());
			}

			List<String> fileNames = this.daoManager.findByNamedParam(hql, new String[] {"externalModule","externalSubModule","key"}, new Object[] {"FlexNodeFilesAttachment",controller,key_value});
			if(fileNames.size() > 0){
				if(result == null){
					result = new HashMap<String,Object>();
				}
				result.put(USER_DEFINED_ATTACHMENT_FILES_NAMES, StringUtils.join(fileNames, ":::"));
			}
			
			return result;
		}
	}
	
	private static final String LESSON_TICKET_TYPE = "lessonticket";
	private static final String USER_DEFINED_FIELD_WELL_NAME = "wellName";
	private static final String USER_DEFINED_FIELD_COUNTRY = "wellCountryCode";
	private static final String USER_DEFINED_FIELD_COUNTRY_LOOKUPLABEL = "wellCountryLabel";
	private static final String USER_DEFINED_CAMPAIGN_NAME = "campaignName";
	private static final String USER_DEFINED_FIELD_WELL_FIELD = "wellField";
	private static final String USER_DEFINED_ATTACHMENT_FILES_NAMES = "attachmentFileNames";
	
	private String countryLookup = null;
	private String userDefinedFieldMappingPrefix = null;
	private Map<String, String> controllersByLessonReportType = null;
	
	public void setControllersByLessonReportType(Map<String, String> controllersByLessonReportType) {
		this.controllersByLessonReportType = controllersByLessonReportType;
	}
	
	public void setCountryLookup(String value){
		this.countryLookup = value;
	}
	
	@Override
	public List<UserDefinedField> getUserDefinedFields(String indexType) throws Exception {
		if(LESSON_TICKET_TYPE.equals(indexType)){
			return UserDefinedFieldBuilder.newInstance()
					.addField(USER_DEFINED_FIELD_WELL_NAME)
					.addField(USER_DEFINED_FIELD_COUNTRY)
					.addField(USER_DEFINED_FIELD_COUNTRY_LOOKUPLABEL)
					.addField(USER_DEFINED_CAMPAIGN_NAME)
					.addField(USER_DEFINED_FIELD_WELL_FIELD)
					.addField(USER_DEFINED_ATTACHMENT_FILES_NAMES, UserDefinedFieldBuilder.INDEX_MAPPING_TYPE_STRING, true, true)
					.getResult();
		}else{
			return null;
		}
	}

	@Override
	public UserDefinedFieldDataLoader getUserDefinedFieldDataLoader(String indexType) throws Exception {
		if(LESSON_TICKET_TYPE.equals(indexType)){
			return new LessonTicketIndexFieldLoader();
		}else{
			return null;
		}
	}
	
	public void setUserDefinedFieldMappingPrefix(String userDefinedFieldMappingPrefix) {
		this.userDefinedFieldMappingPrefix = userDefinedFieldMappingPrefix;
	}

	public String getUserDefinedFieldMappingPrefix() throws Exception {
		return this.userDefinedFieldMappingPrefix;
	}
}
