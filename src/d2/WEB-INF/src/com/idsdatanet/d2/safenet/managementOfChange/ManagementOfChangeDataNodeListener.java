package com.idsdatanet.d2.safenet.managementOfChange;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MocDisp;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ManagementOfChangeDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object obj = node.getData();
		
		if(obj instanceof MocDisp) {
			MocDisp thisMocDisp = (MocDisp) obj;		
			String mocDispNumber = thisMocDisp.getMocDispNumber();
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyyMMdd");
		Date datePrefix = session.getCurrentDaily().getDayDate();
		
		if(obj instanceof MocDisp) {
			MocDisp thisMocDisp = (MocDisp) obj;		
			//String mocDispNumber = thisMocDisp.getMocDispNumber();
			
			String strSql = "SELECT mocDispNumber FROM MocDisp WHERE wellUid =:wellUid AND (isDeleted IS NULL or isDeleted = 0) AND formType='MOC' ORDER BY mocDispNumber DESC";
			List<String> lstmocDispNumber = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", session.getCurrentWell().getWellUid());
			String currentWellName = session.getCurrentWell().getWellName();

			if(lstmocDispNumber.size() > 0) {
				if(thisMocDisp.getMocDispNumber() == null) {
					Integer i = Integer.parseInt(lstmocDispNumber.get(0)) + 1;
					thisMocDisp.setMocDispNumber(i.toString());
					//append '0' to front if less than 2 digits
					Integer minLength = 2;
					for (int j=thisMocDisp.getMocDispNumber().length(); j < minLength; j++) {
						thisMocDisp.setMocDispNumber("0" + thisMocDisp.getMocDispNumber());
					}
				}				
			}
			else
			{
				thisMocDisp.setMocDispNumber("01");
			}
			
			if(thisMocDisp.getMocDispDatetime() != null) {
				//editing will not overwrite old data
			}
			else {
				thisMocDisp.setMocDispDatetime(datePrefix);
			}
			
			String setMocDispNumberPrefix = "" + currentWellName + "-" + dmyFormat.format(datePrefix) + " - " + thisMocDisp.getMocDispNumber();
			if(StringUtils.isNotBlank(thisMocDisp.getMocDispNumberPrefix())){
				//editing will not overwrite old data
			}
			else {
				thisMocDisp.setMocDispNumberPrefix(setMocDispNumberPrefix);
			}
			//set form type = MOC
			thisMocDisp.setFormType("MOC");
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisMocDisp);
			
		}

	}
	
	@Override
	public void beforeDataNodeSaveOrUpdate (CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		
	}

	public void onDataNodePasteAsNew(CommandBean commandBean,CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,UserSession userSession) throws Exception {
		Object object = targetNode.getData();
		if (object instanceof MocDisp) {
			MocDisp thisMocDisp = (MocDisp) object;
			thisMocDisp.setMocDispNumberPrefix(null);
			thisMocDisp.setMocDispDatetime(null);
		}
	}

	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if(obj instanceof MocDisp) {
			if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
				MocDisp thisMocDisp = (MocDisp) obj;
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				thisMocDisp.setMocDispDatetime(daily.getDayDate());
			}
		}
	}
}