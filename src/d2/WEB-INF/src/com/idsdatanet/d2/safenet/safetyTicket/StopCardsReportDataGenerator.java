package com.idsdatanet.d2.safenet.safetyTicket;

import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class StopCardsReportDataGenerator implements ReportDataGenerator{

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, 
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		String dailyUid = today.getDailyUid();
		
		String strSql = "SELECT stc.lookupCategoryUid, std.subCategory, SUM(std.numSafeTicket) as totalSafeTix, SUM(std.numUnsafeTicket) as totalUnsafeTix " +
		"FROM Daily d, SafetyTicket st, SafetyTicketCategory stc, SafetyTicketDetail std " + 
		"WHERE (d.isDeleted is null or d.isDeleted=false) " +
		"AND (st.isDeleted is null or st.isDeleted=false) " +
		"AND (stc.isDeleted is null or stc.isDeleted=false) " +
		"AND (std.isDeleted is null or std.isDeleted=false) " +
		"AND d.dailyUid = :dailyUid " + 
		"AND d.dailyUid = st.dailyUid " +
		"AND st.safetyTicketUid = stc.safetyTicketUid " +
		"AND stc.safetyTicketCategoryUid = std.category " +
		"GROUP BY stc.lookupCategoryUid, std.subCategory";
		
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {dailyUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
	
		if (lstResult.size() > 0){
			this.createStopCards(userContext, reportDataNode, lstResult);
		}
		
		String strSql2 = "SELECT scp.lookupCompanyUid, SUM(scp.noOfCards) as totalCards " +
		"FROM Daily d, SafetyTicket st, StopCardParticipation scp " + 
		"WHERE (d.isDeleted is null or d.isDeleted=false) " +
		"AND (st.isDeleted is null or st.isDeleted=false) " +
		"AND (scp.isDeleted is null or st.isDeleted=false) " +
		"AND d.dailyUid = :dailyUid " +  
		"AND d.dailyUid = st.dailyUid " +
		"AND st.safetyTicketUid = scp.safetyTicketUid " +
		"GROUP BY scp.lookupCompanyUid";
		
			String[] paramsFields22 = {"dailyUid"};
			Object[] paramsValues22 = {dailyUid};
			List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields22, paramsValues22, qp);
		
			if (lstResult2.size() > 0){
				this.createStopCardParticipation(userContext, reportDataNode, lstResult2);
			}
			
		String strSql3 = "SELECT scp.lookupCompanyUid, scp.isHighlight, scp.highlightDescription " +
		"FROM Daily d, SafetyTicket st, StopCardParticipation scp " + 
		"WHERE (d.isDeleted is null or d.isDeleted=false) " +
		"AND (st.isDeleted is null or st.isDeleted=false) " +
		"AND (scp.isDeleted is null or st.isDeleted=false) " +
		"AND d.dailyUid = :dailyUid " +  
		"AND d.dailyUid = st.dailyUid " +
		"AND st.safetyTicketUid = scp.safetyTicketUid ";
		
			String[] paramsFields33 = {"dailyUid"};
			Object[] paramsValues33 = {dailyUid};
			List<Object[]> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields33, paramsValues33, qp);
		
			if (lstResult3.size() > 0){
				this.createStopCardHighlight(userContext, reportDataNode, lstResult3);
			}
	}
		
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 * @param userContext
	 * @param reportDataNode
	 * @param resullt
	 * @throws Exception
	 */
	private void createStopCards(UserContext userContext, ReportDataNode reportDataNode, List resullt) throws Exception {
		
		for(Object objResult: resullt){
		
			Object[] objTix = (Object[]) objResult;
			
			String lookupCategory = "N/A";
			String lookupCategoryName = "N/A";
			String subCategory = "N/A";
			String subCategoryName = "N/A";
			Double totalSafeCards = 0.0;
			Double totalUnsafeCards = 0.0;
			
			if (objTix[0] != null && StringUtils.isNotBlank(objTix[0].toString())) {
				lookupCategory = objTix[0].toString();
				String strSql2 = "SELECT categoryDescr FROM LookupSafetyCategory WHERE (isDeleted is null OR isDeleted = false) AND lookupSafetyCategoryUid = :thisCategory";
				String[] paramsFields2 = {"thisCategory"};
				Object[] paramsValues2 = {lookupCategory};
				
				List lstResultLookUpObservations = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (lstResultLookUpObservations.size()>0)
				{
					Object lookupObservationResult = (Object) lstResultLookUpObservations.get(0);
					if(lookupObservationResult != null) lookupCategoryName = lookupObservationResult.toString();
				}
				
			}
			
			if (objTix[1] != null && StringUtils.isNotBlank(objTix[1].toString())) {
				subCategory = objTix[1].toString();
				String strSql2 = "SELECT subCategoryDescr FROM LookupSafetySubCategory WHERE (isDeleted is null OR isDeleted = false) AND lookupSafetySubCategoryUid = :thisSub";
				String[] paramsFields2 = {"thisSub"};
				Object[] paramsValues2 = {subCategory};
				
				List lstResultLookUpCategory = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (lstResultLookUpCategory.size()>0)
				{
					Object lookupCategoryResult = (Object) lstResultLookUpCategory.get(0);
					if(lookupCategoryResult != null) subCategoryName = lookupCategoryResult.toString();
				}
				
			}
			
			if (objTix[2] != null && StringUtils.isNotBlank(objTix[2].toString())){
				totalSafeCards = Double.parseDouble(objTix[2].toString());
			}
			
			if (objTix[3] != null && StringUtils.isNotBlank(objTix[3].toString())){
				totalUnsafeCards = Double.parseDouble(objTix[3].toString());
			}
			
			ReportDataNode thisReportNode = reportDataNode.addChild("TotalTix");
			thisReportNode.addProperty("lookupCategoryName", lookupCategoryName);
			thisReportNode.addProperty("numOfSafe", totalSafeCards.toString());	
			thisReportNode.addProperty("numOfUnsafe", totalUnsafeCards.toString());	
			thisReportNode.addProperty("subCategory", subCategoryName);
			
		}	
	}
	
private void createStopCardParticipation(UserContext userContext, ReportDataNode reportDataNode, List resullt2) throws Exception {
		
		for(Object objResult2: resullt2){
			
			Object[] objStop = (Object[]) objResult2;
			
			String lookUpCompany = "N/A";
			String lookUpCompanyName = "N/A";
			Double totalCards = 0.0;
			
			if (objStop[0] != null && StringUtils.isNotBlank(objStop[0].toString())) {
				lookUpCompany = objStop[0].toString();
				String strSql2 = "SELECT companyName FROM LookupCompany WHERE (isDeleted is null OR isDeleted = false) AND lookupCompanyUid = :thisCompany";
				String[] paramsFields2 = {"thisCompany"};
				Object[] paramsValues2 = {lookUpCompany};
				
				List lstResultLookUpCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (lstResultLookUpCompany.size()>0)
				{
					Object lookupCompanyResult = (Object) lstResultLookUpCompany.get(0);
					if(lookupCompanyResult != null) lookUpCompanyName = lookupCompanyResult.toString();
				}
				
			}
			
			if (objStop[1] != null && StringUtils.isNotBlank(objStop[1].toString())){
				totalCards = Double.parseDouble(objStop[1].toString());
			}
			
			ReportDataNode thisReportNode = reportDataNode.addChild("StopParticipation");
			thisReportNode.addProperty("lookupCompany", lookUpCompanyName);
			thisReportNode.addProperty("numOfCards", totalCards.toString());	
			
		}	
	}

private void createStopCardHighlight(UserContext userContext, ReportDataNode reportDataNode, List resullt3) throws Exception {
	
	for(Object objResult3: resullt3){
		
		Object[] objHigh = (Object[]) objResult3;
		
		String lookUpCompany = "N/A";
		String lookUpCompanyName = "N/A";
		String isHighlight = "N/A";
		String description = "N/A";
		
		if (objHigh[0] != null && StringUtils.isNotBlank(objHigh[0].toString())) {
			lookUpCompany = objHigh[0].toString();
			String strSql2 = "SELECT companyName FROM LookupCompany WHERE (isDeleted is null OR isDeleted = false) AND lookupCompanyUid = :thisCompany";
			String[] paramsFields2 = {"thisCompany"};
			Object[] paramsValues2 = {lookUpCompany};
			
			List lstResultLookUpCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			if (lstResultLookUpCompany.size()>0)
			{
				Object lookupCompanyResult = (Object) lstResultLookUpCompany.get(0);
				if(lookupCompanyResult != null) lookUpCompanyName = lookupCompanyResult.toString();
			}
			
		}
		
		if (objHigh[1] != null && StringUtils.isNotBlank(objHigh[1].toString())) {
			isHighlight = objHigh[1].toString();
		}
		if (objHigh[2] != null && StringUtils.isNotBlank(objHigh[2].toString())) {
			description = objHigh[2].toString();
		}
		
		ReportDataNode thisReportNode = reportDataNode.addChild("CardHighlight");
		thisReportNode.addProperty("lookupCompany", lookUpCompanyName);
		thisReportNode.addProperty("highlight", isHighlight);
		thisReportNode.addProperty("description", description);
		
	}	
}

}
