package com.idsdatanet.d2.safenet.unwantedEvent;

import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.stt.STTDecisionListener;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class UnwantedEventSTTDecisionListener implements STTDecisionListener {
	
	private final static String EVENT_STATUS_CLOSED = "closed";
	
	public boolean ignore(Object data) {
		UnwantedEvent sourceUnwantedEvent = (UnwantedEvent) data;
		
		if (sourceUnwantedEvent != null) {
			try {
				UnwantedEvent unwantedEvent = (UnwantedEvent) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(UnwantedEvent.class, sourceUnwantedEvent.getUnwantedEventUid());
				if (unwantedEvent != null) {
					if (EVENT_STATUS_CLOSED.equals(unwantedEvent.getEventStatus())) {
						return true;
					}
				}
			} catch (Exception e) {}
		}
		
		return false;
	}

}
