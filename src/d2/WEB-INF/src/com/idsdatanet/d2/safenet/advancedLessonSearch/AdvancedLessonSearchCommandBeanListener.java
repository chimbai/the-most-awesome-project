package com.idsdatanet.d2.safenet.advancedLessonSearch;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;




public class AdvancedLessonSearchCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener ,InitializingBean{
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		
		if("jobTypeCodeList".equals(invocationKey)){
			String wellType = request.getParameter("_wellType");
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if(wellType!=null ){

				if(StringUtils.isNotBlank(wellType)){
					List operationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT a.name,a.shortCode,a.operationCode FROM LookupJobType a " +
							"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) " +
							"AND (a.isActive IS NULL OR a.isActive=TRUE) " +
							"AND a.name!='' " +
							"AND (a.operationCode IN ("+wellType+") OR a.operationCode = '' OR a.operationCode is null) " +
							"ORDER BY a.name");

					Map<String, LookupItem> wellTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://operationtype?key=code&amp;value=label", null, null);
					
					if(operationResult!=null && operationResult.size()>0){
						int i = 1;
						for(Object obj : operationResult){
							String lookuplabel= "-";
							Object[] jobtypeCode = (Object[]) obj;
							writer.startElement("jobTypeCodeList");
							if (StringUtils.isBlank(jobtypeCode[2].toString())){
								jobtypeCode[2] = "-";
							}
							writer.addElement("code", jobtypeCode[1].toString()+".."+i+".."+jobtypeCode[2].toString()); //use [shortCode..i..operationCode] in xmlwriter for "code" to avoid duplicated "code" for each "label"
							if(wellTypeLookup!=null && wellTypeLookup.size()>0){
								if(wellTypeLookup.containsKey(jobtypeCode[2])){
									LookupItem welltype = wellTypeLookup.get(jobtypeCode[2]);
									lookuplabel = welltype.getValue().toString();
								}
							}
							writer.addElement("label", jobtypeCode[0].toString()+" ["+lookuplabel+","+jobtypeCode[1].toString()+"]");
							writer.endElement();
							i++;
						}
					}
				}	
			}
			writer.endElement();
			writer.close();
		}
		
		if("taskCodeList".equals(invocationKey)){
			String jobtypeCode = request.getParameter("_jobtypeCode");
			String onOffShore = request.getParameter("_onOffShore");
			String jobtypewellType = request.getParameter("_wellType");
			List operationResult = new ArrayList();
			List Results = new ArrayList();
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			if(jobtypeCode!=null ){
				jobtypeCode = jobtypeCode.replaceAll("\'", "");
				String[] jobtypeselection = jobtypeCode.split(",");
				for(String arr : jobtypeselection){
					String jobtypeShortCode = "";
					String wellType = "";
					String[] jobtypeselection2 = arr.split("\\..");
					if (jobtypeShortCode == ""){
						jobtypeShortCode += "'"+jobtypeselection2[0].toString()+"'";
					}
					else{
						jobtypeShortCode += ",'"+jobtypeselection2[0].toString()+"'";
					}
					if (wellType == ""){
						if (!jobtypeselection2[2].toString().equalsIgnoreCase("-")){ //skip if operationCode is "-"
							wellType += "'"+jobtypeselection2[2].toString()+"'";
						}
					}
					else{
						if (!jobtypeselection2[2].toString().equalsIgnoreCase("-")){
							wellType += ",'"+jobtypeselection2[2].toString()+"'";
						}
					}
				
					if (wellType.equalsIgnoreCase("")){
						wellType += jobtypewellType.toString();
					}
					if(StringUtils.isNotBlank(jobtypeCode)){
						String str1 = "";
						String str2 = "";
						if (wellType != ""){
							str1 = "AND (a.operationCode IN ("+wellType+") OR a.operationCode = '' OR a.operationCode is null) ";
						}
						if (onOffShore != null){
							str2 = "AND (a.onOffShore IN ("+onOffShore+") or a.onOffShore='' or a.onOffShore is null) ";
						}
						
						operationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT a.name,a.shortCode,a.jobTypeShortCode,a.operationCode FROM LookupTaskCode a " +
								"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) " +
								"AND (a.isActive IS NULL OR a.isActive=TRUE) " +
								"AND a.name!='' " +					
								"AND a.jobTypeShortCode IN ("+jobtypeShortCode+") " +
								str1 +
								str2 +
								"ORDER BY a.name");
						
					}
					if (operationResult != null && operationResult.size()>0){ //operationResult of each loop insert/append into Results
						for(Object obj : operationResult){
							Results.add(obj);		
						}
					}
				}
				Collections.sort(Results, new TaskCodeListComparator()); //sort the Results
				operationResult = new ArrayList();
				int n=0;
				for(Object obj : Results){
					if(operationResult.size()<1){
						operationResult.add(obj);
						n++;
					}
					else{
						if(!obj.equals(operationResult.get(n-1).toString())){ //skip if the current and previous value are duplicated
							operationResult.add(obj);
							n++;
						}
					}	
				}

				Map<String, LookupItem> wellTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://operationtype?key=code&amp;value=label", null, null);
				
				if(operationResult!=null && operationResult.size()>0){
					int i = 1;
					for(Object obj : operationResult){
						String lookuplabel= "-";
						Object[] taskCode = (Object[]) obj;
						writer.startElement("taskCodeList");
						if (StringUtils.isBlank(taskCode[3].toString())){
							taskCode[3] = "-";
						}
						writer.addElement("code", taskCode[1].toString()+".."+i);
						if(wellTypeLookup!=null && wellTypeLookup.size()>0){
							if(wellTypeLookup.containsKey(taskCode[3])){
								LookupItem welltype = wellTypeLookup.get(taskCode[3]);
								lookuplabel = welltype.getValue().toString();
							}
						}
						writer.addElement("label", taskCode[0].toString()+" ["+lookuplabel+","+taskCode[1].toString()+","+taskCode[2].toString()+"]"); //lookup label show "-" if operationCode is null or ""(empty)
						writer.endElement();
						i++;
					}
				}
				
			}
			writer.endElement();
			writer.close();
		}
		
		if("phaseCodeList".equals(invocationKey)){
			String wellType = request.getParameter("_wellType");
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if(wellType!=null ){

				if(StringUtils.isNotBlank(wellType)){
					List operationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT a.name,a.shortCode,a.operationCode FROM LookupPhaseCode a " +
							"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) " +
							"AND (a.isActive IS NULL OR a.isActive=TRUE) " +
							"AND a.name!='' " +
							"AND (a.operationCode IN ("+wellType+") OR a.operationCode = '' OR a.operationCode is null) " +
							"ORDER BY a.name");
					
					Map<String, LookupItem> wellTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://operationtype?key=code&amp;value=label", null, null);
					
					if(operationResult!=null && operationResult.size()>0){
						int i = 1;
						for(Object obj : operationResult){
							String lookuplabel= "-";
							Object[] phaseCode = (Object[]) obj;
							writer.startElement("phaseCodeList");
							if (StringUtils.isBlank(phaseCode[2].toString())){
								phaseCode[2] = "-";
							}
							writer.addElement("code", phaseCode[1].toString()+".."+i+".."+phaseCode[2].toString());
							if(wellTypeLookup!=null && wellTypeLookup.size()>0){
								if(wellTypeLookup.containsKey(phaseCode[2])){
									LookupItem welltype = wellTypeLookup.get(phaseCode[2]);
									lookuplabel = welltype.getValue().toString();
								}
							}
							writer.addElement("label", phaseCode[0].toString()+" ["+lookuplabel+","+phaseCode[1].toString()+"]");
							writer.endElement();
							i++;
						}
					}
				}	
			}
			writer.endElement();
			writer.close();
		}
		
		if("rigTypeList".equals(invocationKey)){
			String onOffShore = request.getParameter("_onOffShore");
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if(onOffShore!=null ){

				if(StringUtils.isNotBlank(onOffShore)){
					onOffShore = StringUtils.replace(onOffShore, "OFF", "1");
					onOffShore = StringUtils.replace(onOffShore, "ON", "0");
					List rigTypeResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT shortCode, lookupLabel, parentReferenceKey " +
							"FROM CommonLookup " +
							"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
							"AND (isActive IS NULL OR isActive=TRUE) " +
							"AND lookupTypeSelection='rigInformation.rigSubType' " +
							"AND (parentReferenceKey IN ("+ onOffShore + ") OR parentReferenceKey IS NULL OR parentReferenceKey='') " +
							"ORDER BY lookupLabel");
					
					Map<String, LookupItem> rigTypeLookup = LookupManager.getConfiguredInstance().getLookup("db://CommonLookup?key=shortCode&value=lookupLabel&order=lookupLabel&cache=false&filter=lookupTypeSelection='Rig_type'", null, null);
					
					if(rigTypeResult!=null && rigTypeResult.size()>0){
						int i=1;
						for(Object obj : rigTypeResult){
							String lookuplabel= "-";
							Object[] rigType = (Object[]) obj;
							writer.startElement("rigTypeList");
							if(rigType[2]==null){
								rigType[2] = "-";
							}else{
								if (StringUtils.isBlank(rigType[2].toString())){
									rigType[2] = "-";
								}else{
									if(rigType[2].equals("1")){
										rigType[2]="OFF";
									}else if(rigType[2].equals("0")){
										rigType[2]="ON";
									}else{
										rigType[2] = "-";
									}
								}
							}
							
							writer.addElement("code", rigType[0].toString());
							if(rigTypeLookup!=null && rigTypeLookup.size()>0){
								if(rigTypeLookup.containsKey(rigType[2])){
									LookupItem rigTypeLookupItem = rigTypeLookup.get(rigType[2]);
									lookuplabel = rigTypeLookupItem.getValue().toString();
								}
							}
							writer.addElement("label", rigType[1].toString());
							writer.endElement();
							i++;
						}
					}
				}	
			}
			writer.endElement();
			writer.close();
		}
		
		if("taskCodeListFromPhaseCode".equals(invocationKey)){
			String phaseCodes = request.getParameter("_phaseCode");
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if(phaseCodes!=null ){

				if(StringUtils.isNotBlank(phaseCodes)){
					String phaseCodeInString = null;
					String[] phaseCode = null;
					if(phaseCodes.contains(",")){
						phaseCode = phaseCodes.split(",");
						for(int i=0; i < phaseCode.length; i++){
							String str = phaseCode[i].toString();
							str = str.replaceAll("\'", "");
							String[] phase = str.split("\\..");
							if (phaseCodeInString==null || phaseCodeInString==""){
								phaseCodeInString = "'"+phase[0].toString()+"'";
							}
							else{
								phaseCodeInString += ",'"+phase[0].toString()+"'";
							}
						}
					}else{
						String str = phaseCodes;
						str = str.replaceAll("\'", "");
						String[] phase = str.split("\\..");
						if (phaseCodeInString==null || phaseCodeInString==""){
							phaseCodeInString = "'"+phase[0].toString()+"'";
						}
						else{
							phaseCodeInString += ",'"+phase[0].toString()+"'";
						}
					}
					
					List taskCodeResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT name,shortCode FROM LookupTaskCode " +
							"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
							"AND (isActive IS NULL OR isActive=TRUE) " +
							"AND name!='' " +
							"AND (phaseShortCode IN ("+phaseCodeInString+") OR phaseShortCode = '' OR phaseShortCode is null) " +
							"ORDER BY name");
					
					if(taskCodeResult!=null && taskCodeResult.size()>0){
						int i = 1;
						for(Object obj : taskCodeResult){
							String lookuplabel= "-";
							Object[] taskCode = (Object[]) obj;
							writer.startElement("taskCodeListFromPhaseCode");
							writer.addElement("code", taskCode[0].toString());
							writer.addElement("label", taskCode[0].toString());
							writer.endElement();
							i++;
						}
					}
				}	
			}
			writer.endElement();
			writer.close();
		}
	}
	
	private class TaskCodeListComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
