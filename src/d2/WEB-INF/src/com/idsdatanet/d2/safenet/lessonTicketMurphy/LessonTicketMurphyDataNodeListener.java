package com.idsdatanet.d2.safenet.lessonTicketMurphy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LookupLessonNumber;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketMurphyDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private String lessonTicketNumberPrefixPattern = null;
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + " - " + thisLessonTicket.getLessonTicketNumber());
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoNewLessonTicketNumber"))){
				if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
					
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					if (StringUtils.isNotBlank(thisLessonTicket.getCountry())) {
						process(thisLessonTicket,userSelection,session);
						commandBean.getSystemMessage().addInfo("Please Add Lesson Ticket Category Element for Newly Created Lesson Ticket.", true);
					}
				}
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			LookupLessonNumber thisLookupLessonNumber = this.getLookupLessonNumber(thisLessonTicket.getCountry());
			
			// Get existing Lesson Tickets within Well and using same Country
			String strSql = "FROM LessonTicket WHERE wellUid = :wellUid AND lessonTicketNumberPrefix =:lessonTicketNumberPrefix AND country=:country AND (isDeleted IS NULL OR isDeleted = FALSE)";
			String[] paramsFields = {"wellUid", "lessonTicketNumberPrefix", "country"};
			String[] paramsValues = {thisLessonTicket.getWellUid(), thisLessonTicket.getLessonTicketNumberPrefix(), thisLessonTicket.getCountry()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0) {
				int largestNumber = thisLookupLessonNumber.getStartingNumber()-1;
				int currentNumber;
				for(Object lstObj: lstResult) {
					LessonTicket currentLessonTicket = (LessonTicket) lstObj;
					currentNumber = Integer.parseInt(currentLessonTicket.getLessonTicketNumber());
					if(largestNumber < currentNumber) {
						largestNumber = currentNumber;
					}
				}
				if(largestNumber < thisLookupLessonNumber.getLastNumberGenerated()) {
					thisLookupLessonNumber.setLastNumberGenerated(largestNumber);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLookupLessonNumber);
				}
			}
			else {
				// If empty reset to starting number - 1 (so next generated will be starting number
				thisLookupLessonNumber.setLastNumberGenerated(thisLookupLessonNumber.getStartingNumber()-1);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLookupLessonNumber);
			}
		}
	}
	
	synchronized private void process(LessonTicket thisLessonTicket, UserSelectionSnapshot userSelection, UserSession session) throws Exception
	{
		LookupLessonNumber thisLookupLessonNumber = this.getLookupLessonNumber(thisLessonTicket.getCountry());
		String prefixPattern = thisLookupLessonNumber.getPrefix();
		String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
		String newreportnumber = this.generatedLessonTicketNumber(thisLookupLessonNumber, userSelection);
		
		if (StringUtils.isNotBlank(getPrefixName)){
			thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
		}

		thisLessonTicket.setLessonTicketNumber(newreportnumber);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
	}
	
	private String generatedLessonTicketNumber(LookupLessonNumber thisLookupLessonNumber, UserSelectionSnapshot userSelection) throws Exception {
		Integer newNumber;
		String strNewNumber;
		
		if(thisLookupLessonNumber != null) {
			// check for latest undeleted ticket from well/country
			String strSql = "FROM LessonTicket WHERE wellUid = :wellUid AND lessonTicketNumberPrefix =:lessonTicketNumberPrefix AND country=:country AND (isDeleted IS NULL OR isDeleted = FALSE)";
			String[] paramsFields = {"wellUid", "lessonTicketNumberPrefix", "country"};
			String[] paramsValues = {userSelection.getWellUid(), thisLookupLessonNumber.getPrefix(), thisLookupLessonNumber.getLookupLessonNumberUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0) {
				int largestNumber = thisLookupLessonNumber.getStartingNumber()-1;
				int currentNumber;
				for(Object lstObj: lstResult) {
					LessonTicket currentLessonTicket = (LessonTicket) lstObj;
					currentNumber = Integer.parseInt(currentLessonTicket.getLessonTicketNumber());
					if(largestNumber < currentNumber) {
						largestNumber = currentNumber;
					}
				}
				newNumber = largestNumber + 1;
			} else {
				// if none, return starting number
				newNumber = thisLookupLessonNumber.getStartingNumber();
			}
			strNewNumber = newNumber.toString();
			thisLookupLessonNumber.setLastNumberGenerated(newNumber);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLookupLessonNumber);
			
			// pad the number to meet minimum length
			if(thisLookupLessonNumber.getMinimumLength() > 0) {
				Integer minLength = thisLookupLessonNumber.getMinimumLength();
				for (int i=strNewNumber.length(); i < minLength; i++) {
					strNewNumber = "0" + strNewNumber;
				}
			}
			return strNewNumber;
		}
		return null;
	}
	
	private LookupLessonNumber getLookupLessonNumber(String lookupLesssonNumberUid) throws Exception {
		
		LookupLessonNumber lookupLessonNumber = (LookupLessonNumber) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupLessonNumber.class, lookupLesssonNumberUid);
		if(lookupLessonNumber != null) return lookupLessonNumber;
		return null;
	}
	
	public void setLessonTicketNumberPrefixPattern(
			String lessonTicketNumberPrefixPattern) {
		this.lessonTicketNumberPrefixPattern = lessonTicketNumberPrefixPattern;
	}

	public String getLessonTicketNumberPrefixPattern() {
		return lessonTicketNumberPrefixPattern;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(LessonTicket.class)) {
			
			String strSql = "FROM LessonTicket WHERE wellUid = :wellUid AND (isDeleted IS NULL OR isDeleted = FALSE)";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			arrayParamNames.add("wellUid");
			arrayParamValues.add(userSelection.getWellUid());
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<LessonTicket> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new LessonTicketComparator());
			
			for(Object objResult: items){
				LessonTicket thisLessonTicket = (LessonTicket) objResult;
				output_maps.add(thisLessonTicket);
			}
						
			return output_maps;
		}
		return null;
	}
	
	private class LessonTicketComparator implements Comparator<LessonTicket>{
		public int compare(LessonTicket o1, LessonTicket o2){
			try {
				String p1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumberPrefix());
				String p2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumberPrefix());
				
				String n1 = WellNameUtil.getPaddedStr(o1.getLessonTicketNumber());
				String n2 = WellNameUtil.getPaddedStr(o2.getLessonTicketNumber());
				
				String ltNumber1 = p1 + " - " + n1;
				String ltNumber2 = p2 + " - " + n2;
				if (ltNumber1 == null || ltNumber2 == null) return 0;
				return ltNumber1.compareTo(ltNumber2);
				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}
