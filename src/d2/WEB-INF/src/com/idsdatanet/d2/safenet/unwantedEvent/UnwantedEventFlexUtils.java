package com.idsdatanet.d2.safenet.unwantedEvent;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.log.CommandLoggingContext;
import com.idsdatanet.d2.core.log.CommandLoggingControl;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupJobType;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.thoughtworks.xstream.XStream;

public class UnwantedEventFlexUtils extends BlazeRemoteClassSupport {
	
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
	private SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
	private MailEngine mailEngine = null;
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	public String getSettings() {
		String result = "<root/>";
		try {
			UserSession session = this.getCurrentUserSession();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			Locale locale = session.getUserLocale();
			DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getInstance(locale);
			String decimalSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getDecimalSeparator());
			writer.addElement("DecimalSeperator", decimalSeperator);
			String thousandSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getGroupingSeparator());
			writer.addElement("ThousandSeperator", thousandSeperator);
			String notifyButton = GroupWidePreference.getValue(session.getCurrentGroupUid(), "hideNotifyPartyButton");
			writer.addElement("NotifyButton", notifyButton);
			writer.addElement("AutoCreateUeRecord", GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CREATE_UNWANTEDEVENTS));
			writer.addElement("AutoRemoveUeRecord", GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoRemoveUnwantedEventFromActivity"));
			
			CustomFieldUom thisConvertor = new CustomFieldUom(locale, ReportDaily.class, "dayCost");
			writer.addElement("CostPrecision", thisConvertor.getUOMMapping().getUnitOutputPrecision() + "");
			writer.addElement("CurrencyUOM", thisConvertor.getUomSymbol());
			thisConvertor.setReferenceMappingField(Activity.class, "activityDuration");
			writer.addElement("DurationPrecision", thisConvertor.getUOMMapping().getUnitOutputPrecision() + "");
			writer.addElement("DurationUOM", thisConvertor.getUomSymbol());
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public String getLookups() {
		String result = "<root/>";
		SimpleAttributes atts;
		try {
			UserSession session = this.getCurrentUserSession();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
		
			String lookupUri = "xml://unwantedEvent.eventStatus?key=code&value=label";
			Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(lookupUri, new UserSelectionSnapshot(session), new LookupCache());
			atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("value", "");
			writer.addElement("LookupEventStatus", null, atts);
			for (Object entry: lookupList.entrySet()) {
				Map.Entry<String,LookupItem> lookupItem = (Map.Entry)entry;
				LookupItem item = lookupItem.getValue();
				atts = new SimpleAttributes();
				atts.addAttribute("key", item.getKey());
				atts.addAttribute("value", item.getValue().toString());
				writer.addElement("LookupEventStatus", null, atts);
			}
			lookupUri = "xml://unwantedEvent.contractorType?key=code&value=label";
			lookupList = LookupManager.getConfiguredInstance().getLookup(lookupUri, new UserSelectionSnapshot(session), new LookupCache());
			atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("value", "");
			writer.addElement("LookupContractorType", null, atts);
			for (Object entry: lookupList.entrySet()) {
				Map.Entry<String,LookupItem> lookupItem = (Map.Entry)entry;
				LookupItem item = lookupItem.getValue();
				atts = new SimpleAttributes();
				atts.addAttribute("key", item.getKey());
				atts.addAttribute("value", item.getValue().toString());
				writer.addElement("LookupContractorType", null, atts);
			}
			atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("value", "");
			atts.addAttribute("service", "");
			writer.addElement("LookupContractor", null, atts);
			String strSql = "SELECT c.lookupCompanyUid, c.companyName, s.code, c.ueEmail FROM LookupCompany c, LookupCompanyService s " +
					" WHERE (c.isDeleted=false or c.isDeleted is null)" +
					" AND (s.isDeleted=false or s.isDeleted is null)" +
					" AND s.lookupCompanyUid=c.lookupCompanyUid" +
					" AND s.code like 'UE_%'" +
					" ORDER BY c.companyName";
			List<Object[]> rsLookupCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (Object[] rec : rsLookupCompany) {
				String lookupCompanyUid = rec[0].toString();
				String companyName = rec[1].toString();
				String service = rec[2].toString();
				String ueEmail = "";
				if (rec[3] != null) ueEmail= rec[3].toString();
				atts = new SimpleAttributes();
				atts.addAttribute("key", lookupCompanyUid);
				atts.addAttribute("value", companyName);
				atts.addAttribute("service", service);
				atts.addAttribute("email", ueEmail);
				writer.addElement("LookupContractor", null, atts);
			}
			writer.endAllElements();
			
			writer.close();
			result = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public Double calculateLossCost(String activityUid) {
		Double totalCost = 0.0;
		
		try {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			Activity activity = null;
			String strSql = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') " +
					"AND activityUid=:activityUid";
			List<Activity> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid, qp);
			if (list.size()>0) activity = list.get(0);
			if (activity==null) return 0.0;
			
			Double duration = activity.getActivityDuration();
			Double totalDuration = 0.0;
			strSql = "SELECT SUM(activityDuration) as activityDuration FROM Activity " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"AND dailyUid=:dailyUid " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') ";
			List<Double> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", activity.getDailyUid(), qp);
			if (rs.size()>0) {
				if (rs.get(0)!=null) totalDuration = rs.get(0); 
			}
			
			Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(activity.getDailyUid());
			if (dayCost==null) {
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(this.getCurrentUserSession(), activity.getDailyUid());
				dayCost = reportDaily.getDaycost();
			}
			if (dayCost==null) dayCost = 0.0;
			totalCost = dayCost * duration / totalDuration;
			
			CustomFieldUom thisConvertor = new CustomFieldUom(this.getCurrentUserSession().getUserLocale(), ReportDaily.class, "dayCost");
			thisConvertor.setBaseValue(totalCost);
			totalCost = thisConvertor.getConvertedValue();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return totalCost;
	}
	
	public String getUnwantedEvent() {
		String result = "<root/>";
		SimpleAttributes atts;
		try {
			UserSession session = this.getCurrentUserSession();
			String operationUid = session.getCurrentOperationUid();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			CustomFieldUom costConvertor = new CustomFieldUom(this.getCurrentUserSession().getUserLocale(), ReportDaily.class, "dayCost");
			CustomFieldUom timeConvertor = new CustomFieldUom(this.getCurrentUserSession().getUserLocale(), Activity.class, "activityDuration");
			String strSql = "FROM UnwantedEvent where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and type='UE' order by eventRef";
			List<UnwantedEvent> unwantedEvent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			Collections.sort(unwantedEvent, new UnwantedEventComparator());
			for (UnwantedEvent rec : unwantedEvent) {
				atts = new SimpleAttributes();
				atts.addAttribute("unwantedEventUid", nullToEmptyString(rec.getUnwantedEventUid()));
				atts.addAttribute("eventRef", nullToEmptyString(rec.getEventRef())); // Report No
				atts.addAttribute("eventTitle", nullToEmptyString(rec.getEventTitle())); //subject / title
				atts.addAttribute("eventNptCode", nullToEmptyString(rec.getEventNptCode())); //NPT Code
				atts.addAttribute("eventStatus", nullToEmptyString(rec.getEventStatus())); //status
				atts.addAttribute("eventCause", nullToEmptyString(rec.getEventCause())); // comment
				atts.addAttribute("contractorType", nullToEmptyString(rec.getContractorType())); //contractor type
				atts.addAttribute("nameOfContractor", nullToEmptyString(rec.getNameOfContractor())); //contractor
				atts.addAttribute("nptDuration", nullToEmptyString(rec.getNptDuration())); //total hour
				atts.addAttribute("estimatedLoss", nullToEmptyString(rec.getEstimatedLoss())); //total cost
				atts.addAttribute("parentUnwantedEventUid", nullToEmptyString(rec.getParentUnwantedEventUid())); //parentUnwantedEventUid
				atts.addAttribute("activityUid", nullToEmptyString(rec.getActivityUid())); //activityUid
				writer.addElement("UnwantedEvent", null, atts);
			}
			
			strSql = "FROM Activity a, Daily d, UnwantedEvent u" +
					" where (a.isDeleted=false or a.isDeleted is null)" +
					" and (u.isDeleted=false or u.isDeleted is null)" +
					" and (d.isDeleted=false or d.isDeleted is null)" +
					" and a.activityUid=u.activityUid" +
					" and a.dailyUid=d.dailyUid" +
					" and (a.isSimop=false or a.isSimop is null) " +
					" and (a.isOffline=false or a.isOffline is null) " +
					" and (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " +
					//" and (a.dayPlus=0 or a.dayPlus is null) " +
					" and u.operationUid=:operationUid " +
					" and u.type='UE'";
			List<Object[]> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (Object[] rec:rs2) {
				Activity activity = (Activity) rec[0];
				Daily daily = (Daily) rec[1];
				Integer dayPlus = activity.getDayPlus();
				if (dayPlus==null) dayPlus = 0;
				atts = new SimpleAttributes();
				atts.addAttribute("activityUid", nullToEmptyString(activity.getActivityUid()));
				atts.addAttribute("startDatetime", timeFormat.format(activity.getStartDatetime()));
				if ("23:59".equalsIgnoreCase(timeFormat.format(activity.getEndDatetime())))
				{
					atts.addAttribute("endDatetime", "24:00");
				}else{
					atts.addAttribute("endDatetime", timeFormat.format(activity.getEndDatetime()));
				}
				atts.addAttribute("activityDuration", nullToEmptyString(activity.getActivityDuration()));
				atts.addAttribute("activityDuration_label", timeConvertor.getFormattedValue(activity.getActivityDuration()));
				atts.addAttribute("activityDescription", nullToEmptyString(activity.getActivityDescription()));
				atts.addAttribute("jobTypeCode", getJobTypeName(activity.getJobTypeCode()));
				atts.addAttribute("classCode", nullToEmptyString(activity.getClassCode()));
				atts.addAttribute("phaseCode", nullToEmptyString(activity.getPhaseCode()));
				atts.addAttribute("taskCode", nullToEmptyString(activity.getTaskCode()));
				atts.addAttribute("rootCauseCode", nullToEmptyString(activity.getRootCauseCode()));
				ReportDaily report = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, daily.getDailyUid());
				if (report!=null) {
					Date reportDate = report.getReportDatetime();
					if (dayPlus>0) reportDate = DateUtils.addDays(reportDate, dayPlus);
					atts.addAttribute("reportNumber", nullToEmptyString(report.getReportNumber()));
					atts.addAttribute("reportDatetime", dateFormat.format(reportDate));
					atts.addAttribute("reportDatetime_raw", dateFormat2.format(reportDate));
				}
				Double cost = this.calculateLossCost(activity.getActivityUid());
				if (dayPlus>0) cost = 0.0;
				atts.addAttribute("estimatedCost", cost.toString());
				atts.addAttribute("estimatedCost_label", costConvertor.getFormattedValue(cost, false));
				
				writer.addElement("Activity", null, atts);
			}
			strSql = "FROM ActivityUnwantedEventLink l, UnwantedEvent u" +
					" where (l.isDeleted=false or l.isDeleted is null)" +
					" and (u.isDeleted=false or u.isDeleted is null)" +
					" and l.unwantedEventUid=u.unwantedEventUid" +
					" and u.operationUid=:operationUid" +
					" and u.type='UE'";
			List<Object[]> rs3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			for (Object[] rec:rs3) {
				ActivityUnwantedEventLink link = (ActivityUnwantedEventLink) rec[0];
				atts = new SimpleAttributes();
				atts.addAttribute("activityUid", nullToEmptyString(link.getActivityUid()));
				atts.addAttribute("unwantedEventUid", nullToEmptyString(link.getUnwantedEventUid()));
				writer.addElement("ActivityUnwantedEventLink", null, atts);
			}
			
			writer.endAllElements();
			
			writer.close();
			result = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	private class UnwantedEventComparator implements  Comparator<UnwantedEvent>{
		public int compare(UnwantedEvent o1, UnwantedEvent o2){
			try{
				
				String s1 = WellNameUtil.getPaddedStr(o1.getEventRef());
				String s2 = WellNameUtil.getPaddedStr(o2.getEventRef());
				
				if (s1 == null || s2 == null) return 0;
				return s1.compareTo(s2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	
	public String getNewUnwantedEvents() {
		String result = "<root/>";
		SimpleAttributes atts;
		try {
			UserSession session = this.getCurrentUserSession();
			String operationUid = session.getCurrentOperationUid();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			String strSql = "FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND d.dailyUid=a.dailyUid " +
					"AND a.internalClassCode in ('TP', 'TU') " +
					"AND a.operationUid=:operationUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " +
					"ORDER BY d.dayDate, a.startDatetime";
			String[] paramsFields = {"operationUid"};
			Object[] paramsValues = {operationUid};
			
			List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			for (Object[] rec : rs) {
				Activity activity = (Activity) rec[0];
				Daily daily = (Daily) rec[1];
				
				strSql = "FROM UnwantedEvent where (isDeleted=false or isDeleted is null) and type='UE' and activityUid=:activityUid";
				List<UnwantedEvent> ue = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activity.getActivityUid());
				if (ue.size()>0) continue;
				
				atts = new SimpleAttributes();
				atts.addAttribute("activityUid", nullToEmptyString(activity.getActivityUid()));
				atts.addAttribute("startDatetime", timeFormat.format(activity.getStartDatetime()));
				if ("23:59".equalsIgnoreCase(timeFormat.format(activity.getEndDatetime())))
				{
					atts.addAttribute("endDatetime", "24:00");
				}else{
					atts.addAttribute("endDatetime", timeFormat.format(activity.getEndDatetime()));
				}
				atts.addAttribute("activityDuration", nullToEmptyString(activity.getActivityDuration()));
				atts.addAttribute("activityDescription", nullToEmptyString(activity.getActivityDescription()));
				atts.addAttribute("jobTypeCode", getJobTypeName(activity.getJobTypeCode()));
				atts.addAttribute("classCode", nullToEmptyString(activity.getClassCode()));
				atts.addAttribute("phaseCode", nullToEmptyString(activity.getPhaseCode()));
				atts.addAttribute("taskCode", nullToEmptyString(activity.getTaskCode()));
				atts.addAttribute("rootCauseCode", nullToEmptyString(activity.getRootCauseCode()));
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, daily.getDailyUid());
				if (reportDaily!=null) {
					atts.addAttribute("reportNumber", nullToEmptyString(reportDaily.getReportNumber()));
					atts.addAttribute("reportDatetime", dateFormat.format(reportDaily.getReportDatetime()));
					atts.addAttribute("reportDatetime_raw", dateFormat2.format(reportDaily.getReportDatetime()));
				}
				atts.addAttribute("estimatedCost", this.calculateLossCost(activity.getActivityUid()).toString());
				writer.addElement("Activity", null, atts);
			}
			writer.endAllElements();
			writer.close();
			result = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public Boolean moveUnwantedEventActivity(String activityUid, String unwantedEventUid) {
		Boolean bSucccess = true;

		String strSql = "FROM ActivityUnwantedEventLink where (isDeleted=false or isDeleted is null) and activityUid=:activityUid";
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {				
				List <ActivityUnwantedEventLink> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);
				if (rs.size()>0) {
					for (ActivityUnwantedEventLink rec:rs) {
						rec.setUnwantedEventUid(unwantedEventUid);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
					}
				} else {
					ActivityUnwantedEventLink rec = new ActivityUnwantedEventLink();
					rec.setActivityUid(activityUid);
					rec.setUnwantedEventUid(unwantedEventUid);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
				}
			} catch (Exception e) {
				bSucccess = false;
				e.printStackTrace();
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			bSucccess = false;
			e.printStackTrace();
		}
		return bSucccess;
	}
	
	public String removeUnwantedEventActivity(String activityUid) {
		String result = "<root/>";
		Boolean bSucccess = true;
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {			
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
				writer.startElement("root");
				String strSql = "FROM UnwantedEvent where (isDeleted=false or isDeleted is null) and type='UE' and activityUid=:activityUid";
				List <UnwantedEvent> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);
				
				int counter = 0;
				if (rs.size()>0) {
					UnwantedEvent rec = (UnwantedEvent) rs.get(0);
					String unwantedEventUid = rec.getUnwantedEventUid();
					strSql = "FROM ActivityUnwantedEventLink WHERE (isDeleted=false or isDeleted is null) and activityUid=:activityUid";
					List <ActivityUnwantedEventLink> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);
					for (ActivityUnwantedEventLink rec2:rs2) {
						if (!unwantedEventUid.equals(rec2.getUnwantedEventUid())) {
							rec2.setUnwantedEventUid(unwantedEventUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec2);
							counter++;
						}
					}
				}
				
				if (counter==0) bSucccess=false;
				
				writer.addElement("status", bSucccess.toString());
				writer.addElement("activityUid",activityUid);
				
				writer.endAllElements();
				writer.close();
				result = bytes.toString();
			} catch (Exception e) {
				bSucccess = false;
				e.printStackTrace();
				result = "<root/>";
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			bSucccess = false;
			e.printStackTrace();
			result = "<root/>";
		} 
		return result;
	}
	
	public String getRemovedActivity(String activityUid) {
		String result = "<root/>";
		SimpleAttributes atts;
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes); 
			writer.startElement("root");
			
			String strSql = "FROM UnwantedEvent WHERE (isDeleted=false or isDeleted is null) and type='UE' and activityUid=:activityUid";
			List <UnwantedEvent> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);
			if (rs.size()>0) {
				UnwantedEvent rec = (UnwantedEvent) rs.get(0);
				atts = new SimpleAttributes();
				atts.addAttribute("unwantedEventUid", nullToEmptyString(rec.getUnwantedEventUid()));
				atts.addAttribute("eventRef", nullToEmptyString(rec.getEventRef())); // Report No
				atts.addAttribute("eventTitle", nullToEmptyString(rec.getEventTitle())); //subject / title
				atts.addAttribute("eventNptCode", nullToEmptyString(rec.getEventNptCode())); //subject / title
				atts.addAttribute("eventStatus", nullToEmptyString(rec.getEventStatus())); //status
				atts.addAttribute("eventCause", nullToEmptyString(rec.getEventCause())); // comment
				atts.addAttribute("contractorType", nullToEmptyString(rec.getContractorType())); //contractor type
				atts.addAttribute("nameOfContractor", nullToEmptyString(rec.getNameOfContractor())); //contractor
				atts.addAttribute("nptDuration", nullToEmptyString(rec.getNptDuration())); //total hour
				atts.addAttribute("estimatedLoss", nullToEmptyString(rec.getEstimatedLoss())); //total cost
				atts.addAttribute("parentUnwantedEventUid", nullToEmptyString(rec.getParentUnwantedEventUid())); //parentUnwantedEventUid
				atts.addAttribute("activityUid", nullToEmptyString(rec.getActivityUid())); //activityUid
				writer.addElement("UnwantedEvent", null, atts);
			}
			
			writer.endAllElements();
			writer.close();
			result = bytes.toString();
		} catch (Exception e) {
			e.printStackTrace();
			result = "<root/>";
		}
		return result;
	}
	
	public Boolean updateUnwantedEventGrouping(String unwantedEventUid, String parentUnwantedEventUid) {
		Boolean bSuccess = true;
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {
				UnwantedEvent rec = (UnwantedEvent) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(UnwantedEvent.class, unwantedEventUid);
				if (rec==null) return false;
				rec.setParentUnwantedEventUid(parentUnwantedEventUid);
				rec.setType("UE");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			} catch (Exception e) {
				bSuccess = false;
				e.printStackTrace();
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			bSuccess = false;
			e.printStackTrace();
		}
		
		return bSuccess;
	}
	
	public Boolean saveUnwantedEvent(String XMLString) {
		Boolean bSucccess = true;
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {
				XStream xstream = new XStream();
				xstream.alias("UnwantedEvents", UnwantedEventRecords.class);
				xstream.alias("record", UnwantedEventRecord.class);
				UnwantedEventRecords command = (UnwantedEventRecords) xstream.fromXML(XMLString);
				List<UnwantedEventRecord> list = command.getRecords();
				
				Integer counter = 0;
				
				for (UnwantedEventRecord ue : list) {
					UnwantedEvent rec = (UnwantedEvent) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(UnwantedEvent.class, ue.getUnwantedEventUid());
					if (rec==null) rec = new UnwantedEvent();
					
					rec.setActivityUid(StringUtils.isBlank(ue.getActivityUid()) ? "" : ue.getActivityUid());
					rec.setContractorType(StringUtils.isBlank(ue.getContractorType()) ? "" : ue.getContractorType());
					rec.setEstimatedLoss(ue.getEstimatedLoss()==null ? 0.0 : ue.getEstimatedLoss());
					rec.setEventCause(StringUtils.isBlank(ue.getEventCause()) ? "" : ue.getEventCause());
					rec.setEventRef(StringUtils.isBlank(ue.getEventRef()) ? "" : ue.getEventRef());
					rec.setEventStatus(StringUtils.isBlank(ue.getEventStatus()) ? "open" : ue.getEventStatus());
					rec.setEventTitle(StringUtils.isBlank(ue.getEventTitle()) ? "" : ue.getEventTitle());
					rec.setEventNptCode(StringUtils.isBlank(ue.getEventNptCode()) ? "" : ue.getEventNptCode());
					rec.setNameOfContractor(StringUtils.isBlank(ue.getNameOfContractor()) ? "" : ue.getNameOfContractor());
					rec.setNptDuration(ue.getNptDuration()==null ? 0.0 : ue.getNptDuration());
					rec.setEventSummary(""); //this field not allow null
					rec.setType("UE");
					//rec.setParentUnwantedEventUid(StringUtils.isBlank(ue.getParentUnwantedEventUid()) ? null : ue.getParentUnwantedEventUid());
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
					
					String strSql = "FROM ActivityUnwantedEventLink where (isDeleted=false or isDeleted is null) and activityUid=:activityUid";
					List<ActivityUnwantedEventLink> link = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", ue.getActivityUid());
					if (link.size()<=0) {
						ActivityUnwantedEventLink newlink = new ActivityUnwantedEventLink();
						newlink.setUnwantedEventUid(rec.getUnwantedEventUid());
						newlink.setActivityUid(rec.getActivityUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newlink);
					}
					
					counter++;
				}
			} catch (Exception e) {
				e.printStackTrace();
				bSucccess = false;
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			e.printStackTrace();
			bSucccess = false;
		} 
		
		return bSucccess;
	}
	
	public Boolean sendNotificationEmail(String[] email, String subject, String text) {
		try {
			if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
			this.mailEngine.sendMail(email, 
					ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
					ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
					subject, 
					text);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public Boolean containsOldEvents() {
		try {
			UserSession session = this.getCurrentUserSession();
			return this.containsOldEvents(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Boolean containsOldEvents(UserSession session) {
		try {
			Integer counter = 0;
			String strSql = "FROM UnwantedEvent WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid AND type='UE' ";
			List<UnwantedEvent> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", session.getCurrentOperationUid());
			for (UnwantedEvent rec:rs) {
				String activityUid = rec.getActivityUid();
				if (activityUid==null) continue;
				Activity a = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, activityUid);
				if (a!=null) {
					if (a.getIsDeleted()==null) a.setIsDeleted(false);
					if (a.getIsDeleted()) {
						counter++;
						//continue;
						break;
					}
					
					if (a.getDayPlus()==null) a.setDayPlus(0);
					if (a.getDayPlus()>0) {
						counter++;
						break;
					}
					
					//CHECK BY USING IDS INTERNAL CLASS CODE FOR TP/TU EVENT
					String strSql2 = "SELECT DISTINCT internalCode FROM LookupClassCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (operationCode = :thisOperationCode OR " +
						"operationCode = '') AND groupUid = :thisGroupUid AND shortCode = :thisShortCode";
					String[] paramsFields = {"thisOperationCode","thisGroupUid","thisShortCode"};
					Object[] paramsValues = {session.getCurrentOperationType(),session.getCurrentGroupUid(),a.getClassCode()};					
					List ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
										
					if(ls.size() > 0)
					{
						if (!"TP".equals(ls.get(0)) && !"TU".equals(ls.get(0))) {
							counter++;
						}
					}
				} else {
					counter++;
				}
			}
			if (counter>0) return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return false;
	}
	
	public Boolean unwantedEventCleanup() {
		try {
			UserSession session = this.getCurrentUserSession();
			return this.unwantedEventCleanup(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private String getJobTypeName(String jobTypeCode) throws Exception {
		if (jobTypeCode==null) return "";
		String strSql = "FROM LookupJobType WHERE (isDeleted=false or isDeleted is null) and shortCode=:jobTypeCode";
		List<LookupJobType> jobTypeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "jobTypeCode", jobTypeCode);
		if (jobTypeList.size()>0) {
			LookupJobType lookupJobType = jobTypeList.get(0);
			if (StringUtils.isNotBlank(lookupJobType.getName())) return lookupJobType.getName(); 
		}
		return jobTypeCode;
	}
	
	private Boolean hasNextDay(String dailyUid) throws Exception {
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily==null) return false;
		String queryString = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid and dayDate>:dayDate order by dayDate ";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String [] {"operationUid","dayDate"}, new Object[] {daily.getOperationUid(), daily.getDayDate()});
		if (list.size()>0) return true;
		return false;
	}
	
	public Boolean unwantedEventCleanup(UserSession session) {
		try{
			CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
			try {			
				String strSql = "FROM UnwantedEvent WHERE (isDeleted=false or isDeleted is null) and type='UE' AND operationUid=:operationUid";
				List<UnwantedEvent> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", session.getCurrentOperationUid());
				for (UnwantedEvent rec:rs) {
					String unwantedEventUid = rec.getUnwantedEventUid();
					String activityUid = rec.getActivityUid();
					
					Activity a = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, activityUid);
					if (a!=null) {
						if (a.getIsDeleted()==null) a.setIsDeleted(false);
						if (a.getDayPlus()==null) a.setDayPlus(0);
						if (!a.getIsDeleted()) {
							//CHECK BY USING IDS INTERNAL CLASS CODE FOR TP/TU EVENT
							String strSql2 = "SELECT DISTINCT internalCode FROM LookupClassCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (operationCode = :thisOperationCode OR " +
								"operationCode = '') AND groupUid = :thisGroupUid AND shortCode = :thisShortCode";
							String[] paramsFields = {"thisOperationCode","thisGroupUid","thisShortCode"};
							Object[] paramsValues = {session.getCurrentOperationType(),session.getCurrentGroupUid(),a.getClassCode()};					
							List ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
													
							if ("TP".equals(ls.get(0)) || "TU".equals(ls.get(0))) {
								if (a.getDayPlus()==0) {
									continue;
								} else {
									//if do not have next day, continue also
									if (!hasNextDay(a.getDailyUid())) {
										continue;
									}
								}
							}
						}
					}
					
					//move child event to root
					strSql = "FROM UnwantedEvent where (isDeleted=false or isDeleted is null) and type='UE' and parentUnwantedEventUid=:unwantedEventUid";
					List<UnwantedEvent> ueList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "unwantedEventUid", unwantedEventUid);
					for (UnwantedEvent ue_rec:ueList) {
						ue_rec.setParentUnwantedEventUid("");
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ue_rec);
					}
					//move associated activities to default UE
					strSql = "FROM ActivityUnwantedEventLink where (isDeleted=false or isDeleted is null) AND unwantedEventUid=:unwantedEventUid";
					List<ActivityUnwantedEventLink> aList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "unwantedEventUid", unwantedEventUid);
					for (ActivityUnwantedEventLink aLink:aList) {
						String thisActivityUid = aLink.getActivityUid();
						strSql = "FROM UnwantedEvent where (isDeleted=false or isDeleted is null) and type='UE' AND activityUid=:activityUid";
						List<UnwantedEvent> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", thisActivityUid);
						for (UnwantedEvent rec2:rs2) {
							String defaultUnwantedEventUid = rec2.getUnwantedEventUid();
							aLink.setUnwantedEventUid(defaultUnwantedEventUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(aLink);
							break;
						}
					}
					
					//remove activity_unwantedevent_link
					strSql = "FROM ActivityUnwantedEventLink where (isDeleted=false or isDeleted is null) AND activityUid=:activityUid";
					List<ActivityUnwantedEventLink> aueList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);
					for (ActivityUnwantedEventLink aLink:aueList) {
						aLink.setIsDeleted(true);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(aLink);
					}
					
					//remove unwanted event
					rec.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				loggingControl.endLoggingCommand();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
}
