package com.idsdatanet.d2.safenet.lessonTicketAdmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LessonTicketAdminDataLoaderInterceptor implements DataLoaderInterceptor{
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		String lessonTitle = (String) commandBean.getRoot().getDynaAttr().get("lessonTitle");
		List<String> selectedCountry = commandBean.getRoot().getMultiSelect().getValues().get("@country");
		String selectedCountryInString = "";
		if(selectedCountry!=null && selectedCountry.size()>0){
			for(String country : selectedCountry){
				if(country!=null && !StringUtils.equalsIgnoreCase(country, "null")){
					if(StringUtils.isBlank(selectedCountryInString)){
						selectedCountryInString = "'"+country+"'";
					}else{
						selectedCountryInString += ",'"+country+"'";
					}
				}
			}
		}
		
		List<String> selectedLessonStatus = commandBean.getRoot().getMultiSelect().getValues().get("@lessonStatus");
		String selectedLessonStatusInString = "";
		if(selectedLessonStatus!=null && selectedLessonStatus.size()>0){
			for(String lessonStatus : selectedLessonStatus){
				if(lessonStatus!=null && !StringUtils.equalsIgnoreCase(lessonStatus, "null")){
					if(StringUtils.isBlank(selectedLessonStatusInString)){
						selectedLessonStatusInString = "'"+lessonStatus+"'";
					}else{
						selectedLessonStatusInString += ",'"+lessonStatus+"'";
					}
				}
			}
		}
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String customCondition = "";
		
		customCondition += "(lessonReportType IS NULL OR lessonReportType = '') AND (isNcr is NULL OR isNcr = 0 OR isNcr= '')";
		
		// to reset @lessonTitle if @country and @lessonStatus is empty
		if(StringUtils.isBlank(selectedCountryInString) && StringUtils.isBlank(selectedLessonStatusInString)){
			lessonTitle = null;
			commandBean.getRoot().getDynaAttr().put("lessonTitle", null);
		}
		if(lessonTitle!=null && StringUtils.isNotBlank(lessonTitle)){
			if(StringUtils.isBlank(customCondition)){
				customCondition += "lessonTitle = :lessonTitle";
			}else{
				customCondition += " AND lessonTitle = :lessonTitle";
			}
			query.addParam("lessonTitle", lessonTitle);
		}
		if(selectedCountryInString!=null && StringUtils.isNotBlank(selectedCountryInString)){
			String relatedOperations = null;
			List<String> operationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT o.operationUid " +
					"FROM Well w, Operation o " +
					"WHERE (w.isDeleted IS NULL OR w.isDeleted=FALSE) " +
					"AND (o.isDeleted IS NULL OR o.isDeleted=FALSE) " +
					"AND w.wellUid=o.wellUid " +
					"AND w.country IN ("+selectedCountryInString+")");
			if(operationResult!=null && operationResult.size()>0){
				for(String operation : operationResult){
					if(relatedOperations==null)
						relatedOperations = "'"+operation+"'";
					else
						relatedOperations += ",'"+operation+"'";
				}
			}
			if(StringUtils.isBlank(customCondition)){
				customCondition += "operationUid IN("+relatedOperations+")";
			}else{
				customCondition += " AND operationUid IN("+relatedOperations+")";
			}
		}
		if(selectedLessonStatusInString!=null && StringUtils.isNotBlank(selectedLessonStatusInString)){
			if(StringUtils.isBlank(customCondition)){
				customCondition += "lessonStatus IN("+selectedLessonStatusInString+")";
			}else{
				customCondition += " AND lessonStatus IN("+selectedLessonStatusInString+")";
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
