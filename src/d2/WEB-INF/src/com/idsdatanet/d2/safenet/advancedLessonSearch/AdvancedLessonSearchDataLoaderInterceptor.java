package com.idsdatanet.d2.safenet.advancedLessonSearch;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class AdvancedLessonSearchDataLoaderInterceptor implements DataLoaderInterceptor{
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private Boolean cascadePhaseCodeFromOperationType = true;
	private Boolean cascadeTaskCodeFromPhaseCode = true;
	
	
	public void setCascadePhaseCodeFromOperationType(Boolean cascadePhaseCodeFromOperationType)
	{
		this.cascadePhaseCodeFromOperationType = cascadePhaseCodeFromOperationType;
	}
	public Boolean getCascadePhaseCodeFromOperationType() {
		return this.cascadePhaseCodeFromOperationType;
	}
	
	public void setCascadeTaskCodeFromPhaseCode(Boolean cascadeTaskCodeFromPhaseCode)
	{
		this.cascadeTaskCodeFromPhaseCode = cascadeTaskCodeFromPhaseCode;
	}
	public Boolean getCascadeTaskCodeFromPhaseCode() {
		return this.cascadeTaskCodeFromPhaseCode;
	}
	
	
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		String advancedLessonFilter = "";
		String onOffShoreInString = "";
		String rigTypeInString = "";
		String wellTypeInString = "";
		String countryInString = "";
		String wellNameInString = ""; 
		String statusInString = "";
		String lessonCategoryInString = "";
		String phaseCodeInString = "";
		String taskCodeInString = "";
		String jobtypeCodeInString = "";
		String rootcauseCodeInString = "";
		String holeSizeInString = "";
		String companiesInString = "";
		String wellborePurposeInString = "";
		String wellboreTypeInString = "";
		
		String queryParam = request.getParameter("queryParam");
		if (!StringUtils.isBlank(queryParam) || !StringUtils.isEmpty(queryParam)){
			byte[] decoded = Base64.decodeBase64(queryParam);
			String decodeText = new String(decoded, "UTF-8");
			String[] Array = decodeText.split(";");
			for(String Arr : Array){
				String[] searchCriteria = Arr.split(",");
				if("advancedLessonFilter".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						advancedLessonFilter += str;
					}
				}
				if("onOffShore".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (onOffShoreInString==null || onOffShoreInString==""){
							onOffShoreInString += str;
						}
						else{
							onOffShoreInString += ","+str;
						}
					}
				}
				if("rigType".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (rigTypeInString==null || rigTypeInString==""){
							rigTypeInString += str;
						}
						else{
							rigTypeInString += ","+str;
						}
					}
				}
				if("wellType".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (wellTypeInString==null || wellTypeInString==""){
							wellTypeInString += str;
						}
						else{
							wellTypeInString += ","+str;
						}
					}
				}
				if("country".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (countryInString==null || countryInString==""){
							countryInString += str;
						}
						else{
							countryInString += ","+str;
						}
					}
				}
				if("wellName".equals(searchCriteria[0].toString()))             
                {                                                               
                    for(int i=1; i > 0 && i < searchCriteria.length; i++){      
                        String str = searchCriteria[i].toString();              
                        if (wellNameInString==null || wellNameInString==""){    
                            wellNameInString += str;                            
                        }                                                       
                        else{                                                   
                            wellNameInString += ","+str;                        
                        }                                                       
                    }                                                           
                } 
				if("status".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (statusInString==null || statusInString==""){
							statusInString += str;
						}
						else{
							statusInString += ","+str;
						}
					}
				}
				if("lessonCategory".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (lessonCategoryInString==null || lessonCategoryInString==""){
							lessonCategoryInString += str;
						}
						else{
							lessonCategoryInString += ","+str;
						}
					}
				}
				if("phaseCode".equals(searchCriteria[0].toString()))
				{
					if(cascadePhaseCodeFromOperationType){
						for(int i=1; i > 0 && i < searchCriteria.length; i++){
							String str = searchCriteria[i].toString();
							str = str.replaceAll("\'", "");
							String[] phasecode = str.split("\\..");
							if (phaseCodeInString==null || phaseCodeInString==""){
								phaseCodeInString += "'"+phasecode[0].toString()+"'";
							}
							else{
								phaseCodeInString += ",'"+phasecode[0].toString()+"'";
							}
						}
					}else{
						for(int i=1; i > 0 && i < searchCriteria.length; i++){
							String str = searchCriteria[i].toString();
							if (phaseCodeInString==null || phaseCodeInString==""){
								phaseCodeInString += str;
							}
							else{
								phaseCodeInString += ","+str;
							}
						}
					}
				}
				if("taskCode".equals(searchCriteria[0].toString()))
				{
					if(cascadeTaskCodeFromPhaseCode){
						for(int i=1; i > 0 && i < searchCriteria.length; i++){
							String str = searchCriteria[i].toString();
							str = str.replaceAll("\'", "");
							String[] taskcode = str.split("\\..");
							if (taskCodeInString==null || taskCodeInString==""){
								taskCodeInString += "'"+taskcode[0].toString()+"'";
							}
							else{
								taskCodeInString += ",'"+taskcode[0].toString()+"'";
							}
						}
					}else{
						for(int i=1; i > 0 && i < searchCriteria.length; i++){
							String str = searchCriteria[i].toString().toString();
							if (taskCodeInString==null || taskCodeInString==""){
								taskCodeInString += str;
							}
							else{
								taskCodeInString += ","+str;
							}
						}
					}
				}
				if("jobtypeCode".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						str = str.replaceAll("\'", "");
						String[] jobtypecode = str.split("\\..");
						if (jobtypeCodeInString==null || jobtypeCodeInString==""){
							jobtypeCodeInString += "'"+jobtypecode[0].toString()+"'";
						}
						else{
							jobtypeCodeInString += ",'"+jobtypecode[0].toString()+"'";
						}
					}
				}
				if("rootcauseCode".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (rootcauseCodeInString==null || rootcauseCodeInString==""){
							rootcauseCodeInString += str;
						}
						else{
							rootcauseCodeInString += ","+str;
						}
					}
				}
				if("holeSize".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (holeSizeInString==null || holeSizeInString==""){
							holeSizeInString += str;
						}
						else{
							holeSizeInString += ","+str;
						}
					}
				}
				if("companies".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i > 0 && i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (companiesInString==null || companiesInString==""){
							companiesInString += str;
						}
						else{
							companiesInString += ","+str;
						}
					}
				}
				if("wellborePurpose".equals(searchCriteria[0].toString())) 
				{
					for(int i=1; i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (wellborePurposeInString==null || wellborePurposeInString=="") {
							wellborePurposeInString += str;
						}
						else {
							wellborePurposeInString += ","+str;
						}
					}
				}
				if("wellboreType".equals(searchCriteria[0].toString()))
				{
					for(int i=1; i < searchCriteria.length; i++){
						String str = searchCriteria[i].toString();
						if (wellboreTypeInString==null || wellboreTypeInString=="") {
							wellboreTypeInString += str;
						}
						else {
							wellboreTypeInString += ","+str;
						}
					}
				}
			}
		}
		
		String queryString = "(lessonReportType IS NULL OR lessonReportType = '') AND (isNcr is NULL OR isNcr = 0 OR isNcr= '')";
		if (!StringUtils.isBlank(advancedLessonFilter)){
			queryString += " AND (lessonTicketNumber like '%"+advancedLessonFilter+"%' or lessonTitle like '%"+advancedLessonFilter+"%' or rp2Date like '%"+advancedLessonFilter+"%' or rp2Originator like '%"+advancedLessonFilter+"%' or "+
			"descrLesson like '%"+advancedLessonFilter+"%' or descrPostevent like '%"+advancedLessonFilter+"%' or lessonResults like '%"+advancedLessonFilter+"%' or lessonLearning like '%"+advancedLessonFilter+"%' or "+
			"rigInformationUid in (select rigInformationUid from RigInformation where (isDeleted is null or isDeleted = false) and rigName like '%"+advancedLessonFilter+"%') or "+
			"wellUid in (select wellUid from Well where (isDeleted is null or isDeleted = false) and wellName like '%"+advancedLessonFilter+"%') or " +
			"reportedbyName like '%"+advancedLessonFilter+"%' or responsibleparty like '%"+advancedLessonFilter+"%')";
			
		}
		if (!StringUtils.isBlank(onOffShoreInString)){
			queryString += " AND (wellUid in (select wellUid from Well where onOffShore in ("+onOffShoreInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		if (!StringUtils.isBlank(rigTypeInString)){
			queryString += " AND (rigInformationUid in (select rigInformationUid from RigInformation where rigSubType in ("+rigTypeInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		if (!StringUtils.isBlank(wellTypeInString)){
			queryString += " AND (operationUid in (select operationUid from Operation where operationCode in ("+wellTypeInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		if (!StringUtils.isBlank(countryInString)){
			queryString += " AND (wellUid in (select wellUid from Well where country in ("+countryInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		if (!StringUtils.isBlank(wellNameInString)){                              
            queryString += " AND (wellUid in (select wellUid from Well where wellUid in ("+wellNameInString+") AND (isDeleted is null or isDeleted = false)))";
        } 
		if (!StringUtils.isBlank(statusInString)){
			queryString += " AND lessonStatus in ("+statusInString+")";
		}
		if (!StringUtils.isBlank(lessonCategoryInString)){
			queryString += " AND lessonType in ("+lessonCategoryInString+")";
		}
		if (!StringUtils.isBlank(phaseCodeInString)){
			queryString += " AND phaseCode in ("+phaseCodeInString+")";
		}
		if (!StringUtils.isBlank(taskCodeInString)){
			taskCodeInString = taskCodeInString.replaceAll("..[0-9]+","");
			queryString += " AND taskCode in ("+taskCodeInString+")";
		}
		if (!StringUtils.isBlank(jobtypeCodeInString)){
			queryString += " AND jobtypeCode in ("+jobtypeCodeInString+")";
		}
		if (!StringUtils.isBlank(rootcauseCodeInString)){
			queryString += " AND rootcauseCode in ("+rootcauseCodeInString+")";
		}
		if (!StringUtils.isBlank(holeSizeInString)){
			queryString += " AND (lessonTicketUid in (select lessonTicketUid from ActivityLessonTicketLink where activityUid in (select activityUid from Activity where holeSize in ("+holeSizeInString+") AND (isDeleted is null or isDeleted = false))))";
		}
		if (!StringUtils.isBlank(companiesInString)){
			queryString += " AND companies in ("+companiesInString+")";
		}
		if (!StringUtils.isBlank(wellborePurposeInString)){
			queryString += " AND (wellboreUid in (select wellboreUid from Wellbore where wellboreFinalPurpose in ("+wellborePurposeInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		if (!StringUtils.isBlank(wellboreTypeInString)){
			queryString += " AND (wellboreUid in (select wellboreUid from Wellbore where wellboreType in ("+wellboreTypeInString+") AND (isDeleted is null or isDeleted = false)))";
		}
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;	
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, queryString);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
