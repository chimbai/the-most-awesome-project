package com.idsdatanet.d2.safenet.lessonTicketDetail;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketDetailPopUpDataNodeAction implements DataNodeAllowedAction{

	@Override
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action,
			HttpServletRequest request) throws Exception {
		
		//Since searchViewMode can load other group data (from searched record), only allow edit when the data node's groupUid is same with session groupUid
		if(action.equals(Action.EDIT)) {
			if(request != null && !Boolean.valueOf(request.getParameter(LessonTicketDetailPopUpController.DIFFERENT_GROUP))) {
				return true;
			}else if(node.getData() != null 
					&& PropertyUtils.isReadable(node.getData(), "groupUid") 
					&& PropertyUtils.getProperty(node.getData(), "groupUid") != null
					&& PropertyUtils.getProperty(node.getData(), "groupUid").equals(session.getCurrentGroupUid())) {
				return true;
			}
		}
		return false;
	}
	
	

}
