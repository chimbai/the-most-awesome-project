package com.idsdatanet.d2.safenet.unwantedEvent;

import java.util.List;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class UnwantedEventActivityReportDataGenerator extends EmptyCommandBeanListener implements ReportDataGenerator {
	
	private HashMap<String,String> ueMap = new HashMap<String,String>();
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		//LOAD HASHMAP FOR unwantedEventUid and parentUnwantedEventUid
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userSelection.getOperationUid().toString())) thisOperationUid = userSelection.getOperationUid().toString();
		String sql = "SELECT unwantedEventUid, parentUnwantedEventUid FROM UnwantedEvent WHERE operationUid = :thisOperationUid AND type='UE' AND (isDeleted IS NULL OR isDeleted = '')";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisOperationUid", thisOperationUid);
		if(result.size() > 0)
		{
			for(Object[] obj: result)
			{
				if (obj[1]==null) {
					ueMap.put(obj[0].toString(), "root");
				} else {
					if(StringUtils.isBlank(obj[1].toString()) || obj[1].toString() == "root") {
						ueMap.put(obj[0].toString(), "root");
					} else {
						ueMap.put(obj[0].toString(), obj[1].toString());
					}
				}
			}
		}
			
		for (CommandBeanTreeNode node1: commandBean.getRoot().getList().get("UnwantedEvent").values()) {
			UnwantedEvent thisUnwantedEvent = (UnwantedEvent) node1.getData();
			node1.getDynaAttr().put("directParentUnwantedEventUid", RecursiveGetParentUid(thisUnwantedEvent.getUnwantedEventUid().toString()));
		}
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		if(StringUtils.isBlank(thisOperationUid)) return;
		
		//LOAD HASHMAP FOR unwantedEventUid and parentUnwantedEventUid
		String sql = "SELECT unwantedEventUid, parentUnwantedEventUid FROM UnwantedEvent WHERE operationUid = :thisOperationUid AND type='UE' AND (isDeleted IS NULL OR isDeleted = '')";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisOperationUid", thisOperationUid);
		if(result.size() > 0)
		{
			for(Object[] obj: result)
			{
				if (obj[1]==null) {
					ueMap.put(obj[0].toString(), "root");
				} else {
					if(StringUtils.isBlank(obj[1].toString()) || obj[1].toString() == "root") {
						ueMap.put(obj[0].toString(), "root");
					} else {
						ueMap.put(obj[0].toString(), obj[1].toString());
					}
				}
			}
		}
			
		String strSql = "SELECT rd.reportNumber, rd.reportDatetime, rd.daycost, rd.dailyUid, a.activityUid, aue.unwantedEventUid, a.startDatetime, a.endDatetime, a.activityDuration, a.classCode, " + 
			"a.phaseCode, a.taskCode, a.rootCauseCode, a.activityDescription, ue.parentUnwantedEventUid FROM Activity a, ActivityUnwantedEventLink aue, ReportDaily rd, UnwantedEvent ue " + 
			"WHERE a.operationUid = :thisOperationUid AND ue.type='UE' AND a.activityUid = aue.activityUid AND rd.dailyUid = a.dailyUid AND (aue.isDeleted IS NULL OR aue.isDeleted = '') AND " + 
			"(a.isDeleted IS NULL OR a.isDeleted = '') AND ue.unwantedEventUid = aue.unwantedEventUid ORDER BY rd.reportDatetime";
			
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisOperationUid", thisOperationUid);
		if (lstResult.size() > 0){
					
			for(Object[] objResult: lstResult){
				ReportDataNode reportDataNode2 = reportDataNode.addChild("UnwantedEventActivity");
				reportDataNode2.addProperty("reportNumber", objResult[0].toString());
					
				Date thisReportDatetime = (Date) objResult[1];
				Long reportDatetimeEpoch = thisReportDatetime.getTime();
				reportDataNode2.addProperty("reportDatetime", reportDatetimeEpoch.toString());
							
				reportDataNode2.addProperty("activityUid", objResult[4].toString());
				reportDataNode2.addProperty("unwantedEventUid", objResult[5].toString());
							
				Date thisStartDatetime = (Date) objResult[6];
				Long startDatetimeEpoch = thisStartDatetime.getTime();
				reportDataNode2.addProperty("startDatetime", startDatetimeEpoch.toString());
				
				Date thisEndDatetime = (Date) objResult[7];
				Long endDatetimeEpoch = thisEndDatetime.getTime();
				reportDataNode2.addProperty("endDatetime", endDatetimeEpoch.toString());
				
				reportDataNode2.addProperty("activityDuration", objResult[8].toString());
				reportDataNode2.addProperty("classCode", objResult[9].toString());
				reportDataNode2.addProperty("phaseCode", (objResult[10]!=null?objResult[10].toString():""));
				reportDataNode2.addProperty("taskCode", (objResult[11]!=null?objResult[11].toString():""));
				reportDataNode2.addProperty("rootCauseCode", (objResult[12]!=null?objResult[12].toString():""));
				reportDataNode2.addProperty("activityDescription", objResult[13].toString());
				
				//GET ESTIMATED LOSS COST
				String strDailyUid = objResult[3].toString();
				Double currentDayCost = 0.0;
				Double totalDuration = 0.0;
				Double estimatedLossCost = 0.0;
				if (objResult[2] != null) currentDayCost = Double.parseDouble(objResult[2].toString());
				Double daycostFromCostNet = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(strDailyUid);
				if(daycostFromCostNet!=null) currentDayCost = daycostFromCostNet;
								
				/*
				String strSql2 = "SELECT SUM(itemTotal) FROM CostDailysheet WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :dailyUid AND itemTotal IS NOT NULL";
				List<Double> lsResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "dailyUid", strDailyUid);
				if (lsResult2.size()>0) {
					Double daycostFromCostNet = lsResult2.get(0);
					if (daycostFromCostNet!=null) {
						if (daycostFromCostNet>0.0) {
							currentDayCost = daycostFromCostNet;
						}
					}
				}*/
				
				String strSql3 = "SELECT SUM(activityDuration) AS activityDuration FROM Activity " +
						"WHERE (isDeleted = false OR isDeleted IS NULL) " +
						"AND dailyUid = :dailyUid " +
						"AND (isSimop=false or isSimop is null) " +
						"AND (isOffline=false or isOffline is null)" +
						"AND (dayPlus IS NULL OR dayPlus=0) ";
				List<Double> lsResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "dailyUid", strDailyUid);
				if (lsResult3.size()>0) {
					if (lsResult3.get(0)!=null) totalDuration = lsResult3.get(0); 
				}
				estimatedLossCost = currentDayCost * (Double.parseDouble(objResult[8].toString())) / totalDuration * 3600;
				reportDataNode2.addProperty("estimatedLossCost", estimatedLossCost.toString());
				
				/*if(objResult[14] != null)
				{
					if(StringUtils.isNotBlank(objResult[14].toString()) && !"root".equals(objResult[14].toString()))
					{
						reportDataNode2.addProperty("parentUnwantedEventUid", objResult[14].toString());
					}
					else
					{
						reportDataNode2.addProperty("parentUnwantedEventUid", objResult[5].toString());
					}
				}
				else
				{
					reportDataNode2.addProperty("parentUnwantedEventUid", objResult[5].toString());
				}*/
				
				reportDataNode2.addProperty("parentUnwantedEventUid", RecursiveGetParentUid(objResult[5].toString()));
				
			}
		}
	}
	
	private String RecursiveGetParentUid(String unwantedEventUid) throws Exception {
		String parentUid = unwantedEventUid;
		if(!"root".equals(ueMap.get(unwantedEventUid)))
		{
			return RecursiveGetParentUid(ueMap.get(unwantedEventUid));
		}
		else
		{
			return parentUid;
		}
	}
}
