package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.LessonTicketProfile;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * Lesson Ticket Detail (ex-Murphy)
 * @author Jackson Ong
 *
 */
public class LessonTicketDetailCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener ,InitializingBean{

	private MailEngine mailEngine = null;
	private String support_email = null;
	private String sendNewMatchingLessonTicketTemplate = null;
	private String sendUpdatedMatchingLessonTicketTemplate = null;
	private String sendNotificationToResponsiblePartyEmailTemplate = null;
	private String lessonReportType = null;
	
	@Override
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN && Boolean.valueOf((String) request.getAttribute(LessonTicketDetailPopUpController.SEARCH_VIEW_MODE))) {
			//set searchViewMode layoutFilterFlag for front end custom logic checking use during searchViewMode
			commandBean.setLayoutFilterFlag(LessonTicketDetailPopUpController.SEARCH_VIEW_MODE, "true");
			commandBean.setLayoutFilterFlag(LessonTicketDetailPopUpController.LESSON_REPORT_TYPE, request.getParameter(LessonTicketDetailPopUpController.LESSON_REPORT_TYPE));
			commandBean.setLayoutFilterFlag(LessonTicketDetailPopUpController.DIFFERENT_GROUP, request.getParameter(LessonTicketDetailPopUpController.DIFFERENT_GROUP));
		}
	}
	

	public void setLessonReportType(String lessonReportType) {
		this.lessonReportType = lessonReportType;
	}

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		Map<String,CommandBeanTreeNode> ll = commandBean.getRoot().getChild("LessonTicket");
		Manager dao = ApplicationUtils.getConfiguredInstance().getDaoManager();
		if(ll != null){	
			for(Map.Entry<String,CommandBeanTreeNode> i : ll.entrySet()){
				CommandBeanTreeNode llNode = i.getValue();
				LessonTicket llRec = (LessonTicket) llNode.getData();
				
				String sql = "select ltce.lessonCategory, ltce.lessonElement, " + ""
						+ "(select lc.lessonTicketCategory from LookupLessonTicketCategory lc where (lc.isDeleted = false or lc.isDeleted is null) and lc.lookupLessonTicketCategoryUid = ltce.lessonCategory) as lessonCategoryLabel, " 
						+ "(select le.lessonTicketElements from LookupLessonTicketElements le where (le.isDeleted = false or le.isDeleted is null) and le.lookupLessonTicketElementsUid = ltce.lessonElement) as lessonElementLabel "
						+ "from LessonTicketCategoryElement ltce where (ltce.isDeleted is null or ltce.isDeleted = false) "
						+ "and ltce.lessonTicketUid = :lessonTicketUid";
				List<Object[]> ltces = dao.findByNamedParam(sql, "lessonTicketUid", llRec.getLessonTicketUid());
				if(ltces != null){
					String contatenatedCategoryElements = null;
					for(Object[] ltce : ltces){
						Object lessonCategoryLabel = ltce[2];
						Object lessonElementLabel = ltce[3];
						if(lessonCategoryLabel != null && lessonElementLabel != null){
							if(contatenatedCategoryElements == null){
								contatenatedCategoryElements = lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}else{
								contatenatedCategoryElements = contatenatedCategoryElements  + "\n" + lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}
						}
					}
					llRec.setCategoryElementLabels(contatenatedCategoryElements);
					dao.saveObject(llRec);
				}
			}
		}
	}

	public String getSendNotificationToResponsiblePartyEmailTemplate() {
		return sendNotificationToResponsiblePartyEmailTemplate;
	}

	public void setSendNotificationToResponsiblePartyEmailTemplate(
			String sendNotificationToResponsiblePartyEmailTemplate) {
		this.sendNotificationToResponsiblePartyEmailTemplate = sendNotificationToResponsiblePartyEmailTemplate;
	}

	private List<LessonTicketProfile> validProfile;
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		
		if ("getLessonTicketCategoryElementsList".equals(invocationKey)) {
			String lessonTicketUid = request.getParameter("lessonTicketUid");
			LessonTicketDetailFlexRemoteUtils a = new LessonTicketDetailFlexRemoteUtils();
			a.getLessonTicketCategoryElementsList(request, response, lessonTicketUid);
		} else if ("lessonTicketCategoryElementList".equals(invocationKey)) {
			String lessonTicketUid = request.getParameter("lessonTicketUid");
			LessonTicketDetailFlexRemoteUtils a = new LessonTicketDetailFlexRemoteUtils();
			a.getCategoryElementList(request, response, lessonTicketUid);
		} else if ("flexLessonDetailSave".equals(invocationKey)) {
			String lessonTicketUid = request.getParameter("lessonTicketUid");
			String selectedStr = request.getParameter("selectedStr");
			LessonTicketDetailFlexRemoteUtils a = new LessonTicketDetailFlexRemoteUtils();
			a.saveSelectedCategoryElementList(request, response, lessonTicketUid, selectedStr);
			
			//SAVE DONE
			this.existingProfile();
			this.sendLessonTicket(lessonTicketUid, commandBean, request);
		}
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(LessonTicket.class)) {
				LessonTicket lessonTicket = (LessonTicket)targetCommandBeanTreeNode.getData();
				if("sendEmailNotification".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					this.sendEmailNotification(lessonTicket.getLessonTicketUid(), (BaseCommandBean)commandBean, request);
				}
			}
		}
	}
	
	public void afterPropertiesSet() throws Exception
	{
		if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
		this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isBlank(support_email)) this.support_email = "support@idsdatanet.com";
	}
	
	/**
	 * Get all existing profile
	 * @throws Exception
	 */
	public void existingProfile() throws Exception
	{	
		String query = "FROM LessonTicketProfile WHERE (isDeleted = false or isDeleted is null) AND distributionKey is not null ORDER BY profileName";
		List<LessonTicketProfile> lessonTicketProfileList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(query);
		
		validProfile = new ArrayList<LessonTicketProfile>();
		
		for (LessonTicketProfile ltp:lessonTicketProfileList)
		{
			validProfile.add(ltp);
		}
	}
	
	/**
	 * Send Lesson Ticket upon confirm and save
	 * @param lessonTicketUid
	 * @param commandBean
	 * @param request 
	 * @throws Exception
	 */
	private void sendLessonTicket(String lessonTicketUid, BaseCommandBean commandBean, HttpServletRequest request) throws Exception
	{
		String hyperLink = null;
		String distributionKey = null;
		String[] emailList = null;
		String[] allEmailList = null;
		Date lastPushDate = null;
		String lessonTicketNumber = null;
		String lessonTicketNumberWithPrefix = null;
		Map<String, Object> lessonLearnedItems = new HashMap<String, Object>();
		
		for(LessonTicketProfile ltp:this.validProfile)
		{
			String sqlCondition = this.constructLessonProfileCondition(ltp);
			
			if (StringUtils.isNotBlank(sqlCondition))
			{
				String query = "FROM LessonTicket WHERE (isDeleted = false or isDeleted is null) AND lessonTicketUid=:lessonTicketUid AND " + sqlCondition;
				List<LessonTicket> lessonTicketList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, "lessonTicketUid", lessonTicketUid);
				
				if (lessonTicketList != null && lessonTicketList.size() > 0) {
					
					distributionKey = ltp.getDistributionKey();
					if(StringUtils.isNotBlank(distributionKey)) {
						emailList = this.getEmailList(ltp.getGroupUid(), distributionKey);
						
						if (allEmailList == null) {
							allEmailList = emailList;
						} else {
							allEmailList = (String[]) ArrayUtils.addAll(allEmailList, emailList);
						}
					}
					for (LessonTicket lessonTicket : lessonTicketList) {
						
						String lessonTicketPrefix = "";
						if (StringUtils.isNotBlank(lessonTicket.getLessonTicketNumberPrefix())) {
							lessonTicketPrefix = lessonTicket.getLessonTicketNumberPrefix();
						}
						
						String siteUrl = UserSession.getInstance(request).getClientBaseUrl();
						lessonTicketNumberWithPrefix = lessonTicketPrefix + " - " + lessonTicket.getLessonTicketNumber();
						hyperLink = siteUrl + "lessonticketdetail.html?gotowellop=" + lessonTicket.getOperationUid() + "&amp;gotoFieldName=lessonTicketUid&amp;gotoFieldUid=" +lessonTicket.getLessonTicketUid();
						Map<String, Object> lessonLearnedItem = new HashMap<String, Object>();
						lessonLearnedItem.put("lessonTicketUid", lessonTicket.getLessonTicketUid());
						lessonLearnedItem.put("lessonTicketNumber", lessonTicketNumberWithPrefix);
						lessonLearnedItem.put("operationUid", lessonTicket.getOperationUid());
						lessonLearnedItem.put("dailyUid", lessonTicket.getDailyUid());
						lessonLearnedItem.put("hyperLink", hyperLink);
						lessonLearnedItems.put(lessonTicket.getLessonTicketUid(), lessonLearnedItem);
						lastPushDate = lessonTicket.getLastPushDateTime();
						lessonTicketNumber = lessonTicket.getLessonTicketNumber();
					}
				}
			}
		}
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("lessonLearned", lessonLearnedItems);
		String emailSubject = "Lesson Learned Match Found";
		
		if(StringUtils.isNotBlank(this.constructEmailSubject(lastPushDate, lessonTicketNumberWithPrefix))) {
			emailSubject = this.constructEmailSubject(lastPushDate, lessonTicketNumberWithPrefix);
		}
		
		if(allEmailList != null && lessonLearnedItems != null){
			if (allEmailList.length > 0 && lessonLearnedItems.size() > 0) {
				if (StringUtils.isBlank(this.sendNewMatchingLessonTicketTemplate)) {
					commandBean.getSystemMessage().addError("Unable to send profile because email template not assigned");
				} else {
					if (lastPushDate == null) {
						mailEngine.sendMail(new String[]{}, allEmailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendNewMatchingLessonTicketTemplate, params));
					} else {
						mailEngine.sendMail(new String[]{}, allEmailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendUpdatedMatchingLessonTicketTemplate, params));
					}
					this.updateLastPushLessonTicket(lessonTicketUid);
				}
			}
		}
	}
	
	/**
	 * Send Email Notification to Responsible Party upon clicking "Send" button
	 * @param lessonTicketUid
	 * @param commandBean
	 * @param request 
	 * @throws Exception
	 */
	private void sendEmailNotification(String lessonTicketUid, BaseCommandBean commandBean, HttpServletRequest request) throws Exception
	{
		LessonTicket thisTicket = (LessonTicket) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LessonTicket.class, lessonTicketUid);
		String[] emailList=null;
		String hyperLink = null;
		
		if(!StringUtils.isBlank(thisTicket.getEmailRecipient())){

			String input = thisTicket.getEmailRecipient();
			if (!StringUtils.isBlank(input)){
				emailList=input.split(",");
			}
			
			String completeLessonTicket = thisTicket.getLessonTicketNumberPrefix() + " - " + thisTicket.getLessonTicketNumber();
			String emailSubject = "System Message - ";
			
			if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("plan")) {
				completeLessonTicket = "PLL - " + completeLessonTicket;
				emailSubject = "Planned " + "Lesson Learned (Lesson #: " + completeLessonTicket + ")";
			}else if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("non_op")) {
				completeLessonTicket = "Non-Op - " + completeLessonTicket;
				emailSubject = "Non-Op " + "Lesson Learned (Lesson #: " + completeLessonTicket + ")";
			}else {
				emailSubject = "Lesson Learned (Lesson #: " + completeLessonTicket + ")";
			}
			
			String url = request.getParameter("baseUrl");
			hyperLink = url + "?gotowellop=" + thisTicket.getOperationUid() + "&amp;gotoFieldName=lessonTicketUid&amp;gotoFieldUid=" +thisTicket.getLessonTicketUid();
			Map<String, Object> lessonTicket = new HashMap<String, Object>();
			lessonTicket.put("lessonTicketUid", thisTicket.getLessonTicketUid());
			lessonTicket.put("lessonTicketNumber", completeLessonTicket);
			lessonTicket.put("operationUid", thisTicket.getOperationUid());
			lessonTicket.put("hyperLink", hyperLink);
			lessonTicket.put("lessonReportType", lessonReportType);
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("lessonTicket", lessonTicket);
			
			if(StringUtils.isBlank(this.sendNotificationToResponsiblePartyEmailTemplate)) {
				commandBean.getSystemMessage().addError("Unable to send profile because email template not assigned");
			} else {
				mailEngine.sendMail(new String[]{}, emailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendNotificationToResponsiblePartyEmailTemplate, params));
				commandBean.getSystemMessage().addInfo("Lesson  " + completeLessonTicket + " has been successfully sent");
			}
		}
	}
	
	/**
	 * 
	 * @param ltp
	 * @param subject
	 * @return
	 * @throws Exception
	 */
	private String constructEmailSubject(Date lastPushDate, String lessonTicketNumberWithPrefix) throws Exception
	{
		String subject = null;
		if(lastPushDate == null) {
			subject = "Lessons Learned Match Found - <" + lessonTicketNumberWithPrefix + ">";
		} else {
			subject = "Lessons Learned Update - <" + lessonTicketNumberWithPrefix + ">";
		}

		return subject;
	}
	
	/**
	 * Update last push date in lesson ticket
	 * New lesson ticket create without last push date. After successfully ticket being send out, system will set last push date to that lesson ticket accordingly.
	 * @param lessonTicketUid
	 * @throws Exception
	 */
	private void updateLastPushLessonTicket(String lessonTicketUid) throws Exception
	{
		if (StringUtils.isNotBlank(lessonTicketUid)) {
			String strSql = "UPDATE LessonTicket SET lastPushDateTime=:lastPushDateTime WHERE lessonTicketUid =:lessonTicketUid AND (isDeleted is null OR isDeleted = false)";
			String[] paramsFields = {"lastPushDateTime", "lessonTicketUid"};
			Object[] paramsValues = {new Date(), lessonTicketUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		}
	}
	
	/**
	 * Get email list with distribution key
	 * @param groupUid
	 * @param distributionKey
	 * @return
	 * @throws Exception
	 */
	private String[] getEmailList(String groupUid, String distributionKey) throws Exception {
		
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(groupUid, distributionKey);
		return emailList;
	}
	
	/**
	 * Construct lesson profile group details condition, use to match lesson ticket category element.
	 * @param ltp
	 * @return
	 * @throws Exception
	 */
	private String constructLessonProfileCondition(LessonTicketProfile ltp) throws Exception
	{	
		String sqlCondition = "";
		
		String sql = "SELECT lessonTicketProfileGroupUid, elementContentLink FROM LessonTicketProfileGroup WHERE (isDeleted = false or isDeleted is null) AND lessonTicketProfileUid=:lessonProfileUid";
		List<Object[]> lstLessonProfileGroup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "lessonProfileUid", ltp.getLessonTicketProfileUid());
		
		if (lstLessonProfileGroup.size() > 0){
			Integer i = lstLessonProfileGroup.size();
			for(Object[] thisLessonProfileGroupResult: lstLessonProfileGroup){
				i = i - 1;
				sqlCondition = sqlCondition + "(";
				String profileGroupUid = null;
				String elementContentLink = null;
				if(thisLessonProfileGroupResult[0] != null) profileGroupUid = thisLessonProfileGroupResult[0].toString();
				if(thisLessonProfileGroupResult[1] != null) elementContentLink = thisLessonProfileGroupResult[1].toString();
				
				String sql2 = "SELECT lessonTicketCategoryUid, lessonTicketElementsUid FROM LessonTicketProfileDetail WHERE (isDeleted = false or isDeleted is null) AND lessonTicketProfileGroupUid=:lessonTicketProfileUid";
				List<Object[]> lstLessonProfileDetail = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "lessonTicketProfileUid", profileGroupUid);
				if (lstLessonProfileDetail.size() > 0){
					Integer j = lstLessonProfileDetail.size();
					for(Object[] thisLessonProfileGroupDetailResult: lstLessonProfileDetail){
						j = j - 1;
						String lessonElements = "";
						
						if(thisLessonProfileGroupDetailResult[1] != null) lessonElements = thisLessonProfileGroupDetailResult[1].toString();
						
						String sql3 = "SELECT llte.lookupLessonTicketElementsUid FROM " +
								"LookupLessonTicketCategory lltc, LookupLessonTicketElements llte " +
								"WHERE lltc.lookupLessonTicketCategoryUid=llte.lookupLessonTicketCategoryUid " +
								"AND (lltc.isDeleted is null OR lltc.isDeleted = false) AND (llte.isDeleted is null OR llte.isDeleted = false) " +
								"AND llte.lookupLessonTicketElementsUid=:lessonElements";
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql3, "lessonElements", lessonElements);
						
						//CHECK if lookup have been deleted.
						if(lstResult.size() > 0) {
							sqlCondition = sqlCondition + "lessonTicketUid in (SELECT lessonTicketUid FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted=false) AND lessonElement='" + lessonElements + "')";							
						} else {
							sqlCondition = sqlCondition + "lessonTicketUid in (SELECT lessonTicketUid FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted=false) AND lessonElement='')";
						}
						if(j!= 0) sqlCondition = sqlCondition + " " + elementContentLink + " ";
					}
				}
				sqlCondition = sqlCondition + ")";
				if(i!=0) sqlCondition = sqlCondition + " AND ";
			}
		}
		
		return sqlCondition;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}
	
	public String getSendNewMatchingLessonTicketTemplate() {
		return sendNewMatchingLessonTicketTemplate;
	}

	public void setSendNewMatchingLessonTicketTemplate(String sendNewMatchingLessonTicketTemplate) {
		this.sendNewMatchingLessonTicketTemplate = sendNewMatchingLessonTicketTemplate;
	}

	public void setSendUpdatedMatchingLessonTicketTemplate(String sendUpdatedMatchingLessonTicketTemplate) {
		this.sendUpdatedMatchingLessonTicketTemplate = sendUpdatedMatchingLessonTicketTemplate;
	}

	public String getSendUpdatedMatchingLessonTicketTemplate() {
		return sendUpdatedMatchingLessonTicketTemplate;
	}
}
