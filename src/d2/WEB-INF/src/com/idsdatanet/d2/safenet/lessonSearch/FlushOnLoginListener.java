package com.idsdatanet.d2.safenet.lessonSearch;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import com.idsdatanet.d2.core.web.event.LoginListener;
import com.idsdatanet.d2.core.web.security.MyUserDetails;
import com.idsdatanet.d2.core.web.security.SecurityUtils;

public class FlushOnLoginListener implements ApplicationListener{
	
	private Boolean enabled = false;
	public void onApplicationEvent(ApplicationEvent event) {
		if(enabled) {
			Object source = event.getSource();
			if (source instanceof LoginListener)
			{	
				String currentUserUid = null;
				Object principal = SecurityUtils.getAuthenticationPrincipal();
				if(principal instanceof MyUserDetails){
					currentUserUid = ((MyUserDetails) principal).getUser().getUserUid();
				}
				LessonSearchModule.flushUserSearchRecord(currentUserUid);
			}
		}
	}
	
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public Boolean getEnabled() {
		return enabled;
	}	
}
