package com.idsdatanet.d2.safenet.unwantedEvent;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class UnwantedEventDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof UnwantedEvent) {
			UnwantedEvent thisUnwantedEvent = (UnwantedEvent) object;
	
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisUnwantedEvent.getWellUid());
			node.getDynaAttr().put("location", well.getCountry());
			
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, thisUnwantedEvent.getOperationUid());
			node.getDynaAttr().put("operationName", operation.getOperationName());
			
			if (StringUtils.isNotBlank(thisUnwantedEvent.getRaisedBy())) {
				this.getOriginator(node, thisUnwantedEvent.getRaisedBy());
			}
			
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisUnwantedEvent.getOperationUid());
			node.getDynaAttr().put("eventRefPrefix", operationName);
			node.getDynaAttr().put("reportNumber", operationName + " " + thisUnwantedEvent.getEventRef());
						
			if (StringUtils.isNotBlank(thisUnwantedEvent.getPhaseCode())) {
				String phaseCode = thisUnwantedEvent.getPhaseCode();
				
				String[] paramsFields = {"thisOperationCode", "thisPhaseCode"};
				Object[] paramsValues = {userSelection.getOperationType(), phaseCode}; 
				String sql = "SELECT subOpsType FROM LookupPhaseCode where (isDeleted = false or isDeleted is null) AND operationCode = :thisOperationCode AND shortCode =:thisPhaseCode";
				List listResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
				if (listResult.size() > 0) {
					Object a = (Object) listResult.get(0);
					if (a != null && StringUtils.isNotBlank(a.toString())){
						
						node.getDynaAttr().put("subOpsType", a.toString());
					}
				}
			}
			
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
	
		if (object instanceof UnwantedEvent) {
			UnwantedEvent thisUnwantedEvent = (UnwantedEvent) object;
			thisUnwantedEvent.setType("UE");
			
			if (StringUtils.isBlank(thisUnwantedEvent.getUnwantedEventUid())) {
				if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "unwantedEventAutoNewReportNumber"))) {
						
					String[] paramsFields = {"operationUid", "eventRef"};
					Object[] paramsValues = {session.getCurrentOperationUid(), thisUnwantedEvent.getEventRef()}; 
					List listResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select eventRef from UnwantedEvent where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and eventRef =:eventRef", paramsFields, paramsValues);
					if (listResult.size() > 0) {
						String newEventRef = autoNewEventRef(node, session.getCurrentOperationUid() , session.getCurrentGroupUid());
						
						if (StringUtils.isNotBlank(newEventRef)) {
							thisUnwantedEvent.setEventRef(newEventRef);
						}
					}
				}else{
					String[] paramsFields = {"operationUid", "eventRef"};
					Object[] paramsValues = {session.getCurrentOperationUid(), thisUnwantedEvent.getEventRef()}; 
					String sql = "select eventRef from UnwantedEvent where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and type='UE' and eventRef =:eventRef";
					List listResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
					if (listResult.size() > 0) {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "eventRef", "Report no. " + thisUnwantedEvent.getEventRef() + " already exists in database");
					}
				}
			}else {
				String[] paramsFields = {"operationUid", "eventRef", "thisUnwantedEventUid"};
				Object[] paramsValues = {session.getCurrentOperationUid(), thisUnwantedEvent.getEventRef(), thisUnwantedEvent.getUnwantedEventUid()}; 
				String sql = "select eventRef from UnwantedEvent where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and type='UE' and eventRef =:eventRef and unwantedEventUid !=:thisUnwantedEventUid";
				List listResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
				if (listResult.size() > 0) {
					status.setContinueProcess(false, true);
					status.setFieldError(node, "eventRef", "Report no. " + thisUnwantedEvent.getEventRef() + " already exists in database");
				}
				
			}
			
			// allow to add item to company lookup
			String coName = (String) node.getDynaAttr().get("coName");
			
			if (StringUtils.isNotBlank(coName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupCompany where (isDeleted = false or isDeleted is null) and companyName = :companyName", "companyName", coName);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "coName", "The company '" + coName + "' already exists in database");
					status.setContinueProcess(false, true);
				}
				
				if (status.isProceed()) {
					LookupCompany thisLookupCompany = CommonUtil.getConfiguredInstance().createLookupCompany(coName);
					commandBean.getSystemMessage().addInfo("A new contractor has been created with minimal information from the Unwanted Event screen. Please make sure that the company lookup module is reviewed and relevant details are added in.");
					thisUnwantedEvent.setNameOfContractor(thisLookupCompany.getLookupCompanyUid());
				}
			}
			
			// set the current user as the person who raised the event when add new UER
			if (StringUtils.isBlank(thisUnwantedEvent.getUnwantedEventUid())) {
				//add new unwanted event
				thisUnwantedEvent.setRaisedBy(session.getUserUid());
			}
			
			//allow user to change status
			if ("by_date_only".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "allowChangeEventStatus"))) {
				if (thisUnwantedEvent.getEventDateClose() != null){
					thisUnwantedEvent.setEventStatus("closed");
					if (!this.isEventClosed(thisUnwantedEvent.getUnwantedEventUid())) {
						thisUnwantedEvent.setEventClosedBy(session.getUserUid());
					}
				}else {
					thisUnwantedEvent.setEventStatus("open");
				}
			} else {
				if (StringUtils.isNotBlank(thisUnwantedEvent.getEventStatus())){
					String eventStatus = thisUnwantedEvent.getEventStatus();
					
					if (eventStatus.equals("closed")) {
						if (thisUnwantedEvent.getEventDateClose() == null) {
							status.setContinueProcess(false, true);
							status.setFieldError(node, "eventDateClose", "This field is required when closing an event");
							return;
						}else if (thisUnwantedEvent.getEventDateClose().before(thisUnwantedEvent.getEventDatetime())) {
							status.setContinueProcess(false, true);
							status.setFieldError(node, "eventDateClose", "The closing date can not be earlier than date of unwanted event");
							return;
						}else {
							if (!this.isEventClosed(thisUnwantedEvent.getUnwantedEventUid())) {
								thisUnwantedEvent.setEventClosedBy(session.getUserUid());
							}
						}
					}else if (eventStatus.equals("open") || eventStatus.equals("inProgress")) {
						if (thisUnwantedEvent.getEventDateClose() != null) {
							status.setContinueProcess(false, true);
							status.setFieldError(node, "eventDateClose", "Event only can be re-opened if date close is empty");
							return;
						}
					}
				}else {
					if (thisUnwantedEvent.getEventDateClose() != null) {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "eventDateClose", "Event only can be re-opened if date close is empty");
						return;
					}
				}
			}
			
			if (StringUtils.isNotBlank(thisUnwantedEvent.getNameOfContractor())){
				thisUnwantedEvent.setCompany(thisUnwantedEvent.getNameOfContractor());
			}
			
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof UnwantedEvent) {
			UnwantedEvent thisUnwantedEvent = (UnwantedEvent) obj;
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				String activityUid = (String) commandBean.getRoot().getDynaAttr().get("activityId");
				if (activityUid != null) {
					ActivityUnwantedEventLink thisLink = new ActivityUnwantedEventLink();
					thisLink.setActivityUid(activityUid);
					thisLink.setUnwantedEventUid(thisUnwantedEvent.getUnwantedEventUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLink);
				}
			}
		}
	
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String activityUid = null;
		if (request !=null){
			activityUid = request.getParameter("activityId");
		}
		
		if (obj instanceof UnwantedEvent) {
			UnwantedEvent thisUnwantedEvent = (UnwantedEvent) obj;
			
			if(meta.getTableClass() == UnwantedEvent.class && StringUtils.isNotBlank(activityUid)){
				
				String strSql = "SELECT dailyUid, startDatetime FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
				String[] paramsFields = {"activityUid"};
				Object[] paramsValues = {activityUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0) {
					Object[] a = (Object[]) lstResult.get(0);
					if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(a[0].toString());
						thisUnwantedEvent.setEventDatetime(daily.getDayDate());
					}
				}
				commandBean.getRoot().addCustomNewChildNodeForInput(thisUnwantedEvent);
				commandBean.getRoot().getDynaAttr().put("activityId", activityUid);	
				
			}else {
				
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				if(daily == null) return;
				thisUnwantedEvent.setEventDatetime(daily.getDayDate());
			}
			
			thisUnwantedEvent.setEventStatus("open");
			
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "unwantedEventAutoNewReportNumber"))) {
				
				String newEventRef = autoNewEventRef(node,userSelection.getOperationUid(), userSelection.getGroupUid());
				
				if (StringUtils.isNotBlank(newEventRef)) {
					thisUnwantedEvent.setEventRef(newEventRef);
					String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), userSelection.getOperationUid());
					node.getDynaAttr().put("reportNumber", operationName + " " + newEventRef);
				}
			}
			
			String defaultResponsiblePerson = GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "unwantedEventResponsiblePersonDefaultSelection").toString();
			
			if (StringUtils.isNotBlank(defaultResponsiblePerson)) {
				String defaultuser = "";
				for (String item:defaultResponsiblePerson.toString().split(";")) {
					String defaultUsername = item.toString();
					
					if (StringUtils.isNotBlank(defaultUsername)) {
						defaultuser = defaultuser + "\t" + getUserUid(defaultUsername);
					}
				}
				
				if (defaultResponsiblePerson.indexOf(";") < -1) {
					defaultuser = getUserUid(defaultResponsiblePerson);					
				}
	
				thisUnwantedEvent.setEventSupervisor(defaultuser);
			}
			
		}
		
	}
	private String getUserUid(String username) throws Exception {
		
		String useruid = null;
		if (StringUtils.isNotBlank(username)) {
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select userUid from User where (isDeleted = false or isDeleted is null) and userName = :userName", "userName", username);
			if (list.size() > 0) {
				useruid = list.get(0).toString();
			}
		}
		
		return useruid;
	}
	
	private String autoNewEventRef(CommandBeanTreeNode node, String operationUid, String groupUid) throws Exception {
		String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
		node.getDynaAttr().put("eventRefPrefix", operationName);
		
		String newreportnumber = null;
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select eventRef from UnwantedEvent where (isDeleted = false or isDeleted is null) and type='UE' and operationUid = :operationUid order by cast(eventRef as int) DESC", "operationUid", operationUid);
		if (list.size() > 0) {
			for(Object objResult: list){
				
				String eventRef = objResult.toString();
			
				if (StringUtils.isNumeric(eventRef)) {
					Integer intEventRef = (Integer)Integer.parseInt(eventRef) + 1;
			
					newreportnumber = intEventRef.toString();
					
					if (newreportnumber.length()== 1){
						newreportnumber = "00" + newreportnumber;
					}else if (newreportnumber.length() == 2 ) {
						newreportnumber = "0" + newreportnumber;
					}else {
						
					}
					return newreportnumber;
				}
			}
			
			if (newreportnumber == null) newreportnumber ="001";
			
		}else {
			newreportnumber ="001";
		}
		
		return newreportnumber;
	}

	private void getOriginator (CommandBeanTreeNode node, String currentUserUid) throws Exception {
		if (StringUtils.isNotBlank(currentUserUid)) {
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select fname from User where (isDeleted = false or isDeleted is null) and userUid = :userUid", "userUid", currentUserUid);
			if (list.size() > 0) {
				node.getDynaAttr().put("originator", list.get(0).toString());
			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		Object object = sourceNode.getData();
		
		if (object instanceof UnwantedEvent) {
			UnwantedEvent unwantedevent = (UnwantedEvent) object;
			UnwantedEvent newUnwantedEvent = (UnwantedEvent) BeanUtils.cloneBean(unwantedevent);
			newUnwantedEvent.setUnwantedEventUid(null);
			newUnwantedEvent.setRaisedBy(null);
			
			if ("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "unwantedEventAutoNewReportNumber"))) {
				
				String newEventRef = autoNewEventRef(targetNode, userSession.getCurrentOperationUid(), userSession.getCurrentGroupUid());
				
				if (StringUtils.isNotBlank(newEventRef)) {
					newUnwantedEvent.setEventRef(newEventRef);
					String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSession.getCurrentGroupUid(), userSession.getCurrentOperationUid());
					targetNode.getDynaAttr().put("reportNumber", operationName + " " + newEventRef);
					
				}
			}
			
			targetNode.setData(newUnwantedEvent);
			targetNode.getDynaAttr().put("originator", null);
			
		}
	}
	
	private boolean isEventClosed (String unwantedEventUid) throws Exception {
		if (StringUtils.isNotBlank(unwantedEventUid)) {
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select eventStatus from UnwantedEvent where (isDeleted = false or isDeleted is null) and unwantedEventUid = :unwantedEventUid", "unwantedEventUid", unwantedEventUid);
			if (list.size() > 0) {
				if (list.get(0) != null) {
					String eventStatus = list.get(0).toString();
					
					if ("closed".equals(eventStatus)){
						return true;
					}else{
						return false;
					}
				}
			}
		}
		return false;
	}
	
}
