package com.idsdatanet.d2.safenet.environmentDischarges;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.FileBridgeFiles;
import com.idsdatanet.d2.core.model.LookupEnvironmentDischarges;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;



public class EnvironmentDischargesCommandBeanListener extends EmptyCommandBeanListener {

    public static final String UID = "lookupEnvironmentDischargesUid";
    public static final String OOC_WET_AVERAGE = "OOC (wet) (Average)";
    public static final String VESSEL_DIESEL = "Vessel Diesel";
    public static final String MODU_DIESEL = "MODU Diesel";
    public static final String WASTES = "Wastes";

    @Override
    public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
    	String dailyUid = userSelection.getDailyUid();
    	root.getDynaAttr().put("dailyUid", dailyUid);
	}
    
    @Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
        if (UID.equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
    		EnvironmentDischarges thisEnvironmentDischarges = (EnvironmentDischarges) targetCommandBeanTreeNode.getData();	
			String lookupEnvironmentDischargesUid = thisEnvironmentDischarges.getLookupEnvironmentDischargesUid();
			if (lookupEnvironmentDischargesUid != null && StringUtils.isNotBlank(lookupEnvironmentDischargesUid)) {
                List<LookupEnvironmentDischarges> lookupEnvironmentDischarges = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupEnvironmentDischarges WHERE (isDeleted = FALSE or isDeleted IS NULL) AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid", UID, lookupEnvironmentDischargesUid);
                if (!lookupEnvironmentDischarges.isEmpty()) {
                    LookupEnvironmentDischarges led = lookupEnvironmentDischarges.get(0);

                    thisEnvironmentDischarges.setStoredUnit(led.getStoredUnit());

                    // 47362 - To populate the lookupLabel as dynaAttr for screen filter use
                    targetCommandBeanTreeNode.getDynaAttr().put(UID, led.getName());
                    String dailyUid = commandBean.getRoot().getDynaAttr().get("dailyUid").toString();

                    // 47362 - Null the field if OOC WET AVERAGE
                    if (StringUtils.equals(led.getName(), OOC_WET_AVERAGE)) {
                        thisEnvironmentDischarges.setActualAmount(null);
                    }
                    
                    if (StringUtils.equals(led.getName(), VESSEL_DIESEL)) {
                    	Double svUsedTotal = EnvironmentDischargesUtil.calculateUsedDieselFromSV(commandBean, targetCommandBeanTreeNode, dailyUid);
                        thisEnvironmentDischarges.setActualAmount(svUsedTotal);
                    }else if (StringUtils.equals(led.getName(), MODU_DIESEL)) {
                    	Double rsUsedTotal = EnvironmentDischargesUtil.calculateUsedDieselFromRigStock(commandBean, targetCommandBeanTreeNode, dailyUid);
                        thisEnvironmentDischarges.setActualAmount(rsUsedTotal);
                    }else {
                        thisEnvironmentDischarges.setActualAmount(null);
                    }
                }
			}
		}
	}	
}
