package com.idsdatanet.d2.safenet.nonConformance;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.model.NonconformanceEventLog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

// nonConformance - jhwong
public class nonConformanceDataNodeListener extends EmptyDataNodeListener {

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {	
		Object object = node.getData();
		
		if (object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
			Date date = CommonUtil.getConfiguredInstance().currentDateTime(userSelection.getGroupUid(), userSelection.getWellUid());
			
			// Removing the time from date (so time is 00:00:00:00)
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			date = calendar.getTime();
			
			thisNonconformanceEvent.setEventDateTime(date);
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;
			thisNonconformanceEvent.setLastEditUserFullname(session.getCurrentUser().getFname());
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		if(object instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) object;			
				
			// Only on current records
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING) {
				NonconformanceEventLog newNonconformanceEventLog = new NonconformanceEventLog();
				newNonconformanceEventLog.setRootCauseDescription(thisNonconformanceEvent.getRootCauseDescription());
				newNonconformanceEventLog.setCorrectiveAction(thisNonconformanceEvent.getCorrectiveAction());
				newNonconformanceEventLog.setEventStatus(thisNonconformanceEvent.getEventStatus());
				newNonconformanceEventLog.setTargetDueDateTime(thisNonconformanceEvent.getTargetDueDateTime());
				newNonconformanceEventLog.setLogUpdatedBy(thisNonconformanceEvent.getLastEditUserFullname());
				newNonconformanceEventLog.setLogDateTime(thisNonconformanceEvent.getLastEditDatetime());
				newNonconformanceEventLog.setNonconformanceEventUid(thisNonconformanceEvent.getNonconformanceEventUid());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newNonconformanceEventLog);
				//commandBean.getSystemMessage().addInfo("A NCR Historical Log has been created.", true);
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj = node.getData();
		
		if (obj instanceof NonconformanceEvent) {
			NonconformanceEvent thisNonconformanceEvent = (NonconformanceEvent) obj;
			String strSql = "UPDATE NonconformanceEventLog SET isDeleted = true WHERE nonconformanceEventUid =:thisNonconformanceEventUid";
			String[] paramsFields = {"thisNonconformanceEventUid"};
			Object[] paramsValues = {thisNonconformanceEvent.getNonconformanceEventUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		}
	}
}