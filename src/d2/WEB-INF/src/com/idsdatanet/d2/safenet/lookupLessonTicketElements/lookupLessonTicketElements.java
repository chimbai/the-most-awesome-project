package com.idsdatanet.d2.safenet.lookupLessonTicketElements;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.LookupLessonTicketElements;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class lookupLessonTicketElements extends EmptyDataNodeListener implements DataLoaderInterceptor{

	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		String selectedLookupLessonTicketCategoryUid = (String) commandBean.getRoot().getDynaAttr().get("lookupLessonTicketCategoryUid");
		
		Object object = node.getData();
		if (object instanceof LookupLessonTicketElements) {
			LookupLessonTicketElements data = (LookupLessonTicketElements) object;
			data.setLookupLessonTicketCategoryUid(selectedLookupLessonTicketCategoryUid);
		}
	}
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedLookupLessonTicketCategoryUid = (String) commandBean.getRoot().getDynaAttr().get("lookupLessonTicketCategoryUid");
			if (selectedLookupLessonTicketCategoryUid==null) selectedLookupLessonTicketCategoryUid = "";
			customCondition = "lookupLessonTicketCategoryUid = :lookupLessonTicketCategoryUid";
			query.addParam("lookupLessonTicketCategoryUid", selectedLookupLessonTicketCategoryUid);
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
