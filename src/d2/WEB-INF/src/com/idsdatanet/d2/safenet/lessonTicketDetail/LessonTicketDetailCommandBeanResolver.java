package com.idsdatanet.d2.safenet.lessonTicketDetail;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.web.mvc.CachedUserSession;
import com.idsdatanet.d2.core.web.mvc.CommandBeanResolver;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketDetailCommandBeanResolver implements CommandBeanResolver, InitializingBean{
	
	private String defaultCommandBean = null;
	private String searchViewModeCommandBean = null;
	
	public void setDefaultCommandBean(String defaultCommandBean) {
		this.defaultCommandBean = defaultCommandBean;
	}

	public void setSearchViewModeCommandBean(String searchViewModeCommandBean) {
		this.searchViewModeCommandBean = searchViewModeCommandBean;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if(this.defaultCommandBean == null || this.searchViewModeCommandBean == null) {
			throw new Exception("default commandbean or searchViewMode commandbean are not set");
		}
	}

	@Override
	public String resolveBeanName(HttpServletRequest request) throws Exception {
		if(request != null && Boolean.valueOf((String) request.getAttribute(LessonTicketDetailPopUpController.SEARCH_VIEW_MODE))) {
			return searchViewModeCommandBean;
		}
		return defaultCommandBean;
	}

	@Override
	public String resolveBeanName(UserSession session) throws Exception {
		return null;
	}

	@Override
	public String resolveBeanName(UserSelectionSnapshot userSelection) throws Exception {
		return null;
	}
	
	@Override
	public String resolveBeanName(CachedUserSession cachedUserSession) throws Exception {
		return null;
	}
}
