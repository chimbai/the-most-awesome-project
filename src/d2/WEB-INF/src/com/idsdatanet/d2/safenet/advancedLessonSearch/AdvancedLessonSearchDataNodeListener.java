package com.idsdatanet.d2.safenet.advancedLessonSearch;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.RigInformations;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.CostAccountCodes;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.query.representation.DatabaseQuery;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._RigInformation;
import com.idsdatanet.d2.core.web.session.ParameterizedQueryFilter;

public class AdvancedLessonSearchDataNodeListener extends EmptyDataNodeListener implements DataNodeAllowedAction {
	
	private String lessonStage = null;
	private String lessonReportType =null;
	private List<String> operationType;
	
	public void setOperationType(List<String> operationType)
	{
		if(operationType != null){
			this.operationType = new ArrayList<String>();
			for(String value: operationType){
				this.operationType.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getOperationType() {
		return this.operationType;
	}
	
	public void setLessonStage(String value){
		this.lessonStage = value;
	}
	
	public void setLessonReportType(String value){
		this.lessonReportType = value;
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
			
			Well thisWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisLessonTicket.getWellUid());
			if (thisWell != null){
				node.getDynaAttr().put("country", thisWell.getCountry());
			}
	
			String userid = thisLessonTicket.getLastEditUserUid();
			if (userid != null) {
				String[] paramsFields = {"thisUserUid"};
				Object[] paramsValues = {userid};
				
				String strSql = "SELECT fname FROM User WHERE userUid = :thisUserUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (!lstResult.isEmpty())
				{	
					Object thisResult = (Object) lstResult.get(0);
					
					thisLessonTicket.setLastEditUserUid(thisResult.toString());
				}
				
			}
			
			wellHeader(node, thisLessonTicket.getWellUid(), thisLessonTicket.getOperationUid());
			if (thisLessonTicket.getLessonTicketUid()!=null)
			{
				String[] paramsFields = {"thisLessonTicketUid"};
				Object[] paramsValues = {thisLessonTicket.getLessonTicketUid()};			
				String strSql = "SELECT activityUid FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and lessonTicketUid = :thisLessonTicketUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (!lstResult.isEmpty())
				{	
					Object thisResult = (Object) lstResult.get(0);		
					activityDetail(node, thisResult.toString(), commandBean, false);
				}
				
			}
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + thisLessonTicket.getLessonTicketNumber());
			
			/*
			String dailyUid = userSelection.getDailyUid();
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisLessonTicket.getOperationUid());
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid";
			String[] paramNames = {"reportType", "dailyUid"};
			Object[] paramValues = {reportType, dailyUid};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
			if (reportDailyList.size()>0) {
				ReportDaily reportDaily = reportDailyList.get(0);
				node.getDynaAttr().put("reportedbyName", reportDaily.getSupervisor());
				node.getDynaAttr().put("responsibleparty", reportDaily.getCompEngineer());
			}
			*/
		}
	}
	
	public void activityDetail(CommandBeanTreeNode child, String activityUid, CommandBean commandBean, Boolean copyActivityDescription) throws Exception{
		String businessSegment="", classCode="", sections="",depthMdMsl="", incidentLevel="", phaseCode="",taskCode="", rootCauseCode="", nptMainCategory="", nptSubCategory="", activityDescription="", dailyUid="", jobTypeCode="";
		String strSql = "SELECT phaseCode, taskCode, rootCauseCode, dailyUid, businessSegment, classCode, sections, depthMdMsl, incidentLevel, nptMainCategory, nptSubCategory, activityDescription, jobTypeCode FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
		String[] paramsFields = {"activityUid"};
		Object[] paramsValues = {activityUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0) {
			Object[] a = (Object[]) lstResult.get(0);
			if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
				phaseCode = a[0].toString();
			}		
			if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
				taskCode = a[1].toString();
			}		
			if (a[2] != null && StringUtils.isNotBlank(a[2].toString())){
				rootCauseCode = a[2].toString();
			}
			if (a[3] != null && StringUtils.isNotBlank(a[3].toString())){
				dailyUid = a[3].toString();
			}
			if (a[4] != null && StringUtils.isNotBlank(a[4].toString())){
				businessSegment = a[4].toString();
			}
			if (a[5] != null && StringUtils.isNotBlank(a[5].toString())){
				classCode = a[5].toString();
			}
			if (a[6] != null && StringUtils.isNotBlank(a[6].toString())){
				sections = a[6].toString();
			}
			if (a[7] != null && StringUtils.isNotBlank(a[7].toString())){
				depthMdMsl = a[7].toString();
			}
			if (a[8] != null && StringUtils.isNotBlank(a[8].toString())){
				incidentLevel = a[8].toString();
			}
			if (a[9] != null && StringUtils.isNotBlank(a[9].toString())){
				nptMainCategory = a[9].toString();
			}
			if (a[10] != null && StringUtils.isNotBlank(a[10].toString())){
				nptSubCategory = a[10].toString();
			}
			if (a[11] != null && StringUtils.isNotBlank(a[11].toString())){
				activityDescription = a[11].toString();
			}
			if (a[12] != null && StringUtils.isNotBlank(a[12].toString())){
				jobTypeCode = a[12].toString();
			}
			child.getDynaAttr().put("businessSegment", businessSegment);						
			child.getDynaAttr().put("classCode", classCode);
			child.getDynaAttr().put("sections", sections);		
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "depthMdMsl");
			if (thisConverter.isUOMMappingAvailable() & StringUtils.isNotBlank(depthMdMsl)){
				child.setCustomUOM("depthMdMsl", thisConverter.getUOMMapping());
				thisConverter.setBaseValue(Double.parseDouble(depthMdMsl));
			}
			
			child.getDynaAttr().put("depthMdMsl", thisConverter.getFormattedValue());
			
			child.getDynaAttr().put("incidentLevel", incidentLevel);
			child.getDynaAttr().put("phaseCode", phaseCode);
			child.getDynaAttr().put("taskCode", taskCode);
			child.getDynaAttr().put("rootCauseCode", rootCauseCode);
			child.getDynaAttr().put("nptMainCategory", nptMainCategory);
			child.getDynaAttr().put("nptSubCategory", nptSubCategory);
			child.getDynaAttr().put("activityUid", activityUid);
			child.getDynaAttr().put("dailyUid", dailyUid);
			child.getDynaAttr().put("jobTypeCode", jobTypeCode);
			
			if (copyActivityDescription && "postdrill".equals(this.lessonStage)){
				Object obj = child.getData();
				if (obj instanceof LessonTicket) {
					LessonTicket thisNewLessonTicket = (LessonTicket) obj;
					thisNewLessonTicket.setDescrEvent(activityDescription);
					thisNewLessonTicket.setPhaseCode(phaseCode);
					thisNewLessonTicket.setTaskCode(taskCode);
				}
			}
			
		}
	}
	
	public void wellHeader(CommandBeanTreeNode child, String wellUid, String operationUid) throws Exception{
		String country="", field="", opCo="", operationName="", strSql ="";
		List lstResult=null;
		if (wellUid !=null)
		{
			strSql = "SELECT country, field FROM Well WHERE (isDeleted = false or isDeleted is null) and wellUid =:wellUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", wellUid);
			
			if (lstResult.size() > 0) {
				Object[] a = (Object[]) lstResult.get(0);
				if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
					country = a[0].toString();
				}
				if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
					field = a[1].toString();
				}
			}				
		}
		if (operationUid !=null)
		{
			strSql = "SELECT opCo, operationName FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			
			if (lstResult.size() > 0) {
				Object[] a = (Object[]) lstResult.get(0);
				if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
					opCo = a[0].toString();
				}
				if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
					operationName = a[1].toString();
				}
			}
		}
		child.getDynaAttr().put("country", country);
		child.getDynaAttr().put("field", field);
		child.getDynaAttr().put("opCo", opCo);
		child.getDynaAttr().put("operationName", operationName);
		
	}
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if(StringUtils.equals(action, "delete") || StringUtils.equals(action, "edit")){	
			return false;	
		}
		return true;
	}

}


