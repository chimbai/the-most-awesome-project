package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.net.URLDecoder;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LessonTicketUserListLookupHandler implements LookupHandler {
	private String table="";
	private String key = "";
	private String value="";
	private String format = "";
	private String order="";
	private String aclGroupName="";
	private boolean globalMode = false;
	
	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getAclGroupName() {
		return aclGroupName;
	}

	public void setAclGroupName(String aclGroupName) {
		this.aclGroupName = aclGroupName;
	}
	
	public void setGlobalMode(boolean globalMode) {
		this.globalMode = globalMode;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		
		if(globalMode) {
			// ticket: 14349
			//responsibleparty and emailRecipient from lesson ticket record (with user list from different child group and absent in global) allows lookup label available for data indexing 
			//and on screen display since ES data indexing happen in global site only
			String sql = "select distinct responsibleparty, emailRecipient from LessonTicket where (isDeleted is null or isDeleted = false) "
					+ "and (responsibleparty is not null and responsibleparty <> '') "
					+ "and (emailRecipient is not null and emailRecipient <> '') order by responsibleparty";

			List<Object[]> lsResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);

			if(lsResults.size() > 0) {
				for (Object[] obj : lsResults) {
					if(!result.containsKey(obj[1])) {
						if (StringUtils.isNotBlank(this.format)) {
							String format = URLDecoder.decode(this.format, "UTF-8");
							StringBuilder sb = new StringBuilder();
							try (Formatter f = new Formatter(sb)) {
								f.format(format, new Object[] {obj[0], obj[1]});
							}
							result.put(obj[1].toString(), new LookupItem(obj[1].toString(),sb.toString()));
						}
					}
				}
			}
		}else {
			String sortAttr= "";
			if(StringUtils.isNotEmpty(this.order)) sortAttr = "ORDER BY " + this.order;
			
			String sql = "FROM User u, UserAclGroup ua WHERE ua.userUid=u.userUid " + 
						"AND (u.isDeleted = false OR u.isDeleted is null) AND (u.isblocked = 0 OR u.isblocked is null) " +
						"AND (ua.isDeleted = false or ua.isDeleted is null) " +
						"AND ua.aclGroupUid IN (SELECT aclGroupUid FROM AclGroup WHERE name='" +this.aclGroupName+"') " +
						sortAttr;
			
			List<Object[]> lsResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			
			if (lsResults.size() > 0) {
				for (Object[] obj : lsResults) {
					//Object[] rec = (Object[]) obj;
					String ukey = (String) PropertyUtils.getProperty(obj[0], this.key);
					
					if(StringUtils.isNotBlank(ukey)) {
						String formatted="";
						Map item = PropertyUtils.describe(obj[0]);
						String value_data = "";
						if (StringUtils.isBlank(this.value)) this.value = this.key;
						Object[] values = this.value.split(",");
						for (int i=0; i<values.length; i++) {
							String fieldName = (String)values[i];
							if (item.containsKey(fieldName.trim())) {
								values[i] = CommonUtil.getConfiguredInstance().nullToEmptyString(item.get(values[i]));
								value_data += (StringUtils.isNotBlank(value_data)?" ":"") + values[i];
							} else {
								values[i] = "";
							}
						}
						
						if (StringUtils.isNotBlank(this.format) && values.length>0) { //format value
							String format = URLDecoder.decode(this.format, "UTF-8");
							StringBuilder sb = new StringBuilder();
							try (Formatter f = new Formatter(sb)) {
								f.format(format, values);
							}
							formatted = sb.toString();
						}
										
						result.put(ukey, new LookupItem(ukey, formatted));
					}
				}
			}
		}
		
		return result;
	}

}

