package com.idsdatanet.d2.safenet.inspection;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupInspectionType;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.EmptyDataSummary;
import com.idsdatanet.d2.core.web.mvc.Summary;
import com.idsdatanet.d2.core.web.mvc.SummaryInfo;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class InspectionDataSummaryListener extends EmptyDataSummary {
	private List<Summary> data = null;
	
	public void generate(HttpServletRequest request) throws Exception {
		this.data = new ArrayList<Summary>();
		UserSession session = UserSession.getInstance(request);
		String operation = session.getCurrentOperationUid();
		
		String inspectionType = request.getParameter("type");
		if (inspectionType==null) inspectionType = "";
		String strSql = "FROM LookupInspectionType where (isDeleted=false or isDeleted is null) and inspectionTypeCode=:inspectionTypeCode";
		List<LookupInspectionType> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "inspectionTypeCode", inspectionType);
		if (rs.size()>0) {
			LookupInspectionType obj = rs.get(0);
			String lookupInspectionTypeUid = obj.getLookupInspectionTypeUid();
			String[] paramNames = {"lookupInspectionTypeUid","operationUid"};
			String[] paramValues = {lookupInspectionTypeUid,operation};
			strSql = "select d.dailyUid FROM Daily d, InspectionResult r where (d.isDeleted=false or d.isDeleted is null) and (r.isDeleted=false or r.isDeleted is null) and d.dailyUid=r.dailyUid and r.lookupInspectionTypeUid=:lookupInspectionTypeUid and r.operationUid=:operationUid order by d.dayDate";
			List<String> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
			Summary summary = new Summary("Day # : ");
			for (String dailyUid:rs2) {
				Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				List reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid", "dailyUid", thisDaily.getDailyUid());
				String reportNumber = "1";
				if(reportDailyList != null && reportDailyList.size() > 0) {
					ReportDaily reportDaily = (ReportDaily) reportDailyList.get(0);
					reportNumber = reportDaily.getReportNumber();
					if (!session.withinAccessScope(reportDaily)) {
						continue;
					}
				}
				summary.addInfo(new SummaryInfo(reportNumber, dailyUid));
			}
			if (summary.getInfo().size() > 0) {
				this.data.add(summary);
			}
		}
		
	}

	public List<Summary> getData() {
		return this.data;
	}
}
