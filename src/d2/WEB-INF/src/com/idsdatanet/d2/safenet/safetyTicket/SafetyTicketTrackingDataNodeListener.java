package com.idsdatanet.d2.safenet.safetyTicket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.SafetyTicket;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SafetyTicketTrackingDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private static String SORTORDER = "ASC";

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
	}
	
	
	public String checkHideRigIssueStatus(String rigInformationUid) throws Exception
	{
		String sql = "select hideRigIssue from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid=:rigInformationUid";	
		
		String[] paramsFields = {"rigInformationUid"};
		Object[] paramsValues = {rigInformationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);	
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			if(thisResult != null) 
				{
					if("true".equals(thisResult.toString()))
					{
						return "";
					}				
				}
		}
		return rigInformationUid;
	}
	
	public String checkOperationIsFilterToRig(String operationUid, String rigInformationUid) throws Exception
	{
		String sql = "select operationUid from Operation where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and rigInformationUid=:rigInformationUid";
		
		String[] paramsFields = {"operationUid", "rigInformationUid"};
		Object[] paramsValues = {operationUid, rigInformationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);	
		if (lstResult.isEmpty())
		{
			return "";
		}
		return operationUid;
	}
	
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
			
		if (meta.getTableClass().equals(SafetyTicket.class)) {
			
			//TO OBTAIN THESE FILTER VALUE FROM USER SELECTION
			String selectedRigInformationUid  = "";
			selectedRigInformationUid = (String) commandBean.getRoot().getDynaAttr().get("rigInformationUid");			
						
			selectedRigInformationUid = checkHideRigIssueStatus(selectedRigInformationUid) ;
						
			String linkedRigInformationUid = request.getParameter("rigInformationUid");
			if(!StringUtils.isBlank(linkedRigInformationUid) || linkedRigInformationUid != null)
			{
				selectedRigInformationUid = checkHideRigIssueStatus(linkedRigInformationUid) ;
				commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			}	
						
			String selectedTypeOfIssue  = (String) commandBean.getRoot().getDynaAttr().get("typeOfIssue");
			String selectedSortedBy  = (String) commandBean.getRoot().getDynaAttr().get("sortedBy");
			String selectedSortingMethod  = (String) commandBean.getRoot().getDynaAttr().get("sortingMethod");
						
			String strSql = "FROM SafetyTicket WHERE rigInformationUid = :rigInformationUid";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			//NOT ALLOW THE FOLLOWING FIELDS TO BE BLANK, SET DEFAULT FILTERING
			if(StringUtils.isBlank(selectedRigInformationUid) || selectedRigInformationUid == null)
			{
				selectedRigInformationUid = checkHideRigIssueStatus(userSelection.getRigInformationUid()) ;
				commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			}
						
			if(StringUtils.isBlank(selectedTypeOfIssue))
			{
				selectedTypeOfIssue = "";
			}else if("true".equals(selectedTypeOfIssue)) // closed
			{
				selectedTypeOfIssue = " AND (completedDateTime IS NOT NULL OR completedDateTime <> '')";
			}else{ //open
				selectedTypeOfIssue = " AND (completedDateTime IS NULL OR completedDateTime = '')";
			}
			
			if(StringUtils.isBlank(selectedSortedBy))
			{
				selectedSortedBy = "sequence";
				commandBean.getRoot().getDynaAttr().put("sortedBy", "sequence");
			}
			
			if(StringUtils.isBlank(selectedSortingMethod))
			{
				selectedSortingMethod = "ASC";
				commandBean.getRoot().getDynaAttr().put("sortingMethod", "ASC");
			}
			
			SORTORDER = selectedSortingMethod;
				
			strSql = strSql +selectedTypeOfIssue+ " AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY " + selectedSortedBy + " " + selectedSortingMethod;
			arrayParamNames.add("rigInformationUid");
										
			arrayParamValues.add(selectedRigInformationUid);			
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<SafetyTicket> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			//USE COMPARATOR TO SORT WHEN SORT TYPE is ReferenceNumber
			if (selectedSortedBy.equalsIgnoreCase("sequence")) {
				Collections.sort(items, new SafetyTicketComparator());
			}
			
			for(Object objResult: items){
				SafetyTicket thisSafetyTicket = (SafetyTicket) objResult;
				output_maps.add(thisSafetyTicket);
			}
						
			return output_maps;
						
		}
		
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private class SafetyTicketComparator implements Comparator<SafetyTicket>{
		public int compare(SafetyTicket o1, SafetyTicket o2){
			try {				
				String f1 = "";
				if(o1.getSequence() != null) f1 = WellNameUtil.getPaddedStr(o1.getSequence().toString());
				String f2 = "";
				if(o2.getSequence() != null) f2 = WellNameUtil.getPaddedStr(o2.getSequence().toString());
				if (f1 == null || f2 == null) return 0;
				if ("DESC".equalsIgnoreCase(SORTORDER)) {
					return f2.compareTo(f1);
				} else {
					return f1.compareTo(f2);
				}				
				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}	
}
