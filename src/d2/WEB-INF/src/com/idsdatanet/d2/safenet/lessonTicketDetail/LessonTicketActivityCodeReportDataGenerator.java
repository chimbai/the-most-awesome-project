package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class LessonTicketActivityCodeReportDataGenerator implements ReportDataGenerator {
	private String lessonReportType = null;
	
	public void setLessonReportType(String lessonReportType) {
		this.lessonReportType = lessonReportType;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		this.generateLessonTicketActivityCode(userContext, reportDataNode, new String[] {"phaseCode", "LookupPhaseCode"});
		this.generateLessonTicketActivityCode(userContext, reportDataNode, new String[] {"taskCode", "LookupTaskCode"});
		this.generateLessonTicketActivityCode(userContext, reportDataNode, new String[] {"rootcauseCode", "LookupRootCauseCode"});
	}

	public void generateLessonTicketActivityCode(UserContext userContext, ReportDataNode reportDataNode, String[] prop) throws Exception {
		// TODO Auto-generated method stub
		String strSql = "SELECT l.lessonTicketUid, o.operationCode, l." + prop[0] + " FROM LessonTicket l, Operation o WHERE l.operationUid = o.operationUid" +
						" AND (l.isDeleted = FALSE OR l.isDeleted IS NULL) AND (o.isDeleted = FALSE OR o.isDeleted IS NULL) AND l.wellUid = :thisWellUid";
		
		if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("plan")) {
			strSql += " AND l.lessonReportType = 'plan'";
		}else if(StringUtils.isNotBlank(lessonReportType) && lessonReportType.equals("non_op")) {
			strSql += " AND l.lessonReportType = 'non_op'";
		}else {
			strSql += " AND (l.lessonReportType is null or l.lessonReportType = '')";
		}

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisWellUid", userContext.getUserSelection().getWellUid());
		
		if (lstResult.size()>0) {
			Map<String,String> result = new HashMap<String,String>();
			String onOffShore = getWellOnOffShore(userContext);
			ReportDataNode thisReportNode = reportDataNode.addChild(prop[1]);
			
			for (Object lstLL : lstResult) {
				Object[] lt = (Object[]) lstLL;
				
				String strSql2 = "SELECT name, shortCode FROM " + prop[1] + " WHERE (operationCode = '" + lt[1] + "' OR operationCode='' OR operationCode IS NULL)" +
						(prop[0]== "rootcauseCode" ? "" : " AND (onOffShore = '" + onOffShore + "' OR onOffShore='' OR onOffShore is null)") + " AND shortCode = :shortCode" +
						" AND (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive IS NULL OR isActive = TRUE)";
				String[] paramNames = {"shortCode"};
				Object[] values = {lt[2].toString()};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames, values);
				
				if (lstResult2.size()>0) {
					for (Object lstLookup : lstResult2) {
						Object[] lookup = (Object[]) lstLookup;
						String lookupLabel = lookup[0].toString() + " (" + lookup[1].toString() + ")";
						result.put(lt[0].toString(), lookupLabel);
					}
				}
			}
			
			for (Map.Entry<String, String> entry : result.entrySet()) {
				ReportDataNode rd = thisReportNode.addChild(prop[1]);
				rd.addProperty("lessonTicketUid", entry.getKey());
				rd.addProperty("lookupLabel", entry.getValue());
			}
		}
	}
	
	public String getWellOnOffShore (UserContext userContext) throws Exception {
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userContext.getUserSelection().getWellUid());
		String onOffShore = "";
		if (well!=null) onOffShore = well.getOnOffShore();
		return onOffShore ;
	}
}
