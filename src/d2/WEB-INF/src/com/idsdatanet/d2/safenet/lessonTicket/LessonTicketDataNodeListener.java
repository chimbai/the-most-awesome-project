package com.idsdatanet.d2.safenet.lessonTicket;

import com.idsdatanet.d2.safenet.lessonTicket.LessonTicketUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketDataNodeListener extends EmptyDataNodeListener implements DataNodeAllowedAction {
	
	private Boolean autoPopulateUsernameToReportedByName = true;
	private String lessonStage = null;
	private String lessonReportType =null;
	private List<String> operationType;
	
	public void setOperationType(List<String> operationType)
	{
		if(operationType != null){
			this.operationType = new ArrayList<String>();
			for(String value: operationType){
				this.operationType.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getOperationType() {
		return this.operationType;
	}
	
	public void setLessonStage(String value){
		this.lessonStage = value;
	}
	
	public void setLessonReportType(String value){
		this.lessonReportType = value;
	}
	
	public Boolean getAutoPopulateUsernameToReportedByName() {
		return this.autoPopulateUsernameToReportedByName;
	}
	
	public void setAutoPopulateUsernameToReportedByName(Boolean value){
		this.autoPopulateUsernameToReportedByName = value;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) object;
	
			String userid = thisLessonTicket.getLastEditUserUid();
			if (userid != null) {
				String[] paramsFields = {"thisUserUid"};
				Object[] paramsValues = {userid};
				
				String strSql = "SELECT fname FROM User WHERE userUid = :thisUserUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (!lstResult.isEmpty())
				{	
					Object thisResult = (Object) lstResult.get(0);
					
					thisLessonTicket.setLastEditUserUid(thisResult.toString());
				}
				
			}
			
			wellHeader(node, thisLessonTicket.getWellUid(), thisLessonTicket.getOperationUid());
			if (thisLessonTicket.getLessonTicketUid()!=null)
			{
				String[] paramsFields = {"thisLessonTicketUid"};
				Object[] paramsValues = {thisLessonTicket.getLessonTicketUid()};			
				String strSql = "SELECT activityUid FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and lessonTicketUid = :thisLessonTicketUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (!lstResult.isEmpty())
				{	
					Object thisResult = (Object) lstResult.get(0);		
					activityDetail(node, thisResult.toString(), commandBean, false);
				}
				
			}
			
			String lessonTicketPrefix = "";
			if (StringUtils.isNotBlank(thisLessonTicket.getLessonTicketNumberPrefix())) {
				lessonTicketPrefix = thisLessonTicket.getLessonTicketNumberPrefix();
			}
			
			node.getDynaAttr().put("lessonTicketNumber", lessonTicketPrefix + thisLessonTicket.getLessonTicketNumber());
	
			/*
			String dailyUid = userSelection.getDailyUid();
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisLessonTicket.getOperationUid());
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid";
			String[] paramNames = {"reportType", "dailyUid"};
			Object[] paramValues = {reportType, dailyUid};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
			if (reportDailyList.size()>0) {
				ReportDaily reportDaily = reportDailyList.get(0);
				node.getDynaAttr().put("reportedbyName", reportDaily.getSupervisor());
				node.getDynaAttr().put("responsibleparty", reportDaily.getCompEngineer());
			}
			*/
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				String activityUid = (String) commandBean.getRoot().getDynaAttr().get("activityId");
				if (StringUtils.isNotBlank(activityUid)) {
					ActivityLessonTicketLink thisLink = new ActivityLessonTicketLink();
					thisLink.setActivityUid(activityUid);
					thisLink.setLessonTicketUid(thisLessonTicket.getLessonTicketUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLink);
				}
				
				//auto populate case no 20100924-0539-slfu 
				//String newreportnumber = null;
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoNewLessonTicketNumber"))){
					
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					String prefixPattern = GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberPrefixPattern");
					
					String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), null, null);
					
					if (StringUtils.isNotBlank(getPrefixName)){
						thisLessonTicket.setLessonTicketNumberPrefix(getPrefixName);
					}
					
					//ticket#26804 gwp lessonTicketNumberByWWO select lesson ticket largest number by well/wellbore/operation based
					List list = null;
					String newreportnumber = null;
					String strCondition = null;
					if (this.lessonReportType == null){
						strCondition = " And (lessonReportType IS NULL OR lessonReportType = '')";
					}
					else{
						strCondition = " And lessonReportType = 'geo_lesson'";
					}
					
					if ("well".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
							list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellUid = :wellUid"+strCondition+" order by lessonTicketNumber DESC", "wellUid", userSelection.getWellUid());
					}
					if ("wellbore".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid"+strCondition+" order by lessonTicketNumber DESC", "wellboreUid", userSelection.getWellboreUid());
					}
					if ("operation".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"))){
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lessonTicketNumber from LessonTicket where (isDeleted = false or isDeleted is null) and operationUid = :operationUid"+strCondition+" order by lessonTicketNumber DESC", "operationUid", userSelection.getOperationUid());
					}
					
					if (list.size() > 0) {
						Collections.sort(list, new LessonTicketNumberComparator());
						String lessonTicketNumber = null;
						Object a = list.get(0);
						if (a!=null) lessonTicketNumber = a.toString();
							
						if (StringUtils.isNotBlank(lessonTicketNumber) && StringUtils.isNumeric(lessonTicketNumber)) {
							Integer intEventRef = (Integer)Integer.parseInt(lessonTicketNumber) + 1;
					
							newreportnumber = intEventRef.toString();
							
							if (newreportnumber.length()== 1){
								newreportnumber = "0" + newreportnumber;
							}
						}else {
							newreportnumber = "01";
						}
	
					}else {
						newreportnumber = "01";
					}
					thisLessonTicket.setLessonTicketNumber(newreportnumber);
					thisLessonTicket.setLessonTicketType(GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberByWWO"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLessonTicket);
					
				}
			}
		}
	
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof LessonTicket) {
			
			LessonTicket lessonTicket = (LessonTicket) node.getData();
			if (this.lessonStage !=null) {
				lessonTicket.setLessonStage(this.lessonStage);
			}
			String value = GroupWidePreference.getValue(session.getCurrentGroupUid(), "auto_populate_lesson_learnt_date"); 
			if ("1".equals(value) || lessonTicket.getEventDateTime()==null) {
				Daily daily = session.getCurrentDaily();
				if (daily!=null) {
					lessonTicket.setEventDateTime(daily.getDayDate());
				}
			}
			
			if(this.getOperationType() != null){
				if (StringUtils.isNotBlank(session.getCurrentDailyUid())) {
					ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), session.getCurrentDailyUid());
					if (reportDaily != null) {				
						if (LessonTicketUtils.checkReportTypeIncluded(this.operationType, reportDaily.getReportType().toString())){
							lessonTicket.setReportedbyName(reportDaily.getSupervisor());
							lessonTicket.setResponsibleparty(reportDaily.getCompEngineer());
						}							
					}						
				}
			}
			
			if(StringUtils.isNotBlank(this.lessonReportType)){
				lessonTicket.setLessonReportType(this.lessonReportType);
			}
			
			// save the last edit user full name (20100912-2215-whayani)
			lessonTicket.setLastEditUserFullname(session.getCurrentUser().getFname());
			
		}
	}
	
	//if redirect to different screen and param value pass thru the url, need to use newRecordInputMode=1
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if("1".equals(request.getParameter("newRecordInputMode"))){
			Object obj = node.getData();
			
			String activityUid = null;
			if (request !=null){
				activityUid = request.getParameter("activityId");
			}
			
			if (obj instanceof LessonTicket) {
				LessonTicket thisLessonTicket = (LessonTicket) obj;
				
				if(meta.getTableClass() == LessonTicket.class && StringUtils.isNotBlank(activityUid)){
					
					String strSql = "SELECT phaseCode, taskCode, rootCauseCode, dailyUid, jobTypeCode FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
					String[] paramsFields = {"activityUid"};
					Object[] paramsValues = {activityUid};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0) {
						Object[] a = (Object[]) lstResult.get(0);
						if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
							thisLessonTicket.setPhaseCode(a[0].toString());
						}
						
						if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
							thisLessonTicket.setTaskCode(a[1].toString());
						}
						
						if (a[2] != null && StringUtils.isNotBlank(a[2].toString())){
							thisLessonTicket.setRootcauseCode(a[2].toString());
						}
						
						if (a[3] != null && StringUtils.isNotBlank(a[3].toString())){
							Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(a[3].toString());
							thisLessonTicket.setRp2Date(daily.getDayDate());
						}
						if (a[4] != null && StringUtils.isNotBlank(a[4].toString())){
							thisLessonTicket.setJobtypeCode(a[4].toString());
						}
					}else {
						if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
							Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
							thisLessonTicket.setRp2Date(daily.getDayDate());
						}
					}
					
					wellHeader(node, userSelection.getWellUid(), userSelection.getOperationUid());
					activityDetail(node, activityUid, commandBean, true);
					commandBean.getRoot().getDynaAttr().put("activityId", activityUid);	
							
				}else {
					if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
						thisLessonTicket.setRp2Date(daily.getDayDate());
					}
				}
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoNewLessonTicketNumber"))){
					String wwo = GroupWidePreference.getValue(userSelection.getGroupUid(), "lessonTicketNumberByWWO");
					wwo = wwo.toUpperCase().substring(0, 1)+wwo.substring(1);
					commandBean.getSystemMessage().addInfo("Current Lesson Learned Ticket Number Is Generated by "+wwo, true);
				}
				
				if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
					ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), userSelection.getDailyUid());
					if (reportDaily!=null) {
						//node.getDynaAttr().put("reportedbyName", reportDaily.getSupervisor());
						//node.getDynaAttr().put("responsibleparty", reportDaily.getCompEngineer());
						String value = GroupWidePreference.getValue(userSelection.getGroupUid(), "auto_populate_lesson_learnt_date"); 
						if ("1".equals(value)) {
							thisLessonTicket.setEventDateTime(reportDaily.getReportDatetime());
						}
						
						if (this.getOperationType() != null){
							if (LessonTicketUtils.checkReportTypeIncluded(this.operationType, reportDaily.getReportType().toString())){
								thisLessonTicket.setReportedbyName(reportDaily.getSupervisor());
								thisLessonTicket.setResponsibleparty(reportDaily.getCompEngineer());
							}									
						}
					}
				}
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoPopulateUsernameToOriginator"))){
					//save the current user name who created the lesson learned records 
					String fullname="";
					fullname = LessonTicketUtils.getUserFullName(userSelection);
					String rp2Originator="";
					rp2Originator = thisLessonTicket.getRp2Originator();
					if(rp2Originator==null || rp2Originator.equalsIgnoreCase(""))
					{
						if(fullname!=null || StringUtils.isNotBlank(fullname)){
							thisLessonTicket.setRp2Originator(fullname);
						}
					}
					
				}
				thisLessonTicket.setLessonStatus("OPEN");

				if (autoPopulateUsernameToReportedByName){
					//save the current user name who created the lesson learned records 
					String fullname="";
					fullname = LessonTicketUtils.getUserFullName(userSelection);
					String reportedbyName="";
					reportedbyName = thisLessonTicket.getReportedbyName();
					if(reportedbyName==null || reportedbyName.equalsIgnoreCase("")){
						if(fullname!=null || StringUtils.isNotBlank(fullname)){
							thisLessonTicket.setReportedbyName(fullname);
						}
					}
				}
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String activityUid = null;
		if (request !=null){
			activityUid = request.getParameter("activityId");
		}
		
		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			
			if(meta.getTableClass() == LessonTicket.class && StringUtils.isNotBlank(activityUid)){
				
				String strSql = "SELECT phaseCode, taskCode, rootCauseCode, dailyUid, jobTypeCode FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
				String[] paramsFields = {"activityUid"};
				Object[] paramsValues = {activityUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0) {
					Object[] a = (Object[]) lstResult.get(0);
					if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
						thisLessonTicket.setPhaseCode(a[0].toString());
					}
					
					if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
						thisLessonTicket.setTaskCode(a[1].toString());
					}
					
					if (a[2] != null && StringUtils.isNotBlank(a[2].toString())){
						thisLessonTicket.setRootcauseCode(a[2].toString());
					}
					
					if (a[3] != null && StringUtils.isNotBlank(a[3].toString())){
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(a[3].toString());
						thisLessonTicket.setRp2Date(daily.getDayDate());
					}
					if (a[4] != null && StringUtils.isNotBlank(a[4].toString())){
						thisLessonTicket.setJobtypeCode(a[4].toString());
					}
				}else {
					if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
						thisLessonTicket.setRp2Date(daily.getDayDate());
					}
				}
				
				
				wellHeader(node, userSelection.getWellUid(), userSelection.getOperationUid());
				activityDetail(node, activityUid, commandBean, true);
					
						
			}else {
				if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
					Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
					thisLessonTicket.setRp2Date(daily.getDayDate());
				}
			}
			
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoNewLessonTicketNumber"))){
				String wwo = GroupWidePreference.getValue(userSelection.getGroupUid(), "lessonTicketNumberByWWO");
				wwo = wwo.toUpperCase().substring(0, 1)+wwo.substring(1);
				commandBean.getSystemMessage().addInfo("Current Lesson Learned Ticket Number Is Generated by "+wwo, true);
			}
			
			if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), userSelection.getDailyUid());
				if (reportDaily!=null) {
					//node.getDynaAttr().put("reportedbyName", reportDaily.getSupervisor());
					//node.getDynaAttr().put("responsibleparty", reportDaily.getCompEngineer());
					String value = GroupWidePreference.getValue(userSelection.getGroupUid(), "auto_populate_lesson_learnt_date"); 
					if ("1".equals(value)) {
						thisLessonTicket.setEventDateTime(reportDaily.getReportDatetime());
					}
					
					if (this.getOperationType() != null){
						if (LessonTicketUtils.checkReportTypeIncluded(this.operationType, reportDaily.getReportType().toString())){
							thisLessonTicket.setReportedbyName(reportDaily.getSupervisor());
							thisLessonTicket.setResponsibleparty(reportDaily.getCompEngineer());
						}									
					}
				}
			}
			
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoPopulateUsernameToOriginator"))){
				//save the current user name who created the lesson learned records 
				String fullname="";
				fullname = LessonTicketUtils.getUserFullName(userSelection);
				String rp2Originator="";
				rp2Originator = thisLessonTicket.getRp2Originator();
				if(rp2Originator==null || rp2Originator.equalsIgnoreCase(""))
				{
					if(fullname!=null || StringUtils.isNotBlank(fullname)){
						thisLessonTicket.setRp2Originator(fullname);
					}
				}
			}
			if (autoPopulateUsernameToReportedByName){
				//save the current user name who created the lesson learned records 
				String fullname="";
				fullname = LessonTicketUtils.getUserFullName(userSelection);
				String reportedbyName="";
				reportedbyName = thisLessonTicket.getReportedbyName();
				if(reportedbyName==null || reportedbyName.equalsIgnoreCase("")){
					if(fullname!=null || StringUtils.isNotBlank(fullname)){
						thisLessonTicket.setReportedbyName(fullname);
					}
				}
			}
		}
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();

		if (obj instanceof LessonTicket) {
			LessonTicket thisLessonTicket = (LessonTicket) obj;
			LessonTicketUtils.deleteActivityLessonTicketLink(thisLessonTicket.getLessonTicketUid());

		}
	}
	
	public void activityDetail(CommandBeanTreeNode child, String activityUid, CommandBean commandBean, Boolean copyActivityDescription) throws Exception{
		String businessSegment="", classCode="", sections="",depthMdMsl="", incidentLevel="", phaseCode="",taskCode="", rootCauseCode="", nptMainCategory="", nptSubCategory="", activityDescription="", dailyUid="", jobTypeCode="";
		String strSql = "SELECT phaseCode, taskCode, rootCauseCode, dailyUid, businessSegment, classCode, sections, depthMdMsl, incidentLevel, nptMainCategory, nptSubCategory, activityDescription, jobTypeCode FROM Activity WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
		String[] paramsFields = {"activityUid"};
		Object[] paramsValues = {activityUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0) {
			Object[] a = (Object[]) lstResult.get(0);
			if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
				phaseCode = a[0].toString();
			}		
			if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
				taskCode = a[1].toString();
			}		
			if (a[2] != null && StringUtils.isNotBlank(a[2].toString())){
				rootCauseCode = a[2].toString();
			}
			if (a[3] != null && StringUtils.isNotBlank(a[3].toString())){
				dailyUid = a[3].toString();
			}
			if (a[4] != null && StringUtils.isNotBlank(a[4].toString())){
				businessSegment = a[4].toString();
			}
			if (a[5] != null && StringUtils.isNotBlank(a[5].toString())){
				classCode = a[5].toString();
			}
			if (a[6] != null && StringUtils.isNotBlank(a[6].toString())){
				sections = a[6].toString();
			}
			if (a[7] != null && StringUtils.isNotBlank(a[7].toString())){
				depthMdMsl = a[7].toString();
			}
			if (a[8] != null && StringUtils.isNotBlank(a[8].toString())){
				incidentLevel = a[8].toString();
			}
			if (a[9] != null && StringUtils.isNotBlank(a[9].toString())){
				nptMainCategory = a[9].toString();
			}
			if (a[10] != null && StringUtils.isNotBlank(a[10].toString())){
				nptSubCategory = a[10].toString();
			}
			if (a[11] != null && StringUtils.isNotBlank(a[11].toString())){
				activityDescription = a[11].toString();
			}
			if (a[12] != null && StringUtils.isNotBlank(a[12].toString())){
				jobTypeCode = a[12].toString();
			}
			child.getDynaAttr().put("businessSegment", businessSegment);						
			child.getDynaAttr().put("classCode", classCode);
			child.getDynaAttr().put("sections", sections);		
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "depthMdMsl");
			if (thisConverter.isUOMMappingAvailable() & StringUtils.isNotBlank(depthMdMsl)){
				child.setCustomUOM("depthMdMsl", thisConverter.getUOMMapping());
				thisConverter.setBaseValue(Double.parseDouble(depthMdMsl));
			}
			
			child.getDynaAttr().put("depthMdMsl", thisConverter.getFormattedValue());
			
			child.getDynaAttr().put("incidentLevel", incidentLevel);
			child.getDynaAttr().put("phaseCode", phaseCode);
			child.getDynaAttr().put("taskCode", taskCode);
			child.getDynaAttr().put("rootCauseCode", rootCauseCode);
			child.getDynaAttr().put("nptMainCategory", nptMainCategory);
			child.getDynaAttr().put("nptSubCategory", nptSubCategory);
			child.getDynaAttr().put("activityUid", activityUid);
			child.getDynaAttr().put("dailyUid", dailyUid);
			child.getDynaAttr().put("jobTypeCode", jobTypeCode);
			
			if (copyActivityDescription && "postdrill".equals(this.lessonStage)){
				Object obj = child.getData();
				if (obj instanceof LessonTicket) {
					LessonTicket thisNewLessonTicket = (LessonTicket) obj;
					thisNewLessonTicket.setDescrEvent(activityDescription);
					thisNewLessonTicket.setPhaseCode(phaseCode);
					thisNewLessonTicket.setTaskCode(taskCode);
				}
			}
			
		}
	}
	
	public void wellHeader(CommandBeanTreeNode child, String wellUid, String operationUid) throws Exception{
		String country="", field="", opCo="", operationName="", strSql ="";
		List lstResult=null;
		if (wellUid !=null)
		{
			strSql = "SELECT country, field FROM Well WHERE (isDeleted = false or isDeleted is null) and wellUid =:wellUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", wellUid);
			
			if (lstResult.size() > 0) {
				Object[] a = (Object[]) lstResult.get(0);
				if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
					country = a[0].toString();
				}
				if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
					field = a[1].toString();
				}
			}				
		}
		if (operationUid !=null)
		{
			strSql = "SELECT opCo, operationName FROM Operation WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			
			if (lstResult.size() > 0) {
				Object[] a = (Object[]) lstResult.get(0);
				if (a[0] != null && StringUtils.isNotBlank(a[0].toString())){
					opCo = a[0].toString();
				}
				if (a[1] != null && StringUtils.isNotBlank(a[1].toString())){
					operationName = a[1].toString();
				}
			}
		}
		//send short code and get country name in return
		String countryname = null;
		countryname = CommonUtil.getConfiguredInstance().getCountryFullName(country);
		child.getDynaAttr().put("country", countryname);
		child.getDynaAttr().put("field", field);
		child.getDynaAttr().put("opCo", opCo);
		child.getDynaAttr().put("operationName", operationName);
		
	}
	
	private class LessonTicketNumberComparator implements Comparator<String>{
		public int compare(String s1, String s2){
			try{
				String f1 = WellNameUtil.getPaddedStr(s1.toString());
				String f2 = WellNameUtil.getPaddedStr(s2.toString());
				
				if (f1 == null || f2 == null) return 0;
				//refine the ticket# sorting order, numeric first then alphanumeric
				if (StringUtils.isBlank(f1.toString()) || StringUtils.isBlank(f2.toString())) return 0;
				if (!StringUtils.isNumeric(f1.toString()) && StringUtils.isAlphanumeric(f1.toString()) && StringUtils.isNumeric(f2.toString())) return 1;
				if (!StringUtils.isNumeric(f2.toString()) && StringUtils.isAlphanumeric(f2.toString()) && StringUtils.isNumeric(f1.toString())) return -1;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}

	}
	//set if status = 'Open', not allow to delete or edit the data
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if(StringUtils.equals(action, "delete") || StringUtils.equals(action, "edit")){
			Object object = node.getData();
				
			if (object instanceof LessonTicket) {
				LessonTicket thisLessonTicket = (LessonTicket) object;
				if (!StringUtils.equalsIgnoreCase(thisLessonTicket.getLessonStatus(),"OPEN"))
				{
					return false;	
				}
			}
		}
		return true;
	}
}
