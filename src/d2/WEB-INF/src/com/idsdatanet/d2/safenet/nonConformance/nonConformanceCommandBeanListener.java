package com.idsdatanet.d2.safenet.nonConformance;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.NonconformanceEvent;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;


public class nonConformanceCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener ,InitializingBean{

	private MailEngine mailEngine = null;
	private String support_email = null;
	private String sendNewMatchingLessonTicketTemplate = null;
	private String sendUpdatedMatchingLessonTicketTemplate = null;
	private String sendNotificationToResponsiblePartyEmailTemplate = null;
	private String lessonReportType = null;
	

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		Map<String,CommandBeanTreeNode> ll = commandBean.getRoot().getChild("LessonTicket");
		Manager dao = ApplicationUtils.getConfiguredInstance().getDaoManager();
		if(ll != null){	
			for(Map.Entry<String,CommandBeanTreeNode> i : ll.entrySet()){
				CommandBeanTreeNode llNode = i.getValue();
				LessonTicket llRec = (LessonTicket) llNode.getData();
				
				String sql = "select ltce.lessonCategory, ltce.lessonElement, " + ""
						+ "(select lc.lessonTicketCategory from LookupLessonTicketCategory lc where (lc.isDeleted = false or lc.isDeleted is null) and lc.lookupLessonTicketCategoryUid = ltce.lessonCategory) as lessonCategoryLabel, " 
						+ "(select le.lessonTicketElements from LookupLessonTicketElements le where (le.isDeleted = false or le.isDeleted is null) and le.lookupLessonTicketElementsUid = ltce.lessonElement) as lessonElementLabel "
						+ "from LessonTicketCategoryElement ltce where (ltce.isDeleted is null or ltce.isDeleted = false) "
						+ "and ltce.lessonTicketUid = :lessonTicketUid";
				List<Object[]> ltces = dao.findByNamedParam(sql, "lessonTicketUid", llRec.getLessonTicketUid());
				if(ltces != null){
					String contatenatedCategoryElements = null;
					for(Object[] ltce : ltces){
						Object lessonCategoryLabel = ltce[2];
						Object lessonElementLabel = ltce[3];
						if(lessonCategoryLabel != null && lessonElementLabel != null){
							if(contatenatedCategoryElements == null){
								contatenatedCategoryElements = lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}else{
								contatenatedCategoryElements = contatenatedCategoryElements  + "\n" + lessonCategoryLabel.toString() + " : " + lessonElementLabel.toString();
							}
						}
					}
					llRec.setCategoryElementLabels(contatenatedCategoryElements);
					dao.saveObject(llRec);
				}
			}
		}
	}

	public String getSendNotificationToResponsiblePartyEmailTemplate() {
		return sendNotificationToResponsiblePartyEmailTemplate;
	}

	public void setSendNotificationToResponsiblePartyEmailTemplate(
			String sendNotificationToResponsiblePartyEmailTemplate) {
		this.sendNotificationToResponsiblePartyEmailTemplate = sendNotificationToResponsiblePartyEmailTemplate;
	}

	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
		
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(NonconformanceEvent.class)) {
				NonconformanceEvent nonconformanceEvent = (NonconformanceEvent)targetCommandBeanTreeNode.getData();
				if("sendEmailNotification".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					this.sendEmailNotification(nonconformanceEvent.getNonconformanceEventUid(), (BaseCommandBean)commandBean, request);
				}
			}
		}
		
		if ("1".equals(request.getParameter("generateReport"))) {
			//String defaultReportType = null;
			
			//DefaultReportModule defaultReportModule =new DefaultReportModule();

			Object obj = targetCommandBeanTreeNode.getData();
			
			if (obj instanceof NonconformanceEvent) {
				NonconformanceEvent col =(NonconformanceEvent) obj;
				targetCommandBeanTreeNode.getDynaAttr().put("downloadUrl",col.getNonconformanceEventUid());

			commandBean.getRoot().getDynaAttr().put("downloadUrl",col.getNonconformanceEventUid());

			}
		}
	}
	
	public void afterPropertiesSet() throws Exception
	{
		if (this.mailEngine==null) this.mailEngine = ApplicationUtils.getConfiguredInstance().getMailEngine();
		this.support_email = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isBlank(support_email)) this.support_email = "support@idsdatanet.com";
	}
	
	
	/**
	 * Send Email Notification to Responsible Party upon clicking "Send" button
	 * @param lessonTicketUid
	 * @param commandBean
	 * @param request 
	 * @throws Exception
	 */
	private void sendEmailNotification(String nonconformanceEventUid, BaseCommandBean commandBean, HttpServletRequest request) throws Exception
	{
		
		NonconformanceEvent thisNCR = (NonconformanceEvent) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(NonconformanceEvent.class, nonconformanceEventUid);
		String[] emailList=null;
		String hyperLink = null;
		String content = null;
		
		if(!StringUtils.isBlank(thisNCR.getEmailReceipient())){

			String input = thisNCR.getEmailReceipient();
			if (!StringUtils.isBlank(input)){
				emailList=input.split(",");
			}
			
			String completeNCR = thisNCR.getEventNumberPrefix() + " - " + thisNCR.getEventNumber();
			String emailSubject = "System Message - Non Conformance (NCR #: NCR-" + completeNCR + ")";
			String url = request.getParameter("baseUrl");
			hyperLink = url + "?gotowellop=" + thisNCR.getOperationUid() + "&amp;gotoFieldName=nonconformanceEventUid&amp;gotoFieldUid=" + thisNCR.getNonconformanceEventUid();
		
			//call vars in email template
			Map<String, Object> ncr = new HashMap<String, Object>();
			ncr.put("eventNumber", completeNCR);
			ncr.put("hyperLink", hyperLink);
			
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("ncr", ncr);
			
			if(StringUtils.isBlank(this.sendNotificationToResponsiblePartyEmailTemplate)) {
				commandBean.getSystemMessage().addError("Unable to send");
			} else {
				mailEngine.sendMail(new String[]{thisNCR.getEmailReceipient()}, emailList, this.support_email, null, emailSubject , mailEngine.generateContentFromTemplate(this.sendNotificationToResponsiblePartyEmailTemplate, params));
				commandBean.getSystemMessage().addInfo("NCR " + completeNCR + " has been successfully sent");
			}
		}
	}
	
	/**
	 * Get email list with distribution key
	 * @param groupUid
	 * @param distributionKey
	 * @return
	 * @throws Exception
	 */
	private String[] getEmailList(String groupUid, String distributionKey) throws Exception {
		
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(groupUid, distributionKey);
		return emailList;
	}
	

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		
		return null;
	}
}
