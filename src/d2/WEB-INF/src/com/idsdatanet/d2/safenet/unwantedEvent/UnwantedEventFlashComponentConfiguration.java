package com.idsdatanet.d2.safenet.unwantedEvent;

import java.text.DecimalFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class UnwantedEventFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = "../../";
		Locale locale = userSession.getUserLocale();
		DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getInstance(locale);
		String decimalSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getDecimalSeparator());
		String thousandSeperator = String.valueOf(numberFormat.getDecimalFormatSymbols().getGroupingSeparator());

		return "d2Url=" + urlEncode(serviceUrl)
			+ "&decimalSeperator=" + urlEncode(decimalSeperator)
			+ "&thousandSeperator=" + urlEncode(thousandSeperator);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "unwantedevent";
	}

	@Override
	public String getAdditionalHtmlHeaderContent(String targetSubModule, UserSession userSession, HttpServletRequest request){
		return "<script language=\"JavaScript\" type=\"text/javascript\">" +
		"function reloadParentHtmlPage(clearQueryString){parent.d2ScreenManager.reloadParentHtmlPage(clearQueryString);}" +
		"function setBlockUI(block){" +
		"  if(block){"+
		"    parent.d2ScreenManager.toggleMainBlockUI();" +
		"  }else{" +
		"    parent.d2ScreenManager.toggleMainUnblockUI();" +
		"  }" +
		"}" +
		"</script>";
	}

}
