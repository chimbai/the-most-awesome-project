package com.idsdatanet.d2.safenet.processSafetyQuestionnaire;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class LeadingLaggingCummulativeDataGenerator implements ReportDataGenerator {

    public void disposeOnDataGeneratorExit() {
        // TODO Auto-generated method stub
    }

    @SuppressWarnings("unchecked")
    public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
        String thisRigInformationUid = "";
        String dailyUidList = "";
        if (userContext.getUserSelection().getRigInformationUid() != null) thisRigInformationUid = userContext.getUserSelection().getRigInformationUid().toString();
        // DISCONTINUE IF NO RIG FOR CURRENT OPERATION
        if (StringUtils.isBlank(thisRigInformationUid)) return;

        // GET ALL operationUid WITH THE SAME rigInformationUid
        String strSql = "SELECT DISTINCT dailyUid FROM ReportDaily WHERE rigInformationUid = :thisRigInformationUid AND (isDeleted = false or isDeleted is null)";
        String[] paramsFields = {"thisRigInformationUid"};
        Object[] paramsValues = {thisRigInformationUid};

        List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
        if (lstResult.size() > 0) {

            for (Object objResult: lstResult) {
                Object thisOperationUid = (Object) objResult;
                if (thisOperationUid != null) dailyUidList = dailyUidList + "'" + thisOperationUid.toString() + "',";
            }
            if (StringUtils.isNotBlank(dailyUidList)) dailyUidList = StringUtils.substring(dailyUidList, 0, -1);
        }
        //END OF GET ALL operationUid WITH THE SAME rigInformationUid

        //DISCONTINUE WHEN NO OPERATION FOR THIS RIG
        if (StringUtils.isBlank(dailyUidList)) return;

        String thisOperationUid = "";
        if (userContext.getUserSelection().getOperationUid() != null) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();

        //DISCONTINUE IF NO OPERATION IS SELECTED    
        if (StringUtils.isBlank(thisOperationUid)) return;

        //DISCONTINUE IF NO DAY IS SELECTED
        Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
        if (daily == null) return;

        //ASSIGN CURRENT SELECTED DATE
        Date currrentDate = daily.getDayDate();

        //ASSIGN CURRENT SELECTED WELL
        String wellUid = userContext.getUserSelection().getWellUid().toString();

        //BREAKDOWN THE DATE TO YEAR AND MONTH
        Integer intYear = Integer.parseInt(StringUtils.substring(currrentDate.toString(), 0, 4));
        Integer intMonth = Integer.parseInt(StringUtils.substring(currrentDate.toString(), 5, 7));
        Integer intDay = Integer.parseInt(StringUtils.substring(currrentDate.toString(), 8, 10));

        ReportDataNode psqrSummaryDataNode = reportDataNode.addChild("LeadingLaggingIndicatorSummary");
        psqrSummaryDataNode.addProperty("operationUid", thisOperationUid);

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql1 = "SELECT COUNT(psq.hasLossPrimarySecondaryBarriers) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasLossPrimarySecondaryBarriers = true) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields1 = {"currrentDate","wellUid"};
        Object[] paramsValues1 = {currrentDate,wellUid};

        List<Object> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
        if (lstResult1.size() > 0) {
            Object hasLossPrimarySecondaryBarriers = (Object) lstResult1.get(0);
            if (hasLossPrimarySecondaryBarriers != null) {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByWell", hasLossPrimarySecondaryBarriers.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql1A = "SELECT COUNT(psq.hasLossPrimarySecondaryBarriers) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossPrimarySecondaryBarriers = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields1A = {"Year", "Month", "Day"};
        Object[] paramsValues1A = {intYear, intMonth, intDay};
        List<Object> lstResult1A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1A, paramsFields1A, paramsValues1A);
        if (lstResult1A.size() > 0) {
            Object hasLossPrimarySecondaryBarriers = (Object) lstResult1A.get(0);                                            
            if (hasLossPrimarySecondaryBarriers != null) {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByMonth", hasLossPrimarySecondaryBarriers.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql1B = "SELECT COUNT(psq.hasLossPrimarySecondaryBarriers) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossPrimarySecondaryBarriers = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields1B = {"Year", "currrentDate"};
        Object[] paramsValues1B = {intYear, currrentDate};

        List<Object> lstResult1B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1B, paramsFields1B, paramsValues1B);
        if (lstResult1B.size() > 0) {
            Object hasLossPrimarySecondaryBarriers = (Object) lstResult1B.get(0);
            if (hasLossPrimarySecondaryBarriers != null) {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByYear", hasLossPrimarySecondaryBarriers.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossPrimarySecondaryBarriersByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql2 = "SELECT COUNT(psq.hasLossWellControl) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasLossWellControl = true) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields2 = {"currrentDate","wellUid"};
        Object[] paramsValues2 = {currrentDate,wellUid};

        List<Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
        if (lstResult2.size() > 0) {
            Object hasLossWellControl = (Object) lstResult2.get(0);
            if (hasLossWellControl != null) {
                psqrSummaryDataNode.addProperty("hasLossWellControlByWell", hasLossWellControl.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossWellControlByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql2A = "SELECT COUNT(psq.hasLossWellControl) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossWellControl = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields2A = {"Year", "Month", "Day"};
        Object[] paramsValues2A = {intYear, intMonth, intDay};
        List<Object> lstResult2A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2A, paramsFields2A, paramsValues2A);
        if (lstResult2A.size() > 0) {
            Object hasLossWellControl = (Object) lstResult2A.get(0);                                            
            if (hasLossWellControl != null) {
                psqrSummaryDataNode.addProperty("hasLossWellControlByMonth", hasLossWellControl.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossWellControlByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql2B = "SELECT COUNT(psq.hasLossWellControl) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossWellControl = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields2B = {"Year", "currrentDate"};
        Object[] paramsValues2B = {intYear, currrentDate};

        List<Object> lstResult2B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2B, paramsFields2B, paramsValues2B);
        if (lstResult2B.size() > 0) {
            Object hasLossWellControl = (Object) lstResult2B.get(0);
            if (hasLossWellControl != null) {
                psqrSummaryDataNode.addProperty("hasLossWellControlByYear", hasLossWellControl.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossWellControlByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql3 = "SELECT COUNT(psq.hasKick) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasKick = true) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields3 = {"currrentDate","wellUid"};
        Object[] paramsValues3 = {currrentDate,wellUid};

        List<Object> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
        if (lstResult3.size() > 0) {
            Object hasKick = (Object) lstResult3.get(0);
            if (hasKick != null) {
                psqrSummaryDataNode.addProperty("hasKickByWell", hasKick.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasKickByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql3A = "SELECT COUNT(psq.hasKick) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasKick = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields3A = {"Year", "Month", "Day"};
        Object[] paramsValues3A = {intYear, intMonth, intDay};
        List<Object> lstResult3A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3A, paramsFields3A, paramsValues3A);
        if (lstResult3A.size() > 0) {
            Object hasKick = (Object) lstResult3A.get(0);                                            
            if (hasKick != null) {
                psqrSummaryDataNode.addProperty("hasKickByMonth", hasKick.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasKickByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql3B = "SELECT COUNT(psq.hasKick) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasKick = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields3B = {"Year", "currrentDate"};
        Object[] paramsValues3B = {intYear, currrentDate};

        List<Object> lstResult3B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3B, paramsFields3B, paramsValues3B);
        if (lstResult3B.size() > 0) {
            Object hasKick = (Object) lstResult3B.get(0);
            if (hasKick != null) {
                psqrSummaryDataNode.addProperty("hasKickByYear", hasKick.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasKickByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql4 = "SELECT COUNT(psq.hasLossBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasLossBarrier = true) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields4 = {"currrentDate","wellUid"};
        Object[] paramsValues4 = {currrentDate, wellUid};

        List<Object> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4);
        if (lstResult4.size() > 0) {
            Object hasLossBarrier = (Object) lstResult4.get(0);
            if (hasLossBarrier != null) {
                psqrSummaryDataNode.addProperty("hasLossBarrierByWell", hasLossBarrier.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossBarrierByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql4A = "SELECT COUNT(psq.hasLossBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossBarrier = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields4A = {"Year", "Month", "Day"};
        Object[] paramsValues4A = {intYear, intMonth, intDay};
        List<Object> lstResult4A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4A, paramsFields4A, paramsValues4A);
        if (lstResult4A.size() > 0) {
            Object hasLossBarrier = (Object) lstResult4A.get(0);                                            
            if (hasLossBarrier != null) {
                psqrSummaryDataNode.addProperty("hasLossBarrierByMonth", hasLossBarrier.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossBarrierByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql4B = "SELECT COUNT(psq.hasLossBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossBarrier = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields4B = {"Year", "currrentDate"};
        Object[] paramsValues4B = {intYear, currrentDate};

        List<Object> lstResult4B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4B, paramsFields4B, paramsValues4B);
        if (lstResult4B.size() > 0) {
            Object hasLossBarrier = (Object) lstResult4B.get(0);
            if (hasLossBarrier != null) {
                psqrSummaryDataNode.addProperty("hasLossBarrierByYear", hasLossBarrier.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossBarrierByYear", "0");
            }
        }

        /* Remove matric as per requested
         * 
         * //GET WELL TO DATE INDICATOR NUMBER
        String strSql5 = "SELECT COUNT(psq.hasLossPrimaryBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasLossPrimaryBarrier = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields5 = {"currrentDate","wellUid"};
        Object[] paramsValues5 = {currrentDate,wellUid};

        List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5);
        if (lstResult5.size() > 0){        
            Object hasLossPrimaryBarrier = (Object) lstResult5.get(0);
            if(hasLossPrimaryBarrier != null)
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByWell", hasLossPrimaryBarrier.toString());
            }
            else
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql5A = "SELECT COUNT(psq.hasLossPrimaryBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossPrimaryBarrier = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields5A = {"Year", "Month", "Day"};
        Object[] paramsValues5A = {intYear, intMonth, intDay};
        List lstResult5A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5A, paramsFields5A, paramsValues5A);
        if (lstResult5A.size() > 0){
            Object hasLossPrimaryBarrier = (Object) lstResult5A.get(0);                                            
            if(hasLossPrimaryBarrier != null)
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByMonth", hasLossPrimaryBarrier.toString());
            }
            else
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql5B = "SELECT COUNT(psq.hasLossPrimaryBarrier) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossPrimaryBarrier = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields5B = {"Year", "currrentDate"};
        Object[] paramsValues5B = {intYear, currrentDate};

        List lstResult5B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5B, paramsFields5B, paramsValues5B);
        if (lstResult5B.size() > 0){
            Object hasLossPrimaryBarrier = (Object) lstResult5B.get(0);
            if(hasLossPrimaryBarrier != null)
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByYear", hasLossPrimaryBarrier.toString());
            }
            else
            {
                psqrSummaryDataNode.addProperty("hasLossPrimaryBarrierByYear", "0");
            }
        }

         */

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql6 = "SELECT COUNT(psq.hasLossMonitoringCapability) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasLossMonitoringCapability = true) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields6 = {"currrentDate","wellUid"};
        Object[] paramsValues6 = {currrentDate,wellUid};

        List<Object> lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6);
        if (lstResult6.size() > 0) {
            Object hasLossMonitoringCapability = (Object) lstResult6.get(0);
            if (hasLossMonitoringCapability != null) {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByWell", hasLossMonitoringCapability.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql6A = "SELECT COUNT(psq.hasLossMonitoringCapability) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossMonitoringCapability = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields6A = {"Year", "Month", "Day"};
        Object[] paramsValues6A = {intYear, intMonth, intDay};
        List<Object> lstResult6A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6A, paramsFields6A, paramsValues6A);
        if (lstResult6A.size() > 0) {
            Object hasLossMonitoringCapability = (Object) lstResult6A.get(0);                                            
            if (hasLossMonitoringCapability != null) {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByMonth", hasLossMonitoringCapability.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql6B = "SELECT COUNT(psq.hasLossMonitoringCapability) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasLossMonitoringCapability = true) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields6B = {"Year", "currrentDate"};
        Object[] paramsValues6B = {intYear, currrentDate};

        List<Object> lstResult6B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6B, paramsFields6B, paramsValues6B);
        if (lstResult6B.size() > 0) {
            Object hasLossMonitoringCapability = (Object) lstResult6B.get(0);
            if (hasLossMonitoringCapability != null) {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByYear", hasLossMonitoringCapability.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasLossMonitoringCapabilityByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql7 = "SELECT SUM(psq.phaseBarrierDiagram24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields7 = {"currrentDate","wellUid"};
        Object[] paramsValues7 = {currrentDate,wellUid};

        List<Object> lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramsFields7, paramsValues7);
        if (lstResult7.size() > 0) {
            Object phaseBarrierDiagram24hrAmount = (Object) lstResult7.get(0);
            if (phaseBarrierDiagram24hrAmount != null) {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByWell", phaseBarrierDiagram24hrAmount.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql7A = "SELECT SUM(psq.phaseBarrierDiagram24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields7A = {"Year", "Month", "Day"};
        Object[] paramsValues7A = {intYear, intMonth, intDay};
        List<Object> lstResult7A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7A, paramsFields7A, paramsValues7A);
        if (lstResult7A.size() > 0) {
            Object phaseBarrierDiagram24hrAmount = (Object) lstResult7A.get(0);                                            
            if (phaseBarrierDiagram24hrAmount != null) {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByMonth", phaseBarrierDiagram24hrAmount.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql7B = "SELECT SUM(psq.phaseBarrierDiagram24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields7B = {"Year", "currrentDate"};
        Object[] paramsValues7B = {intYear, currrentDate};

        List<Object> lstResult7B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7B, paramsFields7B, paramsValues7B);
        if (lstResult7B.size() > 0) {
            Object phaseBarrierDiagram24hrAmount = (Object) lstResult7B.get(0);
            if (phaseBarrierDiagram24hrAmount != null) {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByYear", phaseBarrierDiagram24hrAmount.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPhaseBarrierDiagram24hrByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql8 = "SELECT COUNT(psq.hasWellControlEquipmentFailed) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasWellControlEquipmentFailed = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields8 = {"currrentDate","wellUid"};
        Object[] paramsValues8 = {currrentDate,wellUid};

        List<Object> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, paramsFields8, paramsValues8);
        if (lstResult8.size() > 0) {
            Object hasWellControlEquipmentFailed = (Object) lstResult8.get(0);
            if (hasWellControlEquipmentFailed != null) {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByWell", hasWellControlEquipmentFailed.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql8A = "SELECT COUNT(psq.hasWellControlEquipmentFailed) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasWellControlEquipmentFailed = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields8A = {"Year", "Month", "Day"};
        Object[] paramsValues8A = {intYear, intMonth, intDay};
        List<Object> lstResult8A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8A, paramsFields8A, paramsValues8A);
        if (lstResult8A.size() > 0) {
            Object hasWellControlEquipmentFailed = (Object) lstResult8A.get(0);                                            
            if (hasWellControlEquipmentFailed != null) {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByMonth", hasWellControlEquipmentFailed.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql8B = "SELECT COUNT(psq.hasWellControlEquipmentFailed) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasWellControlEquipmentFailed = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields8B = {"Year", "currrentDate"};
        Object[] paramsValues8B = {intYear, currrentDate};

        List<Object> lstResult8B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8B, paramsFields8B, paramsValues8B);
        if (lstResult8B.size() > 0) {
            Object hasWellControlEquipmentFailed = (Object) lstResult8B.get(0);
            if (hasWellControlEquipmentFailed != null) {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByYear", hasWellControlEquipmentFailed.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasWellControlEquipmentFailedByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql9 = "SELECT COUNT(psq.hasPassCementEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasPassCementEquipmentTest = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields9 = {"currrentDate","wellUid"};
        Object[] paramsValues9 = {currrentDate,wellUid};

        List<Object> lstResult9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9, paramsFields9, paramsValues9);
        if (lstResult9.size() > 0) {
            Object hasPassCementEquipmentTest = (Object) lstResult9.get(0);
            if (hasPassCementEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByWell", hasPassCementEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql9A = "SELECT COUNT(psq.hasPassCementEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassCementEquipmentTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields9A = {"Year", "Month", "Day"};
        Object[] paramsValues9A = {intYear, intMonth, intDay};
        List<Object> lstResult9A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9A, paramsFields9A, paramsValues9A);
        if (lstResult9A.size() > 0) {
            Object hasPassCementEquipmentTest = (Object) lstResult9A.get(0);                                            
            if (hasPassCementEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByMonth", hasPassCementEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql9B = "SELECT COUNT(psq.hasPassCementEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassCementEquipmentTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields9B = {"Year", "currrentDate"};
        Object[] paramsValues9B = {intYear, currrentDate};

        List<Object> lstResult9B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9B, paramsFields9B, paramsValues9B);
        if (lstResult9B.size() > 0) {
            Object hasPassCementEquipmentTest = (Object) lstResult9B.get(0);
            if (hasPassCementEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByYear", hasPassCementEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementEquipmentTestByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql10 = "SELECT COUNT(psq.hasPassCementOperation) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasPassCementOperation = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields10 = {"currrentDate","wellUid"};
        Object[] paramsValues10 = {currrentDate,wellUid};

        List<Object> lstResult10 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10, paramsFields10, paramsValues10);
        if (lstResult10.size() > 0) {
            Object hasPassCementOperation = (Object) lstResult10.get(0);
            if (hasPassCementOperation != null) {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByWell", hasPassCementOperation.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql10A = "SELECT COUNT(psq.hasPassCementOperation) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassCementOperation = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields10A = {"Year", "Month", "Day"};
        Object[] paramsValues10A = {intYear, intMonth, intDay};
        List<Object> lstResult10A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10A, paramsFields10A, paramsValues10A);
        if (lstResult10A.size() > 0) {
            Object hasPassCementOperation = (Object) lstResult10A.get(0);                                            
            if (hasPassCementOperation != null) {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByMonth", hasPassCementOperation.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql10B = "SELECT COUNT(psq.hasPassCementOperation) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassCementOperation = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields10B = {"Year", "currrentDate"};
        Object[] paramsValues10B = {intYear, currrentDate};

        List<Object> lstResult10B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10B, paramsFields10B, paramsValues10B);
        if (lstResult10B.size() > 0) {
            Object hasPassCementOperation = (Object) lstResult10B.get(0);
            if (hasPassCementOperation != null) {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByYear", hasPassCementOperation.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassCementOperationByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql11 = "SELECT COUNT(psq.hasPassDrillingCompletionStandards) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasPassDrillingCompletionStandards = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields11 = {"currrentDate","wellUid"};
        Object[] paramsValues11 = {currrentDate,wellUid};

        List<Object> lstResult11 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11, paramsFields11, paramsValues11);
        if (lstResult11.size() > 0) {
            Object hasPassDrillingCompletionStandards = (Object) lstResult11.get(0);
            if (hasPassDrillingCompletionStandards != null) {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByWell", hasPassDrillingCompletionStandards.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql11A = "SELECT COUNT(psq.hasPassDrillingCompletionStandards) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassDrillingCompletionStandards = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields11A = {"Year", "Month", "Day"};
        Object[] paramsValues11A = {intYear, intMonth, intDay};
        List<Object> lstResult11A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11A, paramsFields11A, paramsValues11A);
        if (lstResult11A.size() > 0) {
            Object hasPassDrillingCompletionStandards = (Object) lstResult11A.get(0);                                            
            if (hasPassDrillingCompletionStandards != null) {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByMonth", hasPassDrillingCompletionStandards.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql11B = "SELECT COUNT(psq.hasPassDrillingCompletionStandards) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassDrillingCompletionStandards = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields11B = {"Year", "currrentDate"};
        Object[] paramsValues11B = {intYear, currrentDate};

        List<Object> lstResult11B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11B, paramsFields11B, paramsValues11B);
        if (lstResult11B.size() > 0) {
            Object hasPassDrillingCompletionStandards = (Object) lstResult11B.get(0);
            if (hasPassDrillingCompletionStandards != null) {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByYear", hasPassDrillingCompletionStandards.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassDrillingCompletionStandardsByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql12 = "SELECT COUNT(psq.hasChangeControl24hr) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasChangeControl24hr = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields12 = {"currrentDate","wellUid"};
        Object[] paramsValues12 = {currrentDate,wellUid};

        List<Object> lstResult12 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql12, paramsFields12, paramsValues12);
        if (lstResult12.size() > 0) {
            Object hasChangeControl24hr = (Object) lstResult12.get(0);
            if (hasChangeControl24hr != null) {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByWell", hasChangeControl24hr.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql12A = "SELECT COUNT(psq.hasChangeControl24hr) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasChangeControl24hr = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields12A = {"Year", "Month", "Day"};
        Object[] paramsValues12A = {intYear, intMonth, intDay};
        List<Object> lstResult12A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql12A, paramsFields12A, paramsValues12A);
        if (lstResult12A.size() > 0) {
            Object hasChangeControl24hr = (Object) lstResult12A.get(0);                                            
            if (hasChangeControl24hr != null) {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByMonth", hasChangeControl24hr.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql12B = "SELECT COUNT(psq.hasChangeControl24hr) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasChangeControl24hr = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields12B = {"Year", "currrentDate"};
        Object[] paramsValues12B = {intYear, currrentDate};

        List<Object> lstResult12B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql12B, paramsFields12B, paramsValues12B);
        if (lstResult12B.size() > 0) {
            Object hasChangeControl24hr = (Object) lstResult12B.get(0);
            if (hasChangeControl24hr != null) {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByYear", hasChangeControl24hr.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControl24hrByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR SUM
        String cc24SumSql = "SELECT SUM(psq.changeControl24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.changeControl24hrAmount IS NOT NULL AND psq.changeControl24hrAmount > 0) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        List<Object> lstResultCC24Sum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(cc24SumSql, new String[] {"currrentDate","wellUid"}, new Object[] {currrentDate,wellUid});
        if (lstResultCC24Sum.size() > 0) {
            Object changeControl24hrAmountSum = (Object) lstResultCC24Sum.get(0);
            if (changeControl24hrAmountSum != null) {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByWell", changeControl24hrAmountSum.toString());
            } else {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        cc24SumSql = "SELECT SUM(psq.changeControl24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.changeControl24hrAmount IS NOT NULL AND psq.changeControl24hrAmount > 0) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        lstResultCC24Sum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(cc24SumSql, new String[] {"Year", "Month", "Day"}, new Object[] {intYear, intMonth, intDay});
        if (lstResultCC24Sum.size() > 0) {
            Object changeControl24hrAmountSum = (Object) lstResultCC24Sum.get(0);                                            
            if (changeControl24hrAmountSum != null) {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByMonth", new String(changeControl24hrAmountSum.toString()));
            } else {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        cc24SumSql = "SELECT SUM(psq.changeControl24hrAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.changeControl24hrAmount IS NOT NULL AND psq.changeControl24hrAmount > 0) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";

        lstResultCC24Sum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(cc24SumSql, new String[] {"Year", "currrentDate"}, new Object[] {intYear, currrentDate});
        if (lstResultCC24Sum.size() > 0) {
            Object changeControl24hrAmountSum = (Object) lstResultCC24Sum.get(0);
            if (changeControl24hrAmountSum != null) {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByYear", new String(changeControl24hrAmountSum.toString()));
            } else {
                psqrSummaryDataNode.addProperty("changeControl24hrAmountByYear", "0");
            }
        }



        //GET WELL TO DATE INDICATOR SUM
        String ccAASumSql = "SELECT SUM(psq.changeControlApprovedAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.changeControlApprovedAmount IS NOT NULL AND psq.changeControlApprovedAmount > 0) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        List<Object> lstResultCCAASum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ccAASumSql, new String[] {"currrentDate","wellUid"}, new Object[] {currrentDate,wellUid});
        if (lstResultCCAASum.size() > 0) {
            Object changeControlApprovedAmount = (Object) lstResultCCAASum.get(0);
            if (changeControlApprovedAmount != null) {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByWell", new String(changeControlApprovedAmount.toString()));
            } else {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        ccAASumSql = "SELECT SUM(psq.changeControlApprovedAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.changeControlApprovedAmount IS NOT NULL AND psq.changeControlApprovedAmount > 0) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        lstResultCCAASum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ccAASumSql, new String[] {"Year", "Month", "Day"}, new Object[] {intYear, intMonth, intDay});
        if (lstResultCCAASum.size() > 0) {
            Object changeControlApprovedAmount = (Object) lstResultCCAASum.get(0);                                            
            if (changeControlApprovedAmount != null) {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByMonth", new String(changeControlApprovedAmount.toString()));
            } else {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        ccAASumSql = "SELECT SUM(psq.changeControlApprovedAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.changeControlApprovedAmount IS NOT NULL AND psq.changeControlApprovedAmount > 0) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";

        lstResultCCAASum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ccAASumSql, new String[] {"Year", "currrentDate"}, new Object[] {intYear, currrentDate});
        if (lstResultCCAASum.size() > 0) {
            Object changeControlApprovedAmount = (Object) lstResultCCAASum.get(0);
            if (changeControlApprovedAmount != null) {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByYear", new String(changeControlApprovedAmount.toString()));
            } else {
                psqrSummaryDataNode.addProperty("changeControlApprovedAmountByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql13 = "SELECT COUNT(psq.isChangeControlApproved) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.isChangeControlApproved = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields13 = {"currrentDate","wellUid"};
        Object[] paramsValues13 = {currrentDate,wellUid};

        List<Object> lstResult13 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql13, paramsFields13, paramsValues13);
        if (lstResult13.size() > 0) {
            Object isChangeControlApproved = (Object) lstResult13.get(0);
            if (isChangeControlApproved != null) {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByWell", isChangeControlApproved.toString());
            } else {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql13A = "SELECT COUNT(psq.isChangeControlApproved) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.isChangeControlApproved = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields13A = {"Year", "Month", "Day"};
        Object[] paramsValues13A = {intYear, intMonth, intDay};
        List<Object> lstResult13A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql13A, paramsFields13A, paramsValues13A);
        if (lstResult13A.size() > 0) {
            Object isChangeControlApproved = (Object) lstResult13A.get(0);                                            
            if (isChangeControlApproved != null) {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByMonth", isChangeControlApproved.toString());
            } else {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql13B = "SELECT COUNT(psq.isChangeControlApproved) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.isChangeControlApproved = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields13B = {"Year", "currrentDate"};
        Object[] paramsValues13B = {intYear, currrentDate};

        List<Object> lstResult13B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql13B, paramsFields13B, paramsValues13B);
        if (lstResult13B.size() > 0) {
            Object isChangeControlApproved = (Object) lstResult13B.get(0);
            if (isChangeControlApproved != null) {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByYear", isChangeControlApproved.toString());
            } else {
                psqrSummaryDataNode.addProperty("isChangeControlApprovedByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql14 = "SELECT COUNT(psq.isBelowProgramMudWeight) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.isBelowProgramMudWeight = '1') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields14 = {"currrentDate","wellUid"};
        Object[] paramsValues14 = {currrentDate,wellUid};

        List<Object> lstResult14 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql14, paramsFields14, paramsValues14);
        if (lstResult14.size() > 0) {
            Object isBelowProgramMudWeight = (Object) lstResult14.get(0);
            if (isBelowProgramMudWeight != null) {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByWell", isBelowProgramMudWeight.toString());
            } else {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql14A = "SELECT COUNT(psq.isBelowProgramMudWeight) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.isBelowProgramMudWeight = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields14A = {"Year", "Month", "Day"};
        Object[] paramsValues14A = {intYear, intMonth, intDay};
        List<Object> lstResult14A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql14A, paramsFields14A, paramsValues14A);
        if (lstResult14A.size() > 0) {
            Object isBelowProgramMudWeight = (Object) lstResult14A.get(0);                                            
            if (isBelowProgramMudWeight != null) {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByMonth", isBelowProgramMudWeight.toString());
            } else {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql14B = "SELECT COUNT(psq.isBelowProgramMudWeight) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.isBelowProgramMudWeight = '1') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields14B = {"Year", "currrentDate"};
        Object[] paramsValues14B = {intYear, currrentDate};

        List<Object> lstResult14B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql14B, paramsFields14B, paramsValues14B);
        if (lstResult14B.size() > 0) {
            Object isBelowProgramMudWeight = (Object) lstResult14B.get(0);
            if (isBelowProgramMudWeight != null) {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByYear", isBelowProgramMudWeight.toString());
            } else {
                psqrSummaryDataNode.addProperty("isBelowProgramMudWeightByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql15 = "SELECT COUNT(psq.hasMonitoredFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasMonitoredFluidColumn = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields15 = {"currrentDate","wellUid"};
        Object[] paramsValues15 = {currrentDate,wellUid};

        List<Object> lstResult15 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql15, paramsFields15, paramsValues15);
        if (lstResult15.size() > 0) {
            Object hasMonitoredFluidColumn = (Object) lstResult15.get(0);
            if (hasMonitoredFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByWell", hasMonitoredFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql15A = "SELECT COUNT(psq.hasMonitoredFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasMonitoredFluidColumn = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields15A = {"Year", "Month", "Day"};
        Object[] paramsValues15A = {intYear, intMonth, intDay};
        List<Object> lstResult15A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql15A, paramsFields15A, paramsValues15A);
        if (lstResult15A.size() > 0) {
            Object hasMonitoredFluidColumn = (Object) lstResult15A.get(0);                                            
            if (hasMonitoredFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByMonth", hasMonitoredFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql15B = "SELECT COUNT(psq.hasMonitoredFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasMonitoredFluidColumn = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields15B = {"Year", "currrentDate"};
        Object[] paramsValues15B = {intYear, currrentDate};

        List<Object> lstResult15B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql15B, paramsFields15B, paramsValues15B);
        if (lstResult15B.size() > 0) {
            Object hasMonitoredFluidColumn = (Object) lstResult15B.get(0);
            if (hasMonitoredFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByYear", hasMonitoredFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasMonitoredFluidColumnByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql16 = "SELECT COUNT(psq.hasChangeControlMonitorFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasChangeControlMonitorFluidColumn = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields16 = {"currrentDate","wellUid"};
        Object[] paramsValues16 = {currrentDate,wellUid};

        List<Object> lstResult16 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql16, paramsFields16, paramsValues16);
        if (lstResult16.size() > 0) {
            Object hasChangeControlMonitorFluidColumn = (Object) lstResult16.get(0);
            if (hasChangeControlMonitorFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByWell", hasChangeControlMonitorFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql16A = "SELECT COUNT(psq.hasChangeControlMonitorFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasChangeControlMonitorFluidColumn = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields16A = {"Year", "Month", "Day"};
        Object[] paramsValues16A = {intYear, intMonth, intDay};
        List<Object> lstResult16A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql16A, paramsFields16A, paramsValues16A);
        if (lstResult16A.size() > 0) {
            Object hasChangeControlMonitorFluidColumn = (Object) lstResult16A.get(0);                                            
            if (hasChangeControlMonitorFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByMonth", hasChangeControlMonitorFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql16B = "SELECT COUNT(psq.hasChangeControlMonitorFluidColumn) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasChangeControlMonitorFluidColumn = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields16B = {"Year", "currrentDate"};
        Object[] paramsValues16B = {intYear, currrentDate};

        List<Object> lstResult16B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql16B, paramsFields16B, paramsValues16B);
        if (lstResult16B.size() > 0) {
            Object hasChangeControlMonitorFluidColumn = (Object) lstResult16B.get(0);
            if (hasChangeControlMonitorFluidColumn != null) {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByYear", hasChangeControlMonitorFluidColumn.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasChangeControlMonitorFluidColumnByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql17 = "SELECT SUM(psq.connectionGasEventAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields17 = {"currrentDate","wellUid"};
        Object[] paramsValues17 = {currrentDate,wellUid};

        List<Object> lstResult17 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql17, paramsFields17, paramsValues17);
        if (lstResult17.size() > 0) {
            Object hasConnectionGasEvent = (Object) lstResult17.get(0);
            if (hasConnectionGasEvent != null) {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByWell", hasConnectionGasEvent.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql17A = "SELECT SUM(psq.connectionGasEventAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields17A = {"Year", "Month", "Day"};
        Object[] paramsValues17A = {intYear, intMonth, intDay};
        List<Object> lstResult17A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql17A, paramsFields17A, paramsValues17A);
        if (lstResult17A.size() > 0) {
            Object hasConnectionGasEvent = (Object) lstResult17A.get(0);                                            
            if (hasConnectionGasEvent != null) {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByMonth", hasConnectionGasEvent.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql17B = "SELECT SUM(psq.connectionGasEventAmount) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields17B = {"Year", "currrentDate"};
        Object[] paramsValues17B = {intYear, currrentDate};

        List<Object> lstResult17B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql17B, paramsFields17B, paramsValues17B);
        if (lstResult17B.size() > 0) {
            Object hasConnectionGasEvent = (Object) lstResult17B.get(0);
            if (hasConnectionGasEvent != null) {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByYear", hasConnectionGasEvent.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasConnectionGasEventByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql18 = "SELECT COUNT(psq.hasPassEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasPassEquipmentTest = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields18 = {"currrentDate","wellUid"};
        Object[] paramsValues18 = {currrentDate,wellUid};

        List<Object> lstResult18 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql18, paramsFields18, paramsValues18);
        if (lstResult18.size() > 0) {
            Object hasPassEquipmentTest = (Object) lstResult18.get(0);
            if (hasPassEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByWell", hasPassEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql18A = "SELECT COUNT(psq.hasPassEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassEquipmentTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields18A = {"Year", "Month", "Day"};
        Object[] paramsValues18A = {intYear, intMonth, intDay};
        List<Object> lstResult18A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql18A, paramsFields18A, paramsValues18A);
        if (lstResult18A.size() > 0) {
            Object hasPassEquipmentTest = (Object) lstResult18A.get(0);                                            
            if (hasPassEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByMonth", hasPassEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql18B = "SELECT COUNT(psq.hasPassEquipmentTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasPassEquipmentTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields18B = {"Year", "currrentDate"};
        Object[] paramsValues18B = {intYear, currrentDate};

        List<Object> lstResult18B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql18B, paramsFields18B, paramsValues18B);
        if (lstResult18B.size() > 0) {
            Object hasPassEquipmentTest = (Object) lstResult18B.get(0);
            if (hasPassEquipmentTest != null) {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByYear", hasPassEquipmentTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasPassEquipmentTestByYear", "0");
            }
        }

        //GET WELL TO DATE INDICATOR NUMBER
        String strSql19 = "SELECT COUNT(psq.hasTemporaryBarrierPassTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.operationUid IN (SELECT operationUid FROM Operation where wellUid = :wellUid AND (isDeleted = false or isDeleted is null)) " +
                "AND DATE(d.dayDate) <= :currrentDate AND (psq.hasTemporaryBarrierPassTest = '0') " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";

        String[] paramsFields19 = {"currrentDate","wellUid"};
        Object[] paramsValues19 = {currrentDate,wellUid};

        List<Object> lstResult19 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql19, paramsFields19, paramsValues19);
        if (lstResult19.size() > 0) {
            Object hasTemporaryBarrierPassTest = (Object) lstResult19.get(0);
            if (hasTemporaryBarrierPassTest != null) {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByWell", hasTemporaryBarrierPassTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByWell", "0");
            }
        }

        //GET MONTHLY INDICATOR NUMBER
        String strSql19A = "SELECT COUNT(psq.hasTemporaryBarrierPassTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND MONTH(d.dayDate) = :Month AND DAY(d.dayDate) <= :Day) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasTemporaryBarrierPassTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
        String[] paramsFields19A = {"Year", "Month", "Day"};
        Object[] paramsValues19A = {intYear, intMonth, intDay};
        List<Object> lstResult19A = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql19A, paramsFields19A, paramsValues19A);
        if (lstResult19A.size() > 0) {
            Object hasTemporaryBarrierPassTest = (Object) lstResult19A.get(0);                                            
            if (hasTemporaryBarrierPassTest != null) {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByMonth", hasTemporaryBarrierPassTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByMonth", "0");
            }
        }

        //GET YEARLY INDICATOR NUMBER
        String strSql19B = "SELECT COUNT(psq.hasTemporaryBarrierPassTest) FROM ProcessSafetyQuestionnaire psq, Daily d " +
                "WHERE psq.dailyUid IN (" + dailyUidList + ") " +
                "AND (YEAR(d.dayDate) = :Year AND DATE(d.dayDate) <= :currrentDate) " +
                "AND (psq.isDeleted = false or psq.isDeleted is null) AND (psq.hasTemporaryBarrierPassTest = '0') " +
                "AND psq.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) ";
        String[] paramsFields19B = {"Year", "currrentDate"};
        Object[] paramsValues19B = {intYear, currrentDate};

        List<Object> lstResult19B = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql19B, paramsFields19B, paramsValues19B);
        if (lstResult19B.size() > 0) {
            Object hasTemporaryBarrierPassTest = (Object) lstResult19B.get(0);
            if (hasTemporaryBarrierPassTest != null) {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByYear", hasTemporaryBarrierPassTest.toString());
            } else {
                psqrSummaryDataNode.addProperty("hasTemporaryBarrierPassTestByYear", "0");
            }
        }
    }
}