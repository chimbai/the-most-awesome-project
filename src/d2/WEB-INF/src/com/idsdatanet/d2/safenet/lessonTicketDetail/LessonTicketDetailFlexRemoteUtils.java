package com.idsdatanet.d2.safenet.lessonTicketDetail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.model.LessonTicketCategoryElement;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LessonTicketDetailFlexRemoteUtils extends BlazeRemoteClassSupport{

	public void getLessonTicketCategoryElementsList(HttpServletRequest request,
			HttpServletResponse response, String lessonTicketUid) {
		// TODO Auto-generated method stub
		
		SimpleXmlWriter writer;
		try {
			writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			String lessonCategorySql = "SELECT ltce.lessonCategory, a.lessonTicketCategory FROM LessonTicketCategoryElement ltce, LookupLessonTicketCategory a " +
					 "WHERE ltce.lessonCategory = a.lookupLessonTicketCategoryUid AND (ltce.isDeleted=false OR ltce.isDeleted is null) AND (a.isDeleted is null OR a.isDeleted=false) " +
					 "AND (a.isActive IS NULL or a.isActive = true)" +
					 "AND ltce.lessonTicketUid = :lessonTicketUid " +
					 "GROUP BY ltce.lessonCategory " +
					 "ORDER by a.sequence";
			List<Object[]> categoryLstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lessonCategorySql, new String[] {"lessonTicketUid"}, new Object[] {lessonTicketUid});
			if (categoryLstResult.size() > 0){
				for (Object[] catRec : categoryLstResult) {
					String lessonCategoryUid = catRec[0].toString();
					String category = catRec[1].toString();
					String elements = null;
					String lessonElementSql = "SELECT b.lookupLessonTicketElementsUid, b.lessonTicketElements FROM LessonTicketCategoryElement ltce, LookupLessonTicketElements b " +
							 "WHERE ltce.lessonElement = b.lookupLessonTicketElementsUid AND (ltce.isDeleted=false OR ltce.isDeleted is null) AND (b.isDeleted is null OR b.isDeleted=false) " +
							 "AND ltce.lessonCategory = :lessonCategoryUid AND ltce.lessonTicketUid = :lessonTicketUid " +
							 "ORDER by b.sequence";
					List<Object[]> elementLstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(lessonElementSql, new String[] {"lessonCategoryUid", "lessonTicketUid"}, new Object[] {lessonCategoryUid, lessonTicketUid});
					if (elementLstResult.size() > 0) {						
						for(Object[] eleRec : elementLstResult) {
							if(StringUtils.isBlank(elements)) {
								elements = eleRec[1].toString();
							} else {
								elements += ", " + eleRec[1].toString();
							}
						}
					}
					// Write the attributes into record
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("lessonCategory", category);
					attr.addAttribute("lessonElements", StringUtils.isNotBlank(elements) ? elements : "");
					writer.addElement("LessonCategoryElements", "", attr);
				}
			}
			writer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void getCategoryElementList(HttpServletRequest request,
			HttpServletResponse response, String lessonTicketUid) throws Exception{
		// TODO Auto-generated method stub
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		
		List<String> criteriaList = new ArrayList<String>();
		String ulsSql = "SELECT lessonElement FROM LessonTicketCategoryElement WHERE (isDeleted is null OR isDeleted=false) and lessonTicketUid=:lessonTicketUid";
		List elementListLstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(ulsSql, 
				new String[] {"lessonTicketUid"}, 
				new Object[] {lessonTicketUid});
		if (elementListLstResult.size() > 0){
			for (Object catRec : elementListLstResult) {
				if(catRec != null) criteriaList.add(catRec.toString());
			}
		}
		
		String sql = "SELECT a.lessonTicketCategory, a.lookupLessonTicketCategoryUid FROM LookupLessonTicketCategory a " +
					 "WHERE (a.isDeleted is null OR a.isDeleted=false) AND (isActive IS NULL or isActive = true)" +
					 "ORDER BY a.sequence";
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		
		writer.startElement("root");
		
		for (Object[] rec : lstResult) {
			
			SimpleAttributes att = new SimpleAttributes();
			att.addAttribute("name", rec[0].toString());
			att.addAttribute("uid", rec[1].toString());
			writer.startElement("category", att);
			
			String sql2 = "SELECT b.lessonTicketElements, b.lookupLessonTicketElementsUid FROM LookupLessonTicketElements b " +
						 "WHERE (b.isDeleted is null OR b.isDeleted=false) AND b.lookupLessonTicketCategoryUid ='"+ rec[1].toString() +"' " +
						 "ORDER BY b.sequence";
			List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql2);
			for (Object[] rec2 : lstResult2) {
				SimpleAttributes elementAtt = new SimpleAttributes();
				elementAtt.addAttribute("name", rec2[0].toString());
				elementAtt.addAttribute("uid", rec2[1].toString());
				if(criteriaList.contains(rec2[1].toString())) {
					elementAtt.addAttribute("selected", "1");
				}
				writer.addElement("element", "", elementAtt);
			}
			writer.endElement();
		}
		writer.endElement();
		writer.close();
	}

	public void saveSelectedCategoryElementList(HttpServletRequest request,
			HttpServletResponse response, String lessonTicketUid,
			String selectedStr) throws Exception {
		// TODO Auto-generated method stub
		
		UserSession session = UserSession.getInstance(request);
		
		List<LessonTicketCategoryElement> lstLessonTicketCategoryElement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LessonTicketCategoryElement WHERE (isDeleted = false or isDeleted is null) and lessonTicketUid=:lessonTicketUid ", "lessonTicketUid", lessonTicketUid);
		for(LessonTicketCategoryElement ltce: lstLessonTicketCategoryElement){
			ApplicationUtils.getConfiguredInstance().getDaoManager().delete(ltce);
		}
		if (StringUtils.isNotBlank(selectedStr)) {
			String[] lines = selectedStr.split("&");
			for(String line : lines)
			{
				String[] str = line.split("=");
				String categoryUid = str[0];
				String data = null;
				if (str.length>1)
					data = str[1];
				if (StringUtils.isNotBlank(data))
				{
					String[] rows = data.split(",");
					for(String row : rows)
					{
						LessonTicketCategoryElement lessonTicketCategoryElement = new LessonTicketCategoryElement();
						lessonTicketCategoryElement.setLessonTicketUid(lessonTicketUid);
						lessonTicketCategoryElement.setLessonCategory(categoryUid);
						lessonTicketCategoryElement.setLessonElement(row);
						lessonTicketCategoryElement.setLastEditDatetime(new Date());
						lessonTicketCategoryElement.setLastEditUserUid(session.getCurrentUser().getUserUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lessonTicketCategoryElement);
					}
				}
			}
		}
	}

}
