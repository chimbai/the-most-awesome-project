package com.idsdatanet.d2.safenet.environmentDischarges;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class EnvironmentDischargesDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("Discharges") || currentClass.equals("Emissions") || currentClass.equals("Wastes")) {
			customFrom = "EnvironmentDischarges";			
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		
		if(currentClass.equals("DischargesComment") || currentClass.equals("EmissionsComment") || currentClass.equals("WastesComment")) {
			customFrom = "EnvironmentDischargesComment";			
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		String category = "";
		
		if(currentClass.equals("Discharges")) category = "discharges";
		if(currentClass.equals("Emissions")) category = "emissions";
		if(currentClass.equals("Wastes")) category = "wastes";
		if(currentClass.equals("DischargesComment")) category = "discharges";
		if(currentClass.equals("EmissionsComment")) category = "emissions";
		if(currentClass.equals("WastesComment")) category = "wastes";
		
		if(currentClass.equals("Discharges") || currentClass.equals("Emissions") || currentClass.equals("Wastes") || currentClass.equals("DischargesComment") || currentClass.equals("EmissionsComment") || currentClass.equals("WastesComment")) {
			customCondition = "(isDeleted = false or isDeleted is null) and dailyUid=:currentDailyUid and group_uid=:groupUid ";
			customCondition += "and category=:category";
			query.addParam("currentDailyUid", userSelection.getDailyUid());
			query.addParam("groupUid", userSelection.getGroupUid());
			query.addParam("category", category);
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
