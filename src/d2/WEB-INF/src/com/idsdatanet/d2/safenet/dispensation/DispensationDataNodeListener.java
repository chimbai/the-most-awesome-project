package com.idsdatanet.d2.safenet.dispensation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MocDisp;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DispensationDataNodeListener  extends EmptyDataNodeListener implements DataNodeLoadHandler{
	

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if(obj instanceof MocDisp) {
			if (StringUtils.isNotBlank(userSelection.getDailyUid())) {
				MocDisp	mocDisp = (MocDisp) obj;
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				mocDisp.setMocDispDatetime(daily.getDayDate());
				String name = ApplicationUtils.getConfiguredInstance().getCachedUser(userSelection.getUserUid()).getFname();
				mocDisp.setOriginatorName(name);
			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean,CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode,UserSession userSession) throws Exception {
		Object object = targetNode.getData();
		if (object instanceof MocDisp) {
			MocDisp thisMocDisp = (MocDisp) object;
			thisMocDisp.setMocDispNumberPrefix(null);
			thisMocDisp.setMocDispDatetime(null);
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof MocDisp) {
			MocDisp mocDisp = (MocDisp) obj;
			Manager dao=ApplicationUtils.getConfiguredInstance().getDaoManager();
			String wellUid=session.getCurrentWellUid();
			String currentWellName = session.getCurrentWell().getWellName();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date plainDate=session.getCurrentDaily().getDayDate();
			String date= dateFormat.format(plainDate);
			
			// To check if it is a new record
			if (node.getInfo().isNewlyCreated()) {
				String mocNumberQuery="SELECT md.mocDispNumber "
						+ "FROM MocDisp md "
						+ "WHERE md.formType='DR' "
						+ "AND (md.isDeleted is null or md.isDeleted=false) "
						+ "AND md.wellUid=:wellUid "
						+ "ORDER BY mocDispNumber DESC ";
				List<String> mocNumber = dao.findByNamedParam(mocNumberQuery,"wellUid",wellUid);
					//Assign dispNumber to new record
				String prefix=currentWellName+"-"+date+" - ";
				if (mocNumber.size()>0) {
					Integer num=Integer.parseInt(mocNumber.get(0));
					String newNum="";
					if (num<9) newNum="0";
					newNum+=Integer.toString(num+1);
					mocDisp.setMocDispNumber(newNum);
					mocDisp.setMocDispNumberPrefix(prefix+newNum);
					
				}else {
					//This is the first mocDisp in the Well
					mocDisp.setMocDispNumber("01");
					mocDisp.setMocDispNumberPrefix(prefix+"01");
				}
				//Set form_type="DR"
				mocDisp.setFormType("DR");
				
				//Add name of the user who created the Dispensation
				String name=session.getCurrentUser().getFname();
				mocDisp.setOriginatorName(name);
				
				mocDisp.setMocDispDatetime(plainDate);
			}
		}	
	}

	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
