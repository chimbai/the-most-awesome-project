package com.idsdatanet.d2.safenet.unwantedEvent;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class UnwantedEventDataNodeAllowedAction implements DataNodeAllowedAction {
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		// ...you have to put all your logic here in order to show or hide your button. Return FALSE to hide the button.
		
		Object object = node.getData();
		if (object instanceof UnwantedEvent) {
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "onlyAllowSupervCreatorEditDel"))) {
				
				if ((StringUtils.equals(action, "edit")) || (StringUtils.equals(action, "delete"))){
					UnwantedEvent thisUnwantedEvent = (UnwantedEvent) object;
					String[] paramsFields = {"currentUser", "thisEvent", "loginUser"};
					Object[] paramsValues = new Object[3]; 
					paramsValues[0] = session.getUserUid(); 
					paramsValues[1] = thisUnwantedEvent.getUnwantedEventUid();
					paramsValues[2] = "%" + session.getUserUid() + "%";
					
					String strSql = "FROM UnwantedEvent WHERE (raisedBy =:currentUser OR eventSupervisor like :loginUser) AND type='UE' and unwantedEventUid =:thisEvent";
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					if (lstResult.size() > 0){
						return true;
					}else{
						return false;
					}
				}
			}
	
		}
		
		return true;

	}
}
