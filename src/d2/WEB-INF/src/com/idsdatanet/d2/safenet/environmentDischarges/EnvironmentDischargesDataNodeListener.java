package com.idsdatanet.d2.safenet.environmentDischarges;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.EnvironmentDischargesComment;
import com.idsdatanet.d2.core.model.LookupEnvironmentDischarges;
import com.idsdatanet.d2.core.model.ProductionPumpParamComp;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.rigstock.RigStockUtil;

public class EnvironmentDischargesDataNodeListener extends EmptyDataNodeListener {
	
    @Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
    	EnvironmentDischargesUtil.calculateSumTotalForWell(commandBean, node);
    	this.populateLookupUidAsDynamicAttribute(node);
        this.populateTotalDailyActualAsDynamicAttribute(commandBean, node);
    }
    
    @Override
    public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
    	this.afterDataNodeLoad(commandBean, null, targetNode, null, null);
    }

    @Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
        // If autoCalculation for totalForWell is needed, use
        // EnvironmentDischargesUtil.calculateSumTotalForWell;
    	this.checkHistoricalRecords(commandBean, node, status, session);
    	this.updateUsedAmountAfterCopyPaste(commandBean, node, session);
    	EnvironmentDischargesUtil.calculateSumTotalForWell(commandBean, node);
	}
	
    
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request, TreeModelDataDefinitionMeta meta) throws Exception {
        // If autoCalculation for totalForWell is needed, use
        // EnvironmentDischargesUtil.calculateSumTotalForWell;
    	EnvironmentDischargesUtil.calculateSumTotalForWell(commandBean, node);
	}
        
	
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, TreeModelDataDefinitionMeta meta) throws Exception{
        // If autoCalculation for totalForWell is needed, use
        // EnvironmentDischargesUtil.calculateSumTotalForWell;
    	this.afterDataNodeLoad(commandBean, null, node, null, null);
    	EnvironmentDischargesUtil.calculateSumTotalForWell(commandBean, node);
	}
	
    /**
     * Populate the lookupLabel as dynaAttribute for screen filter use
     * @param node
     * @throws Exception
     */
    private void populateLookupUidAsDynamicAttribute(CommandBeanTreeNode node) throws Exception {
    	Object obj = node.getData();
        if (obj != null && obj instanceof EnvironmentDischarges) {
            EnvironmentDischarges thisEnvironmentDischarges = (EnvironmentDischarges) node.getData();
            String lookupEnvironmentDischargesUid = thisEnvironmentDischarges.getLookupEnvironmentDischargesUid();
            if (lookupEnvironmentDischargesUid != null && StringUtils.isNotBlank(lookupEnvironmentDischargesUid)) {
                List<LookupEnvironmentDischarges> lookupEnvironmentDischarges = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LookupEnvironmentDischarges WHERE (isDeleted = FALSE or isDeleted IS NULL) AND lookupEnvironmentDischargesUid = :lookupEnvironmentDischargesUid", EnvironmentDischargesCommandBeanListener.UID, lookupEnvironmentDischargesUid);
                if (!lookupEnvironmentDischarges.isEmpty()) {
                    LookupEnvironmentDischarges led = lookupEnvironmentDischarges.get(0);
                    node.getDynaAttr().put(EnvironmentDischargesCommandBeanListener.UID, led.getName());
                }
            }
        }
    }
    
    /**
     * Populate Daily Actual Total as a dynamic attribute for use in DDR.
     * The total is the sum of 3 fields; actualAmount, actualIncidentalAmount and actualUnplannedAmount
     * @param commandBean
     * @param node - containing the EnvironmentDischarges object, and also for putting the dynaAttr
     * @throws Exception
     */
    private void populateTotalDailyActualAsDynamicAttribute(CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	Object obj = node.getData();
        if (obj != null && obj instanceof EnvironmentDischarges) {
            EnvironmentDischarges ed = (EnvironmentDischarges) node.getData();
            Double totalDailyAmount = 0.0;
            
            if (ed.getActualAmount() != null) totalDailyAmount += ed.getActualAmount();
            if (ed.getActualIncidentalAmount() != null) totalDailyAmount += ed.getActualIncidentalAmount();
            if (ed.getActualUnplannedAmount() != null) totalDailyAmount += ed.getActualUnplannedAmount();
            
            if (ed.getCategory()!=null && "discharges".equals(ed.getCategory())){
            	LookupEnvironmentDischarges lookupEnvironmentDischarges = (LookupEnvironmentDischarges) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupEnvironmentDischarges.class, ed.getLookupEnvironmentDischargesUid());
				if(lookupEnvironmentDischarges != null && StringUtils.equalsIgnoreCase(lookupEnvironmentDischarges.getName(), "OOC (wet) (Average)")) {
					if (ed.getActualIncidentalAmount() != null) totalDailyAmount = ed.getActualIncidentalAmount();
				}
            }
          //ticket number #15883 update Total well for "Well OOC Average"
            if (ed.getCategory()!=null && "discharges".equals(ed.getCategory())){
            	LookupEnvironmentDischarges lookupEnvironmentDischarges = (LookupEnvironmentDischarges) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupEnvironmentDischarges.class, ed.getLookupEnvironmentDischargesUid());
				if(lookupEnvironmentDischarges != null && StringUtils.equalsIgnoreCase(lookupEnvironmentDischarges.getName(), "Well OOC (wet average)")) {
					if (ed.getActualAmount() != null) totalDailyAmount = ed.getActualAmount();
				}
            }
            
            CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
            thisConverter.setReferenceMappingField(EnvironmentDischarges.class, "actual_amount");
            if (thisConverter.isUOMMappingAvailable()) {
            	node.getDynaAttr().put("ActualTotalAmount", thisConverter.getFormattedValue(totalDailyAmount));
            }
        }
    }
    
    private void updateUsedAmountAfterCopyPaste(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {
    	Object obj = node.getData();
    	String dailyUid= session.getCurrentDailyUid();
    	Double svUsed = EnvironmentDischargesUtil.calculateUsedDieselFromSV(commandBean, node, dailyUid);
    	Double rsUsed = EnvironmentDischargesUtil.calculateUsedDieselFromRigStock(commandBean, node, dailyUid);
    	if(obj instanceof Emissions) {
    		Emissions emissions = (Emissions) obj;
    		String lookupLabel = node.getDynaAttr().get("lookupEnvironmentDischargesUid").toString();
    		if (lookupLabel.equals("Vessel Diesel")){
    			emissions.setActualAmount(svUsed);
    		}
    		
    		if (lookupLabel.equals("MODU Diesel")){
    			emissions.setActualAmount(rsUsed);
    		}
    	}
    	
    }
    
    private void checkHistoricalRecords(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, UserSession session) throws Exception {
    	Object object = node.getData();
    	String dailyUid = session.getCurrentDailyUid();
    	
    	if(object instanceof Discharges) {
        	String[] detail = EnvironmentDischargesUtil.getStockDetail(node);
        	if (detail != null){
    			if (EnvironmentDischargesUtil.checkDischargesStockExist(dailyUid, detail[0])){
    				status.setContinueProcess(false, true);
    				status.setFieldError(node, "lookupEnvironmentDischargesUid", "The value already exists in the database");
    				return;
    			}
    		}
    	}
    	
    	if(object instanceof DischargesComment)
		{
			String strSql = "FROM EnvironmentDischargesComment WHERE (isDeleted = false or isDeleted is null) AND category ='discharges' AND dailyUid =:dailyUid";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List <EnvironmentDischargesComment> lstResult = (List<EnvironmentDischargesComment>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size()>0){
				status.setContinueProcess(false, true);
				commandBean.getSystemMessage().addError("Only one discharges comment is allowed.");
				return;
			}
		}
    	
    	if(object instanceof Emissions) {
        	String[] detail = EnvironmentDischargesUtil.getStockDetail(node);
        	if (detail != null){
    			if (EnvironmentDischargesUtil.checkEmissionsStockExist(dailyUid, detail[0])){
    				status.setContinueProcess(false, true);
    				status.setFieldError(node, "lookupEnvironmentDischargesUid", "The value already exists in the database");
    				return;
    			}
    		}
    	}
    	
    	if(object instanceof EmissionsComment)
		{
			String strSql = "FROM EnvironmentDischargesComment WHERE (isDeleted = false or isDeleted is null) AND category ='emissions' AND dailyUid =:dailyUid";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List <EnvironmentDischargesComment> lstResult = (List<EnvironmentDischargesComment>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size()>0){
				status.setContinueProcess(false, true);
				commandBean.getSystemMessage().addError("Only one emissions comment is allowed.");
				return;
			}
		}
    	
    	if(object instanceof Wastes) {
        	String[] detail = EnvironmentDischargesUtil.getStockDetail(node);
        	if (detail != null){
    			if (EnvironmentDischargesUtil.checkWastesStockExist(dailyUid, detail[0])){
    				status.setContinueProcess(false, true);
    				status.setFieldError(node, "lookupEnvironmentDischargesUid", "The value already exists in the database");
    				return;
    			}
    		}
    	}
    	
    	if(object instanceof WastesComment) {
			String strSql = "FROM EnvironmentDischargesComment WHERE (isDeleted = false or isDeleted is null) AND category ='wastes' AND dailyUid =:dailyUid";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List <EnvironmentDischargesComment> lstResult = (List<EnvironmentDischargesComment>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size()>0){
				status.setContinueProcess(false, true);
				commandBean.getSystemMessage().addError("Only one wastes comment is allowed.");
				return;
			}
    	}
    }
}
