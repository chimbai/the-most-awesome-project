package com.idsdatanet.d2.safenet.lookupLessonNumber;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupLessonNumber;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupLessonNumberDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LookupLessonNumber) {
			LookupLessonNumber thisLookupLessonNumber = (LookupLessonNumber) object;
			
			if (thisLookupLessonNumber.getStartingNumber() != null && thisLookupLessonNumber.getLastNumberGenerated() != null) {
				Integer startingNumber = thisLookupLessonNumber.getStartingNumber();
				Integer lastGeneratedNumber = thisLookupLessonNumber.getLastNumberGenerated();
				Integer maxLessonTicketNumber;
				if (StringUtils.isNotBlank(thisLookupLessonNumber.getLookupLessonNumberUid())) {
					String[] paramsFields = {"lookupLessonNumberUid"};
					Object[] paramsValues = {thisLookupLessonNumber.getLookupLessonNumberUid()};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select startingNumber from LookupLessonNumber where (isDeleted = false or isDeleted is null) and lookupLessonNumberUid=:lookupLessonNumberUid", paramsFields, paramsValues);
					Integer dbStartingNum;
					if (lstResult.size() > 0) {
						Object a = (Object) lstResult.get(0);
						if (a != null) {
							dbStartingNum = (Integer) Integer.parseInt(a.toString());
							if (dbStartingNum.compareTo(startingNumber) != 0) {

								maxLessonTicketNumber = this.maxLessonTicketNumber(thisLookupLessonNumber.getLookupLessonNumberUid());
								if (maxLessonTicketNumber != null) {
									if(startingNumber < lastGeneratedNumber) {
										status.setContinueProcess(false, true);
										status.setFieldError(node, "startingNumber", "Starting value already exists in the database");
										return;
									}
								}
							}
						}		
					}
				}
			}
			
			if (checkMinimumLengthLessThanPrevious(thisLookupLessonNumber)){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "minimumLength", "The value must be greater than previous");
				return;
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		Object object = node.getData();
		
		if (object instanceof LookupLessonNumber) {
			LookupLessonNumber thisLookupLessonNumber = (LookupLessonNumber) object;
			Boolean isDirty = false;
			if (thisLookupLessonNumber.getStartingNumber() != null && thisLookupLessonNumber.getLastNumberGenerated() == null) {
				thisLookupLessonNumber.setLastNumberGenerated(thisLookupLessonNumber.getStartingNumber()-1);
				isDirty = true;
			} else if (thisLookupLessonNumber.getStartingNumber() != null && thisLookupLessonNumber.getLastNumberGenerated() != null) {
				Integer startingNumber = thisLookupLessonNumber.getStartingNumber();
				Integer lastGeneratedNumber = thisLookupLessonNumber.getLastNumberGenerated();
				Integer maxLessonTicketNumber = this.maxLessonTicketNumber(thisLookupLessonNumber.getLookupLessonNumberUid());
				if (maxLessonTicketNumber == null) {
					thisLookupLessonNumber.setLastNumberGenerated(thisLookupLessonNumber.getStartingNumber()-1);
					isDirty = true;
				} else {
					if(startingNumber > lastGeneratedNumber) {
						thisLookupLessonNumber.setLastNumberGenerated(thisLookupLessonNumber.getStartingNumber()-1);
						isDirty = true;
					}
				}
			}
			if(isDirty) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLookupLessonNumber);
		}
	}
	
	private Integer maxLessonTicketNumber(String lookupLessonNumberUid) throws Exception {
		
		
		if (StringUtils.isNotBlank(lookupLessonNumberUid)) {
			String[] paramsFields = {"country"};
			Object[] paramsValues = {lookupLessonNumberUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select max(lessonTicketNumber) from LessonTicket where country=:country", paramsFields, paramsValues);
			Integer maxNum;
			if (lstResult.size() > 0) {
				Object a = (Object) lstResult.get(0);
				if (a != null) {
					maxNum = (Integer) Integer.parseInt(a.toString());
					return maxNum;
				}		
			}
		}
		
		return null;
		
	}
	
	/**
	 * Method to check if minimum length lesser than previous value before safe
	 * @throws Exception
	 */
	public static Boolean checkMinimumLengthLessThanPrevious(LookupLessonNumber lookupLessonNumber) throws Exception{
		
		if (lookupLessonNumber != null) {
			if (StringUtils.isNotBlank(lookupLessonNumber.getLookupLessonNumberUid())) {
				String[] paramsFields = {"lookupLessonNumberUid"};
				Object[] paramsValues = {lookupLessonNumber.getLookupLessonNumberUid()};
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select minimumLength from LookupLessonNumber where (isDeleted = false or isDeleted is null) and lookupLessonNumberUid=:lookupLessonNumberUid", paramsFields, paramsValues);
				Integer minLength;
				if (lstResult.size() > 0) {
					Object a = (Object) lstResult.get(0);
					if (a != null) {
						minLength = (Integer) Integer.parseInt(a.toString());
						if (lookupLessonNumber.getMinimumLength() < minLength) return true;
					}		
				}
			}
		}
		
		return false;
	}
}
