package com.idsdatanet.d2.safenet.unwantedEvent;

public class UnwantedEventRecord {

	private String unwantedEventUid = null;
	private String eventRef = null;
	private String eventTitle = null;
	private String eventNptCode = null;
	private String eventStatus = null;
	private String eventCause = null;
	private String contractorType = null;
	private String nameOfContractor = null;
	private Double nptDuration = 0.0;
	private Double estimatedLoss = 0.0;
	private String parentUnwantedEventUid = null;
	private String activityUid = null;

	public void setUnwantedEventUid(String unwantedEventUid) { this.unwantedEventUid = unwantedEventUid; }
	public String getUnwantedEventUid() { return this.unwantedEventUid; }
	
	public void setEventRef(String eventRef) { this.eventRef = eventRef; }
	public String getEventRef() { return this.eventRef; }

	public void setEventTitle(String eventTitle) { this.eventTitle = eventTitle; }
	public String getEventTitle() { return this.eventTitle; }

	public void setEventNptCode(String eventNptCode) { this.eventNptCode = eventNptCode; }
	public String getEventNptCode() { return this.eventNptCode; }
	
	public void setEventStatus(String eventStatus) { this.eventStatus = eventStatus; }
	public String getEventStatus() { return this.eventStatus; }

	public void setEventCause(String eventCause) { this.eventCause = eventCause; }
	public String getEventCause() { return this.eventCause; }

	public void setContractorType(String contractorType) { this.contractorType = contractorType; }
	public String getContractorType() { return this.contractorType; }

	public void setNameOfContractor(String nameOfContractor) { this.nameOfContractor = nameOfContractor; }
	public String getNameOfContractor() { return this.nameOfContractor; }

	public void setNptDuration(Double nptDuration) { this.nptDuration = nptDuration; }
	public Double getNptDuration() { return this.nptDuration; }

	public void setEstimatedLoss(Double estimatedLoss) { this.estimatedLoss = estimatedLoss; }
	public Double getEstimatedLoss() { return this.estimatedLoss; }

	public void setParentUnwantedEventUid(String parentUnwantedEventUid) { this.parentUnwantedEventUid = parentUnwantedEventUid; }
	public String getParentUnwantedEventUid() { return this.parentUnwantedEventUid; }

	public void setActivityUid(String activityUid) { this.activityUid = activityUid; }
	public String getActivityUid() { return this.activityUid; }

}
