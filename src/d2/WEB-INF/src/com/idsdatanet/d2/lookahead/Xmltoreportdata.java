package com.idsdatanet.d2.lookahead;

import java.util.Iterator;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;


public class Xmltoreportdata implements ReportDataGenerator{
	
	private String theXmlData;
	
	public void setXmlData(String xmlData){
		this.theXmlData = xmlData;
	}
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportData, ReportValidation reportValidation) throws Exception {
		this.recursive(reportDataNode, this.theXmlData);		
		if(true) return;	
		// TODO Auto-generated method stub
	
	}
	
	private void recursive(ReportDataNode reportDataNode, String xmlData) throws Exception {
		
		SimpleXMLElement getxmldata = SimpleXMLElement.loadXMLString(xmlData);
		for (Iterator i = getxmldata.getChild().iterator(); i.hasNext();) {				
			
			
			SimpleXMLElement calendarData = (SimpleXMLElement) i.next();
			ReportDataNode calendarNode = reportDataNode.addChild(calendarData.getTagName());
			for(Iterator j = calendarData.getChild().iterator(); j.hasNext(); ) 
			{	
				
				SimpleXMLElement daytasks = (SimpleXMLElement) j.next(); 				
				ReportDataNode daytasksnode = calendarNode.addChild(daytasks.getTagName());				
				for(Iterator k = daytasks.getChild().iterator(); k.hasNext(); )
				{
					
					SimpleXMLElement daytasksdetails = (SimpleXMLElement) k.next();
					daytasksnode.addProperty("dayNum",daytasks.getAttribute("dayNum").toString());
					ReportDataNode elementsnode = daytasksnode.addChild(daytasksdetails.getTagName());					
				
					for(Iterator x = daytasksdetails.getChild().iterator(); x.hasNext(); )						
					{
						SimpleXMLElement elements = (SimpleXMLElement) x.next();						
						elementsnode.addProperty(elements.getTagName(),elements.getText());
						
					}					
					
				}
					
			}		
		}
		
	}
	
}
