package com.idsdatanet.d2.lookahead.document;

import java.io.File;
import java.util.Date;

import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class LookaheadDocumentOutputFile {
	private String reportFilesUid = null;
	private Date dayDate = null;
	private String reportNumber = null;
	private String reportFilePath = null;
	//private String reportDisplayName = null;
	private String reportType = null;
	private Date timeReportFileCreated = null;
	private String reportUserUid = null;
	private boolean is_private = true;
	private String docNum = null;
	
	public LookaheadDocumentOutputFile(ReportFiles reportFile, File outputFile) throws Exception {
		this.reportFilesUid = reportFile.getReportFilesUid();
		this.reportFilePath = reportFile.getReportFile();
		//this.reportDisplayName = displayName;
		this.reportUserUid = reportFile.getReportUserUid();

		if(reportFile.getIsPrivate() != null){
			this.is_private = reportFile.getIsPrivate();
		}else{
			this.is_private = true;
		}
		
		this.dayDate = reportFile.getReportDayDate();
		this.reportNumber = reportFile.getReportDayNumber();
		
		this.timeReportFileCreated = new Date(outputFile.lastModified());
		
		this.reportType = reportFile.getReportType();
		
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		extraProperties.readPropertiesFromXmlString(reportFile.getExtraProperties());
		if (!"".equals(extraProperties.getProperty("documentType"))) this.reportType = extraProperties.getProperty("documentType");
		if (!"".equals(extraProperties.getProperty("docNum"))) this.docNum = extraProperties.getProperty("docNum");
	}
	
	public String getReportFilesUid(){
		return this.reportFilesUid;
	}
	
	public String getReportFilePath(){
		return this.reportFilePath;
	}
	
	public Date getDayDate(){
		return this.dayDate;
	}
	
	public String getReportNumber() {
		return this.reportNumber;
	}
	
	public Date getTimeReportFileCreated(){
		return this.timeReportFileCreated;
	}
	
	public String getTimeReportFileCreatedText(){
		return FreemarkerUtils.formatDurationSince(this.timeReportFileCreated);
	}
	
	public boolean getNewlyGeneratedFlag(){
		return FreemarkerUtils.isDocumentNewlyGenerated(this.timeReportFileCreated);
	}
	
	public String getReportUserUid(){
		return this.reportUserUid;
	}
	
	public boolean getIsPrivate(){
		return this.is_private;
	}
	
	public String getReportType(){
		return this.reportType;
	}
	
	public String getDocNum(){
		return this.docNum;
	}
	
	public void setIsPrivate(boolean value){
		this.is_private = value;
	}
	
	public void setReportFilePath(String value){
		this.reportFilePath = value;
	}
	
	public void setReportUserUid(String value){
		this.reportUserUid = value;
	}
	
	public void setTimeReportFileCreated(Date date){
		this.timeReportFileCreated = date;
	}
	
	public void setDocNum (String value) {
		this.docNum = value;
	}
}
