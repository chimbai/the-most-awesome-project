package com.idsdatanet.d2.lookahead.document;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportUtils;
import com.idsdatanet.d2.lookahead.document.LookaheadDocumentOutputFile;
import java.net.URLEncoder;
import com.idsdatanet.d2.lookahead.Xmltoreportdata;

public class LookaheadDocumentModule extends DefaultReportModule{
	
	private Map<String, String> documentXSL = null;
	private String transactionUid = null;
	private String currentReqDocType = null;
	private String currentXmlData = null;
	private String currentOperationUid = null;
	private String existingFileUid = null;
	
	public transient DataLoaderInterceptor dataLoaderInterceptor = null;
	
	public void setDocumentXSL(Map<String, String> list){
		this.documentXSL = list;		
	}
	
	public void setDataLoaderInterceptor(DataLoaderInterceptor interceptor) throws Exception {
		if(this.dataLoaderInterceptor != null) throw new Exception("DataLoaderInterceptor has already been assigned: " + this.dataLoaderInterceptor.getClass().getName());
		this.dataLoaderInterceptor = interceptor;		
	}		
	

	public void generateDocument(UserSession userSession, HttpServletRequest request) throws Exception {		
		this.currentReqDocType = request.getParameter("requestForDoc");
		this.currentXmlData = request.getParameter("xmldata");
		this.currentOperationUid = request.getParameter("operationUid");		
		this.existingFileUid = request.getParameter("existingFileUid");
		if (this.existingFileUid != null) this.deleteDoc(this.existingFileUid);	

		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(userSession);
		userSelection.setCustomProperty("requestDoc", this.currentReqDocType);			
		userSelection.setCustomProperty("operationUid",this.currentOperationUid);			
		
		Xmltoreportdata reportdata = new Xmltoreportdata();		
		reportdata.setXmlData(this.currentXmlData);	
		
		ReportJobParams params = new ReportJobParams();
		params.setAdditionalReportDataGenerator(reportdata);

		this.submitReportJob(userSelection,params);				
	}
	
	public List<LookaheadDocumentOutputFile> listDocuments(UserSession userSession, HttpServletRequest request) throws Exception {
		this.transactionUid = request.getParameter("inventoryTransactionUid");
		//this.currentReqDocType = request.getParameter("requestForDoc");
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(userSession);
		List<Object> documentFiles = this.loadReportOutputFiles(userSelection, request);
		List<LookaheadDocumentOutputFile> finalDocumentFilesOutput = new ArrayList<LookaheadDocumentOutputFile>();
		for (Object file: documentFiles) {
			String downloadUrl = "";
			LookaheadDocumentOutputFile thisFile = (LookaheadDocumentOutputFile) file;
			downloadUrl = userSession.getClientBaseUrl() + "download.html?handlerId=ddr&reportId=" + thisFile.getReportFilesUid() + "&filename=" + thisFile.getReportFilePath();
			thisFile.setReportFilePath(userSession.getClientBaseUrl() + "downloadHelper.jsp?downloadFileUrl=" + URLEncoder.encode(downloadUrl, "utf-8"));
			thisFile.setReportUserUid(this.getUserFullname(thisFile.getReportUserUid()));
			finalDocumentFilesOutput.add(thisFile);
		}
		
		return finalDocumentFilesOutput;
	}
	
	@Override
	protected String getXslFileInLocalPath(UserContext userContext, ReportJobParams reportJobParams) throws Exception {
		String xsl = this.documentXSL.get(userContext.getUserSelection().getCustomProperty("requestDoc"));
		if(StringUtils.isBlank(xsl)) throw new Exception("Xsl file not found for report type: " + this.getReportType(userContext.getUserSelection()));		
		return xsl;
	}
	
	@Override
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{	
		return this.getReportType(userContext.getUserSelection()) + "/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileNameForLookaheadDoc(userContext));
	}
	
	protected String getOutputFileNameForLookaheadDoc(UserContext userContext) throws Exception{		
		//random generate document filename first
		Random rand = new Random(); 	
		return this.getReportType() + "_" + rand.nextInt() + ".pdf";
	}
	
	@Override
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception {
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		extraProperties.setProperty("documentType", this.currentReqDocType);
		extraProperties.setProperty("inventoryTransactionUid", this.currentOperationUid);		
		report.setExtraProperties(extraProperties.serializePropertiesToXmlString());
	}
	
	@Override
	protected List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, String reportType, HttpServletRequest request) throws Exception{
		List<ReportFiles> db_files = this.loadSourceReportFilesFromDB(userSelection, reportType);
		List<Object> output_files = new ArrayList<Object>();
		
		for(ReportFiles db_file: db_files){
			if(StringUtils.isNotBlank(db_file.getReportFile())){
				File outputFile = this.getReportOutputFile(db_file);
				if(outputFile.exists()){
					try{
						//String display_name = this.getReportDisplayName(db_file, operation, userSelection);
						LookaheadDocumentOutputFile output_file = this.onLoadLookaheadDocumentOutputFile(new LookaheadDocumentOutputFile(db_file, outputFile));
						if(output_file != null) output_files.add(output_file);
					}catch(Exception e){
						//this.logger.error("Error encountered when creating ReportOutputFile: " + e.getMessage());
					}
				}else{
					//this.logger.error("Physical output file not found. [ReportFilesUid: " + db_file.getReportFilesUid() + ", Output file: " + null2EmptyString(db_file.getReportFile()) + "]");
				}
			}
		}		
		return output_files;
	}	
	
	@Override
	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception {
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		String query = null;
		
		//param_names.add("requestDoc"); param_values.add(reportType);
		//param_names.add("operationUid"); param_values.add("%" + this.transactionUid + "%");
		param_names.add("reportType"); param_values.add(reportType);
		param_names.add("inventoryTransactionUid"); param_values.add("%" + this.transactionUid + "%");
		//query = "from ReportFiles where reportType = :reportType and (isDeleted = false or isDeleted is null) and extraProperties like :operationUid";
		query = "from ReportFiles where reportType = :reportType and (isDeleted = false or isDeleted is null) and extraProperties like :inventoryTransactionUid order by dateGenerated desc";		
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, param_names, param_values);
	}
	
	protected LookaheadDocumentOutputFile onLoadLookaheadDocumentOutputFile(LookaheadDocumentOutputFile LookaheadDocumentOutputFile){		
		return LookaheadDocumentOutputFile;
		
	}	
	
	private String getUserFullname(String userUid) throws Exception {
		String userFullname = null;
		String strSql = "SELECT fname FROM User WHERE userUid = :reportUserUid";
		String[] paramsField = {"reportUserUid"};
		String[] paramsValue = {userUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		Object fullname = (Object) lstResult.get(0);
		userFullname = fullname.toString();		
		return userFullname;
	}
	
	private void deleteDoc(String fileUid) throws Exception {
		ReportFiles dbFile = (ReportFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportFiles.class, fileUid);
		if(dbFile == null || (dbFile.getIsDeleted() != null && dbFile.getIsDeleted())){
			return;
		}
		
		File actual_file = this.getReportOutputFile(dbFile);
		if(! actual_file.exists()){
			return;
		}
		
		try{
			//System.out.println("trying to delete the previous document......");
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ReportFiles set isDeleted = true where reportFilesUid = :reportFilesUid", new String[] { "reportFilesUid" }, new Object[] { dbFile.getReportFilesUid() });
			actual_file.delete();
		}catch(Exception e){
			
		}		
	}
		
}
