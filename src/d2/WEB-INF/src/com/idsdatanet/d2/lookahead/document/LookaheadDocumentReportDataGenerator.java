package com.idsdatanet.d2.lookahead.document;

import java.util.Date;
import java.util.List;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.LookaheadRequirement;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class LookaheadDocumentReportDataGenerator implements ReportDataGenerator{
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub		
		Date StartDate = CommonUtil.getConfiguredInstance().currentDateTime(userContext.getUserSelection().getGroupUid(), userContext.getUserSelection().getWellUid());
		
		// Getting the Today date
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid())) {
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userContext.getUserSelection().getOperationUid());
			if(operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
				if(operation.getStartDate() != null) StartDate = operation.getStartDate();
			}
		}
		int Year=0;
		int Month=0;
		int Day=0;
		int Hours=0;
		int Minutes=0;
		int Seconds=0;
		
		SimpleDateFormat DateF = new SimpleDateFormat("dd MMM yyyy");
		SimpleDateFormat Hour = new SimpleDateFormat("HHmm");
		SimpleDateFormat YY = new SimpleDateFormat("yyyy");
		SimpleDateFormat MM = new SimpleDateFormat("MM");
		SimpleDateFormat DD = new SimpleDateFormat("dd");
		SimpleDateFormat H = new SimpleDateFormat("HH");
		SimpleDateFormat M = new SimpleDateFormat("mm");
		SimpleDateFormat S = new SimpleDateFormat("ss");
		
		Year = Integer.parseInt(YY.format(StartDate.getTime()));
		Month = Integer.parseInt(MM.format(StartDate.getTime()));
		Day = Integer.parseInt(DD.format(StartDate.getTime()));
		Hours = Integer.parseInt(H.format(StartDate.getTime()));
		Minutes = Integer.parseInt(M.format(StartDate.getTime()));
		Seconds = Integer.parseInt(S.format(StartDate.getTime()));
		
		String[] day_hour = { "0000","0100","0200","0300","0400","0500","0600","0700","0800","0900","1000","1100","1200","1300",
				"1400","1500","1600","1700","1800","1900","2000","2100","2200","2300" };
		
		String[][] dataarray = new String[300][24];
		String[] datadate = new String[300];
		String[] datatime = new String[300];
		String[][] datadesc = new String[300][24];
		String[][] dataReq = new String[300][100];
		int[] dataReqCount = new int[300];
		
		
		String TargetDate=null;
		String TargetTime=null;
		String TodayDate = null;
		TodayDate = DateF.format(todayDate.getTime());
		
		Calendar StartDateObj2 = Calendar.getInstance();
		
		for (int j = 0; j < 300; j++) { 
			dataReqCount[j] = 0;
		}
		for (int j = 1; j < 300; j++) {  
			if (j!= 1){
				StartDateObj2.set(Year, Month-1, Day + j - 1, Hours, Minutes, Seconds);
			}else{
				StartDateObj2.set(Year, Month-1, Day , Hours, Minutes, Seconds);
			}
			TargetDate = DateF.format(StartDateObj2.getTime());
			TargetTime = Hour.format(StartDateObj2.getTime());
			
			datadate[j] = TargetDate;
			datatime[j] = TargetTime;
		}
		
		Double cumHour = 0.00;
		int countx = 0;
		int countTarget = 0;
		int county = 0;
		int TargetHour = 0;
		
		StartDateObj2.set(Year, Month-1, Day , Hours, Minutes, Seconds);
		TargetDate = DateF.format(StartDateObj2.getTime());
		TargetTime = Hour.format(StartDateObj2.getTime());
		TargetHour = Integer.parseInt(H.format(StartDateObj2.getTime()));
		
		String strSql = "FROM OperationPlanTask WHERE (isDeleted = false or isDeleted is null) AND (operationPlanPhaseUid is null) AND operationUid = :thisOperationUid Order By sequence";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);

		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				OperationPlanTask thisOperationPlanTask = (OperationPlanTask) objResult;
				
				Double duration = 0.0;
				String phaseName = null;
				String taskName = null;
				
				if(thisOperationPlanTask.getDuration()!=null && StringUtils.isNotBlank(thisOperationPlanTask.getDuration().toString())){
				//	CustomFieldUom thisConverter = new CustomFieldUom(OperationPlanTask.class, "duration");
				//	thisConverter.setBaseValue(duration);
				//	duration = thisConverter.getFormattedValue();
					duration = thisOperationPlanTask.getDuration() / 3600;
				}
				cumHour = cumHour + duration;
				
				//GET PHASE CODE LOOKUP NAME
				if(thisOperationPlanTask.getPhaseCode()!=null && StringUtils.isNotBlank(thisOperationPlanTask.getPhaseCode())){
					String strSql2 = "FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode = :thisPhaseCode Order By sequence";
					String[] paramsFields2 = {"thisPhaseCode"};
					Object[] paramsValues2 = {thisOperationPlanTask.getPhaseCode()};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					
					if (lstResult2.size() > 0){
						LookupPhaseCode thisLookupPhaseCode = (LookupPhaseCode) lstResult2.get(0);						
						phaseName = thisLookupPhaseCode.getName();
					}
				}
				
				//GET TASK CODE LOOKUP NAME
				if(thisOperationPlanTask.getTaskCode()!=null && StringUtils.isNotBlank(thisOperationPlanTask.getTaskCode())){
					String strSql3 = "FROM LookupTaskCode WHERE (isDeleted=false or isDeleted is null) AND shortCode = :thisTaskCode";
					String[] paramsFields3 = {"thisTaskCode"};
					Object[] paramsValues3 = {thisOperationPlanTask.getTaskCode()};
					
					List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
					
					if (lstResult3.size() > 0){
						LookupTaskCode thisLookupTaskCode = (LookupTaskCode) lstResult3.get(0);						
						taskName = thisLookupTaskCode.getName();
					}
				}
				
				String x;
				for (int j = 1; j < 300; j++) {  
					x = datadate[j];
					if (x.equals(TargetDate)){
						countx = j;
						j = 300;
					}else{
					}
				}

				county = TargetHour;
				
				// Setting the Next Target Date.
				StartDateObj2.set(Year, Month -1, Day, Hours + cumHour.intValue() , 0, 0);
				TargetDate = DateF.format(StartDateObj2.getTime());
				TargetTime = Hour.format(StartDateObj2.getTime());
				TargetHour = Integer.parseInt(H.format(StartDateObj2.getTime()));
				
				// Checking the Target Date Position.
				for (int j = 1; j < 300; j++) {  
					x = datadate[j];
					if (x.equals(TargetDate)){
						countTarget = j;
						j = 300;
					}else{
					}
				}
				if (countx != countTarget){
					for (int k = countx ; k <= countTarget; k++){
						if (dataarray[k][county] == null)
						{
							if (k==countx){
								dataarray[k][county] = phaseName + " > "+ taskName;
								datadesc[k][county] = thisOperationPlanTask.getDescription();
							}else{
								dataarray[k][0] = phaseName + " > "+ taskName;
								datadesc[k][0] = thisOperationPlanTask.getDescription();
							}
						}else{
							if (k == countx){
								dataarray[k][county] = dataarray[k][county] + " \n" + phaseName + " > " + taskName;
								datadesc[k][county] = datadesc[k][county] + " \n" + thisOperationPlanTask.getDescription();
							}else{
								dataarray[k][0] = dataarray[k][0] + " \n" + phaseName + " > " + taskName ;
								datadesc[k][0] = datadesc[k][0] + " \n" + thisOperationPlanTask.getDescription();
							}
						}
					}
				}else{
					if (dataarray[countx][county] == null)
					{
						dataarray[countx][county] = phaseName + " > "+ taskName;
						datadesc[countx][county] =  thisOperationPlanTask.getDescription();
					}else{
						dataarray[countx][county] = dataarray[countx][county] + "\n" +  phaseName + " > "+ taskName;;
						datadesc[countx][county] = datadesc[countx][county] + "\n" +  thisOperationPlanTask.getDescription();
					}
				}
				
				// Getting Requirement.
				String strSqlReq = "FROM LookaheadRequirement WHERE operationPlanTaskUid = :thisOperationPlanTaskUid";
				String[] paramsFieldsReq = {"thisOperationPlanTaskUid"};
				Object[] paramsValuesReq = {thisOperationPlanTask.getOperationPlanTaskUid()};
				List lstResultReq = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlReq, paramsFieldsReq, paramsValuesReq);
				if (lstResultReq.size() > 0){
					for(Object objResult2: lstResultReq){
						LookaheadRequirement thisLookaheadRequirement = (LookaheadRequirement) objResult2;
						for (int k = countx ; k <= countTarget; k++){
							dataReqCount[k] = dataReqCount[k] + 1;
							int Count = dataReqCount[k];
							dataReq[k][Count] = thisLookaheadRequirement.getRequirement();  
						}
					}
				} 
				
				
			} // End of Record For-Loop	
		}	
		String Target = null;
		for (int j = 1; j < 300; j++) {  
			Target = datadate[j];
			if (Target.equals(TodayDate)){
				countx = j;
				j = 300;
			}
		} 
		
		
		for (int j = countx; j < countx+7; j++) {  
			ReportDataNode thisReportNode = reportDataNode.addChild("OperationPlanTask");
			thisReportNode.addProperty("dayDate", datadate[j].toString());
			for (int i = 0; i < 24; i++) {  
				thisReportNode.addProperty("r"+day_hour[i], dataarray[j][i]);
				thisReportNode.addProperty("d"+day_hour[i], datadesc[j][i]);
			}
			if (dataReqCount[j] > 0)
			{
				for (int i = 1; i <= dataReqCount[j]; i++) { 
					ReportDataNode thisReportNode2 = thisReportNode.addChild("Requirement");
					thisReportNode2.addProperty("Requirement", dataReq[j][i]);
				}
			}
		}
		
	}

}
