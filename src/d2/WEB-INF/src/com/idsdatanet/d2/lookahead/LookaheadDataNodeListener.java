package com.idsdatanet.d2.lookahead;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.dao.QueryProperties;

public class LookaheadDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		List dailyLists  = null;
		List activityLists  = null;
		QueryProperties qp = new QueryProperties(); 
		qp.setUomConversionEnabled(false);
		String operationUid = userSelection.getOperationUid();

		// Set the information data into the list.
		dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType='DDR' order by reportDatetime", "operationUid", operationUid);
		
		
		
		if (object instanceof OperationPlanTask)
		{	
			OperationPlanTask OperationPlanTask = (OperationPlanTask)object;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OperationPlanTask.class, "duration");
			
			Double xDuration = 0.0;
			
			String OperationPlanTaskUid = OperationPlanTask.getOperationPlanTaskUid();
			String strSql = "FROM OperationPlanTask WHERE (isDeleted = false or isDeleted is null) and operationPlanTaskUid=:OperationPlanTaskUid";
			List lstOPT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "OperationPlanTaskUid", OperationPlanTaskUid, qp);
			if (lstOPT.size() > 0){
				OperationPlanTask tmpOPT = (OperationPlanTask) lstOPT.get(0);
				xDuration = tmpOPT.getDuration();
			}
			
			String operuid = (String) PropertyUtils.getProperty(object, "operationPlanTaskUid");
			String pCode = (String) PropertyUtils.getProperty(object, "phaseCode");
			String tCode = (String) PropertyUtils.getProperty(object, "taskCode");
			
			String activityCheck = "from Activity where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and phaseCode = :phaseCode and planReference = :taskCode";
			String[] paramNames  = {"operationUid", "phaseCode", "taskCode"};
			String[] paramValues = {operationUid, pCode , tCode};
			List activityChk   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityCheck, paramNames, paramValues);
			
			String activityid = null;
			if (!activityChk.isEmpty())
			{
				Object thisResult = (Object) activityChk.get(0);
				Activity act 	= (Activity)thisResult;
			    activityid = act.getActivityUid();
			}
			
			// Day Ahead Calculation checking.
			Double CumDuration = 0.0;
			Boolean checking = false;
			if (activityChk.size() > 0)
			{
				for(Iterator i=dailyLists.iterator(); i.hasNext();) {
					ReportDaily reportDaily = (ReportDaily)i.next();
					
					String activityQuery = "from Activity where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and operationUid = :operationUid order by endDatetime";
					String[] paramNames2  = {"dailyUid", "operationUid"};
					String[] paramValues2 = {reportDaily.getDailyUid(), operationUid};
					activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, paramNames2, paramValues2, qp);
					if(activityLists != null && activityLists.size() > 0) {
						for(Iterator it=activityLists.iterator(); it.hasNext();) {
							Activity activity 	= (Activity)it.next();
							CumDuration += activity.getActivityDuration();
							if (activity.getActivityUid().equals(activityid)) {
								checking = true;
								break;
							}
						}
					}
					if (checking) break;
				}
			}
			
			if (xDuration != null) {
				Double cDuration = null;

				cDuration = (Double) commandBean.getRoot().getDynaAttr().get("cDuration");
				
				if (cDuration == null) {
					commandBean.getRoot().getDynaAttr().put("cDuration", xDuration);
				
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cDuration", xDuration);
					
					thisConverter.setBaseValue(xDuration);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumDuration",thisConverter.getConvertedValue());
					thisConverter.setBaseValue(CumDuration - xDuration);
					if (checking) {
						if (thisConverter.isUOMMappingAvailable()){
							node.setCustomUOM("@daysAhead", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("daysAhead",thisConverter.getConvertedValue());
					}
					commandBean.getRoot().getDynaAttr().put("operationPlanTaskUid", operuid);
				} else {
					cDuration += xDuration;
					
					commandBean.getRoot().getDynaAttr().put("cDuration", cDuration);
					//thisConverter.setBaseValueFromUserValue(cDuration);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cDuration",cDuration);
					
					thisConverter.setBaseValue(cDuration);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumDuration",thisConverter.getConvertedValue());
					thisConverter.setBaseValue(CumDuration - cDuration);
					if (checking) {
						if (thisConverter.isUOMMappingAvailable()){
							node.setCustomUOM("@daysAhead", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("daysAhead",thisConverter.getConvertedValue());
					}
					commandBean.getRoot().getDynaAttr().put("operationPlanTaskUid", operuid);
				}
			}
		}

	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof OperationPlanTask) {
			CommandBeanTreeNodeImpl lastChildNode = null;
			// get the last activity node
			for (Map<String, CommandBeanTreeNode> map: node.getParent().getList().values()) {
				for (Iterator i = map.values().iterator(); i.hasNext(); ) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) i.next();
					if (!childNode.isTemplateNode() && childNode.getData() instanceof OperationPlanTask) {
						lastChildNode = childNode;
					}
				}
			}
			
			// Save the DataNode When Add New.
			if (lastChildNode != null) {
				Object lastObject = lastChildNode.getData();
				if (lastObject != null) {
					OperationPlanTask lastOperationPlanTask = (OperationPlanTask) lastObject;
					OperationPlanTask thisOperationPlanTask = (OperationPlanTask) obj;
					thisOperationPlanTask.setDepthMdMsl(lastOperationPlanTask.getDepthMdMsl());
					thisOperationPlanTask.setPhaseCode(lastOperationPlanTask.getPhaseCode());
					thisOperationPlanTask.setDuration(0.00);
				}
			}
		}
	}
	
	
}
