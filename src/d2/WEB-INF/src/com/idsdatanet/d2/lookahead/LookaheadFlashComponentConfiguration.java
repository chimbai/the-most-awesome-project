package com.idsdatanet.d2.lookahead;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookaheadFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = "../../webservice/lookaheadservice.html";
		return "d2Url=" + urlEncode(serviceUrl);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "lookahead";
	}
}