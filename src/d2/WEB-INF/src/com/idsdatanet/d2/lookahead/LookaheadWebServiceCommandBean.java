package com.idsdatanet.d2.lookahead;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.validation.BindException;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.LookaheadRequirement;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.LookaheadDayRequirement;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;
import com.idsdatanet.d2.drillnet.report.ReportJobStatus;
import com.idsdatanet.d2.lookahead.document.LookaheadDocumentModule;
import com.idsdatanet.d2.lookahead.document.LookaheadDocumentOutputFile;

public class LookaheadWebServiceCommandBean extends AbstractGenericWebServiceCommandBean{
	public static final String PARAM_INVOCATION_METHOD = "method";
	public static final String METHOD_GET_OPERATION_LIST = "get_activity_codes_list"; 
	public static final String METHOD_GET_LOOKAHEAD_DATA = "get_lookahead_data";
	public static final String ACTION_UPDATE_LOOKAHEAD_DATA = "update_lookahead_data";
	public static final String METHOD_LIST_DOCUMENT				= "listDocuments";
	public static final String METHOD_CHECK_JOB_STATUS			= "checkCurrentJobStatus";
	public static final String ACTION_DELETE_LOOKAHEAD_PHASE_DATA = "delete_lookahead_phase_data";
	public static final String ACTION_DELETE_LOOKAHEAD_TASK_DATA = "delete_lookahead_task_data";
	public static final String ACTION_RETRUN_PHASEUID = "return_phaseuid";
	public static final String METHOD_GET_TASKS_DATA = "get_tasks_data";
	public static final String METHOD_DO_REPORT = "do_report";
	public final static String METHOD_DOWNLOAD_FILE = "downloadFile";
	public static String savePhaseUid = "";
	public static String operationPlanPhaseUid = "";
	
	private LookaheadDocumentModule lDocModule;
	
	public void setLookaheadReportModule(LookaheadDocumentModule lDocModule){	
		this.lDocModule = lDocModule;
	}	
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String method = request.getParameter(PARAM_INVOCATION_METHOD);
		UserSession userSession = UserSession.getInstance(request);
		
		if(METHOD_GET_OPERATION_LIST.equals(method)){
			String operationCode = userSession.getCurrentOperationType();
			this.getActivityCodesList(request, response,operationCode);
		} else if(METHOD_GET_LOOKAHEAD_DATA.equals(method)){
			String operationUid = userSession.getCurrentOperationUid();
			if (userSession.getCurrentOperationUid()==null)
			{
				SimpleXmlResponse.send(response, false, "ERROR","No operation selected");
				return;
			}
			this.getLookaheadData(request, response, operationUid);
		}else if(ACTION_UPDATE_LOOKAHEAD_DATA.equals(method)){
			this.updateLookaheadRecord(request, response);
		}else if(ACTION_DELETE_LOOKAHEAD_PHASE_DATA.equals(method)){
			this.deleteLookaheadPhaseRecord(request, response);
		}else if(ACTION_DELETE_LOOKAHEAD_TASK_DATA.equals(method)){
			this.deleteLookaheadTaskRecord(request, response, null);
		}else if(METHOD_GET_TASKS_DATA.equals(method)){
			this.getAllTaskData(request, response);
		}else if(METHOD_DO_REPORT.equals(method)){
			try {
				this.doDoc(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (METHOD_LIST_DOCUMENT.equals(method)) {
			this.loadDoc(request, response);
		}else if (METHOD_CHECK_JOB_STATUS.equals(method)) {
			this.checkCurrentJobStatus(request, response);			
		}else if (METHOD_DOWNLOAD_FILE.equals(method)) {
			this.downloadFile(request, response);
		} 
	}
	
	private void getActivityCodesList(HttpServletRequest request, HttpServletResponse response,String operationCode) throws Exception {
		String webUrl = UserSession.getInstance(request).getClientBaseUrl();
		List<LookupTaskCode> taskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupTaskCode WHERE (isDeleted is null or isDeleted = false) and (operationCode=:operationCode or operationCode='' or operationCode is null) order by name", "operationCode", operationCode);
		
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		for(LookupTaskCode rec: taskCode){
			writer.startElement("LookupTaskCode");			
			writer.addElement("lookupTaskCodeUid", rec.getLookupTaskCodeUid());			
			writer.addElement("shortCode", rec.getShortCode());
			writer.addElement("name", rec.getName());			
			writer.endElement();
		}
		
		List<LookupPhaseCode> phaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupPhaseCode WHERE (isDeleted is null or isDeleted = false) and (operationCode=:operationCode or operationCode='' or operationCode is null) order by name", "operationCode", operationCode);
		
		for(LookupPhaseCode rec: phaseCode){
			writer.startElement("LookupPhaseCode");			
			writer.addElement("lookupPhaseCodeUid", rec.getLookupPhaseCodeUid());
			writer.addElement("shortCode", rec.getShortCode());
			writer.addElement("name", rec.getName());			
			writer.endElement();
		}
		writer.startElement("WebUrl");			
		writer.addElement("webUrl", webUrl);				
		writer.endElement();
		
		writer.close();
	}
	
	private void getLookaheadData(HttpServletRequest request, HttpServletResponse response, String operationUid) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		String operationCode = userSession.getCurrentOperationType();		
		String[] paramsFields = {"operationUid", "shortCode"};	
		String phaseName="";
		String taskName="";
		String planTaskUid="";
		String get_TaskCode="";
		String get_PhaseCode="";
		
		List<OperationPlanPhase> operationPlanPhase = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanPhase where (isDeleted is null or isDeleted = false) and ((phaseCode IS NOT NULL and phaseCode <> '') or (phaseName IS NOT NULL and phaseName <> '')) and operationUid=:operationUid order by sequence", "operationUid", operationUid);

		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		writer.startElement("CurrentOperationUid");
		writer.addElement("operationUid",operationUid);
		writer.endElement();
		for(OperationPlanPhase rec: operationPlanPhase){			
			writer.startElement("OperationPlanPhase");
			writer.addElement("operationPlanPhaseUid", rec.getOperationPlanPhaseUid());	
			
			try{
			if(rec.getPhaseCode().equals("") || StringUtils.isBlank(rec.getPhaseCode()))
			{
				if(rec.getPhaseName().equals("") || StringUtils.isBlank(rec.getPhaseName()))
					continue;
				else
					get_PhaseCode=rec.getPhaseName();
			}
			else
				get_PhaseCode=rec.getPhaseCode();
			}
			 catch(Exception e) {
			        System.out.println("failed to load" + e);
			    }
			Object[] paramsValues = {operationCode,get_PhaseCode};
			
			//translate short code to long code
			List<LookupPhaseCode> phaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupPhaseCode where (isDeleted is null or isDeleted = false) and operationCode=:operationUid and shortCode=:shortCode", paramsFields, paramsValues);
			for(LookupPhaseCode pcode: phaseCode)
			{
				phaseName=pcode.getName();
			}
			
			try{
			if(phaseName.equals("") && !rec.getPhaseName().equals("") || StringUtils.isBlank(phaseName))
				phaseName = rec.getPhaseName();
			}
			 catch(Exception e) {
			        System.out.println("failed to load " + e);
			    }
			
			writer.addElement("phaseCode", phaseName.toString());	
			writer.addElement("shortCode",rec.getPhaseCode());			
			String operationPlanPhaseUid = rec.getOperationPlanPhaseUid();			
			List<OperationPlanTask> operationPlanTask = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted is null or isDeleted = false) and operationPlanPhaseUid=:operationPlanPhaseUid", "operationPlanPhaseUid", operationPlanPhaseUid);
			
			for(OperationPlanTask rect: operationPlanTask){
				//translate short code to long code
				try{
				if(rect.getTaskCode().equals("") || StringUtils.isBlank(rect.getTaskCode()))
					get_TaskCode=rect.getTaskName();	
				else
					get_TaskCode=rect.getTaskCode();
				}
				 catch(Exception e) {
				        System.out.println("failed to load" + e);
				    }
				
				Object[] tparamsValues = {operationCode, get_TaskCode};
				List<LookupTaskCode> taskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupTaskCode where (isDeleted is null or isDeleted = false) and operationCode=:operationUid and shortCode=:shortCode", paramsFields, tparamsValues);
				for(LookupTaskCode tcode: taskCode)
				{
					taskName=tcode.getName();
				}
				
				try{
				if(taskName.equals("") && !rect.getTaskName().equals("") || StringUtils.isBlank(taskName))
					taskName = rect.getTaskName();
				}
				catch(Exception e) {
			        System.out.println("failed to load" + e);
			    }
				
				planTaskUid = rect.getOperationPlanTaskUid(); 
				//System.out.println("planTaskUid: "+planTaskUid);				
				writer.startElement("OperationPlanTask");
				writer.addElement("shortCode",rect.getTaskCode());
				writer.addElement("operationPlanPhaseUid", rect.getOperationPlanPhaseUid());
				writer.addElement("sequence", rect.getSequence()==null?"0":rect.getSequence().toString());
				writer.addElement("operationPlanTaskUid", rect.getOperationPlanTaskUid());
				writer.addElement("taskCode",  taskName.toString());
				writer.addElement("p50Duration", rect.getP50Duration()==null?"0":rect.getP50Duration().toString());
				writer.addElement("depthMdMsl", rect.getDepthMdMsl()==null?"0":rect.getDepthMdMsl().toString());	
				writer.addElement("description", rect.getDescription());
				
				//requirement section
				List<LookaheadRequirement> LookaheadRequirement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookaheadRequirement where (isDeleted is null or isDeleted = false) and operationPlanTaskUid=:operationPlanTaskUid  order by sequence", "operationPlanTaskUid",planTaskUid);
				for(LookaheadRequirement requirement: LookaheadRequirement)
				{
					writer.startElement("LookaheadRequirement");
					writer.addElement("sequence",requirement.getSequence()==null?"0":requirement.getSequence().toString());
					writer.addElement("requirement",requirement.getRequirement());
					writer.addElement("requirementUid",requirement.getLookaheadRequirementUid());
					writer.addElement("operationCode",taskName.toString());
					writer.addElement("operationPlanTaskUid",requirement.getOperationPlanTaskUid());					
					writer.endElement();
				}
				
				List<FileManagerFiles> FileManagerFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM FileManagerFiles WHERE (isDeleted = false or isDeleted is null) AND externalModule='LookaheadWebServiceCommandBean' AND externalKey1=:operationPlanTaskUid ORDER BY fileName", "operationPlanTaskUid", planTaskUid);				
				
				for (FileManagerFiles fileRec: FileManagerFiles) {
					File attachedFile = this.getAttachedFile(fileRec);
					writer.startElement("File_Manager");
					writer.addElement("fileUid", fileRec.getFileManagerFilesUid());
					writer.addElement("fileName", fileRec.getFileName());
					//writer.addElement("description", fileRec.getDescription());
					//writer.addElement("uploadedDateTime", fileRec.getUploadedDatetime().toString().substring(0, 10));
					writer.addElement("fileSize", String.valueOf(attachedFile.length()));
					writer.addElement("fileDescription", fileRec.getDescription());
					writer.addElement("filePublicPrivate", (fileRec.getIsPrivate() ? "Private" : "Public"));
					writer.addElement("uploadedByUserName", this.getUserName(fileRec.getUploadedUserUid()));
					writer.addElement("uploadedDatetime", (fileRec.getUploadedDatetime() != null ? String.valueOf(fileRec.getUploadedDatetime().getTime()) : ""));
					writer.endElement();
				}
				writer.endElement();
				
				//reset taskCode:
				taskName="";
			}
			writer.endElement();
		}
		
		List<LookaheadDayRequirement> LookaheadDayRequirement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookaheadDayRequirement where (isDeleted is null or isDeleted = false) and operationUid=:operationUid  order by sequence", "operationUid",operationUid);
		for(LookaheadDayRequirement dayrequirement: LookaheadDayRequirement)
		{
			writer.startElement("LookaheadDayRequirement");
			writer.addElement("sequence",dayrequirement.getSequence()==null?"0":dayrequirement.getSequence().toString());
			writer.addElement("requirement",dayrequirement.getRequirement());
			writer.addElement("dayRequirementUid",dayrequirement.getLookaheadDayRequirementUid());
			writer.addElement("attachDate",String.valueOf(dayrequirement.getDayDate().getTime()));			
			writer.endElement();
		}
		
		writer.close();
	}
	
	private void updateLookaheadRecord(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//String operationPlanPhaseUid = request.getParameter("operationPlanPhaseUid");
		UserSession userSession = UserSession.getInstance(request);
		String operationCode = userSession.getCurrentOperationType();
		String[] paramsFields = {"operationUid", "shortCode"};	
		String phaseShortCode="";
		String taskShortCode="";
				
		 Boolean isnewtask =false;
		 Boolean isnewreq = false;
		 Boolean isnewdayreq = false;
		 SimpleXMLElement data = SimpleXMLElement.loadXMLString(request.getParameter("xmldata"));
        
		 for(Iterator i = data.getChild().iterator(); i.hasNext(); ){
             SimpleXMLElement operationPlanPhase = (SimpleXMLElement) i.next();            
             operationPlanPhaseUid = operationPlanPhase.getAttribute("operationPlanPhaseUid");
             String getphaseCode = operationPlanPhase.getAttribute("ID");
             String getSequence = operationPlanPhase.getAttribute("seq");
             
             if(operationPlanPhase.getTagName()=="dailyRequirement")
             {
            	  for(Iterator doIterator = operationPlanPhase.getChild().iterator(); doIterator.hasNext(); ){
            		  SimpleXMLElement dailyRequirement = (SimpleXMLElement) doIterator.next(); 
            		  String dailyRequirementUid = dailyRequirement.getAttribute("dailyRequirementUid");
            		  LookaheadDayRequirement newDailyReq = (LookaheadDayRequirement) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookaheadDayRequirement.class, dailyRequirementUid);
            		  if (dailyRequirementUid.equals("0") || StringUtils.isBlank(dailyRequirementUid))
            			  isnewdayreq = true;
                      if (isnewdayreq) {
                    	  newDailyReq = new LookaheadDayRequirement();
                      }
                      
            		  for(Iterator loop = dailyRequirement.getChild().iterator(); loop.hasNext(); ){
                		  SimpleXMLElement dailyRequirementDetails = (SimpleXMLElement) loop.next(); 
                		                  		        			 
                          if(dailyRequirementDetails.getTagName().equals("req")){
                        	  newDailyReq.setRequirement(dailyRequirementDetails.getText());
                          }                          
                          else if(dailyRequirementDetails.getTagName().equals("attachDate")){
                        	  try {
                        		  
                        		  Date attachDate = CommonDateParser.parse(dailyRequirementDetails.getText());               
	   		              		  newDailyReq.setDayDate(attachDate);             			
	   		              		} catch(Exception ex) {
	   		              			Date newDate = new Date();
	   		              			newDailyReq.setDayDate(newDate);  
	   		              		}
                          }
                          else if(dailyRequirementDetails.getTagName().equals("reqSequence")){
                        	  newDailyReq.setSequence(dailyRequirementDetails.getText());
                          }
                          else if(dailyRequirementDetails.getTagName().equals("markDelete")){
                        	  if(Boolean.valueOf(dailyRequirementDetails.getText())==true)                        		  
                        		  newDailyReq.setIsDeleted(true);
                        	  else
                        		  newDailyReq.setIsDeleted(false);
                          }
            		  }
            		  try{
	              			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDailyReq);	              			
	              			isnewdayreq=false;
	              			
	  	              		}catch(Exception e){
	  	              			SimpleXmlResponse.send(response, false, "error", "UPDATE LOOKAHEAD DAILY REQUIREMENT - " + e.getMessage());
	  	              		} 
            		  
            	  } 
            	 
             }
             else{
             
             if(operationPlanPhaseUid.equals("0") || StringUtils.isBlank(operationPlanPhaseUid))
             {
            	OperationPlanPhase rec = new OperationPlanPhase();
            	
            	String[] paramsValues = {operationCode,getphaseCode};
    			//translate long code to short code
    			List<LookupPhaseCode> phaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupPhaseCode where (isDeleted is null or isDeleted = false) and operationCode=:operationUid and name=:shortCode", paramsFields, paramsValues);
    			for(LookupPhaseCode pcode: phaseCode)
    			{
    				phaseShortCode=pcode.getShortCode();
    			}
            	         		         		
         		rec.setPhaseCode(phaseShortCode.toString()); 
         		//set sequence #
         		try {
    			 	
    			 	Integer sequence = Integer.parseInt(getSequence); 
          			rec.setSequence(sequence);
          		} catch(Exception ex) {
          			rec.setSequence(0);
          		}
         		
         		try{
         			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
         			//SimpleXmlResponse.send(response, true);         			
         		}catch(Exception e){
         			SimpleXmlResponse.send(response, false, "error", "UPDATE OPERATION PLAN PHASE - " + e.getMessage());
         		}
         		savePhaseUid = rec.getOperationPlanPhaseUid();
             }
             else
             {
            	 savePhaseUid = operationPlanPhaseUid;
            	 //update sequence #
            	 OperationPlanPhase phaseRec = (OperationPlanPhase) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanPhase.class, savePhaseUid);
            	 try {
     			 	
     			 	Integer sequence = Integer.parseInt(getSequence); 
           			phaseRec.setSequence(sequence);
           		} catch(Exception ex) {
           			phaseRec.setSequence(0);
           		}
          		
          		try{
          			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(phaseRec);
          			//SimpleXmlResponse.send(response, true);         			
          		}catch(Exception e){
          			SimpleXmlResponse.send(response, false, "error", "UPDATE OPERATION PLAN PHASE - " + e.getMessage());
          		}
          		
          		//set all task to deleted. 
          		//List<OperationPlanTask> getPlanTask = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted is null or isDeleted = false) and operationPlanPhaseUid:savePhaseUid", "savePhaseUid", savePhaseUid);
          		List<OperationPlanTask> operationPlanTask = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted is null or isDeleted = false) and operationPlanPhaseUid=:savePhaseUid", "savePhaseUid", savePhaseUid);
          		for(OperationPlanTask theRecords: operationPlanTask)
     			{
     				theRecords.setIsDeleted(true);
     				try{
	              			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(theRecords); 
	              			
	              		}catch(Exception e){
	              			SimpleXmlResponse.send(response, false, "error", "SET TASK AS DELETED FAILED - " + e.getMessage());
	              		} 
     			}
          		
            	 
             }
            for(Iterator rowIterator = operationPlanPhase.getChild().iterator(); rowIterator.hasNext(); ){
                 SimpleXMLElement phaseCode = (SimpleXMLElement) rowIterator.next();  
                 
                for(Iterator fieldIterator = phaseCode.getChild().iterator(); fieldIterator.hasNext(); ){                	 
                	 SimpleXMLElement taskCode = (SimpleXMLElement) fieldIterator.next();                	
                	 String taskCodeUid = taskCode.getAttribute("codeUid");                	  
                	 LookaheadRequirement lookaheadReq = new LookaheadRequirement(); 
                	 OperationPlanTask Trec = (OperationPlanTask) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanTask.class, taskCodeUid);
                 	 
            		 if (taskCodeUid.equals("0") || StringUtils.isBlank(taskCodeUid))
            			 isnewtask = true;
                     if (isnewtask) 
                    	 Trec = new OperationPlanTask();
                    
                   if(!isnewtask)
                    Trec.setIsDeleted(false);
                     
                     SimpleXMLElement ReqXml = null;
            		 for(Iterator fieldIterator2 = taskCode.getChild().iterator(); fieldIterator2.hasNext(); ){                    	 
                    	 SimpleXMLElement thedata = (SimpleXMLElement) fieldIterator2.next();                    	
                    	 if(thedata.getTagName().equals("taskCode"))
                    	 {
                    		String lTaskCode = thedata.getText();
                    		Object[] tparamsValues = {operationCode, lTaskCode.toString()};
                 			//translate long code to short code
                 			List<LookupTaskCode> theTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupTaskCode where (isDeleted is null or isDeleted = false) and operationCode=:operationUid and name=:shortCode", paramsFields, tparamsValues);
                 			for(LookupTaskCode pcode: theTaskCode)
                 			{
                 				taskShortCode=pcode.getShortCode();
                 			}
 	                	 	Trec.setTaskCode(taskShortCode.toString());
                    	 }
 	                	 else if(thedata.getTagName().equals("p50Duration"))
 	                	 {
 		                	 try {
 		              			Double dp50Duration = Double.parseDouble(thedata.getText()); 
 		              			Trec.setP50Duration(dp50Duration);	              			
 		              		} catch(Exception ex) {
 		              			Trec.setP50Duration(0.0);
 		              		}
 	                	 }
 	                	 else if(thedata.getTagName().equals("depthMdMsl"))
 	                	 {	 
 	                		 try {
 			              			Double ddepthMdMsl = Double.parseDouble(thedata.getText()); 
 			              			Trec.setDepthMdMsl(ddepthMdMsl);
 			              		} catch(Exception ex) {
 			              			Trec.setDepthMdMsl(0.0);
 			              		}
 			               }
 	                	else if(thedata.getTagName().equals("description"))
	                	 {	 
	                		 Trec.setDescription(thedata.getText());
			               }
 	                	else if(thedata.getTagName().equals("sequence"))
	                	 {	 
	                		 
	                		 try {
	                			 	
	                			 	Integer sequence = Integer.parseInt(thedata.getText()); 
			              			Trec.setSequence(sequence);
			              		} catch(Exception ex) {
			              			Trec.setSequence(0);
			              		}
			               }
                    	 
                    	 if(thedata.getTagName().equals("requirement")){
                    		 String getReqXml = thedata.getXML();
                    		 ReqXml = SimpleXMLElement.loadXMLString(getReqXml); 
            		 	}
                     }
            		 	Trec.setOperationPlanPhaseUid(savePhaseUid);	                     
	                     try{
	              			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(Trec);	              			
	              			isnewtask=false;
	              			String savedTask = Trec.getOperationPlanTaskUid();
	              			
	              			for(Iterator fieldIterator3 = ReqXml.getChild().iterator(); fieldIterator3.hasNext(); ){   
	                    		 SimpleXMLElement theReqDet = (SimpleXMLElement) fieldIterator3.next();
	                    		 String requirementUid = theReqDet.getAttribute("requirementUid");
	                    		 lookaheadReq = (LookaheadRequirement) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookaheadRequirement.class, requirementUid);
	                    		 if (requirementUid.equals("0") || StringUtils.isBlank(requirementUid))
	                    			 isnewreq = true;
	                             if (isnewreq) 
	                            	 lookaheadReq = new LookaheadRequirement();
	                    		 for(Iterator fieldIterator4 = theReqDet.getChild().iterator(); fieldIterator4.hasNext(); ){
	                    			 SimpleXMLElement theReqRec = (SimpleXMLElement) fieldIterator4.next();		                    		    
		                    		// System.out.println("val: "+theReqRec.getText().toString()); 		                    		 		                    		  
		                             if(theReqRec.getTagName().equals("reqSequence"))
		                        	 {
		                            	 try {
		 	                			 	
		 	                			 	Integer reqSequence = Integer.parseInt(theReqRec.getText()); 
		 	                			 	lookaheadReq.setSequence(reqSequence);
			 			              		} catch(Exception ex) {
			 			              			lookaheadReq.setSequence(0);
			 			              		}		                    			 
		                        	 }		  
		                             else if(theReqRec.getTagName().equals("reqRequirement"))
		                        	 {
		                    			 lookaheadReq.setRequirement(theReqRec.getText().toString());
		                        	 }
		                             else if(theReqRec.getTagName().equals("markDelete")){
		                            	 if(Boolean.valueOf(theReqRec.getText())==true)                        		  
		                            		 lookaheadReq.setIsDeleted(true);
		                           	  	else
		                           	  		lookaheadReq.setIsDeleted(false);
		                             }
	                    		 }
	                    		 lookaheadReq.setOperationPlanTaskUid(savePhaseUid.toString()); 
	                    		 lookaheadReq.setOperationPlanTaskUid(savedTask.toString());
	 	              			//if success save task, save requirement	              			
	 	              			try{
	 	 	              			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookaheadReq);	              			
	 	 	              			isnewreq=false;
	 	 	              			
	 	 	              		}catch(Exception e){
	 	 	              			SimpleXmlResponse.send(response, false, "error", "UPDATE LOOKAHEAD REQUIREMENT - " + e.getMessage());
	 	 	              		} 
	                    	 }     
	              			
	              		}catch(Exception e){
	              			SimpleXmlResponse.send(response, false, "error", "UPDATE OPERATION PLAN TASK - " + e.getMessage());
	              		} 
                	 
                 }
             }            
		 }
		}
		 SimpleXmlResponse.send(response, true);
	}	
	
	private void deleteLookaheadPhaseRecord(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String operationPlanPhaseUid = request.getParameter("operationPlanPhaseUid");
		if(operationPlanPhaseUid == null){
			SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN PHASE - Missing required parameters");
			return;
		}
		OperationPlanPhase delphaserec = (OperationPlanPhase) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanPhase.class, operationPlanPhaseUid);
		if(delphaserec == null){
			SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN PHASE - Record not found");
			return;
		}
		try{
			delphaserec.setIsDeleted(true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(delphaserec);
			//SimpleXmlResponse.send(response, true);		
		}catch(Exception e){
			SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN PHASE - " + e.getMessage());
		}
		//delete from operation_plan_task table
		List<OperationPlanTask> operationPlanTask = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted is null or isDeleted = false) and operationPlanPhaseUid=:operationPlanPhaseUid", "operationPlanPhaseUid", operationPlanPhaseUid);
		if(operationPlanTask!=null)
		{
			for(OperationPlanTask delTrec: operationPlanTask){
				try{
					delTrec.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(delTrec);
					
				}catch(Exception e){
					SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN TASK - " + e.getMessage());
				}
			}
		
		}
		SimpleXmlResponse.send(response, true);
	}

	private void deleteLookaheadTaskRecord(HttpServletRequest request, HttpServletResponse response,String operationPlanPhaseUid) throws Exception {
	String operationPlanTaskUid = request.getParameter("operationPlanTaskUid");
		
		OperationPlanTask deltaskrec = (OperationPlanTask) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanTask.class, operationPlanTaskUid);
		if(deltaskrec == null){
			SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN TASK - Record not found");
			return;
		}
		try{
			deltaskrec.setIsDeleted(true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(deltaskrec);
			SimpleXmlResponse.send(response, true);
		}catch(Exception e){
			SimpleXmlResponse.send(response, false, "error", "DELETE OPERATION PLAN TASK - " + e.getMessage());
		}
	}
	private void getAllTaskData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		String doperationType = userSession.getCurrentOperationType();
		String doperationUid = userSession.getCurrentOperationUid();
		
		if(userSession.getCurrentDailyUid()==null)
			return;
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
		Date selectedDate = daily.getDayDate();
		String get_taskCode = "";
		String[] paramsFields = {"doperationType", "shortCode"};			
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		List<OperationPlanTask> operationPlanTask = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted is null or isDeleted = false) and ((taskCode IS NOT NULL and taskCode <> '') or (taskName IS NOT NULL and taskName <> '')) and operationUid=:doperationUid order by sequence", "doperationUid", doperationUid);
		for(OperationPlanTask rect: operationPlanTask){
			String doperationCode = rect.getTaskCode();
			Object[] paramsValues = {doperationType, doperationCode};
			String taskName = "";
			String planTaskUid="";
			get_taskCode = rect.getTaskCode();
			
			if(doperationCode.equals(""))
				doperationCode=rect.getTaskName();	
					
			//translate short code to long code			
			List<LookupTaskCode> taskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupTaskCode where (isDeleted is null or isDeleted = false) and operationCode=:doperationType and shortCode=:shortCode", paramsFields, paramsValues);
			
			for(LookupTaskCode tcode: taskCode)
			{
				taskName=tcode.getName();
			}
			
			if(taskName.equals("") && !rect.getTaskName().equals(""))
			{
				taskName = rect.getTaskName();
				get_taskCode = taskName;
			}
			
			writer.startElement("AllTaskData");
			writer.addElement("shortCode",get_taskCode);
			//writer.addElement("operationPlanPhaseUid", rect.getOperationPlanPhaseUid());
			writer.addElement("sequence", rect.getSequence()==null?"0":rect.getSequence().toString());
			writer.addElement("operationPlanTaskUid", rect.getOperationPlanTaskUid());
			writer.addElement("longCode",  taskName.toString());
			writer.addElement("duration",  rect.getP50Duration()==null?"0":rect.getP50Duration().toString());
			writer.addElement("description",  rect.getDescription());
			
			//get requirement section:
			planTaskUid = rect.getOperationPlanTaskUid();
			List<LookaheadRequirement> LookaheadRequirement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookaheadRequirement where (isDeleted is null or isDeleted = false) and operationPlanTaskUid=:operationPlanTaskUid order by sequence", "operationPlanTaskUid",planTaskUid);
			writer.startElement("LookaheadRequirement");
			for(LookaheadRequirement requirement: LookaheadRequirement)
			{
				writer.startElement("LookaheadRequirementDetails");
				writer.addElement("sequence",requirement.getSequence()==null?"0":requirement.getSequence().toString());				
				writer.addElement("requirement",requirement.getRequirement());
				writer.addElement("requirementUid",requirement.getLookaheadRequirementUid());
				writer.addElement("operationPlanTaskUid",requirement.getOperationPlanTaskUid());	
				writer.addElement("operationCode",taskName.toString());
				writer.endElement();
			}
			writer.endElement();
						
			List<FileManagerFiles> FileManagerFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM FileManagerFiles WHERE (isDeleted = false or isDeleted is null) AND externalModule='LookaheadWebServiceCommandBean' AND externalKey1=:operationPlanTaskUid ORDER BY fileName", "operationPlanTaskUid", planTaskUid);				
			writer.startElement("FileManager");
			for (FileManagerFiles fileRec: FileManagerFiles) {
				File attachedFile = this.getAttachedFile(fileRec);
				writer.startElement("FileManagerDetails");
				writer.addElement("fileUid", fileRec.getFileManagerFilesUid());
				writer.addElement("fileName", fileRec.getFileName());
				//writer.addElement("description", fileRec.getDescription());
				//writer.addElement("uploadedDateTime", fileRec.getUploadedDatetime().toString().substring(0, 10));
				writer.addElement("fileSize", String.valueOf(attachedFile.length()));
				writer.addElement("fileDescription", fileRec.getDescription());
				writer.addElement("filePublicPrivate", (fileRec.getIsPrivate() ? "Private" : "Public"));
				writer.addElement("uploadedByUserName", this.getUserName(fileRec.getUploadedUserUid()));
				writer.addElement("uploadedDatetime", (fileRec.getUploadedDatetime() != null ? String.valueOf(fileRec.getUploadedDatetime().getTime()) : ""));
				writer.endElement();
			}
			writer.endElement();// end of file_manager
			
			writer.endElement(); //end for AllTaskData
			
						
			//reset taskName
			taskName="";
		 }
		
		List<LookaheadDayRequirement> LookaheadDayRequirement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookaheadDayRequirement where (isDeleted is null or isDeleted = false) and operationUid=:operationUid  order by sequence", "operationUid",doperationUid);
		for(LookaheadDayRequirement dayrequirement: LookaheadDayRequirement)
		{
			writer.startElement("LookaheadDayRequirement");
			writer.addElement("sequence",dayrequirement.getSequence()==null?"0":dayrequirement.getSequence().toString());
			writer.addElement("requirement",dayrequirement.getRequirement());
			writer.addElement("dayRequirementUid",dayrequirement.getLookaheadDayRequirementUid());
			writer.addElement("attachDate",String.valueOf(dayrequirement.getDayDate().getTime()));				
			writer.endElement();
		}
		
		SimpleDateFormat flexClientSystemDateTimeFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
		List<Operation> getStartDate = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted is null or isDeleted = false) and operationUid=:doperationUid", "doperationUid", doperationUid);
		writer.startElement("Operation");
		for(Operation startDate:getStartDate)
		{
			writer.addElement("startDate",flexClientSystemDateTimeFormat.format(startDate.getStartDate()));
		}
		writer.endElement();
		
		writer.startElement("SelectedDate");
		writer.addElement("startDate",flexClientSystemDateTimeFormat.format(selectedDate));
		writer.endElement();
		writer.close();		
	}
	
	private String getUserName(String userUid) throws Exception {
		User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userUid);
		if(user == null) return "";
		return user.getUserName();
	}
	
	private File getAttachedFile(FileManagerFiles dbFile) throws Exception {
		return this.getAttachedFile(dbFile.getFileManagerFilesUid());	
	}

	private File getAttachedFile(String fileId) throws Exception {
		return new File(this.getFileManagerRootPath(), fileId);	
	}
	
	private File getFileManagerRootPath() throws Exception {
		return new File(ApplicationConfig.getConfiguredInstance().getFileManagerRootPath());
	}
	
	private void doDoc(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession currentUserSession = UserSession.getInstance(request);
		this.lDocModule.generateDocument(currentUserSession,request);
	}
	
	private void checkCurrentJobStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ReportJobStatus status = this.lDocModule.getReportJobStatus();

		if(status.getStatus() == ReportJobStatus.STATUS_NO_JOB){
			SimpleXmlResponse.send(response, true, "no_job", null);
		}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_DONE){
			SimpleXmlResponse.send(response, true, "job_done", null);
		}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_ERROR){
			SimpleXmlResponse.send(response, true, "job_error", status.getResponseMessage());
		}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_RUNNING){
			SimpleXmlResponse.send(response, true, "job_running", null);
		}else{
			SimpleXmlResponse.send(response, true, "job_unknown", null);
		}
	}
	
	private void loadDoc(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//String requestForDoc = request.getParameter("requestForDoc");
		//if (requestForDoc == null || requestForDoc == "") return;
		UserSession currentUserSession = UserSession.getInstance(request);
		List<LookaheadDocumentOutputFile> listDoc = this.lDocModule.listDocuments(currentUserSession,request);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		if (listDoc != null) {
			for (Object rec: listDoc) {
				writer.startElement("Document");
				Map fields = BeanUtils.describe(rec);
				for (Object entry:fields.entrySet()) {
					Map.Entry<String,Object> field = (Map.Entry)entry;
					writer.addElement(field.getKey(), nullToEmptyString(field.getValue()));
				}
				writer.endElement();
			}
		}
		writer.endElement();
		writer.close();			
	}	
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}
		
	private void downloadFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fileId = request.getParameter("fileUid");
		String outFilename = request.getParameter("outfile");
		if(StringUtils.isBlank(fileId)){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		File file = this.getAttachedFileViaDB(fileId);
		if(! file.exists()){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		FileManagerFiles dbFile = (FileManagerFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileManagerFiles.class, fileId);
		if(dbFile == null || (dbFile.getIsDeleted() != null && dbFile.getIsDeleted())){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		FileDownloadWriter.writeFileOutput(request, response, file, true, outFilename);
	}
	
	private File getAttachedFileViaDB(String fileId) throws Exception {
		FileManagerFiles dbFile = (FileManagerFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileManagerFiles.class, fileId);
		return this.getAttachedFileViaDB(dbFile);
	}
	
	private File getAttachedFileViaDB(FileManagerFiles dbFile) throws Exception {
		if(StringUtils.isNotBlank(dbFile.getFileFullPathName())){
			return new File(new File(this.getFileManagerRootPath(), dbFile.getFileFullPathName()), dbFile.getFileManagerFilesUid());
		}else{
			return new File(this.getFileManagerRootPath(), dbFile.getFileManagerFilesUid());
		}
	}
	
	
	
}

