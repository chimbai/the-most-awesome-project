<%@ page contentType="text/plain"%><%@ page import="com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%><%
String requestType = request.getParameter("type");
if (requestType=="menu") {
	LinkedHashMap<String, MenuItem> raw = new LinkedHashMap<String, MenuItem>();
	LinkedHashMap<String, MenuItem> processed = new LinkedHashMap<String, MenuItem>();
	List<String> list = new ArrayList<String>();
	List<MenuItem> result = ApplicationUtils
			.getConfiguredInstance()
			.getDaoManager()
			.find("From MenuItem Where (isDeleted is null or isDeleted = false) Order by beanName, parent, sequence, menuItemUid");
	for (MenuItem mi : result) {
%>"<%=mi.getMenuItemUid()%>","<%=mi.getParent()%>","<%=mi.getBeanName()%>","<%=mi.getLabel()%>"
<%
	}
} else {
	Map<String,String> jarFiles = ApplicationConfig.getConfiguredInstance().getLibsVersion();
	Map<String,String> buildId = ApplicationConfig.getConfiguredInstance().getLibsBundleId();
	for (Map.Entry<String,String> jar : jarFiles.entrySet()) {
%><%=jar.getKey()%>#<%=jar.getValue()%><%
		for (Map.Entry<String,String> id : buildId.entrySet()) {
			if(id.getKey()==jar.getKey()) {
				%>#<%=id.getValue()%><%
			}
		}
		%>
<%
	}
%>
<% } %>