<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%

	UserSession sessionx = UserSession.getInstance(request); 
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String wellboreUid = request.getParameter("wellboreUid");
	
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(wellboreUid))){
		String STRSQL = "UPDATE EdmSync SET isDeleted=true where idsWellboreUid=:wellboreUid";
		String[] params = new String[]{"wellboreUid"};
		Object[] values = new Object[]{wellboreUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	
%>

<br> Operation Name : 
<%=OperationName%>
<%
	String SQLSTR = "FROM EdmSync WHERE idsWellboreUid ='" + sessionx.getCurrentWellboreUid() + "' AND (isDeleted IS NULL OR isDeleted = 0)";
	List<EdmSync> edmSynclist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
	
	
	%>
	<form name="input" method="post">
	<input type="hidden" name="wellboreUid" value="<%=sessionx.getCurrentWellboreUid()%>" >
	<input type="submit" name="action" value="Delete" > 
	</form>	
	
	<table border="1" width="50%">
	<tr>
	<td>ids_table</td>
	<td>edm_object</td>
	</tr>
	<%
	
	for(EdmSync edmSync : edmSynclist) {
	%>
		<tr>
		<td><b><%=edmSync.getIdsTable()   %></b></td>
		<td><%=edmSync.getEdmObject() %></td>
		</tr>
		<%
		
		}
		%>

	</table>
	

</body>
</html>
