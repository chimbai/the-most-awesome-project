<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
com.idsdatanet.depot.witsml.rts.webservice.adapter.listener.DrillingParametersRTSListener,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
com.idsdatanet.depot.witsml.rts.RTSAdapterListener,
com.idsdatanet.depot.witsml.rts.RigStateData,
org.apache.commons.lang.StringUtils"
%><%
	try {
		int totalDeleted = 0;
		int totalRecord = 0;
		UserSession sessionx = UserSession.getInstance(request); 
		String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
		String sql = "FROM Activity WHERE (isDeleted is null or isDeleted = false) and dailyUid='" + sessionx.getCurrentDailyUid() + "' ORDER BY  startDatetime";
		List<Activity> acts = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if (acts.size()>0){
			totalRecord = acts.size();
		%>	<table border="1" width="100%">
			<tr>
			<td>Activity UID</td>
			<td>Start Time</td>
			<td>End Time</td>
			<td>FTR</td>
			<td></td>
			</tr><%

			for (Activity act : acts)
			{	

				%>
				<tr>
					<td><%=act.getActivityUid() %></td>
					<td><%=act.getStartDatetime() %></td>
					<td><%=act.getEndDatetime() %></td>
					<td><%=act.getActivityDescription() %></td>
					<td></td>
				</tr>
				<%
			}
			%></table><br><br><%
		}
		String sqlrsd = "FROM ActivityRsd WHERE (isDeleted is null or isDeleted = false) and dailyUid='" + sessionx.getCurrentDailyUid() + "' ORDER BY  startDatetime";
		List<ActivityRsd> actRsds = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlrsd);
		if (actRsds.size()>0){
			%>	<table border="1" width="100%">
			<tr>
			<td>ActivityRSD UID</td>
			<td>Activity UID</td>
			<td>Start Time</td>
			<td>End Time</td>
			<td>InterCode</td>
			<td></td>
			</tr><%
			
			for (ActivityRsd actrsd : actRsds)
			{	
				String activityRsdUid = actrsd.getActivityRsdUid();
				RigStateRaw rigState = (RigStateRaw)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigStateRaw.class, activityRsdUid);
				%>
				<tr>
					<td><%=actrsd.getActivityRsdUid() %></td>
					<td><%=actrsd.getActivityUid() %></td>
					<td><%=actrsd.getStartDatetime() %></td>
					<td><%=actrsd.getEndDatetime() %></td>
					<td><%=rigState.getRigStateId() %></td>
					<td></td>
				</tr>
				<%
			}
			%></table><br><br><%
			
		}
%>

Total deleted = <%=totalDeleted%> / <%=totalRecord%> <br>
<%=sessionx.getCurrentOperationUid()%> <br>
~ Done on x<%=OperationName%>
<%
	} catch (Exception e) {
		System.out.println(e.getMessage());
%>-- FAILED


<%
	}
%>