<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Preloader</title>
</head>
<body>
<iframe src="flash/filemanager/start" title="filemanager" width="100%" height="300"></iframe>
<iframe src="flash/costnet/flex/costdailysheet.start" title="costdailysheet" width="100%" height="300"></iframe>
<iframe src="flash/costnet/flex/costafemaster.start" title="costafemaster" width="100%" height="300"></iframe>
<iframe src="flash/visnetmapviewer/start" title="visnetmapviewer" width="100%" height="300"></iframe>
<iframe src="flash/lookahead/start" title="lookahead" width="100%" height="300"></iframe>
<iframe src="flash/adat/start" title="adat" width="100%" height="300"></iframe>
<iframe src="flash/wellexplorer/start" title="wellexplorer" width="100%" height="300"></iframe>
<iframe src="operationinformationcenter.html" title="generictemplate" width="100%" height="300"></iframe>
<iframe src="flash/inspection/start" title="inspection" width="100%" height="300"></iframe>
<iframe src="flash/unwantedevent/start" title="unwantedevent" width="100%" height="300"></iframe>
<iframe src="flash/lookupclean/start" title="lookupclean" width="100%" height="300"></iframe>
<iframe src="flash/tightholesetup/start" title="tightholesetup" width="100%" height="300"></iframe>
<iframe src="flash/login/start" title="login" width="100%" height="300"></iframe>
<iframe src="flash/hserelatedoperationsetup/start" title="hserelatedoperationsetup" width="100%" height="300"></iframe>
</body>
</html>