<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
java.text.DecimalFormat,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>

<%
	String groupUid = null;
	String groupsql = "FROM Group WHERE parentGroupUid IS NULL";
	List<Group> listG = ApplicationUtils.getConfiguredInstance().getDaoManager().find(groupsql);
	if (listG.size()>0) {
		Group g = (Group) listG.get(0);
		groupUid = g.getGroupUid();
	}

	String sql = "FROM DepotWitsmlLogRequest WHERE (isDeleted is null or isDeleted=false) and isActive=1 order by latestDateTimeIndex";
	List<DepotWitsmlLogRequest> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
	%>
<table border="1">
	<tr>
		<th>Well/Ops</th>
		<th>Log Start Index</th>
		<th>Log End Index</th>
		<th>Latest Time Index</th>
		<th>% Complete</th>
		<th>Last RSR</th>
	</tr>
	<%
	
	for(DepotWitsmlLogRequest dwlr : list) {
		String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, dwlr.getOperationUid());
		long startDateTimeIndex = 0;
		if (dwlr.getStartDateTimeIndex()!=null) startDateTimeIndex = dwlr.getStartDateTimeIndex().getTime();
		long endDateTimeIndex = 0;
		if (dwlr.getEndDateTimeIndex()!=null) endDateTimeIndex = dwlr.getEndDateTimeIndex().getTime();
		long latestDateTimeIndex = 0;
		if (dwlr.getLatestDateTimeIndex()!=null) latestDateTimeIndex = dwlr.getLatestDateTimeIndex().getTime();
		double complete = 0.0;
		if ((startDateTimeIndex>0) && (endDateTimeIndex>0)) {
			double x =  (double) (latestDateTimeIndex - startDateTimeIndex);
			double y = (double) (endDateTimeIndex - startDateTimeIndex);
			complete = (x / y) * 100;	
		}
		DecimalFormat nf = new DecimalFormat("#0.00");
		
		String activitysql = "SELECT MAX(endDatetime) FROM ActivityRsd WHERE (isDeleted is null or isDeleted=false) and operationUid='" +dwlr.getOperationUid()+ "'";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(activitysql);	
		Date thisLastDate = null;
		if (!lstResult.isEmpty())
		{
			Object b = (Object) lstResult.get(0);
			if (b != null) thisLastDate = (Date) b;
		}

	%>
		<tr>
			<td><%=operationName %></td>
			<td><%=dwlr.getStartDateTimeIndex() %></td>
			<td><%=dwlr.getEndDateTimeIndex() %></td>
			<td><%=dwlr.getLatestDateTimeIndex() %></td>
			<td><%=nf.format(complete) %></td>
			<td><%=thisLastDate %></td>
		</tr>

	<% 
		}
	
	
	
	%>

	
</table>
</body>
</html>
