<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
java.text.DecimalFormat,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.core.util.ClassUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.depot.goldbox.*,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"%>
<%
try{
	UserSession userSession = UserSession.getInstance(request, false);
	userSession.setCurrentDailyUid(null);
} catch (Exception x){
	%> 
	<script>		
		window.location = "login.jsp";
	</script> 
	<%
}
%>
<html>
<body>
<%
	UserSession sessionx = UserSession.getInstance(request); 

		
	String action = request.getParameter("action");
	String opsIndexUid = request.getParameter("opsIndex");
	
	if ((StringUtils.isNotEmpty(action)) && ("index".equals(action))){
		String hql = "delete from GoldboxAuditTransaction where goldboxIndexUid=:goldboxIndexUid";
		String[] paramName = {"goldboxIndexUid"};
		String[] paramValue = {opsIndexUid};
		// permanent delete
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, paramName, paramValue);
		%>
			DONE Delete
		<%
		GoldboxIndex goldbox = (GoldboxIndex) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(GoldboxIndex.class, opsIndexUid);
		if (goldbox !=null) {
			GoldboxManager.getConfiguredInstance().indexWellData(goldbox);
			%>
				DONE ReIndex
			<%
		}
	}
			
	
%>
<br>
<b>Reindex Gold Box</b><br> <br> 

<%
	String SQLSTR = "FROM GoldboxIndex where (isDeleted = false or isDeleted is null) order by wellName, wellboreName, operationName";
	List<GoldboxIndex> goldboxlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
	String opssel = "";
	if (goldboxlist.size()>0){
		for (GoldboxIndex gbox : goldboxlist) {
			String name = gbox.getWellName() + " > " + gbox.getWellboreName() + " > " + gbox.getOperationName();
			opssel = opssel + "<option value='"+ gbox.getGoldboxIndexUid() +"'>" + name + "</option>";	
		}
	}

	

%>
<form method="post">
									
	Re Index the gold box index
	<select name="opsIndex">
		<%=opssel%>
	</select>

	
	<input type="submit" name="action" value="index" > 
</form>	

</body>
</html>
