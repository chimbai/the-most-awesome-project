<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.depot.witsml.rts.*,
com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
com.idsdatanet.d2.core.web.mvc.UserSession,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
java.net.*,java.util.*,
org.apache.commons.lang.StringUtils"%>

<%
try {
	int totalDeleted = 0;
	int totalRecord = 0;
	
	String hql1 = "select activityExtCodeMappingUid, userDefActCode from ActivityExtCodeMapping where (isDeleted is null or isDeleted = false) and (isActive is null or isActive = true) " +
	"and (extCodeL6 is null or extCodeL6 ='') " +
	"and (extCodeL5 is null or extCodeL5 ='') " +
	"and (extCodeL4 is null or extCodeL4 ='') " +
	"and (extCodeL3 is null or extCodeL3 ='') " +
	"and (extCodeL2 is null or extCodeL2 ='') " +
	"and (extCodeL1 is null or extCodeL1 ='') ";

	String lv1 = "";
	String lv2 = "";
	String lv3 = "";
	String lv4 = "";
	String lv5 = "";
	String lv6 = "";

	LinkedHashMap<String, User> userMap = new LinkedHashMap<String, User>();
	String sqlstr2 = "FROM User where (isDeleted = false or isDeleted is null) ";
	List<User> ccllist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstr2);
	if (ccllist.size()>0) {
		for (User ccl : ccllist) {
			userMap.put(ccl.getUserUid(), ccl);
		}
		
	}
	
	UserSession sessionx = UserSession.getInstance(request); 
	List<Object[]> userdef = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql1);
	if (userdef.size()>0)
	{
		totalRecord = userdef.size();
		ArrayList list = new ArrayList();
		ArrayList list2 = new ArrayList();
		
		
		String uidd = null;
		String last_edit = "dbsqlupdate";
			
			int i = 0;
			for (Object[] act: userdef)
            {  
                String lists = act[1].toString();
                uidd = act[0].toString();
                lists = lists.subSequence(0, lists.length()).toString();
                String[] values = lists.split("_");
                
                    list.add(Arrays.toString(values));
                    list2.add(uidd);
                    
                    
                    Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ActivityExtCodeMapping.class, uidd);
                    ActivityExtCodeMapping extactsplit = (ActivityExtCodeMapping) obj;
                    
                    if (values.length >=6) extactsplit.setExtCodeL1(values[5]);
                    if (values.length >=5) extactsplit.setExtCodeL2(values[4]);
                    if (values.length >=4) extactsplit.setExtCodeL3(values[3]);
                    if (values.length >=3) extactsplit.setExtCodeL4(values[2]);
                    if (values.length >=2) extactsplit.setExtCodeL5(values[1]);
                    if (values.length >=1) extactsplit.setExtCodeL6(values[0]);
                    
                    extactsplit.setLastEditUserUid(sessionx.getCurrentUser().getUserUid());
                    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(extactsplit);
            }
	}
	
	%>
	Updated record = <%=totalRecord%> records 
	<br>
	<br>
	<a href="datafixActExt.jsp">Check total empty L1-L6 record In ActivityExtCodeMapping Table</a>
<%
}catch (Exception e) {
	System.out.println(e.getMessage());
%>-- FAILED


<%
}
%>

