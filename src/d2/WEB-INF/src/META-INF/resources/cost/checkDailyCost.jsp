<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
java.text.DecimalFormat,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%
	UserSession sessionx = UserSession.getInstance(request); 
	Map<String, Double> conversionRates = new HashMap<String, Double>();
	
	List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup c, OperationAfe a " +
			"where (c.isDeleted = false or c.isDeleted is null) " +
			"and (a.isDeleted = false or a.isDeleted is null) " +
			"and c.costAfeMasterUid=a.costAfeMasterUid " +
			"and a.operationUid=:operationUid " +
			"order by c.isPrimaryCurrency desc", "operationUid", sessionx.getCurrentOperationUid());
	for (Object[] rec : rs ) {
		CostCurrencyLookup costCurrencyLookup = (CostCurrencyLookup) rec[0];
		if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
			if (costCurrencyLookup.getConversionRate()>0.0) {
				conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
			}
		}
	}
	
	
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String costDailysheetUid = request.getParameter("costDailysheetUid");
	
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(costDailysheetUid))){
		String STRSQL = "UPDATE CostDailysheet SET isDeleted=false where costDailysheetUid=:costDailysheetUid";
		String[] params = new String[]{"costDailysheetUid"};
		Object[] values = new Object[]{costDailysheetUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Accont Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET parentAccountCode=:parentAccountCode";
		String[] params = new String[]{"parentAccountCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Ops Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET operationCode=:operationCode";
		String[] params = new String[]{"operationCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}		
			
	
%>
<b>Check Daily cost data</b><br> 
<br> Operation Name : <%=OperationName%>

<%
	String SQLSTR = "FROM ReportDaily where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and reportType!='DGR' order by reportDatetime";
	List<ReportDaily> rdlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SQLSTR, "operationUid", sessionx.getCurrentOperationUid());


	%>
	
	<table border="1" width="100%">
	<tr>
		<td width="15%">Date</td>
		<td width="15%">Cost (On ReportDaily)</td>
		<td width="15%">Cost (On Recal)</td>
		<td width="15%">Cost (On CostDaily)</td>
		<td width="5%">1=2</td>
		<td width="5%">1=3</td>
		<td width="5%">2=3</td>
	</tr>
	<%
	Double totalrd = 0.0;
	Double totalrel = 0.0;
	Double totalcd = 0.0;
	DecimalFormat df = new DecimalFormat("#,##0.00");
	DecimalFormat df2 = new DecimalFormat("#0.00");
	
	for(ReportDaily rd : rdlist) {
		
		Double dayCost = 0.0;
		Double dayCostdb = 0.0;
		Double costDaily = rd.getDaycost();
		String dailyUid = rd.getDailyUid();
		
		String queryString = "SELECT currency, sum((coalesce(itemCost,0.0) * coalesce(quantity,0.0))), sum((coalesce(itemTotal,0.0))) FROM CostDailysheet " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND dailyUid=:dailyUid " +
				"Group By currency ";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", dailyUid);
		for (Object result : list) {
			Object[] rec = (Object[]) result;
			if (rec[1]!=null) {
				//calculate conversion rate
				Double thisTotal = Double.parseDouble(rec[1].toString());
				Double thisTotaldb = Double.parseDouble(rec[2].toString());
				if (rec[0]!=null) {
					if (conversionRates.containsKey(rec[0].toString())) {
						Double conversionRate = conversionRates.get(rec[0].toString());
						thisTotal = Math.round(thisTotal / conversionRate * 100.0) / 100.0;
					}
				}
				if (dayCost==null) dayCost = 0.0;
				dayCost += thisTotal;
				dayCostdb += thisTotaldb;
			}
		}
		
		String color1 = "white";
		String color2 = "white";
		String color3 = "white";
		Boolean c1 = true;
		Boolean c2 = true;
		Boolean c3 = true;
		Double xx = 0.0;
		if (costDaily!=null) xx = costDaily;

		String v1 = df2.format(xx);
		String v2 = df2.format(dayCost);
		String v3 = df2.format(dayCostdb);
			
		totalrd += Double.parseDouble(v1);
		totalrel += Double.parseDouble(v2);
		totalcd += Double.parseDouble(v3);
		
		if (Double.compare(Double.parseDouble(v1), Double.parseDouble(v2)) == 0) { 
			
		}else{
			c1 = false;
			color1 = "yellow";
		}
		
		if (Double.compare(Double.parseDouble(v1), Double.parseDouble(v3)) == 0) { 
			
		}else{
			c2 = false;
			color2 = "green";
		}
		
		if (Double.compare(Double.parseDouble(v2), Double.parseDouble(v3)) == 0) { 
			
		}else{
			c3 = false;
			color3 = "grey";
		}
	
		String x1 = df.format(xx);
		String x2 = df.format(dayCost);
		String x3 = df.format(dayCostdb);
	%>
		<tr>
			<td width="15%"><%=rd.getReportDatetime() %></td>
			<td width="15%"><%=x1 %></td>
			<td width="15%"><%=x2 %></td>
			<td width="15%"><%=x3 %></td>
			<td width="5%" style="background-color:<%=color1%>"><%=c1 %></td>
			<td width="5%" style="background-color:<%=color2%>"><%=c2 %></td>
			<td width="5%" style="background-color:<%=color3%>"><%=c3 %></td>
		</tr>


	<%
	}
	%>
		<tr>
			<td width="15%"><b>Total</b></td>
			<td width="15%"><b><%=df.format(totalrd)%></b></td>
			<td width="15%"><b><%=df.format(totalrel)%></b></td>
			<td width="15%"><b><%=df.format(totalcd)%></b></td>
		</tr>
	</table>
	

</body>
</html>
