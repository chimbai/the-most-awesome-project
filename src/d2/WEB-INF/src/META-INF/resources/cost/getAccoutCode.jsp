<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%
	LinkedHashMap<String, User> userMap = new LinkedHashMap<String, User>();
	String sqlstr2 = "FROM User where (isDeleted = false or isDeleted is null) ";
	List<User> ccllist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstr2);
	if (ccllist.size()>0) {
		for (User ccl : ccllist) {
			userMap.put(ccl.getUserUid(), ccl);
		}
		
	}
	
	UserSession sessionx = UserSession.getInstance(request); 
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String costDailysheetUid = request.getParameter("costDailysheetUid");
	
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(costDailysheetUid))){
		String STRSQL = "UPDATE CostDailysheet SET isDeleted=false where costDailysheetUid=:costDailysheetUid";
		String[] params = new String[]{"costDailysheetUid"};
		Object[] values = new Object[]{costDailysheetUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Accont Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET parentAccountCode=:parentAccountCode";
		String[] params = new String[]{"parentAccountCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Ops Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET operationCode=:operationCode";
		String[] params = new String[]{"operationCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}		
			
	
%>
<b>Cost Account Code data</b><br> 
<br> Operation Name : <%=OperationName%>
	<form name="input" method="post">
		<input type="submit" name="action" value="Clear All Accont Code" > 
	</form>
	<form name="input" method="post">
		<input type="submit" name="action" value="Clear All Ops Code" > 
	</form>
<%
	String SQLSTR = "FROM CostAccountCodes where (isDeleted = false or isDeleted is null) order by parentAccountCode, accountCode";
	List<CostAccountCodes> caclist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
	
	
	%>
	
	<table border="1" width="100%">
	<tr>
		<td width="15%">Account Code</td>
		<td width="15%">Sub Code</td>
		<td width="25%">Description</td>
		<td width="10%">Tangible?</td>
		<td width="10%">Category</td>
		<td width="25%">Comment</td>
	</tr>
	<%
	for(CostAccountCodes cas : caclist) {
	%>
		<tr>
			<td width="15%"><%=cas.getParentAccountCode() %></td>
			<td width="15%"><%=cas.getAccountCode() %></td>
			<td width="25%"><%=cas.getShortDescription() %></td>
			<td width="10%">
			<% if (BooleanUtils.isTrue(cas.getIsTangible())){ %>
				Yes
			<% } else { %>
				No
			<% }%>
			</td>
			<td width="10%"><%=cas.getCategory() %></td>
			<td width="25%"><%=cas.getLongDescription() %></td>
		</tr>


	<%
	}
	%>
	</table>
	

</body>
</html>
