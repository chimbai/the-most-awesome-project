<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%><%
	try {
		int totalDeleted = 0;
		int totalRecord = 0;
		UserSession sessionx = UserSession.getInstance(request); 
		String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
		LinkedHashMap<String, CostCurrencyLookup> currencyMap = new LinkedHashMap<String, CostCurrencyLookup>();
		
		String sqlstr2 = "FROM CostCurrencyLookup where (isDeleted = false or isDeleted is null) " +
				"AND costAfeMasterUid IN (" +
				"SELECT c.costAfeMasterUid from CostAfeMaster c, OperationAfe a " +
				"where (c.isDeleted = false or c.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and c.costAfeMasterUid=a.costAfeMasterUid " +
				"and a.operationUid='"+sessionx.getCurrentOperationUid()+"' )";

		List<CostCurrencyLookup> ccllist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstr2);
		if (ccllist.size()>0) {
			for (CostCurrencyLookup ccl : ccllist) {
				currencyMap.put(ccl.getCurrencySymbol(), ccl);
				//currencyMap
			}
			
		}
		
		String sqlstr = "FROM CostAfeMaster where (isDeleted = false or isDeleted is null) " +
				"AND costAfeMasterUid IN (" +
				"SELECT costAfeMasterUid from OperationAfe " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"and operationUid=:operationUid )";
		%>
		Recalculate AFE MASTER TOTAL
		<br>
		<% 
		List<CostAfeMaster> camlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlstr, "operationUid", sessionx.getCurrentOperationUid());
		%>
		<form name="input" method="post">
			<input type="submit" name="action" value="Fix All" > 
		</form>	
		<table border="1">
			<tr><th>AFE</th>
				<th width="5%">..</th></tr>
		<% 
		if (camlist.size()>0){
			for (CostAfeMaster cam : camlist){
			
				%>
				
				<tr><td><%=cam.getAfeNumber()%></td>
					<td><form name="input" method="post">
							<input type="hidden" name="costMasterUid" value="<%=cam.getCostAfeMasterUid()%>" >
							<input type="submit" name="action" value="Fix" > 
						</form>	
				
				</td></tr>
				<%
				
			}
		}
		%>
		</table>
		<%
		String action = request.getParameter("action");
		String costMasterUid = request.getParameter("costMasterUid");
		String fixUid = "";
		if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(costMasterUid))){
			fixUid = costMasterUid;
		}
		if ((StringUtils.isNotEmpty(action)) && ("Fix All".equals(action))){
			fixUid = "all";
		}
		int count = 0;
		%>
		<br>
		Status : fix <%=fixUid %>
		<%
		
		if (camlist.size()>0){
			for (CostAfeMaster cam : camlist){
				String costMUid = cam.getCostAfeMasterUid();
				if ((fixUid.equals(costMUid)) || (fixUid.equals("all"))){
					String sqlstrx = "FROM CostAfeDetail where (isDeleted = false or isDeleted is null) and costAfeMasterUid='" + costMUid + "'";
					List<CostAfeDetail> cadlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstrx);
					if (cadlist.size()>0){
						for (CostAfeDetail cad : cadlist){
							Double rate = 1.0;
							if (currencyMap.containsKey(cad.getCurrency())) {
								rate = currencyMap.get(cad.getCurrency()).getConversionRate();
							}
							Double qty = 0.0;
							Double icost = 0.0;
							Double eday = 1.0;
							if (cad.getQuantity()!=null) qty = cad.getQuantity();
							if (cad.getItemCost()!=null) icost = cad.getItemCost();
							if (cad.getEstimatedDays()!=null) eday = cad.getEstimatedDays();
							Double total =  (qty * icost * eday) / rate;
							cad.setItemTotal(total);
							count += 1;
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cad);
						}
					}	
				}
			}	
		}

%>


<br>
<br>
~ Done on <%=OperationName%> <br>
~ Fixed  <%=count%>
<%
	} catch (Exception e) {
		System.out.println(e.getMessage());
%>-- FAILED


<%
	}
%>