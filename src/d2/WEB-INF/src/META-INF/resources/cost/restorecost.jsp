<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%
	LinkedHashMap<String, User> userMap = new LinkedHashMap<String, User>();
	String sqlstr2 = "FROM User where (isDeleted = false or isDeleted is null) ";
	List<User> ccllist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstr2);
	if (ccllist.size()>0) {
		for (User ccl : ccllist) {
			userMap.put(ccl.getUserUid(), ccl);
		}
		
	}
	
	UserSession sessionx = UserSession.getInstance(request); 
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String costDailysheetUid = request.getParameter("costDailysheetUid");
	
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(costDailysheetUid))){
		String STRSQL = "UPDATE CostDailysheet SET isDeleted=false where costDailysheetUid=:costDailysheetUid";
		String[] params = new String[]{"costDailysheetUid"};
		Object[] values = new Object[]{costDailysheetUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Restore All".equals(action))){
		String STRSQL = "UPDATE CostDailysheet SET isDeleted=false where dailyUid=:dailyUid";
		String[] params = new String[]{"dailyUid"};
		Object[] values = new Object[]{sessionx.getCurrentDailyUid()};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
			
			
	
%>
<b>ReStore Daily cost data</b><br> 
<br> Operation Name : <%=OperationName%>
	<form name="input" method="post">
		<input type="submit" name="action" value="Restore All" > 
	</form>
<%
	String SQLSTR = "FROM CostDailysheet where dailyUid='" + sessionx.getCurrentDailyUid() + "' and isDeleted=true";
	List<CostDailysheet> costlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
	
	
	%>
	
	<table border="1" width="100%">
	<tr>
	<td width="15%">Account Code</td>
	<td>Description</td>
	<td width="10%">Item Cost</td>
	<td width="10%">Deleted Date</td>
	<td width="10%">Deleted User</td>
	<td width="5%">Action</td>
	</tr>
	<%
	for(CostDailysheet cds : costlist) {
	%>
		<tr>
			<td ><b><%=cds.getAccountCode()%></b></td>
			<td><%=cds.getAfeItemDescription()%></td>
			<td><%=cds.getItemCost()%></td>
			<td ><%=cds.getLastEditDatetime()%></td>
			<td><%=userMap.get(cds.getLastEditUserUid()).getUserName()  %></td>
			
			<td>
				<form name="input" method="post">
					<input type="hidden" name="costDailysheetUid" value="<%=cds.getCostDailysheetUid()  %>" >
					<input type="submit" name="action" value="Restore" > 
				</form>	
			</td>
		</tr>


	<%
	}
	%>
	</table>
	

</body>
</html>
