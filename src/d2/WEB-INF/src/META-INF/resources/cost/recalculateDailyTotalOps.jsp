<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%><%
	try {
		int totalDeleted = 0;
		int totalRecord = 0;
		UserSession sessionx = UserSession.getInstance(request); 
		String currentOps = sessionx.getCurrentOperationUid();
		String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
		LinkedHashMap<String, CostCurrencyLookup> currencyMap = new LinkedHashMap<String, CostCurrencyLookup>();
		
		String sqlstr2 = "FROM CostCurrencyLookup where (isDeleted = false or isDeleted is null) " +
				"AND costAfeMasterUid IN (" +
				"SELECT c.costAfeMasterUid from CostAfeMaster c, OperationAfe a " +
				"where (c.isDeleted = false or c.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and c.costAfeMasterUid=a.costAfeMasterUid " +
				"and a.operationUid='"+sessionx.getCurrentOperationUid()+"' )";

		List<CostCurrencyLookup> ccllist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlstr2);
		if (ccllist.size()>0) {
			for (CostCurrencyLookup ccl : ccllist) {
				currencyMap.put(ccl.getCurrencySymbol(), ccl);
			}
			
		}
		
		
		%>
		Recalculate Daily Cost TOTAL (By Operation)
		<br>
		<% 
		String daySql = "FROM ReportDaily WHERE operationUid = :operationUid AND (isDeleted IS NULL OR isDeleted = '') order by reportDatetime";
		List<ReportDaily> daylist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(daySql, "operationUid", currentOps );
		
		if (daylist.size()>0){
			for (ReportDaily rd : daylist){
				String currentDaily = rd.getDailyUid();
				String sqlstr = "FROM CostDailysheet where (isDeleted = false or isDeleted is null) " +
						"and dailyUid=:dailyUid )";
				List<CostDailysheet> camlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlstr, "dailyUid", currentDaily);
				int rcount = camlist.size();
				int count = 0;
				if (camlist.size()>0){
					for (CostDailysheet cam : camlist){
						Double rate = 1.0;
						if (currencyMap.containsKey(cam.getCurrency())) {
							rate = currencyMap.get(cam.getCurrency()).getConversionRate();
						}
						Double qty = 0.0;
						Double icost = 0.0;
						if (cam.getQuantity()!=null) qty = cam.getQuantity();
						if (cam.getItemCost()!=null) icost = cam.getItemCost();
						Double total =  (qty * icost) / rate;

						if (Double.compare(total, cam.getItemTotal()) == 0) { 
							
						}else{
							cam.setItemTotal(total);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cam);
							count ++;
						}
					}
				}
				if (count>0){
					CommonUtil.getConfiguredInstance().updateDailyCostFromCostNet(currentDaily);
				}
				%>
				<br> Date: <%=rd.getReportDatetime() %>  Update: <%=count%> / <%=rcount%>
				
				<%
			}
		}


%>
<br>

<br>
<%=sessionx.getCurrentOperationUid()%> <br>
~ Done on x<%=OperationName%>
<%
	} catch (Exception e) {
		System.out.println(e.getMessage());
%>-- FAILED


<%
	}
%>