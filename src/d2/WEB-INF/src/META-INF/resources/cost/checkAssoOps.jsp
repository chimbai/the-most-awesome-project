<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
java.text.DecimalFormat,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%
	UserSession sessionx = UserSession.getInstance(request); 
	Map<String, Double> conversionRates = new HashMap<String, Double>();
	
	List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup c, OperationAfe a " +
			"where (c.isDeleted = false or c.isDeleted is null) " +
			"and (a.isDeleted = false or a.isDeleted is null) " +
			"and c.costAfeMasterUid=a.costAfeMasterUid " +
			"and a.operationUid=:operationUid " +
			"order by c.isPrimaryCurrency desc", "operationUid", sessionx.getCurrentOperationUid());
	for (Object[] rec : rs ) {
		CostCurrencyLookup costCurrencyLookup = (CostCurrencyLookup) rec[0];
		if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
			if (costCurrencyLookup.getConversionRate()>0.0) {
				conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
			}
		}
	}
	
	Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, sessionx.getCurrentDailyUid());
	Date todayDate = null;
	if (daily!=null) todayDate = daily.getDayDate();
	
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String costDailysheetUid = request.getParameter("costDailysheetUid");
	
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(costDailysheetUid))){
		String STRSQL = "UPDATE CostDailysheet SET isDeleted=false where costDailysheetUid=:costDailysheetUid";
		String[] params = new String[]{"costDailysheetUid"};
		Object[] values = new Object[]{costDailysheetUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Accont Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET parentAccountCode=:parentAccountCode";
		String[] params = new String[]{"parentAccountCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}
	if ((StringUtils.isNotEmpty(action)) && ("Clear All Ops Code".equals(action))){
		String STRSQL = "UPDATE CostAccountCodes SET operationCode=:operationCode";
		String[] params = new String[]{"operationCode"};
		Object[] values = new Object[]{""};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL, params, values);
	}		
			
	
%>
<b>Check Associated Operation Cost Data</b><br> 
<br>Date : <%=todayDate%>

<%
	List OpsList = new ArrayList();
	OpsList = CommonUtil.getConfiguredInstance().getAssociatedOpsList(sessionx.getCurrentOperationUid());

	String SQLSTR = "FROM ReportDaily where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and reportType!='DGR' order by reportDatetime";
	List<ReportDaily> rdlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SQLSTR, "operationUid", sessionx.getCurrentOperationUid());


	%>
	
	<table border="1" width="100%">
		<tr>
			<td width="30%">Operation</td>
			<td width="30%">Cost todate</td>
			<td width="40%">.</td>
		</tr>
	<% 
		for (Object ops: OpsList) {
				String opsUid = ops.toString();
				String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and dayDate<=:dayDate and operationUid=:operationUid order by dayDate desc";
				List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, 
						new String[] {"dayDate","operationUid"}, new Object[] {todayDate, opsUid});
				Double opsTotal = 0.0;
				String OpsName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), opsUid);
				
				if (dailyList.size()>0) {
					String getDailyUid = dailyList.get(0).getDailyUid();
					opsTotal = CommonUtil.getConfiguredInstance().calculateCummulativeCostOps(getDailyUid, null);

				}
				%>
				<tr>
					<td width="30%"><%=OpsName %></td>
					<td width="30%"><%=opsTotal %></td>
					<td width="40%">.</td>
				</tr>
				
				<%
		}
	%>
	</table>
	

</body>
</html>
