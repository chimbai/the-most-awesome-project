<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" import="java.net.URLEncoder, java.util.Calendar, java.util.GregorianCalendar, java.util.List, com.idsdatanet.d2.common.i18n.MultiLangUtils, com.idsdatanet.d2.core.web.mvc.ApplicationConfig, com.idsdatanet.d2.core.web.mvc.UserSession, com.idsdatanet.d2.infra.system.security.saml.SAMLUtils" %>
<%
	if(!Boolean.TRUE.equals(request.getAttribute("com.idsdatanet.d2.login.forward"))){
		response.sendRedirect("login.html");
		return;
	}

	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-store");

	String defaultLanguage = "en";
	boolean skipCaptcha = false;
	boolean samlEnabled_ids = SAMLUtils.isSAMLEnabled(request, "ids");
	boolean samlEnabled_default = SAMLUtils.isSAMLEnabled(request, "default");
			
	Cookie[] cookies = request.getCookies();
	if(cookies != null) {
		for(int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if("skipCaptcha".equals(cookie.getName())){
				skipCaptcha = "1".equals(cookie.getValue());
			}else if("storedLanguage".equals(cookie.getName())){
				defaultLanguage = cookie.getValue();
			}
		}
	}
	
	pageContext.setAttribute("clientLogoPath", "images/login/client-logo.png");
	pageContext.setAttribute("clientLogoLabel", "IDS Datanet 2.5");
	pageContext.setAttribute("csrfToken", UserSession.getCSRFToken(request));
	pageContext.setAttribute("defaultLanguage", defaultLanguage);
	pageContext.setAttribute("showCaptcha", ApplicationConfig.getConfiguredInstance().isCaptchaEnabled() && !skipCaptcha);
	pageContext.setAttribute("showSAMLLogin_ids", samlEnabled_ids);
	pageContext.setAttribute("showSAMLLogin_default", samlEnabled_default);
	pageContext.setAttribute("samlLoginLabel_ids", com.idsdatanet.d2.infra.system.security.saml.SAMLLabelManager.getLoginLabelIDSMobile("Login with IDS SAML"));
	pageContext.setAttribute("samlLoginLabel_default", com.idsdatanet.d2.infra.system.security.saml.SAMLLabelManager.getLoginLabelDefaultMobile("Login with SAML"));
	pageContext.setAttribute("v_resources_path", request.getContextPath() + "/vresources/cache/" + URLEncoder.encode(ApplicationConfig.getConfiguredInstance().getSystemStartupRandomValue(), "utf-8") + "/htmllib");
	
    Exception e = (Exception) session.getAttribute(org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION);
	if(e != null){
	    session.removeAttribute(org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION);
		String error_msg = "Invalid username and/or password, please try again.";
		if(e instanceof com.idsdatanet.d2.core.web.security.v2.UserGroupNotFoundException){
			error_msg = "Invalid user group definition, please contact support@idsdatanet.com";
		}else if(e instanceof org.springframework.security.authentication.AccountExpiredException){
			error_msg = "Your user account has expired. Please contact support@idsdatanet.com if login to the system using this account is required.";
		}else if(e instanceof org.springframework.security.authentication.LockedException){
			error_msg = "Account locked, please contact support@idsdatanet.com";
		}else{
			error_msg = "Login failed. " + e.getMessage();
		}
		pageContext.setAttribute("error_msg", error_msg);
	}
	
	boolean showUsernameLoginForm = true;
	if(e != null){
		showUsernameLoginForm = !Boolean.TRUE.equals(session.getAttribute("com.idsdatanet.d2.login.saml.failed"));
		session.removeAttribute("com.idsdatanet.d2.login.saml.failed");
	}else{
		showUsernameLoginForm = !samlEnabled_ids && !samlEnabled_default;
	}
	pageContext.setAttribute("showUsernameLoginForm", showUsernameLoginForm);
	pageContext.setAttribute("showHomeButton", showUsernameLoginForm && (samlEnabled_ids || samlEnabled_default));
	
	List<String> languages = MultiLangUtils.getConfiguredInstance().getLanguages();
	if(languages != null && languages.size() > 0) {
		pageContext.setAttribute("languageOptions", languages);
	}
%>
<!DOCTYPE html>
<html>
<head>
	<title>IDS DataNet 2.5 Mobile</title>
	<meta charset="utf-8">
    <meta name="description" content="IDS DataNet 2.5 Mobile">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="msapplication-config" content="images/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="manifest" href="images/favicon/site.webmanifest">
	<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="stylesheet" type="text/css" href="<c:out value="${v_resources_path}"/>/script/bootstrap413/css/bootstrap.min.css">
	<script type="text/javascript">
		function submitLoginForm(){
			var i = document.getElementById("login_form_client_url");
			if(i){
				i.value = document.URL;
			}
			return true;
		}
		function doSAMLLogin(alias){
			var userLanguage = "";
			var dd = document.getElementById("userLanguage");
			if(dd) userLanguage = dd.value;
			window.location="<c:out value='${pageContext.request.contextPath}'/>/saml/login?myProviderAlias=" + encodeURIComponent(alias) + "&userLanguage=" + encodeURIComponent(userLanguage);			
		}
		function showElem(id, isShow){
			var e = document.getElementById(id);
			if(e) e.style.display = isShow ? "" : "none";
		}
		function showUsernameLoginForm(v){
			showElem("homeBtn", v);
			showElem("UsernameLoginForm", v);
			showElem("SAMLLoginForm", !v);
			showElem("LoginError", false);
		}
	</script>
	<style type="text/css">
		#options-bar {
			font-size: 11px !important;
			line-height: 24px;
			color: #666;
		}
		#username_input {
		  opacity: 1;
		  transition: 0.9s opacity;
		}
		#password_input {
		  opacity: 1;
		  transition: 0.9s opacity;
		}
		#captcha_image {
		  opacity: 1;
		  transition: 0.9s opacity;
		}
		#captcha_input {
		  opacity: 1;
		  transition: 0.9s opacity;
		}
		#idsLoginBtn {
		  opacity: 1;
		  transition: 0.9s opacity;
		}
		.LoginError{
			color: #FFFFFF;
			background: url(images/alertbad_icon.gif) #c00 left no-repeat;
			font-size: 14px;
			padding: 6px 5px 6px 30px;
		}
		#UsernameLoginForm label{
			color: #878787;
		}
		.D2Link{
		    display: inline-block;
		    color: rgba(55,83,181,1) !important;
		    text-decoration: none !important;
    	}
    	.btn{
    	    width: 100%;
    		font-size: 14px;
    		padding: 15px;
    		border: 0;
    		margin-bottom: 10px;
    	}
	</style>
</head>
<body class="bg-white">
	<div class="align-content-center flex-wrap container" style="margin: 8vh auto 10px; max-width: 540px; padding: 0 30px">
		<div id="homeBtn" style="<c:if test="${!showHomeButton}">display:none;</c:if>padding-bottom: 20px;" onclick="showUsernameLoginForm(false)">
			<img src="images/favicon/back-button.png" style="width:30px; height:30px;"></img>
		</div>
		<center><img id="d25logo" src="images/login/datanet2.5-logo.png" style="max-width: 100%"/></center>
		<center><img src="<c:out value='${clientLogoPath}'/>" alt="<c:out value='${clientLogoLabel}'/>" border="0" style="max-width:100%;max-height:250px;padding-top:20px"/></center>
		<c:if test="${!empty error_msg}">
           	<div class="LoginError" id="LoginError"><c:out value="${error_msg}"/></div>
		</c:if>
        <div style="padding-top: 20px;">
           	<form name="form" method="post" action="j_security_check" onSubmit="submitLoginForm()">
				<div id="UsernameLoginForm" style="<c:if test="${!showUsernameLoginForm}">display:none;</c:if>">
                  	<input name="csrfToken" value="<c:out value="${csrfToken}"/>" type="hidden"/>
					<input id="login_form_client_url" type="hidden" name="login_form_client_url" value=""/>
					<div class="form-group" id="username_input">
                    	<label>USERNAME</label>
                        <input type="text" placeholder="Username" name="j_username" id="username" tabindex="1" value="" autocomplete="off" autocapitalize="none" class="form-control"/>
					</div>
                    <div class="form-group" id="password_input">
                    	<label>PASSWORD</label>
                        <input type="password" placeholder="Password" name="j_password" id="password" tabindex="2" value="" autocomplete="off" class="form-control"/>
					</div>
					<c:if test="${showCaptcha}">
						<div class="form-group" id="captcha_image">
							<img src="captcha-image.html" alt="Captcha Image" style="border: 2px solid gray" width="100%"/>
						</div>
						<div class="form-group" id="captcha_input">
							<input type="text" autocomplete="off" autocapitalize="none" placeholder="Enter characters above" name="captcha_response" id="captcha_response" tabindex="3"	value="" class="form-control"/>
						</div>
					</c:if>
					<input id="idsLoginBtn" type="submit" name="login" value="SIGN IN" tabindex="4" class="btn btn-success"/>
				</div>
				<div id="SAMLLoginForm" style="<c:if test="${showUsernameLoginForm}">display:none;</c:if>">
					<c:if test="${showSAMLLogin_ids}">			
						<button id="samlBtnIds" type="button" style="text-align:left;" class="btn btn-primary" tabindex="1" onclick="doSAMLLogin('ids');"><c:out value="${samlLoginLabel_ids}"/></button>
					</c:if>
					<c:if test="${showSAMLLogin_default}"> 
						<button id="samlBtnDefault" type="button" style="text-align:left;" class="btn btn-primary" tabindex="2" onclick="doSAMLLogin('default');"><c:out value="${samlLoginLabel_default}"/></button>
					</c:if>
					<input id="idsLogin" tabindex="3" value="IDS LOGIN" class="btn btn-secondary" style="text-align:left;" onclick="showUsernameLoginForm(true);"></input>
				</div>
				<div id="options-bar">
					&nbsp;|&nbsp; <a href="mailto:support@idsdatanet.com" title="Drop us an e-mail" class="D2Link">Need Help?</a> 
					&nbsp;|&nbsp; <a href="http://www.idsdatanet.com/support" target="_blank" title="..or pick up a phone to call us" class="D2Link">Call us &#9990;</a>
					&nbsp;|&nbsp;Language:&nbsp;
						<select id="userLanguage" name="language">
							<c:if test="${!empty languageOptions}">
								<c:forEach items="${languageOptions}" var="languageCode">
									<option value="<c:out value='${languageCode}'/>"<c:if test="${defaultLanguage == languageCode}"> selected="selected"</c:if>><c:out value='${languageCode}'/></option>
								</c:forEach>
							</c:if>
						</select>
					&nbsp;|&nbsp;
				</div>
			</form>
		</div>
    </div>
</body>
</html>