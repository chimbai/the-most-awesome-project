<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<body>
<table border="1" width="100%">
	<tr>
		<th>ID</th>
		<th>Group</th>
		<th>Section</th>
		<th>Setting Name</th>
		<th>Setting Value</th>
		<th>Description</th>
		<th>Default Value</th>
	</tr>
	<%
		String messageTag = "<div width='100%'><b>SUCCESS</b></div>";
		try {
			
			List<SystemSettings> result = ApplicationUtils
					.getConfiguredInstance()
					.getDaoManager()
					.find(
							"From SystemSettings Where (isDeleted is null or isDeleted = false) order by groupUid, section, settingName");
			
			for (SystemSettings ss : result) 
			{
	%><tr>
		<td><%=ss.getSystemSettingsUid()%></td>
		<td><%=ss.getGroupUid()%></td>
		<td><%=ss.getSection()%></td>
		<td><%=ss.getSettingName()%></td>
		<td><%=StringUtils.isNotBlank(ss.getSettingValue())?ss.getSettingValue():""%></td>
		<td><%=ss.getDescription()%></td>
		<td><%=ss.getDefaultValue()%></td>
	</tr>
	<%
	
			}
	%>
</table>
<table border="1">
	<tr><td colspan="4" bgcolor="#ff0000"><font color="#FFFFFF">DUPLICATE SETTINGS</font></td></tr>
	<tr>
		<th>Group</th>
		<th>Section</th>
		<th>Setting Name</th>
		<th>Value Check</th>
	</tr>    
<%
		List<Object[]> duplicate = ApplicationUtils.getConfiguredInstance().getDaoManager().find("Select DISTINCT groupUid, section, settingName,count(*) From SystemSettings Where (isDeleted is null or isDeleted = false) group by groupUid, section, settingName order by groupUid, section, settingName");
		for (Object[] item : duplicate)
		{
			Long count = (Long)item[3];
			if (count >1)
			{
				String groupUid = item[0].toString();
				String section = item[1].toString();
				String settingName = item[2].toString();
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT settingValue From SystemSettings"+ 
						" Where (isDeleted is null or isDeleted = false) and groupUid=:groupUid and section=:section and settingName=:settingName"
						,new String[]{"groupUid","section","settingName"},new Object[]{groupUid,section,settingName});
				String settingValueCheck = list.size()>1?list.size()+" Diff Value Found":"All Value Same";
				String bgColor = list.size()>1?"bgcolor=\"#FFFF00\"":"";
		%><tr <%=bgColor%> >
			<td><%=groupUid%></td>
			<td><%=section%></td>
			<td><%=settingName%></td>
			<td><%=settingValueCheck%></td>
		</tr>
		<%
				
			}
		}
		%></table>
		
		<% 

	} catch (Exception e) {
		e.printStackTrace();
		messageTag = "<div width='100%' style='color:red;'>FAILED<br/><br/>" + e.getMessage() + "</div>";
	}
	%>

<%=messageTag%>
</body>
</html>