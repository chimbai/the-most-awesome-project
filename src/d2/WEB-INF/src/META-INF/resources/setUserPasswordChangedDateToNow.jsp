<%@ page contentType="text/plain"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.ApplicationUtils,com.idsdatanet.d2.core.dao.QueryProperties,com.idsdatanet.d2.core.model.User,java.util.List, java.util.Date"%>
<%
	try {
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);	
		
		List<User> users = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM User where (isDeleted is NULL or isDeleted= FALSE) AND user_name NOT IN ('idsadmin') AND (ids_user is null or ids_user = false)", queryProperties);
		
		if(users !=null && users.size() > 0){
			for (User user : users) {
				user.setPasswordChangedDate(new Date());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(user, queryProperties);
			}
		}
		
%>-- SUCCESS
<%
	} catch (Exception e) {
%>-- FAILED-
<%=e.getMessage()%><%

	}
%>