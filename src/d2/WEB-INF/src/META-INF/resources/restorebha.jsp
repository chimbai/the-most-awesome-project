<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
<%

	UserSession sessionx = UserSession.getInstance(request); 
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String action = request.getParameter("action");
	String bharunUid = request.getParameter("bharunUid");
	String bharunDailySummaryUid = request.getParameter("bharunDailySummaryUid");
	String bitrunUid = request.getParameter("bitrunUid");
	
	if ( (StringUtils.isNotEmpty(action) && "RestoreBC".equals(action)) && (StringUtils.isNotEmpty(bharunUid))){
		String STRSQL = "UPDATE BhaComponent SET isDeleted=false, lastEditDatetime=now() where bharunUid=:bharunUid";
		String[] params = new String[]{"bharunUid"};
		Object[] values = new Object[]{bharunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}else {
//	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(bharunUid))){
		String STRSQL = "UPDATE Bharun SET isDeleted=false, lastEditDatetime=now() where bharunUid=:bharunUid";
		String[] params = new String[]{"bharunUid"};
		Object[] values = new Object[]{bharunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(bharunDailySummaryUid))){
		String STRSQL = "UPDATE BharunDailySummary SET isDeleted=false, lastEditDatetime=now() where bharunDailySummaryUid=:bharunDailySummaryUid";
		String[] params = new String[]{"bharunDailySummaryUid"};
		Object[] values = new Object[]{bharunDailySummaryUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	if ((StringUtils.isNotEmpty(action)) && (StringUtils.isNotEmpty(bitrunUid))){
		String STRSQL = "UPDATE Bitrun SET isDeleted=false, lastEditDatetime=now() where bitrunUid=:bitrunUid";
		String[] params = new String[]{"bitrunUid"};
		Object[] values = new Object[]{bitrunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(STRSQL,params,values);
	}
	
%>

<br> Operation Name : 
<%=OperationName%>
<%
	String SQLSTR = "FROM Bharun where operationUid='" + sessionx.getCurrentOperationUid() + "'ORDER BY bhaRunNumber";
	List<Bharun> bhalist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
	
	
	%>
	
	<table border="1" width="50%">
	<tr>
	<td>..</td>
	<td>..</td>
	<td>Deleted</td>
	<td>Action</td>
	</tr>
	<%
	for(Bharun bharun : bhalist) {
	%>
		<tr>
		<td colspan='2'><b>Bha Run : <%=bharun.getBhaRunNumber()%></b></td>
		<td><%=bharun.getIsDeleted()%></td>
		<td>
		<%
			String bharunUid1 = bharun.getBharunUid();
			if (BooleanUtils.isTrue(bharun.getIsDeleted()))
			{
		%>
		<form name="input" method="post">
			<input type="hidden" name="bharunUid" value="<%=bharun.getBharunUid()%>" >
			<input type="submit" name="action" value="Restore" > 
		</form>	
		<% } %>
		</td>
		
		</tr>
		<%
		SQLSTR = "FROM BharunDailySummary where bharunUid='" + bharunUid1 + "'";
		List<BharunDailySummary> bhadailylist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
		for(BharunDailySummary bharunDailySummary : bhadailylist) {
			ReportDaily reportDaily = ApplicationUtils.getConfiguredInstance().getReportDaily(bharunDailySummary.getDailyUid(), null);
			SQLSTR = "FROM ReportDaily where reportType !='DGR' and dailyUid='" + bharunDailySummary.getDailyUid() + "'";
			List<ReportDaily> reportDailylist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
			ReportDaily rd = reportDailylist.get(0);
			
			%>
			<tr>
				<td>BDS</td>
				<td><%=rd.getReportNumber() %> - <%=rd.getReportDatetime()%></td></td>
				<td>
				<%	if (BooleanUtils.isTrue(bharunDailySummary.getIsDeleted()))
					{ %>
				<form name="input" method="post">
					<input type="hidden" name="bharunDailySummaryUid" value="<%=bharunDailySummary.getBharunDailySummaryUid()%>" >
					<input type="submit" name="action" value="Restore" > 
				</form>
				<% } %>
				</td>
			</tr>
		
			<%
		}
		%>
		<% 
		SQLSTR = "FROM Bitrun where bharunUid='" + bharunUid1 + "'";
		List<Bitrun> bitrunlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
		for(Bitrun bitrun : bitrunlist) {
			%>
				<tr>
					<td>BIT</td>
					<td><%=bitrun.getBitrunNumber()%></td>
					<td><%=bitrun.getIsDeleted()%></td>
					<td>
					<%	if (BooleanUtils.isTrue(bitrun.getIsDeleted()))
						{ %>
					<form name="input" method="post">
						<input type="hidden" name="bitrunUid" value="<%=bitrun.getBitrunUid()%>" >
						<input type="submit" name="action" value="Restore" > 
					</form>
					<% } %>
					</td>
				</tr>
			
			<%
		}
		%>
		
		<% 
		SQLSTR = "FROM BhaComponent where bharunUid='" + bharunUid1 + "'";
		List<BhaComponent> bclist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(SQLSTR);
		int count = 0;
		int deleteRec = 0;
		for(BhaComponent bc : bclist) {
			if (BooleanUtils.isTrue(bc.getIsDeleted())){
				deleteRec = deleteRec + 1;
			}
			count = count + 1;	
		}
			%>
				<tr>
					<td>Bha Component</td>
					<td><%=count%> Records</td>
					<td><%=deleteRec%> Del Records</td>
					<td>
					<%	if (deleteRec > 0)
						{ %>
					<form name="input" method="post">
						<input type="hidden" name="bharunUid" value="<%=bharunUid1%>" >
						<input type="submit" name="action" value="RestoreBC" > 
					</form>
					<% } %>
					</td>
				</tr>
			
			<%
		
	}
	%>
	</table>
	

</body>
</html>
