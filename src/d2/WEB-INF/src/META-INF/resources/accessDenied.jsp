<%@page import="com.idsdatanet.d2.core.web.mvc.*, java.net.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String screen = "operationinformationcenter.html";	

	try {
		UserSession userSession = UserSession.getInstance(request);
		String userUid = userSession.getUserUid();
		String last_screen_cookie = "lsScreen" + "." + userUid;

		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(last_screen_cookie)) {
					screen = URLDecoder.decode(cookies[i].getValue(),"UTF-8");
					break;
				}
			}
		}
	} catch (Exception e) {}
%>
<head>
	<meta http-equiv="refresh" content="10;url=<%= request.getContextPath() + "/" + screen %>" />
	<title>Access Denied</title>
	<link href="<%= request.getContextPath() %>/styles/screen.css" media="all" rel="Stylesheet" type="text/css" />
</head>
<body class="forgot">
	<div class="container">
		<div id="dialog">
			<h2>Access denied</h2>
			<p>We apologize, but you do not have permission to access this page.</p>
			<br/>
			<p>Please wait while we redirect you to the page you were on. If this page does not redirect in 10 seconds, please click <a href="<%= request.getContextPath() + "/" + screen %>">here</a>.</p>
		</div>
	</div>
</body>
</html>
