<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.io.File,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>

<html>
<body>
Check RTS File <br>
<%

	UserSession sessionx = UserSession.getInstance(request); 
	String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
	String loggingRootPath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/rts/";
	String action = request.getParameter("action");
	String folderDir = request.getParameter("folderDir");
	String opsUid = request.getParameter("opsUid");
	if ( (StringUtils.isNotEmpty(action) && ("DeleteFolder".equals(action) || "DeleteRed".equals(action))) && (StringUtils.isNotEmpty(opsUid))){
		File root = new File(loggingRootPath);
		File operation_dir = new File(root, opsUid);
		if (operation_dir.exists() && operation_dir.isDirectory()) {
			String[] files = operation_dir.list();
			if (files != null && files.length > 0) {
				for (String filename : files) {
					File f = new File(operation_dir, filename);
					try {
						if ("DeleteRed".equals(action)){
							String[] fileparts = filename.split("_");
							String activityRsdUid = fileparts[1];
							ActivityRsd activityRsd = (ActivityRsd)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ActivityRsd.class, activityRsdUid);
							if (activityRsd==null){
								f.delete();
							}
						}
						
						if ("DeleteFolder".equals(action)){
							f.delete();
						}
						//f.delete();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			String[] remaining_files = operation_dir.list();
			if (remaining_files.length == 0) {
				operation_dir.delete(); // delete folder if no more any other files.
			}
		}
	}
	
	

		File root = new File(loggingRootPath);
		if (root != null && root.isDirectory()) {
			File[] operation_dirs = root.listFiles();
			int opsCount = 0;
			if (operation_dirs != null && operation_dirs.length > 0) {
				
				for (File operation_dir : operation_dirs) {
					String current_running_operation_uid = operation_dir.getName();
					OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), current_running_operation_uid);
					if (operation_dir.isDirectory()) {
						opsCount = opsCount + 1;
						%>
						<hr>
						Operation : <%=current_running_operation_uid %> <form name="input" method="post">
							<input type="hidden" name="folderDir" value="<%=operation_dir.getPath()%>" >
							<input type="hidden" name="opsUid" value="<%=current_running_operation_uid%>" >
							<input type="submit" name="action" value="DeleteFolder" > 
						</form>
						
						<form name="input" method="post">
							<input type="hidden" name="folderDir" value="<%=operation_dir.getPath()%>" >
							<input type="hidden" name="opsUid" value="<%=current_running_operation_uid%>" >
							<input type="submit" name="action" value="DeleteRed" > 
						</form>
						Operation Name : <%=OperationName %> <br>
						<% 
						try {
							File[] files = operation_dir.listFiles();
							Arrays.sort(files);
							if (files.length>0) {
								int count = 0;
								for (File filex : files) {
									count = count + 1;
									String filename = filex.getName();
									String[] fileparts = filename.split("_");
									String activityRsdUid = fileparts[1];
									ActivityRsd activityRsd = (ActivityRsd)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ActivityRsd.class, activityRsdUid);
									boolean found = false;
									if (activityRsd!=null) found = true;
									String disText = "XXX";
									if (found) {
										%>
											<p style="color:black;margin-top: 0px;margin-bottom: 0px;"><%=count %> : <%=filename %></p>
										<%
										
									} else {
										%>
											<p style="color:red;margin-top: 0px;margin-bottom: 0px;"><%=count %> : <%=filename %></p>
										<%
									}
									
								}
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (opsCount==0){
					%>
						<br>EMPTY
					<% 
					}
			}
		}else{
			%>
				<br>
				EMPTY
			<% 
			
		}
%>
</body>
</html>
