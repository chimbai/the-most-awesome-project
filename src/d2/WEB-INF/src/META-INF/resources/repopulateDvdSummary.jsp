<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"
%>
<html>
<header>
	<title>Dvd Flush and Restore Script</title>
</header>
<%
try {
	UserSession sessionx = UserSession.getInstance(request);
	String hql = "From DvdSummary where (isDeleted = false or isDeleted is null) and operationUid='" + sessionx.getCurrentOperationUid() + "'";
	List<DvdSummary> dvdlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
	
	if (dvdlist.size()>0)
	{
			int i = 0;
			for (DvdSummary dvd: dvdlist)
			{	
					CommonUtil.getConfiguredInstance().deleteExistingDvdSummary("daily", dvd.getDailyUid());
					CommonUtil.getConfiguredInstance().createDvdSummary(dvd.getOperationUid(), dvd.getDailyUid());
					
			}
			%>--- FLUSHED AND RESTORED ---<%
	}
	%>
<%
}catch (Exception e) {
	System.out.println(e.getMessage());
%>-- FAILED


<%
}
%>

