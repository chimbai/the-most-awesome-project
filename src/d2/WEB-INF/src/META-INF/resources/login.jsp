<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" import="java.util.Calendar, java.util.GregorianCalendar, java.util.List, com.idsdatanet.d2.common.i18n.MultiLangUtils, com.idsdatanet.d2.core.web.mvc.ApplicationConfig, com.idsdatanet.d2.core.web.mvc.UserSession" %>
<% 
	if(!Boolean.TRUE.equals(request.getAttribute("com.idsdatanet.d2.login.forward"))){
		response.sendRedirect("login.html");
		return;
	}

	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-store");

	Calendar calendar = new GregorianCalendar();
	String defaultLanguage = "en";
	String loginType = null;
	boolean skipCaptcha = false;

	Cookie[] cookies = request.getCookies();
	if(cookies != null) {
		for(int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if("d2.loginType".equals(cookie.getName())) {
				loginType = cookie.getValue();
			}else if("skipCaptcha".equals(cookie.getName())){
				skipCaptcha = "1".equals(cookie.getValue());
			}else if("storedLanguage".equals(cookie.getName())){
				defaultLanguage = cookie.getValue();
			}
		}
	}
	
	pageContext.setAttribute("clientLogoPath", "images/login/client-logo.png");
	pageContext.setAttribute("clientLogoLabel", "IDS Datanet 2.5");
	pageContext.setAttribute("currentYear", String.valueOf(calendar.get(Calendar.YEAR)));
	pageContext.setAttribute("csrfToken", UserSession.getCSRFToken(request));
	pageContext.setAttribute("loginType", loginType);
	pageContext.setAttribute("defaultLanguage", defaultLanguage);
	pageContext.setAttribute("showCaptcha", ApplicationConfig.getConfiguredInstance().isCaptchaEnabled() && !skipCaptcha);
	pageContext.setAttribute("showSAMLLogin_ids", com.idsdatanet.d2.infra.system.security.saml.SAMLUtils.isSAMLEnabled(request, "ids"));
	pageContext.setAttribute("showSAMLLogin_default", com.idsdatanet.d2.infra.system.security.saml.SAMLUtils.isSAMLEnabled(request, "default"));
	
	List<String> languages = MultiLangUtils.getConfiguredInstance().getLanguages();
	if(languages != null && languages.size() > 0) {
		pageContext.setAttribute("languageOptions", languages);
	}
	
	Exception e = (Exception) session.getAttribute(org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION);
	if(e != null){
	    session.removeAttribute(org.springframework.security.web.WebAttributes.AUTHENTICATION_EXCEPTION);
		String error_msg = "Invalid username and/or password, please try again.";
		if(e instanceof com.idsdatanet.d2.core.web.security.v2.UserGroupNotFoundException){
			error_msg = "Invalid user group definition, please contact support@idsdatanet.com";
		}else if(e instanceof org.springframework.security.authentication.AccountExpiredException){
			error_msg = "Your user account has expired. Please contact support@idsdatanet.com if login to the system using this account is required.";
		}else if(e instanceof org.springframework.security.authentication.LockedException){
			error_msg = "Account locked, please contact support@idsdatanet.com";
		}else{
			error_msg = "Login failed. " + e.getMessage();
		}
		pageContext.setAttribute("error_msg", error_msg);
	}

	String automationTesting = (request.getParameter("automationTesting") == null ? "" : request.getParameter("automationTesting"));
	pageContext.setAttribute("automationTesting", automationTesting);
%>
<!DOCTYPE html>
<html>
<head>
	<title>IDS DataNet 2.5</title>
	<link href="styles/screen.css" rel="Stylesheet" type="text/css">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<script>
		function submitLoginForm(){
			var i = document.getElementById("login_form_client_url");
			if(i){
				i.value = document.URL;
			}
			return true;
		}
		function doSAMLLogin(alias){
			var userLanguage = "";
			var dd = document.getElementById("userLanguage");
			if(dd) userLanguage = dd.value;
			window.location="<c:out value='${pageContext.request.contextPath}'/>/saml/login?myProviderAlias=" + encodeURIComponent(alias) + "&userLanguage=" + encodeURIComponent(userLanguage);			
		}
	</script>
	<style type="text/css">
		html,body{
			height: 100%;
			width: 100%;
			padding: 0;
			margin: 0;
		}
		.login div.container {
			width: 700px;
			margin: 120px auto 20px auto;
			min-width: inherit;
		}
		.login input {
			font-size: 14px;
		}
		.login input#username,
		.login input#password,
		.login input#captcha_response {
			width: 100%;
		}
		.login div#dialog {
			width: 500px;
			border: 4px solid #ccc;
			box-shadow: 0px 10px 30px #888888;
			text-align: left;
			background: #FFFFFF;
		}
		#top {
			background-image:url('images/login/login_header.png');
			background-repeat: no-repeat;
			background-size: 100%;
			height:15px;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
			src='images/login/login_header.png',
			sizingMethod='scale');
			-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(
			src='images/login/login_header.png',
			sizingMethod='scale')";
		}
		#footer-container {
			border-top: 1px solid #e3e3e3;
			background-image:url('images/login/login_footer.png');
			background-repeat: no-repeat;
			background-size: 100%;
			height:100px;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
			src='images/login/login_footer.png',
			sizingMethod='scale');
			-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(
			src='images/login/login_footer.png',
			sizingMethod='scale')";
		}
		#copyright {
			font-size:  10.5px !important;
			padding-left :0.65em;
			padding-right :0.65em;
			padding-top :8em;
			color: white;
			text-align:right;
		}
		#header {
			margin: 1em auto;
			width:85%;
		}
		#header #logo {
			margin-top :1em;
			font-size: 1.125em;
			width:155px;
		}
		#header #logo2 {
			font-size: 1.125em;
			width:175px;
		}
		#header #d25logo {
			margin-top :0.1em;
			margin-bottom :0.7em;
			font-size: 1.125em;
			width:160px;
		}
		#header #clientTitle {
			margin-top: 0.3em;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 1.35em;
			font-weight:bold;
		}
		.DefaultButton {
			background-color: #FFFFFF;
			background-repeat: repeat-x;
			border-radius: 2px !important;
			border-style: solid;
			border-width: 1px;
			border-color: #1F4788;
			color: #1F4788;
			cursor: pointer;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 13px !important;
			padding: 5px 10px;
		}
		.DefaultButton:hover:enabled {
			background-color: #a5e3f8 !important;
			outline: 0px;
		}
		.fieldEditor {
			outline: none; 
 			border: 1px solid #666;
			padding: 3px 2px;
		}
		.fieldEditor:focus {
 			border: 1px solid #000;
		}
		.inline-link-2 {
			border: none !important;
			text-decoration: none !important;
		}
		#options-bar {
			margin: 5px 0 5px 0;
			padding: 0;
			font-size: 11px !important;
			line-height: 24px;
			color: #666;
		}
		.LoginError{
			color: #FFFFFF;
			background: url(images/alertbad_icon.gif) #c00 left no-repeat;
			font-size: 12px;
			padding: 6px 5px 6px 30px;
		}
	</style>
</head>

<body class="login" onload="document.form.j_username.focus();" style="background: radial-gradient(#AAAAAA, #999999);">

<table border="0" style="height:100%; width:100%">
	<tr><td style="vertical-align: middle">
	<div style="width:510px; margin: 0 auto">
		<div id="dialog" style="width:510px;">
			<div id="top"></div>
			
			<div id="header">
				<div><img id="d25logo" src="images/login/datanet2.5-logo.png" /></div>
				<center><img src="<c:out value='${clientLogoPath}'/>" alt="<c:out value='${clientLogoLabel}'/>" border="0" style="max-height:250px; max-width:400px; margin-top:10px;"></center>
			</div>
			
			<c:if test="${!empty error_msg}">		
				<div class="LoginError"><c:out value="${error_msg}"/></div>
			</c:if>		

			<form name="form" method="post" action="j_security_check" onSubmit="submitLoginForm()">
				<input name="automationTestingMode" value="<c:out value='${automationTesting}'/>" type="hidden">
				<input name="csrfToken" value="<c:out value='${csrfToken}'/>" type="hidden">
				<input id="login_form_client_url" type="hidden" name="login_form_client_url" value=""/>
				<div>
					<c:choose>
					<c:when test="${loginType == 'd2JWT'}">
						<div style="text-align:center; width:100%; padding-top: 50px; padding-bottom: 50px"><span style="font-size:120%; padding: 10px">Session ended, please close this window</span></div>
					</c:when>
					<c:otherwise>
						<table style="text-align:left; padding:10px 96px 0px 90px;" width="100%">
							<tr><td colspan="2"><input type="text" placeholder="Username" name="j_username" id="username" tabindex="1" value="" autocomplete="off" class="fieldEditor" style="box-sizing: border-box"/></td></tr>
							<tr><td colspan="2"><input type="password" placeholder="Password" name="j_password" id="password" tabindex="2" value="" autocomplete="off" class="fieldEditor" style="box-sizing: border-box"/></td></tr>
							<c:if test="${showCaptcha}">
								<tr><td colspan="2"><img src="captcha-image.html" alt="Captcha Image" style="border:2px solid gray; box-sizing: border-box" width="100%" /></td></tr>
								<tr><td colspan="2"><input type="text" autocomplete="off" autocapitalize="none" placeholder="Enter characters above" name="captcha_response" id="captcha_response" tabindex="3" value="" class="fieldEditor" style="box-sizing: border-box"/></td></tr>
							</c:if>	
							<tr>
					            <td><input type="submit" name="login" value="Sign in" tabindex="4" class="DefaultButton"/></td>
								<td style="text-align:right">
									<c:if test="${showSAMLLogin_ids}">
										<label class="DefaultButton" style="cursor:pointer; text-decoration:underline" onclick="doSAMLLogin('ids');">Login with IDS SAML</label>
									</c:if>
									<c:if test="${showSAMLLogin_default}">
										<label class="DefaultButton" style="cursor:pointer; text-decoration:underline" onclick="doSAMLLogin('default');">Login with SAML</label>
									</c:if>
									<input type="hidden" id="mobile_view" name="mobile_view" value="no"/>
							    </td>
							</tr>
						</table>
					</c:otherwise>
					</c:choose>
				</div>
				
				<div>
					<table width="100%" style="line-height:1.5em">
						<tr style="text-align:center">
							<td style="color: rgba(55,83,181,1);">
							<div id="options-bar">
							&nbsp;&nbsp;|&nbsp;&nbsp; <a href="forgot.html" title="Click here for help in recovering it" class="inline-link-2">Forgot your password?</a> 
							&nbsp;&nbsp;|&nbsp;&nbsp; <a href="mailto:support@idsdatanet.com" title="Drop us an e-mail" class="inline-link-2">Need Help?</a> 
							&nbsp;&nbsp;|&nbsp;&nbsp; <a href="http://www.idsdatanet.com/support" target="_blank" title="..or pick up a phone to call us" class="inline-link-2">Call us &#9990;</a>
							&nbsp;&nbsp;|&nbsp;&nbsp;
							<!-- REMOVE THIS INPUT TO ENABLE LANGUAGE CAPABILITY - DEFAULT TO ENGLISH(en) AT THE MOMENT-->
							Language:&nbsp;
								<select id="userLanguage" name="language">
									<c:if test="${!empty languageOptions}">
										<c:forEach items="${languageOptions}" var="languageCode">
											<option value="<c:out value='${languageCode}'/>"<c:if test="${languageCode == defaultLanguage}"> selected="selected"</c:if>><c:out value="${languageCode}"/></option>
										</c:forEach>
									</c:if>											
								</select>
							&nbsp;&nbsp;|&nbsp;&nbsp;
							<!-- End of optional language section -->
							</div>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<div id="footer-container"/>
			<div id="copyright">
				Copyright&nbsp;&copy;&nbsp;2000-<c:out value="${currentYear}"/> Independent Data Services
			</div>
		</div>
	</div>
	</td></tr>
</table>
</body>
</html>