
<%@page import="com.idsdatanet.d2.common.util.CommonUtil"%><%@ page contentType="text/plain"%>
<%@ page import="java.text.SimpleDateFormat, com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.dao.QueryProperties,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%>
<%
	try {
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		UserSession sessionx = UserSession.getInstance(request); 
		
		List<Operation> operation = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Operation " +
				"WHERE (isDeleted IS NULL OR isDeleted=0) and operationUid ='" + sessionx.getCurrentOperationUid() + "' ", queryProperties);
		
		for (Operation thisOperation : operation){
			String operationUid = thisOperation.getOperationUid();
			Double amountSpentPriorToSpud = thisOperation.getAmountSpentPriorToSpud();
			
			List<ReportDaily> reportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM ReportDaily " +
				"WHERE operationUid = '"+thisOperation.getOperationUid()+"' AND (isDeleted IS NULL OR isDeleted=0)", queryProperties);
			
			for (ReportDaily thisReportDaily : reportDaily) {
				String dailyUid = thisReportDaily.getDailyUid();
				Double daycost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(dailyUid);
				
				if (daycost != null){
					thisReportDaily.setDaycost(daycost);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReportDaily, queryProperties);
				}
			}
		}
				
%>-- SUCCESS
<%
	} catch (Exception e) {
%>-- FAILED-
<%=e.getMessage()%><%

	}
%>