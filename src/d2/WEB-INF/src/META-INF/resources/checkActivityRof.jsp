<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
com.idsdatanet.depot.witsml.rts.webservice.adapter.listener.DrillingParametersRTSListener,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
com.idsdatanet.depot.witsml.rts.RTSAdapterListener,
com.idsdatanet.depot.witsml.rts.RigStateData,
com.idsdatanet.depot.goldbox.field.util.ActRofCodeUtil,
org.apache.commons.lang.StringUtils"
%><%
	try {
		int totalDeleted = 0;
		int totalRecord = 0;
		UserSession sessionx = UserSession.getInstance(request); 
		String OperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(sessionx.getCurrentGroupUid(), sessionx.getCurrentOperationUid());
		String sql = "FROM Activity WHERE (isDeleted is null or isDeleted = false) AND (isSimop='0' OR isSimop = '' OR isSimop is NULL) " +
				"AND (isOffline='0' OR isOffline = '' OR isOffline is NULL) AND operationUid='" + sessionx.getCurrentOperationUid() + "' ORDER BY  startDatetime";
		List<Activity> acts = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		int checkCounter= 0;
		if (acts.size()>0){
			totalRecord = acts.size();
		%>	<table border="1" width="100%">
			<tr>
			<td>Activity UID</td>
			<td>Start Time</td>
			<td>End Time</td>
			<td>Rof Code Mapped?</td>
			</tr><%
			
			for (Activity activity : acts)
			{	
				ActivityExtCodeMapping aecm = ActRofCodeUtil.getAECMapping(activity);
				Boolean checked = false;
				String rofCode = "";
				if (aecm != null) {
					checked= true;
					checkCounter = checkCounter + 1;
					if (StringUtils.isNotEmpty(aecm.getExtCodeL1())){
						rofCode = aecm.getExtCodeL1() + (rofCode==""?"":"_" + rofCode);
					}
					if (StringUtils.isNotEmpty(aecm.getExtCodeL2())){
						rofCode = aecm.getExtCodeL2() + (rofCode==""?"":"_" + rofCode);
					}
					if (StringUtils.isNotEmpty(aecm.getExtCodeL3())){
						rofCode = aecm.getExtCodeL3() + (rofCode==""?"":"_" + rofCode);
					}
					if (StringUtils.isNotEmpty(aecm.getExtCodeL4())){
						rofCode = aecm.getExtCodeL4() + (rofCode==""?"":"_" + rofCode);
					}
					if (StringUtils.isNotEmpty(aecm.getExtCodeL5())){
						rofCode = aecm.getExtCodeL5() + (rofCode==""?"":"_" + rofCode);
					}
					if (StringUtils.isNotEmpty(aecm.getExtCodeL6())){
						rofCode = aecm.getExtCodeL6() + (rofCode==""?"":"_" + rofCode);
					}	
				}
				%>
				<tr>
					<td><%=activity.getActivityUid() %></td>
					<td><%=activity.getStartDatetime() %></td>
					<td><%=activity.getEndDatetime() %></td>
					<td><%=rofCode%></td>
				</tr>
				<%
			}
			%></table><br><br><%
		}

%>
<%=checkCounter %> / <%=acts.size()%> <br>
<%=sessionx.getCurrentOperationUid()%> <br>
~ Done on x<%=OperationName%>
<%
	} catch (Exception e) {
		System.out.println(e.getMessage());
%>-- FAILED


<%
	}
%>