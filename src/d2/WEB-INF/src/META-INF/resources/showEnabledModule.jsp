<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<body>
<table border="1">
	<tr>
		<th>Key</th>
		<th>GROUP</th>
		<th>Controller</th>
		<th>Label</th>
		<th>Path</th>
	</tr>
	<%
		String groupUid=null;
		String messageTag = "<div width='100%'><b>SUCCESS</b></div>";
		try {
			LinkedHashMap<String, MenuItem> raw = new LinkedHashMap<String, MenuItem>();
			LinkedHashMap<String, MenuItem> processed = new LinkedHashMap<String, MenuItem>();
			List<String> list = new ArrayList<String>();
			List<MenuItem> result = ApplicationUtils
					.getConfiguredInstance()
					.getDaoManager()
					.find(
							"From MenuItem Where (isDeleted is null or isDeleted = false)");
			for (MenuItem mi : result) {
				raw.put(mi.getMenuItemUid(), mi);
				groupUid=mi.getGroupUid();
			}
			for (MenuItem mi : result) {
				String path = mi.getLabel();
				Boolean include = false;
				if (StringUtils.isNotBlank(mi.getBeanName())) {
					if (StringUtils.isNotBlank(mi.getParent())) {
						MenuItem current = mi;
						Boolean valid = true;
						while (valid && current != null) {
							if (StringUtils.isBlank(current.getParent())) {
								current = null;
								break;
							} else {
								current = raw.get(current.getParent());
								if (current == null) {
									valid = false;
								} else {
									path = current.getLabel() + " > "
											+ path;
								}
							}
						}
						include = valid;
					} else {
						include = true;
					}
					if (include) {
						list.add(path);
						processed.put(path, mi);
					}
				}

			}
			Collections.sort(list);
			for (String path : list) {
				MenuItem mi = processed.get(path);
	%><tr>
		<td><%=mi.getGroupUid()%>_<%=mi.getMenuItemUid()%></td>
		<td><%=mi.getGroupUid()%></td>
		<td><%=mi.getBeanName()%></td>
		<td><%=mi.getLabel()%></td>
		<td><%=path%></td>
	</tr>
	<%
		}
	%>
</table>
<table border="1">
	<tr>
		<th>GroupUid</th>
		<th>Jar</th>
		<th>Version</th>
	</tr>
	
<%
	Map<String,String> files = ApplicationConfig.getConfiguredInstance().getLibsVersion();
	for (Map.Entry<String,String> jar : files.entrySet())
	{
		%><tr>
			<td><%=groupUid%></td>
			<td><%=jar.getKey()%></td>
			<td><%=jar.getValue()%></td>
		</tr><%	
	}
%>
</table>
<%
	} catch (Exception e) {
		messageTag = "<div width='100%' style='color:red;'>FAILED<br/><br/>" + e.getMessage() + "</div>";
	}
%>
<%=messageTag%>
</body>
</html>