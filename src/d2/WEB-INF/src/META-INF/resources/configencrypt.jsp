<%@page contentType="text/html; charset=UTF-8" import="org.apache.commons.lang.StringUtils, org.apache.commons.codec.binary.Base64, java.security.KeyFactory, java.security.spec.X509EncodedKeySpec, java.security.Key, javax.crypto.Cipher, java.security.spec.EncodedKeySpec" %>
<%
	String valueToEncrypt = request.getParameter("valueToEncrypt");
	String encryptedValue = null;
	if(!StringUtils.isBlank(valueToEncrypt)){
		byte[] keyBytes = Base64.decodeBase64("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsrglBSCwW366H8BFiNpMuaTRWhs40AOgLbrZW/oz55+uII/sS6gGTjadO7669rK6S+IG8ZiYPHb79auqdV+V+/PAADV2Bn9nML/zoF0FXYgPMdnbQotcMELiNASb/Ed8+yUtvMHb25pTOvwFiUvwoxqAEoPwh/9tkWFGvV2pN+c7V8zXMyrdk4qhF2H+N4iUvchexcpgdjHdNmMZYZ8t25g1TxQz+6jvMA6Xmtay93gJGA2TxgXsK8oSYvlBwDJfM39pSLWLTuZAfu73qxxtdf+JhsvzDz1tKCVf4xwYvdjvAeKJx0zNrdl7JhK4daruRmvl4Uzgja0HR8gS4F8erwIDAQAB");
		KeyFactory kf = KeyFactory.getInstance("RSA");
        EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        Key key = kf.generatePublic(keySpec);
      	Cipher cipher = Cipher.getInstance("RSA");
      	cipher.init(Cipher.ENCRYPT_MODE, key);
      	encryptedValue = Base64.encodeBase64String(cipher.doFinal(valueToEncrypt.trim().getBytes("utf-8"))) + ",Encrypt";
	}
%>
<!DOCTYPE html>
<html>
<head>
	<script>
		function copyEncryptedText(){
		      var range = document.createRange();
              range.selectNodeContents(document.getElementById("encryptedText"));
              window.getSelection().removeAllRanges();
              window.getSelection().addRange(range);
              document.execCommand("copy");
              window.getSelection().removeAllRanges();
		}         
	</script>
</head>
<body>
	<form method="post">
		Value to encrypt: <input type="text" name="valueToEncrypt" style="width:500px"> <button type="submit">Encrypt</button>
		<% if(encryptedValue != null) {%>
			<br><br>Encrypted value of "<%=valueToEncrypt%>": <div id="encryptedText" style="box-sizing: border-box; font-weight: bold; padding: 10px; width: 100%; word-wrap: break-word"><%=encryptedValue%></div>
			<input type="button" onclick="copyEncryptedText()" value="Copy">
		<% }%>
	</form>
</body>
</html>