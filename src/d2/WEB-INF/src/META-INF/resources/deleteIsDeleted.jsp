<%@ page contentType="text/html"%>
<%@ page
	import="com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
org.apache.commons.lang.BooleanUtils,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils,
com.idsdatanet.d2.common.util.CommonUtil,
com.idsdatanet.d2.core.cache.Operations,
org.apache.commons.lang.StringUtils"%>

<html>
<header>
	<title>Deletion Script</title>
	<script>
		function processHttpAddress(rawHttpAddress, action) {
			return rawHttpAddress.includes("?") ? rawHttpAddress.split("?")[0]
					+ "?action=" + action : rawHttpAddress + "?action="
					+ action;
		}

		function refreshPage() {
			let httpAddress = window.location.href;
			window.location.replace(httpAddress.includes("?") ? httpAddress
					.split("?")[0] : httpAddress);
		}

		function hardDelete(element) {
			let action = element.getAttribute('data-args');
			let rawHttpAddress = window.location.href;
			let processedHttpAddress = processHttpAddress(rawHttpAddress,
					action);

			if (confirm("Are you sure?")) {
				window.location.replace(processedHttpAddress);
			}
		}
	</script>
</header>
<body>
	<%
		String action = request.getParameter("action");
		String[] tables;
		String query;
		String[] params;
		Object[] values;
		if (action != null) {
			System.out.println("Starting deletion process...");
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Deletion in progress!');");
			out.println("</script>");
			try {
				if (action.contentEquals("hardDeleteTables")) {
					tables = new String[] { "FileManagerFiles", "ReportFiles",
							"SysAutoUpdateClientStatus", "SysAutoUpdatePatches",
							"SysDataImportLog", "SysLogDepotTransaction", "SysLogException",
							"SysLogLogin", "SysLogRigStateXml", "SysLogSendToTown",
							"SysLogUserAccess", "SysManualUpdatePatches",
							"SysRemoteApplicationInfo", "TransactionLog", "OpsTeam",
							"OpsTeamOperation", "OpsTeamUser", "SchedulerTemplate",
							"DepotWellOperationMapping" };

					query = "DELETE FROM table WHERE 1=:xx";

					for (int i = 0; i < tables.length; i++) {
						String newTable = tables[i];
						String STRSQL = query.replace("table", newTable);
						params = new String[] { "xx" };
						values = new Object[] { 1 };
						try {
							ApplicationUtils.getConfiguredInstance().getDaoManager()
									.executeByNamedParam(STRSQL, params, values);
						} catch (Exception e) {
							System.out.println("Something went wrong...");
							System.out.println("Error: " + e.toString());
							out.println("<script type=\"text/javascript\">");
							out.println("alert('Error: " + e.toString() + "');");
							out.println("</script>");
							return;
						}
					}
				}
				if (action.contentEquals("hardDeleteUserTable")) {

					try {
						query = "FROM User WHERE (userName='wss' OR userName='filebridge_admin' OR userName='lighthouse_admin' OR userName='elp_admin' OR userName='idsadmin' OR userName='client_admin') OR email=:email";
						params = new String[] { "email" };
						values = new Object[] { "IDSBridge@idsdatanet.com" };
						List<User> queryResult = ApplicationUtils.getConfiguredInstance()
								.getDaoManager().findByNamedParam(query, params, values);
						List<String> userUids = new ArrayList<>();

						if (queryResult.size() > 0) {
							for (User obj : queryResult) {
								userUids.add(obj.getUserUid());
							}
						}

						query = "FROM User";
						List<User> secondaryQueryResult = ApplicationUtils
								.getConfiguredInstance().getDaoManager().find(query);
						for (User user : secondaryQueryResult) {
							if (!userUids.contains(user.getUserUid())) {
								query = "DELETE FROM User WHERE userUid=:userUid";
								params = new String[] { "userUid" };
								values = new Object[] { user.getUserUid() };
								ApplicationUtils.getConfiguredInstance().getDaoManager()
										.executeByNamedParam(query, params, values);
							}
						}

					} catch (Exception e) {
						System.out.println("Something went wrong...");
						System.out.println("Error: " + e.toString());
						out.println("<script type=\"text/javascript\">");
						out.println("alert('Error: " + e.toString() + "');");
						out.println("</script>");
						return;
					}
				}
				if (action.contentEquals("hardDeleteIsDeletedTable")) {
					tables = new String[] { "AclEntry", "AclGroup",
							"ActiveSendToTownLastRefreshedTimestamp", "Activity",
							"ActivityCodingKpiMatrix", "ActivityCodingKpiMatrixPhaseCode",
							"ActivityCodingKpiMatrixTaskCodeExclusion",
							"ActivityDescriptionMatrix", "ActivityEquipmentFailureLink",
							"ActivityHseIncidentLink", "ActivityLessonTicketLink",
							"ActivityRatesBreakdown", "ActivityRatesBreakdownAdmin",
							"ActivityRatesBreakdownDetail", "ActivityRatesBreakdownEvent",
							"ActivityRsd", "ActivityUnwantedEventLink",
							"AdatApplicationPreferences", "AdatFilterCriteria",
							"AdatFilterGroup", "AdatQuery", "AdatQueryDefinition",
							"AdatQueryQuestion", "AdatReportLayout", "AirPackageSummary",
							"Annotations", "AnovaConfigFilter", "AnovaFilter", "AnovaGroup",
							"AnovaRepository", "AnovaRepositoryParameterMapping",
							"AnovaRepositoryReportGroup", "Basin", "BasinFormation", "Bharun",
							"BharunDailySummary", "BhaComponent", "BhaComponentHoursUsed",
							"Biostratigraphy", "BiostratEcozone", "Bitrun", "BitNozzle",
							"BoilerBlowDown", "BonusPenaltyEvent", "Bop", "BopComponentDetail",
							"BopDetail", "BopDetailLog", "BopLog", "BopRamElastomerDetail",
							"BudgetedDowntime", "Campaign", "CampaignDetail", "Case",
							"CasingFailureTest", "CasingJob", "CasingSection", "CasingSummary",
							"CasingTally", "CementActivity", "CementAdditive", "CementFluid",
							"CementJob", "CementStage", "Client", "ClientAssociation",
							"CoalDescription", "CoalDesorption", "CoalDesorptionDetails",
							"CoiledTubing", "CoiledTubingDetails", "CoiledTubingWallThickness",
							"CommonLookup", "CommonLookupAclGroup", "CommonLookupDepot",
							"CommonLookupType", "Company", "CompanyAssociation", "Component",
							"ConsiderationRemark", "ConsultantDetail", "ConsultantMaster",
							"ContractPersonnel", "ContractRate", "Core", "CoreDetail",
							"CorrosionRingProperties", "CostAbandonment", "CostAccountCodes",
							"CostAccountCodesCategory", "CostAfeDetail",
							"CostAfeDetailAdjusted", "CostAfeDetailVault", "CostAfeGroup",
							"CostAfeMaster", "CostAfeMasterAdjusted", "CostAfeMasterVault",
							"CostCurrencyLookup", "CostDailysheet", "CostDailysheetTracker",
							"CostRental", "CostVendorInvoice", "CostVendorInvoiceDetail",
							"CostVendorInvoiceOtherDetail", "CostWorkOrder",
							"CostWorkOrderDetail", "CurrentActivityState", "CurrentSubState",
							"CustomCode", "CustomCodeGroup", "CustomCodeLink",
							"CustomDescriptionMatrix", "CustomFtrLink", "CustomParameterDetail",
							"CustomParameterLink", "Daily", "DailyAfterActionReview",
							"DailyCommandBeanSubmission", "DailyFormationParameters",
							"DailyObservation", "DailyRevisionDetailLog", "DailyRevisionLog",
							"DailySubmissionEmptyRecord", "DatFilter", "DayBreakdown",
							"DepotButtonSetup", "DepotObjectCustomFieldMapping",
							"DepotObjectMapping", "DepotSyncJob", "DepotWitsmlLogRequest",
							"DepotWitsmlMsg", "Division", "DownholeLog", "DownholeTools",
							"Drift", "DrillingParameters", "DrillingParametersLog",
							"DrillPipeDaily", "DrillPipeDailySummary", "DrillPipeRun",
							"DrillPipeTally", "DrillPipeTallyDetail", "DrillPipeTallyPlan",
							"DrillPipeTourDetail", "DrillPipeTourSummary", "DrillStemTest",
							"DrillStemTestDetail", "DroppedInHole", "DroppedInHoleLog",
							"DvdSummary", "DynamicField", "DynamicFieldMeta",
							"DynamicPositioning", "EdmConfiguration", "EdmImport",
							"EdmObjectConfiguration", "EdmSync", "EmailList",
							"EnvironmentDischarges", "EnvironmentDischargesComment",
							"EquipmentActivityLog", "EquipmentFailure", "EquipmentTrackingLog",
							"EquipmentTrackingMaster", "EssentialFields", "ExchangeRates",
							"ExchangeRatesItem", "ExpenseRecap", "FieldTransactionLog",
							"FileBridgeFiles", "FileBridgeTypeLookup", "FileDirectory",
							"FiltrationFluidProperties", "FiltrationFluidSampleDetail",
							"FiltrationJob", "FiltrationJobFluid", "Formation",
							"FormationEvaluationAssessment",
							"FormationEvaluationAssessmentDetail", "FormationFluid",
							"FormationFluidSample", "FracCoilLog", "FracFluidVolume",
							"FracSheet", "FracSheetDetail", "FtrBopEquipment", "FtrDrilling",
							"FtrDrillLineEquipment", "FtrRig", "FtrTripping",
							"FtrWorkoverCompletionSystem", "GasPeak", "GasReadings",
							"GeneralComment", "GeologistTeam", "GeologyDescription",
							"Geolshows", "GloryHole", "Group", "HoleSection", "HoleSectionKpi",
							"HoleVolumes", "HseEnvironmentImpact",
							"HseEnvironmentImpactContact", "HseIncident", "HseInjury",
							"HseInjuryDetail", "HsePlan", "HsePlanEvents",
							"HseRelatedOperation", "HseWitness", "Hydraulics", "Iceberg",
							"ImportExportServerProperties", "InspectionResult",
							"InventoryCategoryLookup", "InventoryServiceLog",
							"InventoryShortDescrLookup", "InventoryTransaction",
							"InventoryTransactionDetail", "InvBbInfo", "InvBbMovementLog",
							"InvGroupClientInfo", "InvReportDaily", "KeyPerformanceIndicator",
							"Kick", "KickDetails", "KickTolerance", "KpiAclEntry", "KpiConfig",
							"KpiCustomConfig", "KpiHoleSection", "KpiOpsType", "KpiRepository",
							"KpiThreshold", "KpiUnitClassification", "LeakOffTest",
							"LeakOffTestDetail", "LeakOffTestPumpOffDetail", "LessonTicket",
							"LessonTicketCategoryElement", "LessonTicketLog",
							"LessonTicketParticipant", "LessonTicketProfile",
							"LessonTicketProfileDetail", "LessonTicketProfileGroup", "LifeBoat",
							"Litholog", "LithologyShows", "Location", "LocationAssociation",
							"LogisticDaily", "LogCurveDefinition", "LogMnemonicsMapping",
							"LookaheadDayJobList", "LookaheadDayRequirement",
							"LookaheadRequirement", "LookupBhaComponent",
							"LookupBhaComponentCatalog", "LookupBhaComponentCatalogDetails",
							"LookupBhaComponentSection", "LookupClassCode", "LookupClean",
							"LookupCompany", "LookupCompanyService", "LookupContractRate",
							"LookupEnvironmentDischarges", "LookupIncidentCategory",
							"LookupInspectionCategory", "LookupInspectionCheckType",
							"LookupInspectionCheckTypeValue", "LookupInspectionDetail",
							"LookupInspectionType", "LookupJobrole", "LookupJobType",
							"LookupLessonNumber", "LookupLessonTicketCategory",
							"LookupLessonTicketElements", "LookupLessonTicketElementsContents",
							"LookupMechanicalEquipment", "LookupMechanicalEquipmentPhase",
							"LookupMechanicalEquipmentTask", "LookupOperationPhase",
							"LookupOperationType", "LookupPhaseCode", "LookupProductList",
							"LookupPurposeType", "LookupRigCabin", "LookupRigPumpComponent",
							"LookupRigStock", "LookupRigTransportConfig", "LookupRootCauseCode",
							"LookupSafetyCategory", "LookupSafetySubCategory", "LookupTaskCode",
							"LookupUserCode", "LostCirculation", "Lwd", "LwdEngineer",
							"LwdFormationPressure", "LwdFormationPressureDetail", "LwdMud",
							"LwdOffset", "LwdPreLogging", "LwdPreLoggingDetail", "LwdSuite",
							"LwdTool", "LwdToolLookup", "LwdWitness", "MarineProperties",
							"MarineSupport", "MarineSupportSecondary", "MechanicalEquipment",
							"MechanicalEquipmentComponent", "MenuItem", "MenuItemOperationType",
							"MenuItemRigType", "MenuItemViewType",
							"MinimumSafeManningRequirements",
							"MinimumSafeManningRequirementsDetails", "MobileSubsLog",
							"ModularFormationDynamicsTester",
							"ModularFormationDynamicsTesterDetail", "Module",
							"ModuleParameterDetail", "ModuleParameterLink",
							"ModuleSubscription", "MudAdditives", "MudPit", "MudPitCapacity",
							"MudProperties", "MudRheology", "MudVolumes", "MudVolumeDetails",
							"MultiLangLabel", "MusterPoint", "NonconformanceEvent",
							"NonconformanceEventLog", "Operation", "OperationActualCost",
							"OperationAfe", "OperationBenchmark", "OperationBenchmarkPFactor",
							"OperationBudget", "OperationDvdAnnotation", "OperationInterest",
							"OperationKpi", "OperationObjective", "OperationPlanDetail",
							"OperationPlanMaster", "OperationPlanMasterVault",
							"OperationPlanPhase", "OperationPlanPhaseVault",
							"OperationPlanSetting", "OperationPlanTask", "OperationRole",
							"OperationSummary", "OperationSuspension", "OpsDatum",
							"OpsTeamWarehouse", "Palynology", "Perforation",
							"PerforationGunConfiguration", "PerforationInterval",
							"PerforationStatusLog", "PersonnelOnSite", "PlanOperationTime",
							"Platform", "PlugAndAbandon", "PlugAndAbandonDetail", "PobMaster",
							"PobOrientationLog", "PorePressure", "PositionData",
							"PriorPeriodAdjustment", "ProcessSafetyQuestionnaire", "Product",
							"ProductionFluid", "ProductionPumpParam", "ProductionPumpParamComp",
							"ProductionPumpParamCompLog", "ProductionPumpParamCompServLog",
							"ProductionString", "ProductionStringDetail",
							"ProductionStringDetailLog", "ProductionStringFish",
							"ProductionStringLine", "ProductionStringMandrel",
							"ProductionStringMaterial", "ProductionStringPacker",
							"ProductionStringPressure", "ProductionStringTesting", "Project",
							"ProjectAssociation", "ProjectConsultant", "ProjectInterest",
							"PublishSubscribe", "PFactor", "QueryFilter", "QueryFilterCriteria",
							"QueryFilterCriteriaCondition", "QueryFilterParameter",
							"RawDesorbedGasTest", "RecoveryVolumes", "RegionHistory",
							"RentalEquipment", "ReportDaily", "ReportSignOff", "ReportTypes",
							"RestrictedTrippingDefinition", "RevisionFiles", "RftTest",
							"RigCompressors", "RigCompressorsDetail", "RigCompressorsLog",
							"RigContract", "RigCrane", "RigCraneEngine", "RigCraneEngineDetail",
							"RigCraneEngineLog", "RigEngine", "RigEngineDetail", "RigEngineLog",
							"RigGenerator", "RigGenerators", "RigGeneratorsDetail",
							"RigGeneratorsLog", "RigInformation", "RigIssue", "RigIssueLog",
							"RigOperationStabilityLog", "RigParameters", "RigProperty",
							"RigPump", "RigPumpComponent", "RigPumpComponentLog",
							"RigPumpParam", "RigSlowPumpParam", "RigStateDefinition",
							"RigStateMapping", "RigStateProcessLog", "RigStateRaw", "RigStock",
							"RigSubmersiblePump", "RigSubmersiblePumpDetail",
							"RigSubmersiblePumpLog", "RigTour", "RigTourMaster",
							"RigWaterMaker", "RigWaterMakerDetail", "RigWaterMakerLog",
							"RiserComponents", "RiserRun", "RiserTally", "RiserTension", "Rov",
							"RushmoreTimeInterrupt", "RushmoreWells", "RushmoreWellsCompletion",
							"SafetyTicket", "SafetyTicketCategory", "SafetyTicketDetail",
							"SafetyTicketLog", "SampleDescription", "SandControl",
							"SandControlGravel", "SandControlMaterial", "SchedulerJobDetail",
							"SchematicAnnotation", "SchematicPlan", "SchematicPlanGroup",
							"SchematicPlanValue", "ServerAclAccess", "SidewallCore",
							"SidewallCoreDetail", "SignificantDowntime", "SiteLocation",
							"Slickline", "SolidControlEquipment", "Stimulation",
							"StimulationFluid", "StimulationFracSand", "StimulationInformation",
							"StopCardParticipation", "SubseaDaily", "SupportVesselInformation",
							"SupportVesselParam", "SurfaceCasingVentFlowTest",
							"SurveyReference", "SurveyStation", "Swab", "SwabDetail",
							"SystemBlockUser", "SystemFilter", "SystemFilterGroup",
							"SystemFilterUserData", "SystemSettings", "TargetZone",
							"TempLookupClassCode", "TempLookupPhaseCode",
							"TempLookupRootCauseCode", "TempLookupTaskCode", "Ticket",
							"TicketLog", "TicketWatchList", "TightHoleUser",
							"ToolConfiguration", "ToolStringDetail", "ToolStringMaster",
							"TourAccessGroup", "TourActivity", "TourBharun",
							"TourBharunSummary", "TourBhaComponent", "TourBitrun",
							"TourBitNozzle", "TourBitSummary", "TourCasingSection",
							"TourCasingTally", "TourDaily", "TourDrillingParameters",
							"TourMudProperties", "TourPersonnelOnSite", "TourRigPumpParam",
							"TourRigSlowPumpParam", "TourRigStock", "TourSurveyReference",
							"TourSurveyStation", "TourWireline", "TourWirelineDetail",
							"TransportDailyParam", "TransportMaster", "TransportMovementLog",
							"UnplannedEvent", "UnwantedEvent", "UnwantedEventCompany",
							"Usermenu", "UserAccessibleOperation", "UserAclGroup",
							"UserAnovaGroup", "UserLessonSearch", "UserRigAccess",
							"UserSearchResults", "UserSettings", "UserSignature",
							"VendorAssessment", "VendorAssessmentDetail", "VisnetDashboard",
							"VisnetDashboardLayout", "VisnetDashboardLayoutItem",
							"VisnetDashboardVisnetVisualisation", "VisnetDatabaseOperator",
							"VisnetDatasetDefinition", "VisnetDataSecurity",
							"VisnetDatumUomRelationship", "VisnetFeatureSetting", "VisnetField",
							"VisnetFieldDatasetTargetField", "VisnetFormula", "VisnetProject",
							"VisnetQueryDefinition", "VisnetQueryFilterCriteria",
							"VisnetQueryFilterGroup", "VisnetQueryFilterRelationship",
							"VisnetSimpleRelationship", "VisnetSoftLink", "VisnetStyle",
							"VisnetStyleProperty", "VisnetVisualisation", "VisnetVisualElement",
							"VisnetVisualElementDetailChart",
							"VisnetVisualElementDetailChartAxis",
							"VisnetVisualElementDetailChartDefinition",
							"VisnetVisualElementDetailChartLegend",
							"VisnetVisualElementDetailChartSeries",
							"VisnetVisualElementDetailChartSeriesLabel",
							"VisnetVisualElementDetailColumn",
							"VisnetVisualElementDetailContainer",
							"VisnetVisualElementDetailDrilldown",
							"VisnetVisualElementDetailDrilldownLayer",
							"VisnetVisualElementDetailDrilldownLink",
							"VisnetVisualElementDetailFieldItem",
							"VisnetVisualElementDetailFieldRow",
							"VisnetVisualElementDetailHeaderFooter",
							"VisnetVisualElementDetailHFDefinition",
							"VisnetVisualElementDetailHFItem", "VisnetVisualElementDetailImage",
							"VisnetVisualElementDetailTable",
							"VisnetVisualElementDetailTableSummary",
							"VisnetVisualElementDetailText", "WasteDisposal",
							"WeatherEnvironment", "Well", "Wellbore", "WellborePlugBackLog",
							"WellheadLoadRelief", "WellCompletion", "WellControl",
							"WellControlComponent", "WellControlComponentTest",
							"WellControlStack", "WellFluid", "WellFormationTest", "WellHead",
							"WellHeadDetail", "WellHeadDetailLog", "WellHeadSection",
							"WellProductionTest", "WellProductionTestDetail", "WellTest",
							"WellTransferStatus", "WirelineInterruptions", "WirelineLog",
							"WirelinePreLogging", "WirelinePreLoggingCirculation",
							"WirelinePreLoggingZone", "WirelineRun", "WirelineSuite",
							"WirelineTool", "WirelineToolLookup", "WitsmlChangeLog",
							"WitsmlLink", "WitsmlLinkModuleSelection", "XmasTree", "Zone" };
					query = "DELETE FROM table WHERE isDeleted=:xx";
					for (int i = 0; i < tables.length; i++) {
						String newTable = tables[i];
						String STRSQL = query.replace("table", newTable);
						params = new String[] { "xx" };
						values = new Object[] { true };
						try {
							ApplicationUtils.getConfiguredInstance().getDaoManager()
									.executeByNamedParam(STRSQL, params, values);
						} catch (Exception e) {
							System.out.println("Something went wrong...");
							System.out.println("Error: " + e.toString());
							out.println("<script type=\"text/javascript\">");
							out.println("alert('Error: " + e.toString() + "');");
							out.println("</script>");
							return;
						}
					}
				}
				System.out.println("Successfully executed!");
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Successfully deleted!');");
				out.println("refreshPage();");
				out.println("</script>");
			} catch (Exception e) {
				System.out.println("Something went wrong...");
				System.out.println("Error: " + e.toString());
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Error: " + e.toString() + "');");
				out.println("</script>");
				return;
			}
		}
	%>
	<button onclick="refreshPage()">Refresh page</button>
	<table border="1" width="50%" style="margin: 0 auto">
		<tr>
			<th>Delete Scripts</th>
			<th>Are you sure?</th>
		</tr>
		<tr>
			<td><span
				title="'FileManagerFiles', 'ReportFiles', 'SysAutoUpdateClientStatus', 'SysAutoUpdatePatches', 'SysDataImportLog', 'SysLogDepotTransaction', 'SysLogException', 'SysLogLogin', 'SysLogRigStateXml', 'SysLogSendToTown', 'SysLogUserAccess', 'SysManualUpdatePatches', 'SysRemoteApplicationInfo', 'TransactionLog', 'OpsTeam', 'OpsTeamOperation', 'OpsTeamUser', 'SchedulerTemplate', 'DepotWellOperationMapping'">
					DELETE FROM tables (hard delete) <br /> <sub>*hover to view
						affected tables</sub>
			</span></td>
			<td align="center">
				<button onclick="hardDelete(this)" data-args="hardDeleteTables">Delete</button>

			</td>
		</tr>
		<tr>
			<td>DELETE FROM `user` table (hard delete)</td>
			<td align="center">
				<button onclick="hardDelete(this)" data-args="hardDeleteUserTable">Delete</button>
			</td>
		</tr>
		<tr>
			<td>DELETE FROM table WHERE is_deleted=TRUE (hard delete)</td>
			<td align="center">
				<button onclick="hardDelete(this)"
					data-args="hardDeleteIsDeletedTable">Delete</button>
			</td>
		</tr>
	</table>
</body>
</html>
