<%@ page contentType="text/plain"%>
<%@ page import="java.text.SimpleDateFormat, 
com.idsdatanet.d2.core.web.mvc.*,org.apache.commons.beanutils.BeanUtils,
com.idsdatanet.d2.core.uom.CustomFieldUom,
com.idsdatanet.d2.core.dao.QueryProperties,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils, com.idsdatanet.d2.core.web.mvc.CommandBean"%>
<%

try {	
	String queryString = "From UnwantedEvent WHERE (isDeleted=false or isDeleted is null) and type='UE'";
	List<UnwantedEvent> uelist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
	int count = 0;
	double totalrec = uelist.size();
	double progress = 0.0;
	if(uelist.size()>0){
		for (UnwantedEvent ue : uelist){
			count = count + 1;
			progress = (count / totalrec) * 100;
			String updateActNpt = ue.getUnwantedEventUid();
			ue.setType("nptEvent");
			if (StringUtils.isNotBlank(ue.getEventCause()) && ue.getEventCause()!=null) ue.setEventSummary(ue.getEventCause());
			ue.setEventCause("");
			
			if ("C".equals(ue.getEventStatus())) ue.setEventStatus("closed");
			if ("O".equals(ue.getEventStatus())) ue.setEventStatus("open");
			if ("I".equals(ue.getEventStatus())) ue.setEventStatus("inProgress");
			if ("N".equals(ue.getEventStatus())) ue.setEventStatus("new");
			
			String queryString1 = "From ActivityUnwantedEventLink WHERE (isDeleted=false or isDeleted is null) and unwantedEventUid='"+ updateActNpt + "'";
			List<ActivityUnwantedEventLink> actuntlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString1);
			
			if (actuntlist.size()==0) {
				ue.setIsDeleted(true);
				ue.setSysDeleted(3);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ue);
			
			%>
			<%=count %>/<%=totalrec %> = <%=progress %>
			<%
			for (ActivityUnwantedEventLink auel : actuntlist){

				String queryString2 = "From Activity WHERE (isDeleted=false or isDeleted is null) and activityUid='"+ auel.getActivityUid() + "'";
				List<Activity> actlist = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString2);
				if (actlist.size()>0){
					Activity act = actlist.get(0);
					act.setNptEventUid(updateActNpt);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(act);
				}
			
			}
			
		}
		
	}



		
				
				
			
	
%>-- SUCCESS

<%
	} catch (Exception e) {
%>-- FAILED-
<%=e.getMessage()%><%

	}
%>