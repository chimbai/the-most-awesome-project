<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" import="com.idsdatanet.d2.core.web.mvc.*, com.idsdatanet.d2.core.wap.*, com.idsdatanet.d2.core.web.mvc.view.ClientBrowserUtil, org.apache.commons.lang.StringUtils, com.idsdatanet.d2.common.i18n.MultiLangUtils, java.util.List, java.util.Locale, net.sf.json.JSONObject, net.sf.json.JSONArray" %>
<%@ page import="java.io.*,java.util.*, java.text.*, com.idsdatanet.d2.core.dao.*, com.idsdatanet.d2.core.query.generators.MSSQLDatabaseManager, com.idsdatanet.d2.common.util.*, com.idsdatanet.depot.witsml.rts.RTSFileProcessInfo" %>

<% 
try{
	UserSession userSession = UserSession.getInstance(request, false);
} catch (Exception x){
	%> 
	<script>		
		window.location = "login.jsp";
	</script> 
	<%
}

if(StringUtils.isNotBlank(request.getParameter("action")) && request.getParameter("action").equals("loadRsdStates")){
	String operationUid = request.getParameter("operationUid");
	
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat datetimeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date dayDate = dateformat.parse(request.getParameter("dayDate").toString());
	
	JSONObject outputResponse = new JSONObject();
	JSONArray rsdStates = new JSONArray();
	try{
		boolean isMysql = MSSQLDatabaseManager.getConfiguredInstance().isMySql;
		String hql = "SELECT rsr.rigStateRawUid, rsm.serviceLevel, rsm.description, rsr.startTime, rsr.endTime, rsr.activityRsdUid, rsr.lastEditMilliseconds FROM RigStateRaw rsr, RigStateMapping rsm WHERE (rsm.isDeleted IS NULL OR rsm.isDeleted IS FALSE) AND (rsr.rigStateId=rsm.rigStateId AND rsr.rigStateDefinitionUid=rsm.rigStateDefinitionUid) AND rsr.operationUid=:operationUid AND CAST(rsr.startTime as date)=:dayDate GROUP BY rsr.rigStateRawUid, rsm.serviceLevel, rsm.description, rsr.startTime, rsr.endTime, rsr.activityRsdUid, rsr.lastEditMilliseconds ORDER BY rsr.lastEditMilliseconds";
		if(!isMysql) { hql = "SELECT rsr.rigStateRawUid, rsm.serviceLevel, rsm.description, rsr.startTime, rsr.endTime, rsr.activityRsdUid, rsr.lastEditMilliseconds FROM RigStateRaw rsr, RigStateMapping rsm WHERE (rsm.isDeleted IS NULL OR rsm.isDeleted IS FALSE) AND (rsr.rigStateId=rsm.rigStateId AND rsr.rigStateDefinitionUid=rsm.rigStateDefinitionUid) AND rsr.operationUid=:operationUid AND CONVERT(DATE,rsr.startTime)=:dayDate GROUP BY rsr.rigStateRawUid, rsm.serviceLevel, rsm.description, rsr.startTime, rsr.endTime, rsr.activityRsdUid, rsr.lastEditMilliseconds ORDER BY rsr.lastEditMilliseconds";}
		List <Object[]> states = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"operationUid", "dayDate"}, new Object[] {operationUid, dayDate});
		
		JSONObject rsdState = new JSONObject();
		for(Object[] state : states){
			String rigStateRawUid =  state[0].toString();
			String serviceLevel = state[1].toString();
			String description = state[2].toString();
			Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(state[3].toString());
			String startTime = datetimeformat.format(start);
			Date end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(state[4].toString());
			String endTime = datetimeformat.format(end);
			String activityRsdUid = (state[5] == null ? null : state[5].toString());
			String activityStartTime = null;
			String activityEndTime = null;
			
			Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
			
			Date d = new Date(thisDuration * 1000L);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss"); // HH for 0-23
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String duration = df.format(d);
			
			if(StringUtils.isNotBlank(activityRsdUid)){
				String act = "SELECT CAST(startDatetime as time), CAST(endDatetime as time) FROM ActivityRsd WHERE activityRsdUid=:activityRsdUid AND (isDeleted IS NULL OR isDeleted=FALSE)";
				List<Object[]> activities = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(act, "activityRsdUid", activityRsdUid);
				Object[] activity = null;
				if(activities.size() > 0){
					activity = activities.get(0);
					activityStartTime = activity[0].toString();
					activityEndTime = activity[1].toString();
				}
			}
			rsdState.put("rigStateRawUid", rigStateRawUid);
			rsdState.put("serviceLevel", serviceLevel);
			rsdState.put("description", description);
			rsdState.put("startTime", startTime);
			rsdState.put("endTime", endTime);
			rsdState.put("duration", duration);
			rsdState.put("activityRsdUid", activityRsdUid);
			rsdState.put("activityStartTime", activityStartTime);
			rsdState.put("activityEndTime", activityEndTime);
			rsdStates.add(rsdState);
		}
		outputResponse.put("rsdStates", rsdStates);
		outputResponse.put("success", true);
		states = null;
	}catch(Exception x){
		outputResponse.put("success", false);
		outputResponse.put("responseMessage", x.getMessage());
		x.printStackTrace();
	}finally{
		response.setContentType("application/json");
		response.getOutputStream().write(outputResponse.toString().getBytes("utf-8"));
	}
	return;
} 
%>
<html>
<script type="text/javascript" src="scripts/system-v2.js"></script>
<script type="text/javascript" src="scripts/reflection.js"></script>
<script src="<%= request.getContextPath() %>/vresources/jartimestamp/<%=String.valueOf(SystemStartupTime.getValue())%>/htmllib/sumoselect/jquery.sumoselect.js"></script>
<script src="<%= request.getContextPath() %>/vresources/jartimestamp/<%=String.valueOf(SystemStartupTime.getValue())%>/htmllib/styles/placeholders.min.js"></script>
<link href="<%= request.getContextPath() %>/vresources/jartimestamp/<%=String.valueOf(SystemStartupTime.getValue())%>/htmllib/styles/screen.css" media="all" rel="stylesheet" type="text/css" />
<link href="<%= request.getContextPath() %>/vresources/jartimestamp/<%=String.valueOf(SystemStartupTime.getValue())%>/htmllib/sumoselect/sumoselect.css" rel="stylesheet" />

<script type="text/javascript">	
	$(document).ready(function(){
		createFixedHeader("#dailySummary");
		createFixedHeader("#dailyRsdStates");
		rescale();
		window.Search = $('.search-box').SumoSelect({search: true, searchText: 'SELECT WELL/OPS'});
	});
	
	function createFixedHeader(id){
	    var counter = 0;
	    $(id+"Header th").each(function(){
	        var width = $('.xScroll tr:last td:eq(' + counter + ')').width();
	        $(id+"NewHeader").append(this);
	        this.width = width;
        	counter++;
    	});
	    if(id == "#dailyRsdStates") {
	    	$("#divNewHeaders").show();	
	    }
	}
	
	function rescale(){
		var tables = document.getElementsByClassName("Table");
		for (var i = 0; i < tables.length; i++) {
			tables[i].style.height = (screen.height * 0.55);	
		}
		
	}
	
	function highlight(object){
		$("input:radio[name='view']").each(function(i) {
		     this.parentNode.parentNode.style.backgroundColor = '';
		});
		
		object.parentNode.parentNode.style.backgroundColor = 'grey';
	}
	
	function loadRsdStates(operationUid, dayDate, button){
		$( "#popup" ).show();
		highlight(button);
		var data = {
	            action: 'loadRsdStates',
				operationUid: operationUid,
	            dayDate: dayDate
	    };
		
		$.post("rsdSummary.jsp", data, function (data, status) {
            if(data.success){
            	loadData(data);
            	rescale();
            } else {
            	alert("Error with message: " + data.responseMessage);
            }
        });
	}
	
	function loadData(response)
    {
		var tableBody = document.getElementById("dailyRsdStates").getElementsByTagName('tbody')[0];
		var rsdStates = response.rsdStates;
		clearTable(tableBody);
		
		for (var i = 0; i < rsdStates.length; i++){
			var state = rsdStates[i];
			var row = tableBody.insertRow(i);
			row.className = "DataGridDataRowFH AlternateDataRowBackground";
			var cell1 = row.insertCell(0);
			cell1.className = "Font";
			cell1.style.width = "10%";
			cell1.innerHTML = state.rigStateRawUid;
			var cell2 = row.insertCell(1);
			cell2.className = "Font";
			cell2.style.width = "10%";
			cell2.style.textAlign = "left";
			cell2.style.paddingLeft = "2px";
			cell2.innerHTML = state.serviceLevel;
			var cell3 = row.insertCell(2);
			cell3.className = "Font";
			cell3.style.width = "16%";
			cell3.style.textAlign = "left";
			cell3.style.paddingLeft = "2px";
			cell3.innerHTML = state.description;
			var cell4 = row.insertCell(3);
			cell4.className = "Font";
			cell4.style.width = "14%";
			cell4.innerHTML = state.startTime;
			var cell5 = row.insertCell(4);
			cell5.className = "Font";
			cell5.style.width = "14%";
			cell5.innerHTML = state.endTime;
			var cell6 = row.insertCell(5);
			cell6.className = "Font";
			cell6.style.width = "9%";
			cell6.innerHTML = state.duration;
			var cell7 = row.insertCell(6);
			cell7.className = "Font";
			cell7.style.width = "9%";
			cell7.innerHTML = state.activityRsdUid == undefined ? "" : state.activityRsdUid;
			var cell8 = row.insertCell(7);
			cell8.className = "Font";
			cell8.style.width = "9%";
			cell8.innerHTML = state.activityStartTime == undefined ? "" : state.activityStartTime;
			var cell9 = row.insertCell(8);
			cell9.className = "Font";
			cell9.style.width = "9%";
			cell9.innerHTML = state.activityEndTime == undefined ? "" : state.activityEndTime;
		}
		createFixedHeader("#dailyRsdStates");	
		$( "#popup" ).hide();
	}
	
	function clearTable(tableObj){
		for(var i = 0; tableObj.rows.length != 0; i++){
			tableObj.deleteRow(0);
		}
	}
</script>
<style type="text/css">
 	html, body {
 		font-family: Helvetica !important;
 		font-size: 12px;
 		height: 100%;
 		width: 100%;
 		margin: 0;
 		padding: 0;
 		border: 0;
  		overflow: hidden; 
 	}
 	.Table {
 		height: calc(100% - 280px);
    	overflow-y:scroll;
 	}
 	.NewHeader {
 		border-top: 1px solid black;
 		border-left: 1px solid black;
 		border-right: 1px solid black;
 		border-collapse: collapse;
 		width:100%;
 		vertical-align: middle;
 		z-index: -1;
 	}
 	.Headers{
 		width:100%;
 	}
 	.Headers th{
  		border-bottom: 1px solid black;
		padding:0 2px 0 0;
 	}
	
	.xScroll td {
		text-align: center;
 		padding-left: 0px;
 		border: 1px solid black;
	}
	.Font{
		font-size: 10pt;
 		padding-left: 10px; 
	}*
	.HeaderFont {
		font-weight: bold;
		font-family: Helvetica !important;
		font-size: 10pt;
	}
	.TableDataGridFloatHeaderCell {
		top: 0;
		padding: 0 8px 0 0;
		white-space: nowrap;
	}
	.DataGridDataRowFH td{
    	padding: 5px 0 5px 0;
	}
	.AlternateDataRowBackground {
	    background-color: #E6E6E6;
	}
	.DataGridDataRowFH {
    	border-bottom: 1px solid #EEEEEE;
	}
	
	.DataGridDataRowFH:hover {
	    background-color: #FAFAFA;
	}
	.FTRProcessInfoTD td {
		text-align: center;
	}
	#mainContentPane-west,
	.ChildContainer,
	.ChildContainerBody {
	    padding: 0 !important;
	}
	.ChildContainer {
	    width: 100%;
	    margin: 0px;
	    padding: 0;
	}
	.ChildContainerBody {
	    vertical-align: top;
	    padding: 5px 0px 0px 0px;
	}
	.ChildContainerBody .BaseNodesGroupContainer {
	    margin-left: 3px;
	    margin-right: 3px;
	    margin-top: 3px;
	}
	.mainContentContainer > .ChildContainer,
	.ChildContainerBody > .BaseNodesGroupContainer {
	    margin-top: 0 !important;
	}
	.BaseNodesGroupContainerHeader {
	    background: #5c9ccc !important;
	    -webkit-border-top-left-radius: 2px;
	    -webkit-border-top-right-radius: 2px;
	    -moz-border-radius-topleft: 2px;
	    -moz-border-radius-topright: 2px;
	    border-top-left-radius: 2px;
	    border-top-right-radius: 2px;
	    height: 29px;
	    color: white;
	    width: 100%;
	}
	.BaseNodesGroupContainerHeaderText {
	    display: inline-block;
	    color: #FFFFFF;
	    font-size: 13px;
	    font-family: Helvetica, Verdana, Arial;
	    margin: 0;
	    min-height: 18px;
	    padding: 4px 8px;
	    white-space: nowrap;
	    vertical-align: middle;
	}
	.fieldRenderer {
	    padding: 2px;
	    margin: 0px;
	    display: inline-block;
	    min-height: 16px;
	    font-family: Helvetica, Verdana, sans-serif;
	    font-size: 12px;
	    white-space: pre-wrap;
	    width: 100%;
	    background-color: #E6E6E6;
	}
	.HtmlTableGrid {
	    width: 100%;
	    border-collapse: collapse;
	    margin: 0px;
	}
	.HtmlTableFieldHeaderItem {
	    padding: 2px;
	    vertical-align: top;
	    background: #c3c3c3;
	    color: #333;
	    padding: 2px;
	    min-height: 16px;
	    border: 1px solid #CCCCCC;
	    min-width: 50px;
	}
	.HtmlTableHeaderItem {
	    padding: 2px;
	    vertical-align: top;
	    background: #DADFE1 !important;
	    color: #2C3E50;
	    font-weight: bold;
	    padding: 2px;
	    min-height: 16px;
	    border: 1px solid #CCCCCC;
	    min-width: 50px;
	}
	.loader {
	    border: 16px solid #f3f3f3; /* Light grey */
	    border-top: 16px solid #3498db; /* Blue */
	    border-radius: 50%;
	    width: 60px;
	    height: 60px;
	    animation: spin 2s linear infinite;
	}
	
	@keyframes spin {
	    0% { transform: rotate(0deg); }
	    100% { transform: rotate(360deg); }
	}
	.popup{
		display:none;
		color: black;
 		background: #E6E6E6;  
 		padding: 5px; 
		position: fixed;
		top: 40%;
		left: 45%;
		z-index: 100;
		margin-right: -25%;
		margin-bottom: -25%;
	}
	.SumoSelect{
		color:black;
		width:500px;
	}
</style>
<body>
<div id="popup" class="popup">
 <div class="loader"></div>
 	<span style="padding-top:70px;font-weight:bold;">Loading...</span>
 </div> 
<div>
<%
QueryProperties qp = new QueryProperties();
qp.setRowsToFetch(0, 3);
String queryLastReceived = "SELECT lastEditDatetime, incomingXml FROM SysLogRigStateXml ORDER BY lastEditDatetime DESC";
List<Object[]> lastReceived = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryLastReceived, qp);

%>
<table class="HtmlTableGrid"><tbody>
	<tr><th class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0" colspan="2"><div class="BaseNodesGroupContainerHeaderText">Last Received From Source</div></th></tr>
<%
if(lastReceived.size() <= 0){
%>
	<tr class="DataGridDataRowFH AlternateDataRowBackground"><td style="text-align:center" colspan="2">None</td></tr>
<%	
}
DateFormat datetimeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
for(Object[] row : lastReceived){
		Date lastEdit = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(row[0].toString());
		String lastEditDatetime = datetimeformat.format(lastEdit);
		String xml = StringUtils.substring(row[1].toString(), 0, 200) + "...";
		
		pageContext.setAttribute("lastEditDatetime", lastEditDatetime);
		pageContext.setAttribute("xml", xml);
%>		
		<tr class="DataGridDataRowFH AlternateDataRowBackground"><td class="HtmlTableFieldHeaderItem HeaderFont Font" style="text-align:right"><c:out value="${lastEditDatetime}"/></td><td class="Font"><c:out value="${xml}"/></td></tr>

<% 
} 

lastReceived = null;

List operationList = new ArrayList();
String sql = "SELECT operationUid FROM RigStateRaw GROUP BY operationUid";						
List list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
for (Object rec : list) {
	operationList.add(rec.toString());
}
										
String queryOpsWell = "SELECT o.operationUid, w.wellName, wb.wellboreName, o.operationName " +  
						" FROM Operation o, Well w, Wellbore wb WHERE o.wellUid = w.wellUid " +
						" AND o.wellboreUid = wb.wellboreUid " +
						" AND w.wellUid = wb.wellUid " +	
						" AND (o.isDeleted is null or o.isDeleted=false) "+
						" AND (w.isDeleted is null or w.isDeleted=false) "+
						" AND (wb.isDeleted is null or wb.isDeleted=false) "+
						" AND o.operationUid in (:operationList) "+
						" GROUP BY o.operationUid, w.wellName, wb.wellboreName, o.operationName ORDER BY 2,3,4";
List<Object[]> wellOps = ApplicationUtils.getConfiguredInstance().getDaoManager().
findByNamedParam(queryOpsWell, new String[] {"operationList"}, new Object[] {operationList});

if(StringUtils.isNotBlank(request.getParameter("selWellOps"))){
	//Operation Summary
	String operationUid = request.getParameter("selWellOps");
	
	String hql = "SELECT MIN(startTime), max(endTime), min(lastEditDatetime), max(lastEditDatetime), (COUNT(rigStateRawUid) - SUM(CAST(isSlicedByTour as int))),COUNT(rigStateRawUid) FROM RigStateRaw WHERE operationUid = :operationUid GROUP BY operationUid";
	List <Object[]> rsrOps = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "operationUid", operationUid);
	if(rsrOps.size() > 0) {
		Object[] values = rsrOps.get(0);
		
		Date rawDate = null;
		if(values[0] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[0].toString());
			pageContext.setAttribute("OperationSummary_startTime", datetimeformat.format(rawDate));
		} else {
			pageContext.setAttribute("OperationSummary_startTime", null);
		}
		
		if(values[1] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[1].toString());
			pageContext.setAttribute("OperationSummary_endTime", datetimeformat.format(rawDate));
		} else {
			pageContext.setAttribute("OperationSummary_endTime", null);
		}
		
		if(values[2] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[2].toString());
			pageContext.setAttribute("OperationSummary_payloadSyncStartTime", datetimeformat.format(rawDate));
		} else {
			pageContext.setAttribute("OperationSummary_payloadSyncStartTime", null);
		}
		
		if(values[3] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[3].toString());
			pageContext.setAttribute("OperationSummary_payloadSyncEndTime", datetimeformat.format(rawDate));
		} else {
			pageContext.setAttribute("OperationSummary_payloadSyncEndTime", null);
		}
		
		if (values[4]!=null) pageContext.setAttribute("OperationSummary_totalRsdStates", values[4].toString());
		else pageContext.setAttribute("OperationSummary_totalRsdStates", 0);
		pageContext.setAttribute("OperationSummary_totalStates", values[5].toString());
		
		hql = "SELECT MIN(startDatetime), MAX(endDatetime), COUNT(DISTINCT dailyUid), COUNT(activityRsdUid) FROM ActivityRsd WHERE operationUid=:operationUid GROUP BY operationUid";
		List <Object[]> actvitySum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "operationUid", operationUid);
		if(actvitySum.size() > 0) {
			values = actvitySum.get(0);
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[0].toString());
			pageContext.setAttribute("OperationSummary_rsdActivityStartTime", datetimeformat.format(rawDate));
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(values[1].toString());
			pageContext.setAttribute("OperationSummary_rsdActivityEndTime", datetimeformat.format(rawDate));	
			pageContext.setAttribute("OperationSummary_totalDaysCreated", values[2].toString());
			pageContext.setAttribute("OperationSummary_totalRsdActivitiesCreated", values[3].toString());
		}
	}
} //if
%>


</div>


<table class="HtmlTableGrid">
	<tbody>
		<tr>
			<th class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0" colspan="7">
				<div class="BaseNodesGroupContainerHeaderText">FTR Process Info</div>
			</th>
		</tr>
		<tr>
			<th class="SimpleGridHeaderCell">Activity RSD UID</th>
			<th class="SimpleGridHeaderCell">Operation Name</th>
			<th class="SimpleGridHeaderCell">Day Date</th>
			<th class="SimpleGridHeaderCell">Start Time</th>
			<th class="SimpleGridHeaderCell">End Time</th>
			<th class="SimpleGridHeaderCell">Duration</th>
			<th class="SimpleGridHeaderCell">Errors</th>
		</tr>
		<%= RTSFileProcessInfo.getHtmlContentForProcessInfo() %>		
	</tbody>
</table>


<table class="ChildContainer" style="width:100%; height:100%;">
<tr><td class="ChildContainerBody" colspan="2" style="height:140px">
	<table class="HtmlTableGrid" style="table-layout:fixed;"><tbody>
		<tr><th class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0" colspan="4"><div class="BaseNodesGroupContainerHeaderText">
			<form id = "frmWellOps" action = "rsdSummary.jsp" method = "POST">
				Operation Summary: <select id="selWellOps" name="selWellOps" class="search-box" onchange="this.form.submit()"> 
				<option value="" >&nbsp;</option>
				<%
				for (int i=0;i<wellOps.size();i++) {
				  Object[] s = wellOps.get(i);
				  String key = s[0].toString();
				  String opsWellValue = s[1] + " > " + s[2] + " > " + s[3];
				  pageContext.setAttribute("key", key);
				  pageContext.setAttribute("opsWellValue", opsWellValue);
				%>
				<option value="<c:out value='${key}'/>" <% if(StringUtils.isNotBlank(request.getParameter("selWellOps")) && request.getParameter("selWellOps").equals(key)) { %> selected="selected" <% } %> ><c:out value='${opsWellValue}'/> </option>	
				<%
				}
				wellOps = null;
				%>
				</select>
			</form>
		</div></th></tr>
		<tr class="HtmlTableRow"><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Total Days with Activity RSD</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value='${OperationSummary_totalDaysCreated}' /></div></td><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Total RSD Activities Created</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_totalRsdActivitiesCreated}" /></div></td></tr>
		<tr class="HtmlTableRow"><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">RSD Start</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value='${OperationSummary_startTime}' /></div></td><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Activity Start</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_rsdActivityStartTime}" /></div></td></tr>
		<tr class="HtmlTableRow"><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">RSD End</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_endTime}" /></div></td><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Activity End</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_rsdActivityEndTime}" /></div></td></tr>
		<tr class="HtmlTableRow"><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Total States From Source</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_totalRsdStates}" /></div></td><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Payload / Sync Start</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_payloadSyncStartTime}" /></div></td></tr>
		<tr class="HtmlTableRow"><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Total States After Tour-Split</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_totalStates}" /></div></td><td class="HtmlTableFieldHeaderItem" style="width:15%"><span class="HtmlTableLabel HeaderFont Font">Payload / Sync End</span></td><td class="HtmlTableGridItem" style="width:25%"><div class="fieldRenderer"><c:out value="${OperationSummary_payloadSyncEndTime}" /></div></td></tr>
	</tbody></table>
</td></tr>
<tr style="height:100%"><td class="ChildContainerBody">
	<div class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0" colspan="7"><div class="BaseNodesGroupContainerHeaderText">Daily Summary</div></div>
	<div class="Headers">
		<table id="dailySummaryNewHeader" class="NewHeader">
		    <tr>
		    </tr>
		</table>
	</div>
	<div class="Table">
		<table id="dailySummary" class="HtmlTableGrid xScroll">
			<tr id="dailySummaryHeader">
				<th class=" HeaderFont" style="width:9%">View</th>
				<th class=" HeaderFont" style="width:20%">Day</th>
				<th class=" HeaderFont" style="width:15%"># of States From Source</th>
				<th class=" HeaderFont" style="width:12%"># of Activities</th>
				<th class=" HeaderFont" style="width:22%">Payload / Sync Start</th>
				<th class=" HeaderFont" style="width:22%">Payload / Sync End</th>
				<th><div style="width:10px">&nbsp;</div></th>
			</tr>
<% 
if(StringUtils.isNotBlank(request.getParameter("selWellOps"))){
	//Daily Summary
	String operationUid = request.getParameter("selWellOps");
	String hql = "SELECT MAX(CAST(startTime as date)), COUNT(rigStateRawUid), MIN(lastEditDatetime), MAX(lastEditDatetime) FROM RigStateRaw WHERE operationUid=:operationUid AND isSlicedByTour=0 GROUP BY year(startTime),month(startTime),day(startTime) ORDER BY year(startTime),month(startTime),day(startTime)";
	List <Object[]> rigStateRaws = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "operationUid", operationUid);
	
	for(Object[] rigStateRaw : rigStateRaws){
		int counter = 0;
		String reportDatetime = rigStateRaw[0].toString();
		Date rawStartTime = new SimpleDateFormat("yyyy-MM-dd").parse(reportDatetime);
		String numberOfStates = rigStateRaw[1].toString();
		
		Date rawDate = null;
		String payloadSyncStart = null;
		String payloadSyncEnd = null;
		if(rigStateRaw[2] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rigStateRaw[2].toString());
			payloadSyncStart = datetimeformat.format(rawDate);
		}
		if(rigStateRaw[3] != null) {
			rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rigStateRaw[3].toString());
			payloadSyncEnd = datetimeformat.format(rawDate);
		}
		
		hql = "SELECT reportNumber, CAST(reportDatetime as date), dailyUid FROM ReportDaily WHERE reportDatetime = :reportDatetime and operationUid=:operationUid AND isDeleted IS NULL AND sysDeleted IS NULL";
		List <Object[]> reportDailys = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"reportDatetime", "operationUid"}, new Object[] {rawStartTime, operationUid});
		
		String reportNumber = "";
		String dailyUid = "";
		String numberOfActivities = "0";
		if(reportDailys.size() > 0){
			Object[] reportDaily = reportDailys.get(0);
			reportNumber = reportDaily[0].toString();
			dailyUid = reportDaily[2].toString();
			
			boolean isMysql = MSSQLDatabaseManager.getConfiguredInstance().isMySql;
			hql = "SELECT COUNT(act.activityRsdUid) FROM ActivityRsd act, RigStateRaw rsr, ReportDaily rd " + 
					  "WHERE (DATE(rsr.startTime)=rd.reportDatetime AND rsr.operationUid=rd.operationUid AND rd.isDeleted IS NULL AND rd.sysDeleted IS NULL) " +
							"AND rsr.isSlicedByTour=0 " +
							"AND act.activityRsdUid=rsr.rigStateRawUid AND act.dailyUid=rd.dailyUid AND rd.dailyUid=:dailyUid";
			
			if(!isMysql) { hql = "SELECT COUNT(act.activityRsdUid) FROM ActivityRsd act, RigStateRaw rsr, ReportDaily rd " + 
						  		"WHERE (convert(DATE,rsr.startTime)=rd.reportDatetime AND rsr.operationUid=rd.operationUid AND rd.isDeleted IS NULL AND rd.sysDeleted IS NULL) " +
								"AND rsr.isSlicedByTour=0 AND act.activityRsdUid=rsr.rigStateRawUid AND act.dailyUid=rd.dailyUid AND rd.dailyUid=:dailyUid"; }
				

			List <Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "dailyUid", dailyUid);
			Object values = result.get(0);
			numberOfActivities = values.toString();
		}
		String day = reportDatetime;
		if(StringUtils.isNotBlank(reportNumber)){
			day = '#' + reportNumber + " (" + reportDatetime + ")";
		} 
				
		pageContext.setAttribute("day", day);
		pageContext.setAttribute("numberOfStates", numberOfStates);
		pageContext.setAttribute("payloadSyncStart", payloadSyncStart);
		pageContext.setAttribute("payloadSyncEnd", payloadSyncEnd);
		pageContext.setAttribute("operationUid", operationUid);
		pageContext.setAttribute("dayDate", reportDatetime);
		pageContext.setAttribute("numberOfActivities", numberOfActivities);
%>		
		<tr class="DataGridDataRowFH AlternateDataRowBackground">
			<td style="text-align:center; width:9%"><input name="view" type="radio" onclick="loadRsdStates('<c:out value="${operationUid}"/>', '<c:out value="${dayDate}"/>', this)"/></td>
			<td class="Font" style="width:20%"><c:out value="${day}"/></td>
			<td class="Font" style="width:15%"><c:out value="${numberOfStates}"/></td>
			<td class="Font" style="width:12%"><c:out value="${numberOfActivities}"/></td>
			<td class="Font" style="width:22%"><c:out value="${payloadSyncStart}"/></td>
			<td class="Font" style="width:22%"><c:out value="${payloadSyncEnd}"/></td>
		</tr>
<% 
		counter++;
	} 
	
	
}
%>
		</table>
	</div>
	
</td><td class="ChildContainerBody" style="width: 60%;">
		<div class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0"><div class="BaseNodesGroupContainerHeaderText">Daily RSD States</div></div>
		<div id="divNewHeaders" class="Headers" style="display:none;">
			<table class="NewHeader">
				<tr>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:70%;border-right:1px solid black;" colspan="6">RSR/State</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:30%;" colspan="4">Created/Merged to Activity</th>
				</tr>
			    <tr id="dailyRsdStatesNewHeader">
			    </tr>
			</table>
		</div>
		<div class="Table" style="width:100%">
			<table id="dailyRsdStates" class="HtmlTableGrid xScroll">	
				<tr id="dailyRsdStatesHeader">
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:22%">UID</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:10%">Service Level</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:12%">Description</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:10%">Start</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:10%">End</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:9%;border-right:1px solid black;">Duration</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:9%">UID</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:9%">Start Time</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font" style="width:9%">End Time</th>
					<th class="TableDataGridFloatHeaderCell HeaderFont Font"><div style="width:10px">&nbsp;</div></th>
				</tr>
			</table>
		</div>
</td></tr>
</table>
</body>
</html>
