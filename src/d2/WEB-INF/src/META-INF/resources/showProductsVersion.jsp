<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<body>

<table border="1">
	<tr>
		<th>Products</th>
		<th>Version</th>
		<th>Build</th>
	</tr>
	
<%
	Map<String,String> jarFiles = ApplicationConfig.getConfiguredInstance().getLibsVersion();
	Map<String,String> buildId = ApplicationConfig.getConfiguredInstance().getLibsBundleId();

	for (Map.Entry<String,String> jar : jarFiles.entrySet())
	{
		%><tr>
			<td><%=jar.getKey()%></td>
			<td><%=jar.getValue()%></td>
		<%
		for (Map.Entry<String,String> id : buildId.entrySet()){
			if(id.getKey()==jar.getKey()){
				%><td><%=id.getValue()%></td><%
			}
		}
		%></tr><%
	}
%>
</table>

</body>
</html>