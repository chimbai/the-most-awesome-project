<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="org.apache.commons.lang.StringUtils, java.util.Scanner,com.idsdatanet.depot.edm.EDMManager, com.idsdatanet.depot.edm.sync.catalog.EDMCatalogManager,com.idsdatanet.depot.core.util.DepotUtils, com.idsdatanet.d2.core.web.mvc.ApplicationConfig,java.sql.*,java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>IDS DataNet - Update Information</title>
	<link rel="icon" type="image/png" href="images/favicon.ico"/>
	<link rel="shortcut icon" href="images/favicon.ico"/>
	<style type="text/css">
	</style>
</head>
<body>

<%
String releaseDate = "";
String releaseId = "";
String filePath = ApplicationConfig.getConfiguredInstance().getServerRootPath()	+ "WEB-INF/config/update.ini";
try {
	Scanner scanner = new Scanner(new FileInputStream(filePath), "UTF-8");
	while (scanner.hasNextLine()) {
		String line = scanner.nextLine();
		if (line != null) {
			line = line.trim();
			if (StringUtils.isNotBlank(line)) {
				if (line.startsWith("update-id")) {
					releaseId = line.substring(12);
				}
				if (line.startsWith("date-time")) {
					releaseDate = line.substring(12);
					//releaseDate = releaseDate.replace(/\\/g, '');
				}
			}
		}
	}
	scanner.close();
} 
catch (FileNotFoundException e) {
	releaseId = "File not found!";
}
%>

<table border="1" cellpadding="8">
	<tr>
		<td>Last Updated Date &amp; Time</td>
		<td><%= releaseDate %></td>
	</tr>
	<tr>
		<td>Last Updated ID</td>
		<td><%= releaseId %></td>
	</tr>
</table>

</body>
</html>
