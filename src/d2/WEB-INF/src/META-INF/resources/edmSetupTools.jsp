<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="org.apache.commons.lang.StringUtils, java.util.Scanner,com.idsdatanet.depot.edm.EDMManager, com.idsdatanet.depot.edm.sync.catalog.EDMCatalogManager,com.idsdatanet.depot.core.util.DepotUtils, com.idsdatanet.d2.core.web.mvc.ApplicationConfig,java.sql.*,java.io.*"%>
	
<%!
	public boolean isDebug(HttpServletRequest request) {
		String debug = request.getParameter("debug");
		return (StringUtils.isNotBlank(debug) && debug.equalsIgnoreCase("true"));
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDM Setup Tools</title>
<style type="text/css">
body {
	font-family: Helvetica, Courier, Arial;
}

.main {
	border-collapse: collapse;
}

.main th {
	background-color: #efefef;
	border: 1px solid #cecece;
	padding: 3px;
}

.main td {
	padding: 5px;
	border: 1px solid #cecece;
}

.main button {
	margin: auto;
	display: block;
}

.main .index {
	text-align: center;
}

.main .status {
	text-align: center;
}
</style>
</head>
<body>
	<%
		String key = null;
		String error = null;
		String status = null;
		
		Connection conn = null;
		Statement statement = null;
		File script = null;
		try {
			key = request.getParameter("setup");
			if (key != null) {
				if (StringUtils.equals(key, "bhacomponent")) {
					conn = EDMManager.getConfiguredInstance()
							.getDataSource().getConnection();
					if (conn != null) {
						statement = conn.createStatement();
					}
					if (statement == null) throw new Exception("Failed to get database connection.");

					String filepath = ApplicationConfig.getConfiguredInstance().getServerRootPath()	+ "WEB-INF/edm/sql/lookup_bha_component_migration.sql";
					StringBuilder text = new StringBuilder();
					Scanner scanner = new Scanner(new FileInputStream(filepath), "UTF-8");
					try {
						while (scanner.hasNextLine()) {
							String nextLine = scanner.nextLine();
							if (nextLine != null) {
								nextLine = nextLine.trim();
								if (StringUtils.isNotBlank(nextLine)) {
									statement.addBatch(nextLine);
								}
							}
						}
					} finally {
						scanner.close();
					}
					if (statement != null) {
						statement.executeBatch();
					}
				} else if (StringUtils.equals(key, "edmcatalog")) {
					EDMCatalogManager.getConfiguredInstance().run("GET", isDebug(request));
				}
			}
		} catch (Exception e) {
			error = DepotUtils.getStackTrace(e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e2) {
				error = DepotUtils.getStackTrace(e2);
			}
			status = (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(error)) ? status = "<span style='color:red;'><b>Failed</b></span>" : "<span style='color:green;'><b>Success</b></span>";
		}
	%>
	<h2>EDM Setup Tools</h2>
	<%
		if (StringUtils.isNotBlank(error)) {
	%>
	<pre style="border: 1px solid red; color: red;"><%=error%></pre>
	<%
		}
	%>
	<form method="post">
		<table>
			<tr>
				<td>Debug Mode:&nbsp;
					<select name="debug">
						<option value="false">OFF</option>
						<option value="true"<%= isDebug(request) ? " selected":"" %>>ON</option>
					</select>
				</td>
			</tr>
		</table>
		<table class="main">
			<tr>
				<th width="5%">No.</th>
				<th width="15%">Name</th>
				<th width="60%">Description</th>
				<th width="10%">Action</th>
				<th width="10%">Status</th>
			</tr>
			<tr>
				<td class="index">1.</td>
				<td>Lookup Bha Component</td>
				<td>Migrate Bha Component Lookup to EDM version. This will flag
					current lookup_bha_component into inactive mode and insert EDM Bha
					Component Lookup.</td>
				<td><button name="setup" type="submit" value="bhacomponent">Migrate</button></td>
				<td class="status">
					<%
						if (StringUtils.equals(key, "bhacomponent")) {
					%><%=status%>
					<%
						}
					%>
				</td>
			</tr>
			<tr>
				<td class="index">2.</td>
				<td>EDM Catalog</td>
				<td>Import EDM BHA Component Lookup Catalog.</td>
				<td><button name="setup" type="submit" value="edmcatalog">Import</button></td>
				<td class="status">
					<%
						if (StringUtils.equals(key, "edmcatalog")) {
					%><%=status%>
					<%
						}
					%>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>

