<%@page import="com.idsdatanet.d2.infra.autoupdate.synchronization.AutoUpdateClientManager" %>
<% 
	com.idsdatanet.d2.core.web.helper.Utils.setExpire01Jan1970(response); 

	String launch_error = null;
	boolean launched = false;
	boolean update_available = false;
	
	if(request.getParameter("launchUpdater") != null){
		try{
			AutoUpdateClientManager.launchUpdater(request);
			launched = true;
		}catch(Exception e){
			launch_error = e.getMessage();
		}
	}else{
		update_available = AutoUpdateClientManager.isUpdateAvailable();
	}
%>
<%!
	String getOriginalUrl(HttpServletRequest request){
		String url = request.getParameter("oUrl");
		if(url == null){
			return "index.jsp";
		}else{
			return url;
		}
	}
%>
<html>
	<head>
		<script type="text/javascript" src="scripts/system.js"></script>
		<script type="text/javascript">
			function gotoOriginalScreen(){
				window.location = "<%=getOriginalUrl(request)%>";
			}

			function launchUpdate(){
				$("#launchUpdaterForm")[0].submit();
			}
		</script>
	</head>
	<body>
		<form id="launchUpdaterForm" method="post">
			<input type="hidden" name="launchUpdater" value="1"/>
		</form>
		<% if(launch_error != null){ %>		
			<table height="100%" width="100%">
				<tr>
				<td valign="middle" align="center">
					<table width="50%" style="background-color:#ff3300">
						<tr>
							<td style="font-weight:bold; padding-top:20; padding-left:20; padding-bottom:20;">
								<%= launch_error %>
							</td>
						</tr>
					</table>
				</td>
				</tr>
			</table>
		<% } else if((! update_available) && (! launched)){ %>
			<table height="100%" width="100%">
				<tr>
				<td valign="middle" align="center">
					<table width="50%" style="background-color:#cccccc">
						<tr>
							<td style="font-size: 20; padding-top:20; padding-bottom:20; padding-left:20; padding-right:20">
								No Updates available for install at this moment. You may return to normal data entry screen.
							</td>
						</tr>
						<tr>
							<td style="padding-left:20; padding-bottom:20;">
								To return to normal data entry screen, click
								<input type="button" style="width:80" onclick="gotoOriginalScreen()" value="Cancel"/>
							</td>
						</tr>
					</table>
				</td>
				</tr>
			</table>								
		<% } else if(! launched){ %>		
			<table height="100%" width="100%">
				<tr>
				<td valign="middle" align="center">
					<table width="50%" style="background-color:#cccccc">
						<tr>
							<td style="font-size: 20; padding-top:20; padding-bottom:20; padding-left:20; padding-right:20">
								Updates are available for install. You may start the update process or return to normal data entry screen.
							</td>
						</tr>
						<tr>
							<td style="padding-bottom:20; padding-left:20;" >
								To start the update process, click
								<input type="button" style="width:80" onclick="launchUpdate()" value="Start"/>
							</td>
						</tr>
						<tr>
							<td style="padding-left:20; padding-bottom:20;">
								To return to normal data entry screen, click
								<input type="button" style="width:80" onclick="gotoOriginalScreen()" value="Cancel"/>
							</td>
						</tr>
					</table>
				</td>
				</tr>
			</table>								
		<%} else { %>
			<table height="100%" width="100%">
				<tr>
				<td valign="middle" align="center">
					<table width="50%" style="background-color:#cccccc">
						<tr>
							<td style="font-weight:bold; padding-top:20; padding-left:20; padding-bottom:20;">
								Update in progress, please wait.
							</td>
						</tr>
					</table>
				</td>
				</tr>
			</table>
		<%} %>
	</body>
</html>