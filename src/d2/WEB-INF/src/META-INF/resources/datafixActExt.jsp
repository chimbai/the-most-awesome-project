<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.depot.witsml.rts.*,
com.idsdatanet.d2.core.web.mvc.*,
com.idsdatanet.d2.core.dao.QueryProperties,
com.idsdatanet.d2.core.model.*,
java.net.*,
java.util.*,
java.net.*,java.util.*,
com.idsdatanet.d2.core.web.mvc.UserSession,
org.apache.commons.lang.StringUtils"%>



<%
try {
	int totalDeleted = 0;
	int totalRecord = 0;

	String hql = "from ActivityExtCodeMapping where (isDeleted is null or isDeleted = false) and (isActive is null or isActive = true) " +
			"and (extCodeL6 is null or extCodeL6 ='') " +
			"and (extCodeL5 is null or extCodeL5 ='') " +
			"and (extCodeL4 is null or extCodeL4 ='') " +
			"and (extCodeL3 is null or extCodeL3 ='') " +
			"and (extCodeL2 is null or extCodeL2 ='') " +
			"and (extCodeL1 is null or extCodeL1 ='') ";
	List<ActivityExtCodeMapping> actexcm = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
	
	if (actexcm.size()>0)
	{
		totalRecord = actexcm.size();
		%>	<table border="1" width="20%">
			<tr>
			<td>Act Uid</td>
			<td>UserDefActCode</td>
			<td>Ext Code L1</td>
			<td>Ext Code L2</td>
			<td>Ext Code L3</td>
			<td>Ext Code L4</td>
			<td>Ext Code L5</td>
			<td>Ext Code L6</td>
			</tr><%
			
			for (ActivityExtCodeMapping act : actexcm)
			{	

				%>
				<tr>
					<td><%=act.getActivityExtCodeMappingUid() %></td>
					<td><%=act.getUserDefActCode() %></td>
					<td><%=act.getExtCodeL1() %></td>
					<td><%=act.getExtCodeL2() %></td>
					<td><%=act.getExtCodeL3() %></td>
					<td><%=act.getExtCodeL4() %></td>
					<td><%=act.getExtCodeL5() %></td>
					<td><%=act.getExtCodeL6() %></td>
				</tr>
				<%
			}
	}
	%>
	Total Empty L1-L6 record In ActivityExtCodeMapping Table : <%=totalRecord%> records
<%
}catch (Exception e) {
	System.out.println(e.getMessage());
%>-- FAILED


<%
}
%>


<html>
<body>

<br>
==============================================================<br>
<a href="trimActivityExternal.jsp">Update Empty L1-L6 with userDefActCode</a>
<br>
<br>
</body>
</html>
