<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>Class Code Description</title>
	<style type="text/css" media="screen">
	body {
		font-family: Arial, "Lucida Grande", verdana, arial, helvetica, sans-serif;
		font-size: 10pt;
		background-color: #fff;
		text-align: left;
	}
	div {
		margin: 20px;
	}
	table {
		border-collapse: collapse;
		border: 1px solid #50888d;
	}
	table tr td, table tr th {
		vertical-align: top;
		padding: 5px;
		border: 1px #50888d solid;
	}
	table tr th {
		color: #fff;
		background-color: #1f4363;
	}
	</style>
</head>
<body>
	<div>
		<table border="1" cellpadding="0" cellspacing="0">
			<tr>
				<th colspan="3" style="text-align:center">Class Code Description</th>
			</tr>
			<tr>
				<th>Code</th>
				<th>Short Description</th>
				<th>Long Description</th>
			</tr>
			<tr>
				<td>P</td>
				<td>Programmed</td>
				<td>A normal operation, budgeted for in the AFE, not NPT (non-productive time)</td>
			</tr>
			<tr style="color:red">
				<td>TP</td>
				<td>Trouble during Programmed Time</td>
				<td>Trouble during programmed time: NPT during budgeted operations</td>
			</tr>
			<tr>
				<td>U</td>
				<td>Unprogrammed</td>
				<td>Where external intervention causes a deviation from the program. For example: you have reached TD at 2500m and are planning to P&amp;A but the explorationists tell you to drill ahead for another 250 meters. All operations assosciated with the addiitonal drilling until the P&amp;A commences are 'unprogrammed'</td>
			</tr>
			<tr style="color:red">
				<td>TU</td>
				<td>Trouble during unprogrammed time</td>
				<td>NPT during unprogrammed operations</td>
			</tr>
		</table>
	</div>
	<div style="color:red">
		For both TP and TU, please enter the Root-cause (RC Code) that contributed to the trouble time.
	</div>
</body>
</html>