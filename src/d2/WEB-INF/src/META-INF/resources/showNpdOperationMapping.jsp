<%@ page contentType="text/html"%>
<%@ page import="com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils,javax.xml.parsers.*,org.w3c.dom.*,java.io.File"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<style>
td {
	vertical-align: top;
}
</style>
<body>
	<table border="1">
		<tr>
			<th style="width:160px">mainop</th>
			<th style="width:160px">subop</th>
			<th style="width:80px">stage code</th>
			<th style="width:80px">class code</th>
			<th style="width:80px">rc code</th>
			<th style="width:80px">phase code</th>
			<th style="width:80px">operation code</th>
		</tr>
	<%
	try {
		String xmlPath = "WEB-INF/config/report/npd/npdOperationMapping.xml";
		String appPath = application.getRealPath("/");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(appPath + xmlPath);
		
		// normalize text representation
		doc.getDocumentElement().normalize();
		
		String[] npdList = new String[7];
		for(int a=0;a<npdList.length;a++){
			npdList[a]="";
		}
		NodeList mainopList = doc.getElementsByTagName("mainop");
		
		String mainOp="" ;
    	String subOp="";
    	String stageId="";
    	String classId ="";
    	String rcId="";
    	String phaseId="" ;
    	String operId="";
    	int count = 0;
		
	    for (int i = 0; i < mainopList.getLength(); i++) {
	    	int mainOpCount = 0;
	    	int operidCount = 0;
	        Node mainopNode = mainopList.item(i);
	        if(mainopNode.getNodeType() == Node.ELEMENT_NODE) {
	            Element mainopElement = (Element)mainopNode;
	            npdList[0] = mainopElement.getAttribute("name");
	            
	            NodeList subopList = mainopElement.getElementsByTagName("subop");
	            
	            for(int a=0;a<subopList.getLength();a++){
	            	Node subopNode = subopList.item(a);
	            	if(subopNode.getNodeType() == Node.ELEMENT_NODE) {
	            		Element subopElement = (Element)subopList.item(a);
	            		
	                	NodeList codeList = subopElement.getElementsByTagName("code");
	                	
	                	for(int b = 0;b < codeList.getLength();b++){
	                		Node codeNode = codeList.item(b);
	                		if(codeNode.getNodeType() == Node.ELEMENT_NODE) {
	                    		Element codeElement = (Element)codeList.item(b);
	                    		
	                    		if(codeElement.getAttribute("operid") != ""){
		                			operidCount++;
		                		}
	                    		if(codeList.getLength() == 1 && codeElement.getAttribute("operid") == ""){
	                    			operidCount++;
	                    		}
	                		}
	                	}
	            	}
	            }
	            
	            for(int k = 0;k < subopList.getLength();k++){
	            	int subOpCount = 0;
	            	int subOpRowCount = 0;
	            	Node subopNode = subopList.item(k);
	            	if(subopNode.getNodeType() == Node.ELEMENT_NODE) {
	            		Element subopElement = (Element)subopList.item(k);
	            		npdList[1] = subopElement.getAttribute("name");
	            		
	                	NodeList codeList = subopElement.getElementsByTagName("code");
	                	
	                	for(int c = 0;c < codeList.getLength();c++){
	                		Node codeNode = codeList.item(c);
	                		if(codeNode.getNodeType() == Node.ELEMENT_NODE) {
	                    		Element codeElement = (Element)codeList.item(c);
	                    		
	                    		if(codeElement.getAttribute("operid") != ""){
		                			subOpRowCount++;
		                		}
	                    		if(codeList.getLength() == 1 && codeElement.getAttribute("operid") == ""){
	                    			subOpRowCount++;
	                    		}
	                		}
	                	}
	                	
	                	for(int l = 0;l < codeList.getLength();l++){
	                		Node codeNode = codeList.item(l);
	                    	if(codeNode.getNodeType() == Node.ELEMENT_NODE) {
	                    		Element codeElement = (Element)codeList.item(l);
	                    		
	                    		if(l == 0){
	                    			count = 0;
	                    		}
	                    		
	                    		if(codeElement.getAttribute("operid") != ""){
	                    			npdList[6] = codeElement.getAttribute("operid");
	                    			
	                    			mainOp = npdList[0];
	    	                    	subOp = npdList[1];
	    	                    	stageId = npdList[2];
	    	                    	classId = npdList[3];
	    	                    	rcId = npdList[4];
	    	                    	phaseId = npdList[5];
	    	                    	operId = npdList[6];
	    	                    	
	    	                    	if(mainOpCount == 0 && subOpCount == 0){
		    	                    	%>
					                	<tr>
					                		<td rowspan="<%=operidCount%>"><%=mainOp%></td>
					                		<td rowspan="<%=subOpRowCount%>"><%=subOp%></td>
					                		<td><%=stageId%></td>
					                		<td><%=classId%></td>
					                		<td><%=rcId%></td>
					                		<td><%=phaseId%></td>
					                		<td><%=operId%></td>
					                	</tr>
					                	<%
				                		mainOpCount++;
					                	subOpCount++;
	    	                    	}
	    	                    	else if(mainOpCount != 0 && subOpCount == 0){
	    	                    		%>
					                	<tr>
					                		<td rowspan="<%=subOpRowCount%>"><%=subOp%></td>
					                		<td><%=stageId%></td>
					                		<td><%=classId%></td>
					                		<td><%=rcId%></td>
					                		<td><%=phaseId%></td>
					                		<td><%=operId%></td>
					                	</tr>
					                	<%
					                	subOpCount++;
	    	                    	}
	    	                    	else{
	    	                    		if(subOpCount != 0){
	    	                    			%>
						                	<tr>
						                		<td><%=stageId%></td>
						                		<td><%=classId%></td>
						                		<td><%=rcId%></td>
						                		<td><%=phaseId%></td>
						                		<td><%=operId%></td>
						                	</tr>
						                	<%
	    	                    		}
	    	                    		else{
		    	                    		%>
						                	<tr>
						                		<td><%=subOp%></td>
						                		<td><%=stageId%></td>
						                		<td><%=classId%></td>
						                		<td><%=rcId%></td>
						                		<td><%=phaseId%></td>
						                		<td><%=operId%></td>
						                	</tr>
						                	<%	
	    	                    		}
	    	                    	}
				                	
				                	if(count > 1){
				                		count--;
				                	}
	                    		}
	                    		else{
	                    			if(count == 0){
			                    		npdList[2] = codeElement.getAttribute("stageid");
			                    		npdList[3] = codeElement.getAttribute("classid");
			                    		npdList[4] = codeElement.getAttribute("rcid");
			                    		npdList[5] = codeElement.getAttribute("phaseid");
			                    		
			                    		if(codeList.getLength() == 1){
			                    			subOp = npdList[1];
			                    			stageId = npdList[2];
			    	                    	classId = npdList[3];
			    	                    	rcId = npdList[4];
			    	                    	phaseId = npdList[5];
			    	                    	operId = codeElement.getAttribute("operid");
	                    					%>
						                	<tr>
						                		<td><%=subOp%></td>
						                		<td><%=stageId%></td>
						                		<td><%=classId%></td>
						                		<td><%=rcId%></td>
						                		<td><%=phaseId%></td>
						                		<td><%=operId%></td>
						                	</tr>
						                	<%
	                    				}
			                    		
			                    		count++;
	                    			}
	                    			else{
	                    				if(codeElement.getAttribute("classid") != ""){
		                    				npdList[3] = codeElement.getAttribute("classid");
		                    			}
		                    			else if(codeElement.getAttribute("rcid") != ""){
		                    				npdList[4] = codeElement.getAttribute("rcid");
		                    			}
		                    			else{
		                    				npdList[5] = codeElement.getAttribute("phaseid");
		                    			}
	                    					
	                    				count++;
	                    			}
	                    		}
	                    	}
	                	}
	            	}
	            }
	        }
	    }
	%>
	</table>
	<%
	    
	} catch (Exception e) {
		e.printStackTrace();
	}
	%>
</body>
</html>