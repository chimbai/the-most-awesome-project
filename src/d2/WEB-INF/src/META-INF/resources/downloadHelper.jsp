<html>
<head>
	<title>Download File</title>
</head>
<body>
	<h2>Download File</h2>
	<a href="javascript:window.open('<%=request.getParameter("downloadFileUrl")%>');self.close();">Click here to start downloading if download doesn't start in a few seconds</a>
	<br/><br/>
	Closing in <span id="closeInSeconds">30</span> seconds. 
	<input type="button" onclick="window.close();" value="Close"/>
	<script>
		var time_to_live_count = 30;
		var time_to_live_txt = document.getElementById("closeInSeconds");
		
		function autoClose(){
			time_to_live_count = time_to_live_count - 1;

			if(time_to_live_count <= 0){
				window.close();
			}else{
				time_to_live_txt.innerText = time_to_live_count;
				setTimeout("autoClose();",1000);
			}
		}
		
		window.open("<%=request.getParameter("downloadFileUrl")%>", "_self");
		setTimeout("autoClose();",1000);
	</script>
</body>
</html>