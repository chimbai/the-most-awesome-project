[before_hibernate_initialization]


[after_hibernate_initialization]
update menu_item m, menu_item p set m.data_matrix_section_name=p.label where (p.is_deleted=false or p.is_deleted is null) and (m.is_deleted=false or m.is_deleted is null) and p.menu_item_uid=m.parent and p.label in ('ProNet','DrillNet','SafeNet','CostNet','Rig Data','FRAC CTU');
update menu_item m, menu_item p set m.data_matrix_section_name=p.label where (p.is_deleted=false or p.is_deleted is null) and (m.is_deleted=false or m.is_deleted is null) and p.menu_item_uid=m.parent and (p.label in ('Reports') and m.url like '%tab=report%');
update menu_item set data_matrix_section_name='ProNet' where data_matrix_section_name='ProNet';
update menu_item set data_matrix_section_name='DrillNet' where data_matrix_section_name='DrillNet';
update menu_item set data_matrix_section_name='SafeNet' where data_matrix_section_name='SafeNet';
update menu_item set data_matrix_section_name='CostNet' where data_matrix_section_name='CostNet';
update menu_item set data_matrix_section_name='Rig Data' where data_matrix_section_name='Rig Data';
update menu_item set data_matrix_section_name='FRAC CTU' where data_matrix_section_name='FRAC CTU';
update menu_item set data_matrix_section_name='Report' where data_matrix_section_name='Reports';
