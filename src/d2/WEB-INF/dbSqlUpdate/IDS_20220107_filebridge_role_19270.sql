[before_hibernate_initialization]
INSERT INTO `acl_group` (`acl_group_uid`, `group_uid`, `name`, `last_edit_datetime`, `can_view_wells`) VALUES (md5('filebridge-group'), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridge', NOW(), 1);
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-1'), md5('filebridge-group'), 'wellboreController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-2'), md5('filebridge-group'), 'rigInformationController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-3'), md5('filebridge-group'), 'rigPumpController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-4'), md5('filebridge-group'), 'lookupCompanyController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-5'), md5('filebridge-group'), 'bharunWithoutBitrunController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-6'), md5('filebridge-group'), 'mudPropertiesController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-7'), md5('filebridge-group'), 'activityController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-8'), md5('filebridge-group'), 'dailyController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-9'), md5('filebridge-group'), 'bharunController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-10'), md5('filebridge-group'), 'wellController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-11'), md5('filebridge-group'), 'bitrunController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-12'), md5('filebridge-group'), 'operationController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-13'), md5('filebridge-group'), 'basinController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-14'), md5('filebridge-group'), 'personnelOnSiteController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-15'), md5('filebridge-group'), 'surveyReferenceController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-16'), md5('filebridge-group'), 'reportDailyController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-17'), md5('filebridge-group'), 'mudVolumesController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-18'), md5('filebridge-group'), 'formationController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-19'), md5('filebridge-group'), 'fileBridgeController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-20'), md5('filebridge-group'), 'webService.fileManagerServiceController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-21'), md5('filebridge-group'), 'opsDatumController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-22'), md5('filebridge-group'), 'drillingParametersController', 30, NOW());
INSERT INTO `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `last_edit_datetime`) VALUES (md5('filebridge-group-23'), md5('filebridge-group'), 'casingSectionController', 30, NOW());
INSERT INTO user_acl_group (user_acl_group_uid, user_uid, acl_group_uid, last_edit_datetime) VALUES (MD5('filebridge-group-acl-entry'), (SELECT user_uid FROM user WHERE user_name='idsadmin' AND (is_deleted IS NULL OR is_deleted=FALSE)), md5('filebridge-group'), NOW());


[after_hibernate_initialization]
