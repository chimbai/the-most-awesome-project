[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE process_safety_questionnaire SET change_control_24hr_amount=1 WHERE has_change_control_24hr='1';
UPDATE process_safety_questionnaire SET change_control_24hr_amount=0 WHERE has_change_control_24hr='0';
UPDATE process_safety_questionnaire SET change_control_approved_amount=1 WHERE is_change_control_approved='1';
UPDATE process_safety_questionnaire SET change_control_approved_amount=0 WHERE is_change_control_approved='0';