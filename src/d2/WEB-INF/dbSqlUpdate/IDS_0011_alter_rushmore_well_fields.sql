[before_hibernate_initialization]


[after_hibernate_initialization]
ALTER TABLE rushmore_wells MODIFY COLUMN drilling_fluid_type TEXT;
ALTER TABLE rushmore_wells MODIFY COLUMN rig_type TEXT;
ALTER TABLE rushmore_wells MODIFY COLUMN drill_method TEXT;