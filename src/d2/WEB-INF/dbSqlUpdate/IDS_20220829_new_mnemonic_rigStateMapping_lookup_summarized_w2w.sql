[before_hibernate_initialization]

[after_hibernate_initialization]
/* add common lookup  */
INSERT INTO common_lookup (common_lookup_uid,group_uid,last_edit_datetime,record_owner_uid,lookup_type_selection,description,short_code,lookup_label) VALUES ('summarized_w2w_mnemonic_1', (SELECT group_uid FROM `group` WHERE (is_deleted IS NULL OR is_deleted = FALSE) AND parent_group_uid IS NULL), NOW(), 'dbsqlupdate', 'rigStateMapping.mnemonic', 'For Rig State 38 summarization and WITSML publication. Helps switch to a different RTS listener for processing (switch from RigStateForwardListener to WitsmlW2WRTSListener).', 'Summarized_W2W', 'Summarized W2W');
