[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE well SET `field` = UPPER(`field`);
INSERT INTO common_lookup_type (common_lookup_type_uid,group_uid,lookup_type_selection,lookup_name, record_owner_uid, last_edit_datetime, is_deleted) VALUES ('well.field', (SELECT group_uid FROM `group` WHERE (is_deleted IS NULL OR is_deleted = FALSE) AND parent_group_uid IS NULL),"well.field","well.field","dbsqlupdate", NULL, NULL);
SET @seqno = 0;
INSERT INTO common_lookup (common_lookup_uid, group_uid, lookup_type_selection, short_code, lookup_label, is_active, record_owner_uid, last_edit_datetime) SELECT CONCAT('Well_Field_',CONVERT(LPAD(@seqno:=@seqno+1,3,0),CHAR(50))) AS seqno, (SELECT group_uid FROM `group` WHERE (is_deleted IS NULL OR is_deleted = FALSE) AND parent_group_uid IS NULL), 'well.field', a.*, a.*, '1', 'dbsqlupdate', NULL FROM (SELECT DISTINCT FIELD FROM well WHERE (FIELD IS NOT NULL AND FIELD != "") AND (is_deleted IS NULL OR is_deleted = FALSE) ORDER BY FIELD ASC) a;