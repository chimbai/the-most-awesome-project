[before_hibernate_initialization]
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'report_daily' AND field_name = 'next_csgweight');
DELETE FROM unit_mapping WHERE table_name = 'report_daily' AND field_name = 'next_csgweight';

[after_hibernate_initialization]
ALTER TABLE report_daily CHANGE next_csgweight next_csgweight VARCHAR(20);
