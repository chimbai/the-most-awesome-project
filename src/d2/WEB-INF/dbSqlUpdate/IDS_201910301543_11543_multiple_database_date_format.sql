[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO common_lookup_type VALUES ('EDM_DATABASE_TYPE', (SELECT group_uid FROM `group` LIMIT 1), '1', '', 'edm.database_type', 'EDM DATABASE TYPE', '', 'uniqueRigId01', now(), 'dbsqlupdate', NULL,NULL,NULL); 

INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_DATABASE_TYPE_MSSQLSERVER', (SELECT group_uid FROM `group` LIMIT 1), '1', 'edm.database_type', 'mssqlserver', 'Microsoft SQL Server', 1, 'dbsqlupdate');

INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_DATABASE_TYPE_ORACLE', (SELECT group_uid FROM `group` LIMIT 1), '1', 'edm.database_type', 'oracle', 'Oracle', 1, 'dbsqlupdate');


INSERT INTO common_lookup_type VALUES ('EDM_DATABASE_DATETIME_FORMAT', (SELECT group_uid FROM `group` LIMIT 1), '1', 'edm.database_type', 'edm.database_datetime_format', 'EDM DATABASE DATETIME FORMAT', '', 'uniqueRigId01', now(), 'dbsqlupdate', NULL, NULL, NULL);

INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `parent_reference_key`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_DATABASE_DATETIME_FORMAT_MSSQLSERVER', (SELECT group_uid FROM `group` LIMIT 1), '1', 'edm.database_datetime_format', 'mssqlserver', 'yyyy-MM-dd', 'yyyy-MM-dd', 1, 'dbsqlupdate');

INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `parent_reference_key`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_DATABASE_DATETIME_FORMAT_ORACLE', (SELECT group_uid FROM `group` LIMIT 1), '1', 'edm.database_datetime_format', 'oracle', 'dd-MMM-yy', 'dd-MMM-yy', 1, 'dbsqlupdate');

UPDATE edm_configuration SET database_type='mssqlserver', database_datetime_format='yyyy-MM-dd';