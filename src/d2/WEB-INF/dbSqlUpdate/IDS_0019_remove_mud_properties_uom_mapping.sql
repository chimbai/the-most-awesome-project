[before_hibernate_initialization]
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'mud_properties' AND field_name = 'metal_cuttings_mass_per_krev');
DELETE FROM unit_mapping WHERE table_name = 'mud_properties' AND field_name = 'metal_cuttings_mass_per_krev';

[after_hibernate_initialization]