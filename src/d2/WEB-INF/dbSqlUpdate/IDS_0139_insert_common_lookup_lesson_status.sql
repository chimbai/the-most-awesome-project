[before_hibernate_initialization]
delete from common_lookup where lookup_type_selection='Lesson_status';
delete from common_lookup_type where lookup_type_selection='Lesson_status';

[after_hibernate_initialization]
INSERT INTO common_lookup_type VALUES ("2c92eec84fda598f01503bffd9d52e01","IDS_DEMO","5","","Lesson_status","Lesson_status","","uniqueRigId01","2015-10-06 07:14:46","2c92eec84f452c2b014f4a02e001003b",NULL,NULL,NULL);

INSERT INTO common_lookup VALUES ("2c92eec84fda598f01503c00b7bf2e04","IDS_DEMO","1","Lesson_status","OPEN","",NULL,"","Open",NULL,NULL,"uniqueRigId01","2015-10-06 07:14:51","2c92eec84f452c2b014f4a02e001003b",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("2c92eec84fda598f01503c00e34c2ecc","IDS_DEMO","2","Lesson_status","CLOSED","",NULL,"","Closed",NULL,NULL,"uniqueRigId01","2015-10-06 07:15:02","2c92eec84f452c2b014f4a02e001003b",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("2c92eec84fda598f01503c017c93349a","IDS_DEMO","3","Lesson_status","REJECTED","",NULL,"","Rejected",NULL,NULL,"uniqueRigId01","2015-10-06 07:15:41","2c92eec84f452c2b014f4a02e001003b",NULL,NULL,NULL);
