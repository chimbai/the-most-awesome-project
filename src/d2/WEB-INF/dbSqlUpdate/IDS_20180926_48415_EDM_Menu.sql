[before_hibernate_initialization]
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `parent`, `sequence`) values('2c90c0c4660fc85a016613df8f88110f','EDM Setup',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'2c907474166fd9e201166fe08107000f','33');

insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90c0c4660fc85a016613e1b1fe1111','Well Transfer',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'wellTransferController','2c90c0c4660fc85a016613df8f88110f','20');
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90c0c4660fc85a016613e1b2311113','Wellbore Mapping',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'edmWellboreMappingController','2c90c0c4660fc85a016613df8f88110f','40');
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90c0c4660fc85a016613e1b2561115','Data Object Setup',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'edmSetupController','2c90c0c4660fc85a016613df8f88110f','30');
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90c0c4660fc85a016613e1b2781117','Server Setup',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'importExportServerPropertiesController','2c90c0c4660fc85a016613df8f88110f','10');

insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `record_owner_uid`, `last_edit_datetime`, `last_edit_user_uid`, `is_deleted`, `sys_deleted`, `is_submit`) values('2c90a91065a7d4980165a7d716120341','2c92f0e71da7ff03011dbcf01d7d218e','edmWellboreMappingController','30','uniqueRigId01','2018-09-05 03:46:55','2c92eecd59f91e8a015a16462ba251e2',NULL,NULL,NULL);

[after_hibernate_initialization]