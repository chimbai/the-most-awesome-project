[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE mud_properties SET retort_solids_pc_volpervol = mud_dissolved_solids, retort_h2o_pc_volpervol = mud_h2o, retort_oil_pc_volpervol = mud_oil, lgs_pc_volpervol = lgs, hgs_pc_volpervol = hgs;
UPDATE mud_properties SET sand_pc_volpervol = sand_txt WHERE sand_txt REGEXP '^[0-9][.][0-9]+$';
UPDATE mud_properties SET sand_pc_volpervol = concat('0',sand_txt) WHERE sand_txt REGEXP '^[.][0-9]+$';
UPDATE mud_properties SET sand_pc_volpervol = replace(sand_txt, '%', '') WHERE sand_txt like '%\%';
UPDATE mud_properties SET sand_pc_volpervol = sand_txt WHERE sand_txt REGEXP '^[0-9]+$'; 
UPDATE mud_properties SET sand_pc_volpervol = concat(sand_txt, '0') WHERE sand_txt REGEXP '^[0-9][.]+$';