[before_hibernate_initialization]

[after_hibernate_initialization]
/* set the older EXT_WELL_CATALOG with no description (not the static uid version) and WellView 1.0.0.0 to isDeleted */
UPDATE common_lookup SET is_deleted=1 WHERE (is_deleted=0 OR is_deleted IS NULL) AND short_code = "EXT_WELL_CATALOG" AND lookup_type_selection="depot_type" AND common_lookup_uid != "EXT_WELL_CATALOG_UID_1";
UPDATE common_lookup SET is_deleted=1 WHERE (is_deleted=0 OR is_deleted IS NULL) AND short_code = "WellView 1.0.0.0" AND lookup_type_selection="depot_version" AND common_lookup_uid != "WellView_EXT_WELL_CATALOG_UID_1";
