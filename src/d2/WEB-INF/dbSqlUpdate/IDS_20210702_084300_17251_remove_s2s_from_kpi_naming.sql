[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Casing - Connection Time' WHERE kpi_repository_uid = 'op_csgs2sdur';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Casing - Connection Time (P50)' WHERE kpi_repository_uid = 'op_csgs2sdurp50';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Casing - Connection Time (P50) Cased Hole' WHERE kpi_repository_uid = 'op_csgs2sdurp50ch';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Casing - Connection Time (P50) Open Hole' WHERE kpi_repository_uid = 'op_csgs2sdurp50oh';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Drilling - Connection Time' WHERE kpi_repository_uid = 'op_drls2sdur';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - POOH - Connection Time' WHERE kpi_repository_uid = 'op_trppoohs2sdur';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - POOH - Connection Time (P50)' WHERE kpi_repository_uid = 'op_trppoohs2sdurp50';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - POOH - Connection Time (P50) Cased Hole' WHERE kpi_repository_uid = 'op_trppoohs2sdurp50ch';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - POOH - Connection Time (P50) Open Hole' WHERE kpi_repository_uid = 'op_trppoohs2sdurp50oh';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - RIH - Connection Time' WHERE kpi_repository_uid = 'op_trprihs2sdur';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - RIH - Connection Time (P50)' WHERE kpi_repository_uid = 'op_trprihs2sdurp50';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - RIH - Connection Time (P50) Cased Hole' WHERE kpi_repository_uid = 'op_trprihs2sdurp50ch';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping - RIH - Connection Time (P50) Open Hole' WHERE kpi_repository_uid = 'op_trprihs2sdurp50oh';
UPDATE kpi_repository SET last_edit_datetime = NOW(), record_owner_uid = 'dbsqlupdate', last_edit_user_uid = '', kpi_name = 'Tripping -  Connection Time' WHERE kpi_repository_uid = 'op_trps2sdur';