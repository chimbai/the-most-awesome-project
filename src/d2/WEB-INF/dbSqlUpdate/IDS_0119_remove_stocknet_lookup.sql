[before_hibernate_initialization]


[after_hibernate_initialization]
DELETE c FROM lookup_company AS c INNER JOIN (SELECT lookup_company_uid,SUM(CASE WHEN CODE ='STOCKNET' THEN 1 ELSE 0 END) AS StocknetCount,SUM(CASE WHEN CODE <>'STOCKNET' THEN 1 ELSE 0 END) AS NonStocknet FROM lookup_company_service GROUP BY lookup_company_uid HAVING NonStocknet=0) AS b ON c.lookup_company_uid = b.lookup_company_uid;
DELETE FROM lookup_company_service WHERE CODE ='STOCKNET';