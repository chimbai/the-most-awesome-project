[before_hibernate_initialization]


[after_hibernate_initialization]
DELETE FROM unit_mapping WHERE table_name IN ('equipment_list_template','equipment_list_template_detail','inventory_bulk_master','inventory_document','inventory_document_detail','inventory_document_partial_log','inventory_master','inventory_master_rental_log','inventory_movement_log','inventory_transaction_bulk_cargo_detail','inventory_warehouse','master_equipment_list_detail','stocknet_transaction_detail','warehouse_pre_allocation_detail','well_equipment_list','well_equipment_list_detail','site','site_daily','site_entity','site_operation','site_operation_assessment','site_operation_assessment_soil_physical','site_operation_assessment','site_operation_assessment_vegetation','site_operation','site_operation_drilling_waste_mud_additives','site_operation_drilling_waste_treatment_chemical','site_operation_drilling_waste_treatment','site_operation_phase_review','site_operation_plan_action','site_operation_plan_cost','site_operation_plan_phase','site_operation_plan','site_operation_reclamation_reduction','site_operation_reclamation_seed_mixture','site_operation_reclamation_treatment','site_sketch_component','site_sketch_component_point','site_sketch','soil_sample','waste_sample','waste_sample_trace_element_analysis');
update user_acl_group set is_deleted = 1 where `acl_group_uid` in (select ae.acl_group_uid from (select * from acl_group) as ae where name like "%Site%");
update acl_entry set is_deleted = 1 where `acl_group_uid` in (select ae.acl_group_uid from (select * from acl_group) as ae where name like "%Site%");
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "retrieveNewSiteFromTownController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteDailyController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteEntityController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteExplorerController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteInformationCenterController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteManagementController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteMergeController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteNetHSEIncidentController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationAssessmentController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteMudAdditivesController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteSampleController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteTreatmentController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationPlanController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationReductionController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationSeedController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationTreatmentController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteOperationSoilSampleController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteSketchController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteSketchTexturesController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteSketchUserSettingController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "webService.retrieveNewSiteFromTownServiceController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "webService.siteExplorerServiceController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "webService.siteSketchServiceController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "lookupContactController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "lookupGoverningBodyController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "regulatoryDocumentsController";
update acl_group set is_deleted = 1 where name like "%Site%";
update user set is_deleted = 1 where fname like "SiteNet%";
delete from acl_entry where is_deleted =1;
delete from acl_group where is_deleted =1;
update menu_item set is_deleted = 1 where `parent` in (select ae.menu_item_uid from (select * from menu_item) as ae where ae.parent in (select ag.menu_item_uid from (select * from menu_item) as ag where upper(ag.label) in ("SITENET SETUP", "SITE DATA", "SITENET", "RECLAMATION", "DRILLING WASTE")));
update menu_item set is_deleted = 1 where `parent` in (select ae.menu_item_uid from (select * from menu_item) as ae where upper(ae.label) in ("SITENET SETUP", "SITE DATA", "SITENET", "RECLAMATION", "DRILLING WASTE"));
update menu_item set is_deleted = 1 where upper(`label`) in ("SITENET SETUP", "SITE DATA", "SITENET", "RECLAMATION", "DRILLING WASTE");
update menu_item set is_deleted = 1 where `parent` in (select ae.menu_item_uid from (select * from menu_item) as ae where is_deleted = 1);	
update menu_item_operation_type set is_deleted = 1 where `menu_item_uid` in (select ae.menu_item_uid from (select * from menu_item) as ae where is_deleted = 1);
update menu_item_view_type set is_deleted = 1 where `menu_item_uid` in (select ae.menu_item_uid from (select * from menu_item) as ae where is_deleted = 1);
delete from menu_item where is_deleted=1;
delete from menu_item_operation_type where is_deleted=1;
delete from menu_item_view_type	where is_deleted=1;