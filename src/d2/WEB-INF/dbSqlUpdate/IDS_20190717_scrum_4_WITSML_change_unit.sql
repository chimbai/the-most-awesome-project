[before_hibernate_initialization]
UPDATE uom_template_mapping SET unit_uid='KilogramsPerMetre' WHERE unit_mapping_uid = 'bha_component_weight_MassPerLengthMeasure' AND uom_template_uid IN (SELECT uom_template_uid FROM uom_template WHERE `name`='WITSML' AND (is_deleted IS NULL OR is_deleted=0));

[after_hibernate_initialization]
