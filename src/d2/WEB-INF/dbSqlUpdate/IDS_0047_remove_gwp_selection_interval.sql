[before_hibernate_initialization]
DELETE FROM system_settings WHERE setting_name = 'activityController.startDatetime.selectionInterval';
DELETE FROM system_settings WHERE setting_name = 'activityController.endDatetime.selectionInterval';

[after_hibernate_initialization]