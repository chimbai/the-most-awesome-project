[before_hibernate_initialization]
ALTER TABLE cost_account_codes MODIFY short_description VARCHAR(255); 
ALTER TABLE cost_account_codes MODIFY account_code VARCHAR(255); 
ALTER TABLE cost_afe_detail MODIFY account_code VARCHAR(255); 
ALTER TABLE cost_afe_detail MODIFY item_description VARCHAR(255); 
ALTER TABLE cost_afe_detail MODIFY short_description VARCHAR(255); 
ALTER TABLE cost_dailysheet MODIFY account_code VARCHAR(255); 
ALTER TABLE cost_dailysheet MODIFY afe_short_description VARCHAR(255); 
ALTER TABLE cost_dailysheet MODIFY afe_item_description VARCHAR(255); 


[after_hibernate_initialization]
 