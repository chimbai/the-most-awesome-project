[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE common_lookup SET short_code='town_side_qc_done_by_report_daily_date', lookup_label='Town Side QC Done By Report Daily Date' WHERE lookup_type_selection='edm.push_condition' AND sequence=2;
INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_PUSH_CONDITION_TSQD_BY_RIG_RELEASE_DATE', (SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted=FALSE) AND g.parent_group_uid IS NULL),'3','edm.push_condition','town_side_qc_done_by_rig_released_date','Town Side QC Done By Rig Release Date', 1,'dbsqlupdate');