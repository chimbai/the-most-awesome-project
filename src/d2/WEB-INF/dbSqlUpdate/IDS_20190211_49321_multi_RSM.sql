[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE rig_state_definition SET NAME = CONCAT(DATE(last_edit_datetime),'_',server_source_type);

UPDATE depot_well_operation_mapping SET log_purpose = 'DP_FTR' WHERE is_ftr = 1;
UPDATE depot_well_operation_mapping SET log_purpose = 'RSD' WHERE is_rig_state = 1;
UPDATE depot_well_operation_mapping SET rig_state_definition_uid = (SELECT rig_state_definition_uid FROM rig_state_definition WHERE is_active = 1);
UPDATE  rig_state_definition set is_active = null where is_active = 0;
