[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Lost Time Accident' ;
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='JSA';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='STOP Card';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Drills';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Fire Drill';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Life Boat Drill';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Evacuation Drill';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Safety Meeting';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Lost Time Injury';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='H2S drill';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='Trip/Pit Drill';
UPDATE lookup_incident_category SET is_active=1 WHERE hse_category='BOP Test/Drill';