[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (CONCAT('lookup_', MD5('OpenWells_Weatherford_Fluid_PDF')), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridgeTypeLookup', 'OpenWells_Weatherford_Fluid_PDF', 'OpenWells Daily Drilling Fluid Report (.pdf)', NOW(), 'dbsqlupdate');
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `parent_reference_key`, `lookup_label`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (CONCAT('sublookup_', MD5('OpenWells_Weatherford_Fluid_PDF_psukejgtyvtw')), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridgeTypeLookupSubReport', 'psukejgtyvtw', 'OpenWells_Weatherford_Fluid_PDF', 'Default', NOW(), 'dbsqlupdate');
INSERT INTO `file_bridge_type_lookup` (`file_bridge_type_lookup_uid`, `parser_key`, `name`, `is_active`, `docparser_parser_id`, `last_edit_datetime`, `last_edit_user_uid`, `report_sub_type`) VALUES (MD5('OpenWells_Weatherford_Fluid_PDF'), 'OpenWells_Weatherford_Fluid_PDF', 'OpenWells Daily Drilling Fluid Report (.pdf)', b'0', 'psukejgtyvtw', NOW(), 'dbsqlupdate', 'psukejgtyvtw');
