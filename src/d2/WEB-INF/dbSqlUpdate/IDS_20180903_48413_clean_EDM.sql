[before_hibernate_initialization]
DELETE FROM scheduler_template WHERE import_export_server_properties_uid IN (SELECT server_properties_uid FROM import_export_server_properties WHERE data_transfer_type='edm');

DELETE FROM depot_well_operation_mapping WHERE scheduler_template_uid NOT IN (SELECT scheduler_template_uid FROM scheduler_template);

[after_hibernate_initialization]