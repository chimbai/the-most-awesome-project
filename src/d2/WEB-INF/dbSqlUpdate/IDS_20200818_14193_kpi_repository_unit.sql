[before_hibernate_initialization]


[after_hibernate_initialization]

insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c924e1273ff5daa0173ff608a6d0007','KPI Unit Classification',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'kpiUnitClassificationController','2c924e1273ff38790173ff40ae69000d','20');
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c924e1273ff38790173ff41b0870011','KPI Repository',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'kpiRepositoryController','2c924e1273ff38790173ff40ae69000d','10');
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c924e1273ff38790173ff40ae69000d','KPI Setup',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'','2c907474166fd9e201166fe08107000f','34');

insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`) values('2c924e1273ff38790173ff4402b70014','2c92f0e71da7ff03011dbcf01d7d218e','kpiRepositoryController','30');
insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`) values('2c924e1273ff5daa0173ff61212c000a','2c92f0e71da7ff03011dbcf01d7d218e','kpiUnitClassificationController','30');
