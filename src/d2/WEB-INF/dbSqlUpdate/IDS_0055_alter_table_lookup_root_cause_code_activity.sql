[before_hibernate_initialization]


[after_hibernate_initialization]
ALTER TABLE lookup_root_cause_code MODIFY short_code VARCHAR(50);
ALTER TABLE activity MODIFY root_cause_code VARCHAR(50);
ALTER TABLE tour_activity MODIFY root_cause_code VARCHAR(50);