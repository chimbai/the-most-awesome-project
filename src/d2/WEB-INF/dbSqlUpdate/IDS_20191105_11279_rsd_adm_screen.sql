[before_hibernate_initialization]


[after_hibernate_initialization]
update custom_description_matrix set for_rsd=1 where (remarks_code is not null and remarks_code <> '');
update custom_description_matrix set activity_description_matrix_uid = null where for_rsd=1;
UPDATE activity_description_matrix SET is_deleted = true WHERE phase_short_code = 'rsd';
UPDATE lookup_phase_code SET is_deleted = true WHERE short_code = 'rsd';

insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`) values('2c90c0c46e3a64e1016e3a6be3d2001a','2c92f0e71da7ff03011dbcf01d7d218e','activityDescriptionMatrixRsdController','30');
