[before_hibernate_initialization]

UPDATE unit_mapping SET datum_conversion=1, is_md=1 WHERE field_name = 'string_from_depth_md_msl' AND table_name = 'activity';
UPDATE unit_mapping SET datum_conversion=1, is_md=1 WHERE field_name = 'string_to_depth_md_msl' AND table_name = 'activity';
UPDATE unit_mapping SET datum_conversion=1, is_md=1 WHERE field_name = 'depth_to_md_msl' AND table_name = 'activity';


[after_hibernate_initialization]