[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "retrieveNewSiteFromTownController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteDailyController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteEntityController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteExplorerController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteInformationCenterController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteManagementController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteMergeController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteNetHSEIncidentController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationAssessmentController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteMudAdditivesController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteSampleController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationDrillingWasteTreatmentController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationPlanController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationReductionController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationSeedController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationReclamationTreatmentController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteOperationSoilSampleController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteSketchController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteSketchTexturesController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteSketchUserSettingController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "webService.retrieveNewSiteFromTownServiceController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "webService.siteExplorerServiceController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "webService.siteSketchServiceController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "lookupContactController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "lookupGoverningBodyController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "regulatoryDocumentsController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteInformationCenterController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteExplorerController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "matrixSiteNetController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "siteManagementController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "dsorReportController";
UPDATE menu_item SET is_deleted = 1 WHERE bean_name = "serReportController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteExplorerController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "matrixSiteNetController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "siteManagementController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "dsorReportController";
UPDATE acl_entry SET is_deleted = 1 WHERE bean_name = "serReportController";