[before_hibernate_initialization]
UPDATE unit_mapping SET datum_conversion=0, is_md=0 WHERE field_name = 'depth_md_msl' AND table_name = 'operation_plan_phase_vault';
UPDATE unit_mapping SET datum_conversion=0, is_md=0 WHERE field_name = 'depth_tvd_msl' AND table_name = 'operation_plan_phase_vault';

[after_hibernate_initialization]
