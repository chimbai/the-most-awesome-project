[before_hibernate_initialization]
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_depth');
DELETE FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_depth';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_height');
DELETE FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_height';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_length');
DELETE FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_length';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_width');
DELETE FROM unit_mapping WHERE table_name = 'inventory_master' AND field_name = 'dimension_width';

DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_depth');
DELETE FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_depth';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_height');
DELETE FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_height';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_length');
DELETE FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_length';
DELETE FROM uom_template_mapping where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_width');
DELETE FROM unit_mapping WHERE table_name = 'inventory_transaction_detail' AND field_name = 'dimension_width';


[after_hibernate_initialization]