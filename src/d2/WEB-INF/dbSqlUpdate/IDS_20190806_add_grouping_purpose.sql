[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE lookup_task_code SET grouping_purpose='SC' WHERE phase_short_code IN ('DR', 'PIHO') AND short_code='DAS';
UPDATE lookup_task_code SET grouping_purpose='RC' WHERE phase_short_code IN ('DR', 'PIHO', 'FMCP') AND short_code IN ('DRR', 'DRS', 'DHR', 'MIL');