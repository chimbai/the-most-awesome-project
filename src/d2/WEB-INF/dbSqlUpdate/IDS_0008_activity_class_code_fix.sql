update activity set internal_class_code = null;
update activity a, operation o set 
internal_class_code = (Select internal_code from lookup_class_code where (is_deleted is null or is_deleted = false) and group_uid=a.group_uid and o.operation_code=operation_code and short_code=a.class_code) 
where 
a.class_code is not null
and (a.is_deleted is null or a.is_deleted = false)
and (o.is_deleted is null or o.is_deleted = false)
and (o.operation_uid=a.operation_uid)
and (o.operation_code in (select distinct operation_code from lookup_class_code where (is_deleted is null or is_deleted = false) and group_uid = a.group_uid));

update activity a, operation o set 
internal_class_code = (Select internal_code from lookup_class_code where (is_deleted is null or is_deleted = false) and group_uid=a.group_uid and (operation_code='' or operation_code is null) and short_code=a.class_code)
where 
a.class_code is not null
and (a.is_deleted is null or a.is_deleted = false)
and (o.is_deleted is null or o.is_deleted = false)
and (o.operation_uid=a.operation_uid)
and (o.operation_code not in (select distinct operation_code from lookup_class_code where (is_deleted is null or is_deleted = false) and group_uid = a.group_uid));