[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (CONCAT('lookup_', MD5('OpenWells_Capricorn_DOR_PDF')), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridgeTypeLookup', 'OpenWells_Capricorn_DOR_PDF', 'OpenWells EP Wells Daily Operations Report (.pdf)', NOW(), 'dbsqlupdate');
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `parent_reference_key`, `lookup_label`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (CONCAT('sublookup_', MD5('OpenWells_Capricorn_DOR_PDF_jqpjymobumqw')), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridgeTypeLookupSubReport', 'jqpjymobumqw', 'OpenWells_Capricorn_DOR_PDF', 'Capricorn Energy v4.1.16R9', NOW(), 'dbsqlupdate');
INSERT INTO `file_bridge_type_lookup` (`file_bridge_type_lookup_uid`, `parser_key`, `name`, `is_active`, `docparser_parser_id`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (MD5('OpenWells_Capricorn_DOR_PDF'), 'OpenWells_Capricorn_DOR_PDF', 'OpenWells EP Wells Daily Operations Report (.pdf)', b'0', 'jqpjymobumqw', NOW(), 'dbsqlupdate');
