[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE common_lookup SET is_deleted = 1 where parent_reference_key = "KPI" and short_code IN ("Dimension","Number Metric","Ratio Metric");
INSERT INTO common_lookup (common_lookup_uid,group_uid,lookup_type_selection,short_code,lookup_label) SELECT CONCAT(common_lookup_uid,"_hashconstant") AS common_lookup_uid,group_uid,lookup_type_selection, "hashconstant" AS short_code,"Hash Constant" AS lookup_label FROM common_lookup where lookup_type_selection = "activityDescriptionMatrix.paramType" and (is_deleted = FALSE or is_deleted IS NULL) LIMIT 1;