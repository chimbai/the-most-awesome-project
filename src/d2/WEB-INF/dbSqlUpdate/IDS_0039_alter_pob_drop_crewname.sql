[before_hibernate_initialization]
ALTER TABLE personnel_on_site DROP COLUMN crewname;
ALTER TABLE tour_personnel_on_site DROP COLUMN crewname;

[after_hibernate_initialization]