[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO common_lookup_type (common_lookup_type_uid, group_uid, last_edit_datetime, lookup_name, lookup_type_selection) VALUES (REPLACE(UUID(),'-',''),(SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'bhaComponent.pinBox','bhaComponent.pinBox');
INSERT INTO common_lookup (common_lookup_uid, group_uid, sequence, lookup_type_selection, short_code, lookup_label, last_edit_datetime) VALUES (REPLACE(UUID(),'-',''),(SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),'1','bhaComponent.pinBox','bottom box, top box','BB',NOW());
INSERT INTO common_lookup (common_lookup_uid, group_uid, sequence, lookup_type_selection, short_code, lookup_label, last_edit_datetime) VALUES (REPLACE(UUID(),'-',''),(SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),'2','bhaComponent.pinBox','bottom box, top pin','BP',NOW());
INSERT INTO common_lookup (common_lookup_uid, group_uid, sequence, lookup_type_selection, short_code, lookup_label, last_edit_datetime) VALUES (REPLACE(UUID(),'-',''),(SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),'3','bhaComponent.pinBox','bottom pin top box','PB',NOW());
INSERT INTO common_lookup (common_lookup_uid, group_uid, sequence, lookup_type_selection, short_code, lookup_label, last_edit_datetime) VALUES (REPLACE(UUID(),'-',''),(SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),'4','bhaComponent.pinBox','bottom pin','P',NOW());
