[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO menu_item (menu_item_uid, label, group_uid, bean_name, parent, sequence, record_owner_uid, last_edit_datetime, last_edit_user_uid) VALUES (UUID(), 'Offset Well Dashboard', (SELECT group_uid FROM `group` WHERE (is_deleted IS NULL OR is_deleted = 0) AND parent_group_uid IS NULL LIMIT 1), 'offsetWellDashboardController', (SELECT p.menu_item_uid FROM menu_item p WHERE (p.is_deleted IS NULL OR p.is_deleted = 0) AND p.label = 'DEPOT'), '60', 'uniqueRigId01', NOW(), (SELECT u.user_uid FROM `user` u WHERE (u.is_deleted IS NULL OR u.is_deleted = 0) AND u.user_name = 'idsadmin' LIMIT 1));
INSERT INTO acl_entry (acl_entry_uid, acl_group_uid, bean_name, permissions, record_owner_uid, last_edit_datetime, last_edit_user_uid) VALUES (UUID(), (SELECT a.acl_group_uid FROM acl_group a WHERE (a.is_deleted IS NULL OR a.is_deleted = 0) AND a.name = 'Super Administrator Group' LIMIT 1), 'offsetWellDashboardController', 30, 'uniqueRigId01', NOW(), (SELECT u.user_uid FROM `user` u WHERE (u.is_deleted IS NULL OR u.is_deleted = 0) AND u.user_name = 'idsadmin' LIMIT 1));
INSERT INTO acl_entry (acl_entry_uid, acl_group_uid, bean_name, permissions, record_owner_uid, last_edit_datetime, last_edit_user_uid) VALUES (UUID(), (SELECT a.acl_group_uid FROM acl_group a WHERE (a.is_deleted IS NULL OR a.is_deleted = 0) AND a.name = 'Super Administrator Group' LIMIT 1), 'operationQatStatusController', 30, 'uniqueRigId01', NOW(), (SELECT u.user_uid FROM `user` u WHERE (u.is_deleted IS NULL OR u.is_deleted = 0) AND u.user_name = 'idsadmin' LIMIT 1));
INSERT INTO acl_entry (acl_entry_uid, acl_group_uid, bean_name, permissions, record_owner_uid, last_edit_datetime, last_edit_user_uid) VALUES (UUID(), (SELECT a.acl_group_uid FROM acl_group a WHERE (a.is_deleted IS NULL OR a.is_deleted = 0) AND a.name = 'Super Administrator Group' LIMIT 1), 'webService.offsetWellDashboardWebServiceController', 30, 'uniqueRigId01', NOW(), (SELECT u.user_uid FROM `user` u WHERE (u.is_deleted IS NULL OR u.is_deleted = 0) AND u.user_name = 'idsadmin' LIMIT 1));



