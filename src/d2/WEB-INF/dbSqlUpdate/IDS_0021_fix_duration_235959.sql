[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE lwd_witness SET duration=duration+1 WHERE (duration%10) > 0 AND duration>1;
UPDATE activity SET activity_duration=activity_duration+1 WHERE (activity_duration%10) > 0 AND activity_duration>1;
UPDATE drilling_parameters SET duration=duration+1 WHERE (duration%10) > 0 AND duration>1;
UPDATE wireline_interruptions SET total_time=total_time+1 WHERE (total_time%10) > 0 AND total_time>1;
UPDATE wireline_run SET total_logging_time=total_logging_time+1 WHERE (total_logging_time%10) > 0 AND total_logging_time>1;
UPDATE wireline_log SET duration=duration+1 WHERE (duration%10) > 0 AND duration>1;