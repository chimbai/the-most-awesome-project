[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE rig_state_mapping SET dynamic_phase_type = 'DEFAULT' WHERE is_dynamic_phase = 1 AND is_deleted IS NULL;

insert into `common_lookup_type` (`common_lookup_type_uid`, `group_uid`, `lookup_type_selection`, `lookup_name`) values('2c90a927685efd2301685efe4d26000a',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'OperationPlanPhase.dynamicPhaseType','OperationPlanPhase.dynamicPhaseType');
insert into `common_lookup_type` (`common_lookup_type_uid`, `group_uid`, `lookup_type_selection`, `lookup_name`) values('2c90a927685efd2301685efe4d38000c',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'RigStateMapping.dynamicPhaseType','RigStateMapping.dynamicPhaseType');


insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685efff48b000f',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'3','OperationPlanPhase.dynamicPhaseType','HOLE','Open Hole');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685efff49b0011',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'2','OperationPlanPhase.dynamicPhaseType','CSG','Casing');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685efff4a50013',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'1','OperationPlanPhase.dynamicPhaseType','DRILL','Drilling');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685f012f8b0015',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'1','RigStateMapping.dynamicPhaseType','DEFAULT','DEFAULT');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685f012f980017',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'2','RigStateMapping.dynamicPhaseType','DRILL','Drilling');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685f012f9f0019',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'3','RigStateMapping.dynamicPhaseType','CSG','Casing');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a927685efd2301685f012fa6001b',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'4','RigStateMapping.dynamicPhaseType','HOLE','Open Hole');
