[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE menu_item SET bean_name = "nocost_bitrunController" WHERE bean_name = "bitrunController_nocost";
UPDATE menu_item SET bean_name = "nocost_materialTransferController" WHERE bean_name = "materialTransferController_nocost";
UPDATE menu_item SET bean_name = "nocost_mudPropertiesController" WHERE bean_name = "mudPropertiesController_nocost";
UPDATE menu_item SET bean_name = "alt2_operationInformationCenterController" WHERE bean_name = "operationInformationCenterController_alt2";
UPDATE menu_item SET bean_name = "nocost_operationController" WHERE bean_name = "operationController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyCoalSeamGasController" WHERE bean_name = "reportDailyCoalSeamGasController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyCTUUController" WHERE bean_name = "reportDailyCTUUController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyDIRController" WHERE bean_name = "reportDailyDIRController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyFracRController" WHERE bean_name = "reportDailyFracRController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyPNAController" WHERE bean_name = "reportDailyPNAController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyRMController" WHERE bean_name = "reportDailyRMController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyWKOController" WHERE bean_name = "reportDailyWKOController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyController" WHERE bean_name = "reportDailyController_nocost";
UPDATE menu_item SET bean_name = "nocost_rigStockController" WHERE bean_name = "rigStockController_nocost";
UPDATE menu_item SET bean_name = "nocost_fluidStockController" WHERE bean_name = "fluidStockController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyCmpltRController" WHERE bean_name = "reportDailyCmpltRController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyWellTestController" WHERE bean_name = "reportDailyWellTestController_nocost";
UPDATE menu_item SET bean_name = "nocost_reportDailyWellServiceController" WHERE bean_name = "reportDailyWellServiceController_nocost";
UPDATE menu_item SET bean_name = "nocost_wellHeadController" WHERE bean_name = "wellHeadController_nocost";
UPDATE menu_item SET bean_name = "intv_hseIncidentByEventTypeController" WHERE bean_name = "hseIncidentByEventTypeController_intv";
UPDATE menu_item SET bean_name = "rig_release_report_wellHeadController" WHERE bean_name = "wellHeadController_for_rig_release_report";
UPDATE acl_entry SET bean_name = "nocost_bitrunController" WHERE bean_name = "bitrunController_nocost";
UPDATE acl_entry SET bean_name = "nocost_materialTransferController" WHERE bean_name = "materialTransferController_nocost";
UPDATE acl_entry SET bean_name = "nocost_mudPropertiesController" WHERE bean_name = "mudPropertiesController_nocost";
UPDATE acl_entry SET bean_name = "alt2_operationInformationCenterController" WHERE bean_name = "operationInformationCenterController_alt2";
UPDATE acl_entry SET bean_name = "nocost_operationController" WHERE bean_name = "operationController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyCoalSeamGasController" WHERE bean_name = "reportDailyCoalSeamGasController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyCTUUController" WHERE bean_name = "reportDailyCTUUController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyDIRController" WHERE bean_name = "reportDailyDIRController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyFracRController" WHERE bean_name = "reportDailyFracRController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyPNAController" WHERE bean_name = "reportDailyPNAController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyRMController" WHERE bean_name = "reportDailyRMController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyWKOController" WHERE bean_name = "reportDailyWKOController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyController" WHERE bean_name = "reportDailyController_nocost";
UPDATE acl_entry SET bean_name = "nocost_rigStockController" WHERE bean_name = "rigStockController_nocost";
UPDATE acl_entry SET bean_name = "nocost_fluidStockController" WHERE bean_name = "fluidStockController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyCmpltRController" WHERE bean_name = "reportDailyCmpltRController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyWellTestController" WHERE bean_name = "reportDailyWellTestController_nocost";
UPDATE acl_entry SET bean_name = "nocost_reportDailyWellServiceController" WHERE bean_name = "reportDailyWellServiceController_nocost";
UPDATE acl_entry SET bean_name = "nocost_wellHeadController" WHERE bean_name = "wellHeadController_nocost";
UPDATE acl_entry SET bean_name = "intv_hseIncidentByEventTypeController" WHERE bean_name = "hseIncidentByEventTypeController_intv";
UPDATE acl_entry SET bean_name = "rig_release_report_wellHeadController" WHERE bean_name = "wellHeadController_for_rig_release_report";
