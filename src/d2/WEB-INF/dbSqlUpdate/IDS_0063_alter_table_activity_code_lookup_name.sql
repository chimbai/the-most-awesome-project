[before_hibernate_initialization]


[after_hibernate_initialization]
ALTER TABLE lookup_class_code MODIFY name VARCHAR(255);
ALTER TABLE lookup_phase_code MODIFY name VARCHAR(255);
ALTER TABLE lookup_root_cause_code MODIFY name VARCHAR(255);
ALTER TABLE lookup_task_code MODIFY name VARCHAR(255);
