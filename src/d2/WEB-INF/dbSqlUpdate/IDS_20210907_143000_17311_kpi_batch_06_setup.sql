[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE kpi_repository SET kpi_name = "Casing - Gross Pipe Moving Duration per Stand (Avg)", kpi_description = "The average time spent running the Casing/Line Stand in the hole. No filters are applied. All connections are taken into account.", record_owner_uid = "dbsqlupdate", last_edit_datetime = NOW() WHERE kpi_repository_uid = "op_csgpmdur";
UPDATE kpi_repository SET kpi_name = "Tripping - POOH - Gross Pipe Moving Duration per Stand (Avg)", kpi_description = "The average time spent tripping the BHA out of the hole. No filters are applied. All connections are taken into account.", record_owner_uid = "dbsqlupdate", last_edit_datetime = NOW()  WHERE kpi_repository_uid = "op_trppoohpmdur";
UPDATE kpi_repository SET kpi_name = "Tripping - RIH - Gross Pipe Moving Duration per Stand (Avg)", kpi_description = "The average time spent tripping the BHA in the hole. No filters are applied. All connections are taken into account.", record_owner_uid = "dbsqlupdate", last_edit_datetime = NOW()  WHERE kpi_repository_uid = "op_trprihpmdur";

INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_csgpmduravgch','Casing - Gross Pipe Moving Duration per Stand (Avg) Cased Hole','The average time spent running the Casing/Line Stand in Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_csgpmduravgoh','Casing - Gross Pipe Moving Duration per Stand (Avg) Open Hole','The average time spent running the Casing/Line Stand in Open Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_csgpmdurp50','Casing - Gross Pipe Moving Duration per Stand (P50)','The mean time spent running the Casing/Line Stand in the hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_csgpmdurp50ch','Casing - Gross Pipe Moving Duration per Stand (P50) Cased Hole','The mean time spent running the Casing/Line Stand in Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_csgpmdurp50oh','Casing - Gross Pipe Moving Duration per Stand (P50) Open Hole','The mean time spent running the Casing/Line Stand in Open Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trppoohpmdursavgch','Tripping - POOH - Gross Pipe Moving Duration per Stand (Avg) Cased Hole','The average time spent tripping out the BHA from Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trppoohpmdursavgoh','Tripping - POOH - Gross Pipe Moving Duration per Stand (Avg) Open Hole','The average time spent tripping out the BHA from Open Hole. No filters are applied. All connections are taken into account',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trppoohpmdurp50','Tripping - POOH - Gross Pipe Moving Duration per Stand (P50)','The mean time spent tripping the BHA out of the hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trppoohpmdurp50ch','Tripping - POOH - Gross Pipe Moving Duration per Stand (P50) Cased Hole','The mean time spent tripping out the BHA from Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trppoohpmdurp50oh','Tripping - POOH - Gross Pipe Moving Duration per Stand (P50) Open Hole','The mean time spent tripping out the BHA from Open Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trprihpmduravgch','Tripping - RIH - Gross Pipe Moving Duration per Stand (Avg) Cased Hole','The average time spent tripping the BHA in Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trprihpmduravgoh','Tripping - RIH - Gross Pipe Moving Duration per Stand (Avg) Open Hole','The average time spent tripping the BHA in Open Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trprihpmdurp50','Tripping - RIH - Gross Pipe Moving Duration per Stand (P50)','The mean time spent tripping the BHA in the hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trprihpmdurp50ch','Tripping - RIH - Gross Pipe Moving Duration per Stand (P50) Cased Hole','The mean time spent tripping the BHA in Cased Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');
INSERT INTO kpi_repository (group_uid, last_edit_datetime, record_owner_uid,last_edit_user_uid,kpi_repository_uid, kpi_name,  kpi_description,  is_hole_section_kpi, is_active, default_unit, unit_classification, unit_type_uid) VALUES ((SELECT g.group_uid FROM `group` g WHERE (g.is_deleted IS NULL OR g.is_deleted = FALSE) AND g.parent_group_uid IS NULL),NOW(),'dbsqlupdate','dbsqlupdate','op_trprihpmdurp50oh','Tripping - RIH - Gross Pipe Moving Duration per Stand (P50) Open Hole','The mean time spent tripping the BHA in Open Hole. No filters are applied. All connections are taken into account.',TRUE,FALSE,'Minute','','TimeMeasure');