[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE depot_witsml_log_request SET is_active=0;

UPDATE depot_witsml_log_request dw SET well_uid = (SELECT well_uid FROM depot_well_operation_mapping WHERE depot_well_operation_mapping_uid = dw.depot_well_operation_mapping_uid);
UPDATE depot_witsml_log_request dw SET wellbore_uid = (SELECT wellbore_uid FROM depot_well_operation_mapping WHERE depot_well_operation_mapping_uid = dw.depot_well_operation_mapping_uid);
UPDATE depot_witsml_log_request dw SET operation_uid = (SELECT operation_uid FROM depot_well_operation_mapping WHERE depot_well_operation_mapping_uid = dw.depot_well_operation_mapping_uid);

insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90a9056923bc28016923c1e932045f','WITSML RSD Task',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'depotWitsmlLogRequestController','2c90c0c35ef05fa5015ef0ad74fc3d7a','90');
insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `record_owner_uid`, `last_edit_datetime`, `last_edit_user_uid`, `is_deleted`, `sys_deleted`, `is_submit`) values('2c90a9056923bc28016923c0a751045a','2c92f0e71da7ff03011dbcf01d7d218e','depotWitsmlLogRequestController','30','uniqueRigId01','2019-02-25 08:23:42','2c92eecd59f91e8a015a16462ba251e2',NULL,NULL,NULL);
