[before_hibernate_initialization]


[after_hibernate_initialization]
DROP INDEX `d2idx_daily_uid_group_uid` ON `mud_pit_capacity`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `riser_tension`;
DROP INDEX `d2idx_table_name_group_uid` ON `dynamic_field_meta`;
DROP INDEX `d2idx_setting_name_group_uid` ON `system_settings`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `sys_log_send_to_town`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `report_files`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `revision_files`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rig_generator`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rig_issue`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `report_daily`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `operation_dvd_annotation`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lookahead_requirement`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `operation_suspension`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `general_comment`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `activity`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `cement_job`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `cement_stage`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `bharun_daily_summary`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `drilling_parameters`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `bha_component_hours_used`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `kick_tolerance`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `survey_station`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `mud_properties`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `mud_volumes`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `solid_control_equipment`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `weather_environment`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `marine_properties`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `riser_components`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `riser_tally`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `marine_support`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `marine_support_secondary`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rig_parameters`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `personnel_on_site`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rig_pump_param`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rig_stock`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `support_vessel_param`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `transport_daily_param`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `logistic_daily`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `iceberg`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `hse_incident`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `key_performance_indicator`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `perforation_interval`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `equipment_failure`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `well_formation_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `rft_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `well_production_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `well_production_test_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `vendor_assessment`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `plug_and_abandon`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `leak_off_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `leak_off_test_pump_off_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `boiler_blow_down`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `waste_disposal`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `biostratigraphy`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lithology_shows`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `litholog`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `sample_description`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `pore_pressure`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `gas_readings`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `gas_peak`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `wireline_log`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `wireline_run`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `core`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `core_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `kick`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lwd_pre_logging`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lwd`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lwd_suite`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lwd_tool`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lwd_witness`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `drill_stem_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `drill_stem_test_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `formation_fluid_sample`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `geolshows`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `modular_formation_dynamics_tester`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `modular_formation_dynamics_tester_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `palynology`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `sidewall_core`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `formation_evaluation_assessment`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `wireline_pre_logging`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `safety_ticket`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `lesson_ticket`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `hse_environment_impact`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `unwanted_event`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `inspection_result`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `perforation`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `perforation_gun_configuration`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `production_fluid`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `well_fluid`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `production_string_pressure`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `production_string_testing`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `stimulation`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `stimulation_fluid`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `stimulation_information`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `swab`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `frac_fluid_volume`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `frac_sheet`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `frac_sheet_detail`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `frac_coil_log`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tool_string_master`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `recovery_volumes`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `well_test`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `slickline`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `air_package_summary`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `file_directory`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `cost_dailysheet`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `cost_rental`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `operation_afe`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `inventory_transaction`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_daily`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_activity`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_bharun_summary`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_bha_component`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `drill_pipe_tally`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_bit_summary`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_rig_pump_param`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_mud_properties`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_rig_stock`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_survey_station`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_drilling_parameters`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_wireline`;
DROP INDEX `d2idx_daily_uid_group_uid` ON `tour_personnel_on_site`;