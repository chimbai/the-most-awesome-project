[before_hibernate_initialization]
insert into `menu_item` (`menu_item_uid`, `label`, `group_uid`, `bean_name`, `parent`, `sequence`) values('2c90c0c46dedb7f1016dedbaf0670275','Wellbore Operation Mapping',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'edmWellboreOperationMappingController','2c90c0c4660fc85a016613df8f88110f','50');
insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`, `record_owner_uid`, `last_edit_datetime`, `last_edit_user_uid`, `is_deleted`, `sys_deleted`, `is_submit`) values('2c90c0c46dedb7f1016dedbc5cef0793','2c92f0e71da7ff03011dbcf01d7d218e','edmWellboreOperationMappingController','30','uniqueRigId01','2019-11-15 00:00:00','2c90c0c46b2f716c016b3f521c181f9e',NULL,NULL,NULL);

update `menu_item` set is_deleted=1 where `menu_item_uid`='2c90c0c4660fc85a016613e1b2311113' or `bean_name`='edmWellboreMappingController';
[after_hibernate_initialization]