[before_hibernate_initialization]
UPDATE uom_template_mapping SET unit_uid = 'Metre' where unit_mapping_uid IN (SELECT unit_mapping_uid FROM unit_mapping WHERE table_name = 'lithology_shows' AND field_name = 'plug_length');
UPDATE unit_mapping SET unit_type_uid = 'LengthMeasure' WHERE table_name = 'lithology_shows' AND field_name = 'plug_length';

[after_hibernate_initialization]