[before_hibernate_initialization]


[after_hibernate_initialization]

ALTER TABLE process_safety_questionnaire DROP COLUMN has_loss_fluid_barrier;
ALTER TABLE process_safety_questionnaire DROP COLUMN has_required_change_control;
ALTER TABLE process_safety_questionnaire DROP COLUMN loss_fluid_barrier_comment;
ALTER TABLE process_safety_questionnaire DROP COLUMN required_change_control_comment;