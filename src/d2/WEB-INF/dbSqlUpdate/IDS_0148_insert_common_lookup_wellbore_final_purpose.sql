[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE common_lookup_depot set is_deleted=1 where common_lookup_uid in (select common_lookup_uid from common_lookup where (is_deleted is null or is_deleted=false) and lookup_type_selection='wellboreFinalPurpose');
UPDATE common_lookup set is_deleted=1 where lookup_type_selection='wellboreFinalPurpose';
UPDATE common_lookup_type set is_deleted=1 where lookup_type_selection='wellboreFinalPurpose';

INSERT INTO common_lookup_type VALUES ("4028aab45359323101535935d77f0032",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"14","","wellbore.wellboreFinalPurpose","Wellbore Purpose","","uniqueRigId01","2016-03-09 02:30:14","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);

INSERT INTO common_lookup VALUES ("4028aab45359323101535936d5cd0041",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"1","wellbore.wellboreFinalPurpose","APR","",0,"","Appraisal","?witsml=appraisal",NULL,"uniqueRigId01","2016-03-09 02:31:19","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab453593231015359371d800046",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"2","wellbore.wellboreFinalPurpose","DEL","",0,"","Delineation","?witsml=unknown",NULL,"uniqueRigId01","2016-03-09 02:31:37","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab4535932310153593768c1004b",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"3","wellbore.wellboreFinalPurpose","DEV","",0,"","Development","?witsml=development",NULL,"uniqueRigId01","2016-03-09 02:31:56","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab45359323101535937b755005c",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"4","wellbore.wellboreFinalPurpose","EXP","",0,"","Exploration","?witsml=exploration",NULL,"uniqueRigId01","2016-03-09 02:32:17","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab453593231015359380ed00061",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"5","wellbore.wellboreFinalPurpose","INJ","",0,"","Injection","?witsml=unknown",NULL,"uniqueRigId01","2016-03-09 02:32:39","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab453593231015359385af30066",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"6","wellbore.wellboreFinalPurpose","OTH","",0,"","Other","?witsml=unknown",NULL,"uniqueRigId01","2016-03-09 02:32:58","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup VALUES ("4028aab45359323101535938abe80077",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),"7","wellbore.wellboreFinalPurpose","PROD","",0,"","Production","?witsml=development%20--%20producer",NULL,"uniqueRigId01","2016-03-09 02:33:19","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);

INSERT INTO common_lookup_depot VALUES ("4028aab45359323101535936d5d70043",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535936d5cd0041","witsml","appraisal","uniqueRigId01","2016-03-09 02:31:19","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab453593231015359371d8a0048",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359371d800046","witsml","unknown","uniqueRigId01","2016-03-09 02:31:37","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab4535932310153593768cb004d",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab4535932310153593768c1004b","witsml","development","uniqueRigId01","2016-03-09 02:31:56","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab45359323101535937b75f005e",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535937b755005c","witsml","exploration","uniqueRigId01","2016-03-09 02:32:16","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab453593231015359380ee40063",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359380ed00061","witsml","unknown","uniqueRigId01","2016-03-09 02:32:39","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab453593231015359385afd0068",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359385af30066","witsml","unknown","uniqueRigId01","2016-03-09 02:32:58","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("4028aab45359323101535938abf20079",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535938abe80077","witsml","development -- producer","uniqueRigId01","2016-03-09 02:33:19","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);

UPDATE common_lookup SET depot_string='?witsml=appraisal&edm=Appraisal,Pilot' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='APR';
UPDATE common_lookup SET depot_string='?witsml=unknown&edm=Delineation' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='DEL';
UPDATE common_lookup SET depot_string='?witsml=development&edm=Development' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='DEV';
UPDATE common_lookup SET depot_string='?witsml=exploration&edm=Exploration' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='EXP';
UPDATE common_lookup SET depot_string='?witsml=unknown&edm=Injection' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='INJ';
UPDATE common_lookup SET depot_string='?witsml=unknown&edm=Other' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='OTH';
UPDATE common_lookup SET depot_string='?witsml=development%20--%20producer&edm=Production' WHERE lookup_type_selection='wellbore.wellboreFinalPurpose' AND short_code='PROD';

INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154380e23dc000a",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535936d5cd0041","edm","Appraisal,Pilot","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154380f4ee8000d",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359371d800046","edm","Delineation","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154380fbe890010",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab4535932310153593768c1004b","edm","Development","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154380feda30013",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535937b755005c","edm","Exploration","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154381014f10016",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359380ed00061","edm","Injection","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be00154381031450019",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab453593231015359385af30066","edm","Other","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);
INSERT INTO common_lookup_depot VALUES ("2c90a9fb54380be0015438105c29001c",(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),NULL,"4028aab45359323101535938abe80077","edm","Production","uniqueRigId01","2016-03-10 06:53:41","user3d3f8acee7cb39.74435223",NULL,NULL,NULL);