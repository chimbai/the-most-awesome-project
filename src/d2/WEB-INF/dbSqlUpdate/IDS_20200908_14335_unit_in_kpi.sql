[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE kpi_repository SET unit_type_uid='TimeMeasure' WHERE default_unit='Minute';
UPDATE kpi_repository SET unit_type_uid='VelocityMeasure' WHERE default_unit='MetresPerHour';
UPDATE kpi_repository SET unit_type_uid='NumberMeasure' WHERE default_unit='StandPerHour';
UPDATE kpi_repository SET unit_type_uid='LengthMeasure' WHERE default_unit='Metre';