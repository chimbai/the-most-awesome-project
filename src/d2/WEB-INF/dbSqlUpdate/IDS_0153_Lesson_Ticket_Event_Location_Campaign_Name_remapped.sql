[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE lesson_ticket lt, well w SET lt.event_location = w.well_uid WHERE lt.event_location = w.well_name AND (w.is_deleted IS NULL OR w.is_deleted = FALSE);
UPDATE lesson_ticket lt, campaign c SET lt.campaign_name = c.campaign_uid WHERE lt.campaign_name = c.campaign_name AND (c.is_deleted IS NULL OR c.is_deleted = FALSE);