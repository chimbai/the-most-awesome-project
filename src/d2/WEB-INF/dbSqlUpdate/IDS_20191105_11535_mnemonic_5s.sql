[before_hibernate_initialization]


[after_hibernate_initialization]
DELETE FROM common_lookup  WHERE lookup_type_selection='rigStateMapping.mnemonic';
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a9c66e4902ba016e490975780021',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'2','rigStateMapping.mnemonic ','Channel_1','Channel 1');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a9c66e4902ba016e490975840023',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'4','rigStateMapping.mnemonic ','Channel_3','Channel 3');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a9c66e4902ba016e4909758a0025',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'3','rigStateMapping.mnemonic ','Channel_2','Channel 2');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`) values('2c90a9c66e4902ba016e490975900027',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'1','rigStateMapping.mnemonic ','Basic_State','Basic State');

UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='1';
UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='2';
UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='3';
UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='4';
UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='5';
UPDATE rig_state_mapping SET mnemonic='Basic_State' WHERE rig_state_id='10';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='12';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='13';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='14';
UPDATE rig_state_mapping SET mnemonic='Channel_3' WHERE rig_state_id='15';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='17';
UPDATE rig_state_mapping SET mnemonic='Channel_3' WHERE rig_state_id='18';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='19';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='20';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='22';
UPDATE rig_state_mapping SET mnemonic='Channel_3' WHERE rig_state_id='23';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='24';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='28';
UPDATE rig_state_mapping SET mnemonic='Channel_3' WHERE rig_state_id='29';
UPDATE rig_state_mapping SET mnemonic='Channel_3' WHERE rig_state_id='30';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='31';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='35';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='36';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='38';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='40';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='41';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='42';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='43';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='44';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='45';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='47';
UPDATE rig_state_mapping SET mnemonic='Channel_1' WHERE rig_state_id='50';
UPDATE rig_state_mapping SET mnemonic='Channel_2' WHERE rig_state_id='51';
