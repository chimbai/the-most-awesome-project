[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE formation_fluid_sample SET flow_pressure_avg=(flow_pressure_max + flow_pressure_min) / 2;
UPDATE formation_fluid_sample SET chamber_duration=TIME_TO_SEC(chamber_end_time) - TIME_TO_SEC(chamber_start_time);
UPDATE formation_fluid_sample SET chamber_duration=chamber_duration+1 WHERE (chamber_duration%10) > 0 AND chamber_duration>1;
UPDATE formation_fluid_sample SET pump_duration=TIME_TO_SEC(pump_end_time) - TIME_TO_SEC(pump_start_time);
UPDATE formation_fluid_sample SET pump_duration=pump_duration+1 WHERE (pump_duration%10) > 0 AND pump_duration>1;
 