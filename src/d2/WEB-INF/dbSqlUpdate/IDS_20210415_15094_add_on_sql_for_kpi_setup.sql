[before_hibernate_initialization]

[after_hibernate_initialization]
/* Add idsbridge to importExportServerProperties */
INSERT INTO import_export_server_properties (server_properties_uid, data_transfer_type, service_name, end_point, last_edit_datetime, last_edit_user_uid, depot_version, bearer_token)SELECT REPLACE(UUID(),'-',''),'idsbridge','GraphQL','https://sg.idsdatanet.com/ids-bridge/graphql',NOW(),'dbsqlupdate','1.0','1|sgr5FPaKbwOhKZJ4dDeq6Rjekt2kPQo9v9iVqFrJVFtmui9lpXr8dx1CkZns0FcKp0EEbSSyuYEem3sg' FROM import_export_server_properties WHERE NOT EXISTS (SELECT data_transfer_type FROM import_export_server_properties WHERE data_transfer_type = "idsbridge") LIMIT 1;

/* Add KPI Scorecard into anova_repository */
INSERT INTO anova_repository (anova_repository_uid,resource_title,resource_description,resource_type,bean_name,resource_url,last_edit_datetime,anova_repository_package) SELECT 'kpiscorecardid','KPI Scorecard','Rig/Well Performance Tracker','custom','jasperReportAnovaViewController','kpiScorecard',NOW(),'advance' FROM anova_repository WHERE NOT EXISTS (SELECT resource_url FROM anova_repository WHERE resource_url = "kpiScorecard") LIMIT 1;

/* Set Parameter Mapping in anova_repository_parameter_mapping */
INSERT INTO anova_repository_parameter_mapping (anova_repository_parameter_mapping_uid, anova_repository_uid, sequence, filter_element, url_parameter, record_owner_uid, last_edit_datetime, multi_value) SELECT 'kpiscorecarduidparam','kpiscorecardid','1','operationUid','operationUid','dbsqlupdate',NOW(), TRUE FROM anova_repository_parameter_mapping WHERE NOT EXISTS (SELECT anova_repository_uid FROM anova_repository_parameter_mapping WHERE anova_repository_uid = (SELECT anova_repository_uid FROM anova_repository WHERE resource_url = "kpiScorecard")) LIMIT 1;

/* Create new 'menu' in ANOVA Group if KPI scorecard not exist. */
INSERT INTO anova_group (anova_group_uid,sequence,`description`,record_owner_uid,last_edit_datetime) SELECT 'kpiscorecardidanvgrp','99','Scorecard','dbsqlupdate',NOW() FROM anova_group WHERE NOT EXISTS (SELECT anova_repository_uid FROM anova_repository_report_group WHERE anova_repository_uid = (SELECT anova_repository_uid FROM anova_repository WHERE resource_url = "kpiScorecard")) LIMIT 1;

/* Set Report Group in anova_repository_report_group */
INSERT INTO anova_repository_report_group (anova_repository_report_group_uid, anova_repository_uid, anova_group_uid, record_owner_uid, last_edit_datetime) SELECT 'kpiscorecarduidgroup','kpiscorecardid',(SELECT anova_group_uid FROM anova_group WHERE `description` = "DPM" AND (is_deleted IS NULL OR is_deleted = "0")),'dbsqlupdate', NOW() FROM anova_repository_report_group WHERE NOT EXISTS (SELECT anova_repository_uid FROM anova_repository_report_group WHERE anova_repository_uid = (SELECT anova_repository_uid FROM anova_repository WHERE resource_url = "kpiScorecard")) LIMIT 1;

/* Add Custom into Resource Type dropdown */
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `description`, `is_active`, `parent_reference_key`, `lookup_label`, `depot_string`, `on_off_shore`, `record_owner_uid`, `last_edit_datetime`, `last_edit_user_uid`, `is_deleted`, `sys_deleted`, `is_submit`) SELECT '2c92491a73bc022f0173bc805313005b', (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL), '3', 'anovaRepository.resourceType', 'custom', '', TRUE, '', 'Custom', NULL, NULL, 'uniqueRigId01', '2017-10-05 02:37:00', 'user3d3f8acee7cb39.74435223', NULL, NULL, NULL FROM common_lookup WHERE NOT EXISTS (SELECT short_code FROM common_lookup WHERE lookup_type_selection = "anovaRepository.resourceType" AND short_code = "custom") LIMIT 1;

/* Update unit_type_uid for default_uid=JointPerHour */
UPDATE kpi_repository SET unit_type_uid='NumberMeasure' WHERE default_unit='JointPerHour';