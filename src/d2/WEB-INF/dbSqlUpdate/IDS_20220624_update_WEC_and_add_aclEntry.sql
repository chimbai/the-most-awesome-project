[before_hibernate_initialization]

[after_hibernate_initialization]
/* change the current WEC chevron records to not imported */
UPDATE well_external_catalog SET STATUS = NULL WHERE well_name IN ("CHEVRON HZ FOXCK 13-29-62-19","CHEVRON HZ FOXCK 5-28-62-19","CHEVRON 102 HZ FOXCK 11-14-62-19","CHEVRON HZ FOXCK 2-29-62-19","CHEVRON 103 HZ FOXCK 5-14-62-19","CHEVRON HZ FOXCK 4-14-62-19","CHEVRON 104 HZ FOXCK 15-10-62-19","CHEVRON 103 HZ FOXCK 1-15-62-19","CHEVRON 102 HZ FOXCK 10-29-62-19");
/* add Super Admin Group acl_entry for the WEC webservice (used to view raw data or have a custom Import from X server button text) */
INSERT INTO acl_entry (acl_group_uid, bean_name, permissions) VALUES ((select acl_group_uid from acl_group where name = "Super Administrator Group" LIMIT 1), "webService.wellExternalCatalogServiceController", 30);