[before_hibernate_initialization]
ALTER TABLE wellbore CHANGE sys_deleted sys_deleted INTEGER;
ALTER TABLE operation CHANGE sys_deleted sys_deleted INTEGER;
ALTER TABLE daily CHANGE sys_deleted sys_deleted INTEGER;
ALTER TABLE bharun CHANGE sys_deleted sys_deleted INTEGER;
ALTER TABLE tour_bharun CHANGE sys_deleted sys_deleted INTEGER;

[after_hibernate_initialization]

