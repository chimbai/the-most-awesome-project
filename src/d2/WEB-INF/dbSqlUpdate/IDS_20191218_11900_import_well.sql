[before_hibernate_initialization]


[after_hibernate_initialization]
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('402881e46ef37b21016ef384265b001c',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'depot_type','subscribe','1','D2 Subscriber');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('402881e46ef37b21016ef3842686001e',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'depot_type','publish','1','D2 Publisher');

insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `is_active`, `parent_reference_key`, `lookup_label`) values('402881e46ef37b21016ef3860a490029',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'depot_version','1.0','1','publish','1.0');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `is_active`, `parent_reference_key`, `lookup_label`) values('402881e46ef37b21016ef385dc6f0027',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'depot_version','1.0','1','subscribe','1.0');

insert into `user` (`user_uid`, `group_uid`, `user_name`, `password`, `fname`, `email`, `password_changed_date`, `is_system_user`) values('402881e46f169d82016f16b556c50057',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'ps_user','7432355b1bb140a0950303a15b3e0189','Publish Subscribe','ps@ids.com','2019-12-18 01:53:00','1');
insert into `user_acl_group` (`user_acl_group_uid`, `user_uid`, `acl_group_uid`, `record_owner_uid`, `last_edit_datetime`, `last_edit_user_uid`, `is_deleted`, `sys_deleted`, `is_submit`) values('402881e46f169d82016f16b556ef0059','402881e46f169d82016f16b556c50057','2c92f0e71da7ff03011dbcf01d7d218e','uniqueRigId01','2019-12-18 01:53:00','2c92eecd59f91e8a015a16462ba251e2',NULL,NULL,NULL);

insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`) values('402881e46ef2c16a016ef2c73b61000f','2c92f0e71da7ff03011dbcf01d7d218e','publishSubscribeController','30');
insert into `acl_entry` (`acl_entry_uid`, `acl_group_uid`, `bean_name`, `permissions`) values('402881ec6eeefe40016eef05bc9e0018','2c92f0e71da7ff03011dbcf01d7d218e','webService.wellTransferController','30');
