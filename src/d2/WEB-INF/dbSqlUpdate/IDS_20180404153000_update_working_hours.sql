[before_hibernate_initialization]


[after_hibernate_initialization]
update personnel_on_site set total_working_hours=total_working_hours * 3600 where working_hours=12;
update personnel_on_site set working_hours=working_hours * 3600 where working_hours=12;
update uom_template_mapping set unit_uid='hour' where unit_mapping_uid in (select unit_mapping_uid from unit_mapping where table_name='personnel_on_site' and field_name='working_hours');
update uom_template_mapping set unit_uid='hour' where unit_mapping_uid in (select unit_mapping_uid from unit_mapping where table_name='personnel_on_site' and field_name='total_working_hours');