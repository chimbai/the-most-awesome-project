[before_hibernate_initialization]
update menu_item set specific_view='WellManagement' where specific_view='Well Management';
update menu_item set specific_view='SiteManagement' where specific_view='Site Management';
update menu_item set specific_view='TourManagement' where specific_view='Tour Management';
update menu_item set specific_view='AssetManagement' where specific_view='Asset Management';

update menu_item_view_type set view_name='WellManagement' where view_name='Well Management';
update menu_item_view_type set view_name='SiteManagement' where view_name='Site Management';
update menu_item_view_type set view_name='TourManagement' where view_name='Tour Management';
update menu_item_view_type set view_name='AssetManagement' where view_name='Asset Management';

update common_lookup set short_code='WellManagement' where short_code='Well Management';
update common_lookup set short_code='TourManagement' where short_code='Tour Management';
update common_lookup set short_code='AssetManagement' where short_code='Asset Management';
update common_lookup set short_code='SiteManagement' where short_code='Site Management';

[after_hibernate_initialization]