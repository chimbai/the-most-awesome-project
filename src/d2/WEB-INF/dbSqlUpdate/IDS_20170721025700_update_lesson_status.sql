[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE common_lookup_type SET lookup_type_selection='lessonTicket.lessonStatus', lookup_name='Lesson Status' WHERE common_lookup_type_uid='2c92eec84fda598f01503bffd9d52e01';
UPDATE common_lookup SET lookup_type_selection='lessonTicket.lessonStatus' WHERE common_lookup_uid IN ('2c92eec84fda598f01503c00b7bf2e04','2c92eec84fda598f01503c00e34c2ecc','2c92eec84fda598f01503c017c93349a');
UPDATE common_lookup SET is_deleted=1 WHERE common_lookup_uid='2c92eec84fda598f01503c017c93349a';