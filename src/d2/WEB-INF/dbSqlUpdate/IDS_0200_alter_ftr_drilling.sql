[before_hibernate_initialization]
UPDATE unit_mapping SET datum_conversion=1, is_md=1 WHERE field_name = 'start_depth' AND table_name = 'ftr_drilling';
UPDATE unit_mapping SET datum_conversion=1, is_md=1 WHERE field_name = 'end_depth' AND table_name = 'ftr_drilling';

[after_hibernate_initialization]
