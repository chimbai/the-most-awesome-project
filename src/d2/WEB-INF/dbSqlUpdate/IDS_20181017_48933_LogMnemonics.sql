[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE uom_template_mapping SET unit_uid='Hour' WHERE unit_mapping_uid='log_curve_definition_request_time_interval_TimeMeasure';

delete from common_lookup WHERE lookup_type_selection='Log.mnemonicType'; 
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb6e4501',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'1','Log.mnemonicType','min:datetime','1','Start Date');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb784502',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'2','Log.mnemonicType','max:datetime','1','End Date');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb814503',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'3','Log.mnemonicType','min:measure','1','Measure(Min)');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb8a4504',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'4','Log.mnemonicType','avg:measure','1','Measure(Avg)');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb944505',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'5','Log.mnemonicType','max:measure','1','Measure(Max)');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c92eecd598150f801598157cb9d4506',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'6','Log.mnemonicType','sum:measure','1','Measure(Sum)');
insert into `common_lookup` (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `is_active`, `lookup_label`) values('2c90a90666801d5801668093664e0007',(SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL),'7','Log.mnemonicType','grp:rigState','1','Rig State');
