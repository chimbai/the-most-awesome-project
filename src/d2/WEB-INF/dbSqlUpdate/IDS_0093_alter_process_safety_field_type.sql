[before_hibernate_initialization]


[after_hibernate_initialization]

ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_phase_barrier_diagram_24hr VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_well_control_equipment_failed VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_pass_cement_equipment_test VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_pass_cement_operation VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_pass_drilling_completion_standards VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_change_control_24hr VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN is_below_program_mud_weight VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_monitored_fluid_column VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_change_control_monitor_fluid_column VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_connection_gas_event VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_pass_equipment_test VARCHAR(50);
ALTER TABLE process_safety_questionnaire MODIFY COLUMN has_temporary_barrier_pass_test VARCHAR(50);