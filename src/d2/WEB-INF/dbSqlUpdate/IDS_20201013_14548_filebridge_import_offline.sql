[before_hibernate_initialization]

[after_hibernate_initialization]
ALTER TABLE `file_bridge_files` MODIFY well_uid VARCHAR(50) NULL;
ALTER TABLE `file_bridge_files` MODIFY wellbore_uid VARCHAR(50) NULL;
ALTER TABLE `file_bridge_files` MODIFY operation_uid VARCHAR(50) NULL;
ALTER TABLE `file_bridge_files` MODIFY daily_uid VARCHAR(50) NULL;