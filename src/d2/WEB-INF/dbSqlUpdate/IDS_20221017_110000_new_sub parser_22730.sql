[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `parent_reference_key`, `lookup_label`, `last_edit_datetime`, `last_edit_user_uid`) VALUES (CONCAT('sublookup_', MD5('OpenWells_Capricorn_DOR_PDF_urclxedmxntw')), (SELECT group_uid FROM `group` WHERE parent_group_uid IS NULL LIMIT 1), 'FileBridgeTypeLookupSubReport', 'urclxedmxntw', 'OpenWells_Capricorn_DOR_PDF', 'Weatherford/PDO v4.1.1210', NOW(), 'dbsqlupdate');
UPDATE file_bridge_type_lookup SET report_sub_type = docparser_parser_id WHERE 1;
UPDATE file_bridge_type_lookup SET docparser_parser_id = 'tcjzvvgdwsxe' WHERE parser_key='EQUINOR_DDR_PDF';
UPDATE file_bridge_type_lookup SET docparser_parser_id = 'jqpjymobumqw' WHERE parser_key='OpenWells_Capricorn_DOR_PDF';