[before_hibernate_initialization]

[after_hibernate_initialization]
UPDATE rig_state_raw SET hole_end_depth_raw = hole_end_depth_md_msl;
UPDATE rig_state_raw SET hole_start_depth_raw = hole_start_depth_md_msl;
UPDATE rig_state_raw SET bit_end_depth_raw = bit_end_depth_md_msl;
UPDATE rig_state_raw SET bit_start_depth_raw = bit_start_depth_md_msl;