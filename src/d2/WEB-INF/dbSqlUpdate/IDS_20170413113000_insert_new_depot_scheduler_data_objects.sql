[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`, `last_edit_datetime`) VALUES ((SELECT UUID()), (SELECT group_uid FROM `group` limit 1), 'depot.schedulerDataObjects', 'mudLogGeologyInterval', 'mudLog (geologyInterval)', '1', 'uniqueRigId01', now());

INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`, `last_edit_datetime`) VALUES ((SELECT UUID()), (SELECT group_uid FROM `group` limit 1), 'depot.schedulerDataObjects', 'mudLogLitholog', 'mudLog (litholog)', '1', 'uniqueRigId01', now());

INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`, `last_edit_datetime`) VALUES ((SELECT UUID()), (SELECT group_uid FROM `group` limit 1), 'depot.schedulerDataObjects', 'mudLogShow', 'mudLog (show)', '1', 'uniqueRigId01', now());

INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`, `last_edit_datetime`) VALUES ((SELECT UUID()), (SELECT group_uid FROM `group` limit 1), 'depot.schedulerDataObjects', 'mudLogChromatograph', 'mudLog (chromatograph)', '1', 'uniqueRigId01', now());

INSERT INTO `common_lookup` (`common_lookup_uid`, `group_uid`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`, `last_edit_datetime`) VALUES ((SELECT UUID()), (SELECT group_uid FROM `group` limit 1), 'depot.schedulerDataObjects', 'mudLogMudGas', 'mudLog (mudGas)', '1', 'uniqueRigId01', now());
