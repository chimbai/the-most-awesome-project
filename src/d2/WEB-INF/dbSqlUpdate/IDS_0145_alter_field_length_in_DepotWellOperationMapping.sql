[before_hibernate_initialization]


[after_hibernate_initialization]
alter table depot_well_operation_mapping modify well_uid varchar(50);
alter table depot_well_operation_mapping modify depot_well_uid varchar(50);
alter table depot_well_operation_mapping modify wellbore_uid varchar(50);
alter table depot_well_operation_mapping modify depot_wellbore_uid varchar(50);
alter table depot_well_operation_mapping modify operation_uid varchar(50);
alter table depot_well_operation_mapping modify depot_operation_uid varchar(50);
alter table depot_well_operation_mapping modify scheduler_template_uid varchar(50);