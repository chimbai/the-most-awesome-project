[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO common_lookup_type VALUES ('EDM_PUSH_CONDITION',(SELECT group_uid FROM `group` LIMIT 1),'1','','edm.push_condition','EDM Push Condition','','uniqueRigId01','2015-10-20 06:23:15','dbsqlupdate',NULL,NULL,NULL);

INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_PUSH_CONDITION_AUTO', (SELECT group_uid FROM `group` LIMIT 1),'1','edm.push_condition','auto','Auto', 1, 'dbsqlupdate');
INSERT INTO common_lookup (`common_lookup_uid`, `group_uid`, `sequence`, `lookup_type_selection`, `short_code`, `lookup_label`, `is_active`, `record_owner_uid`) VALUES ('EDM_PUSH_CONDITION_TOWN_SIDE_QC_DONE', (SELECT group_uid FROM `group` LIMIT 1),'2','edm.push_condition','town_side_qc_done','Town Side QC Done', 1, 'dbsqlupdate');

UPDATE edm_configuration SET push_condition='auto';