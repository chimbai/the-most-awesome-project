[before_hibernate_initialization]


[after_hibernate_initialization]
INSERT INTO lookup_bha_component (lookup_bha_component_uid, group_uid, `type`, record_owner_uid, last_edit_datetime, last_edit_user_uid) SELECT concat(a.common_lookup_uid, '_lookup_import'), (SELECT group_uid FROM `group` WHERE (is_deleted IS NULL OR is_deleted = FALSE) AND parent_group_uid IS NULL), a.short_code,'dbsqlupdate',NOW(),'dbsqlupdate' FROM (SELECT common_lookup_uid, short_code FROM common_lookup WHERE is_deleted IS NULL AND (is_active ='1' OR is_active IS NULL) AND lookup_type_selection ='bhacomponent.equipment_type_lookup' AND short_code  NOT IN (SELECT `type` FROM lookup_bha_component WHERE is_deleted IS NULL)) a;
UPDATE lookup_bha_component AS lb1, common_lookup_depot AS lb2 SET lb1.witsml_lookup_key = lb2.depot_value WHERE  CONCAT(lb2.common_lookup_uid,'_lookup_import') = lb1.lookup_bha_component_uid AND depot_type = 'witsml';
UPDATE common_lookup SET is_deleted ='1', last_edit_datetime = NOW() WHERE lookup_type_selection ='bhacomponent.equipment_type_lookup';
UPDATE bha_component AS bc, lookup_bha_component AS lbc SET bc.type = lbc.lookup_bha_component_uid, bc.last_edit_datetime = NOW() WHERE bc.type = lbc.type AND (lbc.is_deleted IS NULL OR lbc.is_deleted =0);