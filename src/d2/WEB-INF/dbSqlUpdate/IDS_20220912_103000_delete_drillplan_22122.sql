[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE activity_ext_code_mapping SET is_deleted=1 WHERE activity_ext_code_def_uid IN (SELECT activity_ext_code_def_uid FROM activity_ext_code_def WHERE `name`='Drillplan');
UPDATE activity_ext_code_def SET is_deleted=1 WHERE `name`='Drillplan';
UPDATE file_bridge_type_lookup SET activity_ext_code_def_uid = (SELECT activity_ext_code_def_uid FROM activity_ext_code_def WHERE `name`='WCDF') WHERE parser_key='IWC_DRILLPLAN_TIME_PLAN_XLSX';