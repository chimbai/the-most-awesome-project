[before_hibernate_initialization]


[after_hibernate_initialization]
ALTER TABLE activity_rsd MODIFY start_datetime DATETIME NULL; 
ALTER TABLE activity_rsd MODIFY end_datetime DATETIME NULL;
ALTER TABLE bha_component MODIFY top_connection_thread VARCHAR(50) NULL; 
ALTER TABLE bitrun MODIFY connection_thread VARCHAR(50) NULL;
ALTER TABLE custom_code_link MODIFY well_uid VARCHAR(50) NULL;
ALTER TABLE custom_code_link MODIFY wellbore_uid VARCHAR(50) NULL;
ALTER TABLE custom_code_link MODIFY operation_uid VARCHAR(50) NULL;
ALTER TABLE custom_code_link MODIFY daily_uid VARCHAR(50) NULL;
ALTER TABLE custom_ftr_link MODIFY group_uid VARCHAR(50) NULL;
ALTER TABLE custom_ftr_link MODIFY well_uid VARCHAR(50) NULL;
ALTER TABLE custom_ftr_link MODIFY wellbore_uid VARCHAR(50) NULL;
ALTER TABLE custom_ftr_link MODIFY operation_uid VARCHAR(50) NULL;
ALTER TABLE custom_ftr_link MODIFY daily_uid VARCHAR(50) NULL;
ALTER TABLE custom_parameter_link MODIFY well_uid VARCHAR(50) NULL;
ALTER TABLE custom_parameter_link MODIFY wellbore_uid VARCHAR(50) NULL;
ALTER TABLE custom_parameter_link MODIFY operation_uid VARCHAR(50) NULL;
ALTER TABLE custom_parameter_link MODIFY daily_uid VARCHAR(50) NULL;
ALTER TABLE custom_parameter_link MODIFY custom_ftr_link_uid VARCHAR(50) NULL;
ALTER TABLE mud_pit_capacity MODIFY pit_number VARCHAR(50) NULL;
ALTER TABLE menu_item MODIFY bean_name VARCHAR(50) NULL; 
ALTER TABLE rig_pump_component_log MODIFY cylinder_number VARCHAR(10) NULL; 
ALTER TABLE witsml_change_log MODIFY well_uid VARCHAR(50) NULL; 
ALTER TABLE witsml_change_log MODIFY wellbore_uid VARCHAR(50) NULL; 
ALTER TABLE witsml_change_log MODIFY operation_uid VARCHAR(50) NULL; 
