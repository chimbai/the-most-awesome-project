[before_hibernate_initialization]


[after_hibernate_initialization]
UPDATE activity a, daily d SET start_datetime=concat(date_format(d.day_date,'%Y%m%d'),date_format(a.start_datetime,'%H%i%s')) WHERE a.daily_uid=d.daily_uid AND (d.is_deleted IS NULL OR d.is_deleted=FALSE) AND (a.is_deleted=FALSE OR a.is_deleted IS NULL);

UPDATE activity a, daily d SET end_datetime=concat(date_format(d.day_date,'%Y%m%d'),date_format(a.end_datetime,'%H%i%s')) WHERE a.daily_uid=d.daily_uid AND (d.is_deleted IS NULL OR d.is_deleted=FALSE) AND (a.is_deleted=FALSE OR a.is_deleted IS NULL); 