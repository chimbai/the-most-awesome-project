[before_hibernate_initialization]
ALTER TABLE cost_afe_detail MODIFY COLUMN account_code VARCHAR(200);
ALTER TABLE cost_afe_detail_vault MODIFY COLUMN account_code VARCHAR(200);
ALTER TABLE cost_afe_detail_adjusted MODIFY COLUMN account_code VARCHAR(200);

[after_hibernate_initialization]