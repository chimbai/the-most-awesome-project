function onFilterSelect() {
	document.filterForm.submit();
};

function onMenuWellDaySelect(currentObj) {
	var submit = true;
	if(currentObj.attr("id") == "_sys_menu_daily") {
		var value = currentObj.val();
		if(value == "createNewDate" || value == "...") {
			submit = false;
			var previousSelected = currentObj.attr("selectedValue")? currentObj.attr("selectedValue"):"...";
			currentObj.val(previousSelected);
			
			// popup window to create a new day date.
			// if(value == "createNewDate") $("#createNewDateLink").trigger("click");
			
			window.location.href = "reportdaily.html?newRecordInputMode=1";
		}
	}
	
	if(submit) document.menuForm.submit();
};
 
function onMenuWellDayTourSelect(currentObj) {
	var submit = true;
	if(currentObj.attr("id") == "_sys_menu_tour") {
		var value = currentObj.val();
	}
	if(submit) document.menuForm.submit();
};

function onMenuUOMChange(){
	document.menuForm.submit();
};

function onPopupMenuSelect(url){
	tb_remove();
	d2ScreenManager.showSimpleModalMsg("Change Menu", "Loading, please wait...");
	window.location = url;
};

function restartSimpleAjaxRefreshChecking(sourceElementId, labelWhenRestart){
	var source = $("#" + sourceElementId).get(0);
	_submitSimpleAjax(source, labelWhenRestart, 'restartRefreshChecking');
};

function submitSimpleAjaxAction(sourceButton, labelWhenSubmit){
	_submitSimpleAjax(sourceButton, labelWhenSubmit, 'submitAction');
};

function _submitSimpleAjax(sourceButton, labelWhenSubmit, systemAction){
	if($(sourceButton).attr("simpleAjax.state") != "waiting"){
		$(sourceButton).attr("value",labelWhenSubmit);
		
		$(sourceButton).ajaxError(function(request, settings){
			alert("Unable to get a response from server, please refresh current page");
 			$(this).attr("value",$(sourceButton).attr("simpleAjaxOriginalLabel"));
		});
		
		var targetUrl = $(sourceButton).attr("targetUrl");
		if(! targetUrl) targetUrl = document.URL;
		$.getJSON(targetUrl, {simpleAjaxAction: systemAction, action: $(sourceButton).attr("simpleAjaxAction"), sourceElementId: sourceButton.id}, function(json){
			if("true" == json.responseSet){
				if(json.responseButtonLabel != ""){
					$(sourceButton).attr("value",json.responseButtonLabel);
				}else{
					$(sourceButton).attr("value",$(sourceButton).attr("simpleAjaxOriginalLabel"));
				}
				if(json.responseAlert != "") alert(json.responseAlert);
				
				if(json.checkPendingAction == "true"){
					$(sourceButton).attr("simpleAjax.state", "waiting");
					var checkInterval = 5000;
					if(json.checkPendingActionInterval > 0) checkInterval = json.checkPendingActionInterval;
					setTimeout("checkSimpleAjaxPendingAction(" + checkInterval + ",'" + $(sourceButton).attr("id") + "','" + $(sourceButton).attr("simpleAjaxAction") + "','" + json.actionId + "')", checkInterval);
				}
			}else{
				$(sourceButton).attr("value",$(sourceButton).attr("simpleAjaxOriginalLabel"));
			}
		});
	}
};

function checkSimpleAjaxPendingAction(checkInterval, sourceElementId, action, actionId){
	var sourceButton = $("#" + sourceElementId);
	
	if(sourceButton.attr("ajax-highlight") == undefined){
		sourceButton.attr("ajax-highlight", "0");
	}

	if(sourceButton.attr("ajax-highlight") == "0"){
		sourceButton.attr("style","color:orange");
		sourceButton.attr("ajax-highlight", "1");
	}else{
		sourceButton.attr("style","color:white");
		sourceButton.attr("ajax-highlight", "0");
	}

	var targetUrl = sourceButton.attr("targetUrl");
	if(! targetUrl) targetUrl = document.URL;
	
	$.getJSON(targetUrl, {simpleAjaxAction: "getRefreshInstruction", action: action, actionId: actionId, sourceElementId: sourceElementId}, function(json){
		if(json.refreshAlert != "") alert(json.refreshAlert);
		if("checkAgain" == json.refreshInstruction){
			setTimeout("checkSimpleAjaxPendingAction(" + checkInterval + ",'" + sourceElementId + "','" + action + "','" + actionId + "')", checkInterval);
		}else if("reloadScreen" == json.refreshInstruction){
			window.location.href = window.location.href;
		}else if("stopChecking" == json.refreshInstruction){
			sourceButton.attr("simpleAjax.state", "");
			sourceButton.attr("value",sourceButton.attr("simpleAjaxOriginalLabel"));
			sourceButton.attr("style","color:white");
		} 
	});
};

var d2ScreenManager = {
	gotoToUrl:function(url,msg){
		this.showSimpleModalMsg("Redirect", msg);
		window.location.href = url;	
	},
	
	reloadParentHtmlPage: function(clearQueryString){
		this.showSimpleModalMsg("Refresh", "Loading, please wait...");
		if(clearQueryString){
			window.location.href = window.location.href.replace(/\?.*$/, "");
		}else{
			window.location.href = window.location.href;
		}
	},
	
	addHiddenInputToForm: function(formId, name, value){
		var input = jQuery("<input type='hidden'/>");
		input.attr("name", name);
		input.val(value);

		var form = $("#" + formId);
		form.append(input);	
	},
	
	submitForServerSideProcess: function(params){
		for(key in params){
			d2ScreenManager.addHiddenInputToForm('form',key, params[key]);
		}
		
		d2ScreenManager.showSimpleModalMsg("Loading", "Loading, please wait...");
		var form_element = $("#form");
		form_element.find("#d2RootAction").val("submitForServerSideProcess");
		form_element.append(d2ScreenManager.getFormSubmissionMarker());
		form_element[0].submit();
	},
	
	_template_import_state: null,
	
	templateImportSubmit: function(){
		var csv_value = d2ScreenManager._template_import_state.input.val();
		
		if(csv_value == ""){
			alert("Please enter data that you wish to import");
			return;
		}
		
		var input_source_text = jQuery("<input type='hidden'/>");
		input_source_text.attr("name", d2ScreenManager._template_import_state.bindingName + ".sourceText");
		input_source_text.val(csv_value);

		var input_source_type = jQuery("<input type='hidden'/>");
		input_source_type.attr("name", d2ScreenManager._template_import_state.bindingName + ".sourceType");
		input_source_type.val(d2ScreenManager._template_import_state.sourceType);
		
		var input_target_class = jQuery("<input type='hidden'/>");
		input_target_class.attr("name", d2ScreenManager._template_import_state.bindingName + ".targetClass");
		input_target_class.val(d2ScreenManager._template_import_state.targetClass);
		
		d2ScreenManager.showSimpleModalMsg("Import", "Loading, please wait...");
		
		var form_element = $("#form");
		form_element.find("#d2RootAction").val("importFromCustomTemplate");
		form_element.append(input_source_text);
		form_element.append(input_source_type);
		form_element.append(input_target_class);
		form_element.append(d2ScreenManager.getFormSubmissionMarker());
		form_element[0].submit();
	},
	
	templateImportDownload: function(){
		window.open(d2ScreenManager._template_import_state.downloadTemplateUrl);
	},
	
	showTemplateImportPopup: function(sourceElement){
		var popup = $("#templateImportPopup");
		if(! popup.length){
			alert("templateImportPopup not found");
			return;
		}
		
		var sourceObject = $(sourceElement);
		
		d2ScreenManager._template_import_state = {};
		d2ScreenManager._template_import_state.bindingName = sourceObject.attr("name");
		d2ScreenManager._template_import_state.targetClass = sourceObject.attr("templateImportTargetClass");
		d2ScreenManager._template_import_state.sourceType = sourceObject.attr("templateImportSourceType");
		d2ScreenManager._template_import_state.downloadTemplateUrl = sourceObject.attr("templateUrl");

		var source_input = popup.find("textarea[templateImport=source]");
		d2ScreenManager._template_import_state.input = source_input;
		source_input.val("");

		var template_url = sourceObject.attr("templateUrl");
		if(template_url == "" || template_url == null){
			popup.find("a[templateImport=templateLink]").css("display","none");
		}
		
		tb_show(sourceObject.attr("title"), sourceObject.attr("params"), false);
	},
	
	cloneAndAddAsSiblingParam: [],
	
	cloneAndAddAsSibling: function(id, dataNodeClass, customTemplateKey, numNewRowsOnCreate) {
		d2ScreenManager.showSimpleModalMsg("New Record", "Creating new record, please wait...");
		var param = [];
		param["id"] = id;
		param["dataNodeClass"] = dataNodeClass;
		param["customTemplateKey"] = customTemplateKey;
		param["numNewRowsOnCreate"] = numNewRowsOnCreate;
		d2ScreenManager.cloneAndAddAsSiblingParam = param;
		setTimeout("d2ScreenManager._cloneAndAddAsSibling_delayed()", 0);
	},
	
	_cloneAndAddAsSibling_delayed: function() {
		id 					= d2ScreenManager.cloneAndAddAsSiblingParam["id"];
		dataNodeClass 		= d2ScreenManager.cloneAndAddAsSiblingParam["dataNodeClass"];
		customTemplateKey 	= d2ScreenManager.cloneAndAddAsSiblingParam["customTemplateKey"];
		numNewRowsOnCreate 	= d2ScreenManager.cloneAndAddAsSiblingParam["numNewRowsOnCreate"];
		var isFirstRecord 	= true;
		for(i=0; i<numNewRowsOnCreate; i++) {
			this._cloneAndAddAsSibling(id, dataNodeClass, customTemplateKey, isFirstRecord, i);
			isFirstRecord = false;
		}
		d2ScreenManager.hideSimpleModalMsg();
		var divOffset = $('#ContentWrapper').scrollTop() - 180;
	    var pOffset = $('#anchor_' + d2ScreenManager.firstNewRecordId).offset().top;
	    var pScroll = pOffset + divOffset;
	    $("#ContentWrapper").animate({scrollTop: pScroll + 'px'}, 1000, 'easeInOutQuad');

		if(d2ScreenManager.firstInput != null) $(d2ScreenManager.firstInput).focus();
	},
	
	_cloneAndAddAsSibling: function(id, dataNodeClass, customTemplateKey, isFirstRecord, i){
		this.toggleMainBlockUI();
		var current_record_id = "_new_" + i + "_" + new UUID();
		var new_record_id = "";
		var current_id = "_empty_";
		if(customTemplateKey != "") current_id = customTemplateKey;

		var template = $("#row_" + id);
		var new_instance = template.clone(true).attr("d2Template", "0").attr("d2NewRecord", "1").removeClass("hide").attr("id", function() {
			return d2ScreenManager.replaceFirst($(this).attr("id"),current_id,current_record_id);
		}).attr("d2RecordId", function() {
			new_record_id = d2ScreenManager.replaceFirst($(this).attr("d2RecordId"),current_id,current_record_id);
			return new_record_id;
		});
		
		if(isFirstRecord)
			d2ScreenManager.firstNewRecordId = new_record_id;

		this.getCurrentRecordLabels(new_instance, id).not("[showInEdit=1]").addClass("hide");
		
		d2ScreenManager.firstInput = null;
		
		new_instance.find("input[d2RecordId=" + id + "][isTemplate], textarea[d2RecordId=" + id + "][isTemplate], select[d2RecordId=" + id + "][isTemplate], table[d2RecordId=" + id + "][isTemplate], img[d2RecordId=" + id + "][isTemplate]").each(function(){
			if(d2ScreenManager.firstInput == null && ($(this).attr("type") == "text") && $(this).attr("d2CreateNew") != "1" && $(this).filter(":input:visible").length) {
				if(isFirstRecord)
					d2ScreenManager.firstInput = this;
			}
			$(this).attr("isTemplate","0");
		});
		
		this.showCancelButton(new_instance,id,true);
		this.setRecordAction(new_instance,id,"save");

		new_instance.find("input[d2ReplaceName=1],textarea[d2ReplaceName=1],img[d2ReplaceName=1],a[d2ReplaceName=1],span[d2ReplaceName=1],select[d2ReplaceName=1],tr[d2ReplaceName=1],div[d2ReplaceName]").each(function(){
			var e = $(this);
			if(e.attr("name") != undefined) {
				e.attr("name", function(){
					return d2ScreenManager.replaceFirst(e.attr("name"),current_id,current_record_id);
				});
			}
			
			if(e.attr("id") != undefined) {
				e.attr("id", function(){
					return d2ScreenManager.replaceFirst(e.attr("id"),current_id,current_record_id);
				});
			}
		});

		new_instance.find("tr[d2RecordId], input[d2RecordId], textarea[d2RecordId], select[d2RecordId], img[d2RecordId], a[d2RecordId], label[d2RecordId], ul[d2RecordId], table[d2RecordId], span[d2RecordId], div[d2RecordId]").each(function() {
			$(this).attr("d2RecordId", function(){
				return d2ScreenManager.replaceFirst($(this).attr("d2RecordId"),current_id,current_record_id); 
			});
		});
		
		new_instance.find("input[timeField][d2RecordId=" + new_record_id + "]").each(function() {
			$(this).siblings("a").attr("d2TimeId", function() {
				var d2TimeId = $(this).attr("d2TimeId");
				if(d2TimeId) {
					d2TimeId = d2TimeId.replace(/_empty_/, $.ptTimeSelect._counter);
					return d2TimeId;
				}
			});
			
			$(this).ptTimeSelect({
				labels: {
					clazz:'show',
					d2FieldType:'input',
					isTemplate:'0',
					recordId:new_record_id,
					recordField: $(this).attr("timefield")
				}
			});
		});

		this.afterCreate(current_record_id, dataNodeClass, new_instance);
		template.before(new_instance);			
	},
	
	afterCreate : function(current_record_id, current_class, new_instance) {
	},
	
	replaceFirst: function(value, find, replace){
		if(value == null || value == undefined) return value; 
		var i = value.indexOf(find);
		if(i == -1) return value;
		return value.substr(0,i) + replace + value.substr(i + find.length);
	},
	
	replaceAll: function(value, find, replace) {
		if(value == null || value == undefined) return value;		
		return value.replace(eval("/" + find + "/g"), replace);
	},
	
	toggleUndoAutoTime: function(currentObj) {
		var id = currentObj.attr("d2TimeId");
		currentObj.addClass("hide");
		$("#" + id).css({display:"inline"});
	},
	
	toggleAutoTime: function(currentObj) {
		var value = currentObj.val();
		if($.trim(value) == "") {
			currentObj.val("");
			currentObj.parent().css({display:"none"});
			currentObj.parent().siblings("a").removeClass("hide");
		}
	},
	
	getCurrentRecordInputs: function(targetObj, d2RecordId){
		return targetObj.find("input[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "], textarea[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "], select[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "], table[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "], a[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "], img[d2FieldType=input][isTemplate=0][d2RecordId=" + d2RecordId + "]");
	},
	
	getAllRecordInputs: function(targetObj){
		return targetObj.find("input[d2FieldType=input][isTemplate=0], textarea[d2FieldType=input][isTemplate=0], select[d2FieldType=input][isTemplate=0], table[d2FieldType=input][isTemplate=0], a[d2FieldType=input][isTemplate=0], img[d2FieldType=input][isTemplate=0]");
	},
	
	getCurrentRecordLabels: function(targetObj, d2RecordId){
		return targetObj.find("label[d2FieldType=label][d2RecordId=" + d2RecordId + "], ul[d2FieldType=label][d2RecordId=" + d2RecordId + "], a[d2FieldType=label][d2RecordId=" + d2RecordId + "]");
	},

	getAllRecordLabels: function(targetObj){
		return targetObj.find("label[d2FieldType=label], ul[d2FieldType=label], a[d2FieldType=label]");
	},
	
	setRecordAction: function(targetObj, d2RecordId, action){
		targetObj.find("input[d2RecordHidden=action][type=hidden][d2RecordId=" + d2RecordId + "]").attr("value", action);
	},

	getRecordAction: function(targetObj, d2RecordId){
		return targetObj.find("input[d2RecordHidden=action][type=hidden][d2RecordId=" + d2RecordId + "]").attr("value");
	},

	setAllRecordsAction: function(targetObj, action){
		targetObj.find("input[d2RecordHidden=action][type=hidden]").attr("value", action);
	},
	
	showCancelButton: function(targetObj, d2RecordId, isShowCancelButton){
		targetObj.find("input[d2RecordActionBtn][d2RecordId=" + d2RecordId + "]").each(function() {
			var actionText = $(this).attr("d2RecordActionBtn");
			if(actionText == "edit" || actionText == "delete") {
				if(isShowCancelButton){
					$(this).addClass("hide");
				}else{
					$(this).removeClass("hide");
				}
			} else if(actionText == "cancel") {
				if(isShowCancelButton){
					$(this).removeClass("hide");
				}else{
					$(this).addClass("hide");
				}
			}
		});
	},

	showAllCancelButtons: function(targetObj, isShowCancelButton){
		targetObj.find("input[d2RecordActionBtn]").each(function() {
			var actionText = $(this).attr("d2RecordActionBtn");
			if(actionText == "edit" || actionText == "delete") {
				if(isShowCancelButton){
					$(this).addClass("hide");
				}else{
					$(this).removeClass("hide");
				}
			} else if(actionText == "cancel") {
				if(isShowCancelButton){
					$(this).removeClass("hide");
				}else{
					$(this).addClass("hide");
				}
			}
		});
	},
	
	hideAllChildActionButtons: function(targetObj, d2RecordId){
		targetObj.find("input[d2RecordActionBtn]").not("[d2RecordId=" + d2RecordId + "]").addClass("hide");
	},
	
	continueToggleUI: true,
	
	toggleUI: function(selectedAction, flag, dataNodeClass) {
		var id 			= selectedAction.attr("d2RecordId");
		var targetObj 	= $("#row_" + id);
		var isNewRecord = (targetObj.attr("d2NewRecord") == 1);
		var previousAction = this.getRecordAction(targetObj,id);
		
		if(isNewRecord){
			if(flag == "cancel" || flag == "delete") targetObj.remove();
			this.toggleEditModeBlockUI(flag);
			return;
		}
		
		if(flag == "edit") {
			this.getCurrentRecordLabels(targetObj, id).not("[showInEdit=1]").addClass("hide");
			
			var visible_inputs = this.getCurrentRecordInputs(targetObj, id).not("[type=hidden]");
			var first_input = null;
			visible_inputs.each(function() {
				if(first_input == null && ($(this).attr("type") == "text") && $(this).attr("d2CreateNew") != "1") { first_input = $(this); }
				$(this).removeClass("hide");
			});
			
			targetObj.find("img[d2CreateNewWidget=control][d2RecordId=" + id + "]").removeClass("hide");
			this.setRecordAction(targetObj,id,"save");
			this.showCancelButton(targetObj,id,true);
			if(first_input != null) {
				try {
					first_input.focus();
				} catch(e) {}
			}
		} else if(flag == "cancel") {
			if(previousAction == "save"){
				this.getCurrentRecordLabels(targetObj,id).removeClass("hide");
				this.getCurrentRecordInputs(targetObj,id).not("[type=hidden]").each(function() {
					$(this).addClass("hide");
					var originValue = $(this).attr("d2OriginValue");
					if(originValue) $(this).val(originValue); // place back the original value
					$(this).removeClass("error"); // clear error class
				});
				targetObj.find("img[d2CreateNewWidget=control][d2RecordId=" + id + "],span[d2CreateNewWidget=input][d2RecordId=" + id + "]").addClass("hide");
				this.setRecordAction(targetObj,id,"");
				this.showCancelButton(targetObj,id,false);
				targetObj.find("span[d2ErrorMsg=1][d2RecordId=" + id + "]").html("");
			}else if(previousAction == "delete"){
				targetObj.find(".deleted").removeClass("deleted");
				this.setAllRecordsAction(targetObj,"");
				this.showAllCancelButtons(targetObj,false);
				targetObj.find("input[d2recordrootactionbtn]").removeClass("hide");
			}
		} else if(flag == "delete") {
			this.onDeleteClick(id, dataNodeClass, targetObj);
			if(this.continueToggleUI) {
				this.getAllRecordLabels(targetObj).removeClass("hide").parent().addClass("deleted");
				this.getAllRecordInputs(targetObj).not("[type=hidden]").addClass("hide");
				targetObj.find("img[d2CreateNewWidget=control],span[d2CreateNewWidget=input]").addClass("hide");
				targetObj.find("span[d2ErrorMsg=1]").html("");
				this.setAllRecordsAction(targetObj,"delete");
				this.showCancelButton(targetObj,id,true);
				this.hideAllChildActionButtons(targetObj, id);
				targetObj.find("input[d2recordrootactionbtn]").addClass("hide");
			}
		}
		
		if(this.continueToggleUI) this.toggleEditModeBlockUI(flag);
		
		this.resetState(flag);
	},
	
	onDeleteClick: function(currentRecordId, dataNodeClass, targetObj) {
	},
	
	resetState: function(flag) {
		this.continueToggleUI = true;
	},
	
	toggleExtraDataAjax: function(currentObj, url, ispopup) {
		var id = currentObj.attr("d2RecordId");
		$("#extraData_content_" + id).empty();
		$("#extraData_img_" + id).attr("src", "images/indicator.gif");
		var params = this.serializeBeforeSubmit(currentObj);
		var paramIndex = url.indexOf("?");
		if( paramIndex != -1 ) {
			params += url.substring(paramIndex + 1, url.length);
			url = url.substring(0, paramIndex);
		}
		
		$.get(url, params, function(data) {
			$("#extraData_content_" + id).empty().append(data).bgiframe();
			if(ispopup)
				$("#extraData_img_" + id).attr("src", "images/grow.gif");
			else
				$("#extraData_img_" + id).attr("src", "images/ftv2blank.gif");
		});
	},
	
	toggleExtraData: function(currentObj) {
		var id 		  = currentObj.attr("d2RecordId");
		var position  = currentObj.offset();
		var targetObj = document.getElementById("extraData_content_" + id);
		targetObj.style.left = (position.left + 10) + "px";
		$("#extraData_content_" + id).bgiframe().removeClass("hide");
	},
	
	toggleUndoExtraData: function(currentObj) {
		d2ScreenManager.extraDataObj  = $("#extraData_content_" + currentObj.attr("d2RecordId"));
		d2ScreenManager.extraData 	  = setTimeout('d2ScreenManager.closeExtraData()', 500);
	},
	
	closeExtraData: function() {
		d2ScreenManager.extraDataObj.addClass("hide");
	},
	
	toggleCreateNew: function(currentObj, isChild) {
		var d2RecordId = currentObj.attr("d2RecordId");
		var targetObj  = $("#row_" + d2RecordId);
		var targetDropdown = currentObj.attr("targetDropdown");
		var targetDropdown_element = targetObj.find("#" + d2RecordId + "_" + targetDropdown);
		var createnew_input = targetObj.find("#" + d2RecordId + "_" + targetDropdown + "_cnw_input");
		
		// set dropdown value to blank
		targetDropdown_element.val("");
		
		currentObj.addClass("hide");
		targetDropdown_element.addClass("hide");
		createnew_input.removeClass("hide");
		
		// toggle validation between normal input and "create new" input
		targetDropdown_element.attr("d2Valid", "").siblings("span[d2ErrorMsg=1]").html("");
		createnew_input.find("[d2FieldType=input]").attr("d2Valid","1");

		if(isChild){
			createnew_input.find("a[d2CreateNewWidget=undo]").addClass("hide");
		}else{
			createnew_input.find("a[d2CreateNewWidget=undo]").removeClass("hide");
		}
		
		targetObj.find("img[d2CreateNewWidget=control][d2RecordId=" + d2RecordId + "][cascadeParent=" + targetDropdown + "]").each(function() {
			d2ScreenManager.toggleCreateNew($(this), true);
		});
	},
	
	toggleUndoCreateNew: function(currentObj) {
		var d2RecordId = currentObj.attr("d2RecordId");
		var targetObj  = $("#row_" + d2RecordId);
		var targetDropdown = currentObj.attr("targetDropdown");
		var targetDropdown_element = targetObj.find("#" + d2RecordId + "_" + targetDropdown);
		var createnew_control = targetObj.find("#" + d2RecordId + "_" + targetDropdown + "_cnw_control");
		var createnew_input = targetObj.find("#" + d2RecordId + "_" + targetDropdown + "_cnw_input");
		
		targetDropdown_element.removeClass("hide");
		createnew_control.removeClass("hide");
		createnew_input.addClass("hide");
		
		targetDropdown_element.attr("d2Valid", "1");
		targetDropdown_element.val(targetDropdown_element.attr("d2OriginValue"));
		createnew_input.find("[d2FieldType=input]").val("").attr("d2Valid","").siblings("span[d2ErrorMsg=1]").html("");
		
		targetObj.find("a[d2CreateNewWidget=undo][d2RecordId=" + d2RecordId + "][cascadeParent=" + targetDropdown + "]").each(function() {
			d2ScreenManager.toggleUndoCreateNew($(this));
		});
	},
	
	toggleSelectRecord: function() {
		var currentTxt = $("#d2SelectRecord").val();
		var allCheckboxSpan = $("span.record-checkbox-container");
		if(currentTxt == "Select All") {
			$("#d2SelectRecord").val("Un-select All");
			allCheckboxSpan.each(function(){
				d2ScreenManager.setRecordSelected($(this),true);
			});
		} else {
			$("#d2SelectRecord").val("Select All");
			allCheckboxSpan.each(function(){
				d2ScreenManager.setRecordSelected($(this),false);
			});
		}
		this.toggleCheckbox();
	},
	
	toggleCheckbox: function() {
		var rowSelectors = $("input[d2RowSelector=1]");
		var numCheckedRecords = rowSelectors.not("[value=]").length;
		var numUncheckedRecords = rowSelectors.not("[value=1]").length;
		
		// none of the checkbox checked, then make the button to show "Select All"
		if(numUncheckedRecords > 0) {
			$("#d2SelectRecord").val("Select All");
		// one of the checkbox checked, then make the button to show "Un-select All"
		} else {
			$("#d2SelectRecord").val("Un-select All");
		}
		this.toggleSelectedActionButtonOn(numCheckedRecords != 0);
	},
	
	toggleSelectedActionButtonOn: function(turnOn) {
		if(turnOn) {
			$("#copySelected").removeClass("hide");
			$("#editSelected").removeClass("hide");
			$("#deleteSelected").removeClass("hide");
		} else {
			$("#copySelected").addClass("hide");
			$("#editSelected").addClass("hide");
			$("#deleteSelected").addClass("hide");
		}
	},
	
	toggleEditModeBlockUI: function(mode) {
		if(mode == "edit" || mode == "delete") {
			this.toggleMainBlockUI();
		} else if(mode == "cancel") {
			var numVisibleCancel = $("input.cancel-link", "#form").not(".hide").length;
			if(numVisibleCancel == 0) {
				var refreshAfterAllCancel = "";
				if(currentPageState){
					//if(currentPageState.dirtyFlag) refreshAfterAllCancel = "1"; // this is no longer necessary, label now will not include rejected binding error, it is ok just to show the label when click cancel -ssjong
				}
				if(refreshAfterAllCancel == "") refreshAfterAllCancel = $("#cancel_top").attr("refreshAfterAllCancel");
				if(refreshAfterAllCancel == "1") {
					var targetURL = document.URL;
					if(targetURL.indexOf("?") > 0) {
						targetURL = targetURL.substring(0, targetURL.indexOf("?"));
					}
					window.location.href = targetURL;
				} else {
					this.toggleMainUnblockUI(true);
				}
			}
		}
	},
	
	toggleMainBlockUI: function() {
		if(!this.UIblocked) {
			$("#root-buttons").hide();
			$("#saveAndCancel_top").show(); $("#saveAndCancel_bottom").show();
			$("#topMenuWrapper").block();
			$("#navigationHistoryTrend").block();
			$("#topContainerWrapper").block();
			if ($.browser.msie) {
				$("div.mmenu").css({display:"none"});
			}
			$('#main-tabs').tabsDisable(2);
			$('#main-tabs').tabsDisable(3);
			$("#summary").block();
			this.UIblocked = true;
		}
	},
	
	toggleMainUnblockUI: function(forceUnblock) {
		if(this.UIblocked || forceUnblock) {
			$("#root-buttons").show();
			this.closeSystemMessage();
			$("#saveAndCancel_top").hide(); $("#saveAndCancel_bottom").hide();
			$("#topMenuWrapper").unblock();
			if ($.browser.msie) {
				$("div.mmenu").css({display:""});
			}
			$("#navigationHistoryTrend").unblock();
			$("#topContainerWrapper").unblock();
			$('#main-tabs').tabsEnable(2);
			$('#main-tabs').tabsEnable(3);
			$("#summary").unblock();
			this.UIblocked = false;
		}
	},
	
	toggleImportExportBlockUI: function() {
		if(!this.UIblocked) {
			$("#topMenuWrapper").block();
			$("#topContainerWrapper").block();
			$('#main-tabs').tabsDisable(1);
			$('#main-tabs').tabsDisable(3);
			$("#summary").block();
			this.UIblocked = true;
		}
	},
	
	toggleLoading: function() {
		$.blockUI('Loading, please wait...');
	},
	
	toggleUnloading: function() {
		$.unblockUI();
	},
	
	getFormSubmissionMarker: function(){
		return "<input type='hidden' name='__mainformsubmission' value='1' />";
	},
	
	getIEPreviewFormSubmissionMarker: function(){
		return "<input type='hidden' name='__iepreviewformsubmission' value='1' />";
	},
	
	getInvokeCustomFilterMarker: function(invocationKey) {
		return "<input type='hidden' name='_invokeCustomFilter' value='" + invocationKey + "' />";
	},
	
	getSuppressValidationMarker: function() {
		return "<input type='hidden' name='__suppress_validation' value='true' />";
	},
	
	onChangeRefreshPage: function(message) {
		d2ScreenManager.showSimpleModalMsg(message, "Loading, please wait...");
		var form_element = $("#form");
		form_element.append(d2ScreenManager.getFormSubmissionMarker());
		form_element.append(d2ScreenManager.getSuppressValidationMarker());
		form_element[0].submit();
	},
	
	onChangeRefreshIEPage: function(message) {
		d2ScreenManager.showSimpleModalMsg(message, "Loading, please wait...");
		var form_element = $("#previewForm");
		$("#ieTabMode", form_element).val("ie");
		form_element.append(d2ScreenManager.getFormSubmissionMarker());
		form_element.append(d2ScreenManager.getSuppressValidationMarker());
		form_element[0].submit();
	},
	
	discardUnnecessaryInputsBeforeFormSubmit: function(formElement) {
		var allRows = formElement.find("tr[d2Template]");
	
		allRows.each(function(){
			var current_element = $(this);
			var d2RecordId = current_element.attr("d2RecordId");
			var current_action = d2ScreenManager.getRecordAction(current_element,d2RecordId);
			if(current_action == "save" || current_action == "copySelected" || current_action == "pasteAsNew") {
				current_element.parents("tr[d2Template]").attr("submit_preprocess","1");
			}else if(current_action == "delete"){
				d2ScreenManager.getCurrentRecordInputs(current_element,d2RecordId).remove();
				current_element.parents("tr[d2Template]").attr("submit_preprocess","1");
			}
			current_element.attr("preprocess_action",current_action);
		});
		
		allRows.each(function(){
			var current_element = $(this);
			var current_action = current_element.attr("preprocess_action");
			if(current_action == "save" || current_action == "delete" || current_action == "copySelected" || current_action == "pasteAsNew"){
				// do nothing
			}else{
				var submit_preprocess = current_element.attr("submit_preprocess");
				var d2RecordId = current_element.attr("d2RecordId");
				if(submit_preprocess == "1"){ // remove inputs for record that contain child record that has an action
					d2ScreenManager.getCurrentRecordInputs(current_element,d2RecordId).remove();
				}else{
					var rowSelector = current_element.find("input[d2RowSelector=1][d2RecordId=" + d2RecordId + "]");
					if(rowSelector.val() == "1"){
						formElement.append(rowSelector.css("display","none"));
					}
					current_element.remove();
				}
			}
		});
	},
	
	removeEmptyNewRecord: function(formElement){	
		// remove empty record that has not been filled
		formElement.find("tr[d2Template=0][d2NewRecord=1]").each(function() {
			var current_element = $(this);
			var d2RecordId = current_element.attr("d2RecordId");
			var inputDirty = false;
			d2ScreenManager.getAllRecordInputs(current_element).not("[type=hidden]").each(function(){
				var value = $(this).attr("value");				
				if(value != undefined && $.trim(value) != "") {
					inputDirty = true;
					return;				
				}
			});
			if(!inputDirty) {
				current_element.remove();			
			}
		});
	},
	
	removeTemplate: function() {
		var templates = $("tr[d2Template=1]");
		if(templates != null || templates != undefined) {
			templates.remove();
		}
	},
	
	resetForm: function() {
		window.location = $("#form")[0].action + "?useFlexClient=true";
	},

	validateD2Inputs: function(formElement){
		var errorCount = 0;
		formElement.find("input[d2FieldType=input][isTemplate=0][d2Valid=1]:visible, textarea[d2FieldType=input][isTemplate=0][d2Valid=1]:visible, select[d2FieldType=input][isTemplate=0][d2Valid=1]:visible").each(function() {
			if(this.name) {
				if(! d2ScreenManager.validator.d2Check(this, true)) {
					errorCount++;
				}
			}
		});
		return errorCount;
	},
		
	triggerAllCancelButtons: function(formId){
		if(currentPageState){
			// this is no longer necessary, label now will not include rejected binding error, it is ok just to show the label when click cancel -ssjong
			//if(currentPageState.dirtyFlag){
				//window.location = $("#" + formId).attr("action");
				//return;
			//}
		}
		
		var btns = $("#" + formId).find("input[d2RecordActionBtn=cancel]").not(".hide");
		if(btns.length){
			btns.each(function(){
				// check again in case it is a "child cancel button"
				$(this).not(".hide").trigger("click");
			});
		}else{
			d2ScreenManager.toggleEditModeBlockUI("cancel");
		}
	},
	
	_popup_msg_box: false,
	
	showSimpleModalMsg: function(title, msg){
		d2ScreenManager._popup_msg_box = jBox.open('inline-jBoxID','inline','<br/><center>' + msg + '</center>',title,'center=true,width=400,height=50,model=true');
	},
	
	hideSimpleModalMsg: function(){
		if(! d2ScreenManager._popup_msg_box) return;
		jBox.close(d2ScreenManager._popup_msg_box);
		d2ScreenManager._popup_msg_box = false;
	},
	
	validateAndSubmitForm: function(sourceBtn, form, rootActionId, isMainForm) {
		d2ScreenManager.showSimpleModalMsg("Submit Form", "Submitting data, please wait...");
		var param = [];
		param["sourceBtn"] = sourceBtn;
		param["form"] = form;
		param["rootActionId"] = rootActionId;
		param["isMainForm"] = isMainForm;
		d2ScreenManager._validateAndSubmitForm_param = param;
		setTimeout("d2ScreenManager._validateAndSubmitForm_delayed()",0);
	},
	
	_validateAndSubmitForm_param: [],
	
	_validateAndSubmitForm_delayed: function() {
		var sourceBtn = d2ScreenManager._validateAndSubmitForm_param["sourceBtn"];
		var form = d2ScreenManager._validateAndSubmitForm_param["form"];
		var rootActionId = d2ScreenManager._validateAndSubmitForm_param["rootActionId"];
		var isMainForm = d2ScreenManager._validateAndSubmitForm_param["isMainForm"];
		
		var isValidate = false;
		
		// if this is an import, check how many selected records
		if(rootActionId == "d2ImportExportPreviewAction") {
			var selectedRecords = $("#previewForm .inner-table").find("input[d2RowSelector=1][value=1]");
			
			// if this is an import and selected record is zero, complaints user they must select at least one record to import
			if(selectedRecords != null && selectedRecords.length == 0) {
				d2ScreenManager.hideSimpleModalMsg();
				alert("Please select at least one record to import.");
				return;
			}

			$("#previewForm .inner-table").find("input[d2RowSelector=1]").not("[value=1]").each(function() {
				var id = $(this).attr("d2RecordId");
				$(this).parents("tr#row_" + id).remove();
			});
		}

		var form_element = $("#" + form);
			
		if(isMainForm) {
			this.removeEmptyNewRecord(form_element);
				
			// validate form before submit
			var errorCount = d2ScreenManager.validateD2Inputs(form_element);
			if(errorCount == 0){
				isValidate = true;
				var deleteCount = form_element.find("input[type=hidden][d2RecordHidden=action][value=delete]").size();
				if(deleteCount > 0 || sourceBtn.attr("action") == "deleteSelected"){
					if(! confirm("Are you sure you want to delete selected record ?")) {
						d2ScreenManager.hideSimpleModalMsg();
						return;
					}
				}
				d2ScreenManager.discardUnnecessaryInputsBeforeFormSubmit(form_element);
			}
		} else if(form == "witsmlForm" || form == "npdForm" || form == "previewForm") {
			isValidate = (d2ScreenManager.validateD2Inputs(form_element) == 0); // zero error
		}
		
		if(isValidate) {
			this.beforeSubmit();
			if(d2ScreenManager.continueProcess) {
				form_element.find("#" + rootActionId).val(sourceBtn.attr("action"));
				if(form == "previewForm") {
					form_element.append(d2ScreenManager.getIEPreviewFormSubmissionMarker());
				}
				form_element.append(d2ScreenManager.getFormSubmissionMarker());
				form_element[0].submit();
			}
		} else {
			d2ScreenManager.hideSimpleModalMsg();
			alert("You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
		}
	},
	
	beforeSubmit: function() {
	},
	
	paginate: function(id, page) {
		$("#paginationId").val(page);
		$("#" + id).submit();
	},
	
	closeSystemMessage: function() {
		$("#systemMessageContent").slideUp();
	},
	
	toggleSystemMessage: function(targetObj) {
		var flag = targetObj.attr("flag");
		if(flag == 0) {
			$("#systemMessage").addClass("hide").removeClass("show");
			$("#errorMessage").addClass("hide").removeClass("show");
			targetObj.attr("flag", "1");
		} else {
			$("#systemMessage").addClass("show").removeClass("hide");
			$("#errorMessage").addClass("show").removeClass("hide");
			targetObj.attr("flag", "0");
		}
	},
	
	submitSummary: function(id) {
		var daily = $("#_sys_menu_daily");
		daily.val(id);
		$("#menuForm")[0].submit();
	},
	
	submitTourSummary: function(id) {
		var tour = $("#_sys_menu_tour");
		tour.val(id);
		$("#menuForm")[0].submit();
	},
	
	submitRefresh: function() {
		$('#sysRefreshForm').submit();
	},
	
	triggerMainTab: function(id) {
		$("#main-tabs").tabsClick(id);
	},
	
	updateMsg : function() {
     $.post("messaging.html",{ time: d2ScreenManager.timestamp }, function(xml) {
       $("#loading").remove();
       d2ScreenManager.addMessages(xml);
     });
     //setTimeout('d2ScreenManager.updateMsg()', 4000);
   	},
   
   	setFieldValue: function(currentField, id) {
   		var currentFieldId = "#" + currentField.attr("d2RecordId") + "_" + id;
   		defaultField = $(currentFieldId).val();
   		if(defaultField == "") {
   			$(currentFieldId).val(currentField.val());
   		}
   	},
   
  	toggleAppleSearch: function() {
		// check whether to show delete button
		var fldValue = $("#srch_fld").val();
		var btn 	 = $("#srch_clear");
		if (fldValue.length > 0 && !d2ScreenManager.clearBtn) {
			btn.css({background: "url('images/srch_r_f2.gif') no-repeat top left"});
			btn.click(function() {
				d2ScreenManager.toggleAppleClearBtn();
			});
			d2ScreenManager.clearBtn = true;

		} else if (fldValue.length == 0 && d2ScreenManager.clearBtn) {
			btn.css({background: "url('images/srch_r.gif') no-repeat top left"});
			btn.click(function() {
				return;
			});
			d2ScreenManager.clearBtn = false;
		}
	},
	
	toggleAppleClearBtn: function() {
		$("#srch_fld").val("");
		this.toggleAppleSearch();
	},
	
	toggleFilter: function() {
		// $("#filterMenu").attr("display", "");
		var isOpen = $("#filterMenu").attr("isShow");
		if(isOpen == "false") {
			// obj.text("Unfilter").removeClass("blue-link").addClass("menu-selected-link");
			$("#filterMenu").attr("isShow", "true").slideDown("fast");
		} else {
			// obj.text("Filter").removeClass("menu-selected-link").addClass("blue-link");
			$("#filterMenu").attr("isShow", "false").slideUp("fast");
		}
	},
	
	toggleLogout: function() {
		var logout = false;
		logout = confirm("Are you sure you want to logout ?");
		if(logout) {
			window.location.href = "logout.jsp";
		}
	},
	
	closeQuickLinkMenu: function() {
		$("#quick-links").slideUp("fast");
	},
	
	showDropDown: function() {
		$("label.barMenuValue").not(".notHide").hide();
		$("select.barMenuDropdown").show().css("display", "inline");
	},
	
    deserializeAndPopulate: function(data, d2RecordId) {
    	var properties = data.split("&");
    	for(var i=0; i<properties.length; i++) {
    		var map = properties[i].split("=");
    		var property = map[0];
    		var value = map[1];
    		
    		$("#" + d2RecordId + "_" + property).attr("value", unescape(value).replace(/\+/g, " "));
    	}
    },
    
    serializeBeforeSubmit: function(currentObj) {
    	var d2RecordId 	= currentObj.attr("d2RecordId");
    	var fields 		= "";
    	$("[d2RecordId=" + d2RecordId + "][d2FieldType=input]").each(function() {
    		var fieldName = $(this).attr("d2SourceField");
    		var fieldValue = $(this).val();
    		if(fieldName != undefined && fieldValue != undefined) {
    			fields += fieldName + "=" + fieldValue + "&";
    		}
    	});
    	return fields.substring(0, fields.length - 1);
    },
	
	toggleAutoPopulate: function(currentObj, dataNodeClass) {
		var d2RecordId 	 = currentObj.attr("d2RecordId") ? currentObj.attr("d2RecordId") : "";
		var dataToSubmit = this.serializeBeforeSubmit(currentObj);
		var clazz 		 = currentObj.attr("d2ClassName") ? currentObj.attr("d2ClassName") : (dataNodeClass ? dataNodeClass : "");
		var currentField = currentObj.attr("d2FieldName") ? currentObj.attr("d2FieldName") : "";
		$.post(document.URL,
			   { ajaxAutoPopulate : 1,
			     className : clazz, 
				 recordId : d2RecordId, 
				 triggeredField : currentField,
				 recordData : dataToSubmit 
			   }, 
				function(data) {
					d2ScreenManager.deserializeAndPopulate(data, d2RecordId);
					d2ScreenManager.toggleUnloading();
		});
	},
	
	sort: function(currentObj) {
		$("#sort").val(currentObj.attr("sort") + "." + currentObj.attr("order"));
		$("#sortForm").submit();
	},
	
	setRecordSelected: function(cbContainer, selected){
		if(selected) {
			cbContainer.find("img").attr("src", "images/checkmark.gif");
			var currentRowId = cbContainer.find("input[d2RowSelector=1]").val("1").attr("d2RecordId");
			//$("#row_" + currentRowId).find("td").not(".record-actions, .child-title").addClass("selected");
		} else {
			cbContainer.find("img").attr("src", "images/checkmark_off.gif");
			var currentRowId = cbContainer.find("input[d2RowSelector=1]").val("").attr("d2RecordId");
			//$("#row_" + currentRowId).find("td").not(".record-actions, .child-title").removeClass("selected");
		}
	},
	
	// set all cascade lookup value to previous selected state if defaultValue is set at showField macro
	toggleFilterMenu: function() {
		var wellMenu = $("#root_sysWellMenu");
		wellMenu.trigger("onchange");
		var cascadeChild = $("select[@cascadeParent=" 
							+ wellMenu.attr("cascadeField") 
							+ "][@d2RecordId="
							+ wellMenu.attr("d2RecordId")
							+ "]");
		var cascadeChildDefaultValue = cascadeChild.attr("cascadeValue");
		cascadeChild.val(cascadeChildDefaultValue);
	},
	
	expandOrCollapseLabelText: function(currentLabel) {
		if(currentLabel.attr("d2IsFullText") == "false") {
			var currentText = currentLabel.html();
			currentText = currentText.substring(0, currentText.length - 3) + currentLabel.attr("d2lot");
			currentLabel.html(currentText);
			
			currentLabel.attr("d2IsFullText", "true");
		} else {
			currentLabel.html(currentLabel.attr("d2tt"));
			currentLabel.attr("d2IsFullText", "false");
		}
	},
	
	formatDate: function(date,format) {
		format=format+"";
		var result="";
		var i_format=0;
		var c="";
		var token="";
		var y=date.getYear()+"";
		var M=date.getMonth()+1;
		var d=date.getDate();
		var E=date.getDay();
		var H=date.getHours();
		var m=date.getMinutes();
		var s=date.getSeconds();
		var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
		// Convert real date parts into formatted versions
		var value=new Object();
		if (y.length < 4) {y=""+(y-0+1900);}
		value["y"]=""+y;
		value["yyyy"]=y;
		value["yy"]=y.substring(2,4);
		value["M"]=M;
		value["MM"]=LZ(M);
		value["MMM"]=MONTH_NAMES[M-1];
		value["NNN"]=MONTH_NAMES[M+11];
		value["d"]=d;
		value["dd"]=LZ(d);
		value["E"]=DAY_NAMES[E+7];
		value["EE"]=DAY_NAMES[E];
		value["H"]=H;
		value["HH"]=LZ(H);
		if (H==0){value["h"]=12;}
		else if (H>12){value["h"]=H-12;}
		else {value["h"]=H;}
		value["hh"]=LZ(value["h"]);
		if (H>11){value["K"]=H-12;} else {value["K"]=H;}
		value["k"]=H+1;
		value["KK"]=LZ(value["K"]);
		value["kk"]=LZ(value["k"]);
		if (H > 11) { value["a"]="PM"; }
		else { value["a"]="AM"; }
		value["m"]=m;
		value["mm"]=LZ(m);
		value["s"]=s;
		value["ss"]=LZ(s);
		while (i_format < format.length) {
			c=format.charAt(i_format);
			token="";
			while ((format.charAt(i_format)==c) && (i_format < format.length)) {
				token += format.charAt(i_format++);
				}
			if (value[token] != null) { result=result + value[token]; }
			else { result=result + token; }
			}
		return result;
	},
	
	isInteger: function(val) {
		var digits="1234567890";
		for (var i=0; i < val.length; i++) {
			if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
		return true;
	},
	
	selectAllChildCheckboxes: function(currentObj) {
		var hiddenValue = currentObj.find("input[name=childCheckAllCheckbox]");
		var img = currentObj.find("img.checkbox-img");
		if(hiddenValue.val() == 1) {
			hiddenValue.val("");
			img.attr("src", "images/checkmark_off.gif");
		} else {
			hiddenValue.val(1);
			img.attr("src", "images/checkmark.gif");
		}
		
		currentObj.parents("table:first").find("span.record-checkbox-container").each(function() {
			d2ScreenManager.setRecordSelected($(this), hiddenValue.val());
		});
		this.toggleCheckbox();
	},
	
	checkboxOnClick: function(currentObj) {
		var d2RecordId 	= currentObj.attr("d2RecordId");
		var selected 	= currentObj.find("input[d2RowSelector=1]").val();
		
		$("#row_" + d2RecordId).find("span.record-checkbox-container").each(function() {
			d2ScreenManager.setRecordSelected($(this), selected != "1");
		});
		this.toggleCheckbox();
	},
	
	toggleQuickLinkButton: function(currentObj) {
		var position = currentObj.offset();
		var quickLink = document.getElementById("quick-links");
		quickLink.style.left = position.left - 1 + "px";
		quickLink.style.display = 'inline';
	},
	
	toggleQuickLinkMoustout: function() {
		d2ScreenManager.quicklink = setTimeout('d2ScreenManager.closeQuickLinkMenu()', 100);
	},
	
	toggleUserNavigationMouseOver: function() {
		clearTimeout(d2ScreenManager.userNavigation);
		this.showClearUserNavigationTrash();
	},
	
	toggleUserNavigationMouseOut: function() {
		d2ScreenManager.userNavigation = setTimeout('d2SM.hideClearUserNavigationTrash()', 500); 
	},
	
	showClearUserNavigationTrash: function() {
		$("#clearUserNavigationHistory").removeClass("hide");
	},
	
	hideClearUserNavigationTrash: function() {
		$("#clearUserNavigationHistory").addClass("hide");
	},
	
	getContentWrapperHeight: function(elem) {
		elem.style.height = document.documentElement.clientHeight - 163 + "px";
	},
	
	getContentWrapperWidth: function(elem) {
		elem.style.width = document.documentElement.clientWidth - 35 + "px";
	},

	getInnerTabContentWrapperHeight: function(elem) {
		elem.style.height = document.documentElement.clientHeight - 60 + "px";
	},
	
	getInnerTabContentWrapperWidth: function(elem) {
		elem.style.width = document.documentElement.clientWidth - 6 + "px";
	},

	getPartnerPortalContentWrapperHeight: function(elem) {
		elem.style.height = document.documentElement.clientHeight - 80 + "px";
	},
	
	getPartnerPortalContentWrapperWidth: function(elem) {
		elem.style.width = document.documentElement.clientWidth - 20 + "px";
	},

	getInnerTabFrameWrapperHeight: function(elem) {
		elem.style.height = document.documentElement.clientHeight - 100 + "px";
	},
	
	getInnerTabFrameWrapperWidth: function(elem) {
		elem.style.width = document.documentElement.clientWidth - 15 + "px";
	},

	validate: function(element){
		d2ScreenManager.validator.d2Check(element);
	},
	
	mainTabsOnClick: function(clickedTab, divOfClickedTab, currentTab){
		if($(clickedTab).attr("href") == "#reportsTab"){
			d2ScreenManager.onReportTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#tightHoleSetupTab"){
			d2ScreenManager.onTightHoleSetupTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#activityMatrixTab"){
			d2ScreenManager.onActivityMatrixTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#gasGraphTab"){
			d2ScreenManager.onGasGraphTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#fileManagerTab"){
			d2ScreenManager.onFileManagerTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#hseRelatedOperationSetupTab"){
			d2ScreenManager.onHseRelatedOperationSetupTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#personnelOnSiteSummaryTab"){
			d2ScreenManager.onPersonnelOnSiteSummaryTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#productionStringHistoryOverviewTab"){
			d2ScreenManager.onProductionStringHistoryOverviewTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#wellHeadHistoryOverviewTab"){
			d2ScreenManager.onWellHeadHistoryOverviewTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#pobMasterListTab"){
			d2ScreenManager.onPobMasterListTabClicked(clickedTab);
		}else if($(clickedTab).attr("href") == "#qcStatusTab"){
			d2ScreenManager.onQcStatusTabClicked(clickedTab);
		}
	},

	onFileManagerTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#fileManagerTab").find("iframe");
			iframe.attr("src",url + "&srcloc=" + escape(window.location));
		}
	},
	
	onTightHoleSetupTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#tightHoleSetupTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onActivityMatrixTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#activityMatrixTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onGasGraphTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#gasGraphTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onHseRelatedOperationSetupTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#hseRelatedOperationSetupTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onPersonnelOnSiteSummaryTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#personnelOnSiteSummaryTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onProductionStringHistoryOverviewTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#productionStringHistoryOverviewTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onWellHeadHistoryOverviewTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#wellHeadHistoryOverviewTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onPobMasterListTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#pobMasterListTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	
	onQcStatusTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var iframe = $("#qcStatusTab").find("iframe");
			iframe.attr("src", url);
		}
	},
	gotoTightHoleSetupTab: function() {
		$("#tightHoleSetupTabHeader").click();
	},
	
	gotoActivityMatrixTab: function() {
		$("#activityMatrixTabHeader").click();
	},
	
	gotoGasGraphTab: function() {
		$("#gasGraphTabHeader").click();
	},
	
	gotoHseRelatedOperationSetupTab: function() {
		$("#hseRelatedOperationSetupTabHeader").click();
	},
	
	gotoPersonnelOnSiteSummaryTab: function() {
		$("#personnelOnSiteSummaryTabHeader").click();
	},
	
	gotoProductionStringHistoryOverviewTab: function() {
		$("#productionStringHistoryOverviewTabHeader").click();
	},
	
	gotoWellHeadHistoryOverviewTab: function() {
		$("#wellHeadHistoryOverviewTabHeader").click();
	},
	
	gotoPobMasterListTab: function() {
		$("#pobMasterListTabHeader").click();
	},
	
	gotoQcStatusTab: function() {
		$("#qcStatusTabHeader").click();
	},
	
	onReportTabClicked: function(clickedTab){
		var tab_elem = $(clickedTab);
		if(tab_elem.attr("loaded") == "true") return;
		tab_elem.attr("loaded", "true");

		var url = tab_elem.attr("dynamicTabUrl");
		if(url){
			var report_tab_iframe = $("#reportsTab").find("iframe");
			report_tab_iframe.attr("src",url);
		}
	},
	
	copyOrPasteAsNewRoot: function(currentObj, action) {
		$("#d2RootCopySelectedClass").val(currentObj.attr("d2ClassName"));
		$("#d2RootAction").val(action);
		var form_element = $("#form");
		form_element.append(d2ScreenManager.getFormSubmissionMarker());
		form_element[0].submit();
	},
	
	copyOrPasteAsNewSelectedChild: function(currentObj, action) {
		var id = currentObj.attr("d2RecordId");
		var targetObj = $("#row_" + id);
		targetObj.find("input[d2RecordId=" + id + "][d2RecordHidden=copySelectedClass][type=hidden]").val(currentObj.attr("d2ClassName"));
		this.setRecordAction(targetObj, id, action);
		
		$("#applyChangesButton").trigger("click");
	},
	
	clearUserNavigationHistory: function() {
		$.post("userNavigationHistoryAjax.html", "action=clear", function(data) {
			$("#navigationHistoryTrend").empty();
		});
	},
	
	addMessage: function(msg, type) {
		var found = false;
		$("#systemMessage li").each(function() {
			if($.trim($(this).html().toLowerCase()) == $.trim(msg.toLowerCase()))
				found = true;
		});
		
		if(! found) {
			$("#systemMessage").append("<li class='" + type + "'>" + msg + "</li>");
		}
		$("#systemMessageWrapper").css({display:"inline"});
		$("#systemMessageContent").show();
	},
	
	removeMessage: function(msg) {
		$("#systemMessage li").each(function() {
			if($.trim($(this).html().toLowerCase()) == $.trim(msg.toLowerCase())) {
				$(this).remove();
			}
		});
		
		var size = $("#systemMessage li").size();
		if(size == 0) {
			d2ScreenManager.closeSystemMessage();
		}
	},
	
	npdAjaxSubmit: function(id, service, versionKind) {
		var actionLink = $("#" + id);
		
		if (id == "npdDownloadForm") {
			// NPD Download feature is submitted via a form
			$("#npdService").val(service);
			$("#npdVersionKind").val(versionKind);
			actionLink.submit();
		
		} else {
			// NPD Preview and Send need to open separate dialog boxes to display their content
			$("#" + id).attr("href", function() {
				var targetUrl = $(actionLink).attr("targetUrl");
				var actionStr = $(actionLink).attr("npdAction");
				return targetUrl + "&service=" + encodeURIComponent(service) + "&versionKind=" + encodeURIComponent(versionKind) + "&action=" + encodeURIComponent(actionStr);
			});
			actionLink.click();
		}
	}
};

var d2SM = d2ScreenManager;

/* ==================== custom added jQuery validation method ========================== */
// custom validation for maximum and minimum. current validator can't accept ","
$.validator.addMethod("maxValue", function(value, element, param) {
    return +value.replace(/,/g, "") <= param;
}, "Please enter a value less than or equal to {0}.");

$.validator.addMethod("minValue", function(value, element, param) {
    return +value.replace(/,/g, "") >= param;
}, "Please enter a value more than or equal to {0}."); 

$.validator.addMethod("date", function(value, element, param) {
	var _value = $.trim(value); 
	if(_value == "now" || _value == "today" || _value == "tomorrow" || _value == "yesterday") return true;
	return this.optional(element) || !/Invalid|NaN/.test(new Date(_value));
}, "Please enter a valid date.");

$.validator.addMethod("datetime", function(value, element, param) {
	return true;
}, "Please enter a valid datetime.");

$.validator.addMethod("number", function(value, element, param) {
	var _value = $.trim(value);
	_value = _value.replace(/./g, ""); // remove decimal
	_value = _value.replace(/,/g, ""); // remove comma
	_value = _value.replace(/ /g, ""); // remove empty space
	var startswith = _value.substring(0, 1);
	if(startswith == "-") {
		_value = _value.substring(1, _value.length);
	}
	if(!d2ScreenManager.isInteger(_value)) return false;
	return true;
}, "Please enter a valid number.");

$.validator.addMethod("time", function(value, element, param) {
	if($.trim(value) == "") return true;
	if(! param) return true;
	var time = value.replace(":","");
	if(time.length == 3) time = "0" + time;
	if(time.length != 4) return false;
	var h = time.substr(0,2);
	var m = time.substr(2,2);
	if(isNaN(h) || isNaN(m)) return false;
	if(h >= 0 && h <= 24 && m >= 0 && m <= 59) return true;
    return false;
}, "Please enter a time in the format of hh:mm (e.g. 12:00)"); 
/* ============================= end of declaration ==================================== */

/* ============================= jQuery Logging plugin - only work on !IE ============== */
jQuery.fn.log = function (msg) {
    console.log("%s: %o", msg, this);
    return this;
};

/*
============
PAGE ON LOAD
============
*/
$(document).ready(function(){
	d2ScreenManager.timestamp = 0;
	d2ScreenManager.quicklink = 0;
	d2ScreenManager.userNavigation = 0;
	d2ScreenManager.extraData = 0;
	d2ScreenManager.delay 	  = 1;
	d2ScreenManager.UIblocked = false;
	d2ScreenManager.selectedAction = null;
	d2ScreenManager.continueProcess = true;
	d2ScreenManager.firstInput = null;
	d2ScreenManager.firstNewRecordId = null;
		
	d2ScreenManager.toggleFilterMenu();
	
	/* ========= jQUERY VALIDATION PLUGIN ========== */
	d2ScreenManager.validator = new jQuery.validator([],[]);
	
	/* ========= IMPORT BUTTON ========= */
	/*
	$('#import').click(function() {
  		$('#importDataTypes').show();
	});
	*/
	
	/*
	=====
	TABS
	=====
	*/
	$("#main-tabs").tabs({
		click: d2ScreenManager.mainTabsOnClick
	});
	// $("#ieContainer").tabs();
	
	/*
	=======================
	SHOW ELEMENT AFTER LOAD
	=======================
	*/
	// var realBody = document.getElementById("realBody");
	// if(realBody) realBody.style.display = "inline";
	
	/*
	===========
	QUICK LINKS
	===========
	*/
	// var quickLinks = document.getElementById("quick-links");
	// if(quickLinks) $(quickLinks).bgiframe();
	
	$("#systemMessageContent").bgiframe();
	
	$(".tooltip").Tooltip({showURL:false});
	
	/*
	=======================================================
	SETTING MAX HEIGHT AND MAX WIDTH FOR DATA TAB's CONTENT
	=======================================================
	*/
	if(! $.browser.msie) {
		var contentWrapper = document.getElementById("ContentWrapper");
		if(contentWrapper){
			contentWrapper.style.height = self.innerHeight - 160 + "px";
			contentWrapper.style.width = self.innerWidth;
		}

		var partnerPortalContentWrapper = document.getElementById("PartnerPortalContentWrapper");
		if(partnerPortalContentWrapper){
			partnerPortalContentWrapper.style.height = self.innerHeight - 80 + "px";
			partnerPortalContentWrapper.style.width = self.innerWidth;
		}
		
		var tightHoleSetupTab = document.getElementById("tightHoleSetupTab");
		if(tightHoleSetupTab){
			var tightHoleSetupTab_div = $(tightHoleSetupTab).find("div.InnerTabFrameWrapper");
			tightHoleSetupTab_div[0].style.height = self.innerHeight - 100 + "px";
			tightHoleSetupTab_div[0].style.width = self.innerWidth;
		}
		
		var activityMatrixTab = document.getElementById("activityMatrixTab");
		if(activityMatrixSetupTab){
			var activityMatrixSetupTab_div = $(activityMatrixSetupTab).find("div.InnerTabFrameWrapper");
			activityMatrixSetupTab_div[0].style.height = self.innerHeight - 100 + "px";
			activityMatrixSetupTab_div[0].style.width = self.innerWidth;
		}
		
		var gasGraphTab = document.getElementById("gasGraphTab");
		if(gasGraphSetupTab){
			var gasGraphSetupTab_div = $(gasGraphSetupTab).find("div.InnerTabFrameWrapper");
			gasGraphSetupTab_div[0].style.height = self.innerHeight - 100 + "px";
			gasGraphSetupTab_div[0].style.width = self.innerWidth;
		}
		
		var hseRelatedOperationSetupTab = document.getElementById("hseRelatedOperationSetupTab");
		if(hseRelatedOperationSetupTab){
			var hseRelatedOperationSetupTab_div = $(hseRelatedOperationSetupTab).find("div.InnerTabFrameWrapper");
			hseRelatedOperationSetupTab_div[0].style.height = self.innerHeight - 100 + "px";
			hseRelatedOperationSetupTab_div[0].style.width = self.innerWidth;
		}
		
		var personnelOnSiteSummaryTab = document.getElementById("personnelOnSiteSummaryTab");
		if(personnelOnSiteSummaryTab){
			var personnelOnSiteSummaryTab_div = $(personnelOnSiteSummaryTab).find("div.InnerTabFrameWrapper");
			personnelOnSiteSummaryTab_div[0].style.height = self.innerHeight - 100 + "px";
			personnelOnSiteSummaryTab_div[0].style.width = self.innerWidth;
		}
		
		var productionStringHistoryOverviewTab = document.getElementById("productionStringHistoryOverviewTab");
		if(productionStringHistoryOverviewTab){
			var productionStringHistoryOverviewTab_div = $(productionStringHistoryOverviewTab).find("div.InnerTabFrameWrapper");
			productionStringHistoryOverviewTab_div[0].style.height = self.innerHeight - 100 + "px";
			productionStringHistoryOverviewTab_div[0].style.width = self.innerWidth;
		}
		
		var wellHeadHistoryOverviewTab = document.getElementById("wellHeadHistoryOverviewTab");
		if(wellHeadHistoryOverviewTab){
			var wellHeadHistoryOverviewTab_div = $(wellHeadHistoryOverviewTab).find("div.InnerTabFrameWrapper");
			wellHeadHistoryOverviewTab_div[0].style.height = self.innerHeight - 100 + "px";
			wellHeadHistoryOverviewTab_div[0].style.width = self.innerWidth;
		}
		
		var reportsTab = document.getElementById("reportsTab");
		if(reportsTab){
			var reportsTab_div = $(reportsTab).find("div.InnerTabFrameWrapper");
			reportsTab_div[0].style.height = self.innerHeight - 100 + "px";
			reportsTab_div[0].style.width = self.innerWidth;
		}

		var fileManagerTab = document.getElementById("fileManagerTab");
		if(fileManagerTab){
			var fileManagerTab_div = $(fileManagerTab).find("div.InnerTabFrameWrapper");
			fileManagerTab_div[0].style.height = self.innerHeight - 100 + "px";
			fileManagerTab_div[0].style.width = self.innerWidth;
		}
		
		var flashTab = document.getElementById("flashTab");
		if(flashTab){
			var flashTab_div = $(flashTab).find("div.InnerTabFrameWrapper");
			if(flashTab_div[0]) {
				flashTab_div[0].style.height = self.innerHeight - 100 + "px";
				flashTab_div[0].style.width = self.innerWidth;
			}
		}

		
		var pobMasterListTab = document.getElementById("pobMasterListTab");
		if(pobMasterListTab){
			var pobMasterListTab_div = $(pobMasterListTab).find("div.InnerTabFrameWrapper");
			pobMasterListTab_div[0].style.height = self.innerHeight - 100 + "px";
			pobMasterListTab_div[0].style.width = self.innerWidth;
		}
		
		var qcStatusTab = document.getElementById("qcStatusTab");
		if(qcStatusTab){
			var qcStatustTab_div = $(qcStatusTab).find("div.InnerTabFrameWrapper");
			qcStatusTab_div[0].style.height = self.innerHeight - 100 + "px";
			qcStatusTab_div[0].style.width = self.innerWidth;
		}
	}
});