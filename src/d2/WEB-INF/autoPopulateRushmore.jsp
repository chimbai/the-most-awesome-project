<%@ page contentType="text/plain"%>
<%@ page import="com.idsdatanet.d2.drillnet.rushmore.*,com.idsdatanet.d2.core.web.mvc.*,com.idsdatanet.d2.core.dao.QueryProperties,com.idsdatanet.d2.core.model.*,java.net.*,java.util.*,org.apache.commons.lang.StringUtils"%>

<%
	try {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		Map<String,Well> wells = new LinkedHashMap<String,Well>();
		   
		List<Operation> opList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Operation Where (isDeleted is null or isDeleted = false) and sysOperationSubclass='well' and (isCampaignOverviewOperation is null or isCampaignOverviewOperation = false)");    
		for (Operation op : opList)
		{
			if (wells.get(op.getWellUid())== null)
			{
				Well well =(Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class,op.getWellUid());
				wells.put(well.getWellUid(),well);
			}
			OpsDatum datum = null;
			if (StringUtils.isNotBlank(op.getDefaultDatumUid()))
			{
				List<OpsDatum> datums = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From OpsDatum Where (isDeleted is null or isDeleted = false) and opsDatumUid=:opsDatumUid",new String[]{"opsDatumUid"},new Object[]{op.getDefaultDatumUid()});
				if (datums.size()>0)
					datum = datums.get(0);
			}
			
			
			Well well = wells.get(op.getWellUid());
			List<RushmoreWells> rws = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From RushmoreWells Where (isDeleted is null or isDeleted = false) and operationUid=:operationUid", new String[]{"operationUid"}, new Object[]{op.getOperationUid()});
			if (rws.size()<1)
			{
				RushmoreWells newRushmoreWell = new RushmoreWells();
				RushmoreUtils.defaultPopulateProperties(newRushmoreWell,op.getOperationUid(),null,null,false,datum);
				RushmoreUtils.beforeSaveOrUpdate(newRushmoreWell,false,well.getOnOffShore());
				newRushmoreWell.setOperationUid(op.getOperationUid());
				newRushmoreWell.setWellboreUid(op.getWellboreUid());
				newRushmoreWell.setWellUid(op.getWellUid());
				newRushmoreWell.setGroupUid(op.getGroupUid());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newRushmoreWell,qp);
				RushmoreUtils.populateInterruptCodes(null, newRushmoreWell, null, op.getOperationUid(), qp);

			}
		}
		
%>-- SUCCESS
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
